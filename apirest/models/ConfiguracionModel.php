<?php
class ConfiguracionModel extends CI_Model{
	
 public function __construct(){
		parent::__construct();
	}
	
	public function obtenerEmpresa(){
        $this->db->from('empresa');
        $this->db->join('configuracion AS CONFI', 'CONFI.ID_Empresa = empresa.ID_Empresa', 'left');
        $this->db->join('organizacion AS ORG', 'ORG.ID_Empresa = empresa.ID_Empresa', 'left');
        $this->db->join('almacen AS ALMA', 'ORG.ID_Organizacion = ALMA.ID_Organizacion', 'left');
    	$this->db->join('tabla_dato AS TDESTADO', 'TDESTADO.Nu_Valor=empresa.Nu_Tipo_Proveedor_FE AND TDESTADO.No_Relacion = "Tipos_Proveedor_FE"', 'join');
        $this->db->where('empresa.ID_Empresa', $this->user->ID_Empresa);
        $this->db->where('ORG.ID_Organizacion', $this->user->ID_Organizacion);
        $query = $this->db->get();
        return $query->row();
	}
	
	public function inicio(){
	    $query = "
SELECT
 (SELECT COUNT(*) FROM producto WHERE ID_Empresa = " . $this->user->ID_Empresa . ") AS productos,
 (SELECT COUNT(*) FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad=0) AS clientes,
 (SELECT COUNT(*) FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $this->user->ID_Organizacion . " AND ID_Tipo_Asiento=1) AS ventas,
 (SELECT COUNT(*) FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $this->user->ID_Organizacion . " AND ID_Tipo_Asiento=2) AS compras,
 (SELECT COUNT(*) FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $this->user->ID_Organizacion . " AND ID_Tipo_Asiento=1 AND ID_Tipo_Documento IN(3,4,5,6) AND Nu_Estado IN(6,7)) AS documento_pendiente,
 (SELECT COUNT(*) FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $this->user->ID_Organizacion . " AND ID_Tipo_Asiento=1 AND ID_Tipo_Documento IN(3,4,5,6) AND Nu_Estado IN(9,11)) AS documento_error,
 (SELECT COUNT(*) FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $this->user->ID_Organizacion . " AND ID_Tipo_Asiento=1 AND ID_Tipo_Documento IN(2,3,4,5,6) AND Nu_Estado IN(6,8) AND Fe_Emision='" . dateNow('fecha') . "') AS documento_completado,
 (SELECT COUNT(*) FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $this->user->ID_Organizacion . " AND ID_Tipo_Asiento=1 AND ID_Tipo_Documento IN(2,3,4,5,6) AND Nu_Estado=10 AND Fe_Emision='" . dateNow('fecha') . "') AS documento_anulado
FROM
 empresa
WHERE
 ID_Empresa=" . $this->user->ID_Empresa;
	    return $this->db->query($query)->row();
	}
	
	public function reporteGrafico($arrGrafico){
		$sql = "
SELECT
 VC.Fe_Emision,
 MONE.ID_Moneda,
 MONE.No_Signo,
 SUM(COALESCE(VD.total_bfnd, 0)) AS venta_bfnd,
 SUM(COALESCE(VD.total_nc, 0)) AS venta_nc,
 SUM(COALESCE(VD.total_bfnd, 0) - COALESCE(VD.total_nc, 0)) AS venta_neta
FROM
 documento_cabecera AS VC
 JOIN moneda AS MONE ON(VC.ID_Moneda = MONE.ID_Moneda)
 JOIN (
 SELECT
  VC.ID_Documento_Cabecera,
  SUM((CASE WHEN VC.ID_Tipo_Documento IN(2,3,4,6) THEN VD.Ss_SubTotal END)) AS total_bfnd,
  SUM((CASE WHEN VC.ID_Tipo_Documento = 5 THEN VD.Ss_SubTotal END)) AS total_nc
 FROM
  documento_cabecera AS VC
  JOIN moneda AS MONE ON(VC.ID_Moneda = MONE.ID_Moneda)
  JOIN documento_detalle AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 WHERE
  VC.ID_Empresa = " . $this->user->ID_Empresa . "
  AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
  AND VC.ID_Tipo_Asiento = 1
  AND VC.ID_Tipo_Documento IN(2,3,4,5,6)
  AND VC.Nu_Estado IN(6,8)
  AND VC.Fe_Emision BETWEEN '" . $arrGrafico["dInicial"] . "' AND '" . $arrGrafico["dFinal"] . "'
 GROUP BY
  1
 ) AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
WHERE
 VC.ID_Empresa = " . $this->user->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
 AND VC.ID_Tipo_Asiento = 1
 AND VC.ID_Tipo_Documento IN(2,3,4,5,6)
 AND VC.Nu_Estado IN(6,8)
 AND VC.Fe_Emision BETWEEN '" . $arrGrafico["dInicial"] . "' AND '" . $arrGrafico["dFinal"] . "'
GROUP BY
 VC.Fe_Emision,
 MONE.ID_Moneda
ORDER BY
 VC.Fe_Emision DESC;";
 
		$r['Tabla'] = $this->db->query($sql)->result();
		
		$arrDate = explode('-', $arrGrafico["dFinal"]);
		$iYear=$arrDate[0];
		$iMonth=$arrDate[1];
		
		// Reporte Grafico
		$r['Grafica'] = array('Categoria' => '', 'Moneda' => '', 'Vendido' => '');
		$i = 0;
		$x = 0;
		
		for($i = 0; $i <= date('t', strtotime($iYear."/".$iMonth."/01")); $i++){
			$encontrado = true;
			foreach($r['Tabla'] as $t){
				$d = date('d', strtotime($t->Fe_Emision));
				
				if($i == $d){
					$r['Grafica']['Categoria'] .= $i . ($i!=0 ? ',' : '');
					$r['Grafica']['Moneda'] .= $t->No_Signo . ($i!=0 ? ',' : '');
					$r['Grafica']['Vendido'] .= $t->venta_neta . ($i!=0 ? ',' : '');
					
					$encontrado = false;
					break;
				}
			}
			
			if($encontrado == true && $i > 0){
				$r['Grafica']['Categoria'] .= $i . ',';
				$r['Grafica']['Moneda'] .= '0' . ',';
				$r['Grafica']['Vendido'] .= '0' . ',';
			}
		}

		// SQL - Productos mas vendidos
		$sql = "
SELECT
 PROD.ID_Producto,
 M.No_Marca,
 PROD.No_Producto,
 SUM((CASE WHEN VC.ID_Tipo_Documento IN(2,3,4,6) THEN VD.Qt_Producto ELSE 0 END) - (CASE WHEN VC.ID_Tipo_Documento=5 THEN VD.Qt_Producto ELSE 0 END)) AS Qt_Producto
FROM
 documento_cabecera AS VC
 JOIN moneda AS MONE ON(VC.ID_Moneda = MONE.ID_Moneda)
 JOIN documento_detalle AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 JOIN producto AS PROD ON(VD.ID_Producto = PROD.ID_Producto)
 LEFT JOIN marca AS M ON(PROD.ID_Marca = M.ID_Marca)
WHERE
 VC.ID_Empresa = " . $this->user->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
 AND VC.ID_Tipo_Documento IN(2,3,4,5,6)
 AND VC.Nu_Estado IN(6,8)
 AND VC.ID_Tipo_Asiento = 1
 AND VC.Fe_Emision BETWEEN '" . $arrGrafico["dInicial"] . "' AND '" . $arrGrafico["dFinal"] . "'
GROUP BY
 1
ORDER BY
 Qt_Producto DESC
LIMIT 5;";
		$r['arrProductosVendidos'] = $this->db->query($sql)->result();

        // SQL - Mejores Clientes
		$sql = "
SELECT
 COUNT(*) Cantidad,
 CLI.No_Entidad AS No_Razsocial,
 SUM(VD.Qt_Producto) AS Qt_Producto,
 ROUND(SUM(COALESCE(VD.total_bfnd, 0) - COALESCE(VD.total_nc, 0)), 2) AS venta_neta
FROM
 documento_cabecera AS VC
 JOIN moneda AS MONE ON(VC.ID_Moneda = MONE.ID_Moneda)
 JOIN entidad AS CLI ON(VC.ID_Entidad = CLI.ID_Entidad)
 JOIN (
 SELECT
  VC.ID_Documento_Cabecera,
  SUM((CASE WHEN VC.ID_Tipo_Documento IN(2,3,4,6) THEN VD.Ss_SubTotal END)) AS total_bfnd,
  SUM((CASE WHEN VC.ID_Tipo_Documento = 5 THEN VD.Ss_SubTotal END)) AS total_nc,
  SUM((CASE WHEN VC.ID_Tipo_Documento IN(2,3,4,6) THEN VD.Qt_Producto ELSE 0 END) - (CASE WHEN VC.ID_Tipo_Documento=5 THEN VD.Qt_Producto ELSE 0 END)) AS Qt_Producto
 FROM
  documento_cabecera AS VC
  JOIN moneda AS MONE ON(VC.ID_Moneda = MONE.ID_Moneda)
  JOIN documento_detalle AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 WHERE
  VC.ID_Empresa = " . $this->user->ID_Empresa . "
  AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
  AND VC.ID_Tipo_Asiento = 1
  AND VC.ID_Tipo_Documento IN(2,3,4,5,6)
  AND VC.Nu_Estado IN(6,8)
  AND VC.Fe_Emision BETWEEN '" . $arrGrafico["dInicial"] . "' AND '" . $arrGrafico["dFinal"] . "'
 GROUP BY
  1
 ) AS VD ON (VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
WHERE
 VC.ID_Empresa = " . $this->user->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
 AND VC.ID_Tipo_Asiento = 1
 AND VC.ID_Tipo_Documento IN(2,3,4,5,6)
 AND VC.Nu_Estado IN(6,8)
 AND VC.Fe_Emision BETWEEN '" . $arrGrafico["dInicial"] . "' AND '" . $arrGrafico["dFinal"] . "'
GROUP BY
 CLI.ID_Entidad,
 MONE.ID_Moneda
ORDER BY
 venta_neta DESC
LIMIT 5;";
		$r['arrMejoresClientes'] = $this->db->query($sql)->result();

        $sql = "
SELECT
 VC.Fe_Emision,
 VC.Fe_Vencimiento,
 VC.Fe_Periodo,
 VC.ID_Documento_Cabecera,
 CLI.No_Entidad,
 CONTACT.No_Entidad AS No_Contacto,
 MONE.No_Signo,
 VC.Ss_Total,
 TDESTADO.No_Class AS No_Class_Estado,
 TDESTADO.No_Descripcion AS No_Descripcion_Estado
FROM
 documento_cabecera AS VC
 JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
 JOIN entidad AS CONTACT ON(CONTACT.ID_Entidad = VC.ID_Contacto)
 JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
WHERE
 VC.ID_Empresa = " . $this->user->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
 AND VC.ID_Tipo_Asiento = 1
 AND VC.ID_Tipo_Documento = 1
 AND VC.Nu_Estado IN(0,1,5)
 AND VC.Fe_Emision BETWEEN '" . $arrGrafico["dInicial"] . "' AND '" . $arrGrafico["dFinal"] . "'
ORDER BY
 VC.Fe_Emision DESC
LIMIT 7;";
		$r['arrOrdenesVenta'] = $this->db->query($sql)->result();

		return $r;
	}
	
    public function actualizarEstadoActualizacionVersionSistema($where, $data){
	    if ( $this->db->update('configuracion', $data, $where) > 0 ) {
	        return array(
				'status' => 'success',
				'style_modal' => 'modal-success',
				'message' => 'Se esta actualizando la nueva versión del sistema, se le notificará en cuanto haya culminado.',
			);
	    } else {
            return array(
				'status' => 'error',
    			'style_modal' => 'modal-danger',
    			'message' => 'No se pudo realizar la actualización, inténtelo más tarde. (Model)',
			);
	    }
    }
}
