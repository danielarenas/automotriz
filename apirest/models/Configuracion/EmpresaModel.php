<?php
class EmpresaModel extends CI_Model{
	var $table                          = 'empresa';
	var $table_tabla_dato               = 'tabla_dato';
	var $table_distrito                 = 'distrito';
	var $table_documento_cabecera       = 'documento_cabecera';
	var $table_configuracion = 'configuracion';
	
    var $column_order = array('Nu_Tipo_Proveedor_FE', 'Nu_Tipo_Ecommerce_Empresa', 'Nu_Documento_Identidad', 'No_Empresa', null);
    var $column_search = array('Nu_Tipo_Proveedor_FE', 'Nu_Tipo_Ecommerce_Empresa', 'Nu_Documento_Identidad', 'No_Empresa', null);
    var $order = array('Nu_Documento_Identidad' => 'asc');
	
	public function __construct(){
		parent::__construct();
        $this->load->database();
	}
	
	public function _get_datatables_query(){
        $this->db->select('empresa.ID_Empresa, Nu_Documento_Identidad, No_Empresa, Txt_Direccion_Empresa, TIPOPROVEEDORFE.No_Class AS No_Class_Proveedor_FE, TIPOPROVEEDORFE.No_Descripcion AS No_Descripcion_Proveedor_FE, TIPOECOMMERCE.No_Class AS No_Class_Ecommerce, TIPOECOMMERCE.No_Descripcion AS No_Descripcion_Ecommerce, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, TDESTADO.Nu_Valor AS Nu_Estado_Empresa, Nu_Tipo_Proveedor_FE, CONFI.ID_Configuracion')
        ->from($this->table)
        ->join($this->table_configuracion . ' AS CONFI', 'CONFI.ID_Empresa = ' . $this->table . '.ID_Empresa', 'left')
        ->join($this->table_tabla_dato . ' AS TIPOPROVEEDORFE', 'TIPOPROVEEDORFE.Nu_Valor=' . $this->table . '.Nu_Tipo_Proveedor_FE AND TIPOPROVEEDORFE.No_Relacion = "Tipos_Proveedor_FE"', 'join')
        ->join($this->table_tabla_dato . ' AS TIPOECOMMERCE', 'TIPOECOMMERCE.ID_Tabla_Dato=' . $this->table . '.Nu_Tipo_Ecommerce_Empresa AND TIPOECOMMERCE.No_Relacion = "Tipos_Ecommerce_Empresa"', 'left')
        ->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor=' . $this->table . '.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_Estados"', 'join');
            
        if ( $this->user->No_Usuario == 'root' ) {
            if( $this->input->post('Filtros_Empresas') == 'Empresa' ){
                $this->db->like('No_Empresa', $this->input->post('Global_Filter'));
            }
            
            if( $this->input->post('Filtros_Empresas') == 'RUC' ){
                $this->db->like('Nu_Documento_Identidad', $this->input->post('Global_Filter'));
            }
        } else {
            $this->db->where('empresa.ID_Empresa', $this->empresa->ID_Empresa);
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $this->db->from($this->table);
        $this->db->where('ID_Empresa',$ID);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function agregarEmpresa($data){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE Nu_Documento_Identidad='" . $data['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			if ( $this->db->insert($this->table, $data) > 0 )
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
		}
		return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
    }
    
    public function actualizarEmpresa($where, $data, $ENu_Documento_Identidad){
		if( $ENu_Documento_Identidad != $data['Nu_Documento_Identidad'] && $this->db->query("SELECT COUNT(*) existe FROM " . $this->table . " WHERE Nu_Documento_Identidad='" . $data['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe > 0 ){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		} else if ( $ENu_Documento_Identidad != $data['Nu_Documento_Identidad'] && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_documento_cabecera . " WHERE ID_Empresa=" . $where['ID_Empresa'] . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'La empresa tiene movimiento(s)');
        } else {
		    if ( $this->db->update($this->table, $data, $where) > 0 )
		        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
    }
    
	public function eliminarEmpresa($ID){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_documento_cabecera . " WHERE ID_Empresa=" . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'La empresa tiene movimiento(s)');
		} else {
			$this->db->where('ID_Empresa', $ID);
            $this->db->delete($this->table);
		    if ( $this->db->affected_rows() > 0 )
		        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al eliminar');
	}
    
	public function configuracionAutomaticaOpciones($arrPost){
	}
}
