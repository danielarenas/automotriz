<?php
class LoginModel extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function verificarAccesoLogin($data){
		$No_Password = trim($data['No_Password']);
		$No_Password = strip_tags($No_Password);
		
		$No_Usuario	= trim($data['No_Usuario']);
		$No_Usuario = strip_tags($No_Usuario);
		
		$No_Password = trim($data['No_Password']);
		$No_Password = strip_tags($No_Password);
						
		$this->db->where('No_Usuario', $No_Usuario)->or_where('Nu_Celular', $No_Usuario)->or_where('Txt_Email', $No_Usuario);
		$u = $this->db->get('usuario')->row();
		
		if(is_object($u)){
			if ($this->db->query("SELECT Nu_Estado FROM empresa WHERE ID_Empresa = $u->ID_Empresa LIMIT 1")->row()->Nu_Estado == 1) {
				$UNo_Password = $this->encryption->decrypt($u->No_Password);//Verificar contraseña
				if( $UNo_Password == $No_Password ){
					if($u->Nu_Estado == 1){// 1 = Activo
						$ID_Empresa = trim($data['ID_Empresa']);
						$ID_Empresa = strip_tags($ID_Empresa);
						
						$ID_Organizacion = trim($data['ID_Organizacion']);
						$ID_Organizacion = strip_tags($ID_Organizacion);
						
						if ( $ID_Empresa == '' && $ID_Organizacion == '' ) {
							$query = "SELECT COUNT(*) as cantidad FROM usuario WHERE (No_Usuario = '" . $No_Usuario . "' OR Nu_Celular = '" . $No_Usuario . "' OR Txt_Email = '" . $No_Usuario . "')";
							$row = $this->db->query($query)->row();
							$iCantidadAcessoUsuario = $row->cantidad;

							$sql = "
SELECT
 USR.*,
 GRP.ID_Organizacion,
 GRP.No_Grupo,
 GRPUSR.ID_Grupo_Usuario
FROM
 usuario AS USR
 JOIN grupo_usuario AS GRPUSR ON (USR.ID_Usuario = GRPUSR.ID_Usuario)
 JOIN grupo AS GRP ON (GRP.ID_Grupo = GRPUSR.ID_Grupo)
WHERE
 (USR.No_Usuario = '" . $No_Usuario . "' OR USR.Nu_Celular = '" . $No_Usuario . "' OR USR.Txt_Email = '" . $No_Usuario . "')
LIMIT 1;";
						$u = $this->db->query($sql)->row();
						
						// Protegemos la contraseña
						unset($No_Password);
						unset($UNo_Password);
						unset($u->No_Password);

						// Respondemos
						$this->session->set_userdata('usuario', $u);
						return array(
							'sStatus' => 'success',
							'sMessage' => 'Iniciando sesión',
							'iCantidadAcessoUsuario' => $iCantidadAcessoUsuario,
						);
					} else {
						$sql = "
SELECT
 USR.*,
 GRP.ID_Organizacion,
 GRP.No_Grupo,
 GRPUSR.ID_Grupo_Usuario
FROM
 usuario AS USR
 JOIN grupo_usuario AS GRPUSR ON (USR.ID_Usuario = GRPUSR.ID_Usuario)
 JOIN grupo AS GRP ON (GRP.ID_Grupo = GRPUSR.ID_Grupo)
WHERE
 (USR.No_Usuario = '" . $No_Usuario . "' OR USR.Nu_Celular = '" . $No_Usuario . "' OR USR.Txt_Email = '" . $No_Usuario . "')
 AND GRP.ID_Empresa = " . $ID_Empresa . "
 AND GRP.ID_Organizacion = " . $ID_Organizacion . "
LIMIT 1;";
							$u = $this->db->query($sql)->row();
							
							// Protegemos la contraseña
							unset($No_Password);
							unset($UNo_Password);
							unset($u->No_Password);

							// Respondemos
							$this->session->set_userdata('usuario', $u);
							return array(
								'sStatus' => 'success',
								'sMessage' => 'Iniciando sesión',
							);
						}// /. if - else verificar empresa y organizacion
					} else {
						return array(
							'sStatus' => 'warning',
							'sMessage' => 'Usuario suspendido, comuníquese con su administrador de sistemas',
						);//Usuario Suspendido
					}// /. if - else usuario suspendido
				} else {
					return array(
						'sStatus' => 'warning',
						'sMessage' => 'Contraseña incorrecta',
					);//Contraseña incorrecta
				} // /. if - else contraseña incorrecta
			} else {
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'comuníquese con su administrador de sistemas',
				);//Usuario no existe
			} // /. if - else usuario no existe
		} else {
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'No existe usuario',
			);//Usuario no existe
		} // /. if - else empresa desactivada
	}
	
	public function verificar_email($data){
		$Txt_Email = trim($data['Txt_Email']);
		$Txt_Email = strip_tags($Txt_Email);
		$result = array ('data' => NULL, 'status' => 'error', 'type' => 'invalido', 'message' => 'No existe correo');
		$query = $this->db->get_where('usuario', array('Txt_Email' => $Txt_Email, 'Nu_Estado' => 1));
        if ($query->num_rows() == 1) {//Existe usuario activo
			$result = array ('data' => NULL, 'status' => 'success_error_modificar', 'type' => 'activo', 'message' => 'Problemas al modificar usuario');
			if($this->startRecoveryPassword($query->row()->ID_Usuario, $data['Txt_Token_Activacion']))
        		$result = array ('data' => $query->row(), 'status' => 'success', 'type' => 'activo', 'message' => 'Usuario activo');
		} else if ($this->db->query("SELECT COUNT(*) AS existe FROM usuario WHERE Nu_Estado = 0 AND Txt_Email = '" . $Txt_Email . "' LIMIT 1")->row()->existe > 0)
			$result = array ('data' => NULL, 'status' => 'warning', 'type' => 'inactivo', 'message' => 'Usuario suspendido');
		return $result;
	}
	
	private function startRecoveryPassword($ID_Usuario, $Txt_Token_Activacion){
		$Fe_Vencimiento_Token = date('Y-m-d H:i:s', strtotime("+15 min"));//15 minutos al usuario para recuperar su password
		$data = array("Fe_Vencimiento_Token" => $Fe_Vencimiento_Token, "Txt_Token_Activacion" => $Txt_Token_Activacion);
		$this->db->where("ID_Usuario", $ID_Usuario);
		$status = FALSE;
		if($this->db->update("usuario", $data))
			$status = TRUE;
		return $status;
	}
	
	public function cambiar_clave($data){
		$No_Password = $data['No_Password'];
		$Txt_Token_Activacion = $data['Txt_Token_Activacion'];
		$result = array ('status' => 'error', 'type' => 'invalido', 'message' => 'Token inválido');
		if ($this->db->query("SELECT COUNT(*) AS existe FROM usuario WHERE Nu_Estado = 1 AND Txt_Token_Activacion = '" . $Txt_Token_Activacion . "' AND Fe_Vencimiento_Token > '" . dateNow('fecha_hora') . "' LIMIT 1")->row()->existe > 0){
			$result = array ('status' => 'warning', 'type' => 'token_no_expirado', 'message' => 'Problemas al cambiar contraseña');	
			if($this->changePassword($No_Password, $Txt_Token_Activacion))
				$result = array ('status' => 'success', 'type' => 'token_no_expirado', 'message' => 'Contraseña cambiada satisfactoriamente');	
		}
		return $result;
	}
	
	private function changePassword($No_Password, $Txt_Token_Activacion){
		$data = array("No_Password" => $No_Password);
		$this->db->where("Txt_Token_Activacion", $Txt_Token_Activacion);
		$status = FALSE;
		if($this->db->update("usuario", $data))
			$status = TRUE;
		return $status;
	}
}
