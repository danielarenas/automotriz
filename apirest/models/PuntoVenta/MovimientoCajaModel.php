<?php
class MovimientoCajaModel extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    
    public function addMovimientoCaja($arrPost){
        //Iniciamos la transacción:
        $this->db->trans_begin();
        
        $this->db->insert('caja_pos', $arrPost);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return array('sStatus' => 'danger', 'sMessage' => 'Problemas al registrar');
        } else {
            $this->db->trans_commit();
            return array('sStatus' => 'success', 'sMessage' => 'Registro guardado');
        }
	}
	
    public function getReporte(){
		$query = "
SELECT
 AC.Fe_Movimiento,
 TOC.No_Tipo_Operacion_Caja,
 MONE.No_Signo,
 AC.Ss_Total,
 AC.Txt_Nota,
 TOC.Nu_Tipo
FROM
 caja_pos AS AC
 JOIN tipo_operacion_caja AS TOC ON(TOC.ID_Tipo_Operacion_Caja = AC.ID_Tipo_Operacion_Caja)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = AC.ID_Moneda)
WHERE
 AC.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND AC.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND AC.ID_Matricula_Empleado = " . $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado . "
 AND AC.Fe_Movimiento >= '" . $this->session->userdata['arrDataPersonal']['arrData'][0]->Fe_Matricula . "'
 AND TOC.Nu_Tipo IN(5,6)
ORDER BY
 AC.Fe_Movimiento DESC,
 TOC.No_Tipo_Operacion_Caja,
 AC.ID_Moneda";
        
        if ( !$this->db->simple_query($query) ){
            $error = $this->db->error();
            return array(
                'sStatus' => 'danger',
                'sMessage' => 'Problemas al obtener datos',
                'sCodeSQL' => $error['code'],
                'sMessageSQL' => $error['message'],
            );
        }
        $arrResponseSQL = $this->db->query($query);
        if ( $arrResponseSQL->num_rows() > 0 ){
            return array(
                'sStatus' => 'success',
                'arrData' => $arrResponseSQL->result(),
            );
        }
        
        return array(
            'sStatus' => 'warning',
            'sMessage' => 'No se encontro registro',
        );
    }
}
