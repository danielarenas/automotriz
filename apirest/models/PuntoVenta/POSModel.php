<?php
class POSModel extends CI_Model{	
	public function __construct(){
		parent::__construct();
	}
	
	public function agregarVentaPos($arrPost){
		$query = "SELECT
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM
serie_documento
WHERE
ID_Empresa=" . $this->empresa->ID_Empresa . "
AND ID_Organizacion=" . $this->empresa->ID_Organizacion . "
AND ID_Almacen=" . $arrPost['arrCabecera']['iIdAlmacen'] . "
AND ID_Tipo_Documento=" . $arrPost['arrCabecera']['ID_Tipo_Documento'] . "
AND Nu_Estado=1
AND ID_POS=".$this->session->userdata['arrDataPersonal']['arrData'][0]->ID_POS."
LIMIT 1";
		$arrSerieDocumento = $this->db->query($query)->row();
		
		$sTidoDocumento = 'Doc. Interno';
		if ( $arrPost['arrCabecera']['ID_Tipo_Documento'] == '4' )
			$sTidoDocumento = 'Boleta';
		else if ( $arrPost['arrCabecera']['ID_Tipo_Documento'] == '3' )
			$sTidoDocumento = 'Factura';
		
		if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
			return array('sStatus' => 'danger', 'sMessage' => 'Deben configurar serie para ' . $sTidoDocumento . ', no existe');
		}

		if ($this->db->query("SELECT COUNT(*) AS existe FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND ID_Tipo_Documento = " . $arrPost['arrCabecera']['ID_Tipo_Documento'] . " AND ID_Serie_Documento = '" . $arrSerieDocumento->ID_Serie_Documento . "' AND ID_Numero_Documento = '" . $arrSerieDocumento->Nu_Numero_Documento . "' LIMIT 1")->row()->existe > 0){
			return array('sStatus' => 'warning', 'sMessage' => 'Ya existe venta ' . $sTidoDocumento . ' - ' . $arrSerieDocumento->ID_Serie_Documento . ' - ' . $arrSerieDocumento->Nu_Numero_Documento . ' modificar correlativo en la opción Ventas -> Series' );
		} else {
			$this->db->trans_begin();

			$Last_ID_Entidad = $arrPost['arrCabecera']['ID_Entidad'];
			// Cliente ya esta registrado en BD
			if ( $arrPost['arrCabecera']['iTipoCliente']=='3' && !empty($Last_ID_Entidad) ){
				$arrClienteBD = $this->db->query("SELECT Txt_Email_Entidad, Nu_Celular_Entidad FROM entidad WHERE ID_Entidad = " . $Last_ID_Entidad . " LIMIT 1")->result();
				$Nu_Celular_Entidad = '';
				if ( $arrPost['arrCliente']['Nu_Celular_Entidad'] && strlen($arrPost['arrCliente']['Nu_Celular_Entidad']) == 11){
					$Nu_Celular_Entidad = explode(' ', $arrPost['arrCliente']['Nu_Celular_Entidad']);
					$Nu_Celular_Entidad = $Nu_Celular_Entidad[0].$Nu_Celular_Entidad[1].$Nu_Celular_Entidad[2];
				}
				if ( $arrClienteBD[0]->Nu_Celular_Entidad != $Nu_Celular_Entidad || $arrClienteBD[0]->Txt_Email_Entidad != $arrPost['arrCliente']['Txt_Email_Entidad'] ) {
					$sql = "UPDATE entidad SET Nu_Celular_Entidad = '" . $Nu_Celular_Entidad . "', Txt_Email_Entidad = '" . $arrPost['arrCliente']['Txt_Email_Entidad'] . "' WHERE ID_Entidad = " . $Last_ID_Entidad;
					$this->db->query($sql);
				} // /. if cambiar celular o correo
			} // /. if cliente existe en BD

			if ($arrPost['arrCabecera']['iTipoCliente']=='1') {// Cliente existente < 700 IGV y es Boleta
				$query = "SELECT ID_Entidad FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND No_Entidad LIKE '%clientes varios%' LIMIT 1"; //1 = ID_Entidad -> Cliente varios
				if ( !$this->db->simple_query($query) ){
					$this->db->trans_rollback();
					$error = $this->db->error();
					return array(
						'sStatus' => 'danger',
						'sMessage' => 'Problemas al obtener datos de clientes varios',
						'sClassModal' => 'modal-danger',
						'sCodeSQL' => $error['code'],
						'sMessageSQL' => $error['message'],
					);
				}
				$arrResponseSQL = $this->db->query($query);
				if ( $arrResponseSQL->num_rows() > 0 ){
					$arrData = $arrResponseSQL->result();
					$Last_ID_Entidad = $arrData[0]->ID_Entidad;
				} else {
					$this->db->trans_rollback();
					return array(
						'sStatus' => 'warning',
						'sMessage' => 'No se encontro clientes varios',
						'sClassModal' => 'modal-warning',
					);
				}
			}// /. Cliente existente < 700 IGV y es Boleta

			if ( $arrPost['arrCabecera']['iTipoCliente']=='3' && empty($arrPost['arrCabecera']['ID_Entidad']) ) {//3=Cliente nuevo
				$query = "SELECT ID_Entidad FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrPost['arrCliente']['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrPost['arrCliente']['Nu_Documento_Identidad'] . "' AND No_Entidad = '" . $arrPost['arrCliente']['No_Entidad'] . "' LIMIT 1";
				$arrResponseSQL = $this->db->query($query);
				if ( $arrResponseSQL->num_rows() > 0 ){
					$arrData = $arrResponseSQL->result();
					$Last_ID_Entidad = $arrData[0]->ID_Entidad;
				} else {
					$Nu_Celular_Entidad = '';
					if ( $arrPost['arrCliente']['Nu_Celular_Entidad'] && strlen($arrPost['arrCliente']['Nu_Celular_Entidad']) == 11){
						$Nu_Celular_Entidad = explode(' ', $arrPost['arrCliente']['Nu_Celular_Entidad']);
						$Nu_Celular_Entidad = $Nu_Celular_Entidad[0].$Nu_Celular_Entidad[1].$Nu_Celular_Entidad[2];
					}
					$arrCliente = array(
						'ID_Empresa' => $this->empresa->ID_Empresa,
						'ID_Organizacion' => $this->empresa->ID_Organizacion,
						'Nu_Tipo_Entidad' => 0,//0=Cliente
						'ID_Tipo_Documento_Identidad' => $arrPost['arrCliente']['ID_Tipo_Documento_Identidad'],
						'Nu_Documento_Identidad' => $arrPost['arrCliente']['Nu_Documento_Identidad'],
						'No_Entidad' => $arrPost['arrCliente']['No_Entidad'],
						'Nu_Estado' => $arrPost['arrCliente']['Nu_Estado'],
						'Nu_Celular_Entidad' => $Nu_Celular_Entidad,
						'Txt_Email_Entidad'	=> $arrPost['arrCliente']['Txt_Email_Entidad'],
					);
					$this->db->insert('entidad', $arrCliente);
					$Last_ID_Entidad = $this->db->insert_id();
				}
			}// ./ if cliente nuevo

			//Generar venta
			$Nu_Correlativo = 0;
			$Fe_Year = dateNow('año');
			$Fe_Month = dateNow('mes');
			
			if ( $arrPost['arrCabecera']['ID_Tipo_Documento'] != '2' ) {
				// Obtener correlativo			
				if($this->db->query("SELECT COUNT(*) existe FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->existe > 0){
					$sql_correlativo_libro_sunat = "
UPDATE
 correlativo_tipo_asiento
SET
 Nu_Correlativo=Nu_Correlativo + 1
WHERE
 ID_Empresa=" . $this->empresa->ID_Empresa . "
 AND ID_Tipo_Asiento=1
 AND Fe_Year='" . $Fe_Year. "'
 AND Fe_Month='" . $Fe_Month . "'";
				$this->db->query($sql_correlativo_libro_sunat);
			} else {
				$sql_correlativo_libro_sunat = "
INSERT INTO correlativo_tipo_asiento (
 ID_Empresa,
 ID_Tipo_Asiento,
 Fe_Year,
 Fe_Month,
 Nu_Correlativo
) VALUES (
 " . $this->empresa->ID_Empresa . ",
 1,
 '" . $Fe_Year . "',
 '" . $Fe_Month . "',
 1
);";
					$this->db->query($sql_correlativo_libro_sunat);
				}
				$Nu_Correlativo = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->Nu_Correlativo;
				// /. Obtener correlativo
			}// if validacion correlativo documento interno
			
			$fTotalxMP = 0.00;
			$fVuelto = 0.00;
			foreach ($arrPost['arrFormaPago'] as $row) {
				$fTotalxMP += $row['Ss_Total'];
				if ( $fTotalxMP > $arrPost['arrCabecera']['Ss_Total'] )
					$fVuelto = $fTotalxMP - $arrPost['arrCabecera']['Ss_Total'];
				if ( $row['iTipoVista'] == '1' )//Credito
					$fVuelto = $row['Ss_Total'];
			}

			$arrVentaCabecera = array(
				'ID_Empresa'				=> $this->empresa->ID_Empresa,
				'ID_Organizacion'			=> $this->empresa->ID_Organizacion,
				'ID_Almacen'			    => $this->empresa->ID_Almacen,
				'ID_Entidad'				=> $Last_ID_Entidad,
				'ID_Matricula_Empleado'	    => $arrPost['arrCabecera']['ID_Matricula_Empleado'],
				'ID_Tipo_Asiento'			=> 1,//Venta
				'ID_Tipo_Documento'			=> $arrPost['arrCabecera']['ID_Tipo_Documento'],
				'ID_Serie_Documento_PK'		=> $arrSerieDocumento->ID_Serie_Documento_PK,
				'ID_Serie_Documento'		=> $arrSerieDocumento->ID_Serie_Documento,
				'ID_Numero_Documento'		=> $arrSerieDocumento->Nu_Numero_Documento,
				'Fe_Emision'				=> dateNow('fecha'),
				'Fe_Emision_Hora'			=> dateNow('fecha_hora'),
				'ID_Moneda'					=> $arrPost['arrCabecera']['ID_Moneda'],//Soles
				'ID_Medio_Pago'				=> $arrPost['arrFormaPago'][0]['ID_Medio_Pago'],
				'Fe_Vencimiento'			=> dateNow('fecha'),
				'Fe_Periodo' => dateNow('fecha'),
				'Nu_Descargar_Inventario' => 1,
				'Ss_Total' => $arrPost['arrCabecera']['Ss_Total'],
				'Ss_Total_Saldo' => $arrPost['arrCabecera']['Ss_Total_Saldo'],
				'Ss_Vuelto' => $fVuelto,
				'Nu_Correlativo' => $Nu_Correlativo,
				'Nu_Estado' => 6,//Completado
				'Nu_Transporte_Lavanderia_Hoy' => $arrPost['arrCabecera']['Nu_Transporte_Lavanderia_Hoy'],
				'Nu_Estado_Lavado' => ($arrPost['arrCabecera']['Nu_Transporte_Lavanderia_Hoy'] != 2 ? 1 : 11),
				'Fe_Entrega' => ToDate($arrPost['arrCabecera']['Fe_Entrega']),
				'Nu_Tipo_Recepcion' => $arrPost['arrCabecera']['Nu_Tipo_Recepcion'],
				'ID_Transporte_Delivery' => $arrPost['arrCabecera']['ID_Transporte_Delivery'],
				'Txt_Direccion_Delivery' => $arrPost['arrCabecera']['sDireccionDelivery'],
				'Txt_Glosa' => nl2br($arrPost['arrCabecera']['sGlosa']),
				'No_Formato_PDF' => 'TICKET',
			);
			
			if ( $_POST['arrCabecera']['ID_Lista_Precio_Cabecera'] != 0 )
				$arrVentaCabecera = array_merge($arrVentaCabecera, array("ID_Lista_Precio_Cabecera" => $this->security->xss_clean($_POST['arrCabecera']['ID_Lista_Precio_Cabecera'])));

			$this->db->insert('documento_cabecera', $arrVentaCabecera);
			$Last_ID_Documento_Cabecera = $this->db->insert_id();
			
			foreach($arrPost['arrDetalle'] as $row) {
				$documento_detalle[] = array(
					'ID_Empresa' => $this->empresa->ID_Empresa,
					'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
					'ID_Producto' => $row['ID_Producto'],
					'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio' => $row['Ss_Precio'],
					'Ss_SubTotal' => $row['fSubtotalItem'],
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['fDescuentoPorcentajeItem'],
					'Txt_Nota' => nl2br($row['Txt_Nota']),
					'ID_Impuesto_Cruce_Documento' => $row['ID_Impuesto_Cruce_Documento'],
					'Ss_Impuesto' => $row['fImpuestoItem'],
					'Ss_Total' => $row['Ss_Total_Producto'],
					'Nu_Estado_Lavado' => ($arrPost['arrCabecera']['Nu_Transporte_Lavanderia_Hoy'] != 2 ? 1 : 11),
				);
			}
			$this->db->insert_batch('documento_detalle', $documento_detalle);
			
			foreach($arrPost['arrFormaPago'] as $row) {
				$documento_medio_pago[] = array(
					'ID_Empresa' => $this->empresa->ID_Empresa,
					'ID_Documento_Cabecera'	=> $Last_ID_Documento_Cabecera,
					'ID_Medio_Pago'	=> $this->security->xss_clean($row['ID_Medio_Pago']),
					'Nu_Transaccion' => $this->security->xss_clean($row['Nu_Transaccion']),
					'Nu_Tarjeta' => $this->security->xss_clean($row['Nu_Tarjeta']),
					'Ss_Total' => $this->security->xss_clean($row['Ss_Total']),
					'ID_Tipo_Medio_Pago' => $this->security->xss_clean($row['ID_Tarjeta_Credito']),
				);
			}
			$this->db->insert_batch('documento_medio_pago', $documento_medio_pago);
		
			$sql = "UPDATE serie_documento SET Nu_Numero_Documento=Nu_Numero_Documento+1
WHERE
ID_Empresa=" . $this->empresa->ID_Empresa . "
AND ID_Organizacion=" . $this->empresa->ID_Organizacion . "
AND ID_Almacen=" . $arrPost['arrCabecera']['iIdAlmacen'] . "
AND ID_Tipo_Documento=" . $arrPost['arrCabecera']['ID_Tipo_Documento'] . "
AND ID_Serie_Documento='" . $arrSerieDocumento->ID_Serie_Documento . "'";
			$this->db->query($sql);

			$this->MovimientoInventarioModel->crudMovimientoInventario(1,$Last_ID_Documento_Cabecera,0,$arrPost['arrDetalle'],1,0,'',1,1);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('sStatus' => 'danger', 'sMessage' => 'Problemas al registrar venta');
			} else {
				$this->db->trans_commit();

				if ($arrPost['arrCabecera']['ID_Tipo_Documento'] != '2') {// 2 = Documento interno
					$arrParams = array(
						'iCodigoProveedorDocumentoElectronico' => 1,
						'iEstadoVenta' => 6,//6=Completado
						'iIdDocumentoCabecera' =>  $Last_ID_Documento_Cabecera,
						'sEmailCliente' => ( !isset($arrPost['arrCliente']['Txt_Email_Entidad']) ? '' : $arrPost['arrCliente']['Txt_Email_Entidad'] ),
						'sTipoRespuesta' => 'php',
					);
					$arrResponseFE = array();
					if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 && $this->empresa->Nu_Estado_Pago_Sistema == 1 ) {//Nubefact
						$arrResponseFE = $this->DocumentoElectronicoModel->generarFormatoDocumentoElectronico( $arrParams );
						$arrResponseFEMensaje = $this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE, $arrParams );
						if ( $arrResponseFEMensaje['sStatus'] != 'success' ) {
							return $arrResponseFEMensaje;
						}
						if ( $arrResponseFE['sStatus'] != 'success' ) {
							return $arrResponseFE;
						}
					} else if ( $this->empresa->Nu_Tipo_Proveedor_FE == 2 && $this->empresa->Nu_Estado_Pago_Sistema == 1 ) {//Facturador sunat
						$arrResponseFE = $this->DocumentoElectronicoModel->generarFormatoDocumentoElectronicoSunat( $arrParams );
						$arrResponseFEMensaje = $this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE, $arrParams );
						if ( $arrResponseFEMensaje['sStatus'] != 'success' ) {
							return $arrResponseFEMensaje;
						}
						if ( $arrResponseFE['sStatus'] != 'success' ) {
							return $arrResponseFE;
						}
					}
					if ( $this->empresa->Nu_Estado_Pago_Sistema == 1 ) {
						return array(
							'sStatus' => 'success',
							'sMessage' => 'Venta completada',
							'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
							'arrResponseFE' => $arrResponseFE,
						);
					} else {
						return array(
							'sStatus' => 'success',
							'sMessage' => 'Venta completada (Documento no enviado a SUNAT)',
							'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
							'arrResponseFE' => '',
						);
					}
				}// /. if - 2 = documento interno

				return array(
					'sStatus' => 'success',
					'sMessage' => 'Venta completada',
					'iIdDocumentoCabecera' => $Last_ID_Documento_Cabecera,
					'arrResponseFE' => '',
				);
			}
		}// if - else validacion si existe comprobante
	}
}
