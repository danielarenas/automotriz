<?php
class AperturaCajaModel extends CI_Model{	
	public function __construct(){
		parent::__construct();
	}
	
	/*
	 Funcion matricular personal y aperturar caja
	 MP = Matricula Personal
	 AC = Apertura de Caja
	*/
    public function agregarMPyAC($arrData){
		$iExisteMatriculaPersonal = 0;
		$query="
SELECT
 Fe_Matricula
FROM
 matricula_empleado
WHERE
 ID_Empresa=" . $this->user->ID_Empresa . "
 AND ID_Organizacion=" . $this->empresa->ID_Organizacion . "
 AND ID_POS=" . $arrData['ID_POS'] . "
ORDER BY
 Fe_Matricula DESC
LIMIT 1";
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			$dMatricula=$this->db->query($query)->row()->Fe_Matricula;

			$query="
SELECT
 COUNT(*) AS existe
FROM
 caja_pos
WHERE
 ID_Empresa=" . $this->user->ID_Empresa . "
 AND ID_Organizacion=" . $this->empresa->ID_Organizacion . "
 AND ID_POS=" . $arrData['ID_POS'] . "
 AND Fe_Movimiento>='" . $dMatricula . "'
 AND ID_Tipo_Operacion_Caja=1
 AND Nu_Estado=0
LIMIT 1";
			$bStatusMatriculaPersonal=true;
			$iExisteMatriculaPersonal= $this->db->query($query)->row()->existe;
		}// /. if validacion de matricula personal
		
		if( $iExisteMatriculaPersonal > 0 ){
			return array('sStatus' => 'warning', 'sMessage' => 'La caja se encuentra aperturada' );
		}else{
			if ( empty($arrData['ID_Tipo_Operacion_Caja']) || $this->db->query("SELECT COUNT(*) AS existe FROM tipo_operacion_caja WHERE ID_Tipo_Operacion_Caja = " . $arrData['ID_Tipo_Operacion_Caja'] . " LIMIT 1")->row()->existe == 0){
				return array('sStatus' => 'warning', 'sMessage' => 'No existe el tipo de operación Apertura de caja' );
			} else {
				//Iniciamos la transacción:
				$this->db->trans_begin();
				
				$dFechaHoraActual=dateNow('fecha_hora');//Fecha matricula y movimiento de apertura de caja

				$arrDataMP = array(
					'ID_Empresa' => $this->empresa->ID_Empresa,
					'ID_Organizacion' => $this->empresa->ID_Organizacion,
					'ID_Almacen' => $this->empresa->ID_Almacen,
					'ID_Entidad' => $arrData['ID_Entidad'],
					'ID_POS' => $arrData['ID_POS'],
					'Fe_Matricula' => $dFechaHoraActual,
				);
				$this->db->insert('matricula_empleado', $arrDataMP);
				
				$ID_Matricula_Empleado = $this->db->insert_id();

				$arrDataAC = array(
					'ID_Empresa' => $this->empresa->ID_Empresa,
					'ID_Organizacion' => $this->empresa->ID_Organizacion,
					'ID_Almacen' => $this->empresa->ID_Almacen,
					'ID_Matricula_Empleado' => $ID_Matricula_Empleado,
					'ID_POS' => $arrData['ID_POS'],
					'Fe_Movimiento' => $dFechaHoraActual,
					'ID_Tipo_Operacion_Caja' => $arrData['ID_Tipo_Operacion_Caja'],
					'ID_Moneda' => $arrData['ID_Moneda'],
					'Ss_Total' => $arrData['Ss_Total'],
					'Txt_Nota' => $arrData['Txt_Nota'],
					'Nu_Estado' => 0,//Abierto
					'ID_Enlace_Apertura_Caja_Pos' => 0,
				);
				$this->db->insert('caja_pos', $arrDataAC);

				//Todo lo que se haga a partir de aquí puede guardarse o cancelarse por completo.
				//Después de una serie de procesos, verificamos si hubo algún error:
				if ($this->db->trans_status() === FALSE) {
					//Si la transacción falló por algún motivo, volvemos todo atrás con rollback:
					$this->db->trans_rollback();
					return array('sStatus' => 'danger', 'sMessage' => 'Problemas al registrar');
				} else {
					$this->db->trans_commit();
					return array('sStatus' => 'success', 'sMessage' => 'Registro guardado', 'ID_Matricula_Empleado' => $ID_Matricula_Empleado);
				}
			}// if - else validacion de existe tipo de operacion de caja
		}
	}
}
