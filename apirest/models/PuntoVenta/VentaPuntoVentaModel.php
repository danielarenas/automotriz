<?php
class VentaPuntoVentaModel extends CI_Model{
	  public function __construct(){
		  parent::__construct();
	  }
	
    public function getReporte($arrParams){
        $iTipoConsultaFecha=$arrParams['iTipoConsultaFecha'];
        $Fe_Inicio=$arrParams['Fe_Inicio'];
        $Fe_Fin=$arrParams['Fe_Fin'];
        $ID_Tipo_Documento=$arrParams['ID_Tipo_Documento'];
        $ID_Serie_Documento=$arrParams['ID_Serie_Documento'];
        $ID_Numero_Documento=$arrParams['ID_Numero_Documento'];
        $Nu_Estado_Documento=$arrParams['Nu_Estado_Documento'];
        $iIdCliente=$arrParams['iIdCliente'];
        $sNombreCliente=$arrParams['sNombreCliente'];
        $iTipoRecepcionCliente=$arrParams['iTipoRecepcionCliente'];

        $cond_tipo = $ID_Tipo_Documento != "0" ? 'AND VC.ID_Tipo_Documento = ' . $ID_Tipo_Documento : 'AND VC.ID_Tipo_Documento IN(2,3,4,5,6)';
        $cond_serie = $ID_Serie_Documento != "0" ? "AND VC.ID_Serie_Documento = '" . $ID_Serie_Documento . "'" : "";
        $cond_numero = $ID_Numero_Documento != "-" ? "AND VC.ID_Numero_Documento = '" . $ID_Numero_Documento . "'" : "";
        $cond_estado_documento = $Nu_Estado_Documento != "0" ? 'AND VC.Nu_Estado = ' . $Nu_Estado_Documento : "";
        $cond_cliente = ( $iIdCliente != '-' && $sNombreCliente != '-' ) ? 'AND CLI.ID_Entidad = ' . $iIdCliente : "";
        $cond_tipo_recepcion_cliente = $iTipoRecepcionCliente != "0" ? 'AND VC.Nu_Tipo_Recepcion = ' . $iTipoRecepcionCliente : "";
        
        $cond_fecha_matricula_empleado = "AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'";
        if ( $iTipoConsultaFecha=='0' ) {//0=Actual
            $cond_fecha_matricula_empleado = "
AND VC.ID_Matricula_Empleado=" . $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado . "
AND VC.Fe_Emision_Hora>='" . $this->session->userdata['arrDataPersonal']['arrData'][0]->Fe_Matricula . "'";
        }

        $query = "
SELECT
 TD.No_Tipo_Documento_Breve,
 EMPLE.No_Entidad AS No_Empleado,
 VC.ID_Documento_Cabecera,
 VC.ID_Tipo_Documento,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento,
 VC.Fe_Emision_Hora,
 MONE.ID_Moneda,
 MONE.No_Signo,
 MONE.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Moneda,
 CLI.No_Entidad,
 TC.Ss_Compra_Oficial AS Ss_Tipo_Cambio,
 VE.Ss_Tipo_Cambio_Modificar,
 VC.Ss_Total,
 VC.Ss_Total_Saldo,
 TDESTADO.No_Descripcion AS No_Estado,
 TDESTADO.No_Class AS No_Class_Estado,
 VC.Nu_Estado,
 VC.Nu_Estado_Lavado,
 VC.Nu_Estado_Lavado_Recepcion_Cliente,
 VMP.ID_Documento_Medio_Pago
FROM
 documento_cabecera AS VC
 JOIN documento_medio_pago AS VMP ON(VMP.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)
 JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
 JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = VC.ID_Tipo_Documento)
 JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
 JOIN matricula_empleado AS MEMPLE ON(VC.ID_Matricula_Empleado = MEMPLE.ID_Matricula_Empleado)
 JOIN entidad AS EMPLE ON(MEMPLE.ID_Entidad = EMPLE.ID_Entidad)
 JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
 LEFT JOIN tasa_cambio AS TC ON(VC.ID_Empresa = TC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND VC.Fe_Emision = TC.Fe_Ingreso)
 LEFT JOIN (
  SELECT
   VE.ID_Documento_Cabecera,
   TC.Ss_Venta_Oficial AS Ss_Tipo_Cambio_Modificar
  FROM
   documento_cabecera AS VC
   JOIN documento_enlace AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera_Enlace)
   JOIN tasa_cambio AS TC ON(TC.ID_Empresa = VC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND TC.Fe_Ingreso = VC.Fe_Emision)
 ) AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera)
WHERE
 VC.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND VC.ID_Tipo_Asiento = 1
 AND SD.ID_POS > 0
 AND VMP.ID_Documento_Medio_Pago_Enlace IS NULL
 " . $cond_fecha_matricula_empleado . "
 " . $cond_tipo . "
 " . $cond_serie . "
 " . $cond_numero . "
 " . $cond_estado_documento . "
 " . $cond_cliente . "
 " . $cond_tipo_recepcion_cliente . "
ORDER BY
 VC.Fe_Emision_Hora DESC,
 VC.ID_Tipo_Documento DESC,
 VC.ID_Serie_Documento DESC,
 CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC;";
 
        if ( !$this->db->simple_query($query) ){
            $error = $this->db->error();
            return array(
                'sStatus' => 'danger',
                'sMessage' => 'Problemas al obtener datos',
                'sCodeSQL' => $error['code'],
                'sMessageSQL' => $error['message'],
            );
        }
        $arrResponseSQL = $this->db->query($query);
        if ( $arrResponseSQL->num_rows() > 0 ){
            return array(
                'sStatus' => 'success',
                'arrData' => $arrResponseSQL->result(),
            );
        }
        
        return array(
            'sStatus' => 'warning',
            'sMessage' => 'No se encontráron registro',
        );
    }

	public function entregarPedidoLavado($arrPost){
        $this->db->trans_begin();

        $data = array( 'Nu_Estado_Lavado_Recepcion_Cliente' => $arrPost['iEstadoLavadoRecepcionCliente'] );
        if ( isset($arrPost['iCrearEntidad']) && $arrPost['iCrearEntidad'] == 0 ) {// 0 Crear entidad recepcion lavado cliente = 9
            $arrEntidadLavado = array(
                'ID_Empresa' => $this->empresa->ID_Empresa,
                'ID_Organizacion' => $this->empresa->ID_Organizacion,
                'Nu_Tipo_Entidad' => 9,
                'ID_Tipo_Documento_Identidad' => 1,
                'No_Entidad' => $arrPost['sNombreRecepcion'],
            );
            $this->db->insert('entidad', $arrEntidadLavado);
            $iIdEntidad = $this->db->insert_id();
            $data = array( 'Nu_Estado_Lavado_Recepcion_Cliente' => $arrPost['iEstadoLavadoRecepcionCliente'], 'ID_Mesero' => $iIdEntidad );
        }

        if ( !empty($arrPost['fPagoCliente']) ) {
            $documento_medio_pago = array(
                'ID_Empresa'			=> $this->empresa->ID_Empresa,
                'ID_Documento_Cabecera'	=> $arrPost['iIdDocumentoCabecera'],
                'ID_Medio_Pago'		    => $arrPost['iFormaPago'],
                'Nu_Transaccion'		=> $arrPost['iNumeroTransaccion'],
                'Nu_Tarjeta'		    => $arrPost['iNumeroTarjeta'],
                'Ss_Total'		        => $arrPost['fPagoCliente'],
                'ID_Tipo_Medio_Pago' => isset($arrPost['iTipoMedioPago']) ? $arrPost['iTipoMedioPago'] : 0,
				'Fe_Emision_Hora_Pago' => dateNow('fecha_hora'),
				'ID_Matricula_Empleado' => $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado,
				'ID_Documento_Medio_Pago_Enlace' => $arrPost['iIdDocumentoMedioPago'],
            );
            $this->db->insert('documento_medio_pago', $documento_medio_pago);
            $data = array_merge( $data, array( 'Ss_Total_Saldo' => $arrPost['fSaldoCliente'] - $arrPost['fPagoCliente'] ) );
        }
        
        $where = array( 'ID_Documento_Cabecera' => $arrPost['iIdDocumentoCabecera'] );
        $this->db->update('documento_cabecera', $data, $where);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Problemas al cambiar procesar');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Procesado');
        }
    }
    
	public function facturarOrdenLavanderia($arrPost){
		$query = "
SELECT
 ID_Serie_Documento_PK,
 ID_Serie_Documento,
 Nu_Numero_Documento
FROM
 serie_documento
WHERE
 ID_Empresa=" . $this->empresa->ID_Empresa . "
 AND ID_Organizacion=" . $this->empresa->ID_Organizacion . "
 AND ID_Tipo_Documento=" . $arrPost['tipo_documento'] . "
 AND Nu_Estado=1
 AND ID_POS=".$this->session->userdata['arrDataPersonal']['arrData'][0]->ID_POS."
LIMIT 1";
		$arrSerieDocumento = $this->db->query($query)->row();
		
		if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
			$sTidoDocumento = 'Boleta';
			if ( $arrPost['tipo_documento'] == '3' )
				$sTidoDocumento = 'Factura';
			return array('sStatus' => 'danger', 'sMessage' => 'Deben configurar serie para ' . $sTidoDocumento . ', no existe');
		}

		if ($this->db->query("SELECT COUNT(*) AS existe FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND ID_Tipo_Documento = " . $arrPost['tipo_documento'] . " AND ID_Serie_Documento = '" . $arrSerieDocumento->ID_Serie_Documento . "' AND ID_Numero_Documento = '" . $arrSerieDocumento->Nu_Numero_Documento . "' LIMIT 1")->row()->existe > 0){
			return array('sStatus' => 'warning', 'sMessage' => 'Ya existe venta ' . $sTidoDocumento . ' ' . $arrSerieDocumento->ID_Serie_Documento . ' ' . $arrSerieDocumento->Nu_Numero_Documento );
		} else {
			$this->db->trans_begin();

            $arrCabecera = $this->db->query("SELECT * FROM documento_cabecera WHERE ID_Documento_Cabecera = " . $arrPost['iIdDocumentoCabecera'] . " LIMIT 1")->row();
            $arrDetalle = $this->db->query("SELECT * FROM documento_detalle WHERE ID_Documento_Cabecera = " . $arrPost['iIdDocumentoCabecera'] . " LIMIT 1")->result();
            $arrFormaPago = $this->db->query("SELECT * FROM documento_medio_pago WHERE ID_Documento_Cabecera = " . $arrPost['iIdDocumentoCabecera'] . " LIMIT 1")->result();
    
			$Last_ID_Entidad = $arrPost['AID'];
			// Cliente ya esta registrado en BD
			if ( !empty($Last_ID_Entidad) ){
				$arrClienteBD = $this->db->query("SELECT Txt_Email_Entidad, Nu_Celular_Entidad FROM entidad WHERE ID_Entidad = " . $Last_ID_Entidad . " LIMIT 1")->result();
				if ( $arrClienteBD[0]->Txt_Email_Entidad != $arrPost['Txt_Email_Entidad'] ) {
					$sql = "UPDATE entidad SET Txt_Email_Entidad = '" . $arrPost['Txt_Email_Entidad'] . "' WHERE ID_Entidad = " . $Last_ID_Entidad;
					$this->db->query($sql);
				} // /. if cambiar celular o correo
			} // /. if cliente existe en BD

			if ( empty($arrPost['AID']) ) {//3=Cliente nuevo
				$query = "SELECT ID_Entidad FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrPost['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrPost['Nu_Documento_Identidad'] . "' AND No_Entidad = '" . $arrPost['No_Entidad'] . "' LIMIT 1";
				$arrResponseSQL = $this->db->query($query);
				if ( $arrResponseSQL->num_rows() > 0 ){
					$arrData = $arrResponseSQL->result();
					$Last_ID_Entidad = $arrData[0]->ID_Entidad;
				} else {
					$arrCliente = array(
						'ID_Empresa' => $this->empresa->ID_Empresa,
						'ID_Organizacion' => $this->empresa->ID_Organizacion,
						'Nu_Tipo_Entidad' => 0,//0=Cliente
						'ID_Tipo_Documento_Identidad' => $arrPost['ID_Tipo_Documento_Identidad'],
						'Nu_Documento_Identidad' => $arrPost['Nu_Documento_Identidad'],
						'No_Entidad' => $arrPost['No_Entidad'],
						'Nu_Estado' => $arrPost['Nu_Estado_Entidad'],
						'Txt_Email_Entidad'	=> $arrPost['Txt_Email_Entidad'],
					);
					$this->db->insert('entidad', $arrCliente);
					$Last_ID_Entidad = $this->db->insert_id();
				}
            }// ./ if cliente nuevo
            
			//Generar venta
			$Nu_Correlativo = 0;
			$Fe_Year = dateNow('año');
			$Fe_Month = dateNow('mes');
			
            // Obtener correlativo
			if($this->db->query("SELECT COUNT(*) existe FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->existe > 0){
				$sql_correlativo_libro_sunat = "
UPDATE
 correlativo_tipo_asiento
SET
 Nu_Correlativo=Nu_Correlativo + 1
WHERE
 ID_Empresa=" . $arrCabecera->ID_Empresa . "
 AND ID_Tipo_Asiento=1
 AND Fe_Year='" . $Fe_Year. "'
 AND Fe_Month='" . $Fe_Month . "'";
				$this->db->query($sql_correlativo_libro_sunat);
			} else {
				$sql_correlativo_libro_sunat = "
INSERT INTO correlativo_tipo_asiento (
 ID_Empresa,
 ID_Tipo_Asiento,
 Fe_Year,
 Fe_Month,
 Nu_Correlativo
) VALUES (
 " . $arrCabecera->ID_Empresa . ",
 1,
 '" . $Fe_Year . "',
 '" . $Fe_Month . "',
 1
);";
				$this->db->query($sql_correlativo_libro_sunat);
			}
			$Nu_Correlativo = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->Nu_Correlativo;
			// /. Obtener correlativo
			
			$arrVentaCabecera = array(
				'ID_Empresa'				=> $arrCabecera->ID_Empresa,
				'ID_Organizacion'			=> $arrCabecera->ID_Organizacion,
				'ID_Almacen'			    => $arrCabecera->ID_Almacen,
				'ID_Entidad'				=> $Last_ID_Entidad,
				'ID_Matricula_Empleado'	    => $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado,
				'ID_Tipo_Asiento'			=> 1,//Venta
				'ID_Tipo_Documento'			=> $arrPost['tipo_documento'],
				'ID_Serie_Documento_PK'		=> $arrSerieDocumento->ID_Serie_Documento_PK,
				'ID_Serie_Documento'		=> $arrSerieDocumento->ID_Serie_Documento,
				'ID_Numero_Documento'		=> $arrSerieDocumento->Nu_Numero_Documento,
				'Fe_Emision'				=> dateNow('fecha'),
				'Fe_Emision_Hora'			=> dateNow('fecha_hora'),
				'ID_Moneda'					=> $arrCabecera->ID_Moneda,//Soles
				'ID_Medio_Pago'				=> $arrCabecera->ID_Medio_Pago,
				'Fe_Vencimiento'			=> dateNow('fecha'),
				'Fe_Periodo' => dateNow('fecha'),
				'Nu_Descargar_Inventario' => 1,
				'Ss_Total' => $arrCabecera->Ss_Total,
				'Ss_Total_Saldo' => $arrCabecera->Ss_Total_Saldo,
				'Ss_Vuelto' => $arrCabecera->Ss_Vuelto,
				'Nu_Correlativo' => $Nu_Correlativo,
				'Nu_Estado' => 6,//Completado
				'Nu_Transporte_Lavanderia_Hoy' => $arrCabecera->Nu_Transporte_Lavanderia_Hoy,
				'Nu_Estado_Lavado' => ($arrCabecera->Nu_Transporte_Lavanderia_Hoy != 2 ? 1 : 11),
				'Fe_Entrega' => $arrCabecera->Fe_Entrega,
				'Nu_Tipo_Recepcion' => $arrCabecera->Nu_Tipo_Recepcion,
				'ID_Transporte_Delivery' => $arrCabecera->ID_Transporte_Delivery,
				'Txt_Direccion_Delivery' => $arrCabecera->Txt_Direccion_Delivery,
				'Txt_Glosa' => $arrCabecera->Txt_Glosa,
                'No_Formato_PDF' => 'TICKET',
				'ID_Lista_Precio_Cabecera' => $arrCabecera->ID_Lista_Precio_Cabecera,
				'Nu_Estado_Lavado_Recepcion_Cliente' => $arrPost['Nu_Estado_Lavado_Recepcion_Cliente'],
			);
			
			$this->db->insert('documento_cabecera', $arrVentaCabecera);
			$Last_ID_Documento_Cabecera = $this->db->insert_id();
			
			foreach($arrDetalle as $row) {
				$documento_detalle[] = array(
					'ID_Empresa' => $row->ID_Empresa,
					'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
					'ID_Producto' => $row->ID_Producto,
					'Qt_Producto' => $row->Qt_Producto,
					'Ss_Precio' => $row->Ss_Precio,
					'Ss_SubTotal' => $row->Ss_SubTotal,
					'Ss_Descuento' => $row->Ss_Descuento,
					'Ss_Descuento_Impuesto' => $row->Ss_Descuento_Impuesto,
					'Po_Descuento' => $row->Po_Descuento,
					'Txt_Nota' => $row->Txt_Nota,
					'ID_Impuesto_Cruce_Documento' => $row->ID_Impuesto_Cruce_Documento,
					'Ss_Impuesto' => $row->Ss_Impuesto,
					'Ss_Total' => $row->Ss_Total,
					'Nu_Estado_Lavado' => ($arrCabecera->Nu_Transporte_Lavanderia_Hoy != 2 ? 1 : 11),
				);
			}
			$this->db->insert_batch('documento_detalle', $documento_detalle);
			
			foreach($arrFormaPago as $row) {
				$documento_medio_pago[] = array(
					'ID_Empresa' => $row->ID_Empresa,
					'ID_Documento_Cabecera'	=> $Last_ID_Documento_Cabecera,
					'ID_Medio_Pago'	=> $row->ID_Medio_Pago,
					'Nu_Transaccion' => $row->Nu_Transaccion,
					'Nu_Tarjeta' => $row->Nu_Tarjeta,
					'Ss_Total' => $row->Ss_Total,
					'ID_Tipo_Medio_Pago' => $row->ID_Tipo_Medio_Pago,
				);
			}
			$this->db->insert_batch('documento_medio_pago', $documento_medio_pago);
		
			$sql = "
UPDATE
serie_documento
SET
Nu_Numero_Documento=Nu_Numero_Documento+1
WHERE
ID_Empresa=" . $this->empresa->ID_Empresa . "
AND ID_Organizacion=" . $this->empresa->ID_Organizacion . "
AND ID_Tipo_Documento=" . $arrPost['tipo_documento'] . "
AND ID_Serie_Documento='" . $arrSerieDocumento->ID_Serie_Documento . "'";
            $this->db->query($sql);
            
            $arrParamsDelete = array(
                'iIdDocumentoCabecera' => $arrPost['iIdDocumentoCabecera'],
            );
            $this->eliminarVenta($arrParamsDelete);

			$this->MovimientoInventarioModel->crudMovimientoInventario(1,$Last_ID_Documento_Cabecera,0,$documento_detalle,1,0,'',1,1);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('sStatus' => 'danger', 'sMessage' => 'Problemas al registrar venta');
			} else {
				$this->db->trans_commit();

				$arrParams = array(
					'iCodigoProveedorDocumentoElectronico' => 1,
					'iEstadoVenta' => 6,//6=Completado
					'iIdDocumentoCabecera' =>  $Last_ID_Documento_Cabecera,
					'sEmailCliente' => ( !isset($arrPost['Txt_Email_Entidad']) ? '' : $arrPost['Txt_Email_Entidad'] ),
					'sTipoRespuesta' => 'php',
				);
				if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 && $this->empresa->Nu_Estado_Pago_Sistema == 1 ) {//Nubefact
					$arrResponseFE = $this->DocumentoElectronicoModel->generarFormatoDocumentoElectronico( $arrParams );
					$arrResponseFEMensaje = $this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE, $arrParams );
					if ( $arrResponseFEMensaje['sStatus'] != 'success' ) {
						return $arrResponseFEMensaje;
					}
					if ( $arrResponseFE['sStatus'] != 'success' ) {
						return $arrResponseFE;
					}
				} else if ( $this->empresa->Nu_Tipo_Proveedor_FE == 2 && $this->empresa->Nu_Estado_Pago_Sistema == 1 ) {//Facturador sunat
					$arrResponseFE = $this->DocumentoElectronicoModel->generarFormatoDocumentoElectronicoSunat( $arrParams );
					$arrResponseFEMensaje = $this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE, $arrParams );
					if ( $arrResponseFEMensaje['sStatus'] != 'success' ) {
						return $arrResponseFEMensaje;
					}
					if ( $arrResponseFE['sStatus'] != 'success' ) {
						return $arrResponseFE;
					}
				}
				
				if ( $this->empresa->Nu_Estado_Pago_Sistema == 1 ) {
					return array(
						'sStatus' => 'success',
						'sMessage' => 'Venta completada',
						'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
						'arrResponseFE' => $arrResponseFE,
					);
				} else {
					return array(
						'sStatus' => 'success',
						'sMessage' => 'Venta completada (Documento no enviado a SUNAT)',
						'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
						'arrResponseFE' => '',
					);
				}
			}
        }// if - else validacion si existe comprobante
    }
    
	public function eliminarVenta($arrParamsDelete){		
		$this->db->where('ID_Documento_Cabecera', $arrParamsDelete['iIdDocumentoCabecera']);
		$this->db->delete('documento_detalle');
		
		$this->db->where('ID_Documento_Cabecera', $arrParamsDelete['iIdDocumentoCabecera']);
        $this->db->delete('documento_medio_pago');
        
        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera=".$arrParamsDelete['iIdDocumentoCabecera'];
        $arrDetalle = $this->db->query($query)->result();
        foreach ($arrDetalle as $row) {
            if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa=" . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen=" . $row->ID_Almacen . " AND ID_Producto=" . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
                $where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
                $Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa=" . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen=" . $row->ID_Almacen . " AND ID_Producto=" . $row->ID_Producto)->row()->Qt_Producto;
                
                $stock_producto = array(
                    'ID_Producto'		=> $row->ID_Producto,
                    'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
                    'Ss_Costo_Promedio'	=> 0.00,
                );
                $this->db->update('stock_producto', $stock_producto, $where);
            }
        }
        
        $this->db->where('ID_Documento_Cabecera', $arrParamsDelete['iIdDocumentoCabecera']);
        $this->db->delete('movimiento_inventario');
        
		$this->db->where('ID_Documento_Cabecera', $arrParamsDelete['iIdDocumentoCabecera']);
        $this->db->delete('documento_cabecera');
	}
}
