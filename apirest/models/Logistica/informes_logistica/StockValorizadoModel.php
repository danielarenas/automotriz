<?php
class StockValorizadoModel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}
	
    public function getStockValorizado($ID_Empresa, $ID_Almacen, $iTipoStock, $ID_Linea, $ID_Producto){
        $cond_stock = $iTipoStock > 0 ? '' : 'AND STOCKPRO.Qt_Producto > 0';
        $cond_linea = $ID_Linea > 0 ? 'AND L.ID_Linea = ' . $ID_Linea : '';
        $cond_producto = $ID_Producto > 0 ? 'AND PRO.ID_Producto = ' . $ID_Producto : '';
        
        $query = "
        SELECT
            L.ID_Linea,
            L.No_Linea,
            STOCKPRO.ID_Producto,
            PRO.Nu_Codigo_Barra,
            PRO.No_Producto,
            UM.No_Unidad_Medida,
            STOCKPRO.Qt_Producto
        FROM
            stock_producto AS STOCKPRO
            JOIN producto AS PRO
                ON (STOCKPRO.ID_Producto = PRO.ID_Producto)
            JOIN unidad_medida AS UM
                ON(UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
            LEFT JOIN linea AS L
                ON (L.ID_Linea = PRO.ID_Linea)
        WHERE
            STOCKPRO.ID_Empresa = " . $ID_Empresa . "
            AND STOCKPRO.ID_Almacen = " . $ID_Almacen . "
            " . $cond_stock . "
            " . $cond_linea . "
            " . $cond_producto ."
        ORDER BY
            L.ID_Linea,
            PRO.No_Producto
        ";
        return $this->db->query($query)->result();
    }
    
    public function getStockValorizadoxFecha($ID_Empresa, $ID_Almacen, $Fe_Inicio, $Fe_Fin, $ID_Linea, $ID_Producto){
        $cond_linea = $ID_Linea > 0 ? 'AND L.ID_Linea = ' . $ID_Linea : '';
        $cond_producto = $ID_Producto > 0 ? 'AND PRO.ID_Producto = ' . $ID_Producto : '';
        
        $query = "
        (SELECT
            L.ID_Linea,
            L.No_Linea,
            GD.ID_Producto,
            PRO.Nu_Codigo_Barra,
            PRO.No_Producto,
            UM.No_Unidad_Medida
        FROM
            guia_cabecera AS GC
            JOIN guia_detalle AS GD
                ON (GC.ID_Guia_Cabecera = GD.ID_Guia_Cabecera)
            JOIN producto AS PRO
                ON (GD.ID_Producto = PRO.ID_Producto)
            JOIN unidad_medida AS UM
                ON(UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
            LEFT JOIN linea AS L
                ON (L.ID_Linea = PRO.ID_Linea)
        WHERE
            GC.ID_Empresa = " . $ID_Empresa . "
            AND GC.ID_Almacen = " . $ID_Almacen . "
            AND GC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
            " . $cond_linea . "
            " . $cond_producto . "
        GROUP BY
            GD.ID_Producto
        ) UNION (
        SELECT
            L.ID_Linea,
            L.No_Linea,
            CD.ID_Producto,
            PRO.Nu_Codigo_Barra,
            PRO.No_Producto,
            UM.No_Unidad_Medida
        FROM
            documento_cabecera AS CC
            JOIN documento_detalle AS CD
                ON (CC.ID_Documento_Cabecera = CD.ID_Documento_Cabecera)
            JOIN producto AS PRO
                ON (CD.ID_Producto = PRO.ID_Producto)
            JOIN unidad_medida AS UM
                ON(UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
            LEFT JOIN linea AS L
                ON (L.ID_Linea = PRO.ID_Linea)
        WHERE
            CC.ID_Empresa = " . $ID_Empresa . "
            AND CC.ID_Almacen = " . $ID_Almacen . "
            AND CC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
            " . $cond_linea . "
            " . $cond_producto . "
        GROUP BY
            CD.ID_Producto)
        ORDER BY
            ID_Linea,
            No_Producto
        ";
        return $this->db->query($query)->result();
    }
    
    public function getStockValorizadoxProducto($ID_Empresa, $ID_Almacen, $Fe_Inicio, $Fe_Fin, $ID_Producto){
        $query = "
        SELECT
            SUM(GD.Qt_Producto) AS Qt_Compra
        FROM
            guia_cabecera AS GC
            JOIN guia_detalle AS GD
                ON (GC.ID_Guia_Cabecera = GD.ID_Guia_Cabecera)
            JOIN tipo_movimiento AS TM
                ON (GC.ID_Tipo_Movimiento = TM.ID_Tipo_Movimiento)
        WHERE
            GC.ID_Empresa = " . $ID_Empresa . "
            AND GC.ID_Almacen = " . $ID_Almacen . "
            AND GC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
            AND TM.Nu_Tipo_Movimiento = 0
            AND GD.ID_Producto = " . $ID_Producto;

        $iCantidadProductoCompraG = $this->db->query($query)->row()->Qt_Compra;
        settype($iCantidadProductoCompraG, "int");
        
        $query = "
        SELECT
            SUM(CD.Qt_Producto) AS Qt_Compra
        FROM
            documento_cabecera AS CC
            JOIN documento_detalle AS CD
                ON (CC.ID_Documento_Cabecera = CD.ID_Documento_Cabecera)
        WHERE
            CC.ID_Empresa = " . $ID_Empresa . "
            AND CC.ID_Almacen = " . $ID_Almacen . "
            AND CC.ID_Tipo_Asiento = 2
            AND CC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
            AND CD.ID_Producto = " . $ID_Producto;
    
        $iCantidadProductoCompraD = $this->db->query($query)->row()->Qt_Compra;
        settype($iCantidadProductoCompraD, "int");
        
        $iCantidadProductoCompra = $iCantidadProductoCompraD + $iCantidadProductoCompraG;
        
        $query = "
        SELECT
            SUM(GD.Qt_Producto) AS Qt_Compra
        FROM
            guia_cabecera AS GC
            JOIN guia_detalle AS GD
                ON (GC.ID_Guia_Cabecera = GD.ID_Guia_Cabecera)
            JOIN tipo_movimiento AS TM
                ON (GC.ID_Tipo_Movimiento = TM.ID_Tipo_Movimiento)
        WHERE
            GC.ID_Empresa = " . $ID_Empresa . "
            AND GC.ID_Almacen = " . $ID_Almacen . "
            AND GC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
            AND TM.Nu_Tipo_Movimiento = 1
            AND GD.ID_Producto = " . $ID_Producto;

        $iCantidadProductoVentaG = $this->db->query($query)->row()->Qt_Compra;
        settype($iCantidadProductoVentaG, "int");
        
        $query = "
        SELECT
            SUM(CD.Qt_Producto) AS Qt_Venta
        FROM
            documento_cabecera AS CC
            JOIN documento_detalle AS CD
                ON (CC.ID_Documento_Cabecera = CD.ID_Documento_Cabecera)
        WHERE
            CC.ID_Empresa = " . $ID_Empresa . "
            AND CC.ID_Almacen = " . $ID_Almacen . "
            AND CC.ID_Tipo_Asiento = 1
            AND CC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
            AND CD.ID_Producto = " . $ID_Producto;
        
        $iCantidadProductoVentaD = $this->db->query($query)->row()->Qt_Venta;
        settype($iCantidadProductoVentaD, "int");
        
        $iCantidadProductoVenta = $iCantidadProductoVentaG + $iCantidadProductoVentaD;
        
        $iCantidadProducto = ($iCantidadProductoVenta < $iCantidadProductoCompra) ? ($iCantidadProductoCompra - $iCantidadProductoVenta) : 0;
        return $iCantidadProducto;
    }
}
