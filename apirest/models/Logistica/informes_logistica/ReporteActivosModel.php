<?php
class ReporteActivosModel extends CI_Model{

	public function __construct(){ 
		parent::__construct();
	}
	
    public function getReporte($arrParams){ 
        $Fe_Inicio = $arrParams['Fe_Inicio'];
        $Fe_Fin = $arrParams['Fe_Fin'];
        $ID_Tipo_Documento = $arrParams['ID_Tipo_Documento'];
        $ID_Serie_Documento = $arrParams['ID_Serie_Documento'];
        $ID_Numero_Documento = $arrParams['ID_Numero_Documento'];
        $Nu_Estado_Documento = $arrParams['Nu_Estado_Documento'];

        // M044 - INICIO 
        //$iIdProveedor=$arrParams['iIdProveedor'];
        //$sNombreProveedor=$arrParams['sNombreProveedor'];

        $iIdCliente=$arrParams['iIdCliente'];
        $sNombreCliente=$arrParams['sNombreCliente'];
        // M044 - FIN 

        $iIdItem=$arrParams['iIdItem'];
        $sNombreItem=$arrParams['sNombreItem'];
        $ID_OI_Detalle=$arrParams['ID_OI_Detalle'];

        $cond_serie = $ID_Serie_Documento != "-" ? "AND CC.ID_Serie_Documento = '" . $ID_Serie_Documento . "'" : "";
        $cond_numero = $ID_Numero_Documento != "-" ? "AND CC.ID_Numero_Documento = '" . $ID_Numero_Documento . "'" : "";
        $cond_estado_documento = $Nu_Estado_Documento != "0" ? 'AND CC.Nu_Estado = ' . $Nu_Estado_Documento : "";
        
        // M044 - INICIO 
        //$cond_proveedor = ( $iIdProveedor != '-' && $sNombreProveedor != '-' ) ? 'AND PROV.ID_Entidad = ' . $iIdProveedor : "";
        $cond_cliente = ( $iIdCliente != '-' && $sNombreCliente != '-' ) ? 'AND AE.ID_entidad = ' . $iIdCliente : "";
        // M044 - FIN 

        $cond_item = ( $iIdItem != '-' && $sNombreItem != '-' ) ? 'AND CD.ID_Producto = ' . $iIdItem : "";
        
        $query = "SELECT
PROV.ID_Entidad,
TD.No_Tipo_Documento_Breve,
CC.ID_Tipo_Documento,
CC.ID_Serie_Documento,
CC.ID_Numero_Documento,
CC.Fe_Emision,
PROV.Nu_Documento_Identidad,
PROV.No_Entidad,
MONE.ID_Moneda,
MONE.No_Signo,
MONE.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Moneda,
PROD.Nu_Codigo_Barra,
PROD.No_Producto,

/* M023 - FIX - I */ 
coalesce(SF.No_Sub_Familia,' ') AS No_Sub_Familia,
coalesce(MA.No_Marca,' ') AS No_Marca,
coalesce(PROD.No_Modelo_Vehiculo,' ')  AS Modelo_Tipo,
coalesce(PROD.No_Motor,' ') AS Contenido,
/* M023 - FIX - F */

CD.Qt_Producto,
CD.Ss_Precio,
TDESTADO.No_Descripcion AS No_Estado,
TDESTADO.No_Class AS No_Class_Estado,
AE.ID_Activo_Empresa,
AE.Fe_Salida_Item,

/* M023 - I */
coalesce(RECP.No_Entidad,' ')    AS receptor,
coalesce(SOLIC.No_Entidad,' ')   AS  solic_loc,
CASE WHEN AE.ID_tipo_activo =2 THEN 'Mayor'
WHEN  AE.ID_tipo_activo =1 THEN 'Menor' 
ELSE ' '
END AS Tipo_Activo,
/* M023 - F */

TAREA.No_Tipo_Documento_Breve AS No_Area_Ingreso,
TDESTADODEPRE.No_Descripcion AS No_Estado_Activo,
TDESTADODEPRE.No_Class AS No_Class_Estado_Activo
FROM
guia_cabecera AS CC
LEFT JOIN guia_detalle AS CD ON(CC.ID_Guia_Cabecera = CD.ID_Guia_Cabecera)
LEFT JOIN activo_empresa AS AE ON(AE.ID_Guia_Cabecera = CC.ID_Guia_Cabecera AND AE.ID_Guia_Detalle = CD.ID_Guia_Detalle)
JOIN tipo_documento AS TAREA ON(TAREA.ID_Tipo_Documento = AE.ID_Area_Ingreso)

/* M023 - I */
LEFT JOIN entidad RECP ON RECP.ID_Entidad =AE.ID_receptor 
LEFT JOIN entidad SOLIC ON SOLIC.ID_Entidad =AE.ID_solic_loc 
/* M023 - F */

JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = CC.ID_Tipo_Documento)
JOIN entidad AS PROV ON(PROV.ID_Entidad = CC.ID_Entidad)
LEFT JOIN producto AS PROD ON(CD.ID_Producto = PROD.ID_Producto)

/* M023 - I */
LEFT JOIN subfamilia SF ON PROD.ID_Familia=SF.ID_Familia AND PROD.ID_Sub_Familia=SF.ID_Sub_Familia 
LEFT JOIN marca MA ON PROD.ID_Marca =MA.ID_Marca
LEFT JOIN tipo_producto TP  ON PROD.ID_Tipo_Producto=TP.ID_Tipo_Producto 
/* M023 - F */

JOIN moneda AS MONE ON(MONE.ID_Moneda = CC.ID_Moneda)
JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = CC.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
JOIN tabla_dato AS TDESTADODEPRE ON(TDESTADODEPRE.Nu_Valor = AE.Nu_Estado_Depreciacion AND TDESTADODEPRE.No_Relacion = 'Tipos_Activo_Empresa')
WHERE
CC.ID_Empresa = " . $this->user->ID_Empresa . "
AND CC.ID_Organizacion = " . $this->user->ID_Organizacion . "
AND CC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
" . $cond_serie . "
" . $cond_numero . "
" . $cond_estado_documento . "

/* M044 - INICIO */
/* " . $cond_proveedor . " */
" . $cond_cliente . " 
/* M044 - FIN   */

" . $cond_item . "
ORDER BY
PROV.ID_Entidad,
CC.Fe_Emision,
CC.ID_Tipo_Documento,
CC.ID_Serie_Documento,
CONVERT(CC.ID_Numero_Documento, SIGNED INTEGER),
AE.ID_Activo_Empresa DESC;"; 
        if ( !$this->db->simple_query($query) ){
            $error = $this->db->error();
            return array(
                'sStatus' => 'danger',
                'sMessage' => 'Problemas al obtener datos',
                'sCodeSQL' => $error['code'],
                'sMessageSQL' => $error['message'],
            );
        }
        $arrResponseSQL = $this->db->query($query);
        if ( $arrResponseSQL->num_rows() > 0 ){
            return array(
                'sStatus' => 'success',
                'arrData' => $arrResponseSQL->result(),
            );
        }
        return array(
            'sStatus' => 'warning',
            'sMessage' => 'No se encontro registro',
        );
    }
}
