<?php
class SalidaInventarioModel extends CI_Model{ 
	var $table          				= 'guia_cabecera';
	var $table_guia_detalle				= 'guia_detalle';
	var $table_guia_enlace 				= 'guia_enlace';
	var $table_flete		 			= 'flete';
	var $table_tipo_documento			= 'tipo_documento';
	var $table_tipo_movimiento			= 'tipo_movimiento';
	var $table_impuesto_cruce_documento	= 'impuesto_cruce_documento';
	var $table_entidad					= 'entidad';
	var $table_tipo_documento_identidad	= 'tipo_documento_identidad';
	var $table_moneda					= 'moneda';
	var $table_organizacion				= 'organizacion';
	var $table_tabla_dato				= 'tabla_dato';
	var $table_documento_detalle_lote = 'documento_detalle_lote';
	
    var $column_order = array('');
    var $column_search = array('');
    var $order = array('');
	
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){        
        $this->db->select('VC.ID_Guia_Cabecera, VC.Fe_Emision, TDOCU.No_Tipo_Documento_Breve, VC.ID_Serie_Documento, VC.ID_Numero_Documento, TDOCUIDEN.No_Tipo_Documento_Identidad_Breve, PROVE.No_Entidad, MONE.No_Signo, ROUND(VC.Ss_Total, 2) AS Ss_Total, VC.Nu_Estado, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, TDOCU.Nu_Enlace, VC.Nu_Descargar_Inventario, VE.ID_Guia_Cabecera AS ID_Guia_Cabecera_Enlace')
		->from($this->table . ' AS VC')
		->join($this->table_guia_enlace . ' AS VE', 'VE.ID_Documento_Cabecera = VC.ID_Guia_Cabecera', 'left')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
		->join($this->table_entidad . ' AS PROVE', 'PROVE.ID_Entidad = VC.ID_Entidad', 'join')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = PROVE.ID_Tipo_Documento_Identidad', 'join')
		->join($this->table_moneda . ' AS MONE', 'MONE.ID_Moneda = VC.ID_Moneda', 'join')
		->join($this->table_tipo_movimiento . ' AS TMOVI', 'TMOVI.ID_Tipo_Movimiento = VC.ID_Tipo_Movimiento', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
		->where('VC.ID_Tipo_Asiento', 3)
		->where('TMOVI.Nu_Tipo_Movimiento', 1);
		
    	$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
        
        if($this->input->post('Filtro_TiposDocumento'))
        	$this->db->where('VC.ID_Tipo_Documento', $this->input->post('Filtro_TiposDocumento'));
        
        if($this->input->post('Filtro_SerieDocumento'))
        	$this->db->where('VC.ID_Serie_Documento', $this->input->post('Filtro_SerieDocumento'));
        
        if($this->input->post('Filtro_NumeroDocumento'))
        	$this->db->where('VC.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento'));
        
        if($this->input->post('Filtro_Estado') != '')
        	$this->db->where('VC.Nu_Estado', $this->input->post('Filtro_Estado'));
        
        if($this->input->post('Filtro_Entidad'))
        	$this->db->where('PROVE.No_Entidad', $this->input->post('Filtro_Entidad'));

        if(isset($_POST['order']))
        	$this->db->order_by( 'VC.Fe_Emision DESC, VC.ID_Tipo_Documento DESC, VC.ID_Serie_Documento DESC, CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
        else if(isset($this->order))
        	$this->db->order_by( 'VC.Fe_Emision DESC, VC.ID_Tipo_Documento DESC, VC.ID_Serie_Documento DESC, CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $query = "SELECT
VC.ID_Guia_Cabecera,
VC.ID_Empresa,
VC.ID_Organizacion,
VC.ID_Almacen,
VC.ID_Guia_Cabecera,
PROVE.ID_Entidad,
PROVE.No_Entidad,
PROVE.Nu_Documento_Identidad,
VC.ID_Tipo_Documento,
TDOCU.Nu_Impuesto,
TDOCU.Nu_Enlace,
TDOCU.No_Tipo_Documento,
VC.ID_Serie_Documento,
VC.ID_Numero_Documento,
VC.Fe_Emision,
VC.ID_Moneda,
VC.Fe_Periodo,
VC.Nu_Descargar_Inventario,
VC.ID_Lista_Precio_Cabecera,
VD.ID_Producto,
TDOCUIDEN.No_Tipo_Documento_Identidad_Breve,
PRO.Nu_Tipo_Producto,
PRO.Nu_Codigo_Barra,
PRO.No_Producto,
ROUND(VD.Ss_Precio, 6) AS Ss_Precio,
VD.Qt_Producto,
VD.ID_Impuesto_Cruce_Documento,
VD.Ss_SubTotal AS Ss_SubTotal_Producto,
VD.Ss_Impuesto AS Ss_Impuesto_Producto,
ROUND(VD.Ss_Descuento, 2) AS Ss_Descuento_Producto,
ROUND(VD.Ss_Descuento_Impuesto, 2) AS Ss_Descuento_Impuesto_Producto,
ROUND(VD.Po_Descuento, 2) AS Po_Descuento_Impuesto_Producto,
ROUND(VD.Ss_Total, 2) AS Ss_Total_Producto,
ICDOCU.Ss_Impuesto,
IMP.Nu_Tipo_Impuesto,
VC.Txt_Glosa,
ROUND(VC.Ss_Descuento, 2) AS Ss_Descuento,
ROUND(VC.Ss_Total, 2) AS Ss_Total,
VC.Po_Descuento,
VC.ID_Tipo_Movimiento,
F.ID_Entidad AS ID_Entidad_Transportista,
TRANS.No_Entidad AS No_Entidad_Transportista,
TRANS.Nu_Documento_Identidad AS Nu_Documento_Identidad_Transportista,
F.No_Placa,
F.Fe_Traslado,
F.ID_Motivo_Traslado,
F.No_Licencia,
F.No_Certificado_Inscripcion,
VC.Nu_Estado,
VC.Nu_Tipo_Mantenimiento,
MONE.No_Signo,
MONE.No_Moneda,
VDL.Nu_Lote_Vencimiento,
VDL.Fe_Lote_Vencimiento,
VDL.ID_Area_Ingreso,
VDL.Nu_Estado_Depreciacion,
PROVE.Txt_Direccion_Entidad
FROM
" . $this->table . " AS VC
JOIN " . $this->table_guia_detalle . " AS VD ON(VC.ID_Guia_Cabecera = VD.ID_Guia_Cabecera)
LEFT JOIN " . $this->table_documento_detalle_lote . " AS VDL ON(VC.ID_Guia_Cabecera = VDL.ID_Guia_Cabecera AND VD.ID_Guia_Detalle = VDL.ID_Guia_Detalle)
JOIN " . $this->table_entidad . " AS PROVE ON(PROVE.ID_Entidad = VC.ID_Entidad)
JOIN tipo_documento_identidad AS TDOCUIDEN ON(PROVE.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
JOIN " . $this->table_impuesto_cruce_documento . " AS ICDOCU ON(ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
JOIN impuesto AS IMP ON(IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
LEFT JOIN flete AS F ON(F.ID_Guia_Cabecera = VC.ID_Guia_Cabecera)
JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
LEFT JOIN " . $this->table_entidad . " AS TRANS ON(TRANS.ID_Entidad = F.ID_Entidad)
WHERE
VC.ID_Guia_Cabecera = " . $ID;
        return $this->db->query($query)->result();
    }
    
    public function agregarCompra($arrCompraCabecera, $arrCompraDetalle, $esEnlace, $ID_Guia_Cabecera, $arrClienteNuevo){
    	$ID_Entidad=0;
    	if (!empty($arrCompraCabecera['ID_Entidad']))
    		$ID_Entidad=$arrCompraCabecera['ID_Entidad'];
		$query = "SELECT
ID_Tipo_Documento,
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM
serie_documento
WHERE
ID_Serie_Documento_PK=" . $arrCompraCabecera['ID_Serie_Documento_PK'];
		$arrSerieDocumento = $this->db->query($query)->row();

		$arrCompraCabecera['ID_Numero_Documento'] = $arrSerieDocumento->Nu_Numero_Documento;
		
		if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
			$this->db->trans_rollback();
			return array('status' => 'danger', 'style_modal' => 'modal-danger', 'message' => 'Deben configurar serie, no existe');
		}

		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 3 AND ID_Entidad = " . $ID_Entidad . " AND ID_Tipo_Documento = " . $arrSerieDocumento->ID_Tipo_Documento . " AND ID_Serie_Documento = '" . $arrSerieDocumento->ID_Serie_Documento . "' AND ID_Numero_Documento = '" . $arrSerieDocumento->Nu_Numero_Documento . "' LIMIT 1")->row()->existe > 0){
			$this->db->trans_rollback();
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			foreach ($arrCompraDetalle as $row) {
				$arrParams = array(
					'ID_Empresa' => $this->empresa->ID_Empresa,
					'ID_Organizacion' => $this->empresa->ID_Organizacion,
					'ID_Almacen' => $arrCompraCabecera['ID_Almacen'],
					'ID_Producto' => $row['ID_Producto'],
				);
				if ($this->HelperModel->obtenerTipoItem($arrParams) == 1 && $this->empresa->Nu_Validar_Stock == 1) {
					$Qt_Stock_Actual = $this->HelperModel->validateStockNowInterno($arrParams);
					settype($Qt_Stock_Actual, "double");
					if ($Qt_Stock_Actual < $row['Qt_Producto']) {
						return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El stock a despachar es mayor al actual');
					}
				}
			}

			$this->db->trans_begin();

			$Nu_Correlativo = 0;
			$Fe_Year = ToYear($arrCompraCabecera['Fe_Periodo']);
			$Fe_Month = ToMonth($arrCompraCabecera['Fe_Periodo']);
			$arrCorrelativoPendiente = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento_pendiente WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 3 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' ORDER BY Nu_Correlativo DESC LIMIT 1")->result();

			if ( count($arrCorrelativoPendiente) > 0 ){
				$Nu_Correlativo = $arrCorrelativoPendiente[0]->Nu_Correlativo;
				
				$this->db->where('ID_Empresa', $this->user->ID_Empresa);
				$this->db->where('ID_Tipo_Asiento', 3);
				$this->db->where('Fe_Year', $Fe_Year);
				$this->db->where('Fe_Month', $Fe_Month);
				$this->db->where('Nu_Correlativo', $Nu_Correlativo);
		        $this->db->delete('correlativo_tipo_asiento_pendiente');
			} else {
				if($this->db->query("SELECT COUNT(*) AS existe FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 3 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->existe > 0){
					$sql_correlativo_libro_sunat = "UPDATE
correlativo_tipo_asiento
SET
Nu_Correlativo = Nu_Correlativo + 1
WHERE
ID_Empresa=" . $this->user->ID_Empresa . "
AND ID_Tipo_Asiento=3
AND Fe_Year='" . $Fe_Year. "'
AND Fe_Month='" . $Fe_Month . "'";
					$this->db->query($sql_correlativo_libro_sunat);
				} else {
					$sql_correlativo_libro_sunat = "INSERT INTO correlativo_tipo_asiento (
ID_Empresa,
ID_Tipo_Asiento,
Fe_Year,
Fe_Month,
Nu_Correlativo
) VALUES (
 " . $this->user->ID_Empresa . ",
 3,
 '" . $Fe_Year . "',
 '" . $Fe_Month . "',
 1
);";
					$this->db->query($sql_correlativo_libro_sunat);
				}
				$Nu_Correlativo = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 3 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->Nu_Correlativo;
			}
			
			if (is_array($arrClienteNuevo)){
			    unset($arrCompraCabecera['ID_Entidad']);
			    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
					$arrCliente = array(
		                'ID_Empresa'					=> $this->user->ID_Empresa,
	                	'ID_Organizacion'				=> $arrCompraCabecera['ID_Organizacion'],
		                'Nu_Tipo_Entidad'				=> 0,//Cliente
		                'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
		                'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
		                'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
		                'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
		                'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
		                'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
		                'Nu_Estado' 					=> 1,
		            );
		    		$this->db->insert('entidad', $arrCliente);
		    		$Last_ID_Entidad = $this->db->insert_id();
			    } else {
					$this->db->trans_rollback();
					return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
				}
	    		$arrCompraCabecera = array_merge($arrCompraCabecera, array("ID_Entidad" => $Last_ID_Entidad));
			}

			//Flete
			$arrFlete = array(
				'iFlete' => $arrCompraCabecera['iFlete'],
				'ID_Entidad' => $arrCompraCabecera['ID_Entidad_Transportista'],
				'No_Placa' => $arrCompraCabecera['No_Placa'],
				'Fe_Traslado' => $arrCompraCabecera['Fe_Traslado'],
				'ID_Motivo_Traslado' => $arrCompraCabecera['ID_Motivo_Traslado'],
				'No_Licencia' => $arrCompraCabecera['No_Licencia'],
				'No_Certificado_Inscripcion' => $arrCompraCabecera['No_Certificado_Inscripcion'],
			);
			$ID_Origen_Tabla = (isset($arrCompraCabecera['ID_Origen_Tabla']) ? $arrCompraCabecera['ID_Origen_Tabla'] : '');
			unset( $arrCompraCabecera['ID_Origen_Tabla'] );
			unset( $arrCompraCabecera['iFlete'] );
			unset( $arrCompraCabecera['ID_Entidad_Transportista'] );
			unset( $arrCompraCabecera['No_Placa'] );
			unset( $arrCompraCabecera['Fe_Traslado'] );
			unset( $arrCompraCabecera['ID_Motivo_Traslado'] );
			unset( $arrCompraCabecera['No_Licencia'] );
			unset( $arrCompraCabecera['No_Certificado_Inscripcion'] );

			$arrCompraCabecera = array_merge($arrCompraCabecera, array("Nu_Correlativo" => $Nu_Correlativo));
			$this->db->insert($this->table, $arrCompraCabecera);
			$Last_ID_Guia_Cabecera = $this->db->insert_id();
			
			if ( $arrFlete['iFlete'] == '1' ) {
				unset( $arrFlete['iFlete'] );
				$arrFlete = array_merge( $arrFlete, array( 'ID_Guia_Cabecera' => $Last_ID_Guia_Cabecera) );
				$this->db->insert($this->table_flete, $arrFlete);
			}

			if ( $ID_Origen_Tabla != 0 ) {
				$arrRelacionTabla = array(
					'ID_Empresa' => $arrCompraCabecera['ID_Empresa'],
					'Nu_Relacion_Datos' => 0,//Presupuesto a vale guia de salida
					'ID_Relacion_Enlace_Tabla' => $Last_ID_Guia_Cabecera,
					'ID_Origen_Tabla' => $ID_Origen_Tabla,
				);
				$this->db->insert('relacion_tabla', $arrRelacionTabla);
			}

			foreach ($arrCompraDetalle as $row) {
				$guia_detalle[] = array(
					'ID_Empresa' => $this->user->ID_Empresa,
					'ID_Guia_Cabecera' => $Last_ID_Guia_Cabecera,
					'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
					'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio' => $this->security->xss_clean($row['Ss_Precio']),
					'Ss_SubTotal' => $this->security->xss_clean($row['Ss_SubTotal']),
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['Ss_Descuento'],
					'ID_Impuesto_Cruce_Documento' => $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
					'Ss_Impuesto' => $this->security->xss_clean($row['Ss_Impuesto']),
					'Ss_Total' => round($this->security->xss_clean($row['Ss_Total']), 2),
				);
			}
			$this->db->insert_batch($this->table_guia_detalle, $guia_detalle);
			$iIdDocumentoDetalleFirst = $this->db->insert_id();

			$iCountDataDetalle = count($arrCompraDetalle);
			foreach ($arrCompraDetalle as $row) {
				if (!empty($row['Nu_Lote_Vencimiento']) && !empty($row['Fe_Lote_Vencimiento'])) {
					$documento_detalle_lote[] = array(
						'ID_Empresa' => $this->user->ID_Empresa,
						'ID_Organizacion' => $this->empresa->ID_Organizacion,
						'ID_Almacen' => (isset($arrCompraCabecera['ID_Almacen']) ? $arrCompraCabecera['ID_Almacen'] : ''),
						'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
						'ID_Guia_Cabecera'	=> $Last_ID_Guia_Cabecera,
						'ID_Guia_Detalle'	=> $iIdDocumentoDetalleFirst,
						'Nu_Lote_Vencimiento' => $this->security->xss_clean($row['Nu_Lote_Vencimiento']),
						'Fe_Lote_Vencimiento' => ToDate($this->security->xss_clean($row['Fe_Lote_Vencimiento'])),
						'ID_Area_Ingreso' => $this->security->xss_clean($row['ID_Area_Ingreso']),
						'Nu_Estado_Depreciacion' => $this->security->xss_clean($row['Nu_Estado_Depreciacion']),
					);
					++$iIdDocumentoDetalleFirst;
				}
			}
			if ( isset($documento_detalle_lote) )
				$this->db->insert_batch($this->table_documento_detalle_lote, $documento_detalle_lote);

	        if ($this->db->trans_status() === FALSE) { 
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
	        } else {
	            $this->db->trans_commit();
				$this->db->query("UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1 WHERE ID_Serie_Documento_PK=" . $arrCompraCabecera['ID_Serie_Documento_PK']);
	            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado', 'Last_ID_Guia_Cabecera' => $Last_ID_Guia_Cabecera);
	        }
		}
    }
	 
// STOCK_PROD_UPDT - INICIO 
public function Cust_ActualizaStock (  $ID_Empresa,$ID_Organizacion ,$ID_Almacen , $arrDetalleCompra){

	$ID_Producto='x';
	if ( !empty($arrDetalleCompra) ) {
		foreach ($arrDetalleCompra as $row) {

			$ID_Producto = $this->security->xss_clean($row['ID_Producto']);

			// saldo inicial
			$query = "SELECT
			SUM(K.Qt_Producto) as Total
			FROM
			movimiento_inventario AS K
			JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
			JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
			JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
			WHERE
			K.ID_Empresa =  " . $ID_Empresa . "  AND K.ID_Organizacion = " . $ID_Organizacion   . " AND K.ID_Almacen = " . $ID_Almacen . "  
			and TMOVI.ID_Tipo_Movimiento in (2,7)
			AND PRO.Nu_Tipo_Producto = 1 AND CVCAB.ID_Tipo_Documento != 20
			AND PRO.ID_Producto = ". $ID_Producto ;
			$arrSaldoInicial = $this->db->query($query)->row();
			$Saldo_Inicial = $arrSaldoInicial->Qt_Producto;
			if ( empty($Saldo_Inicial) ) {
				$Saldo_Inicial = 0;
			} 

			// ingreso kardex
			$query = "
			SELECT
			SUM(K.Qt_Producto) AS Total_ingreso
			FROM
			movimiento_inventario AS K
			JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
			JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
			JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
			WHERE
			K.ID_Empresa = " . $ID_Empresa . " AND K.ID_Organizacion = " . $ID_Organizacion   . " 
			AND K.ID_Almacen = " . $ID_Almacen . " and TMOVI.ID_Tipo_Movimiento   not in (2,7)   
			AND PRO.Nu_Tipo_Producto = 1 AND CVCAB.ID_Tipo_Documento != 20
			AND PRO.ID_Producto =" . $ID_Producto . " ";
			$arrIngreso = $this->db->query($query)->row();
			$Total_ingreso = $arrIngreso->Total_ingreso;
			if ( empty($Total_ingreso) ) {
				$Total_ingreso = 0;
			}  

			// egresos kardex
			$query = "
			SELECT
			SUM(K.Qt_Producto) as Total_egreso
			FROM
			movimiento_inventario AS K
			JOIN guia_cabecera AS GESCAB ON(K.ID_Guia_Cabecera = GESCAB.ID_Guia_Cabecera)
			JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
			JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
			WHERE
			K.ID_Empresa = " . $ID_Empresa . " AND K.ID_Organizacion = " . $ID_Organizacion   . " 
			AND K.ID_Almacen = " . $ID_Almacen . " and TMOVI.ID_Tipo_Movimiento   not in (2,7)   
			AND PRO.Nu_Tipo_Producto = 1 AND GESCAB.ID_Tipo_Documento != 20
			AND PRO.ID_Producto=" . $ID_Producto . " ";
			$arrEgreso = $this->db->query($query)->row();
			$Total_egreso = $arrEgreso->Total_egreso;
			if ( empty($Total_egreso) ) {
				$Total_egreso = 0;
			} 

			$Stck_prod = ( $Saldo_Inicial +  $Total_ingreso ) - $Total_egreso ;

			$sql = " UPDATE stock_producto SET qt_producto = " . $Stck_prod . " WHERE id_producto = " . $ID_Producto ;
			$this->db->query($sql);

		}
			 
	}
	       
	// return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => $Saldo_Inicial .'/'. $Total_ingreso .'/' . $Total_egreso ) ;

	    
	return "Stock_Actualizado";
}
  
// STOCK_PROD_UPDT - FIN 


    public function actualizarCompra($where, $arrCompraCabecera, $arrCompraDetalle, $esEnlace, $ID_Guia_Cabecera, $arrClienteNuevo){
		$this->db->trans_begin();

		$arrDataModificar = $this->db->query("SELECT ID_Organizacion, ID_Guia_Cabecera, ID_Tipo_Documento, Nu_Correlativo, Nu_Descargar_Inventario FROM guia_cabecera WHERE ID_Guia_Cabecera = " . $where['ID_Guia_Cabecera'] . " LIMIT 1")->result();
		
		$ID_Guia_Cabecera = $arrDataModificar[0]->ID_Guia_Cabecera;
		$ID_Tipo_Documento = $arrDataModificar[0]->ID_Tipo_Documento;
		$Nu_Correlativo = $arrDataModificar[0]->Nu_Correlativo;
		$Nu_Descargar_Inventario = $arrDataModificar[0]->Nu_Descargar_Inventario;
		
		if ($ID_Tipo_Documento != $arrCompraCabecera['ID_Tipo_Documento'])
			$this->db->query("UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1 WHERE ID_Serie_Documento_PK=" . $arrCompraCabecera['ID_Serie_Documento_PK']);

		$this->db->delete($this->table_guia_detalle, $where);

		if ($Nu_Descargar_Inventario == 1) {
			$query = "SELECT * FROM movimiento_inventario WHERE ID_Guia_Cabecera = " . $ID_Guia_Cabecera;
			$arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where_stock_producto = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
					$stock_producto = array(
						'ID_Producto'		=> $row->ID_Producto,
						'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
						'Ss_Costo_Promedio'	=> 0.00,
					);
					$this->db->update('stock_producto', $stock_producto, $where_stock_producto);
				}
			}
			$this->db->where('ID_Guia_Cabecera', $ID_Guia_Cabecera);
			$this->db->delete('movimiento_inventario');
		}
		
		$this->db->delete($this->table, $where);
		$ID_Entidad=0;
    	if (!empty($arrCompraCabecera['ID_Entidad']))
    		$ID_Entidad=$arrCompraCabecera['ID_Entidad'];
		if($ID_Tipo_Documento != $arrCompraCabecera['ID_Tipo_Documento'] &&  $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 3 AND ID_Entidad = " . $ID_Entidad . " AND ID_Tipo_Documento = " . $arrCompraCabecera['ID_Tipo_Documento'] . " AND ID_Serie_Documento = '" . $arrCompraCabecera['ID_Serie_Documento'] . "' AND ID_Numero_Documento = " . $arrCompraCabecera['ID_Numero_Documento'] . " LIMIT 1")->row()->existe > 0){
			$this->db->trans_rollback();
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			if (is_array($arrClienteNuevo)){
				unset($arrCompraCabecera['ID_Entidad']);
				if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
					$arrCliente = array(
						'ID_Empresa'					=> $this->user->ID_Empresa,
						'ID_Organizacion'				=> $arrCompraCabecera['ID_Organizacion'],
						'Nu_Tipo_Entidad'				=> 0,//Cliente
						'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
						'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
						'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
						'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
						'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
						'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
						'Nu_Estado' 					=> 1,
					);
					$this->db->insert('entidad', $arrCliente);
					$Last_ID_Entidad = $this->db->insert_id();
				} else {
					$this->db->trans_rollback();
					return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
				}
				$arrCompraCabecera = array_merge($arrCompraCabecera, array("ID_Entidad" => $Last_ID_Entidad));
			}
			
			//Flete
			$arrFlete = array(
				'iFlete' => $arrCompraCabecera['iFlete'],
				'ID_Entidad' => $arrCompraCabecera['ID_Entidad_Transportista'],
				'No_Placa' => $arrCompraCabecera['No_Placa'],
				'Fe_Traslado' => $arrCompraCabecera['Fe_Traslado'],
				'ID_Motivo_Traslado' => $arrCompraCabecera['ID_Motivo_Traslado'],
				'No_Licencia' => $arrCompraCabecera['No_Licencia'],
				'No_Certificado_Inscripcion' => $arrCompraCabecera['No_Certificado_Inscripcion'],
			);
			$ID_Origen_Tabla = (isset($arrCompraCabecera['ID_Origen_Tabla']) ? $arrCompraCabecera['ID_Origen_Tabla'] : '');
			unset( $arrCompraCabecera['ID_Origen_Tabla'] );
			unset( $arrCompraCabecera['iFlete'] );
			unset( $arrCompraCabecera['ID_Entidad_Transportista'] );
			unset( $arrCompraCabecera['No_Placa'] );
			unset( $arrCompraCabecera['Fe_Traslado'] );
			unset( $arrCompraCabecera['ID_Motivo_Traslado'] );
			unset( $arrCompraCabecera['No_Licencia'] );
			unset( $arrCompraCabecera['No_Certificado_Inscripcion'] );

			$arrCompraCabecera = array_merge($arrCompraCabecera, array( 'ID_Guia_Cabecera' => $ID_Guia_Cabecera, "Nu_Correlativo" => $Nu_Correlativo));
			$this->db->insert($this->table, $arrCompraCabecera);
			$Last_ID_Guia_Cabecera = $ID_Guia_Cabecera;
			//$Last_ID_Guia_Cabecera = $this->db->insert_id();
			
			if ( $arrFlete['iFlete'] == '1' ) {
				unset( $arrFlete['iFlete'] );
				$arrFlete = array_merge( $arrFlete, array( 'ID_Guia_Cabecera' => $Last_ID_Guia_Cabecera) );
				$this->db->insert($this->table_flete, $arrFlete);
			}

			if ( $ID_Origen_Tabla != 0 ) {
				$arrParams = array(
					'iRelacionDatos' => 0,
					'iIdRelacionEnlaceTabla' => $Last_ID_Guia_Cabecera
				);
				$arrRelacionTabla = array(
					'ID_Empresa' => $arrCompraCabecera['ID_Empresa'],
					'Nu_Relacion_Datos' => 0,//Presupuesto a vale guia
					'ID_Relacion_Enlace_Tabla' => $Last_ID_Guia_Cabecera,
					'ID_Origen_Tabla' => $this->HelperModel->getRelacionTabla($arrParams),
				);
				$this->db->insert('relacion_tabla', $arrRelacionTabla);
			}

			foreach ($arrCompraDetalle as $row) {
				$guia_detalle[] = array(
					'ID_Empresa' => $this->user->ID_Empresa,
					'ID_Guia_Cabecera' => $Last_ID_Guia_Cabecera,
					'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
					'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio' => $this->security->xss_clean($row['Ss_Precio']),
					'Ss_SubTotal' => $this->security->xss_clean($row['Ss_SubTotal']),
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['Ss_Descuento'],
					'ID_Impuesto_Cruce_Documento' => $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
					'Ss_Impuesto' => $this->security->xss_clean($row['Ss_Impuesto']),
					'Ss_Total' => round($this->security->xss_clean($row['Ss_Total']), 2),
				);
			}
			$this->db->insert_batch($this->table_guia_detalle, $guia_detalle);
			$iIdDocumentoDetalleFirst = $this->db->insert_id();

			$iCountDataDetalle = count($arrCompraDetalle);
			foreach ($arrCompraDetalle as $row) {
				if (!empty($row['Nu_Lote_Vencimiento']) && !empty($row['Fe_Lote_Vencimiento'])) {
					$documento_detalle_lote[] = array(
						'ID_Empresa' => $this->user->ID_Empresa,
						'ID_Organizacion' => $this->empresa->ID_Organizacion,
						'ID_Almacen' => (isset($arrCompraCabecera['ID_Almacen']) ? $arrCompraCabecera['ID_Almacen'] : ''),
						'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
						'ID_Guia_Cabecera'	=> $Last_ID_Guia_Cabecera,
						'ID_Guia_Detalle'	=> $iIdDocumentoDetalleFirst,
						'Nu_Lote_Vencimiento' => $this->security->xss_clean($row['Nu_Lote_Vencimiento']),
						'Fe_Lote_Vencimiento' => ToDate($this->security->xss_clean($row['Fe_Lote_Vencimiento'])),
						'ID_Area_Ingreso' => $this->security->xss_clean($row['ID_Area_Ingreso']),
						'Nu_Estado_Depreciacion' => $this->security->xss_clean($row['Nu_Estado_Depreciacion']),
					);
					++$iIdDocumentoDetalleFirst;
				}
			}
			if ( isset($documento_detalle_lote) )
				$this->db->insert_batch($this->table_documento_detalle_lote, $documento_detalle_lote);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al actualizar');
			} else {
				$this->db->trans_commit();
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro actualizado', 'Last_ID_Guia_Cabecera' => $Last_ID_Guia_Cabecera);
			}
		}
    }
    
	public function anularCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		$this->db->trans_begin();
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Guia_Cabecera', $ID);
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Guia_Cabecera', $ID);
        $this->db->delete($this->table_guia_detalle);

    	if ($Nu_Descargar_Inventario == 1) {    		
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Guia_Cabecera = ".$ID;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
					$stock_producto = array(
						'ID_Producto'		=> $row->ID_Producto,
						'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
						'Ss_Costo_Promedio'	=> 0.00,
					);
					$this->db->update('stock_producto', $stock_producto, $where);
				}
        	}
	        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Guia_Cabecera', $ID);
			$data = array(
				'Qt_Producto' => 0,
				'Ss_Precio' => 0,
				'Ss_SubTotal' => 0,
				'Ss_Costo_Promedio' => 0,
			);
	        $this->db->update('movimiento_inventario', $data);
    	}
    	
		if ($this->db->query("SELECT count(*) existe FROM guia_enlace WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Guia_Cabecera = " . $ID . " LIMIT 1")->row()->existe > 0){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Guia_Cabecera', $ID);
	        $this->db->delete('guia_enlace');
		}

        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Guia_Cabecera', $ID);
		$data = array(
			'Nu_Estado' => 7,
			'Ss_Descuento' => 0.00,
			'Ss_Total' => 0.00,
		);
        $this->db->update($this->table, $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al anular');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro anulado');
        }
	}
    
	public function eliminarCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		$this->db->trans_begin();
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Guia_Cabecera', $ID);
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Guia_Cabecera', $ID);
        $this->db->delete($this->table_guia_detalle);
                
    	if ($Nu_Descargar_Inventario == 1) {
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Guia_Cabecera = " . $ID;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
					$stock_producto = array(
						'ID_Producto' => $row->ID_Producto,
						'Qt_Producto' => ($Qt_Producto - round($row->Qt_Producto, 6)),
						'Ss_Costo_Promedio'	=> 0.00,
					);
					$this->db->update('stock_producto', $stock_producto, $where);
				}
        	}
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Guia_Cabecera', $ID);
	        $this->db->delete('movimiento_inventario');
    	}
        
        $arrCorrelativoPendiente = $this->db->query("SELECT Fe_Periodo, Nu_Correlativo FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Guia_Cabecera = " . $ID . " LIMIT 1")->result();
        
        $sql_correlativo_pendiente_libro_sunat = "
INSERT INTO correlativo_tipo_asiento_pendiente (
 ID_Empresa,
 ID_Tipo_Asiento,
 Fe_Year,
 Fe_Month,
 Nu_Correlativo
) VALUES (
 " . $this->user->ID_Empresa . ",
 3,
 '" . ToYear($arrCorrelativoPendiente[0]->Fe_Periodo) . "',
 '" . ToMonth($arrCorrelativoPendiente[0]->Fe_Periodo) . "',
 " . $arrCorrelativoPendiente[0]->Nu_Correlativo . "
);
		";
		$this->db->query($sql_correlativo_pendiente_libro_sunat);

		if ($this->db->query("SELECT count(*) existe FROM guia_enlace WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Guia_Cabecera = " . $ID . " LIMIT 1")->row()->existe > 0){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Guia_Cabecera', $ID);
	        $this->db->delete('guia_enlace');
		}
        
        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Guia_Cabecera', $ID);
        $this->db->delete($this->table);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
        }
	}
	
	public function getDetalle($arrParams){
		$query = "SELECT
GC.ID_Empresa,
GC.ID_Organizacion,
GC.ID_Almacen,
TDOCU.No_Tipo_Documento_Breve,
GC.ID_Serie_Documento,
GC.ID_Numero_Documento,
GD.ID_Guia_Detalle,
GD.ID_Producto,
ITEM.Nu_Codigo_Barra,
ITEM.No_Producto,
GD.Qt_Producto,
GD.Ss_Precio
FROM
guia_cabecera AS GC
JOIN guia_detalle AS GD ON(GC.ID_Guia_Cabecera = GD.ID_Guia_Cabecera)
JOIN tipo_documento AS TDOCU ON(TDOCU.ID_Tipo_Documento = GC.ID_Tipo_Documento)
JOIN producto AS ITEM ON(ITEM.ID_Producto = GD.ID_Producto)
WHERE GC.ID_Guia_Cabecera = " . $arrParams['ID_Guia_Cabecera'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}
	
	public function saveActivo($arrPost){
		if (!empty($arrPost['arrDetalleCompraModal'])) {
			$this->db->trans_begin();
			$activo_empresa=array();
			foreach ($arrPost['arrDetalleCompraModal'] as $row) {
				$activo_empresa[] = array(
					'ID_Empresa' => $row['ID_Empresa'],
					'ID_Organizacion' => $row['ID_Organizacion'],
					'ID_Almacen' => $row['ID_Almacen'],
					'ID_Guia_Cabecera' => $row['ID_Guia_Cabecera'],
					'ID_Guia_Detalle' => $row['ID_Guia_Detalle'],
					'Fe_Salida_Item' => ToDate($row['Fe_Salida_Item']),
					'ID_Area_Ingreso' => $row['ID_Area_Ingreso'],
					'Nu_Estado_Depreciacion' => $row['Nu_Estado_Depreciacion'],
						
						// M021 - INICIO     
						'ID_entidad' => $row['ID_Cliente_1'], 
						'ID_receptor' => $row['ID_Cliente_2'], 
						'ID_solic_loc' => $row['ID_Cliente_3'], 
						// M021- FIN

						// inicio m022
						'ID_tipo_activo' => $row['ID_Tipo_Activo'], 
						//fin m022  
				);
			}
			if(!empty($activo_empresa)){
				$this->db->insert_batch('activo_empresa', $activo_empresa);
			}        
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('sStatus' => 'danger','sMessage' => 'Error al guardar activo',);
			} else {
				$this->db->trans_commit();
				return array('sStatus' => 'success','sMessage' => 'Se creo activo',);
			}
		} else {
			return array('sStatus' => 'warning','sMessage' => 'No se encontro registro',);
		}
	}
}
