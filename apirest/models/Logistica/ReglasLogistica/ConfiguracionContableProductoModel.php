<?php
class ConfiguracionContableProductoModel extends CI_Model{
	var $table                                  = 'configuracion_cuenta_contable_producto';
	var $table_configuracion_cuenta_contable    = 'configuracion_cuenta_contable';
	var $table_producto                         = 'producto';
	var $table_tabla_dato                       = 'tabla_dato';
	//var $table_configuracion_cuenta_contable_producto     = 'configuracion_cuenta_contable_producto';
	
    var $column_order = array('Nu_Cuenta_Contable', null);
    var $column_search = array('Nu_Cuenta_Contable',);
    var $order = array('Nu_Cuenta_Contable' => 'asc',);
    
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
        if( $this->input->post('Filtros_ConfiguracionContableProductos') == 'ConfiguracionContableProducto' ){
            $this->db->like('Nu_Cuenta_Contable', $this->input->post('Global_Filter'));
        }
        
        $this->db->select($this->table . '.ID_Configuracion_Cuenta_Contable_Producto, CC.Nu_Tipo_Cuenta, CC.Nu_Cuenta_Contable, ITEM.No_Producto, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado')
		->from($this->table)
		->join($this->table_configuracion_cuenta_contable . ' AS CC', 'CC.ID_Configuracion_Cuenta_Contable = ' . $this->table . '.ID_Configuracion_Cuenta_Contable', 'join')
		->join($this->table_producto . ' AS ITEM', 'ITEM.ID_Producto = ' . $this->table . '.ID_Producto', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = ' . $this->table . '.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_Estados"', 'join')
        ->where($this->table . '.ID_Empresa', $this->user->ID_Empresa);

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $this->db->from($this->table);
        $this->db->where('ID_Configuracion_Cuenta_Contable_Producto',$ID);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function agregarConfiguracionContableProducto($data){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa =".$data['ID_Empresa']." AND ID_Configuracion_Cuenta_Contable=" . $data['ID_Configuracion_Cuenta_Contable'] . " AND ID_Producto='" . $data['ID_Producto'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			if ( $this->db->insert($this->table, $data) > 0 )
                return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
		}
		return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
    }
    
    public function actualizarConfiguracionContableProducto($where, $data, $EID_Configuracion_Cuenta_Contable, $EID_Producto){
		if( ($EID_Configuracion_Cuenta_Contable != $data['ID_Configuracion_Cuenta_Contable'] || $EID_Producto != $data['ID_Producto']) && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa =".$data['ID_Empresa']." AND ID_Configuracion_Cuenta_Contable=" . $data['ID_Configuracion_Cuenta_Contable'] . " AND ID_Producto='" . $data['ID_Producto'] . "' LIMIT 1")->row()->existe > 0 ){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
		    if ( $this->db->update($this->table, $data, $where) > 0 )
                return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
    }
    
	public function eliminarConfiguracionContableProducto($ID){
        /*
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_configuracion_cuenta_contable_producto . " WHERE ID_Configuracion_Cuenta_Contable_Producto=" . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'La cuenta tiene contable producto');
		}else{
        //para validar si ya se amarro con una OI
        */
			$this->db->where('ID_Configuracion_Cuenta_Contable_Producto', $ID);
            $this->db->delete($this->table);
		    if ( $this->db->affected_rows() > 0 )
		        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
                /*
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
        */
	}
}
