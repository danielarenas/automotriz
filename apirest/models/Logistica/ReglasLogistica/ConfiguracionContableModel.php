<?php
class ConfiguracionContableModel extends CI_Model{
	var $table              = 'configuracion_cuenta_contable';
	var $table_tabla_dato   = 'tabla_dato';
	var $table_configuracion_cuenta_contable_producto     = 'configuracion_cuenta_contable_producto';
	
    var $column_order = array('Nu_Cuenta_Contable', null);
    var $column_search = array('Nu_Cuenta_Contable',);
    var $order = array('Nu_Cuenta_Contable' => 'asc',);
    
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
        if( $this->input->post('Filtros_ConfiguracionContables') == 'ConfiguracionContable' ){
            $this->db->like('Nu_Cuenta_Contable', $this->input->post('Global_Filter'));
        }
        
        $this->db->select('ID_Configuracion_Cuenta_Contable, Nu_Tipo_Cuenta, Nu_Cuenta_Contable, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado')
		->from($this->table)
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = ' . $this->table . '.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_Estados"', 'join')
        ->where('ID_Empresa', $this->user->ID_Empresa);

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $this->db->from($this->table);
        $this->db->where('ID_Configuracion_Cuenta_Contable',$ID);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function agregarConfiguracionContable($data){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa =".$data['ID_Empresa']." AND Nu_Tipo_Cuenta=" . $data['Nu_Tipo_Cuenta'] . " AND Nu_Cuenta_Contable='" . $data['Nu_Cuenta_Contable'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			if ( $this->db->insert($this->table, $data) > 0 )
                return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
		}
		return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
    }
    
    public function actualizarConfiguracionContable($where, $data, $ENu_Tipo_Cuenta, $ENu_Cuenta_Contable){
		if( ($ENu_Tipo_Cuenta != $data['Nu_Tipo_Cuenta'] || $ENu_Cuenta_Contable != $data['Nu_Cuenta_Contable']) && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa =".$data['ID_Empresa']." AND Nu_Tipo_Cuenta=" . $data['Nu_Tipo_Cuenta'] . " AND Nu_Cuenta_Contable='" . $data['Nu_Cuenta_Contable'] . "' LIMIT 1")->row()->existe > 0 ){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
		    if ( $this->db->update($this->table, $data, $where) > 0 )
                return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
    }
    
	public function eliminarConfiguracionContable($ID){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_configuracion_cuenta_contable_producto . " WHERE ID_Configuracion_Cuenta_Contable=" . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'La cuenta tiene contable producto');
		}else{
        //para validar si ya se amarro con un producto y cuenta
			$this->db->where('ID_Configuracion_Cuenta_Contable', $ID);
            $this->db->delete($this->table);
		    if ( $this->db->affected_rows() > 0 )
		        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
	}
}
