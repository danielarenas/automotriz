<?php
class LineaModel extends CI_Model{
	var $table              = 'subfamilia';
	var $table_familia      = 'familia';
	var $table_tabla_dato   = 'tabla_dato';
	var $table_producto     = 'producto';
	
    var $column_order = array('No_Sub_Familia', null);
    var $column_search = array('No_Sub_Familia');
    var $order = array('No_Sub_Familia' => 'asc',);
	
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
        if( $this->input->post('Filtros_Lineas')=='Linea' ){
            $this->db->like('No_Sub_Familia', $this->input->post('Global_Filter'));
        }
        
        if( $this->input->post('Filtros_Lineas')=='Categoria' ){
            $this->db->like('No_Familia', $this->input->post('Global_Filter'));
        }
        
        $this->db->select('No_Familia, ID_Sub_Familia, No_Sub_Familia, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado')
		->from($this->table . ' AS SF')
		->join($this->table_familia . ' AS F', 'F.ID_Familia = SF.ID_Familia', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = SF.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_Estados"', 'join')
    	->where('SF.ID_Empresa', $this->user->ID_Empresa);

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $this->db->from($this->table);
        $this->db->where('ID_Sub_Familia',$ID);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function agregarLinea($data){
		$response = array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al insertar');
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa=" . $data['ID_Empresa'] . " AND ID_Familia=" . $data['ID_Familia'] . " AND No_Sub_Familia='" . $data['No_Sub_Familia'] . "' LIMIT 1")->row()->existe > 0){
			$response = array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			if ( $this->db->insert($this->table, $data) > 0 )
				$response = array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
		}
		return $response;
    }
    
    public function actualizarLinea($where, $data, $EID_Familia, $ENo_Sub_Familia){
        $response = array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al modificar');
		if( ($EID_Familia != $data['ID_Familia'] || $ENo_Sub_Familia != $data['No_Sub_Familia']) && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa=" . $data['ID_Empresa'] . " AND ID_Familia=" . $data['ID_Familia'] . " AND No_Sub_Familia='" . $data['No_Sub_Familia'] . "' LIMIT 1")->row()->existe > 0 ){
			$response = array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
		    if ( $this->db->update($this->table, $data, $where) > 0 )
		        $response = array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		}
        return $response;
    }
    
	public function eliminarLinea($ID){
		$response = array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al eliminar');
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_producto . " WHERE ID_Sub_Familia = " . $ID . " LIMIT 1")->row()->existe > 0){
			$response = array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'La sub categoría tiene producto(s)');
		}else{
			$this->db->where('ID_Sub_Familia', $ID);
            $this->db->delete($this->table);
		    if ( $this->db->affected_rows() > 0 ) {
		        $response = array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
		    }
		}
        return $response;
	}
}
