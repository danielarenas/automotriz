<?php
class AlmacenModel extends CI_Model{
	var $table = 'almacen';
	var $table_empresa = 'empresa';
	var $table_organizacion = 'organizacion';
	var $table_tabla_dato = 'tabla_dato';
	var $table_documento_cabecera = 'documento_cabecera';
	
    var $column_order = array('Nu_Tipo_Proveedor_FE', 'Nu_Tipo_Ecommerce_Empresa', 'No_Empresa', 'No_Organizacion', 'No_Almacen', 'Txt_Direccion_Almacen', 'Nu_Estado_Pago_Sistema');
    var $column_search = array('Nu_Tipo_Proveedor_FE', 'Nu_Tipo_Ecommerce_Empresa', 'No_Empresa', 'No_Organizacion', 'No_Almacen');
    var $order = array('No_Empresa' => 'asc', 'No_Organizacion' => 'asc', 'No_Almacen' => 'asc');
	
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
        if( $this->input->post('filtro_empresa') )
        	$this->db->where('EMP.ID_Empresa', $this->input->post('filtro_empresa'));
        
        if( $this->input->post('filtro_organizacion') )
            $this->db->where('ORG.ID_Organizacion', $this->input->post('filtro_organizacion'));

        if( $this->input->post('Filtros_Almacenes') == 'Almacen' )
            $this->db->like('No_Almacen', $this->input->post('Global_Filter'));

		$this->db->select('EMP.ID_Empresa, EMP.No_Empresa, ORG.ID_Organizacion, ORG.No_Organizacion, ID_Almacen, No_Almacen, Txt_Direccion_Almacen, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, Nu_Estado_Pago_Sistema, TIPOPROVEEDORFE.No_Class AS No_Class_Proveedor_FE, TIPOPROVEEDORFE.No_Descripcion AS No_Descripcion_Proveedor_FE, TIPOECOMMERCE.No_Class AS No_Class_Ecommerce, TIPOECOMMERCE.No_Descripcion AS No_Descripcion_Ecommerce')
		->from($this->table)
        ->join($this->table_organizacion . ' AS ORG', 'ORG.ID_Organizacion = ' . $this->table . '.ID_Organizacion', 'join')
        ->join($this->table_empresa . ' AS EMP', 'EMP.ID_Empresa = ORG.ID_Empresa', 'join')
        ->join($this->table_tabla_dato . ' AS TIPOPROVEEDORFE', 'TIPOPROVEEDORFE.Nu_Valor=EMP.Nu_Tipo_Proveedor_FE AND TIPOPROVEEDORFE.No_Relacion = "Tipos_Proveedor_FE"', 'join')
        ->join($this->table_tabla_dato . ' AS TIPOECOMMERCE', 'TIPOECOMMERCE.ID_Tabla_Dato=EMP.Nu_Tipo_Ecommerce_Empresa AND TIPOECOMMERCE.No_Relacion = "Tipos_Ecommerce_Empresa"', 'left')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = ' . $this->table . '.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_Estados"', 'join');
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $this->db->select('EMP.ID_Empresa, EMP.Nu_Tipo_Proveedor_FE, EMP.ID_Empresa_Marketplace, ORG.ID_Organizacion, ' . $this->table . '.*' );
        $this->db->from($this->table);
        $this->db->join($this->table_organizacion . ' AS ORG', 'ORG.ID_Organizacion = ' . $this->table . '.ID_Organizacion', 'join');
        $this->db->join($this->table_empresa . ' AS EMP', 'EMP.ID_Empresa = ORG.ID_Empresa', 'join');
        $this->db->where('ID_Almacen', $ID);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function agregarAlmacen($data){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Organizacion=" . $data['ID_Organizacion'] . " AND No_Almacen='" . $data['No_Almacen'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			if ( $this->db->insert($this->table, $data) > 0 )
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
		}
		return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
    }
    
    public function actualizarAlmacen($where, $data, $ENo_Almacen){
		if( $ENo_Almacen != $data['No_Almacen'] && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Organizacion=" . $data['ID_Organizacion'] . " AND No_Almacen='" . $data['No_Almacen'] . "' LIMIT 1")->row()->existe > 0 ){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
		    if ( $this->db->update($this->table, $data, $where) > 0 )
		        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
    }
    
	public function eliminarAlmacen($ID){
	    if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_documento_cabecera . " WHERE ID_Almacen = " . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El almacén tiene movimiento(s)');
		}else{
			$this->db->where('ID_Almacen', $ID);
            $this->db->delete($this->table);
            if ( $this->db->affected_rows() > 0 )
                return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
	}
}
