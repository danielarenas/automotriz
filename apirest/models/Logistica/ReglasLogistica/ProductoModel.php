<?php
class ProductoModel extends CI_Model{
	var $table = 'producto';
	var $table_stock_producto = 'stock_producto';
	var $table_enlace_producto = 'enlace_producto';
	var $table_tabla_dato = 'tabla_dato';
	var $table_familia = 'familia';
	var $table_subfamilia = 'subfamilia';
	var $table_impuesto = 'impuesto';
	var $table_impuesto_cruce_documento = 'impuesto_cruce_documento';
	var $table_marca = 'marca';
	var $table_unidad_medida = 'unidad_medida';
	var $table_documento_detalle = 'documento_detalle';
	var $table_lista_precio_detalle = 'lista_precio_detalle';
	var $table_configuracion_cuenta_contable_producto = 'configuracion_cuenta_contable_producto';
	var $column_order = ['No_Descripcion_Grupo', 'No_Unidad_Medida', 'No_Marca', 'No_Familia', 'Nu_Codigo_Barra', 'No_Producto', 'No_Impuesto_Breve', 'Ss_Precio', 'Ss_Costo', 'Qt_Producto'];
	var $column_search = [];
	var $order = ['No_Producto' => 'asc'];

	private $upload_path = '../assets/images/productos/';
	private $_batchImport;

	public function __construct(){
		parent::__construct();
	}

	public function setBatchImport($arrProducto) {
		$this->_batchImport = $arrProducto;
	}

	public function importData() {
		$ID_Empresa = $this->user->ID_Empresa;
		$ID_Ubicacion_Inventario = 1;
		$iIdTipoLavado = 0;
		$ID_Impuesto = 0;
		$ID_Marca = 0;
		$ID_Unidad_Medida = 0;
		$ID_Familia = 0;
		$ID_Sub_Familia = 0;
		$ID_Producto_Sunat = 0;
		$ID_Familia_Marketplace = 0;
		$ID_Sub_Familia_Marketplace = 0;
		$ID_Marca_Marketplace = 0;

		foreach ($this->_batchImport as $row) {
        	if ( !empty($row['No_Impuesto']) ) {
	       		if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_impuesto . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Impuesto='" . $row['No_Impuesto'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Impuesto = $this->db->query("SELECT ID_Impuesto FROM " . $this->table_impuesto . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Impuesto='" . $row['No_Impuesto'] . "' LIMIT 1")->row()->ID_Impuesto;
			}
			
        	if ( !empty($row['No_Marca']) ) {
	       		if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Marca='" . $row['No_Marca'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Marca = $this->db->query("SELECT ID_Marca FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Marca='" . $row['No_Marca'] . "' LIMIT 1")->row()->ID_Marca;
        	}
        	
        	if ( !empty($row['No_Unidad_Medida']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_unidad_medida . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Unidad_Medida='" . $row['No_Unidad_Medida'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Unidad_Medida = $this->db->query("SELECT ID_Unidad_Medida FROM " . $this->table_unidad_medida . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Unidad_Medida='" . $row['No_Unidad_Medida'] . "' LIMIT 1")->row()->ID_Unidad_Medida;
        	}
        	
        	if ( !empty($row['No_Familia']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Familia='" . $row['No_Familia'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Familia = $this->db->query("SELECT ID_Familia FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Familia= '" . $row['No_Familia'] . "' LIMIT 1")->row()->ID_Familia;
        	}
        	
        	if ( !empty($row['No_Sub_Familia']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Sub_Familia='" . $row['No_Sub_Familia'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Sub_Familia = $this->db->query("SELECT ID_Sub_Familia FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Sub_Familia= '" . $row['No_Sub_Familia'] . "' LIMIT 1")->row()->ID_Sub_Familia;
        	}
        	
        	if ( !empty($row['Nu_Codigo_Producto_Sunat']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Catalogo_Producto_Sunat' AND Nu_Valor='" . $row['Nu_Codigo_Producto_Sunat'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Producto_Sunat = $this->db->query("SELECT ID_Tabla_Dato FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Catalogo_Producto_Sunat' AND Nu_Valor='" . $row['Nu_Codigo_Producto_Sunat'] . "' LIMIT 1")->row()->ID_Tabla_Dato;
        	}
        	
        	if ( !empty($row['iTipoLavado']) ) {
				if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Tipos_PedidoLavado' AND Nu_Valor='" . $row['iTipoLavado'] . "' LIMIT 1")->row()->existe > 0)
					$iIdTipoLavado = $this->db->query("SELECT Nu_Valor FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Tipos_PedidoLavado' AND Nu_Valor='" . $row['iTipoLavado'] . "' LIMIT 1")->row()->Nu_Valor;
			}

        	if ( !empty($row['No_Familia_Marketplace']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Familia='" . $row['No_Familia_Marketplace'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Familia_Marketplace = $this->db->query("SELECT ID_Familia FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Familia= '" . $row['No_Familia_Marketplace'] . "' LIMIT 1")->row()->ID_Familia;
        	}
        	
        	if ( !empty($row['No_Sub_Familia_Marketplace']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Sub_Familia='" . $row['No_Sub_Familia_Marketplace'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Sub_Familia_Marketplace = $this->db->query("SELECT ID_Sub_Familia FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Sub_Familia= '" . $row['No_Sub_Familia_Marketplace'] . "' LIMIT 1")->row()->ID_Sub_Familia;
			}
        	
        	if ( !empty($row['No_Marca_Marketplace']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Marca='" . $row['No_Marca_Marketplace'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Marca_Marketplace = $this->db->query("SELECT ID_Marca FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Marca= '" . $row['No_Marca_Marketplace'] . "' LIMIT 1")->row()->ID_Marca;
			}
			
        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa=" . $ID_Empresa . " AND Nu_Codigo_Barra='" . $row['Nu_Codigo_Barra'] . "' LIMIT 1")->row()->existe == 0){
            	$_arrProducto = array(
					'ID_Empresa' => $ID_Empresa,
					'Nu_Tipo_Producto' => $row['Nu_Tipo_Producto'],
					'ID_Tipo_Producto' => $row['ID_Tipo_Producto'],
					'Nu_Codigo_Barra' => $row['Nu_Codigo_Barra'],
					'No_Codigo_Interno' => $row['Nu_Codigo_Producto'],
					'No_Producto' => $row['No_Producto'],
					'ID_Impuesto' => $ID_Impuesto,
					'ID_Ubicacion_Inventario' => $ID_Ubicacion_Inventario,
					'ID_Unidad_Medida' => $ID_Unidad_Medida,
					'ID_Producto_Sunat' => $ID_Producto_Sunat,
					'Nu_Compuesto' => 0,
					'Qt_CO2_Producto' => $row['Qt_CO2_Producto'],
					'Ss_Precio' => $row['fPrecio'],
					'Ss_Costo' => $row['fCosto'],
					'Nu_Stock_Minimo' => $row['iStockMinimo'],
					'ID_Tipo_Pedido_Lavado' => $iIdTipoLavado,
					'Nu_Estado' => (($row['iEstado'] != 0 || $row['iEstado'] == '' || $row['iEstado'] == NULL ) ? 1 : 0),
					'Ss_Precio_Ecommerce_Online_Regular' => $row['Ss_Precio_Ecommerce_Online_Regular'],
					'Ss_Precio_Ecommerce_Online' => $row['Ss_Precio_Ecommerce_Online'],
            	);
				if ( !empty($ID_Marca) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Marca" => $ID_Marca));
				if ( !empty($ID_Familia) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Familia" => $ID_Familia));
				if ( !empty($ID_Sub_Familia) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Sub_Familia" => $ID_Sub_Familia));
				if ( !empty($ID_Familia_Marketplace) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Familia_Marketplace" => $ID_Familia_Marketplace));
				if ( !empty($ID_Sub_Familia_Marketplace) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Sub_Familia_Marketplace" => $ID_Sub_Familia_Marketplace));
				if ( !empty($ID_Marca_Marketplace) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Marca_Marketplace" => $ID_Marca_Marketplace));
				$arrProducto[] = $_arrProducto;
        	} else {
        		$ID_Producto = $this->db->query("SELECT ID_Producto FROM " . $this->table . " WHERE ID_Empresa=" . $ID_Empresa . " AND Nu_Codigo_Barra='" . $row['Nu_Codigo_Barra'] . "' LIMIT 1")->row()->ID_Producto;
        		$_arrProductoUPD = array(
					'ID_Producto' => $ID_Producto,
					'ID_Empresa' => $ID_Empresa,
					'Nu_Tipo_Producto' => $row['Nu_Tipo_Producto'],
					'ID_Tipo_Producto' => $row['ID_Tipo_Producto'],
					'Nu_Codigo_Barra' => $row['Nu_Codigo_Barra'],
					'No_Codigo_Interno' => $row['Nu_Codigo_Producto'],
					'No_Producto' => $row['No_Producto'],
					'ID_Impuesto' => $ID_Impuesto,
					'ID_Ubicacion_Inventario' => $ID_Ubicacion_Inventario,
					'ID_Unidad_Medida' => $ID_Unidad_Medida,
					'ID_Producto_Sunat' => $ID_Producto_Sunat,
					'Nu_Compuesto' => 0,
					'Qt_CO2_Producto' => $row['Qt_CO2_Producto'],
					'Ss_Precio' => $row['fPrecio'],
					'Ss_Costo' => $row['fCosto'],
					'Nu_Stock_Minimo' => $row['iStockMinimo'],
					'ID_Tipo_Pedido_Lavado' => $iIdTipoLavado,
					'Nu_Estado' => (($row['iEstado'] != 0 || $row['iEstado'] == '' || $row['iEstado'] == NULL ) ? 1 : 0),
					'Ss_Precio_Ecommerce_Online_Regular' => $row['Ss_Precio_Ecommerce_Online_Regular'],
					'Ss_Precio_Ecommerce_Online' => $row['Ss_Precio_Ecommerce_Online'],
            	);
				if ( !empty($ID_Marca) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Marca" => $ID_Marca));
				if ( !empty($ID_Familia) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Familia" => $ID_Familia));
				if ( !empty($ID_Sub_Familia) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Sub_Familia" => $ID_Sub_Familia));
				if ( !empty($ID_Familia_Marketplace) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Familia_Marketplace" => $ID_Familia_Marketplace));
				if ( !empty($ID_Sub_Familia_Marketplace) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Sub_Familia_Marketplace" => $ID_Sub_Familia_Marketplace));
				if ( !empty($ID_Marca_Marketplace) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Marca_Marketplace" => $ID_Marca_Marketplace));
				$arrProductoUPD[] = $_arrProductoUPD;
        	}
        }

        $bStatus=false;
        if (isset($arrProducto) && is_array($arrProducto))
    		$this->db->insert_batch($this->table, $arrProducto);
    		if ($this->db->affected_rows() > 0)
    			$bStatus = true;
    	if (isset($arrProductoUPD) && is_array($arrProductoUPD))
    		$this->db->update_batch($this->table, $arrProductoUPD, 'ID_Producto');
    		if ($this->db->affected_rows() > 0)
    			$bStatus = true;

    	unset($arrProducto);
    	unset($arrProductoUPD);
    	return $bStatus;
	}

	public function _get_datatables_query(){
		if( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Producto' ){
			$this->db->like('No_Producto', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'CodigoBarra' ){
			$this->db->like('Nu_Codigo_Barra', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Grupo' ){
			$this->db->like('GRUPOITEM.No_Descripcion', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Area' ){
			$this->db->like('AP.No_Descripcion', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Categoria' ){
			$this->db->like('F.No_Familia', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'UnidadMedida' ){
			$this->db->like('UM.No_Unidad_Medida', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Marca' ){
			$this->db->like('M.No_Marca', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Impuesto' ){
			$this->db->like('IMP.No_Impuesto_Breve', $this->input->post('Global_Filter'));
		} else if ( trim($this->input->post('Global_Filter')) != "" && $this->input->post('Filtros_Productos') == 'Stock' ){
			$this->db->where('STOCK.Qt_Producto', intval($this->input->post('Global_Filter')));
		}

		$this->db->select('PRO.ID_Empresa, PRO.Nu_Stock_Minimo, PRO.Txt_Ubicacion_Producto_Tienda, PRO.ID_Producto, Nu_Codigo_Barra, No_Producto, STOCK.Qt_Producto AS Qt_Producto, Ss_Precio, Nu_Compuesto, No_Imagen_Item, PRO.Nu_Version_Imagen, AP.No_Descripcion AS No_Area_Almacen, GRUPOITEM.No_Descripcion AS No_Descripcion_Grupo, IMP.No_Impuesto_Breve, PRO.Ss_Costo, F.No_Familia, SF.No_Sub_Familia, UM.No_Unidad_Medida, M.No_Marca')

		->from($this->table . ' AS PRO')
		->join($this->table_stock_producto . ' AS STOCK', 'STOCK.ID_Organizacion = ' . $this->empresa->ID_Organizacion . ' AND STOCK.ID_Almacen = ' . $this->empresa->ID_Almacen . ' AND STOCK.ID_Producto = PRO.ID_Producto', 'left')
		->join($this->table_tabla_dato . ' AS GRUPOITEM', 'GRUPOITEM.Nu_Valor = PRO.Nu_Tipo_Producto AND GRUPOITEM.No_Relacion = "Tipos_Item"', 'join')
		->join('tabla_dato AS AP', 'AP.Nu_Valor = PRO.Nu_Area_Almacen AND AP.No_Relacion = "Area_de_Almacen"', 'left')
		->join($this->table_impuesto . ' AS IMP', 'IMP.ID_Impuesto = PRO.ID_Impuesto', 'join')
		->join($this->table_familia . ' AS F', 'F.ID_Familia = PRO.ID_Familia', 'left')
		->join($this->table_subfamilia . ' AS SF', 'SF.ID_Familia = PRO.ID_Familia AND SF.ID_Sub_Familia = PRO.ID_Sub_Familia', 'left')
		->join($this->table_unidad_medida . ' AS UM', 'UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida', 'join')
		->join($this->table_marca . ' AS M', 'M.ID_Marca = PRO.ID_Marca', 'left')
		->where('PRO.ID_Empresa', $this->empresa->ID_Empresa)
		->where_in('PRO.Nu_Tipo_Producto', array('0','1','2'));

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if(isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables(){
		$this->_get_datatables_query();
		if($_POST['length'] != -1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered(){
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all(){
        if( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Producto' ){
            $this->db->like('No_Producto', $this->input->post('Global_Filter'));
        } else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'CodigoBarra' ){
        	$this->db->like('Nu_Codigo_Barra', $this->input->post('Global_Filter'));
        } else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Grupo' ){
        	$this->db->like('GRUPOITEM.No_Descripcion', $this->input->post('Global_Filter'));
        } else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Categoria' ){
        	$this->db->like('F.No_Familia', $this->input->post('Global_Filter'));
        } else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'UnidadMedida' ){
        	$this->db->like('UM.No_Unidad_Medida', $this->input->post('Global_Filter'));
        } else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Marca' ){
        	$this->db->like('M.No_Marca', $this->input->post('Global_Filter'));
		} else if ( !empty($this->input->post('Global_Filter')) && $this->input->post('Filtros_Productos') == 'Impuesto' ){
        	$this->db->like('IMP.No_Impuesto_Breve', $this->input->post('Global_Filter'));
		}  else if ( trim($this->input->post('Global_Filter')) != "" && $this->input->post('Filtros_Productos') == 'Stock' ){
        	$this->db->where('STOCK.Qt_Producto', intval($this->input->post('Global_Filter')));
		}

		//$this->db->select('PRO.ID_Empresa, PRO.ID_Producto, Nu_Codigo_Barra, No_Producto, STOCK.Qt_Producto AS Qt_Producto, Ss_Precio, Nu_Compuesto, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, No_Imagen_Item, PRO.Nu_Version_Imagen, GRUPOITEM.No_Descripcion AS No_Descripcion_Grupo, IMP.No_Impuesto_Breve, PRO.Ss_Costo, F.No_Familia, UM.No_Unidad_Medida, M.No_Marca')
		$this->db->select('PRO.ID_Empresa, PRO.ID_Producto, Nu_Codigo_Barra, No_Producto, STOCK.Qt_Producto AS Qt_Producto, Ss_Precio, Nu_Compuesto, No_Imagen_Item, PRO.Nu_Version_Imagen, GRUPOITEM.No_Descripcion AS No_Descripcion_Grupo, IMP.No_Impuesto_Breve, PRO.Ss_Costo, F.No_Familia, UM.No_Unidad_Medida, M.No_Marca')
		->from($this->table . ' AS PRO')
		->join($this->table_stock_producto . ' AS STOCK', 'STOCK.ID_Organizacion = ' . $this->empresa->ID_Organizacion . ' AND STOCK.ID_Almacen = ' . $this->empresa->ID_Almacen . ' AND STOCK.ID_Producto = PRO.ID_Producto', 'left')
		//->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = PRO.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_Estados"', 'join')
		->join($this->table_tabla_dato . ' AS GRUPOITEM', 'GRUPOITEM.Nu_Valor = PRO.Nu_Tipo_Producto AND GRUPOITEM.No_Relacion = "Tipos_Item"', 'join')
		->join($this->table_impuesto . ' AS IMP', 'IMP.ID_Impuesto = PRO.ID_Impuesto', 'join')
		->join($this->table_familia . ' AS F', 'F.ID_Familia = PRO.ID_Familia', 'left')
		->join($this->table_unidad_medida . ' AS UM', 'UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida', 'join')
		->join($this->table_marca . ' AS M', 'M.ID_Marca = PRO.ID_Marca', 'left')
        ->where('PRO.ID_Empresa', $this->empresa->ID_Empresa)
    	->where_in('PRO.Nu_Tipo_Producto', array('0','1','2'));
		
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
    	$this->db->select('PRO.*, ITEMSUNAT.ID_Tabla_Dato AS ID_Producto_Sunat, ITEMSUNAT.No_Descripcion AS No_Producto_Sunat');
        $this->db->from($this->table . ' AS PRO');
        $this->db->join($this->table_tabla_dato . ' AS ITEMSUNAT', 'ITEMSUNAT.ID_Tabla_Dato = PRO.ID_Producto_Sunat AND ITEMSUNAT.No_Relacion="Catalogo_Producto_Sunat"', 'left');
        $this->db->where('PRO.ID_Producto',$ID);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function get_by_id_enlace($ID){
        $query = "
SELECT
 ENLAPRO.ID_Producto,
 PRO.Nu_Codigo_Barra,
 PRO.No_Producto,
 ENLAPRO.Qt_Producto_Descargar
FROM
 " . $this->table_enlace_producto . " AS ENLAPRO
 JOIN " . $this->table . " AS PRO ON(PRO.ID_Producto = ENLAPRO.ID_Producto)
WHERE
 ENLAPRO.ID_Producto_Enlace = " . $ID;
        return $this->db->query($query)->result();
    }

	public function agregarProducto($data, $data_enlace)
	{
		if($data['Nu_Codigo_Barra'] != '' && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa=" . $data['ID_Empresa'] . " AND Nu_Codigo_Barra='" . $data['Nu_Codigo_Barra'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		} else {
			$this->db->trans_begin();
			$this->db->insert($this->table, $data);
			$Last_ID_Producto = $this->db->insert_id();
			if ($data['Nu_Compuesto'] == 1){
				for($i = 0; $i < count($data_enlace); $i++) {
					$enlace_producto[] = array(
						'ID_Producto' => $this->security->xss_clean($data_enlace[$i]['ID_Producto_Enlace']),
						'ID_Producto_Enlace' => $Last_ID_Producto,
						'Qt_Producto_Descargar'	=> $this->security->xss_clean($data_enlace[$i]['Qt_Producto_Descargar']),
					);
				}
				$this->db->insert_batch($this->table_enlace_producto, $enlace_producto);
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
			} else {
				$this->db->trans_commit();
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado', 'iIDItem' => $Last_ID_Producto);
			}
		}
	}

	public function actualizarProducto($where, $data, $ENu_Codigo_Barra, $data_enlace)
	{
		if($ENu_Codigo_Barra != $data['Nu_Codigo_Barra'] && $this->db->query("SELECT COUNT(*) existe FROM " . $this->table . " WHERE ID_Empresa=" . $data['ID_Empresa'] . " AND Nu_Codigo_Barra='" . $data['Nu_Codigo_Barra'] . "' LIMIT 1")->row()->existe > 0){
			return ['status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe'];
		} else {
			$this->db->trans_begin();
			$this->db->update($this->table, $data, $where);
			$this->db->where('ID_Producto_Enlace', $where['ID_Producto']);
			$this->db->delete($this->table_enlace_producto);
			if ($data['Nu_Compuesto'] == 1) {
				for($i = 0; $i < count($data_enlace); $i++){
					$enlace_producto[] = [
						'ID_Producto'	=> $this->security->xss_clean($data_enlace[$i]['ID_Producto_Enlace']),
						'ID_Producto_Enlace' => $where['ID_Producto'],
						'Qt_Producto_Descargar'	=> $this->security->xss_clean($data_enlace[$i]['Qt_Producto_Descargar']),
					];
				}
				$this->db->insert_batch($this->table_enlace_producto, $enlace_producto);
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return ['status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar'];
			} else {
				$this->db->trans_commit();
				return ['status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado', 'iIDItem' => $where['ID_Producto']];
			}
		}
	}

	public function eliminarProducto($ID_Empresa, $ID, $Nu_Codigo_Barra, $Nu_Compuesto, $sNombreImagenItem){
		if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_documento_detalle . " WHERE ID_Empresa=" . $ID_Empresa . " AND ID_Producto=" . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El producto tiene movimiento(s)');
		} else if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->configuracion_cuenta_contable_producto . " WHERE ID_Producto=" . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El producto tiene cuenta contable');
		} else if ($this->db->query("
SELECT
 COUNT(*) AS existe
FROM
 " . $this->table_lista_precio_detalle . " AS LPD
 JOIN lista_precio_cabecera AS LPC ON(LPC.ID_Lista_Precio_Cabecera = LPD.ID_Lista_Precio_Cabecera)
WHERE
 LPC.ID_Empresa = " . $ID_Empresa . "
 AND LPD.ID_Producto = " . $ID . "
LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El producto tiene precio(s) asignados');
		} else {
			$this->db->trans_begin();
			
			$sUrlImage = $this->db->query("SELECT No_Imagen_Item FROM producto WHERE ID_Producto=" . $ID . " LIMIT 1")->row()->No_Imagen_Item;

			$this->db->where('ID_Producto_Enlace', $ID);
            $this->db->delete($this->table_enlace_producto);
            
			$this->db->where('ID_Producto', $ID);
            $this->db->delete($this->table);
            
        	$this->db->trans_complete();
	        if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al eliminar');
	        } else {
	            $this->db->trans_commit();
				
				if ( !empty($sUrlImage) ) {
					$arrUrlImage = explode($this->empresa->Nu_Documento_Identidad, $sUrlImage);
					$path = $this->upload_path . $this->empresa->Nu_Documento_Identidad . $arrUrlImage[1];
					if ( file_exists($path) )
						unlink($path);
				}
	            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
	        }
		}
	}

    public function actualizarVersionImagen($where, $data){
        if ( $this->db->update($this->table, $data, $where) > 0 )
            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Version de imagen modificada');
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error Version de imagen modificada');
    }
}