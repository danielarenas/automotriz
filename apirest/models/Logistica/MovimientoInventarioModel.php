<?php
class MovimientoInventarioModel extends CI_Model{
    public function crudMovimientoInventario($ID_Almacen, $ID_Documento_Cabecera, $ID_Guia_Cabecera, $arrDetalle, $ID_Tipo_Movimiento, $iAgregar_Modificar, $arrWhere, $Nu_Tipo_Movimiento, $iGeneraStock){
		$this->db->trans_begin(); 

		//Validar producto cambiado por otro y lo actualizamos de la tabla stock_producto
		if ($iAgregar_Modificar == 1) {//1 = Modificar - Solo sin son documentos de compra, venta o guía
			$ID_Documento_Guia = (isset($arrWhere['ID_Documento_Cabecera']) ? $arrWhere['ID_Documento_Cabecera'] : $arrWhere['ID_Guia_Cabecera'] );
			if (!empty($ID_Documento_Guia)) {// Si guia no esta vacia
				$query = "
SELECT
 MI.ID_Empresa,
 MI.ID_Organizacion,
 MI.ID_Almacen,
 MI.ID_Producto,
 MI.Qt_Producto,
 PROD.Nu_Compuesto
FROM
 movimiento_inventario AS MI
 JOIN producto AS PROD ON(PROD.ID_Producto = MI.ID_Producto)
WHERE
 MI.ID_Documento_Cabecera = " . $ID_Documento_Guia . " OR MI.ID_Guia_Cabecera = " . $ID_Documento_Guia;
				$arrDetalleAnterior = $this->db->query($query)->result();
				
				foreach ($arrDetalleAnterior as $row) {
					if ($row->Nu_Compuesto == '0'){
						$Qt_Producto_Actual = $this->db->query("
SELECT
 Qt_Producto
FROM
 stock_producto
WHERE
 ID_Empresa = " . $row->ID_Empresa . "
 AND ID_Organizacion = " . $row->ID_Organizacion . "
 AND ID_Almacen = " . $row->ID_Almacen . "
 AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
						settype($Qt_Producto_Actual, "double");
						
						$where_stock_producto = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
						$stock_producto_anterior = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> $Qt_Producto_Actual - $row->Qt_Producto,
							'Ss_Costo_Promedio'	=> 0,
						);
						$this->db->update('stock_producto', $stock_producto_anterior, $where_stock_producto);
					} else {
				        $query = "
SELECT
 ENLAPRO.ID_Producto,
 ENLAPRO.Qt_Producto_Descargar
FROM
 enlace_producto AS ENLAPRO
 JOIN producto AS PROD ON(PROD.ID_Producto = ENLAPRO.ID_Producto)
WHERE
 ENLAPRO.ID_Producto_Enlace = " . $row->ID_Producto;
				        $arrItems = $this->db->query($query)->result();
				        
						foreach ($arrItems as $row_enlace) {
							$Qt_Producto_Actual = $this->db->query("
SELECT
 Qt_Producto
FROM
 stock_producto
WHERE
 ID_Empresa = " . $row->ID_Empresa . "
 AND ID_Almacen = " . $row->ID_Almacen . "
 AND ID_Producto = " . $row_enlace->ID_Producto)->row()->Qt_Producto;
							settype($Qt_Producto_Actual, "double");
							
							$where_stock_producto = array('ID_Empresa' => $row->ID_Empresa, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row_enlace->ID_Producto);
							$stock_producto_anterior = array(
								'ID_Producto'		=> $row_enlace->ID_Producto,
								'Qt_Producto'		=> $Qt_Producto_Actual - ($row->Qt_Producto * $row_enlace->Qt_Producto_Descargar),
								'Ss_Costo_Promedio'	=> 0,
							);
							$this->db->update('stock_producto', $stock_producto_anterior, $where_stock_producto);
						}
					}
				}
	        	$this->db->delete('movimiento_inventario', $arrWhere);
			} // ./ if Si guia no esta vacia
		} // ./ if Si se modifico registro
		
		// foreach - Registrar movimiento de inventario por ITEM NORMAL e ITEM PROMOCION
		foreach ($arrDetalle as $row) {
			$ID_Producto = $this->security->xss_clean($row['ID_Producto']);
			
			$arrDataItem = $this->db->query("SELECT Nu_Tipo_Producto, Nu_Compuesto FROM producto WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Producto = " . $ID_Producto . " LIMIT 1")->result();
			$Nu_Tipo_Producto = $arrDataItem[0]->Nu_Tipo_Producto;
			$Nu_Compuesto = $arrDataItem[0]->Nu_Compuesto;
			
			if ( $Nu_Tipo_Producto != '0' ) {
				if ($Nu_Compuesto == 0){		
					$_movimiento_inventario = array(
						'ID_Empresa'			=> $this->empresa->ID_Empresa,
						'ID_Organizacion'		=> $this->empresa->ID_Organizacion,
						'ID_Almacen'			=> $this->empresa->ID_Almacen,
						'ID_Tipo_Movimiento'	=> $ID_Tipo_Movimiento,
						'ID_Producto'			=> $ID_Producto,
						'Qt_Producto'			=> round($this->security->xss_clean($row['Qt_Producto']), 6),
						'Ss_Precio'				=> round($this->security->xss_clean($row['Ss_Precio']), 6),
						'Ss_SubTotal' 			=> round($this->security->xss_clean($row['Qt_Producto'] * $this->security->xss_clean($row['Ss_Precio'])), 2),
						'Ss_Costo_Promedio'		=> 0,
					);
					if ( !empty($ID_Documento_Cabecera) )
						$_movimiento_inventario = array_merge($_movimiento_inventario, array('ID_Documento_Cabecera' => $ID_Documento_Cabecera));
					if ( !empty($ID_Guia_Cabecera) )
						$_movimiento_inventario = array_merge($_movimiento_inventario, array('ID_Guia_Cabecera' => $ID_Guia_Cabecera));
					$movimiento_inventario[] = $_movimiento_inventario;
				} else {
		        	$query = "
SELECT
 ENLAPRO.ID_Producto,
 ENLAPRO.Qt_Producto_Descargar
FROM
 enlace_producto AS ENLAPRO
 JOIN producto AS PROD ON(PROD.ID_Producto = ENLAPRO.ID_Producto)
WHERE
 ENLAPRO.ID_Producto_Enlace = " . $ID_Producto;
					$arrItems = $this->db->query($query)->result();
					
					foreach ($arrItems as $row_enlace) {
						$_movimiento_inventario = array(
							'ID_Empresa'			=> $this->empresa->ID_Empresa,
							'ID_Organizacion'		=> $this->empresa->ID_Organizacion,
							'ID_Almacen'			=> $this->empresa->ID_Almacen,
							'ID_Tipo_Movimiento'	=> $ID_Tipo_Movimiento,
							'ID_Producto'			=> $row_enlace->ID_Producto,
							'Qt_Producto'			=> round($this->security->xss_clean($row['Qt_Producto'] * $row_enlace->Qt_Producto_Descargar), 6),
							'Ss_Precio'				=> round($this->security->xss_clean($row['Ss_Precio']), 6),
							'Ss_SubTotal' 			=> round($this->security->xss_clean(($row['Qt_Producto'] * $row_enlace->Qt_Producto_Descargar) * $this->security->xss_clean($row['Ss_Precio'])), 2),
							'Ss_Costo_Promedio'		=> 0,
						);
						if ( !empty($ID_Documento_Cabecera) )
							$_movimiento_inventario = array_merge($_movimiento_inventario, array('ID_Documento_Cabecera' => $ID_Documento_Cabecera));
						if ( !empty($ID_Guia_Cabecera) )
							$_movimiento_inventario = array_merge($_movimiento_inventario, array('ID_Guia_Cabecera' => $ID_Guia_Cabecera));
						$movimiento_inventario[] = $_movimiento_inventario;
					}// ./ foreach generar movimiento de inventario
				}// ./ if - else ITEM NORMAL e ITEM PROMOCION 
			}// /. validando tipo de item 0 = Servicio
		}// ./ foreach - Registrar movimiento de inventario por ITEM NORMAL e ITEM PROMOCION

		if (isset($movimiento_inventario))
			$this->db->insert_batch('movimiento_inventario', $movimiento_inventario);
		
		// foreach - Actualizar cantidad tabla stock_producto
		foreach ($arrDetalle as $row) {
			$ID_Producto = $this->security->xss_clean($row['ID_Producto']);
			
			if ($iGeneraStock == 1) {//tabla stock_producto
				if ( $Nu_Tipo_Producto != '0' ) {
					if ($Nu_Compuesto == 0){
						if ($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND ID_Almacen = " . $this->empresa->ID_Almacen . " AND ID_Producto = " . $ID_Producto . " LIMIT 1")->row()->existe > 0){
							$where_stock_producto = array('ID_Empresa' => $this->empresa->ID_Empresa, 'ID_Organizacion' => $this->empresa->ID_Organizacion, 'ID_Almacen' => $this->empresa->ID_Almacen, 'ID_Producto' => $ID_Producto);
						
							// STOCK_PROD_UPDT_3 - INICIO 

								// saldo inicial
								$Saldo_Inicial = $this->db->query("SELECT
								SUM(K.Qt_Producto) as Qt_Producto
								FROM
								movimiento_inventario AS K
								JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
								JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
								JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
								WHERE
								K.ID_Empresa =  " . $this->empresa->ID_Empresa  . "  AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion   . " AND K.ID_Almacen = " . $this->empresa->ID_Almacen . "  
								and TMOVI.ID_Tipo_Movimiento in (2,7)
								AND PRO.Nu_Tipo_Producto = 1 AND CVCAB.ID_Tipo_Documento != 20
								AND PRO.ID_Producto = ". $ID_Producto . " "   )->row()->Qt_Producto;
								if ( empty($Saldo_Inicial) ) {
									$Saldo_Inicial = 0;
								} 
								settype($Saldo_Inicial, "double");
									

								// salida 
								$Qt_Producto_Salida = $this->db->query("
								SELECT
								SUM(K.Qt_Producto) as Qt_Producto
								FROM
								movimiento_inventario AS K
								JOIN guia_cabecera AS GESCAB ON(K.ID_Guia_Cabecera = GESCAB.ID_Guia_Cabecera)
								JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
								JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
								WHERE
								K.ID_Empresa = " . $this->empresa->ID_Empresa . " AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion   . " 
								AND K.ID_Almacen = " . $this->empresa->ID_Almacen . " and TMOVI.ID_Tipo_Movimiento   not in (2,7)   
								AND PRO.Nu_Tipo_Producto = 1 AND GESCAB.ID_Tipo_Documento != 20
								AND PRO.ID_Producto= ".$ID_Producto. " " )->row()->Qt_Producto;
								if ( empty($Qt_Producto_Salida) ) {
									$Qt_Producto_Salida = 0;
								} 
								settype($Qt_Producto_Salida, "double");
												

								// entrada 
								$Qt_Producto_Entrada = $this->db->query("
								SELECT
								SUM(K.Qt_Producto) AS Qt_Producto
								FROM
								movimiento_inventario AS K
								JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
								JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
								JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
								WHERE
								K.ID_Empresa = " . $this->empresa->ID_Empresa . " AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion   . " 
								AND K.ID_Almacen = " . $this->empresa->ID_Almacen . " and TMOVI.ID_Tipo_Movimiento   not in (2,7)   
								AND PRO.Nu_Tipo_Producto = 1 AND CVCAB.ID_Tipo_Documento != 20
								AND PRO.ID_Producto = ".$ID_Producto. " " )->row()->Qt_Producto;
								if ( empty($Qt_Producto_Entrada) ) {
									$Qt_Producto_Entrada = 0;
								} 
								settype($Qt_Producto_Entrada, "double");
								


							/*
							$Qt_Producto_Salida = $this->db->query("
SELECT
 SUM(K.Qt_Producto) AS Qt_Producto
FROM
 movimiento_inventario AS K
 JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
 K.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND K.ID_Almacen = " . $this->empresa->ID_Almacen . "
 AND K.ID_Producto = " . $ID_Producto . "
 AND TMOVI.Nu_Tipo_Movimiento = 1")->row()->Qt_Producto;
							settype($Qt_Producto_Salida, "double");
						
							$Qt_Producto_Entrada = $this->db->query("
SELECT
 SUM(K.Qt_Producto) AS Qt_Producto
FROM
 movimiento_inventario AS K
 JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
 K.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND K.ID_Almacen = " . $this->empresa->ID_Almacen . "
 AND K.ID_Producto = " . $ID_Producto . "
 AND TMOVI.Nu_Tipo_Movimiento = 0")->row()->Qt_Producto;
							settype($Qt_Producto_Entrada, "double");
							*/
							
							// STOCK_PROD_UPDT_3 - FIN 


							// STOCK_PROD_UPDT_3 - POR CORREGIR - 1 
							$stock_producto = array(
								'ID_Producto'		=> $ID_Producto,
								// STOCK_PROD_UPDT_3 - INICIO 
									 'Qt_Producto'		=>  ( $Saldo_Inicial +  $Qt_Producto_Entrada ) - $Qt_Producto_Salida   ,
								// STOCK_PROD_UPDT_3 - FIN 
								'Ss_Costo_Promedio'	=> 0,
							);
							$this->db->update('stock_producto', $stock_producto, $where_stock_producto);
						} else {
							$stock_producto = array(
								'ID_Empresa'		=> $this->empresa->ID_Empresa,
								'ID_Organizacion'	=> $this->empresa->ID_Organizacion,
								'ID_Almacen'		=> $this->empresa->ID_Almacen,
								'ID_Producto'		=> $ID_Producto,
								'Qt_Producto'		=> $ID_Tipo_Movimiento == 1 ? - round($this->security->xss_clean($row['Qt_Producto']), 6) : round($this->security->xss_clean($row['Qt_Producto']), 6),
								'Ss_Costo_Promedio'	=> 0,
							);
							$this->db->insert('stock_producto', $stock_producto);
						}
					} else {
		    			$query = "
SELECT
 ENLAPRO.ID_Producto
FROM
 enlace_producto AS ENLAPRO
 JOIN producto AS PROD ON(PROD.ID_Producto = ENLAPRO.ID_Producto)
WHERE
 ENLAPRO.ID_Producto_Enlace = " . $ID_Producto;
						$arrItems = $this->db->query($query)->result();
						
						foreach ($arrItems as $row_enlace) {
							$ID_Producto = $row_enlace->ID_Producto;
							if ($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND ID_Almacen = " . $this->empresa->ID_Almacen . " AND ID_Producto = " . $ID_Producto . " LIMIT 1")->row()->existe > 0){
								$where_stock_producto = array('ID_Empresa' => $this->empresa->ID_Empresa, 'ID_Organizacion' => $this->empresa->ID_Organizacion, 'ID_Almacen' => $this->empresa->ID_Almacen, 'ID_Producto' => $ID_Producto);
								
								// STOCK_PROD_UPDT_3 - INICIO 

								// saldo inicial
								$Saldo_Inicial = $this->db->query("SELECT
								SUM(K.Qt_Producto) as Qt_Producto
								FROM
								movimiento_inventario AS K
								JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
								JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
								JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
								WHERE
								K.ID_Empresa =  " . $this->empresa->ID_Empresa  . "  AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion   . " AND K.ID_Almacen = " . $this->empresa->ID_Almacen . "  
								and TMOVI.ID_Tipo_Movimiento in (2,7)
								AND PRO.Nu_Tipo_Producto = 1 AND CVCAB.ID_Tipo_Documento != 20
								AND PRO.ID_Producto = ". $ID_Producto . " "   )->row()->Qt_Producto;
								if ( empty($Saldo_Inicial) ) {
									$Saldo_Inicial = 0;
								} 
								settype($Saldo_Inicial, "double");
									

								// salida 
								$Qt_Producto_Salida = $this->db->query("
								SELECT
								SUM(K.Qt_Producto) as Qt_Producto
								FROM
								movimiento_inventario AS K
								JOIN guia_cabecera AS GESCAB ON(K.ID_Guia_Cabecera = GESCAB.ID_Guia_Cabecera)
								JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
								JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
								WHERE
								K.ID_Empresa = " . $this->empresa->ID_Empresa . " AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion   . " 
								AND K.ID_Almacen = " . $this->empresa->ID_Almacen . " and TMOVI.ID_Tipo_Movimiento   not in (2,7)   
								AND PRO.Nu_Tipo_Producto = 1 AND GESCAB.ID_Tipo_Documento != 20
								AND PRO.ID_Producto= ".$ID_Producto. " " )->row()->Qt_Producto;
								if ( empty($Qt_Producto_Salida) ) {
									$Qt_Producto_Salida = 0;
								}
								settype($Qt_Producto_Salida, "double");
												

								// entrada 
								$Qt_Producto_Entrada = $this->db->query("
								SELECT
								SUM(K.Qt_Producto) AS Qt_Producto
								FROM
								movimiento_inventario AS K
								JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
								JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
								JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
								WHERE
								K.ID_Empresa = " . $this->empresa->ID_Empresa . " AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion   . " 
								AND K.ID_Almacen = " . $this->empresa->ID_Almacen . " and TMOVI.ID_Tipo_Movimiento   not in (2,7)   
								AND PRO.Nu_Tipo_Producto = 1 AND CVCAB.ID_Tipo_Documento != 20
								AND PRO.ID_Producto = ".$ID_Producto. " " )->row()->Qt_Producto;
								if ( empty($Qt_Producto_Entrada) ) {
									$Qt_Producto_Entrada = 0;
								} 
								settype($Qt_Producto_Entrada, "double");
								

								

								/* 
								$Qt_Producto_Salida = $this->db->query("
SELECT
 SUM(K.Qt_Producto) AS Qt_Producto
FROM
 movimiento_inventario AS K
 JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
 K.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND K.ID_Almacen = " . $this->empresa->ID_Almacen . "
 AND K.ID_Producto = " . $ID_Producto . "
 AND TMOVI.Nu_Tipo_Movimiento = 1")->row()->Qt_Producto;
								settype($Qt_Producto_Salida, "double");
								
								$Qt_Producto_Entrada = $this->db->query("
SELECT
 SUM(K.Qt_Producto) AS Qt_Producto
FROM
 movimiento_inventario AS K
 JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
 K.ID_Empresa = " . $this->user->ID_Empresa . "
 AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND K.ID_Almacen = " . $this->empresa->ID_Almacen . "
 AND K.ID_Producto = " . $ID_Producto . "
 AND TMOVI.Nu_Tipo_Movimiento = 0")->row()->Qt_Producto;
							settype($Qt_Producto_Entrada, "double");
							*/

							// STOCK_PROD_UPDT_3 - FIN 

							// STOCK_PROD_UPDT_3 - POR CORREGIR - 2 
								$stock_producto = array(
									'ID_Producto'		=> $ID_Producto,
									// STOCK_PROD_UPDT_3 - INICIO 
									'Qt_Producto'		=>  ( $Saldo_Inicial +  $Qt_Producto_Entrada ) - $Qt_Producto_Salida   ,
									// STOCK_PROD_UPDT_3 - FIN 
									'Ss_Costo_Promedio'	=> 0,
								);
								$this->db->update('stock_producto', $stock_producto, $where_stock_producto);
							} else {
								$stock_producto = array(
									'ID_Empresa'		=> $this->empresa->ID_Empresa,
									'ID_Organizacion'	=> $this->empresa->ID_Organizacion,
									'ID_Almacen'		=> $this->empresa->ID_Almacen,
									'ID_Producto'		=> $ID_Producto,
									'Qt_Producto'		=> $ID_Tipo_Movimiento == 1 ? - round($this->security->xss_clean($row['Qt_Producto']), 6) : round($this->security->xss_clean($row['Qt_Producto']), 6),
									'Ss_Costo_Promedio'	=> 0,
								);
								$this->db->insert('stock_producto', $stock_producto);
							}
						} // /. foreach calcular stock por enlaces de item
					} // /. validacion item si es compuesto
				} // /. validando tipo de item 0 = Servicio
			} // /. genera stock value de function controller
		}// foreach - Actualizar cantidad tabla stock_producto
		
		$mensaje = $iAgregar_Modificar == 1 ? 'Modificado' : 'Agregado';
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array(
				'status' => 'error',
				'style_modal' => 'modal-danger',
				'message' => 'Error al ' . $mensaje,
				'sStatus' => 'danger',
				'sMessage' => 'Error al ' . $mensaje,
			);
        } else {
            $this->db->trans_commit();
            return array(
				'status' => 'success',
				'style_modal' => 'modal-success',
				'message' => 'Registro ' . $mensaje,
				'sEnviarSunatAutomatic' => 'No',
				'sStatus' => 'success',
				'sMessage' => 'Venta completada',
				'iIdDocumentoCabecera' => $ID_Documento_Cabecera
			);
        }
    }
}