<?php
class CompraModel extends CI_Model{
	var $table = 'documento_cabecera';
	var $table_documento_detalle = 'documento_detalle';
	var $table_documento_detalle_lote	= 'documento_detalle_lote';
	var $table_documento_enlace	= 'documento_enlace';
	var $table_tipo_documento	= 'tipo_documento';
	var $table_impuesto_cruce_documento	= 'impuesto_cruce_documento';
	var $table_entidad = 'entidad';
	var $table_tipo_documento_identidad	= 'tipo_documento_identidad';
	var $table_moneda = 'moneda';
	var $table_organizacion = 'organizacion';
	var $table_tabla_dato = 'tabla_dato';
	var $table_producto = 'producto';
	var $table_almacen = 'almacen';
	var $table_configuracion_cuenta_contable_producto = 'configuracion_cuenta_contable_producto';
	var $table_configuracion_cuenta_contable = 'configuracion_cuenta_contable';
	var $table_medio_pago	= 'medio_pago';
	var $table_orden_ingreso = 'orden_ingreso';
	var $table_serie_documento = 'serie_documento';

	var $column_order = array('');
	var $column_search = array('');
	var $order = array('');

	public function __construct(){
		parent::__construct();
	}

	public function _get_asiento_query(){
		$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
		if($this->input->post('Filtro_TiposDocumento')) {
			$this->db->where('VC.ID_Tipo_Documento', $this->input->post('Filtro_TiposDocumento'));
		}
		if($this->input->post('Filtro_SerieDocumento')) {
			$this->db->where('VC.ID_Serie_Documento', $this->input->post('Filtro_SerieDocumento'));
		}
		if($this->input->post('Filtro_NumeroDocumento')) {
			$this->db->where('VC.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento'));
		}
		if($this->input->post('Filtro_Nu_Correlativo')) {
			$this->db->where('VC.Nu_Correlativo_Compra', $this->input->post('Filtro_Nu_Correlativo'));
		}
		if($this->input->post('Filtro_Estado') != '') {
			$this->db->where('VC.Nu_Estado', $this->input->post('Filtro_Estado'));
		}
		if($this->input->post('Filtro_Entidad')) {
			$this->db->where('PROVE.No_Entidad', $this->input->post('Filtro_Entidad'));
		}

		$this->db->select('VC.ID_Tipo_Documento as IDTipo_Documento,MP.No_Medio_Pago,TDOCU_ORI.centro_costo,CC.Nu_Cuenta_Contable,AL.No_Almacen,PO.NO_Producto,PO.Nu_Codigo_Barra,VD.Ss_SubTotal,CONCAT(SERIE.ID_Serie_Documento,"",ORI.ID_Numero_Documento) as ID_OI_Detalle,VD.Ss_Impuesto,VD.Ss_Total as Ss_TotalD,VD.Qt_Producto,VC.Nu_Correlativo_Compra, VC.ID_Documento_Cabecera, VC.Fe_Emision, VC.Fe_Vencimiento, TDOCU.No_Tipo_Documento_Breve, VC.ID_Serie_Documento, VC.ID_Numero_Documento, TDOCUIDEN.No_Tipo_Documento_Identidad_Breve, PROVE.Nu_Documento_Identidad, PROVE.No_Entidad, MONE.No_Signo, ROUND(VC.Ss_Total, 2) AS Ss_Total, VC.Nu_Estado, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, TDOCU.Nu_Enlace, VC.Nu_Descargar_Inventario, VE.ID_Documento_Cabecera_Enlace, VC.ID_Orden_Ingreso')
			->from($this->table . ' AS VC')
			->join($this->table_documento_enlace . ' AS VE', 'VE.ID_Documento_Cabecera_Enlace = VC.ID_Documento_Cabecera', 'left')
			->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
			->join($this->table_entidad . ' AS PROVE', 'PROVE.ID_Entidad = VC.ID_Entidad', 'join')
			->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = PROVE.ID_Tipo_Documento_Identidad', 'join')
			->join($this->table_moneda . ' AS MONE', 'MONE.ID_Moneda = VC.ID_Moneda', 'join')
			->join($this->table_medio_pago . ' AS MP', 'MP.ID_Medio_Pago = VC.ID_Medio_Pago', 'join')
			->join($this->table_documento_detalle . ' AS VD', 'VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera', 'left')
			->join($this->table_orden_ingreso . ' AS ORI', 'ORI.ID_Orden_Ingreso = VD.ID_OI_Detalle', 'left')
			->join($this->table_serie_documento . ' AS SERIE', 'SERIE.ID_Serie_Documento_PK = ORI.ID_Serie_Documento', 'left')
			->join($this->table_tipo_documento . ' AS TDOCU_ORI', 'TDOCU_ORI.ID_Tipo_Documento = ORI.ID_Area_Ingreso', 'left')
			->join($this->table_producto . ' AS PO', 'VD.ID_Producto = PO.ID_Producto', 'join')
			->join($this->table_almacen . ' AS AL', 'VC.ID_Almacen = AL.ID_Almacen', 'left')
			->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
			->join($this->table_configuracion_cuenta_contable_producto . ' AS CCP', 'VD.ID_Producto = CCP.ID_Producto', 'left')
			->join($this->table_configuracion_cuenta_contable . ' AS CC', 'CCP.ID_Configuracion_Cuenta_Contable = CC.ID_Configuracion_Cuenta_Contable', 'left')
			->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
			->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
			->where('VC.ID_Tipo_Asiento', 2)
			->where('VC.ID_Tipo_Documento != ', 12)
			->where('TDOCU.Nu_Compra', 1)
			->where('PROVE.Nu_Tipo_Entidad', 1);

		if(isset($_POST['order'])) {
			$this->db->order_by( 'VC.Nu_Correlativo_Compra DESC' );
		} else if(isset($this->order)) {
			$this->db->order_by( 'VC.Nu_Correlativo_Compra DESC' );
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function _get_datatables_query(){
		$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
		if($this->input->post('Filtro_TiposDocumento')) {
			$this->db->where('VC.ID_Tipo_Documento', $this->input->post('Filtro_TiposDocumento'));
		}
		if($this->input->post('Filtro_SerieDocumento')) {
			$this->db->where('VC.ID_Serie_Documento', $this->input->post('Filtro_SerieDocumento'));
		}
		if($this->input->post('Filtro_NumeroDocumento')) {
			$this->db->where('VC.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento'));
		}
		if($this->input->post('Filtro_Nu_Correlativo')) {
			$this->db->where('VC.Nu_Correlativo_Compra', $this->input->post('Filtro_Nu_Correlativo'));
		}
		if($this->input->post('Filtro_Estado') != '') {
			$this->db->where('VC.Nu_Estado', $this->input->post('Filtro_Estado'));
		}
		if($this->input->post('Filtro_Entidad')) {
			$this->db->where('PROVE.No_Entidad', $this->input->post('Filtro_Entidad'));
		}

		$this->db->select('VC.Fe_Periodo,VC.Nu_Correlativo_Compra, VC.ID_Documento_Cabecera, VC.Fe_Emision, TDOCU.No_Tipo_Documento_Breve, VC.ID_Serie_Documento, VC.ID_Numero_Documento, TDOCUIDEN.No_Tipo_Documento_Identidad_Breve, PROVE.Nu_Documento_Identidad, PROVE.No_Entidad, MONE.No_Signo, ROUND(VC.Ss_Total, 2) AS Ss_Total, VC.Nu_Estado, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, TDOCU.Nu_Enlace, VC.Nu_Descargar_Inventario, VE.ID_Documento_Cabecera_Enlace, VC.ID_Orden_Ingreso')
		->from($this->table . ' AS VC')
		->join($this->table_documento_enlace . ' AS VE', 'VE.ID_Documento_Cabecera_Enlace = VC.ID_Documento_Cabecera', 'left')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
		->join($this->table_entidad . ' AS PROVE', 'PROVE.ID_Entidad = VC.ID_Entidad', 'join')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = PROVE.ID_Tipo_Documento_Identidad', 'join')
		->join($this->table_moneda . ' AS MONE', 'MONE.ID_Moneda = VC.ID_Moneda', 'join')
		->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
		->where('VC.ID_Tipo_Asiento', 2)
		->where('VC.ID_Tipo_Documento != ', 12)
		->where('TDOCU.Nu_Compra', 1)
		->where('PROVE.Nu_Tipo_Entidad', 1);

		if(isset($_POST['order'])) {
			$this->db->order_by('VC.Nu_Correlativo_Compra DESC');
		} else if(isset($this->order)) {
			$this->db->order_by('VC.Nu_Correlativo_Compra DESC');
		}
	}

	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $query = "SELECT
VC.ID_Empresa,
VC.ID_Organizacion,
VC.ID_Almacen,
VC.ID_Documento_Cabecera,
PROVE.ID_Entidad,
PROVE.No_Entidad,
PROVE.Nu_Documento_Identidad,
PROVE.Txt_Direccion_Entidad,
VC.ID_Tipo_Documento,
TDOCU.Nu_Impuesto,
TDOCU.Nu_Enlace,
VC.ID_Serie_Documento,
VC.ID_Numero_Documento,
VC.Fe_Emision,
VC.ID_Moneda,
VC.ID_Medio_Pago,
VC.Fe_Vencimiento,
VC.Fe_Periodo,
VC.Nu_Descargar_Inventario,
VC.ID_Lista_Precio_Cabecera,
VD.ID_Producto,
PRO.Nu_Codigo_Barra,
PRO.No_Producto,
ROUND(VD.Ss_Precio, 6) AS Ss_Precio,
VD.Qt_Producto,
VD.ID_Impuesto_Cruce_Documento,
VD.Ss_SubTotal AS Ss_SubTotal_Producto,
VD.Ss_Impuesto AS Ss_Impuesto_Producto,
ROUND(VD.Ss_Descuento, 2) AS Ss_Descuento_Producto,
ROUND(VD.Ss_Descuento_Impuesto, 2) AS Ss_Descuento_Impuesto_Producto,
ROUND(VD.Po_Descuento, 2) AS Po_Descuento_Impuesto_Producto,
ROUND(VD.Ss_Total, 2) AS Ss_Total_Producto,
ICDOCU.Ss_Impuesto,
VE.ID_Tipo_Documento_Modificar,
VE.ID_Serie_Documento_Modificar,
VE.ID_Numero_Documento_Modificar,
MP.Nu_Tipo,
IMP.Nu_Tipo_Impuesto,
VC.Txt_Glosa,
ROUND(VC.Ss_Descuento, 2) AS Ss_Descuento,
ROUND(VC.Ss_Total, 2) AS Ss_Total,
ROUND(VC.Ss_Percepcion, 2) AS Ss_Percepcion,
VC.Fe_Detraccion,
VC.Nu_Detraccion,
VC.ID_Rubro,
VC.Po_Descuento,
VDL.Nu_Lote_Vencimiento,
VDL.Fe_Lote_Vencimiento,
VC.ID_Tipo_Documento_Rentanilidad,
VC.ID_Orden_Ingreso,
VC.Fe_Emision_Anterior,
VD.ID_OI_Detalle
FROM
" . $this->table . " AS VC
JOIN " . $this->table_documento_detalle . " AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
LEFT JOIN " . $this->table_documento_detalle_lote . " AS VDL ON(VC.ID_Documento_Cabecera = VDL.ID_Documento_Cabecera AND VD.ID_Documento_Detalle = VDL.ID_Documento_Detalle)
JOIN " . $this->table_entidad . " AS PROVE ON(PROVE.ID_Entidad = VC.ID_Entidad)
JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
JOIN " . $this->table_impuesto_cruce_documento . " AS ICDOCU ON(ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
JOIN impuesto AS IMP ON(IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
JOIN medio_pago AS MP ON(MP.ID_Medio_Pago = VC.ID_Medio_Pago)
LEFT JOIN (
SELECT
VE.ID_Documento_Cabecera,
VC.ID_Tipo_Documento AS ID_Tipo_Documento_Modificar,
VC.ID_Serie_Documento AS ID_Serie_Documento_Modificar,
VC.ID_Numero_Documento AS ID_Numero_Documento_Modificar
FROM
" . $this->table . " AS VC
JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
JOIN " . $this->table_documento_enlace . " AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera_Enlace)
WHERE
VC.ID_Tipo_Asiento = 2
AND TDOCU.Nu_Enlace_Modificar = 1
) AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera)
WHERE VC.ID_Documento_Cabecera = " . $ID;
        return $this->db->query($query)->result();
    }
    
    public function agregarCompra($arrCompraCabecera, $arrCompraDetalle, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrProveedorNuevo){
    	$ID_Entidad=0;
    	if (!empty($arrCompraCabecera['ID_Entidad']))
    		$ID_Entidad=$arrCompraCabecera['ID_Entidad'];
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 2 AND ID_Entidad = " . $ID_Entidad . " AND ID_Tipo_Documento = " . $arrCompraCabecera['ID_Tipo_Documento'] . " AND ID_Serie_Documento = '" . $arrCompraCabecera['ID_Serie_Documento'] . "' AND ID_Numero_Documento = " . $arrCompraCabecera['ID_Numero_Documento'] . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			$this->db->trans_begin();

			$Nu_Correlativo = 0;
			$Fe_Year = ToYear($arrCompraCabecera['Fe_Periodo']);
			$Fe_Month = ToMonth($arrCompraCabecera['Fe_Periodo']);
			$arrCorrelativoPendiente = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento_pendiente WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 2 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' ORDER BY Nu_Correlativo DESC LIMIT 1")->result();

			if ( count($arrCorrelativoPendiente) > 0 ){
				$Nu_Correlativo = $arrCorrelativoPendiente[0]->Nu_Correlativo;
				
				$this->db->where('ID_Empresa', $this->user->ID_Empresa);
				$this->db->where('ID_Tipo_Asiento', 2);
				$this->db->where('Fe_Year', $Fe_Year);
				$this->db->where('Fe_Month', $Fe_Month);
				$this->db->where('Nu_Correlativo', $Nu_Correlativo);
		        $this->db->delete('correlativo_tipo_asiento_pendiente');
			} else {
				if($this->db->query("SELECT COUNT(*) AS existe FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 2 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->existe > 0){
					$sql_correlativo_libro_sunat = "UPDATE
correlativo_tipo_asiento
SET
Nu_Correlativo = Nu_Correlativo + 1
WHERE
ID_Empresa=" . $this->user->ID_Empresa . "
AND ID_Tipo_Asiento=2
AND Fe_Year='" . $Fe_Year. "'
AND Fe_Month='" . $Fe_Month . "'";
					$this->db->query($sql_correlativo_libro_sunat);
				} else {
					$sql_correlativo_libro_sunat = "INSERT INTO correlativo_tipo_asiento (
ID_Empresa,
ID_Tipo_Asiento,
Fe_Year,
Fe_Month,
Nu_Correlativo
) VALUES (
" . $this->user->ID_Empresa . ",
2,
'" . $Fe_Year . "',
'" . $Fe_Month . "',
1
);";
					$this->db->query($sql_correlativo_libro_sunat);
				}
				$Nu_Correlativo = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 2 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->Nu_Correlativo;
			}
			
			if (is_array($arrProveedorNuevo)){
			    unset($arrCompraCabecera['ID_Entidad']);
			    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 1 AND ID_Tipo_Documento_Identidad = " . $arrProveedorNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrProveedorNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
					$arrProveedor = array(
		                'ID_Empresa'					=> $this->user->ID_Empresa,
	                	'ID_Organizacion'				=> $arrCompraCabecera['ID_Organizacion'],
		                'Nu_Tipo_Entidad'				=> 1,//Proveedor
		                'ID_Tipo_Documento_Identidad'	=> $arrProveedorNuevo['ID_Tipo_Documento_Identidad'],
		                'Nu_Documento_Identidad'		=> $arrProveedorNuevo['Nu_Documento_Identidad'],
		                'No_Entidad'					=> $arrProveedorNuevo['No_Entidad'],
		                'Txt_Direccion_Entidad' 		=> $arrProveedorNuevo['Txt_Direccion_Entidad'],
		                'Nu_Telefono_Entidad'			=> $arrProveedorNuevo['Nu_Telefono_Entidad'],
		                'Nu_Celular_Entidad'			=> $arrProveedorNuevo['Nu_Celular_Entidad'],
		                'Nu_Estado' 					=> 1,
		            );
		    		$this->db->insert('entidad', $arrProveedor);
		    		$Last_ID_Entidad = $this->db->insert_id();
			    } else {
					$this->db->trans_rollback();
					return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El proveedor ya se encuentra creado, seleccionar Existente');
				}
	    		$arrCompraCabecera = array_merge($arrCompraCabecera, array("ID_Entidad" => $Last_ID_Entidad));
			}
			
			$query = "SELECT Nu_Correlativo_Compra FROM empresa WHERE ID_Empresa=" . $this->user->ID_Empresa . " LIMIT 1";
			$Nu_Correlativo_Compra = $this->db->query($query)->row()->Nu_Correlativo_Compra;

			$arrCompraCabecera = array_merge($arrCompraCabecera, array("Nu_Correlativo" => $Nu_Correlativo, "Nu_Correlativo_Compra" => $Nu_Correlativo_Compra));
			$this->db->insert($this->table, $arrCompraCabecera);
			$Last_ID_Documento_Cabecera = $this->db->insert_id();
			
			$query = "UPDATE empresa SET Nu_Correlativo_Compra=Nu_Correlativo_Compra+1 WHERE ID_Empresa=" . $this->user->ID_Empresa;
			$this->db->query($query);

			/* Solo para Notas de Crédito y Débito */
			if ( $esEnlace == 1 ){
				$table_documento_enlace = array(
					'ID_Empresa'					=> $this->user->ID_Empresa,
					'ID_Documento_Cabecera'			=> $Last_ID_Documento_Cabecera,
					'ID_Documento_Cabecera_Enlace'	=> $ID_Documento_Cabecera_Enlace,
				);
				$this->db->insert($this->table_documento_enlace, $table_documento_enlace);
			}
			
			foreach ($arrCompraDetalle as $row) {
				$documento_detalle[] = array(
					'ID_Empresa' => $this->user->ID_Empresa,
					'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
					'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
					'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio' => $this->security->xss_clean($row['Ss_Precio']),
					'Ss_SubTotal' => $this->security->xss_clean($row['Ss_SubTotal']),
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['Ss_Descuento'],
					'ID_Impuesto_Cruce_Documento' => $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
					'Ss_Impuesto' => $this->security->xss_clean($row['Ss_Impuesto']),
					'Ss_Total' => round($this->security->xss_clean($row['Ss_Total']), 2),
					'ID_OI_Detalle' => $row['ID_Orden_Ingreso_Existe_Detalle'],
				);
			}
			$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
			$iIdDocumentoDetalleFirst = $this->db->insert_id();

			$iCountDataDetalle = count($arrCompraDetalle);
			foreach ($arrCompraDetalle as $row) {
				//if (!empty($row['Nu_Lote_Vencimiento']) && !empty($row['Fe_Lote_Vencimiento'])) {
					$documento_detalle_lote[] = array(
						'ID_Empresa' => $this->user->ID_Empresa,
						'ID_Organizacion' => $this->empresa->ID_Organizacion,
						'ID_Almacen' => (isset($arrCompraCabecera['ID_Almacen']) ? $arrCompraCabecera['ID_Almacen'] : ''),
						'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
						'ID_Documento_Cabecera'	=> $Last_ID_Documento_Cabecera,
						'ID_Documento_Detalle'	=> $iIdDocumentoDetalleFirst,
						'Nu_Lote_Vencimiento' => $this->security->xss_clean($row['Nu_Lote_Vencimiento']),
						'Fe_Lote_Vencimiento' => (!empty($row['Fe_Lote_Vencimiento']) ? ToDate($this->security->xss_clean($row['Fe_Lote_Vencimiento'])) : ''),
					);
					++$iIdDocumentoDetalleFirst;
				//}
			}
			if ( isset($documento_detalle_lote) )
				$this->db->insert_batch($this->table_documento_detalle_lote, $documento_detalle_lote);

	        if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
	        } else {
	            $this->db->trans_commit();
	            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado', 'Last_ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera);
	        }
		}
    }

    public function actualizarCompra($where, $arrCompraCabecera, $arrCompraDetalle, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrProveedorNuevo){
		$this->db->trans_begin();
		
		$arrDataModificar = $this->db->query("SELECT VC.Nu_Correlativo_Compra, VC.ID_Organizacion, VC.ID_Documento_Cabecera, VC.ID_Tipo_Documento, VC.Nu_Correlativo, VC.Nu_Descargar_Inventario, VE.ID_Documento_Cabecera_Enlace FROM documento_cabecera AS VC LEFT JOIN documento_enlace AS VE ON(VE.ID_Documento_Cabecera = VC.ID_Documento_Cabecera) WHERE VC.ID_Documento_Cabecera = " . $where['ID_Documento_Cabecera'] . " LIMIT 1")->result();
		
		$ID_Documento_Cabecera = $arrDataModificar[0]->ID_Documento_Cabecera;
		$ID_Tipo_Documento = $arrDataModificar[0]->ID_Tipo_Documento;
		$Nu_Correlativo = $arrDataModificar[0]->Nu_Correlativo;
		$Nu_Descargar_Inventario = $arrDataModificar[0]->Nu_Descargar_Inventario;
		$ID_Documento_Cabecera_Enlace = $arrDataModificar[0]->ID_Documento_Cabecera_Enlace;
		$Nu_Correlativo_Compra = $arrDataModificar[0]->Nu_Correlativo_Compra;
		
        $this->db->delete($this->table_documento_detalle_lote, $where);
		$this->db->delete($this->table_documento_detalle, $where);
		/* Para Notas de Crédito y Débito */
		//if ( $esEnlace == 1 ){
			$this->db->delete($this->table_documento_enlace, $where);
		//}
		
    	if ($Nu_Descargar_Inventario == 1) {
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera = " . $ID_Documento_Cabecera;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where_stock_producto = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
	        		if ($ID_Tipo_Documento != 5){//Nota de Crédito
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where_stock_producto);
	        		} else {
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto + round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where_stock_producto);
	        		}
				}
        	}
			$this->db->where('ID_Documento_Cabecera', $ID_Documento_Cabecera);
	        $this->db->delete('movimiento_inventario');
		}
		
        $this->db->delete($this->table, $where);

		if (is_array($arrProveedorNuevo)){
		    unset($arrCompraCabecera['ID_Entidad']);
		    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 1 AND ID_Tipo_Documento_Identidad = " . $arrProveedorNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrProveedorNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
				$arrProveedor = array(
	                'ID_Empresa'					=> $this->user->ID_Empresa,
	                'ID_Organizacion'				=> $arrDataModificar[0]->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 1,//Proveedor
	                'ID_Tipo_Documento_Identidad'	=> $arrProveedorNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrProveedorNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrProveedorNuevo['No_Entidad'],
	                'Txt_Direccion_Entidad' 		=> $arrProveedorNuevo['Txt_Direccion_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrProveedorNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrProveedorNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrProveedor);
	    		$Last_ID_Entidad = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El proveedor ya se encuentra creado, seleccionar Existente');
			}
    		$arrCompraCabecera = array_merge($arrCompraCabecera, array("ID_Entidad" => $Last_ID_Entidad));
		}

		$arrCompraCabecera = array_merge($arrCompraCabecera, array("Nu_Correlativo" => $Nu_Correlativo, "Nu_Correlativo_Compra" => $Nu_Correlativo_Compra));
		$this->db->insert($this->table, $arrCompraCabecera);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();
        
		/* Solo para Notas de Crédito y Débito */
		
		if ( !empty($ID_Documento_Cabecera_Enlace) ) {
			$table_documento_enlace = array(
				'ID_Empresa'					=> $this->user->ID_Empresa,
				'ID_Documento_Cabecera'			=> $Last_ID_Documento_Cabecera,
				'ID_Documento_Cabecera_Enlace'	=> $ID_Documento_Cabecera_Enlace,
			);
			$this->db->insert($this->table_documento_enlace, $table_documento_enlace);
		}
		/*
		if ( $esEnlace == 1 ){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $Last_ID_Documento_Cabecera);
	        $this->db->delete($this->table_documento_enlace);
	        
			$table_documento_enlace = array(
				'ID_Empresa'					=> $this->user->ID_Empresa,
				'ID_Documento_Cabecera'			=> $Last_ID_Documento_Cabecera,
				'ID_Documento_Cabecera_Enlace'	=> $ID_Documento_Cabecera_Enlace,
			);
			$this->db->insert($this->table_documento_enlace, $table_documento_enlace);
		}
		*/

		foreach ($arrCompraDetalle as $row) {
			$documento_detalle[] = array(
				'ID_Empresa' => $this->user->ID_Empresa,
				'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
				'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
				'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
				'Ss_Precio' => $this->security->xss_clean($row['Ss_Precio']),
				'Ss_SubTotal' => $this->security->xss_clean($row['Ss_SubTotal']),
				'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
				'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
				'Po_Descuento' => $row['Ss_Descuento'],
				'ID_Impuesto_Cruce_Documento' => $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
				'Ss_Impuesto' => $this->security->xss_clean($row['Ss_Impuesto']),
				'Ss_Total' => round($this->security->xss_clean($row['Ss_Total']), 2),
				'ID_OI_Detalle' => $row['ID_Orden_Ingreso_Existe_Detalle'],
			);
		}
		$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
		$iIdDocumentoDetalleFirst = $this->db->insert_id();

		$iCountDataDetalle = count($arrCompraDetalle);
		foreach ($arrCompraDetalle as $row) {
			//if (!empty($row['Nu_Lote_Vencimiento']) && !empty($row['Fe_Lote_Vencimiento'])) {
				$documento_detalle_lote[] = array(
					'ID_Empresa' => $this->user->ID_Empresa,
					'ID_Organizacion' => $this->empresa->ID_Organizacion,
					'ID_Almacen' => (isset($arrCompraCabecera['ID_Almacen']) ? $arrCompraCabecera['ID_Almacen'] : ''),
					'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
					'ID_Documento_Cabecera'	=> $Last_ID_Documento_Cabecera,
					'ID_Documento_Detalle'	=> $iIdDocumentoDetalleFirst,
					'Nu_Lote_Vencimiento' => $this->security->xss_clean($row['Nu_Lote_Vencimiento']),
					'Fe_Lote_Vencimiento' => ToDate($this->security->xss_clean($row['Fe_Lote_Vencimiento'])),
				);
				++$iIdDocumentoDetalleFirst;
			//}
		}
		if ( isset($documento_detalle_lote) )
			$this->db->insert_batch($this->table_documento_detalle_lote, $documento_detalle_lote);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
        } else {
            $this->db->trans_commit();
	        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado', 'Last_ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera);
        }
    }
    
	public function anularCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		$this->db->trans_begin();
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
		$this->db->delete($this->table_documento_detalle_lote);
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
        $this->db->delete($this->table_documento_detalle);
        
        //Validar N/C y N/B
        if ($Nu_Enlace == 1){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete($this->table_documento_enlace);
        }

    	if ($Nu_Descargar_Inventario == 1) {
    		$ID_Tipo_Documento = $this->db->query("SELECT ID_Tipo_Documento FROM documento_cabecera WHERE ID_Empresa=".$this->user->ID_Empresa." AND ID_Documento_Cabecera=".$ID." LIMIT 1")->row()->ID_Tipo_Documento;
    		
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera = ".$ID;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
	        		if ($ID_Tipo_Documento != 5){//Nota de Crédito
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		} else {
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto + round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		}
				}
        	}
	        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
			$data = array(
				'Qt_Producto' => 0,
				'Ss_Precio' => 0,
				'Ss_SubTotal' => 0,
				'Ss_Costo_Promedio' => 0,
			);
	        $this->db->update('movimiento_inventario', $data);
    	}
    	
		if ($this->db->query("SELECT count(*) existe FROM guia_enlace WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Documento_Cabecera = " . $ID . " LIMIT 1")->row()->existe > 0){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete('guia_enlace');
		}

        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
		$data = array(
			'Nu_Estado' => 7,
			'Ss_Descuento' => 0.00,
			'Ss_Total' => 0.00,
			'Ss_Percepcion' => 0.00,
		);
        $this->db->update($this->table, $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al anular');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro anulado');
        }
	}
    
	public function eliminarCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		$this->db->trans_begin();
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
		$this->db->delete($this->table_documento_detalle_lote);
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
        $this->db->delete($this->table_documento_detalle);
        
        //Validar N/C y N/B
        if ($Nu_Enlace == 1){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete($this->table_documento_enlace);
        }
        
    	if ($Nu_Descargar_Inventario == 1) {
    		$ID_Tipo_Documento = $this->db->query("SELECT ID_Tipo_Documento FROM documento_cabecera WHERE ID_Empresa=".$this->user->ID_Empresa." AND ID_Documento_Cabecera=".$ID." LIMIT 1")->row()->ID_Tipo_Documento;
    		
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera = " . $ID;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
	        		if ($ID_Tipo_Documento != 5){//Nota de Crédito
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		} else {
						$stock_producto = array(
							'ID_Producto' => $row->ID_Producto,
							'Qt_Producto' => ($Qt_Producto + round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		}
				}
        	}
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete('movimiento_inventario');
    	}
        
        $arrCorrelativoPendiente = $this->db->query("SELECT Fe_Periodo, Nu_Correlativo FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Documento_Cabecera = " . $ID . " LIMIT 1")->result();
        
        $sql_correlativo_pendiente_libro_sunat = "
INSERT INTO correlativo_tipo_asiento_pendiente (
 ID_Empresa,
 ID_Tipo_Asiento,
 Fe_Year,
 Fe_Month,
 Nu_Correlativo
) VALUES (
 " . $this->user->ID_Empresa . ",
 2,
 '" . ToYear($arrCorrelativoPendiente[0]->Fe_Periodo) . "',
 '" . ToMonth($arrCorrelativoPendiente[0]->Fe_Periodo) . "',
 " . $arrCorrelativoPendiente[0]->Nu_Correlativo . "
);
		";
		$this->db->query($sql_correlativo_pendiente_libro_sunat);

		if ($this->db->query("SELECT count(*) existe FROM guia_enlace WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Documento_Cabecera = " . $ID . " LIMIT 1")->row()->existe > 0){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete('guia_enlace');
		}
        
        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
        $this->db->delete($this->table);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
        }
	}

	public function getDocumentoCabeceraOfNoteOfCredit($id) {
		$this->db->select('documento_cabecera.ID_Serie_Documento, documento_cabecera.ID_Numero_Documento, documento_cabecera.Fe_Emision, TDOCU.No_Tipo_Documento_Breve')
		->from($this->table_documento_enlace)
		->join($this->table, 'documento_enlace.ID_Documento_Cabecera_Enlace = documento_cabecera.ID_Documento_Cabecera')
		->join($this->table_entidad . ' AS PROVE', 'PROVE.ID_Entidad = documento_cabecera.ID_Entidad', 'join')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = PROVE.ID_Tipo_Documento_Identidad', 'join')
		->join($this->table_tipo_documento. ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = documento_cabecera.ID_Tipo_Documento')
		->where('documento_enlace.ID_Documento_Cabecera', $id);
		$query = $this->db->get();
		return @$query->result()[0];
	}
}
