<?php
class DocumentoElectronicoModel extends CI_Model{
	var $table = 'documento_cabecera';
	var $table_documento_detalle = 'documento_detalle';
	var $table_documento_medio_pago = 'documento_medio_pago';
	var $table_documento_enlace	= 'documento_enlace';
	var $table_tipo_documento = 'tipo_documento';
	var $table_impuesto_cruce_documento	= 'impuesto_cruce_documento';
	var $table_entidad = 'entidad';
	var $table_tipo_documento_identidad	= 'tipo_documento_identidad';
	var $table_moneda = 'moneda';
	var $table_medio_pago = 'medio_pago';

	public function __construct(){
		parent::__construct();
	}

	public function obtenerComprobanteMedioPago($arrParams){
		$query = "SELECT MP.Txt_Medio_Pago, VMP.Ss_Total AS Ss_Total_Medio_Pago FROM " . $this->table . " AS VC JOIN " . $this->table_documento_medio_pago . " AS VMP ON(VC.ID_Documento_Cabecera = VMP.ID_Documento_Cabecera) JOIN " . $this->table_medio_pago . " AS MP ON(MP.ID_Medio_Pago = VMP.ID_Medio_Pago) WHERE VC.ID_Documento_Cabecera = " . $arrParams['iIdDocumentoCabecera'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener medio(s) de pago',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No hay registro',
		);
	}

    public function obtenerComprobante($arrParams){
        $query = "
SELECT
 ALMA.Txt_FE_Ruta,
 ALMA.Txt_FE_Token,
 VC.ID_Empresa,
 VC.ID_Organizacion,
 VC.ID_Almacen,
 VC.ID_Documento_Cabecera,
 CLI.ID_Entidad,
 CLI.No_Entidad,
 CLI.Nu_Documento_Identidad,
 CLI.Txt_Direccion_Entidad,
 VC.ID_Tipo_Documento,
 TDOCU.Nu_Impuesto,
 TDOCU.Nu_Enlace,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento,
 VC.Fe_Emision,
 VC.ID_Moneda,
 VC.ID_Medio_Pago,
 VC.Fe_Vencimiento,
 VC.Nu_Descargar_Inventario,
 VC.ID_Lista_Precio_Cabecera,
 VD.ID_Producto,
 PRO.Nu_Codigo_Barra,
 PRO.No_Producto,
 VD.Ss_Precio,
 VD.Qt_Producto,
 VD.ID_Impuesto_Cruce_Documento,
 ROUND(VD.Ss_SubTotal, 2) AS Ss_SubTotal_Producto,
 ROUND(VD.Ss_Impuesto, 2) AS Ss_Impuesto_Producto,
 ROUND(VD.Ss_Descuento, 2) AS Ss_Descuento_Producto,
 ROUND(VD.Ss_Total, 2) AS Ss_Total_Producto,
 PRO.ID_Impuesto_Icbper,
 VD.Txt_Nota,
 ICDOCU.Ss_Impuesto,
 VE.ID_Tipo_Documento_Modificar,
 VE.Nu_Sunat_Codigo_Tipo_Documento_Modificar,
 VE.ID_Serie_Documento_Modificar,
 VE.ID_Numero_Documento_Modificar,
 IMP.Nu_Tipo_Impuesto,
 IMP.ID_Impuesto,
 VC.Txt_Glosa,
 ROUND(VC.Ss_Descuento, 2) AS Ss_Descuento,
 MONE.No_Signo,
 ROUND(VC.Ss_Total, 2) AS Ss_Total,
 MONE.No_Moneda,
 VC.Po_Descuento,
 TDOCUIDEN.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_TDI,
 UM.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_UM,
 VC.Txt_Url_PDF,
 VC.Txt_Url_XML,
 VC.Txt_Url_CDR,
 VC.Txt_Url_Comprobante,
 TDOCU.No_Tipo_Documento,
 VC.Nu_Codigo_Motivo_Referencia,
 VC.Nu_Detraccion,
 TC.Ss_Venta_Oficial AS Ss_Tipo_Cambio,
 VC.No_Formato_PDF,
 VD.Txt_Nota AS Txt_Nota_Item,
 MONE.Nu_Valor_Fe AS Nu_Valor_Fe_Moneda,
 IMP.Nu_Valor_Fe AS Nu_Valor_Fe_Impuesto,
 MONE.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Moneda,
 IMP.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Impuesto,
 TDOCU.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Tipo_Documento,
 TDMOTIVO.No_Descripcion AS No_Descripcion_Motivo_Referencia,
 SD.Nu_Cantidad_Caracteres
FROM
 " . $this->table . " AS VC
 JOIN organizacion AS ORG ON(VC.ID_Organizacion = ORG.ID_Organizacion)
 JOIN almacen AS ALMA ON(VC.ID_Almacen = ALMA.ID_Almacen)
 JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
 JOIN " . $this->table_documento_detalle . " AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 JOIN " . $this->table_entidad . " AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
 JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
 JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
 JOIN unidad_medida AS UM ON(UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
 JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
 JOIN " . $this->table_impuesto_cruce_documento . " AS ICDOCU ON(ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
 JOIN impuesto AS IMP ON(IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
 LEFT JOIN tabla_dato AS TDMOTIVO ON(TDMOTIVO.Nu_Valor = VC.Nu_Codigo_Motivo_Referencia AND TDMOTIVO.No_Class = VC.ID_Tipo_Documento AND TDMOTIVO.No_Relacion = 'Motivos_Referencia')
 LEFT JOIN tasa_cambio AS TC ON(TC.ID_Empresa = VC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND TC.Fe_Ingreso = VC.Fe_Emision)
 LEFT JOIN (
 SELECT
  VE.ID_Documento_Cabecera,
  VC.ID_Tipo_Documento AS ID_Tipo_Documento_Modificar,
  TDOCU.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Tipo_Documento_Modificar,
  VC.ID_Serie_Documento AS ID_Serie_Documento_Modificar,
  VC.ID_Numero_Documento AS ID_Numero_Documento_Modificar
 FROM
  " . $this->table . " AS VC
  JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
  JOIN " . $this->table_documento_enlace . " AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera_Enlace)
 ) AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera)
WHERE
 VC.ID_Documento_Cabecera = " . $arrParams['iIdDocumentoCabecera'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al generar formato documento electrónico',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No hay registro',
		);
    }

	public function obtenerComprobanteAnulado($arrParams){
		$query = "
SELECT
 ALMA.Txt_FE_Ruta,
 ALMA.Txt_FE_Token,
 VC.ID_Tipo_Documento,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento
FROM
 documento_cabecera AS VC
 JOIN almacen AS ALMA ON(VC.ID_Almacen = ALMA.ID_Almacen)
WHERE
 VC.ID_Documento_Cabecera = " . $arrParams['iIdDocumentoCabecera'] . "
LIMIT 1";

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'sStatus' => 'warning',
			'sMessage' => $sMessage,
		);
	}

	public function generarFormatoDocumentoElectronicoSunat($arrParams){
		if ($arrParams['iEstadoVenta'] == 6 || $arrParams['iEstadoVenta'] == 9 ) {
			$arrData = $this->DocumentoElectronicoModel->obtenerComprobante($arrParams);
			if ( $arrData['sStatus'] == 'success' ) {
				$arrData = $arrData['arrData'];

				$Ss_SubTotal_Producto = 0.00;
				$Ss_Descuento_Producto = 0.00;
				$Ss_Total_Producto = 0.00;
				$Ss_Gravada = 0.00;
				$Ss_Inafecto = 0.00;
				$Ss_Exonerada = 0.00;
				$Ss_Gratuita = 0.00;
				$Ss_IGV = 0.00;
				$Ss_Total = 0.00;
				
				$i = 0;
				$fTotalIcbper = 0.00;
				$Po_IGV = "";
				$iCounter = 1;
				$sPrecioTipoCodigoDetalle = '01';
				foreach ($arrData as $row) {
					$sPrecioTipoCodigoDetalle = '01';
					$Ss_Precio_VU = $row->Ss_Precio;
					if ($row->Nu_Tipo_Impuesto == 1){//IGV
						$Po_IGV = "18.00";
						$Ss_Precio_VU = round($row->Ss_Precio / $row->Ss_Impuesto, 6);
						$Ss_IGV += $row->Ss_Impuesto_Producto;
						$Ss_Gravada += $row->Ss_SubTotal_Producto;
					} else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
						$Ss_Inafecto += $row->Ss_SubTotal_Producto;
					} else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
						$Ss_Exonerada += $row->Ss_SubTotal_Producto;
					} else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
						$Ss_Gratuita += $row->Ss_SubTotal_Producto;
						$sPrecioTipoCodigoDetalle = '02';
					}

					if ( $row->ID_Impuesto_Icbper == 1 )
						$fTotalIcbper += $row->Ss_Total_Producto;

					$Ss_Descuento_Producto += $row->Ss_Descuento_Producto;
					$Ss_Total += $row->Ss_Total_Producto;

					$data_detalle["detalle"][$i]["ITEM"] = $iCounter;
					$data_detalle["detalle"][$i]["UNIDAD_MEDIDA_DET"] = $row->Nu_Sunat_Codigo_UM;
					$data_detalle["detalle"][$i]["CODIGO_DET"] = $row->Nu_Codigo_Barra;
					$data_detalle["detalle"][$i]["DESCRIPCION_DET"] = $row->No_Producto . ($row->Txt_Nota_Item != '' ? ' ' . $row->Txt_Nota_Item : '');
					$data_detalle["detalle"][$i]["CANTIDAD_DET"] = $row->Qt_Producto;
					$data_detalle["detalle"][$i]["PRECIO_SIN_IGV_DET"] = $Ss_Precio_VU;
					$data_detalle["detalle"][$i]["PRECIO_DET"] = $row->Ss_Precio;
					$data_detalle["detalle"][$i]["IMPORTE_DET"] = $row->Ss_SubTotal_Producto;
					$data_detalle["detalle"][$i]["COD_TIPO_OPERACION"] = $row->Nu_Sunat_Codigo_Impuesto;
					$data_detalle["detalle"][$i]["IGV"] = $row->Ss_Impuesto_Producto;
					$data_detalle["detalle"][$i]["ISC"] = "0";
					$data_detalle["detalle"][$i]["PRECIO_TIPO_CODIGO"] = $sPrecioTipoCodigoDetalle;
					$data_detalle["detalle"][$i]["TOTAL"] = $row->Ss_Total_Producto;
					$i++;
					++$iCounter;
				}

				$this->load->library('EnLetras', 'el');
				$EnLetras = new EnLetras();

				$data_cabecera = array(
					"operacion"	=> "generar_comprobante",
					"TIPO_OPERACION" => "0101",
					"COD_TIPO_DOCUMENTO" => $arrData[0]->Nu_Sunat_Codigo_Tipo_Documento,
					"NRO_COMPROBANTE" => $arrData[0]->ID_Serie_Documento . '-' . autocompletarConCeros('', $arrData[0]->ID_Numero_Documento, $arrData[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT),
					"TIPO_DOCUMENTO_CLIENTE" => $arrData[0]->Nu_Sunat_Codigo_TDI,
					"NRO_DOCUMENTO_CLIENTE" => $arrData[0]->Nu_Documento_Identidad,
					"RAZON_SOCIAL_CLIENTE" => $arrData[0]->No_Entidad,
					"DIRECCION_CLIENTE" => $arrData[0]->Txt_Direccion_Entidad,
					"CIUDAD_CLIENTE" => "",
					"COD_PAIS_CLIENTE" => "",
					"FECHA_DOCUMENTO" => $arrData[0]->Fe_Emision,
					"FECHA_VTO" => $arrData[0]->Fe_Vencimiento,
					"COD_MONEDA" => $arrData[0]->Nu_Sunat_Codigo_Moneda,
					"POR_IGV" => $Po_IGV,
					"TOTAL_DESCUENTO" => $arrData[0]->Ss_Descuento + $Ss_Descuento_Producto,
					"SUB_TOTAL" => $Ss_Gravada,
					"TOTAL_GRAVADAS" => $Ss_Gravada,
					"TOTAL_INAFECTA" => $Ss_Inafecto,
					"TOTAL_EXONERADAS" => $Ss_Exonerada,
					"TOTAL_IGV" => $Ss_IGV,
					"TOTAL_GRATUITAS" => "",
					"TOTAL" => $Ss_Total + $fTotalIcbper,
					"ICBP" => $fTotalIcbper,
					"TIPO_COMPROBANTE_MODIFICA" => $arrData[0]->Nu_Sunat_Codigo_Tipo_Documento_Modificar,
					"NRO_DOCUMENTO_MODIFICA" => $arrData[0]->ID_Serie_Documento_Modificar . '-' . autocompletarConCeros('', $arrData[0]->ID_Numero_Documento_Modificar, $arrData[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT),
					"COD_TIPO_MOTIVO" => "0" . $arrData[0]->Nu_Codigo_Motivo_Referencia,
					"DESCRIPCION_MOTIVO" => $arrData[0]->No_Descripcion_Motivo_Referencia,
					"TOTAL_LETRAS" => $EnLetras->ValorEnLetras($arrData[0]->Ss_Total + $fTotalIcbper, $arrData[0]->No_Moneda),
					"GLOSA" => $arrData[0]->Txt_Glosa,
				);
				$data = array_merge($data_cabecera, $data_detalle);

				$ruta = $arrData[0]->Txt_FE_Ruta;
				$token = $arrData[0]->Txt_FE_Token;
				
				$arrParamsFE = array(
					"ruta" => $ruta,
					"token" => $token,
					"arrData" => $data,
				);
				
				return $this->enviarDocumentoElectronicoProveedorSunat($arrParamsFE, $arrParams);
			} else {
				return $arrData;
			}// ./ if - else respuesta de modal del comprobante
		} else {
			$arrData = $this->DocumentoElectronicoModel->obtenerComprobante($arrParams);
			if ( $arrData['sStatus'] == 'success' ) {
				$arrData = $arrData['arrData'];
				if ( $arrParams['sTipoBajaSunat'] == 'RC') {					
					$Ss_Gravada = 0.00;
					$Ss_Inafecto = 0.00;
					$Ss_Exonerada = 0.00;
					$Ss_Gratuita = 0.00;
					$Ss_IGV = 0.00;
					$fTotalIcbper = 0.00;
					$Ss_Total = 0.00;					
					foreach ($arrData as $row) {
						if ($row->Nu_Tipo_Impuesto == 1){//IGV
							$Ss_IGV += $row->Ss_Impuesto_Producto;
							$Ss_Gravada += $row->Ss_SubTotal_Producto;
						} else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
							$Ss_Inafecto += $row->Ss_SubTotal_Producto;
						} else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
							$Ss_Exonerada += $row->Ss_SubTotal_Producto;
						} else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
							$Ss_Gratuita += $row->Ss_SubTotal_Producto;
						}

						if ( $row->ID_Impuesto_Icbper == 1 )
							$fTotalIcbper += $row->Ss_Total_Producto;
						$Ss_Total += $row->Ss_Total_Producto;
					}

					$data = array(
						"operacion" => "generar_anulacion",
						"CODIGO" => $arrParams['sTipoBajaSunat'],
						"FECHA_REFERENCIA" => $arrData[0]->Fe_Emision,
						"FECHA_DOCUMENTO" => dateNow('fecha'),
						"detalle" => array(
							"0" => array(
								"ITEM" => 1,
								"TIPO_COMPROBANTE" => $arrData[0]->Nu_Sunat_Codigo_Tipo_Documento,
								"NRO_COMPROBANTE" => $arrData[0]->ID_Serie_Documento . '-' . autocompletarConCeros('', $arrData[0]->ID_Numero_Documento, $arrData[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT),
								"TIPO_DOCUMENTO" => $arrData[0]->Nu_Sunat_Codigo_TDI,
								"NRO_DOCUMENTO" => $arrData[0]->Nu_Documento_Identidad,			
								"TIPO_COMPROBANTE_REF" => $arrData[0]->Nu_Sunat_Codigo_Tipo_Documento_Modificar,
								"NRO_COMPROBANTE_REF" => $arrData[0]->ID_Serie_Documento_Modificar . '-' . autocompletarConCeros('', $arrData[0]->ID_Numero_Documento_Modificar, $arrData[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT),
								"STATU" => "3",
								"COD_MONEDA" => $arrData[0]->Nu_Sunat_Codigo_Moneda,			
								"GRAVADA" => $Ss_Gravada,
								"INAFECTO" => $Ss_Inafecto,
								"EXONERADO" => $Ss_Exonerada,
								"EXPORTACION" => $Ss_IGV,
								"GRATUITAS" => "",
								"IGV" => $Ss_IGV,
								"TOTAL" => $Ss_Total + $fTotalIcbper,
								"ISC" => "0",
								"OTROS" => "0",
								"CARGO_X_ASIGNACION" => "0",
								"MONTO_CARGO_X_ASIG" => "0",
							),
						),
					);
				} else if ( $arrParams['sTipoBajaSunat'] == 'RA' ) {
					$data = array(
						"operacion" => "generar_anulacion",
						"CODIGO" => $arrParams['sTipoBajaSunat'],
						"FECHA_REFERENCIA" => $arrData[0]->Fe_Emision,
						"FECHA_BAJA" => dateNow('fecha'),
						"detalle" => array(
							"0" => array(
								"ITEM" => 1,
								"TIPO_COMPROBANTE" => $arrData[0]->Nu_Sunat_Codigo_Tipo_Documento,
								"SERIE" => $arrData[0]->ID_Serie_Documento,
								"NUMERO" => autocompletarConCeros('', $arrData[0]->ID_Numero_Documento, $arrData[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT),
								"DESCRIPCION" => "ERROR DE DIGITACION",
							),
						),
					);
				}

				$ruta = $arrData[0]->Txt_FE_Ruta;
				$token = $arrData[0]->Txt_FE_Token;

				$arrParamsFE = array(
					"ruta" => $ruta,
					"token" => $token,
					"arrData" => $data,
				);
				
				return $this->enviarDocumentoElectronicoProveedorSunat($arrParamsFE, $arrParams);
			} else {
				return $arrData;
			}// ./ if - else respuesta de modal del comprobante
		}// ./ if - else generar formato de documento electronico SUNAT
	} // ./ generarFormatoDocumentoElectronicoSunat

	private function enviarDocumentoElectronicoProveedorSunat($arrParamsFE, $arrParams){
		$arrData = $arrParamsFE['arrData'];
		$data_json = json_encode($arrData);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $arrParamsFE['ruta']);
		curl_setopt(
			$ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Token token="'.$arrParamsFE['token'].'"',
			'Content-Type: application/json',
			'X-API-Key: ' . $arrParamsFE['token'],
			)
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Obtener el código de respuesta
		$respuesta = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE );	
		curl_close($ch);
		// Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
		$accepted_response = array( 200, 301, 302 );
		if( in_array( $httpcode, $accepted_response ) ) {
			$leer_respuesta = json_decode($respuesta, true);
			
			if ( ($arrParams['iEstadoVenta'] == 6 || $arrParams['iEstadoVenta'] == 9) && !empty($leer_respuesta) ) {
				$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 8 ));
			} else if ( empty($leer_respuesta) ) {
				$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 9 ));
			} else {
				$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 10 ));
			}

			$arrParams = array_merge( $arrParams, array( 'arrMessagePSE' => $leer_respuesta ) );
			$arrResponseEstadoDocumento = $this->cambiarEstadoDocumentoElectronico( $arrParams );
			if ( $arrResponseEstadoDocumento['sStatus'] == 'success' ) {
				if ( $arrResponseEstadoDocumento['iEstadoVenta'] == 8 && !empty($arrParams['sEmailCliente']) ) { 
					$arrParamsEmail = array_merge( $arrParams, array( 'sGenerarRespuestaJson' => false ) );
					$this->sendCorreoFacturaVentaSUNAT($arrParamsEmail);
				}
				
				return array(
					'status' => (!isset($leer_respuesta['codigo']) ? 'success' : 'warning'),
					'style_modal' => (!isset($leer_respuesta['codigo']) ? 'modal-success' : 'modal-warning'),
					'message' => (!isset($leer_respuesta['codigo']) ? 'Venta completada' : 'Venta completada - ' . $leer_respuesta['message']),
					'message_nubefact' => (!isset($leer_respuesta['codigo']) ? 'Venta completada' : 'Venta completada - ' . $leer_respuesta['message']),
					'sStatus' => (!isset($leer_respuesta['codigo']) ? 'success' : 'warning'),
					'sMessage' => (!isset($leer_respuesta['codigo']) ? 'Venta completada' : 'Venta completada - ' . $leer_respuesta['message']),
					'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
					'arrMessagePSE' => (!empty($leer_respuesta) ? $leer_respuesta['message'] : 'No se genero PDF - XML'),
					'sCodigo' => (!isset($leer_respuesta['codigo']) ? '0' : $leer_respuesta['codigo']),
				);
			} else {
				return $arrResponseEstadoDocumento;
			}
		} else {
			$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 9 ));
			$arrResponseEstadoDocumento = $this->cambiarEstadoDocumentoElectronico( $arrParams );
			return array(
				'status' => 'warning',
				'style_modal' => 'modal-warning',
				'message' => 'Venta completada - No enviada a SUNAT',
				'message_nubefact' => 'Venta completada - No enviada a SUNAT',
				'sStatus' => 'warning',
				'sMessage' => 'Venta completada - No enviada a SUNAT',
				'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
				'arrMessagePSE' => 'No existe URL del proveedor para enviar los documentos a SUNAT',
				'sCodigo' => '-500',
			);			
		}
	}

	public function generarFormatoDocumentoElectronico($arrParams){
		if ($arrParams['iEstadoVenta'] == 6) {
			$arrData = $this->DocumentoElectronicoModel->obtenerComprobante($arrParams);
			if ( $arrData['sStatus'] == 'success' ) {
				$arrData = $arrData['arrData'];

				$ruta = $arrData[0]->Txt_FE_Ruta;
				$token = $arrData[0]->Txt_FE_Token;
				
				$Ss_SubTotal_Producto = 0.00;
				$Ss_Descuento_Producto = 0.00;
				$Ss_Total_Producto = 0.00;
				$Ss_Gravada = 0.00;
				$Ss_Inafecto = 0.00;
				$Ss_Exonerada = 0.00;
				$Ss_Gratuita = 0.00;
				$Ss_IGV = 0.00;
				$Ss_Total = 0.00;
				
				$i = 0;
				$fTotalIcbper = 0.00;
				$Po_IGV = "";
				foreach ($arrData as $row) {
					$Ss_Precio_VU = $row->Ss_Precio;
					if ($row->Nu_Tipo_Impuesto == 1){//IGV
						$Po_IGV = "18.00";
						$Ss_Precio_VU = round($row->Ss_Precio / $row->Ss_Impuesto, 6);
						$Ss_IGV += $row->Ss_Impuesto_Producto;
						$Ss_Gravada += $row->Ss_SubTotal_Producto;
					} else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
						$Ss_Inafecto += $row->Ss_SubTotal_Producto;
					} else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
						$Ss_Exonerada += $row->Ss_SubTotal_Producto;
					} else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
						$Ss_Gratuita += $row->Ss_SubTotal_Producto;
					}

					if ( $row->ID_Impuesto_Icbper == 1 )
						$fTotalIcbper += $row->Ss_Total_Producto;

					$Ss_Descuento_Producto += $row->Ss_Descuento_Producto;
					$Ss_Total += $row->Ss_Total_Producto;
					
					$data_detalle["items"][$i]["unidad_de_medida"] = $row->Nu_Sunat_Codigo_UM;
					$data_detalle["items"][$i]["codigo"] = $row->Nu_Codigo_Barra;
					$data_detalle["items"][$i]["descripcion"] = $row->No_Producto . ($row->Txt_Nota_Item != '' ? ' ' . $row->Txt_Nota_Item : '');
					$data_detalle["items"][$i]["cantidad"] = $row->Qt_Producto;
					$data_detalle["items"][$i]["valor_unitario"] = $Ss_Precio_VU;
					$data_detalle["items"][$i]["precio_unitario"] = $row->Ss_Precio;
					$data_detalle["items"][$i]["descuento"] = $row->Ss_Descuento_Producto;
					$data_detalle["items"][$i]["subtotal"] = $row->Ss_SubTotal_Producto;
					$data_detalle["items"][$i]["tipo_de_igv"] = $row->Nu_Valor_Fe_Impuesto;
					$data_detalle["items"][$i]["igv"] = $row->Ss_Impuesto_Producto;
					$data_detalle["items"][$i]["total"] = $row->Ss_Total_Producto;
					$data_detalle["items"][$i]["anticipo_regularizacion"]	= false;
					$data_detalle["items"][$i]["anticipo_documento_serie"]	= "";
					$data_detalle["items"][$i]["anticipo_documento_numero"] = "";
					$i++;
				}

				$arrDataMediosPago = $this->DocumentoElectronicoModel->obtenerComprobanteMedioPago($arrParams);
				if ( $arrDataMediosPago['sStatus'] == 'success' ) {
					$sConcatenarMultiplesMedioPago = '';
					foreach ($arrDataMediosPago['arrData'] as $row)
						$sConcatenarMultiplesMedioPago .= $row->Txt_Medio_Pago . ': ' . $arrData[0]->No_Signo . ' ' . $row->Ss_Total_Medio_Pago . ', ';
					$sConcatenarMultiplesMedioPago = substr($sConcatenarMultiplesMedioPago, 0, -2);	
				} else {
					return $arrDataMediosPago;
				}

				$iTipoNC = "";
				$iTipoND = "";
				
				$iTipoComprobante = 2;//Boleta
				if ($arrData[0]->ID_Tipo_Documento == 3) {//Factura
					$iTipoComprobante = 1;
				} else if ($arrData[0]->ID_Tipo_Documento == 5) {//N/Crédito
					$iTipoComprobante = 3;
					$iTipoNC = $arrData[0]->Nu_Codigo_Motivo_Referencia;
				} else if ($arrData[0]->ID_Tipo_Documento == 6) {//N/Débito
					$iTipoComprobante = 4;
					$iTipoND = $arrData[0]->Nu_Codigo_Motivo_Referencia;
				}
				
				$iTipoComprobanteModifica = "";
				if (!empty($arrData[0]->ID_Tipo_Documento_Modificar)) {
					$iTipoComprobanteModifica = 2;//BOLETAS DE VENTA ELECTRÓNICAS
					if ($arrData[0]->ID_Tipo_Documento_Modificar == 3)//Factura
						$iTipoComprobanteModifica = 1;//FACTURAS ELECTRÓNICAS
				}

				$iIdClienteTipoDocumentoIdentidad = $arrData[0]->Nu_Sunat_Codigo_TDI;
				$sNombreCliente = $arrData[0]->No_Entidad;
				if (
					$arrData[0]->ID_Tipo_Documento == 4
					&& (empty($arrData[0]->Nu_Documento_Identidad) || empty($arrData[0]->No_Entidad))
					&& ($Ss_Total + $fTotalIcbper) < 700
				) {
					$iIdClienteTipoDocumentoIdentidad = '-';
					$sNombreCliente = 'vacio';
				}

				$data_cabecera = array(
					"operacion"							=> "generar_comprobante",
					"tipo_de_comprobante"               => $iTipoComprobante,
					"serie"                             => $arrData[0]->ID_Serie_Documento,
					"numero"							=> $arrData[0]->ID_Numero_Documento,
					"sunat_transaction"					=> "1",
					"cliente_tipo_de_documento"			=> $iIdClienteTipoDocumentoIdentidad,
					"cliente_numero_de_documento"		=> $arrData[0]->Nu_Documento_Identidad,
					"cliente_denominacion"              => $sNombreCliente,
					"cliente_direccion"                 => $arrData[0]->Txt_Direccion_Entidad,
					"cliente_email"                     => "",
					"cliente_email_1"                   => "",
					"cliente_email_2"                   => "",
					"fecha_de_emision"                  => $arrData[0]->Fe_Emision,
					"fecha_de_vencimiento"              => $arrData[0]->Fe_Vencimiento,
					"moneda"                            => $arrData[0]->Nu_Valor_Fe_Moneda,
					"tipo_de_cambio"                    => ($arrData[0]->Nu_Valor_Fe_Moneda == 1 ? "" : $arrData[0]->Ss_Tipo_Cambio),
					"porcentaje_de_igv"                 => $Po_IGV,
					"descuento_global"                  => "",
					"total_descuento"                   => $arrData[0]->Ss_Descuento + $Ss_Descuento_Producto,
					"total_anticipo"                    => "",
					"total_gravada"                     => $Ss_Gravada,
					"total_inafecta"                    => $Ss_Inafecto,
					"total_exonerada"                   => $Ss_Exonerada,
					"total_igv"                         => $Ss_IGV,
					"total_gratuita"                    => "",
					"total_otros_cargos"                => "",
					"total"                             => $Ss_Total + $fTotalIcbper,
					"percepcion_tipo"                   => "",
					"percepcion_base_imponible"         => "",
					"total_percepcion"                  => "",
					"total_incluido_percepcion"         => "",
					"total_impuestos_bolsas" => $fTotalIcbper,
					"detraccion"                        => ($arrData[0]->Nu_Detraccion == 0 ? false : true),
					"observaciones"                     => $arrData[0]->Txt_Glosa,
					"documento_que_se_modifica_tipo"    => $iTipoComprobanteModifica,
					"documento_que_se_modifica_serie"   => $arrData[0]->ID_Serie_Documento_Modificar,
					"documento_que_se_modifica_numero"  => $arrData[0]->ID_Numero_Documento_Modificar,
					"tipo_de_nota_de_credito"           => $iTipoNC,
					"tipo_de_nota_de_debito"            => $iTipoND,
					"enviar_automaticamente_a_la_sunat" => true,
					"enviar_automaticamente_al_cliente" => false,
					"codigo_unico"                      => $iTipoComprobante . $arrData[0]->ID_Serie_Documento . $arrData[0]->ID_Numero_Documento,
					"condiciones_de_pago"               => "",
					"medio_de_pago"                     => $sConcatenarMultiplesMedioPago,
					"placa_vehiculo"                    => "",
					"orden_compra_servicio"             => "",
					"tabla_personalizada_codigo"        => "",
					"formato_de_pdf"                    => $arrData[0]->No_Formato_PDF,
				);
				$data = array_merge($data_cabecera, $data_detalle);

				$arrParamsFE = array(
					"ruta" => $ruta,
					"token" => $token,
					"arrData" => $data,
				);
				return $this->enviarDocumentoElectronicoProveedor($arrParamsFE, $arrParams);
			} else {
				return $arrData;
			}// ./ if - else respuesta de modal del comprobante
		} else {			
			$arrData = $this->DocumentoElectronicoModel->obtenerComprobanteAnulado($arrParams);

			if ( $arrData['sStatus'] == 'success' ) {
				$arrData = $arrData['arrData'];
				$iTipoComprobante = 2;//Boleta
				if ($arrData[0]->ID_Tipo_Documento == 3)//Factura
					$iTipoComprobante = 1;
				else if ($arrData[0]->ID_Tipo_Documento == 5)//N/Crédito
					$iTipoComprobante = 3;
				else if ($arrData[0]->ID_Tipo_Documento == 6)//N/Débito
					$iTipoComprobante = 4;
								
				$ruta = $arrData[0]->Txt_FE_Ruta;
				$token = $arrData[0]->Txt_FE_Token;

				$data = array(
					"operacion" => "generar_anulacion",
					"tipo_de_comprobante" => $iTipoComprobante,
					"serie" => $arrData[0]->ID_Serie_Documento,
					"numero" => $arrData[0]->ID_Numero_Documento,
					"motivo" => $Txt_Glosa,
					"codigo_unico" => ""
				);

				$arrParamsFE = array(
					"ruta" => $ruta,
					"token" => $token,
					"arrData" => $data,
				);
				return $this->enviarDocumentoElectronicoProveedor($arrParamsFE, $arrParams);
			} else {
				return $arrData;
			}// ./ if - else respuesta de modal del comprobante
		}// ./ if - else generar formato de documento electronico
	} // ./ generarFormatoDocumentoElectronico

	private function enviarDocumentoElectronicoProveedor($arrParamsFE, $arrParams){
		$arrData = $arrParamsFE['arrData'];
		$data_json = json_encode($arrData);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $arrParamsFE['ruta']);
		curl_setopt(
			$ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Token token="'.$arrParamsFE['token'].'"',
			'Content-Type: application/json',
			)
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$respuesta = curl_exec($ch);
		curl_close($ch);
		
		$leer_respuesta = json_decode($respuesta, true);
		if (!isset($leer_respuesta['errors']) && !empty($leer_respuesta) || isset($leer_respuesta['codigo']) ) {
			if ( $arrParams['iEstadoVenta'] == 6 ) {
				$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 8 ));
			} else {
				$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 10 ));
			}

			$arrParams = array_merge( $arrParams, array( 'arrMessagePSE' => $leer_respuesta ) );
			$arrResponseEstadoDocumento = $this->cambiarEstadoDocumentoElectronico( $arrParams );
			if ( $arrResponseEstadoDocumento['sStatus'] == 'success' ) {
				if ( $arrResponseEstadoDocumento['iEstadoVenta'] == 8 && !empty($arrParams['sEmailCliente']) ) { 
					$arrParamsEmail = array_merge( $arrParams, array( 'sGenerarRespuestaJson' => false ) );
					$this->sendCorreoFacturaVentaSUNAT($arrParamsEmail);
				}
	
				return array(
					'sStatus' => !isset($leer_respuesta['codigo']) ? 'success' : 'warning',
					'sMessage' => !isset($leer_respuesta['codigo']) ? 'Venta completada' : 'Venta completada - ' . $leer_respuesta['errors'],
					'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
					'arrMessagePSE' => !isset($leer_respuesta['errors']) ? 'Comprobante aceptado' : $leer_respuesta['errors'],
					'sCodigo' => !isset($leer_respuesta['codigo']) ? '0' : $leer_respuesta['codigo'],
				);
			} else {
				return $arrResponseEstadoDocumento;
			}
		} else {
			if ( $arrParams['iEstadoVenta'] == 6 ) {
				$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 9 ));
			} else {
				$arrParams = array_merge( $arrParams, array( 'iEstadoVenta' => 11 ));
			}
			
			$arrResponseEstadoDocumento = $this->cambiarEstadoDocumentoElectronico( $arrParams );
			if ( $arrResponseEstadoDocumento['sStatus'] == 'success' ) {
				return array(
					'sStatus' => 'danger',
					'sMessage' => (!empty($leer_respuesta) ? 'Problemas al generar documento electrónico' : 'Venta guardada pero no se envio a SUNAT'),
					'iCodigoProveedorDocumentoElectronico' => $arrParams['iCodigoProveedorDocumentoElectronico'],
					'arrMessagePSE' => (!empty($leer_respuesta) ? $leer_respuesta : 'Venta guardada pero no se envio a SUNAT'),
					'sCodigo' => '-200',
				);
			} else {
				return $arrResponseEstadoDocumento;
			}
		}
	}

	public function cambiarEstadoDocumentoElectronico( $arrParams ){
		$this->db->trans_begin();
		if ($arrParams['iEstadoVenta'] == 8 || $arrParams['iEstadoVenta'] == 10){
			if (!isset($arrParams['arrMessagePSE']['codigo'])) {
				if ($arrParams['iEstadoVenta'] == 8) {
					$data = array(
						'Nu_Estado' => $arrParams['iEstadoVenta'],
						'Txt_Url_Comprobante' => $arrParams['arrMessagePSE']['enlace'],
						'Txt_Url_PDF' => $arrParams['arrMessagePSE']['enlace_del_pdf'],
						'Txt_Url_XML' => $arrParams['arrMessagePSE']['enlace_del_xml'],
						'Txt_Url_CDR' => $arrParams['arrMessagePSE']['enlace_del_cdr'],
						'Txt_QR' => $arrParams['arrMessagePSE']['cadena_para_codigo_qr'],
						'Txt_Hash' => $arrParams['arrMessagePSE']['codigo_hash'],
					);
				} else {
					$data = array(
						'Nu_Estado' => $arrParams['iEstadoVenta'],
					);
				}
			} else {
				$data = array(
					'Nu_Estado' => ($arrParams['iEstadoVenta'] == 8 ? 9 : 11),
				);
			}
		} else {
			$data = array(
				'Nu_Estado' => $arrParams['iEstadoVenta'],
			);
		}

		$this->db->update($this->table, $data, array('ID_Documento_Cabecera' => $arrParams['iIdDocumentoCabecera']));
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array(
				'status' => 'danger',
				'style_modal' => 'modal-danger',
				'message' => 'No se envió a SUNAT',
				'message_nubefact' => 'No se envió a SUNAT',
				'sStatus' => 'danger',
				'sMessage' => 'No se envió a SUNAT',
				'iCodigoProveedorDocumentoElectronico' => $arrParams['iCodigoProveedorDocumentoElectronico'],
				'arrMessagePSE' => 'No se envió a SUNAT',
				'sCodigo' => '-100',
			);
		} else {
			$this->db->trans_commit();
			return array(
				'status' => 'success',
				'style_modal' => 'modal-success',
				'message' => 'Estado cambiado satisfactoriamente',
				'message_nubefact' => 'Estado cambiado satisfactoriamente',
				'sStatus' => 'success',
				'sMessage' => 'Estado cambiado satisfactoriamente',
				'iEstadoVenta' => $arrParams['iEstadoVenta'],
				'iIdDocumentoCabecera' => $arrParams['iIdDocumentoCabecera'],
			);
		}
	}
	
	public function sendCorreoFacturaVentaSUNAT($arrParams){
		$arrData = $this->obtenerComprobante($arrParams);
		if ( $arrData['sStatus'] == 'success' ) {
			$arrData = $arrData['arrData'];

			$this->load->library('email');

			$data = array();
			
			$data["No_Documento"] = strtoupper($arrData[0]->No_Tipo_Documento) . ' ELECTRÓNICA '  . ' ' . $arrData[0]->ID_Serie_Documento . '-' . $arrData[0]->ID_Numero_Documento;
			$data["Fe_Emision"] = ToDateBD($arrData[0]->Fe_Emision);
			$data["No_Signo"] = $arrData[0]->No_Signo;
			$data["Ss_Total"] = $arrData[0]->Ss_Total;
			
			$data["No_Entidad"] = $arrData[0]->No_Entidad;
			
			$data["No_Empresa"] = $this->empresa->No_Empresa;
			$data["Nu_Documento_Identidad_Empresa"] = $this->empresa->Nu_Documento_Identidad;
			// Falta agregar sede de donde se envio el correo
			
			$data["url_comprobante"] = (!empty($arrData[0]->Txt_Url_Comprobante) ? $arrData[0]->Txt_Url_Comprobante : '');
			
			$asunto = 'COPIA DE ' . $data["No_Documento"] . ' ' . $this->empresa->No_Empresa . ' | ' . $this->empresa->Nu_Documento_Identidad;
			
			$message = $this->load->view('correos/documentos_electronicos', $data, true);
			
			$this->email->from('noreply@laesystems.com', $this->empresa->No_Empresa);//de
			
			if ( !isset($_POST['ID']) )
				$this->email->to($arrParams['sEmailCliente']);//para
			else
				$this->email->to($this->input->post('Txt_Email'));//para
				
			$this->email->subject($asunto);
			$this->email->message($message);
			if (!empty($arrData[0]->Txt_Url_PDF))
				$this->email->attach($arrData[0]->Txt_Url_PDF);
			if (!empty($arrData[0]->Txt_Url_XML))
				$this->email->attach($arrData[0]->Txt_Url_XML);
			if (!empty($arrData[0]->Txt_Url_CDR))
				$this->email->attach($arrData[0]->Txt_Url_CDR);
			$this->email->set_newline("\r\n");

			$isSend = $this->email->send();
			
			if($isSend) {
				$peticion = array(
					'status' => 'success',
					'style_modal' => 'modal-success',
					'message' => 'Correo enviado',
				);
			} else {
				$peticion = array(
					'status' => 'error',
					'style_modal' => 'modal-danger',
					'message' => 'No se pudo enviar el correo, inténtelo más tarde.',
					'sMessageErrorEmail' => $this->email->print_debugger(),
				);
			}
			if ( $arrParams['sGenerarRespuestaJson'] )
				echo json_encode($peticion);
			else
				return $peticion;
		} else {
			if ( $arrParams['sGenerarRespuestaJson'] )
				echo json_encode($arrData);
			else
				return $peticion;
		}
	}

	public function agregarMensajeRespuestaProveedorFE( $arrResponseFE, $arrParams ){
		$data = json_encode(array(
			'Proveedor' => $this->empresa->Nu_Tipo_Proveedor_FE == 1 ? 'Nubefact' : 'Sunat',
			'Enviada_SUNAT' => $arrResponseFE['sStatus'] == 'success' ? 'Si' : 'No',
			'Aceptada_SUNAT' => $arrResponseFE['sStatus'] == 'success' ? 'Si' : 'No',
			'Codigo_SUNAT' => $arrResponseFE['sCodigo'],
			'Mensaje_SUNAT' => utf8_decode($arrResponseFE['arrMessagePSE']),
			'Fecha_Registro' => dateNow('fecha_hora'),
			'Fecha_Envio' => dateNow('fecha_hora'),
		));
		$sql = "UPDATE " . $this->table . " SET Txt_Respuesta_Sunat_FE='" . $data . "' WHERE ID_Documento_Cabecera=" . $arrParams['iIdDocumentoCabecera'];
		if ( $this->db->query($sql) > 0 ) {
			return array(
				'sStatus' => 'success',
			);
		}
		return array(
			'sStatus' => 'danger',
			'sMessage' => 'Problemas al guardar mensaje proveedor FE',
		);
	}
}
