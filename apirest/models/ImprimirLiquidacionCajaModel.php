<?php
class ImprimirLiquidacionCajaModel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}
	
	public function formatoImpresionLiquidacionCaja($arrPost){
		$arrDataVentasMultiples = array();
		
		$campo_familia_item = 'FAMI.No_Familia AS No_Familia_Item,';
		$groupby_familia_item = 'FAMI.ID_Familia,';
		$orderby_familia_item = 'FAMI.No_Familia,';
		if ( $this->empresa->Nu_Imprimir_Liquidacion_Caja == 2 ) {// 2 detallado por item
			$campo_familia_item = 'ITEM.No_Producto AS No_Familia_Item,';
			$groupby_familia_item = 'ITEM.ID_Producto,';
			$orderby_familia_item = 'ITEM.No_Producto,';
		}

		$query = "
SELECT
 " . $campo_familia_item . "
 ROUND(SUM(VD.Qt_Producto), 2) AS Qt_Producto,
 MONE.No_Signo,
 ROUND(SUM(VD.Ss_Total), 2) AS Ss_Total
FROM
 caja_pos AS AC
 JOIN caja_pos AS CC ON(AC.ID_Caja_Pos = CC.ID_Enlace_Apertura_Caja_Pos)
 JOIN documento_cabecera AS VC ON(VC.ID_Empresa = CC.ID_Empresa AND VC.ID_Organizacion = CC.ID_Organizacion AND VC.ID_Matricula_Empleado=CC.ID_Matricula_Empleado)
 JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = VC.ID_Serie_Documento_PK)
 JOIN documento_detalle AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 JOIN producto AS ITEM ON(VD.ID_Producto = ITEM.ID_Producto)
 JOIN familia AS FAMI ON(ITEM.ID_Familia = FAMI.ID_Familia)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
WHERE
 AC.ID_Empresa = ".$this->empresa->ID_Empresa."
 AND AC.ID_Organizacion = ".$this->empresa->ID_Organizacion."
 AND VC.ID_Tipo_Asiento = 1
 AND SD.ID_POS > 0
 AND CC.ID_Matricula_Empleado = " . $arrPost['iIdMatriculaEmpleado'] . "
 AND CC.ID_Enlace_Apertura_Caja_Pos = " . $arrPost['iIdEnlaceAperturaCaja'] . "
 AND CC.ID_Caja_Pos = " . $arrPost['iIdEnlaceCierreCaja'] . "
 AND VC.Fe_Emision_Hora BETWEEN AC.Fe_Movimiento AND CC.Fe_Movimiento
 GROUP BY
  " . $groupby_familia_item . "
  VC.ID_Moneda
 ORDER BY
  " . $orderby_familia_item . "
  VC.ID_Moneda";
  
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);

		$arrDataVentasMultiples['VentasxFamilia'] = array();
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrDataVentasMultiples['VentasxFamilia'] = $arrResponseSQL->result();
		}

		$query = "
SELECT
 TOC.No_Tipo_Operacion_Caja,
 MONE.No_Signo,
 ROUND(SUM(MVPOS.Ss_Total), 2) AS Ss_Total,
 TOC.Nu_Tipo
FROM
 caja_pos AS CC
 JOIN caja_pos AS AC ON(AC.ID_Caja_Pos = CC.ID_Enlace_Apertura_Caja_Pos)
 JOIN caja_pos AS MVPOS ON(MVPOS.ID_Empresa = CC.ID_Empresa AND MVPOS.ID_Organizacion = CC.ID_Organizacion AND MVPOS.ID_Matricula_Empleado=CC.ID_Matricula_Empleado)
 JOIN tipo_operacion_caja AS TOC ON(TOC.ID_Tipo_Operacion_Caja = MVPOS.ID_Tipo_Operacion_Caja)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = MVPOS.ID_Moneda)
 JOIN matricula_empleado AS ME ON(ME.ID_Matricula_Empleado = MVPOS.ID_Matricula_Empleado)
 JOIN entidad AS TRAB ON(TRAB.ID_Entidad = ME.ID_Entidad)
WHERE
 MVPOS.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND MVPOS.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND MVPOS.ID_Matricula_Empleado = " . $arrPost['iIdMatriculaEmpleado'] . "
 AND MVPOS.Fe_Movimiento BETWEEN AC.Fe_Movimiento AND CC.Fe_Movimiento
 AND TOC.Nu_Tipo != 4
GROUP BY
 TOC.ID_Tipo_Operacion_Caja,
 MVPOS.ID_Moneda
ORDER BY
 TOC.No_Tipo_Operacion_Caja,
 MVPOS.ID_Moneda";

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);

		$arrDataVentasMultiples['MovimientosCaja'] = array();
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrDataVentasMultiples['MovimientosCaja'] = $arrResponseSQL->result();
		}

		$query = "
SELECT
 VC.ID_Documento_Cabecera,
 MP.No_Medio_Pago,
 MONE.No_Signo,
 VMP.Ss_Total AS Ss_Total_VMP,
 VC.Ss_Total AS Ss_Total_VC,
 MP.Nu_Tipo_Caja
FROM
 caja_pos AS AC
 JOIN caja_pos AS CC ON(AC.ID_Caja_Pos = CC.ID_Enlace_Apertura_Caja_Pos)
 JOIN documento_cabecera AS VC ON(VC.ID_Empresa = CC.ID_Empresa AND VC.ID_Organizacion = CC.ID_Organizacion AND VC.ID_Matricula_Empleado=CC.ID_Matricula_Empleado)
 JOIN documento_medio_pago AS VMP ON(VMP.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)
 JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = VC.ID_Serie_Documento_PK)
 JOIN medio_pago AS MP ON(MP.ID_Medio_Pago = VMP.ID_Medio_Pago)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
WHERE
 AC.ID_Empresa = ".$this->empresa->ID_Empresa."
 AND AC.ID_Organizacion = ".$this->empresa->ID_Organizacion."
 AND VC.ID_Tipo_Asiento = 1
 AND SD.ID_POS > 0
 AND CC.ID_Matricula_Empleado = " . $arrPost['iIdMatriculaEmpleado'] . "
 AND CC.ID_Enlace_Apertura_Caja_Pos = " . $arrPost['iIdEnlaceAperturaCaja'] . "
 AND CC.ID_Caja_Pos = " . $arrPost['iIdEnlaceCierreCaja'] . "
 AND (VC.Fe_Emision_Hora BETWEEN AC.Fe_Movimiento AND CC.Fe_Movimiento OR VMP.Fe_Emision_Hora_Pago BETWEEN AC.Fe_Movimiento AND CC.Fe_Movimiento)
ORDER BY
  VC.ID_Documento_Cabecera";

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);

		$arrDataVentasMultiples['VentasGeneralesEfectivo'] = array();
		if ( $arrResponseSQL->num_rows() > 0 ){
			$fTotalEfectivoxDocumento = 0.00;
			$fTotalOtrosMPxDocumento = 0.00;
			$fTotalxDocumento = 0.00;
			$fSumTotalEfectivo = 0.00;
			$iIdDocumentoCabecera = 0;
			foreach ($arrResponseSQL->result() as $row){
				if ( $iIdDocumentoCabecera != $row->ID_Documento_Cabecera ) {
					$iIdDocumentoCabecera = $row->ID_Documento_Cabecera;
					$fTotalEfectivoxDocumento = 0.00;
					$fTotalOtrosMPxDocumento = 0.00;
					$fTotalxDocumento = 0.00;
				}
				$fTotalxDocumento += $row->Ss_Total_VMP;
				if ( $row->Nu_Tipo_Caja != 0 )
					$fTotalOtrosMPxDocumento += $row->Ss_Total_VMP;
				if ( $row->Nu_Tipo_Caja == 0 )
					$fTotalEfectivoxDocumento += $row->Ss_Total_VMP;
				if ( $fTotalxDocumento > $row->Ss_Total_VC )
					$fSumTotalEfectivo += $row->Ss_Total_VC - $fTotalOtrosMPxDocumento;
				else if ( $fTotalxDocumento == $row->Ss_Total_VC )
					$fSumTotalEfectivo += $fTotalEfectivoxDocumento;
			}
			$arrDataVentasMultiples['VentasGeneralesEfectivo'][0] = (object)array(
				'No_Medio_Pago' => 'Efectivo',
				'No_Signo' => 'S/',
				'Ss_Total' => $fSumTotalEfectivo,
				'Nu_Tipo_Caja' => 0,
			);
		}

		$query = "
SELECT
 MP.No_Medio_Pago,
 MONE.No_Signo,
 ROUND(SUM(VMP.Ss_Total), 2) AS Ss_Total,
 MP.Nu_Tipo_Caja
FROM
 caja_pos AS AC
 JOIN caja_pos AS CC ON(AC.ID_Caja_Pos = CC.ID_Enlace_Apertura_Caja_Pos)
 JOIN documento_cabecera AS VC ON(VC.ID_Empresa = CC.ID_Empresa AND VC.ID_Organizacion = CC.ID_Organizacion AND VC.ID_Matricula_Empleado=CC.ID_Matricula_Empleado)
 JOIN documento_medio_pago AS VMP ON(VMP.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)
 JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = VC.ID_Serie_Documento_PK)
 JOIN medio_pago AS MP ON(MP.ID_Medio_Pago = VMP.ID_Medio_Pago)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
WHERE
 AC.ID_Empresa = ".$this->empresa->ID_Empresa."
 AND AC.ID_Organizacion = ".$this->empresa->ID_Organizacion."
 AND VC.ID_Tipo_Asiento = 1
 AND SD.ID_POS > 0
 AND CC.ID_Matricula_Empleado = " . $arrPost['iIdMatriculaEmpleado'] . "
 AND CC.ID_Enlace_Apertura_Caja_Pos = " . $arrPost['iIdEnlaceAperturaCaja'] . "
 AND CC.ID_Caja_Pos = " . $arrPost['iIdEnlaceCierreCaja'] . "
 AND (VC.Fe_Emision_Hora BETWEEN AC.Fe_Movimiento AND CC.Fe_Movimiento OR VMP.Fe_Emision_Hora_Pago BETWEEN AC.Fe_Movimiento AND CC.Fe_Movimiento)
 AND MP.Nu_Tipo_Caja != 0
GROUP BY
 MP.ID_Medio_Pago,
 VC.ID_Moneda
ORDER BY
 MP.No_Medio_Pago,
 VC.ID_Moneda";

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);

		$arrDataVentasMultiples['VentasGeneralesSinEfectivo'] = array();
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrDataVentasMultiples['VentasGeneralesSinEfectivo'] = $arrResponseSQL->result();
		}

		$arrDataVentasMultiples['VentasGenerales'] = array_merge($arrDataVentasMultiples['VentasGeneralesEfectivo'], $arrDataVentasMultiples['VentasGeneralesSinEfectivo']);

		// Obtener totales a liquidar y depositar
		$query = "
SELECT
 CC.ID_POS,
 TRAB.No_Entidad,
 AC.Fe_Movimiento AS Fe_Apertura,
 CC.Fe_Movimiento AS Fe_Cierre,
 SUM(CC.Ss_Expectativa) AS Ss_Expectativa,
 SUM(CC.Ss_Total) AS Ss_Total
FROM
 caja_pos AS AC
 JOIN caja_pos AS CC ON(AC.ID_Caja_Pos = CC.ID_Enlace_Apertura_Caja_Pos)
 JOIN matricula_empleado AS ME ON(ME.ID_Matricula_Empleado = CC.ID_Matricula_Empleado)
 JOIN entidad AS TRAB ON(TRAB.ID_Entidad = ME.ID_Entidad)
WHERE
 CC.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND CC.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND CC.ID_Matricula_Empleado=" . $arrPost['iIdMatriculaEmpleado'] . "
 AND CC.ID_Caja_Pos=" . $arrPost['iIdEnlaceCierreCaja'];

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);

		$arrDataVentasMultiples['TotalesLiquidacionCaja'] = array();
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrDataVentasMultiples['TotalesLiquidacionCaja'] = $arrResponseSQL->result();
		}

		if (
			count( $arrDataVentasMultiples['VentasxFamilia'] ) > 0
			|| count( $arrDataVentasMultiples['MovimientosCaja'] ) > 0
			|| count( $arrDataVentasMultiples['VentasGenerales'] ) > 0
		){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrDataVentasMultiples,
			);
		}
        
        return array(
            'sStatus' => 'warning',
            'sMessage' => 'No se encontro registro',
        );
	}
}
