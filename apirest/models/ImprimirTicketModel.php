<?php
class ImprimirTicketModel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}
	
	public function formatoImpresionTicket($ID_Documento_Cabecera){
		$query = "
SELECT
 CONFI.No_Logo_Empresa,
 CONFI.Nu_Celular_Empresa,
 CONFI.Nu_Telefono_Empresa,
 CONFI.Txt_Email_Empresa,
 EMP.Nu_Documento_Identidad AS Nu_Documento_Identidad_Empresa,
 EMP.No_Empresa,
 EMP.Txt_Direccion_Empresa,
 TDOCU.No_Tipo_Documento,
 VC.ID_Documento_Cabecera,
 VD.ID_Documento_Detalle,
 VC.Fe_Emision_Hora,
 VC.Fe_Vencimiento,
 VC.ID_Tipo_Documento,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento,
 TDOCUIDE.No_Tipo_Documento_Identidad_Breve,
 CLI.Nu_Documento_Identidad,
 CLI.No_Entidad,
 PRO.No_Producto,
 LAB.No_Laboratorio_Breve,
 PRO.Qt_CO2_Producto,
 PRO.ID_Impuesto_Icbper,
 VD.Qt_Producto,
 ROUND(VD.Ss_Precio, 3) AS ss_precio_unitario,
 IMP.Nu_Tipo_Impuesto,
 ROUND(VD.Ss_Descuento + VD.Ss_Descuento_Impuesto, 2) AS Ss_Descuento_Producto,
 VD.Ss_Total AS Ss_Total_Producto,
 ICDOCU.Ss_Impuesto AS Ss_Impuesto_Producto,
 TDOCU.Nu_Impuesto,
 ICDOCU.Po_Impuesto,
 ROUND(VC.Ss_Total, 2) AS Ss_Total,
 MONE.No_Moneda,
 MONE.No_Signo,
 CONFI.No_Dominio_Empresa,
 EMPLE.No_Entidad AS No_Empleado,
 TDRECEPCION.No_Descripcion AS No_Recepcion,
 VD.Txt_Nota AS Txt_Nota_Item,
 VC.Txt_Hash,
 VC.Txt_QR,
 MP.No_Medio_Pago,
 VC.Ss_Descuento AS Ss_Descuento_Total,
 UM.Nu_Sunat_Codigo AS nu_codigo_unidad_medida_sunat,
 MOZO.No_Entidad AS No_Mesero,
 MESA.No_Mesa,
 VC.Txt_QR,
 pos.Nu_Pos,
 VC.Fe_Entrega,
 VC.Ss_Total_Saldo,
 EMP.Nu_Tipo_Proveedor_FE,
 ORG.Nu_Estado_Sistema,
 VC.Txt_Glosa AS Txt_Glosa_Global,
 ALMA.Txt_Direccion_Almacen
FROM
 documento_cabecera AS VC
 JOIN empresa AS EMP ON(EMP.ID_Empresa = VC.ID_Empresa)
 JOIN configuracion AS CONFI ON(CONFI.ID_Empresa = EMP.ID_Empresa)
 JOIN organizacion AS ORG ON(ORG.ID_Empresa = EMP.ID_Empresa)
 JOIN almacen AS ALMA ON(VC.ID_Organizacion = ALMA.ID_Organizacion AND VC.ID_Almacen = ALMA.ID_Almacen)
 JOIN entidad AS CLI ON (CLI.ID_Entidad = VC.ID_Entidad)
 JOIN tipo_documento_identidad AS TDOCUIDE ON (TDOCUIDE.ID_Tipo_Documento_Identidad = CLI.ID_Tipo_Documento_Identidad)
 JOIN tipo_documento AS TDOCU ON (TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
 JOIN documento_detalle AS VD ON (VD.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)
 JOIN impuesto_cruce_documento AS ICDOCU ON (ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
 JOIN impuesto AS IMP ON (IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
 JOIN producto AS PRO ON (PRO.ID_Producto = VD.ID_Producto)
 LEFT JOIN laboratorio AS LAB ON (LAB.ID_Laboratorio = PRO.ID_Laboratorio)
 JOIN moneda AS MONE ON (VC.ID_Moneda = MONE.ID_Moneda)
 JOIN matricula_empleado AS MEMPLE ON(VC.ID_Matricula_Empleado = MEMPLE.ID_Matricula_Empleado)
 JOIN entidad AS EMPLE ON(MEMPLE.ID_Entidad = EMPLE.ID_Entidad)
 JOIN tabla_dato AS TDRECEPCION ON(TDRECEPCION.Nu_Valor = VC.Nu_Tipo_Recepcion AND TDRECEPCION.No_Relacion = 'Tipos_recepcion')
 JOIN medio_pago AS MP ON(MP.ID_Medio_Pago = VC.ID_Medio_Pago)
 JOIN unidad_medida AS UM ON (UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
 LEFT JOIN pedido_cabecera AS VTEMPC ON (VTEMPC.ID_Pedido_Cabecera = VC.ID_Pedido_Cabecera)
 LEFT JOIN entidad AS MOZO ON(VTEMPC.ID_Mesero = MOZO.ID_Entidad)
 LEFT JOIN mesa AS MESA ON(MESA.ID_Mesa = VTEMPC.ID_Mesa)
 JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
 LEFT JOIN pos ON(pos.ID_Empresa = VC.ID_Empresa AND pos.ID_Organizacion = VC.ID_Organizacion AND pos.ID_POS = SD.ID_POS)
WHERE
 VC.ID_Documento_Cabecera = " . $ID_Documento_Cabecera;
		return $this->db->query($query)->result();
	}
	
	public function formatoImpresionTicketOrden($ID_Venta_Temporal_Cabecera){
		$query = "
SELECT
	EMP.Nu_Documento_Identidad AS Nu_Documento_Identidad_Empresa,
	EMP.No_Empresa,
	EMP.Txt_Direccion_Empresa,
	VC.ID_Pedido_Cabecera AS ID_Venta_Temporal_Cabecera,
	VC.Fe_Emision_Hora,
    EMPLE.No_Entidad AS No_Empleado,
	MOZO.No_Entidad AS No_Mesero,
	MESA.No_Mesa,
	PRO.No_Producto,
	VD.Qt_Producto,
	ROUND(VD.Ss_Precio, 3) AS ss_precio_unitario,
	IMP.Nu_Tipo_Impuesto,
	0 AS Ss_Descuento,
	VD.Ss_SubTotal AS Ss_SubTotal_Producto,
	ICDOCU.Ss_Impuesto AS Ss_Impuesto_Producto,
    ICDOCU.Po_Impuesto,
    ROUND(VC.Ss_Total, 2) AS Ss_Total,
    MONE.No_Moneda,
    MONE.No_Signo,
    CONFI.No_Dominio_Empresa,
    TDRECEPCION.No_Descripcion AS No_Recepcion,
    VD.Txt_Nota
FROM
	pedido_cabecera AS VC
	JOIN empresa AS EMP ON (EMP.ID_Empresa = VC.ID_Empresa)
	JOIN configuracion AS CONFI ON (CONFI.ID_Empresa = EMP.ID_Empresa)
	JOIN pedido_detalle AS VD ON (VD.ID_Pedido_Cabecera = VC.ID_Pedido_Cabecera)
    JOIN impuesto_cruce_documento AS ICDOCU ON (ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
    JOIN impuesto AS IMP ON (IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
	JOIN producto AS PRO ON (PRO.ID_Producto = VD.ID_Producto)
	JOIN moneda AS MONE ON (VC.ID_Moneda = MONE.ID_Moneda)
	JOIN matricula_empleado AS MEMPLE ON(VC.ID_Matricula_Empleado = MEMPLE.ID_Matricula_Empleado)
	JOIN entidad AS EMPLE ON(MEMPLE.ID_Entidad = EMPLE.ID_Entidad)
	JOIN entidad AS MOZO ON(VC.ID_Mesero = MOZO.ID_Entidad)
	JOIN mesa AS MESA ON(MESA.ID_Mesa = VC.ID_Mesa)
	JOIN tabla_dato AS TDRECEPCION ON(TDRECEPCION.Nu_Valor = VC.Nu_Tipo_Recepcion AND TDRECEPCION.No_Relacion = 'Tipos_recepcion')
WHERE
	VC.ID_Pedido_Cabecera = " . $ID_Venta_Temporal_Cabecera;
		return $this->db->query($query)->result();
	}
	
	public function formatoImpresionTicketComandaLavado($arrPost){
		$iIdDocumentoCabecera = $arrPost['iIdDocumentoCabecera'];
		$query = "
SELECT
 CONFI.No_Logo_Empresa,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento,
 TEEPEDIDOLAVA.No_Descripcion AS No_Estado_Pedido_Lavado, 
 VC.Fe_Emision_Hora,
 VC.Fe_Entrega,
 CLI.Nu_Documento_Identidad,
 CLI.No_Entidad,
 CLI.Nu_Celular_Entidad,
 VD.Qt_Producto,
 PRO.No_Producto,
 VD.Txt_Nota AS Txt_Nota_Item,
 EMPLE.No_Entidad AS No_Empleado
FROM
 documento_cabecera AS VC
 JOIN configuracion AS CONFI ON(CONFI.ID_Empresa = VC.ID_Empresa)
 JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
 JOIN documento_detalle AS VD ON(VD.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)
 JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
 JOIN matricula_empleado AS MEMPLE ON(VC.ID_Matricula_Empleado = MEMPLE.ID_Matricula_Empleado)
 JOIN entidad AS EMPLE ON(MEMPLE.ID_Entidad = EMPLE.ID_Entidad)
 JOIN tabla_dato AS TEEPEDIDOLAVA ON(TEEPEDIDOLAVA.Nu_Valor = VC.Nu_Transporte_Lavanderia_Hoy AND TEEPEDIDOLAVA.No_Relacion = 'Tipos_EstadoEnvioPedidoLavado')
WHERE
 VC.ID_Documento_Cabecera = " . $iIdDocumentoCabecera;
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros',
		);
	}
}
