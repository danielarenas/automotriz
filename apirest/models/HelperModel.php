<?php
class HelperModel extends CI_Model{

	public function __construct(){ 
		parent::__construct();
	} 

	public function getTiposDocumentosModificar($Nu_Tipo_Filtro){
		$this->db->where('Nu_Estado', 1);
		$this->db->where('Nu_Enlace_Modificar', 1);
		if ($Nu_Tipo_Filtro == '1')//Venta
			$this->db->where('Nu_Venta', 1);
		if ($Nu_Tipo_Filtro == '2')//Compra
			$this->db->where('Nu_Compra', 1);
		$this->db->order_by('No_Tipo_Documento');
		return $this->db->get('tipo_documento')->result();
	}
	
	public function getSeriesDocumentoModificar($arrPost){
		$iIDOrganizacion = $arrPost['ID_Organizacion'];
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		
		$query = "SELECT * FROM serie_documento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $iIDOrganizacion . " AND ID_Tipo_Documento = " . $iIDTipoDocumento . " AND Nu_Estado = 1";
		return $this->db->query($query)->result();
	}
	
	public function getSeriesDocumentoModificarxAlmacen($arrPost){
		$iIDOrganizacion = $arrPost['ID_Organizacion'];
		$iIdAlmacen = $arrPost['ID_Almacen'];
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		
		$query = "SELECT * FROM serie_documento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Organizacion = " . $iIDOrganizacion . " AND ID_Almacen = " . $iIdAlmacen . " AND ID_Tipo_Documento = " . $iIDTipoDocumento . " AND Nu_Estado = 1";
		return $this->db->query($query)->result();
	}
	
	public function getMotivosReferenciaModificar($ID_Tipo_Documento){
		$this->db->order_by('No_Descripcion');
		$this->db->where('No_Relacion', 'Motivos_Referencia');
		$this->db->where('No_Class', $ID_Tipo_Documento);
		return $this->db->get('tabla_dato')->result();
	}

	public function documentExistVerify($ID_Documento_Guardado, $ID_Tipo_Documento_Modificar, $ID_Serie_Documento_Modificar, $ID_Numero_Documento_Modificar, $arrPost){
		$iIdEntidad = $arrPost['iIdEntidad'];
		// Cliente existente < 700 IGV y es Boleta
		if ($arrPost['iTipoCliente'] == '3') {
			$query = "SELECT ID_Entidad FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND No_Entidad LIKE '%clientes varios%' LIMIT 1"; //1 = ID_Entidad -> Cliente varios
			if (!$this->db->simple_query($query)){
				$this->db->trans_rollback();
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos de clientes varios',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ($arrResponseSQL->num_rows() > 0){
				$arrData = $arrResponseSQL->result();
				$iIdEntidad = $arrData[0]->ID_Entidad;
			} else {
				$this->db->trans_rollback();
				return array(
					'sStatus' => 'warning',
					'sMessage' => 'No se encontro clientes varios',
					'sClassModal' => 'modal-warning',
				);
			}
		}
		// Cliente existente < 700 IGV y es Boleta
		$arrData = $this->db->query("SELECT ID_Documento_Cabecera, Nu_Presupuesto, Nu_Estado, ID_Producto FROM documento_cabecera WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Documento = " . $ID_Tipo_Documento_Modificar . " AND ID_Serie_Documento = '" . $ID_Serie_Documento_Modificar . "' AND ID_Numero_Documento = " . $ID_Numero_Documento_Modificar . " AND ID_Entidad = " . $iIdEntidad . " LIMIT 1")->result();
		if(!empty($arrData)){
			$ID_Documento_Cabecera_Enlace = $arrData[0]->ID_Documento_Cabecera;
			if ($ID_Documento_Guardado == 0 && $this->db->query("SELECT COUNT(*) existe FROM documento_enlace WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Documento_Cabecera_Enlace = " . $ID_Documento_Cabecera_Enlace . " LIMIT 1")->row()->existe == 1){
				return array('status' => 'warning', 'style_panel' => 'panel-warning', 'style_p' => 'text-warning', 'style_modal' => 'modal-warning', 'message' => 'El documento ya se encuentra enlazado', 'ID_Documento_Cabecera_Enlace' => $ID_Documento_Cabecera_Enlace);
			} else {
				return array(
					'status' => 'success',
					'style_panel' => 'panel-success',
					'style_p' => 'text-success',
					'style_modal' => 'modal-succes',
					'message' => 'Documento encontrado',
					'ID_Documento_Cabecera_Enlace' => $ID_Documento_Cabecera_Enlace,
					'Nu_Presupuesto' => $arrData[0]->Nu_Presupuesto,
					'Nu_Estado' => $arrData[0]->Nu_Estado,
				);
			}
		}
		return array('status' => 'error', 'style_panel' => 'panel-danger', 'style_p' => 'text-danger', 'style_modal' => 'modal-danger', 'message' => 'No existe documento');
	}

	public function getEmpresas(){
		$query = "SELECT
 ID_Empresa,
 No_Empresa,
 Nu_Tipo_Proveedor_FE,
 TIPOPROVEEDORFE.No_Descripcion AS No_Descripcion_Proveedor_FE,
 TIPOECOMMERCE.Nu_Valor AS Nu_Tipo_Ecommerce
FROM
 empresa AS EMP
 LEFT JOIN tabla_dato AS TIPOPROVEEDORFE ON(TIPOPROVEEDORFE.Nu_Valor=EMP.Nu_Tipo_Proveedor_FE AND TIPOPROVEEDORFE.No_Relacion = 'Tipos_Proveedor_FE')
 LEFT JOIN tabla_dato AS TIPOECOMMERCE ON(TIPOECOMMERCE.ID_Tabla_Dato=EMP.Nu_Tipo_Ecommerce_Empresa AND TIPOECOMMERCE.No_Relacion = 'Tipos_Ecommerce_Empresa')
WHERE
 Nu_Estado = 1
ORDER BY
 No_Empresa;";
		return $this->db->query($query)->result();
	}
	
	public function getOrganizaciones($arrPost){
		$where_empresa = '';
		if ( isset($arrPost['iIdEmpresa']) ) {
			$where_empresa = ' AND ID_Empresa = ' . $arrPost['iIdEmpresa'];
		}
		$query = "SELECT ID_Organizacion, No_Organizacion FROM organizacion WHERE Nu_Estado = 1 " . $where_empresa . " ORDER BY No_Organizacion";
		return $this->db->query($query)->result();
	}
	
	public function getAlmacenes($arrPost){
		$where_id_organizacion = 'AND ID_Organizacion=' . $this->empresa->ID_Organizacion;
		if ( isset( $arrPost['iIdOrganizacion'] ) ) {
			$where_id_organizacion = 'AND ID_Organizacion=' . $arrPost['iIdOrganizacion'];
		}
		$query = "SELECT
ID_Almacen,
No_Almacen,
Txt_Direccion_Almacen
FROM
almacen
WHERE
Nu_Estado = 1
" . $where_id_organizacion . "
ORDER BY
No_Almacen;";
		return $this->db->query($query)->result();
	}
	
	public function getAlmacenesEmpresa(){
		$query = "SELECT ID_Almacen, No_Almacen FROM almacen WHERE ID_Organizacion IN (SELECT ID_Organizacion FROM organizacion WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Estado = 1) AND Nu_Estado = 1 ORDER BY No_Almacen";
		return $this->db->query($query)->result();
	}
	
	public function getGrupos($arrPost){
		$where_empresa = '';
		if ( isset($arrPost['iIdEmpresa']) ) {
			$where_empresa = ' AND ID_Empresa=' . $arrPost['iIdEmpresa'];
		}
		$where_organizacion = '';
		if ( isset($arrPost['iIdOrganizacion']) ) {
			$where_organizacion = ' AND ID_Organizacion=' . $arrPost['iIdOrganizacion'];
		}
		$query = "SELECT ID_Grupo, No_Grupo FROM grupo WHERE ID_Grupo>2 " . $where_empresa . $where_organizacion . " AND Nu_Estado=1 ORDER BY No_Grupo";
		return $this->db->query($query)->result();
	}
	
	public function getMonedas(){
		$query = "SELECT ID_Moneda, No_Moneda, No_Signo FROM moneda WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Estado = 1 ORDER BY ID_Moneda";
		return $this->db->query($query)->result();
	}

	public function getPaises(){
		$this->db->where('Nu_Estado', 1);
		$this->db->order_by('No_Pais');
		return $this->db->get('pais')->result();
	}

	public function getDepartamentos($ID_Pais){
		$this->db->where('Nu_Estado', 1);
	    $this->db->where('ID_Pais', $ID_Pais);
		$this->db->order_by('No_Departamento');
		return $this->db->get('departamento')->result();
	}
	
	public function getProvincias($ID_Departamento){
		$this->db->where('Nu_Estado', 1);
	    $this->db->where('ID_Departamento', $ID_Departamento);
		$this->db->order_by('No_Provincia');
		return $this->db->get('provincia')->result();
	}

	public function getDistritos($ID_Provincia){
		$this->db->where('Nu_Estado', 1);
		if (!empty($ID_Provincia))
	    	$this->db->where('ID_Provincia', $ID_Provincia);
		$this->db->order_by('No_Distrito');
		return $this->db->get('distrito')->result();
	}

	public function getTiposSexo(){
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Tipos_Sexo');
		return $this->db->get('tabla_dato')->result();
	}

// OC-modal-lunes - Inicio 
public function Cust_getRelacionTablaMultiplePresupuestoAndOI($arrPost){
								
	$query = "SELECT
	OI.ID_Orden_Ingreso,
	TD.No_Tipo_Documento,
	SD.ID_Serie_Documento,
	OI.ID_Numero_Documento,
	TDESTADO.No_Descripcion AS No_Descripcion_Estado,
	OI.Fe_Entrega_Tentativa,
	OIE.No_Etapa,
	OIE.No_Class_Etapa,
	OI.No_Kilometraje
	FROM
	relacion_tabla AS RT
	JOIN orden_ingreso AS OI ON(OI.ID_Orden_Ingreso = RT.ID_Relacion_Enlace_Tabla)
	JOIN orden_ingreso_etapa AS OIE ON(OIE.ID_Etapa = OI.ID_Etapa)
	JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = OI.ID_Area_Ingreso)
	JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = OI.ID_Serie_Documento)
	JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = OI.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoOrdenIngreso')
	WHERE RT.Nu_Relacion_Datos = " . $arrPost['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrPost['iIdOrigenTabla'] . " LIMIT 1";
			return $this->db->query($query)->result();
		}
// OC-modal-lunes - Fin  


	public function getSeriesDocumento($arrPost){
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		
		$query = "SELECT * FROM
serie_documento
WHERE
 ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND ID_Tipo_Documento = " . $iIDTipoDocumento . "
 AND Nu_Estado = 1
 AND ID_POS IS NULL
ORDER BY
 ID_Serie_Documento";
		return $this->db->query($query)->result();
	}

	public function getSeriesDocumentoArray($arrPost){
		$iIDTipoDocumento = implode(",", $arrPost['ID_Tipo_Documento']);
		$whereID_Tipo_Documento = " AND serie_documento.ID_Tipo_Documento IN ($iIDTipoDocumento)";
		$join = 'JOIN tipo_documento ON tipo_documento.ID_Tipo_Documento = serie_documento.ID_Tipo_Documento';
		$query =
			"SELECT * FROM
				serie_documento
			$join
			WHERE
				serie_documento.ID_Empresa = " . $this->empresa->ID_Empresa . "
				AND serie_documento.ID_Organizacion = " . $this->empresa->ID_Organizacion .
				$whereID_Tipo_Documento . "
				AND serie_documento.Nu_Estado = 1
				AND serie_documento.ID_POS IS NULL
			ORDER BY
				serie_documento.ID_Serie_Documento";
		$result = $this->db->query($query)->result();
		$group = [];
		foreach ($result as $row) {
			if (isset($group[$row->ID_Tipo_Documento])) {
				$group[$row->ID_Tipo_Documento]['items'][] = $row;
			} else {
				$group[$row->ID_Tipo_Documento]['name'] = $row->No_Tipo_Documento_Breve;
				$group[$row->ID_Tipo_Documento]['items'][] = $row;
			}
		}
		return array_values($group);
	}

	public function getSeriesDocumentoxAlmacen($arrPost){
		$iIdAlmacen = $arrPost['ID_Almacen'];
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		
		$query = "SELECT * FROM serie_documento
WHERE
 ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND ID_Almacen = " . $iIdAlmacen . "
 AND ID_Tipo_Documento = " . $iIDTipoDocumento . "
 AND Nu_Estado = 1
 AND ID_POS IS NULL
ORDER BY
 ID_Serie_Documento";
		return $this->db->query($query)->result();
	}

	public function getSeriesDocumentoPuntoVenta($arrPost){	
		$query = "
SELECT
 ID_POS,
 ID_Serie_Documento
FROM
 serie_documento
WHERE
 ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND ID_Tipo_Documento = " . $arrPost['ID_Tipo_Documento'] . "
 AND Nu_Estado = 1
 AND ID_POS > 0
ORDER BY
 ID_Serie_Documento";
		return $this->db->query($query)->result();
	}

	public function getSeriesEmpresaOrgAlmacenDocumentoOficinaPuntoVenta($arrPost){
		$iIdEmpresa = $arrPost['iIdEmpresa'];
		$iIdOrganizacion = $arrPost['iIdOrganizacion'];
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		
		$where_almacen = (isset($arrPost['iIdAlmacen']) && $arrPost['iIdAlmacen'] > 0 ? ' AND ID_Almacen=' . $arrPost['iIdAlmacen'] : '');

		$query = "
SELECT
 *
FROM
serie_documento
WHERE
 ID_Empresa = " . $iIdEmpresa . "
 AND ID_Organizacion = " . $iIdOrganizacion . "
 " . $where_almacen . "
 AND ID_Tipo_Documento = " . $iIDTipoDocumento . "
 AND Nu_Estado = 1
ORDER BY
 ID_Serie_Documento";
		return $this->db->query($query)->result();
	}

	public function getSeriesDocumentoOficinaPuntoVenta($arrPost){
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		
		$query = "SELECT * FROM
serie_documento
WHERE
 ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND ID_Tipo_Documento = " . $iIDTipoDocumento . "
 AND Nu_Estado = 1
ORDER BY
 ID_Serie_Documento";
		return $this->db->query($query)->result();
	}

	public function getPuntoVenta($arrPost){
		$query = "
SELECT
 ID_POS
FROM
 serie_documento
WHERE
 ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND Nu_Estado = 1
 AND ID_POS > 0
GROUP BY
 ID_POS
ORDER BY
 ID_POS";
		return $this->db->query($query)->result();
	}
	
	public function getNumeroDocumento($arrPost){
		$iIDOrganizacion = $arrPost['ID_Organizacion'];
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		$iIDSerieDocumento = $arrPost['ID_Serie_Documento'];
		
		$query = "
SELECT
 Nu_Numero_Documento AS ID_Numero_Documento
FROM
 serie_documento
WHERE
 ID_Empresa = " . $this->user->ID_Empresa . "
 AND ID_Organizacion = " . $iIDOrganizacion . "
 AND ID_Tipo_Documento = " . $iIDTipoDocumento . "
 AND ID_Serie_Documento = '" . $iIDSerieDocumento . "'
LIMIT 1";
		return $this->db->query($query)->row();
	}
	
	public function getNumeroDocumentoxAlmacen($arrPost){
		$iIDOrganizacion = $arrPost['ID_Organizacion'];
		$iIdAlmacen = $arrPost['ID_Almacen'];
		$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
		$iIDSerieDocumento = $arrPost['ID_Serie_Documento'];
		
		$query = "
SELECT
 Nu_Numero_Documento AS ID_Numero_Documento
FROM
 serie_documento
WHERE
 ID_Empresa = " . $this->user->ID_Empresa . "
 AND ID_Organizacion = " . $iIDOrganizacion . "
 AND ID_Almacen = " . $iIdAlmacen . "
 AND ID_Tipo_Documento = " . $iIDTipoDocumento . "
 AND ID_Serie_Documento = '" . $iIDSerieDocumento . "'
LIMIT 1";
		return $this->db->query($query)->row();
	}
	
	public function getStockProducto($arrParams){
		$Fe_Buscar = $arrParams['fYear'] . '-' . $arrParams['fMonth'];

		$query = "
SELECT
SUM(K.Qt_Producto) AS Qt_Producto
FROM
movimiento_inventario AS K
JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
K.ID_Empresa = " . $this->empresa->ID_Empresa . "
AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
AND K.ID_Almacen = " . $arrParams['ID_Almacen'] . "
AND CONCAT(YEAR(CVCAB.Fe_Emision), '-', MONTH(CVCAB.Fe_Emision)) < '" . $Fe_Buscar . "'
AND TMOVI.Nu_Tipo_Movimiento = 0
AND K.ID_Producto = " . $arrParams['ID_Producto'];
		$row_cantidad_entrada = $this->db->query($query)->row();
		
		$query = "
SELECT
SUM(K.Qt_Producto) AS Qt_Producto
FROM
movimiento_inventario AS K
JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
K.ID_Empresa = " . $this->empresa->ID_Empresa . "
AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
AND K.ID_Almacen = " . $arrParams['ID_Almacen'] . "
AND CONCAT(YEAR(CVCAB.Fe_Emision), '-', MONTH(CVCAB.Fe_Emision)) < '" . $Fe_Buscar . "'
AND TMOVI.Nu_Tipo_Movimiento = 1
AND K.ID_Producto = " . $arrParams['ID_Producto'];
		$row_cantidad_salida = $this->db->query($query)->row();
		
		return $row_cantidad_entrada->Qt_Producto - $row_cantidad_salida->Qt_Producto;
	}

	public function getStockProductoxFechaInicioyFin($arrParams){
		$query = "
SELECT
SUM(K.Qt_Producto) AS Qt_Producto
FROM
movimiento_inventario AS K
JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
K.ID_Empresa = " . $this->empresa->ID_Empresa . "
AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
AND K.ID_Almacen = " . $arrParams['ID_Almacen'] . "
AND CVCAB.Fe_Emision < '" . $arrParams['dInicio'] . "'
AND TMOVI.Nu_Tipo_Movimiento = 0
AND K.ID_Producto = " . $arrParams['ID_Producto'];
		$row_cantidad_entrada = $this->db->query($query)->row();
		
		$query = "
SELECT
SUM(K.Qt_Producto) AS Qt_Producto
FROM
movimiento_inventario AS K
JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
WHERE
K.ID_Empresa = " . $this->empresa->ID_Empresa . "
AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
AND K.ID_Almacen = " . $arrParams['ID_Almacen'] . "
AND CVCAB.Fe_Emision < '" . $arrParams['dInicio'] . "'
AND TMOVI.Nu_Tipo_Movimiento = 1
AND K.ID_Producto = " . $arrParams['ID_Producto'];
		$row_cantidad_salida = $this->db->query($query)->row();
		
		return $row_cantidad_entrada->Qt_Producto - $row_cantidad_salida->Qt_Producto;
	}
	
	// M011 - I 
	public function getAreaAlmacenProducto(){
		//$query = "SELECT nu_valor, no_descripcion from tabla_dato WHERE no_relacion='Area_de_Almacen' ORDER BY nu_valor asc";
		//return $this->db->query($query)->result();
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Area_de_Almacen');
		return $this->db->get('tabla_dato')->result();
	}
	// M011 - F 

	public function getTiposProducto(){
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Tipos_Item');
		return $this->db->get('tabla_dato')->result();
	}
	
// M02 - INICIO
	
public function getIdDocumento_x_SerieBolFact($arrPost){
		
	$iIDTipoDocumento = $arrPost['ID_Tipo_Documento'];
	$iIDSerieDocumento = $arrPost['ID_Serie_Documento'];
	$iIDNumeroDocumento = $arrPost['ID_Numero_Documento'];
	
	$query = "SELECT ID_Documento_Cabecera 
	FROM documento_cabecera 
	WHERE ID_Tipo_Documento = " . $iIDTipoDocumento . "  
	AND ID_Serie_Documento= '" . $iIDSerieDocumento . "'  
	AND ID_Numero_Documento= '" . $iIDNumeroDocumento . "' ";

	
	return $this->db->query($query)->row();
}

// M02 - FIN 


	public function getTiposExistenciaProducto(){
		$query = "SELECT ID_Tipo_Producto, No_Tipo_Producto FROM tipo_producto ORDER BY Nu_Orden";
		return $this->db->query($query)->result();
	}
	
	public function getRubros(){
		$query = "SELECT
R.ID_Rubro,
R.No_Rubro,
IMPDOC.Ss_Impuesto,
IMP.Nu_Tipo_Impuesto
FROM
rubro AS R
JOIN impuesto AS IMP ON (IMP.ID_Impuesto = R.ID_Impuesto)
JOIN impuesto_cruce_documento AS IMPDOC ON (IMPDOC.ID_Impuesto = IMP.ID_Impuesto)
WHERE
IMPDOC.Nu_Estado = 1
ORDER BY
R.No_Rubro;";
		return $this->db->query($query)->result();
	}
	
	public function getTiposDocumentoIdentidad(){
		$this->db->where('Nu_Estado', 1);
		$this->db->order_by('Nu_Orden');
		return $this->db->get('tipo_documento_identidad')->result();
	}
	
		// M021 -  FIX - INICIO   
			
		public function getClientesCbo(){
			 $query="SELECT ID_entidad,nu_documento_identidad,no_entidad FROM entidad ORDER BY 3 asc";
			return $this->db->query($query)->result();
		}
		
		// M021 - FIX - FIN

	public function getTiposCliente(){
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Tipos_Entidad');
		return $this->db->get('tabla_dato')->result();
	}
	
	public function getTiposFormaPago(){
		$this->db->order_by('No_Tipo_Forma_Pago');
		return $this->db->get('tipo_forma_pago')->result();
	}
	

	// 03_08_INICIO 
	public function Cust_getIDProductoOC(){
		$query = "SELECT ID_Producto FROM producto WHERE No_Producto='SIN PLACA'";
		return $this->db->query($query)->result();
	}
	// 03_08_FIN

	public function getImpuestos($arrPost){ 
		$where_id_empresa = 'IMPDOC.Nu_Estado=1 AND ID_Empresa=' . $this->empresa->ID_Empresa;
		if ( isset( $arrPost['iIdEmpresa'] ) ) {
			$where_id_empresa = 'ID_Empresa=' . $arrPost['iIdEmpresa'];
		}
		$sql = "
SELECT
 IMP.ID_Impuesto,
 IMPDOC.ID_Impuesto_Cruce_Documento,
 IMP.No_Impuesto AS No_Impuesto_,
 CONCAT(IMP.No_Impuesto, ' ', IMPDOC.Po_Impuesto, ' %') AS No_Impuesto,
 IMPDOC.Ss_Impuesto,
 IMP.Nu_Tipo_Impuesto
FROM
 impuesto AS IMP
 LEFT JOIN impuesto_cruce_documento AS IMPDOC ON (IMPDOC.ID_Impuesto = IMP.ID_Impuesto)
WHERE
 " . $where_id_empresa . "
ORDER BY
 IMPDOC.Nu_Orden;";
		return $this->db->query($sql)->result();
	}
	
	public function getLineas(){
		$this->db->where('Nu_Estado', 1);
		$this->db->order_by('No_Linea');
		return $this->db->get('linea')->result();
	}
	
	public function getMarcas(){
		$this->db->where('Nu_Estado', 1);
		$this->db->where('ID_Empresa', $this->empresa->ID_Empresa);
		$this->db->order_by('No_Marca');
		return $this->db->get('marca')->result();
	}
	
	public function getUnidadesMedida(){
		$query = "
SELECT
 ID_Unidad_Medida,
 No_Unidad_Medida
FROM
 unidad_medida
WHERE
 ID_Empresa=" . $this->user->ID_Empresa . "
 AND Nu_Estado=1
ORDER BY
 Nu_Orden,
 No_Unidad_Medida;";
		return $this->db->query($query)->result();
	}
	
	public function getTipoMovimiento($Nu_Tipo_Movimiento){
		$this->db->order_by('No_Tipo_Movimiento');
		$this->db->where('Nu_Tipo_Movimiento' , $Nu_Tipo_Movimiento);
		return $this->db->get('tipo_movimiento')->result();
	}

	public function getTiposDocumentos($Nu_Tipo_Filtro){
		$this->db->order_by('No_Tipo_Documento');
		if ($Nu_Tipo_Filtro=='1' || $Nu_Tipo_Filtro=='3'){//Venta
			//$this->db->where('Nu_Venta', 1);//17/02/2021
			$this->db->where_in('ID_Tipo_Documento', array(2,3,4,5,6));
		}

		if ($Nu_Tipo_Filtro=='4'){//Compra
			$this->db->where_in('ID_Tipo_Documento', array(2,3,4,5,6,11));
		}

		if ($Nu_Tipo_Filtro=='1' || $Nu_Tipo_Filtro=='2')//Venta
			$this->db->where('Nu_Es_Sunat', 1);
		if ($Nu_Tipo_Filtro=='5'){//Venta o Orden de ingreso
			$this->db->where('Nu_Orden_Ingreso', 1);
			$this->db->where('Nu_Venta', 1);
		}
		
		if ($Nu_Tipo_Filtro=='13'){//Orden de ingreso FINAL
    		$this->db->where_in('ID_Tipo_Documento', array('14','15','16','17','18','27', '65', '66', '68', 69,74,75,76));
		}

		if ($Nu_Tipo_Filtro=='6'){//Orden de ingreso
			$this->db->where('Nu_Orden_Ingreso', 5);
		}
		if ($Nu_Tipo_Filtro=='7'){//Salida de Inventario
    		$this->db->where_in('ID_Tipo_Documento', array(21,26,28,29,30,31,32,34,37, 65,66, 67, 68, 69, 70,73));
		}

		if ($Nu_Tipo_Filtro=='8'){//Orden de ingreso Generar MODAL FINAL
    		$this->db->where_in('ID_Tipo_Documento', array('14','15','16','17','18','19'));
		}

		if ($Nu_Tipo_Filtro=='10'){//Presupesto P&P y Presupuesto Mecanica
			//$this->db->where('Nu_Cotizacion_Venta', 1);
			//$this->db->where_in('ID_Tipo_Documento', array('14','15','16','17','18','19')); 11/02/2021
			$this->db->where_in('ID_Tipo_Documento', array('22','23','35', 71,72,77));
		}
		
		if ($Nu_Tipo_Filtro=='11'){//Orden de ingreso
    		$this->db->where_in('ID_Tipo_Documento', array('21'));
		}
		if ($Nu_Tipo_Filtro=='12'){//Solo Pago de Contratista
			$this->db->where('ID_Tipo_Documento', 25);
		}
		
		if ($Nu_Tipo_Filtro=='20'){//SERIE
    		$this->db->where_in('ID_Tipo_Documento', array(2,3,4,5,6,7,14,15,16,17,18,19,21,22,23,24,26,27,28,29,30,31,32,33,36,37,65,66,67,68,69,70,71,72,73,74,75,76,77,78));
		}
		
		if ($Nu_Tipo_Filtro=='21'){//Salida de inventario ACTIVOS
    		$this->db->where_in('ID_Tipo_Documento', array('14','15','16','17','18','27','33','36',38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64));
		}

		return $this->db->get('tipo_documento')->result();
	}
	
	public function getMediosPago($arrPost){
		$where_id_empresa = 'AND ID_Empresa=' . $this->empresa->ID_Empresa;
		if ( isset( $arrPost['iIdEmpresa'] ) ) {
			$where_id_empresa = 'AND ID_Empresa=' . $arrPost['iIdEmpresa'];
		}
		$query = "SELECT ID_Medio_Pago, No_Medio_Pago, Nu_Tipo, Nu_Tipo_Caja FROM medio_pago WHERE Nu_Estado = 1 " . $where_id_empresa . " ORDER BY Nu_Orden";
		return $this->db->query($query)->result();
	}
	
	public function getUbicacionesInventario(){
		$this->db->where('Nu_Estado', 1);
		$this->db->order_by('No_Ubicacion_Inventario');
		return $this->db->get('ubicacion_inventario')->result();
	}
	
	public function getDescargarInventario(){
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Descargar_Inventario');
		return $this->db->get('tabla_dato')->result();
	}

	public function getTiposDocumentosOrden(){
		$this->db->order_by('No_Tipo_Documento');
    	$this->db->where_in('ID_Tipo_Documento', array('3','4'));
		return $this->db->get('tipo_documento')->result();
	}

	public function getTiposDocumentosVales(){
		$this->db->order_by('No_Tipo_Documento');
		$this->db->where('ID_Tipo_Documento', 21);
		return $this->db->get('tipo_documento')->result();
	}
	
	public function getTiposOrdenSeguimiento(){
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Tipos_Orden_Seguimiento');
		return $this->db->get('tabla_dato')->result();
	}
	
	public function getTiposTarjetaCredito($ID_Medio_Pago){
		$query = "SELECT ID_Tipo_Medio_Pago, No_Tipo_Medio_Pago FROM tipo_medio_pago WHERE ID_Medio_Pago=" . $ID_Medio_Pago . " AND Nu_Estado=1 ORDER BY No_Tipo_Medio_Pago";
		return $this->db->query($query)->result();
	}
	
	public function getToken(){
		$query = "SELECT Nu_Activar_Descuento_Punto_Venta, Txt_Terminos_Condiciones_Ticket, Nu_Verificar_Autorizacion_Venta, Nu_Height_Logo_Ticket, Nu_Width_Logo_Ticket, Nu_Imprimir_Liquidacion_Caja, Nu_Dia_Limite_Fecha_Vencimiento, Nu_Validar_Stock, Nu_Logo_Empresa_Ticket, Nu_Tipo_Rubro_Empresa, Fe_Inicio_Sistema, Txt_Token FROM configuracion WHERE ID_Empresa=" . $this->user->ID_Empresa . " LIMIT 1";
		return $this->db->query($query)->row();
	}
	
	public function getCodigoUnidadMedida(){
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Unidades_Medida');
		return $this->db->get('tabla_dato')->result();
	}
	
	// M041 - I 
	public function getDataProducto($ID_Producto){
		$query = "SELECT Ss_Costo FROM producto WHERE ID_Producto = ". $ID_Producto;
		return $this->db->query($query)->result();
	}
	// M041 - F 

	public function getListaPrecio($iTipoLista, $ID_Organizacion, $ID_Almacen){
		$cond_id_almacen = $ID_Almacen > 0 ? '=' . $ID_Almacen : 'IS NULL';		
		$cond_lista_precio = "";
		if (!empty($iTipoLista))
			$cond_lista_precio = "AND Nu_Tipo_Lista_Precio=" . $iTipoLista;
		$query = "
SELECT
 ID_Lista_Precio_Cabecera,
 No_Lista_Precio
FROM
 lista_precio_cabecera
WHERE
 ID_Empresa = " . $this->user->ID_Empresa . "
 AND ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND ID_Almacen " . $cond_id_almacen . "
 AND No_Lista_Precio NOT LIKE '%web%'
 AND No_Lista_Precio NOT LIKE '%app%'
 " . $cond_lista_precio . "
 AND Nu_Estado=1
ORDER BY
 No_Lista_Precio";
		return $this->db->query($query)->result();
	}
	
	public function getMotivosTraslado(){
		$this->db->order_by('Nu_Valor');
		$this->db->where('No_Relacion', 'Motivo_Traslado');
		return $this->db->get('tabla_dato')->result();
	}

	public function getTipoOperacionCaja($Nu_Tipo){
		$this->db->order_by('ID_Tipo_Operacion_Caja');
		$this->db->where('Nu_Estado', 1);
		$this->db->where('Nu_Tipo', $Nu_Tipo);
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Organizacion', $this->user->ID_Organizacion);
		return $this->db->get('tipo_operacion_caja')->result();
	}
	
	public function getValidarStock(){
		$query = "SELECT Nu_Validar_Stock FROM configuracion WHERE ID_Empresa = " . $this->user->ID_Empresa . " LIMIT 1";
		return $this->db->query($query)->row();
	}
	
	public function getItems($ID_Almacen, $ID_Lista_Precio_Cabecera, $ID_Linea){
		$cond_Linea = (!empty($ID_Linea) ? "AND ID_Linea = " . $ID_Linea : "" );
		//AND STOCKPRO.ID_Almacen = " . $ID_Almacen. "
		$sql = "
SELECT
 PRO.ID_Producto,
 PRO.Nu_Codigo_Barra,
 PRO.No_Producto,
 PRO.Txt_Producto
FROM
 producto AS PRO
 LEFT JOIN stock_producto AS STOCK ON(STOCK.ID_Empresa = " . $this->empresa->ID_Empresa . " AND STOCK.ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND STOCK.ID_Producto = PRO.ID_Producto)
 LEFT JOIN lista_precio_detalle AS LPD ON(LPD.ID_Lista_Precio_Cabecera = " . $ID_Lista_Precio_Cabecera . " AND LPD.ID_Producto = PRO.ID_Producto)
WHERE
 PRO.ID_Empresa=" . $this->user->ID_Empresa . "
 " . $cond_Linea . "
 AND PRO.Nu_Estado=1
 ORDER BY
  No_Producto DESC";
		return $this->db->query($sql)->result();
	}
	
	public function getUltimoCierre(){
		$query = "
SELECT
 CPOS.ID_Matricula_Empleado,
 CPOS.ID_Caja_Pos,
 CPOS.Fe_Movimiento,
 MEMPLE.ID_Empleado
FROM
 caja_pos AS CPOS
 JOIN matricula_empleado AS MEMPLE ON(CPOS.ID_Matricula_Empleado = MEMPLE.ID_Matricula_Empleado)
WHERE
 CPOS.Nu_Estado = 0
LIMIT 1;";
		return $this->db->query($query)->row();
	}
	
	public function getDataGeneral($arrPost){
		if ( $arrPost['sTipoData'] == 'item' ) {
			$left_join_stock_producto = '';
			$left_join_lista_precio_detalle = '';
			if ( isset($arrPost['iIdListaPrecioCabecera']) ) {
				$left_join_stock_producto = "LEFT JOIN stock_producto AS STOCK ON(STOCK.ID_Empresa = " . $this->empresa->ID_Empresa . " AND STOCK.ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND STOCK.ID_Producto = ITEM.ID_Producto)";
				$left_join_lista_precio_detalle = "LEFT JOIN lista_precio_detalle AS LPD ON(LPD.ID_Lista_Precio_Cabecera = " . $arrPost['iIdListaPrecioCabecera'] . " AND LPD.ID_Producto = ITEM.ID_Producto)";
			}
			$query = "
SELECT
 ITEM.*,
 ITEM.ID_Producto AS ID,
 ITEM.No_Producto AS Nombre,
 LPD.Ss_Precio_Interno,
 LPD.Po_Descuento,
 LPD.Ss_Precio,
 ROUND(STOCK.Qt_Producto, 0) AS Qt_Producto,
 ITEM.Ss_Precio as Ss_Precio_Item,
 ITEM.Ss_Costo as Ss_Costo_Item
FROM
 producto AS ITEM
 " . $left_join_stock_producto . "
 " . $left_join_lista_precio_detalle . "
WHERE
 ITEM.ID_Producto=" . $arrPost['iIdItem'] . " LIMIT 1";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos - item',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontraron registros - item',
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'categoria' ) {
			$where_id_empresa='';
			if ( isset($arrPost['iIdEmpresa']) ) {
				$where_id_empresa='AND ID_Empresa=' . $arrPost['iIdEmpresa'];
			}
			$query = "SELECT ID_Familia AS ID, No_Familia AS Nombre FROM familia WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Estado=1 ORDER BY No_Familia";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos - Categorías',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrados',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontraron registros - Categorías',
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'subcategoria' ) {
			$cond_id_categoria='';
			if ( isset($arrPost['sWhereIdCategoria']) && !empty($arrPost['sWhereIdCategoria']) ) {
				$cond_id_categoria='AND ID_Familia=' . $arrPost['sWhereIdCategoria'];
			}
			$query = "SELECT ID_Sub_Familia AS ID, No_Sub_Familia AS Nombre FROM subfamilia WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Estado=1 " . $cond_id_categoria . " ORDER BY No_Sub_Familia";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos - Sub categorías',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
					'sql' => $query,
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrados',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontraron registros - Sub categorías',
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'laboratorio' ) {
			$query = "SELECT ID_Laboratorio AS ID, No_Laboratorio AS Nombre FROM laboratorio WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Estado=1 ORDER BY No_Laboratorio";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos - laboratorio',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrados',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontraron registros - laboratorio',
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'composicion' ) {
			$cond_id_composicion='';
			if ( isset($arrPost['sWhereIdComposicion']) && !empty($arrPost['sWhereIdComposicion']) ) {
				$cond_id_composicion='AND ID_Composicion IN(' . $arrPost['sWhereIdComposicion'] . ')';
			}
			$query = "SELECT ID_Composicion AS ID, No_Composicion AS Nombre FROM composicion WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Estado=1 " . $cond_id_composicion . " ORDER BY No_Composicion";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos - composiciones',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrados',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontraron registros - composiciones',
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'entidad' ) {
			$query = "SELECT ID_Entidad AS ID, No_Entidad AS Nombre FROM entidad WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Estado=1 AND Nu_Tipo_Entidad = " . $arrPost['iTipoEntidad'] . " ORDER BY No_Entidad";
			
			$sNombreEntidad = 'Clientes';
			if ( $arrPost['iTipoEntidad'] == 1 ) {
				$sNombreEntidad = 'Proveedores';
			} else if ( $arrPost['iTipoEntidad'] == 4 ) {
				$sNombreEntidad = 'Empleados';
			} else if ( $arrPost['iTipoEntidad'] == 6 ) {
				$sNombreEntidad = 'Delivery';
			}

			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos - ' . $sNombreEntidad,
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrados',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontraron registros - ' . $sNombreEntidad,
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'get_entidad' ) {
			$where_id_entidad = 'AND ID_Entidad = ' . $arrPost['iIDEntidad'];
			$where_nombre_entidad = '';
			if ( !empty($arrPost['sNombreEntidad']) ) {
				$where_id_entidad = '';
				$where_nombre_entidad = "AND No_Entidad LIKE '%" . $arrPost['sNombreEntidad'] . "%'";			
			}
			$query = "SELECT Nu_Estado, Txt_Email_Entidad FROM entidad WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Tipo_Entidad = " . $arrPost['iTipoEntidad'] . " " . $where_id_entidad . $where_nombre_entidad . " LIMIT 1";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrado',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontro registro',
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'Porcentaje_Comision_Vendedores' ) {
			$query = "SELECT * FROM tabla_dato WHERE No_Relacion='" . $arrPost['sTipoData'] . "'";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrado',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontro registro',
				'sClassModal' => 'modal-warning',
			);
		} else if ( $arrPost['sTipoData'] == 'movimiento_caja_pv' ) {
			$query = "SELECT ID_Tipo_Operacion_Caja AS ID, No_Tipo_Operacion_Caja AS Nombre FROM tipo_operacion_caja WHERE ID_Empresa=".$this->empresa->ID_Empresa." AND Nu_Tipo IN(5,6) AND Nu_Estado=1 ORDER BY No_Tipo_Operacion_Caja";
			if ( !$this->db->simple_query($query) ){
				$error = $this->db->error();
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'Problemas al obtener datos - laboratorio',
					'sClassModal' => 'modal-danger',
					'sCodeSQL' => $error['code'],
					'sMessageSQL' => $error['message'],
				);
			}
			$arrResponseSQL = $this->db->query($query);
			if ( $arrResponseSQL->num_rows() > 0 ){
				return array(
					'sStatus' => 'success',
					'sMessage' => 'Registros encontrados',
					'sClassModal' => 'modal-success',
					'arrData' => $arrResponseSQL->result(),
				);
			}
			
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontraron registros - laboratorio',
				'sClassModal' => 'modal-warning',
			);
		}
	}
	
	public function getPosConfiguracionxSerie($arrPost){
		$where_id_empresa = 'AND ID_Empresa=' . $this->empresa->ID_Empresa;
		$where_id_organizacion = 'AND ID_Organizacion=' . $this->empresa->ID_Organizacion;
		if ( isset( $arrPost['iIdEmpresa'] ) ) {
			$where_id_empresa = 'AND ID_Empresa=' . $arrPost['iIdEmpresa'];
		}
		if ( isset( $arrPost['iIdOrganizacion'] ) ) {
			$where_id_organizacion = 'AND ID_Organizacion=' . $arrPost['iIdOrganizacion'];
		}
		$query = "SELECT ID_POS, Nu_Pos FROM pos WHERE Nu_Estado>0 " . $where_id_empresa . " " . $where_id_organizacion . " ORDER BY ID_POS";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - POS',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sClassModal' => 'modal-success',
				'arrData' => $arrResponseSQL->result(),
			);
		}		
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros - POS',
		);
	}

	public function getPos(){
		$query = "SELECT ID_POS, Nu_Pos, Txt_Autorizacion_Venta_Serie_Disco_Duro FROM pos WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND Nu_Estado>0 ORDER BY ID_POS";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - POS',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrDataPos=$arrResponseSQL->result();
			$arrDataMatriculaPersonal=array();
			$arrDataSaldoPos=array();
			foreach( $arrDataPos as $row ){
				$arrDataMatriculaPersonal[$row->ID_POS][]=$this->getUltimaMatriculaPersonalxPos($row->ID_POS);
			}
			foreach( $arrDataPos as $row ){
				$arrDataSaldoPos[$row->ID_POS][]=$this->getUltimoCierrexPos($row->ID_POS);
			}
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrDataPos,
				'arrDataMatriculaPersonal' => $arrDataMatriculaPersonal,
				'arrDataSaldoPos' => $arrDataSaldoPos,
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros - POS',
		);
	}

	public function getUltimaMatriculaPersonalxPos($iIDPos){
		$query = "
SELECT
 ME.ID_Matricula_Empleado,
 ME.ID_Entidad,
 TRA.No_Entidad,
 ME.Fe_Matricula
FROM
 matricula_empleado AS ME
 LEFT JOIN entidad AS TRA ON(TRA.ID_Entidad=ME.ID_Entidad)
WHERE
 ME.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND ME.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND ME.ID_Pos=".$iIDPos."
ORDER BY
 ME.Fe_Matricula DESC
LIMIT 1";

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - Matricula de personal x pos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros - Matricula de personal x pos',
		);
	}

	public function getUltimoCierrexPos($iIDPos){
		$query = "
SELECT
 CP.ID_Matricula_Empleado,
 MONE.No_Signo,
 CP.Ss_Total,
 CP.Fe_Movimiento,
 TOC.No_Tipo_Operacion_Caja,
 CASE WHEN TOC.Nu_Tipo=3 THEN 'success' ELSE 'danger' END AS No_Class_Estado,
 CP.ID_Moneda AS ID_Moneda_Caja_Pos
FROM
 caja_pos AS CP
 JOIN tipo_operacion_caja AS TOC ON(TOC.ID_Tipo_Operacion_Caja=CP.ID_Tipo_Operacion_Caja)
 JOIN moneda AS MONE ON(MONE.ID_Moneda=CP.ID_Moneda)
WHERE
 CP.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND CP.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND CP.ID_POS=".$iIDPos."
 AND TOC.Nu_Tipo IN(3, 4)
ORDER BY
 CP.Fe_Movimiento DESC
LIMIT 1";
		
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - Ultimo cierre x pos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros - Ultimo cierre x pos',
		);
	}

	public function getPersonal($arrPost){
		$cond_where_nu_documento_identidad="";
		$cond_where_nu_pin="";
		if ( isset($arrPost['iNumeroDocumentoIdentidad']) ) {
			$cond_where_nu_documento_identidad="AND Nu_Documento_Identidad=".$arrPost['iNumeroDocumentoIdentidad'];
			$sMessage = 'No se encontraron registros - Personal';
		}
		if ( isset($arrPost['iPin']) ) {
			$cond_where_nu_pin="AND Nu_Pin_Caja=".$arrPost['iPin'];
			$sMessage = 'No se encontro PIN';
		}
		$query = "SELECT * FROM entidad WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND Nu_Tipo_Entidad=4 " . $cond_where_nu_documento_identidad . " " . $cond_where_nu_pin . " LIMIT 1";
		
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - Personal',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => $sMessage,
		);
	}

	public function getMatriculaPersonal($ID_Matricula_Empleado, $iIdMoneda){
		$query = "
SELECT
 ME.ID_Matricula_Empleado,
 ME.ID_Entidad,
 TRA.No_Entidad,
 ME.Fe_Matricula,
 ME.ID_POS,
 POS.Nu_Pos AS Nu_Caja
FROM
 matricula_empleado AS ME
 JOIN entidad AS TRA ON(TRA.ID_Entidad=ME.ID_Entidad)
 JOIN pos AS POS ON(POS.ID_POS = ME.ID_POS)
WHERE
 ME.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND ME.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND ME.ID_Matricula_Empleado=".$ID_Matricula_Empleado."
ORDER BY
 ME.Fe_Matricula DESC
LIMIT 1";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - Matricula de personal x ID',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrData = array_merge( $arrResponseSQL->result(), array('ID_Moneda' => $iIdMoneda) );
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrData,
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros - Matricula de personal x ID',
		);
	}

	public function validacionStockMinimo($arrPost){
		$iIdItem = $arrPost['iIdItem'];
		$fCantidadItem = $arrPost['fCantidadItem'];
		$query = "SELECT Nu_Stock_Minimo FROM producto WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Producto = " . $iIdItem . " AND Nu_Stock_Minimo >= " . $fCantidadItem . " LIMIT 1";
		
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Stock Mínimo',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'Item no supera el stock minimo',
		);
	}

	public function validacionVentaRecetaMedica($arrPost){
		$iIdItem = $arrPost['iIdItem'];
		$query = "SELECT Nu_Receta_Medica FROM producto WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Producto = " . $iIdItem . " AND Nu_Receta_Medica=1 LIMIT 1";
		
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Venta solo con <b>Receta Médica</b>',
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'Sin receta medica',
		);
	}

	public function validacionLoteVencimiento($arrPost){
		$iIdItem = $arrPost['iIdItem'];
		$query = "SELECT Nu_Lote_Vencimiento, Fe_Lote_Vencimiento FROM documento_detalle_lote WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND ID_Producto = " . $iIdItem . " ORDER BY Fe_Lote_Vencimiento ASC LIMIT 1";
		
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				//'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrData = $arrResponseSQL->result();
			$dLoteVencimiento = strtotime( '-' . $this->empresa->Nu_Dia_Limite_Fecha_Vencimiento . ' day', strtotime( $arrData[0]->Fe_Lote_Vencimiento ) );
			$dLoteVencimiento = date( 'Y-m-j', $dLoteVencimiento );
			$arrFechaLoteVencimiento = explode('-', $dLoteVencimiento);//Y-M-D

			$iMonth = (strlen(1 + $arrFechaLoteVencimiento[1]) > 1 ? $arrFechaLoteVencimiento[1] : '0' . $arrFechaLoteVencimiento[1]);
			$iDay = (strlen($arrFechaLoteVencimiento[2]) > 1 ? $arrFechaLoteVencimiento[2] : '0' . $arrFechaLoteVencimiento[2]);

			$dLoteVencimiento = $arrFechaLoteVencimiento[0] . '-' . $iMonth . '-' . $iDay;

			return array(
				'sStatus' => 'success',
				'sMessage' => 'Lote Vencimiento',
				'Nu_Lote_Vencimiento' => $arrData[0]->Nu_Lote_Vencimiento,
				'dToday' => dateNow('fecha'),
				'dLoteVencimientoOperacion' => $dLoteVencimiento,
				'dLoteVencimiento' => $arrData[0]->Fe_Lote_Vencimiento,
				'iDiasLoteVencimiento' => $this->empresa->Nu_Dia_Limite_Fecha_Vencimiento
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No hay lote de vencimiento'
		);
	}
	
	public function validateStockNow($arrPost){
		$iIdItem = $arrPost['iIdItem'];
		$query = "SELECT Qt_Producto FROM stock_producto WHERE ID_Empresa=" . $this->empresa->ID_Empresa . " AND ID_Organizacion=" . $this->empresa->ID_Organizacion . " AND ID_Producto=" . $iIdItem . " LIMIT 1";
		return $this->db->query($query)->row();
	}
	
	public function obtenerTipoItem($arrParams){
		$query = "SELECT Nu_Tipo_Producto FROM producto WHERE ID_Empresa=" . $arrParams['ID_Empresa'] . " AND ID_Producto=" . $arrParams['ID_Producto'] . " LIMIT 1";
		return $this->db->query($query)->row()->Nu_Tipo_Producto;
	}

	public function validateStockNowInterno($arrParams){
		$query = "SELECT Qt_Producto FROM stock_producto WHERE ID_Empresa=" . $arrParams['ID_Empresa'] . " AND ID_Organizacion=" . $arrParams['ID_Organizacion'] . " AND ID_Almacen=" . $arrParams['ID_Almacen'] . " AND ID_Producto=" . $arrParams['ID_Producto'] . " LIMIT 1";
		//echo $query;
		return $this->db->query($query)->row()->Qt_Producto;
	}

	public function getValoresTablaDato($arrPost){
		$query = "SELECT * FROM tabla_dato WHERE No_Relacion='" . $arrPost['sTipoData'] . "'";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - ' . $arrPost['sTipoData'],
				'sClassModal' => 'modal-danger',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sClassModal' => 'modal-success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro - ' . $arrPost['sTipoData'],
			'sClassModal' => 'modal-warning',
		);
	}

	public function connectToMysqlLocalhost($arrPost){
		$query = "SELECT Txt_Autorizacion_Venta_Localhost_Database, Txt_Autorizacion_Venta_Localhost_Hostname, Txt_Autorizacion_Venta_Localhost_User, Txt_Autorizacion_Venta_Localhost_Password FROM organizacion WHERE ID_Empresa=" . $this->user->ID_Empresa . " AND ID_Organizacion=" . $this->user->ID_Organizacion;
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener credenciales localhost',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrDataCredencialesMysqlLocalhost = $arrResponseSQL->result();
			
			$dbs = 'mysql:dbname=' . $arrDataCredencialesMysqlLocalhost[0]->Txt_Autorizacion_Venta_Localhost_Database . ';host=' . $arrDataCredencialesMysqlLocalhost[0]->Txt_Autorizacion_Venta_Localhost_Hostname;
			$user = $arrDataCredencialesMysqlLocalhost[0]->Txt_Autorizacion_Venta_Localhost_User;
			$password = $arrDataCredencialesMysqlLocalhost[0]->Txt_Autorizacion_Venta_Localhost_Password;

			try {
				$conexion = new PDO($dbs, $user, $password);      
				$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$arrData = $conexion->query("SELECT ID_POS, Txt_Key_Serie_HDD FROM punto_autorizado LIMIT 1");
				$sKeySerieHDD = '';
				foreach($arrData as $row)
					$sKeySerieHDD = $row['Txt_Key_Serie_HDD'];
				
				if ( !empty($sKeySerieHDD) ) {
					return array(
						'sStatus' => 'success',
						'sMessage' => 'Conexión realizada satisfactoriamente',
						'sKeySerieHDD' => $sKeySerieHDD,
					);
				} else {
					return array(
						'sStatus' => 'warning',
						'sMessage' => 'No se encontro key serie HDD',
					);
				}
			} catch(PDOException $e) {
				return array(
					'sStatus' => 'danger',
					'sMessage' => 'No hay conexión',
					'sMessageSQL' => 'La conexión ha fallado: ' . $e->getMessage(),
				);
			}
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro de credenciales',
		);
	}
	
	public function validationKeySerieHDD($arrPost){
		$where_key_serie_hdd = ( isset($arrPost['sKeySerieHDD']) ? "AND Txt_Autorizacion_Venta_Serie_Disco_Duro = '" . $arrPost['sKeySerieHDD'] . "' LIMIT 1" : '' );
		$query = "SELECT Txt_Autorizacion_Venta_Serie_Disco_Duro FROM pos WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND ID_Organizacion = " . $this->empresa->ID_Organizacion . " AND Nu_Estado>0 " . $where_key_serie_hdd;
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos - POS',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros - POS',
		);
	}
	
	public function getDocumentoDetalle($arrPost){
		$query = "SELECT VD.ID_Producto, VD.Qt_Producto, ITEM.No_Producto, VD.Txt_Nota AS Txt_Nota_Item FROM documento_detalle AS VD JOIN producto AS ITEM ON(ITEM.ID_Producto = VD.ID_Producto) WHERE ID_Documento_Cabecera = " . $arrPost['iIdDocumentoCabecera'] . " AND ID_Documento_Detalle = " . $arrPost['iIdDocumentoDetalle'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros',
		);
	}
	
	public function getDocumentoDetalleEstadoLavado($arrPost){
		/*DDEL.*,*/
		$query = "
SELECT DISTINCT
 DDEL.Txt_Final_Prelavado,
 DDEL.Txt_Final_Lavado_Seco,
 DDEL.Txt_Planchado,
 DDEL.Txt_Doblado,
 DDEL.Txt_Embolsado,
 VD.Qt_Producto,
 ITEMPELAVA.No_Descripcion AS No_Tipo_Pedido_Lavado,
 ITEM.No_Producto,
 TDESTADOLAVADETALLE.No_Descripcion AS No_Estado_Lavado_Detalle,
 TDESTADOLAVADETALLE.No_Class AS No_Class_Estado_Lavado_Detalle,
 LI.No_Entidad AS No_Entidad_Lavado_Iniciado,
 LF.No_Entidad AS No_Entidad_Lavado_Finalizado,
 LP.No_Entidad AS No_Entidad_Lavado_Planchado,
 LD.No_Entidad AS No_Entidad_Lavado_Doblado,
 LE.No_Entidad AS No_Entidad_Lavado_Embolsado,
 TRANSPT.No_Entidad AS No_Entidad_Transporte,
 TIPOENVIO.No_Descripcion AS No_Tipo_Envio_Transporte_Detalle,
 TIPOENVIO.No_Class AS No_Class_Tipo_Envio_Transporte_Detalle
FROM
 documento_detalle AS VD
 JOIN documento_cabecera AS VC ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 JOIN tabla_dato AS TIPOENVIO ON(TIPOENVIO.Nu_Valor = VC.Nu_Transporte_Lavanderia_Hoy AND TIPOENVIO.No_Relacion = 'Tipos_EstadoEnvioPedidoLavado')
 LEFT JOIN entidad AS TRANSPT ON(TRANSPT.ID_Entidad = VC.ID_Transporte_Sede_Planta)
 LEFT JOIN documento_detalle_estado_lavado AS DDEL ON(DDEL.ID_Documento_Cabecera = VD.ID_Documento_Cabecera AND DDEL.ID_Documento_Detalle = VD.ID_Documento_Detalle)
 JOIN producto AS ITEM ON(VD.ID_Producto = ITEM.ID_Producto)
 JOIN tabla_dato AS ITEMPELAVA ON(ITEMPELAVA.Nu_Valor = ITEM.ID_Tipo_Pedido_Lavado AND ITEMPELAVA.No_Relacion = 'Tipos_PedidoLavado')
 LEFT JOIN tabla_dato AS TDESTADOLAVADETALLE ON(TDESTADOLAVADETALLE.Nu_Valor = DDEL.Nu_Estado_Lavado AND TDESTADOLAVADETALLE.No_Relacion = 'Tipos_EstadoLavado')
 LEFT JOIN entidad AS LI ON(LI.ID_Entidad = DDEL.ID_Entidad_Lavado_Iniciado)
 LEFT JOIN entidad AS LF ON(LF.ID_Entidad = DDEL.ID_Entidad_Lavado_Finalizado)
 LEFT JOIN entidad AS LP ON(LP.ID_Entidad = DDEL.ID_Entidad_Lavado_Planchado)
 LEFT JOIN entidad AS LD ON(LD.ID_Entidad = DDEL.ID_Entidad_Lavado_Doblado)
 LEFT JOIN entidad AS LE ON(LE.ID_Entidad = DDEL.ID_Entidad_Lavado_Embolsado)
WHERE
 VD.ID_Documento_Cabecera = " . $arrPost['iIdDocumentoCabecera'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros',
		);
	}

	public function getDocumentoDetalleEstadoLavadoxDocumentoDetalle($arrPost){
		$query = "SELECT * FROM documento_detalle_estado_lavado WHERE ID_Documento_Detalle = " . $arrPost['iIdDocumentoDetalle'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'sMessage' => 'Registros encontrados',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontraron registros',
		);
	}

	public function cobranzaClientePuntoVenta($arrPost){
        $this->db->trans_begin();

        $data = array( 'Nu_Estado_Lavado_Recepcion_Cliente' => $arrPost['iEstadoLavadoRecepcionCliente'] );
        if ( isset($arrPost['iCrearEntidad']) && $arrPost['iCrearEntidad'] == 0 ) {// 0 Crear entidad recepcion lavado cliente = 9
            $arrEntidadLavado = array(
                'ID_Empresa' => $this->empresa->ID_Empresa,
                'ID_Organizacion' => $this->empresa->ID_Organizacion,
                'Nu_Tipo_Entidad' => 9,
                'ID_Tipo_Documento_Identidad' => 1,
                'No_Entidad' => $arrPost['sNombreRecepcion'],
            );
            $this->db->insert('entidad', $arrEntidadLavado);
            $iIdEntidad = $this->db->insert_id();
            $data = array( 'Nu_Estado_Lavado_Recepcion_Cliente' => $arrPost['iEstadoLavadoRecepcionCliente'], 'ID_Mesero' => $iIdEntidad );
        }

        if ( !empty($arrPost['fPagoCliente']) ) {
            $documento_medio_pago = array(
                'ID_Empresa'			=> $this->empresa->ID_Empresa,
                'ID_Documento_Cabecera'	=> $arrPost['iIdDocumentoCabecera'],
                'ID_Medio_Pago'		    => $arrPost['iFormaPago'],
                'Nu_Transaccion'		=> $arrPost['iNumeroTransaccion'],
                'Nu_Tarjeta'		    => $arrPost['iNumeroTarjeta'],
                'Ss_Total'		        => $arrPost['fPagoCliente'],
                'ID_Tipo_Medio_Pago' => isset($arrPost['iTipoMedioPago']) ? $arrPost['iTipoMedioPago'] : 0,
				'Fe_Emision_Hora_Pago' => dateNow('fecha_hora'),
				'ID_Matricula_Empleado' => (isset($this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado) ? $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado : 0),
            );
            $this->db->insert('documento_medio_pago', $documento_medio_pago);
            $data = array_merge( $data, array( 'Ss_Total_Saldo' => $arrPost['fSaldoCliente'] - $arrPost['fPagoCliente'] ) );
        }
        
        $where = array( 'ID_Documento_Cabecera' => $arrPost['iIdDocumentoCabecera'] );
        $this->db->update('documento_cabecera', $data, $where);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Problemas al procesar pago del cliente');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Se agrego pago de cliente');
        }
	}
	
	public function getEmpresasOpcionesMenu(){
		$query = "SELECT EMP.*, CONF.* FROM empresa AS EMP JOIN configuracion AS CONF ON(EMP.ID_Empresa = CONF.ID_Empresa) WHERE EMP.Nu_Estado = 1 ORDER BY EMP.No_Empresa;";
		return $this->db->query($query)->result();
	}

	public function getEmpresasMarketplace($arrPost){
		$query = "SELECT * FROM empresa AS EMP JOIN tabla_dato AS TIPOECOMMERCE ON(TIPOECOMMERCE.ID_Tabla_Dato=EMP.Nu_Tipo_Ecommerce_Empresa AND TIPOECOMMERCE.No_Relacion = 'Tipos_Ecommerce_Empresa') WHERE TIPOECOMMERCE.Nu_Valor = 1 ORDER BY No_Empresa;";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getCategoriasMarketplace(){
		$query = "SELECT * FROM familia WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace;
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getSubCategoriasMarketplace($arrPost){
		$query = "SELECT * FROM subfamilia WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND ID_Familia = " . $arrPost['iIdFamilia'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getMarcasMarketplace(){
		$query = "SELECT * FROM marca WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace;
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}
	
	public function getMediosPagoMarketplace($arrPost){
		$where_id_empresa = 'AND ID_Empresa=' . $this->empresa->ID_Empresa;
		if ( isset( $arrPost['iIdEmpresa'] ) ) {
			$where_id_empresa = 'AND ID_Empresa=' . $arrPost['iIdEmpresa'];
		}
		$query = "SELECT * FROM medio_pago_marketplace WHERE Nu_Estado = 1 " . $where_id_empresa . " ORDER BY Nu_Orden";
		return $this->db->query($query)->result();
	}

	public function getRelacionTablaPresupuestoToOI($arrParams)
	{
		$query = "SELECT
			OV.ID_Documento_Cabecera,
			OV.ID_Tipo_Documento,
			TD.No_Tipo_Documento,
			SD.ID_Serie_Documento,
			OV.ID_Numero_Documento,
			OV.Ss_Total,
			OV.Nu_Estado
			FROM
			relacion_tabla AS RT
			JOIN documento_cabecera AS OV ON(OV.ID_Documento_Cabecera = RT.ID_Origen_Tabla)
			JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = OV.ID_Tipo_Documento)
			JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = OV.ID_Serie_Documento_PK)
			WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Relacion_Enlace_Tabla = " . $arrParams['ID_Relacion_Enlace_Tabla'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaOIInicioyPresupuesto($arrParams){
		$query = "SELECT
OV.ID_Documento_Cabecera,
OV.ID_Tipo_Documento,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
OV.ID_Numero_Documento
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS OV ON(OV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = OV.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = OV.ID_Serie_Documento_PK)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " LIMIT 1";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaOTtoPresupuesto($arrParams){
		$query = "SELECT
OV.ID_Documento_Cabecera,
OV.ID_Tipo_Documento,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
OV.ID_Numero_Documento
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS OV ON(OV.ID_Documento_Cabecera = RT.ID_Origen_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = OV.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = OV.ID_Serie_Documento_PK)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Relacion_Enlace_Tabla = " . $arrParams['iIdOrigenTabla'] . " LIMIT 1";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaPresupuestoToOT($arrParams){//antes estaba así getRelacionTablaPresupuestoToOT
		$query = "SELECT
OV.ID_Documento_Cabecera
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS OV ON(OV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " LIMIT 1";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaMultiplePresupuestoAndOI($arrParams){
		$query = "SELECT
OI.ID_Orden_Ingreso,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
OI.ID_Numero_Documento,
TDESTADO.No_Descripcion AS No_Descripcion_Estado,
OI.Fe_Entrega_Tentativa,
OIE.No_Etapa,
OIE.No_Class_Etapa,
OI.No_Kilometraje
FROM
relacion_tabla AS RT
JOIN orden_ingreso AS OI ON(OI.ID_Orden_Ingreso = RT.ID_Relacion_Enlace_Tabla)
JOIN orden_ingreso_etapa AS OIE ON(OIE.ID_Etapa = OI.ID_Etapa)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = OI.ID_Area_Ingreso)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = OI.ID_Serie_Documento)
JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = OI.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoOrdenIngreso')
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " LIMIT 1";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

// OC_MODAL_3 - INICIO 
// OC_MODAL_4 - INICIO
// OC_MODAL_5-1 - INICIO

public function Cust_getSumaTotalOc_En_Presupuesto($arrParams){
	
	
	$query = "SELECT 
	sum(B.Ss_Total) as Ss_Total 
	from deta_presupuesto_oc A 
	JOIN documento_cabecera B ON A.ID_Cabe_OC=B.id_documento_cabecera 
	WHERE A.ID_Cabe_Presupuesto= " .$arrParams['ID_Cabe_Presupuesto'] . " 
	AND B.Estado_Documento=1  	" ;

	if ( !$this->db->simple_query($query) ){
		$error = $this->db->error();
		return array(
			'sStatus' => 'danger',
			'sMessage' => 'Problemas al obtener datos',
			'sCodeSQL' => $error['code'],
			'sMessageSQL' => $error['message'],
		);
	}
	$arrResponseSQL = $this->db->query($query);
	if ( $arrResponseSQL->num_rows() > 0 ){
		return array(
			'sStatus' => 'success',
			'arrData' => $arrResponseSQL->result(),
		);
	}
	
	return array(
		'sStatus' => 'warning',
		'sMessage' => 'No se encontro registro',
	);
}

// OC_MODAL_5-1 - FIN
// OC_MODAL_4 - FIN 
// OC_MODAL_3 - FIN 


// OC_MODAL_5-2 - INICIO
// OC_MODAL_5-3 - INICIO
public function Cust_getIDsOc_En_Presupuesto($arrParams){
	
	$query = "SELECT  
	CONCAT('OC - ', B.ID_Numero_Documento ) AS ID_Cabe_OC , 
	B.Estado_Documento 
	from deta_presupuesto_oc A 
	JOIN documento_cabecera B ON A.ID_Cabe_OC=B.id_documento_cabecera 
	WHERE A.ID_Cabe_Presupuesto=  " .$arrParams['ID_Cabe_Presupuesto'] . "  " ;


	if ( !$this->db->simple_query($query) ){
		$error = $this->db->error();
		return array(
			'sStatus' => 'danger',
			'sMessage' => 'Problemas al obtener datos',
			'sCodeSQL' => $error['code'],
			'sMessageSQL' => $error['message'],
		);
	}
	$arrResponseSQL = $this->db->query($query);
	if ( $arrResponseSQL->num_rows() > 0 ){
		return array(
			'sStatus' => 'success',
			'arrData' => $arrResponseSQL->result(),
		);
	}
	
	return array(
		'sStatus' => 'warning',
		'sMessage' => 'No se encontro registro',
	);
}
// OC_MODAL_5-3 - FIN
// OC_MODAL_5-2 - FIN

	public function getRelacionTablaPresupuestoToPagoContratista($arrParams){
		$query = "SELECT
PC.ID_Documento_Cabecera,
PC.ID_Tipo_Documento,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
PC.ID_Numero_Documento
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS PC ON(PC.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = PC.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = PC.ID_Serie_Documento_PK)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'];
//WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " LIMIT 1";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaPagoContratistaToPresupuesto($arrParams){
		$query = "SELECT
PC.ID_Documento_Cabecera,
PC.ID_Tipo_Documento,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
PC.ID_Numero_Documento
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS PC ON(PC.ID_Documento_Cabecera = RT.ID_Origen_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = PC.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = PC.ID_Serie_Documento_PK)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Relacion_Enlace_Tabla = " . $arrParams['ID_Relacion_Enlace_Tabla'] . " LIMIT 1";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaOIyPresupuesto($arrParams){
		$query = "SELECT
OI.ID_Orden_Ingreso,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
OI.ID_Numero_Documento
FROM
relacion_tabla AS RT
JOIN orden_ingreso AS OI ON(OI.ID_Orden_Ingreso = RT.ID_Origen_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = OI.ID_Area_Ingreso)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = OI.ID_Serie_Documento)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " LIMIT 1";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTabla($arrParams){
		$query = "SELECT
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
GC.ID_Numero_Documento,
GC.Ss_Total
FROM
relacion_tabla AS RT
JOIN guia_cabecera AS GC ON(GC.ID_Guia_Cabecera = RT.ID_Relacion_Enlace_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = GC.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = GC.ID_Serie_Documento_PK)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'];
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaValeToPresupuesto($arrParams){
		$query = "SELECT
GC.ID_Documento_Cabecera,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
GC.ID_Numero_Documento,
GC.Ss_Total
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS GC ON(GC.ID_Documento_Cabecera = RT.ID_Origen_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = GC.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = GC.ID_Serie_Documento_PK)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Relacion_Enlace_Tabla = " . $arrParams['ID_Relacion_Enlace_Tabla'];
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaPresupuestoALiquidacion($arrParams){
		$query = "SELECT
LV.ID_Documento_Cabecera,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
LV.ID_Numero_Documento
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS LV ON(LV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = LV.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = LV.ID_Serie_Documento_PK)
WHERE RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " LIMIT 1";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	// C-02 - Inicio 
	public function CustPrep_getRelacionTablaPresupuestoAFactura($arrParams){

		$query = " 	SELECT 
		TD.No_Tipo_Documento,
		SD.ID_Serie_Documento,
		FV.ID_Numero_Documento,
		FV.Ss_Total,
		FV.Fe_Emision
		FROM 
		relacion_tabla AS RT 
		JOIN documento_cabecera AS FV ON(FV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
		JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = FV.ID_Tipo_Documento)
		JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = FV.ID_Serie_Documento_PK)
		WHERE
		RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . "
		AND (RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " OR RT.ID_Relacion_Enlace_Tabla = " . $arrParams['iIdOrigenTabla'] . " )
		AND RT.ID_Relacion_Tabla = ( SELECT MAX( A.ID_Relacion_Tabla ) FROM relacion_tabla AS A 
		WHERE A.ID_Empresa=RT.ID_Empresa AND A.Nu_Relacion_Datos=RT.Nu_Relacion_Datos 
		AND A.ID_Relacion_Enlace_Tabla = RT.ID_Relacion_Enlace_Tabla AND A.ID_Origen_Tabla = RT.ID_Origen_Tabla )
		ORDER BY ID_Documento_Cabecera DESC ";

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}
	// C-02 - Fin 

	public function getRelacionTablaPresupuestoAFactura($arrParams){
		$query = "SELECT
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
FV.ID_Numero_Documento,
FV.Ss_Total,
FV.Fe_Emision
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS FV ON(FV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = FV.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = FV.ID_Serie_Documento_PK)
WHERE
RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . "
AND (RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " OR RT.ID_Relacion_Enlace_Tabla = " . $arrParams['iIdOrigenTabla'] . ")";
//AND (RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " OR RT.ID_Relacion_Enlace_Tabla = " . $arrParams['iIdOrigenTabla'] . ") LIMIT 1";//16/02/2021
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	// C-02 - Inicio
	public function CustOI_getRelacionTablaPresupuestoAFacturaDetalle($arrParams) 
	{   $query = " SELECT 
		FV.ID_Documento_Cabecera,
		TD.No_Tipo_Documento,
		SD.ID_Serie_Documento,
		FV.ID_Numero_Documento,			
		VD.Ss_Total  
		FROM relacion_tabla AS RT 
		JOIN documento_cabecera AS FV ON(FV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
		JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = FV.ID_Tipo_Documento)
		JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = FV.ID_Serie_Documento_PK)
		JOIN documento_detalle AS VD ON(VD.ID_Documento_Cabecera = FV.ID_Documento_Cabecera)
		WHERE
		RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . "
		AND (RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " OR RT.ID_Relacion_Enlace_Tabla = " . $arrParams['iIdOrigenTabla'] . "  )
		AND RT.ID_Relacion_Tabla = ( SELECT MAX( A.ID_Relacion_Tabla ) FROM relacion_tabla AS A 
		 WHERE A.ID_Empresa=RT.ID_Empresa AND A.Nu_Relacion_Datos=RT.Nu_Relacion_Datos 
		 AND A.ID_Relacion_Enlace_Tabla = RT.ID_Relacion_Enlace_Tabla AND A.ID_Origen_Tabla = RT.ID_Origen_Tabla )
		AND VD.ID_OI_Detalle =  ". $arrParams['ID_OI_Detalle']. " 
		ORDER BY FV.ID_Documento_Cabecera DESC " ;

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	
	// C-02 - Fin 

	public function getRelacionTablaPresupuestoAFacturaDetallev2($arrParams) 
	{   $query = " SELECT 
		FV.ID_Documento_Cabecera,
		TD.No_Tipo_Documento,
		SD.ID_Serie_Documento,
		FV.ID_Numero_Documento,
		VD.Ss_Total
		FROM relacion_tabla AS RT 
		JOIN documento_cabecera AS FV ON(FV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
		JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = FV.ID_Tipo_Documento)
		JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = FV.ID_Serie_Documento_PK)
		JOIN documento_detalle AS VD ON(VD.ID_Documento_Cabecera = FV.ID_Documento_Cabecera)
		WHERE
		VD.ID_OI_Detalle =  ". $arrParams['ID_OI_Detalle']. "
		AND RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos']. "
		ORDER BY FV.ID_Documento_Cabecera DESC ";

		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaPresupuestoAFacturaDetalle($arrParams) 
	{
		$query = "SELECT
			FV.ID_Documento_Cabecera,
			TD.No_Tipo_Documento,
			SD.ID_Serie_Documento,
			FV.ID_Numero_Documento,
			VD.Ss_Total
			FROM
			relacion_tabla AS RT
			JOIN documento_cabecera AS FV ON(FV.ID_Documento_Cabecera = RT.ID_Relacion_Enlace_Tabla)
			JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = FV.ID_Tipo_Documento)
			JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = FV.ID_Serie_Documento_PK)
			JOIN documento_detalle AS VD ON(VD.ID_Documento_Cabecera = FV.ID_Documento_Cabecera)
			WHERE
			RT.Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . "
			AND (RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " OR RT.ID_Relacion_Enlace_Tabla = " . $arrParams['iIdOrigenTabla'] . ")
			AND VD.ID_OI_Detalle = " . $arrParams['ID_OI_Detalle'];
		//AND (RT.ID_Origen_Tabla = " . $arrParams['iIdOrigenTabla'] . " OR RT.ID_Relacion_Enlace_Tabla = " . $arrParams['iIdOrigenTabla'] . ") LIMIT 1";//16/02/2021
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaOItoIngresoCompra($arrParams){
		$query = "SELECT
TD.No_Tipo_Documento,
IC.ID_Serie_Documento,
IC.ID_Numero_Documento,
IC.Ss_Total
FROM
documento_cabecera AS IC
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = IC.ID_Tipo_Documento)
WHERE
IC.ID_Orden_Ingreso = " . $arrParams['ID_Orden_Ingreso'];
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaOItoIngresoCompraDetalle($arrParams){
		$query = "SELECT
TD.No_Tipo_Documento,
IC.ID_Serie_Documento,
IC.ID_Numero_Documento,
SUM(CD.Ss_Total) AS Ss_Total
FROM
documento_cabecera AS IC
LEFT JOIN documento_detalle AS CD ON(IC.ID_Documento_Cabecera = CD.ID_Documento_Cabecera)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = IC.ID_Tipo_Documento)
WHERE
CD.ID_OI_Detalle = " . $arrParams['ID_Orden_Ingreso'] . "
GROUP BY
TD.No_Tipo_Documento,
IC.ID_Serie_Documento,
IC.ID_Numero_Documento";
//array_debug($query);
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getRelacionTablaReferencia($arrParams){
		$obj = $this->db->query("SELECT ID_Origen_Tabla FROM relacion_tabla WHERE Nu_Relacion_Datos = " . $arrParams['iRelacionDatos'] . " AND ID_Relacion_Enlace_Tabla = " . $arrParams['iIdRelacionEnlaceTabla'] . " LIMIT 1")->row();
		return (is_object($obj) ? $obj->ID_Origen_Tabla : '');
	}

	public function getTiposServicios($arrPost){
		$query = "SELECT * FROM servicio_presupuesto_negocio";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getCuentasContables($arrPost){
		$query = "SELECT * FROM configuracion_cuenta_contable";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function getProductos($arrPost){
		$query = "SELECT * FROM producto";
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	function obtenerEstadoDocumento($iEstado){
		if( $iEstado == 6 )
			return '<span class="label label-success">Completado</span>';
		else if( $iEstado == 7 )
			return '<span class="label label-warning">Anulado</span>';
		else if( $iEstado == 8 )
			return '<span class="label label-success">Enviado</span>';
		else if( $iEstado == 9 )
			return '<span class="label label-danger">Completado E.</span>';
		else if( $iEstado == 10 )
			return '<span class="label label-warning">Anulado E.</span>';
		else if( $iEstado == 11 )
			return '<span class="label label-danger">Anulado Error</span>';
	}

	function obtenerEstadoRegistroArray($iEstado){
		if( $iEstado == 1 )
			return array('No_Estado' => 'Activo','No_Class_Estado' => 'success');
		return array('No_Estado' => 'Inactivo','No_Class_Estado' => 'danger');
	}

	function obtenerEstadoDocumentoArray($iEstado){
		if( $iEstado == 6 )
			return array('No_Estado' => 'Completado','No_Class_Estado' => 'success');
		else if( $iEstado == 7 )
			return array('No_Estado' => 'Anulado','No_Class_Estado' => 'warning');
		else if( $iEstado == 8 )
			return array('No_Estado' => 'Enviado','No_Class_Estado' => 'success');
		else if( $iEstado == 9 )
			return array('No_Estado' => 'Completado E.','No_Class_Estado' => 'danger');
		else if( $iEstado == 10 )
			return array('No_Estado' => 'Anulado E.','No_Class_Estado' => 'warning');
		else if( $iEstado == 11 )
			return array('No_Estado' => 'Error','No_Class_Estado' => 'danger');
	}

	function obtenerEstadoRecepcionArray($iEstado){
		if( $iEstado == 1 )
			return array('No_Estado' => 'Marketplace - Empresa','No_Class_Estado' => 'success');
		else if( $iEstado == 2 )
			return array('No_Estado' => 'Marketplace - Delivery','No_Class_Estado' => 'primary');
		else if( $iEstado == 3 )
			return array('No_Estado' => 'Tienda online - Empresa','No_Class_Estado' => 'primary');
		else if( $iEstado == 4 )
			return array('No_Estado' => 'Tienda online - Delivery','No_Class_Estado' => 'primary');
		else if( $iEstado == 5 )
			return array('No_Estado' => 'Empresa','No_Class_Estado' => 'primary');
		else if( $iEstado == 6 )
			return array('No_Estado' => 'Delivery','No_Class_Estado' => 'primary');
		else
			return array('No_Estado' => 'Recojo en Tienda','No_Class_Estado' => 'primary');
	}

	function obtenerEstadoDespachoArray($iEstado){
		if( $iEstado == 0 )
			return array('No_Estado' => 'Pendiente','No_Class_Estado' => 'warning');
		else if( $iEstado == 1 )
			return array('No_Estado' => 'Preparando','No_Class_Estado' => 'default');
		else if( $iEstado == 2 )
			return array('No_Estado' => 'Enviado','No_Class_Estado' => 'success');
		else if( $iEstado == 3 )
			return array('No_Estado' => 'Entregado','No_Class_Estado' => 'primary');
		else if( $iEstado == 4 )
			return array('No_Estado' => 'Rechazado','No_Class_Estado' => 'danger');
	}

	function obtenerEstadoOIArray($iEstado){
		if( $iEstado == 1 )
			return array('No_Estado' => 'En taller','No_Class_Estado' => 'info');
		else if( $iEstado == 2 )
			return array('No_Estado' => 'Entregado','No_Class_Estado' => 'primary');
		else
			return array('No_Estado' => 'Facturado','No_Class_Estado' => 'success');
	}

	function obtenerEstadoFacturacionArray($iEstado){
		if( $iEstado == 0 )
			return array('No_Estado' => 'Pendiente','No_Class_Estado' => 'primary');
		else if( $iEstado == 1 )
			return array('No_Estado' => 'Facturado','No_Class_Estado' => 'success');
		else if( $iEstado == 2 )
			return array('No_Estado' => 'Anulado','No_Class_Estado' => 'warning');
		else
			return array('No_Estado' => 'Reclamo','No_Class_Estado' => 'danger');
	}

	public function getDocumentoEnlaceOrigen($arrParams){
		$query = "SELECT TD.No_Tipo_Documento_Breve, VC.ID_Serie_Documento AS _ID_Serie_Documento, SD.ID_Serie_Documento, COALESCE(VC.ID_Numero_Documento,VC.ID_Documento_Cabecera) AS ID_Numero_Documento, VC.Ss_Total FROM
documento_enlace AS VE
JOIN documento_cabecera AS VC ON(VE.ID_Documento_Cabecera_Enlace = VC.ID_Documento_Cabecera)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = VC.ID_Tipo_Documento)
LEFT JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
WHERE VE.ID_Documento_Cabecera = " . $arrParams['ID_Documento_Cabecera'];
		
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}
		
		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);		
	}

	public function getDocumentoEnlaceDestino($arrParams){
		$query = "SELECT TD.No_Tipo_Documento_Breve, VC.ID_Serie_Documento AS _ID_Serie_Documento, SD.ID_Serie_Documento, VC.ID_Numero_Documento, VC.Ss_Total FROM
			documento_enlace AS VE
			JOIN documento_cabecera AS VC ON(VE.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)
			JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = VC.ID_Tipo_Documento)
			LEFT JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
			WHERE VE.ID_Documento_Cabecera_Enlace = " . $arrParams['ID_Documento_Cabecera'];
		if (!$this->db->simple_query($query)){
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'sStatus' => 'warning',
			'sMessage' => 'No se encontro registro',
		);
	}

	public function updateEstadoDocumentoFacturado($NuPresupuesto) {
		$arrData = $this->db->query("SELECT ID_Producto, Nu_Estado FROM documento_cabecera where ID_Numero_Documento = " . $NuPresupuesto)->result();
		if(!empty($arrData) && $arrData[0]->Nu_Estado == '21'){
			$this->db->update('documento_cabecera', ['Nu_Estado' => 22, 'Nu_Presupuesto' => intval($NuPresupuesto)], ['ID_Numero_Documento' => intval($NuPresupuesto)]);
			$arrDataOI = $this->db->query("SELECT ID_Orden_Ingreso FROM orden_ingreso where ID_Producto = " . $arrData[0]->ID_Producto)->result();
			if(!empty($arrDataOI)){
				foreach($arrDataOI as $oi) {
					$this->db->update('orden_ingreso', ['Nu_Estado_Facturacion' => 0], ['ID_Orden_Ingreso' => intval($oi->ID_Orden_Ingreso)]);
				}
			}
		}
	}

	public function getPlacaAndOIByGuiaCabeceraID($ID_Guia_Cabecera) {
		$arrData = $this->db->select('producto.No_Placa_Vehiculo, documento_cabecera.ID_Documento_Cabecera')
			->from('relacion_tabla')
			->join('documento_cabecera', 'documento_cabecera.ID_Documento_Cabecera = relacion_tabla.ID_Origen_Tabla', 'join')
			->join('producto', 'producto.ID_Producto = documento_cabecera.ID_Producto', 'join')
			->where('ID_Relacion_Enlace_Tabla', $ID_Guia_Cabecera)
			->where('Nu_Relacion_Datos', 0)
			->limit(1)->get()->result();
		$result = [
			'No_Placa_Vehiculo' => '',
			'Orden_Ingreso' => '',
		];
		foreach ($arrData as $row) {
			$result['No_Placa_Vehiculo'] = $row->No_Placa_Vehiculo;
			$arrParams = [
				'iRelacionDatos' => 6,
				'iIdOrigenTabla' => $row->ID_Documento_Cabecera
			];
			$arrResponseRelacion = $this->getRelacionTablaMultiplePresupuestoAndOI($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$result['Orden_Ingreso'] = $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento;
				}
			}
		}
		return $result;
	}
}
