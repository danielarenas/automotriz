<?php
class VentaModel extends CI_Model{
	var $table          				= 'documento_cabecera';
	var $table_documento_detalle		= 'documento_detalle';
	var $table_documento_enlace			= 'documento_enlace';
	var $table_tipo_documento			= 'tipo_documento';
	var $table_impuesto_cruce_documento	= 'impuesto_cruce_documento';
	var $table_entidad					= 'entidad';
	var $table_tipo_documento_identidad	= 'tipo_documento_identidad';
	var $table_moneda					= 'moneda';
	var $table_medio_pago				= 'medio_pago';
	var $table_tipo_medio_pago = 'tipo_medio_pago';
	var $table_organizacion				= 'organizacion';
	var $table_tabla_dato				= 'tabla_dato';
	var $table_serie_documento = 'serie_documento';
	var $table_servicio_presupuesto_negocio = 'servicio_presupuesto_negocio';
	
    var $column_order = array('');
    var $column_search = array('');
    var $order = array('');
	
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
    	$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
        
        if($this->input->post('Filtro_TiposDocumento'))
        	$this->db->where('VC.ID_Tipo_Documento', $this->input->post('Filtro_TiposDocumento'));
        
        if($this->input->post('Filtro_SeriesDocumento'))
        	$this->db->where('VC.ID_Serie_Documento', $this->input->post('Filtro_SeriesDocumento'));
        
        if($this->input->post('Filtro_NumeroDocumento'))
        	$this->db->where('VC.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento'));

        if($this->input->post('Filtro_Estado') != '')
        	$this->db->where('VC.Nu_Estado', $this->input->post('Filtro_Estado'));
        
        if($this->input->post('Filtro_Entidad'))
        	$this->db->where('CLI.No_Entidad', $this->input->post('Filtro_Entidad'));

        if($this->input->post('Filtro_MediosPago') != 0)
        	$this->db->where('MP.ID_Medio_Pago', $this->input->post('Filtro_MediosPago'));

        if($this->input->post('Filtro_tarjeta_credito') != 0)
        	$this->db->where('VC.ID_Tipo_Medio_Pago', $this->input->post('Filtro_tarjeta_credito'));
        
        $this->db->select('VC.ID_Documento_Cabecera, VC.ID_Tipo_Documento, VC.Fe_Emision, TDOCU.No_Tipo_Documento_Breve, VC.ID_Serie_Documento, VC.ID_Numero_Documento, TDOCUIDEN.No_Tipo_Documento_Identidad_Breve, CLI.ID_Entidad AS id_cliente, CLI.No_Entidad, MONE.No_Signo, VC.Ss_Total, VC.Nu_Estado, TDOCU.Nu_Enlace, VC.Nu_Descargar_Inventario, VE.ID_Documento_Cabecera as ID_Documento_Cabecera_VE, VE.ID_Documento_Cabecera_Enlace, VC.Txt_Url_PDF, VC.Txt_Url_XML, VC.Txt_Url_CDR, VC.Txt_Respuesta_Sunat_FE, MP.Nu_Tipo AS Nu_Tipo_Medio_Pago, VC.Ss_Total_Saldo, SD.ID_POS, VC.Ss_Saldo_Detraccion, VC.Ss_Retencion, MP.No_Medio_Pago, TMP.No_Tipo_Medio_Pago, VC.Nu_Presupuesto, VC.Nu_Orden_Compra, VC.Fe_Entrega_Factura, SPN.No_Servicio_Presupuesto_Negocio, VC.Fe_Entrega_Vencimiento_Factura')
		->from($this->table . ' AS VC')
		->join($this->table_serie_documento . ' AS SD', 'SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK', 'left')
		->join($this->table_documento_enlace . ' AS VE', 'VE.ID_Documento_Cabecera_Enlace = VC.ID_Documento_Cabecera', 'left')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
		->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = VC.ID_Entidad', 'join')
		//->join($this->table_entidad . ' AS VEN', 'VEN.ID_Entidad = VC.ID_Mesero', 'left')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = CLI.ID_Tipo_Documento_Identidad', 'join')
		->join($this->table_moneda . ' AS MONE', 'MONE.ID_Moneda = VC.ID_Moneda', 'join')
		->join($this->table_medio_pago . ' AS MP', 'MP.ID_Medio_Pago = VC.ID_Medio_Pago', 'join')
		->join($this->table_tipo_medio_pago . ' AS TMP', 'VC.ID_Tipo_Medio_Pago = TMP.ID_Tipo_Medio_Pago', 'left')
		->join($this->table_servicio_presupuesto_negocio . ' AS SPN', 'VC.ID_Servicio_Presupuesto_Negocio = SPN.ID_Servicio_Presupuesto_Negocio', 'left')
    	//->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
    	//->join($this->table_tabla_dato . ' AS TCOMI', 'TCOMI.ID_Tabla_Dato = VC.ID_Comision AND TCOMI.No_Relacion = "Porcentaje_Comision_Vendedores"', 'left')
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
    	->where('VC.ID_Tipo_Asiento', 1)
    	->where_in('VC.ID_Tipo_Documento', array('2','3','4','5','6'));
		
        if(isset($_POST['order']))
        	$this->db->order_by( 'CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
        else if(isset($this->order))
        	$this->db->order_by( 'CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );

		/*
        if(isset($_POST['order']))
        	$this->db->order_by( 'VC.Fe_Emision DESC, VC.ID_Tipo_Documento DESC, VC.ID_Serie_Documento DESC, CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
        else if(isset($this->order))
        	$this->db->order_by( 'VC.Fe_Emision DESC, VC.ID_Tipo_Documento DESC, VC.ID_Serie_Documento DESC, CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
			*/
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
	}

	public function get_by_id($ID)
	{
		$query = "SELECT
			ALMA.Txt_FE_Ruta,
			ALMA.Txt_FE_Token,
			VC.ID_Empresa,
			VC.ID_Organizacion,
			VC.ID_Almacen,
			VC.ID_Documento_Cabecera,
			CLI.ID_Entidad,
			TDOCUIDEN.No_Tipo_Documento_Identidad_Breve,
			CLI.No_Entidad,
			CLI.Nu_Documento_Identidad,
			CLI.Txt_Direccion_Entidad,
			VC.ID_Tipo_Documento,
			TDOCU.Nu_Impuesto,
			TDOCU.Nu_Enlace,
			VC.ID_Serie_Documento,
			VC.ID_Numero_Documento,
			VC.Fe_Emision,
			VC.ID_Moneda,
			VC.ID_Medio_Pago,
			VC.Fe_Vencimiento,
			VC.Nu_Descargar_Inventario,
			VC.ID_Lista_Precio_Cabecera,
			VD.ID_Producto,
			PRO.Nu_Tipo_Producto,
			PRO.Nu_Codigo_Barra,
			PRO.No_Producto,
			VD.Ss_Precio,
			VD.Qt_Producto,
			VD.ID_Impuesto_Cruce_Documento,
			VD.Ss_SubTotal AS Ss_SubTotal_Producto,
			VD.Ss_Impuesto AS Ss_Impuesto_Producto,
			ROUND(VD.Ss_Descuento, 2) AS Ss_Descuento_Producto,
			ROUND(VD.Ss_Descuento_Impuesto, 2) AS Ss_Descuento_Impuesto_Producto,
			ROUND(VD.Po_Descuento, 2) AS Po_Descuento_Impuesto_Producto,
			ROUND(VD.Ss_Total, 2) AS Ss_Total_Producto,
			PRO.ID_Impuesto_Icbper,
			ICDOCU.Ss_Impuesto,
			VE.ID_Documento_Cabecera_Enlace,
			VE.ID_Tipo_Documento_Modificar,
			VE.ID_Serie_Documento_Modificar,
			VE.ID_Numero_Documento_Modificar,
			MP.Nu_Tipo,
			IMP.Nu_Tipo_Impuesto,
			IMP.ID_Impuesto,
			VC.Txt_Glosa,
			ROUND(VC.Ss_Descuento, 2) AS Ss_Descuento,
			MONE.No_Signo,
			ROUND(VC.Ss_Total, 2) AS Ss_Total,
			MONE.No_Moneda,
			VC.Po_Descuento,
			TDOCUIDEN.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_TDI,
			UM.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_UM,
			VC.Txt_Url_PDF,
			VC.Txt_Url_XML,
			VC.Txt_Url_CDR,
			VC.Txt_Url_Comprobante,
			TDOCU.No_Tipo_Documento,
			VC.Nu_Codigo_Motivo_Referencia,
			VC.Nu_Detraccion,
			TC.Ss_Venta_Oficial AS Ss_Tipo_Cambio,
			ITEMSUNAT.Nu_Valor AS Nu_Codigo_Producto_Sunat,
			VC.No_Formato_PDF,
			MP.Txt_Medio_Pago,
			MP.No_Codigo_Sunat_PLE AS No_Codigo_Medio_Pago_Sunat_PLE,
			VC.Txt_Garantia,
			VC.ID_Mesero,
			VC.ID_Comision,
			CLI.Nu_Dias_Credito,
			SD.Nu_Cantidad_Caracteres,
			CLI.Txt_Email_Entidad,
			MONE.Nu_Valor_Fe AS Nu_Valor_Fe_Moneda,
			IMP.Nu_Valor_Fe AS Nu_Valor_Fe_Impuesto,
			VC.Nu_Retencion,
			VC.Ss_Retencion,
			VC.ID_Tipo_Medio_Pago,
			VC.Nu_Presupuesto,
			VC.Nu_Orden_Compra,
			VC.Fe_Entrega_Factura,
			VC.Fe_Entrega_Vencimiento_Factura,
			VC.ID_Servicio_Presupuesto_Negocio,
			VD.ID_OI_Detalle,
			VD.ID_Documento_Detalle,
			VC.Ss_Saldo_Detraccion,
			VC.Ss_Total_Saldo
			FROM
			" . $this->table . " AS VC
			JOIN organizacion AS ORG ON(VC.ID_Organizacion = ORG.ID_Organizacion)
			JOIN almacen AS ALMA ON(VC.ID_Almacen = ALMA.ID_Almacen)
			JOIN " . $this->table_documento_detalle . " AS VD ON (VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
			JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
			JOIN " . $this->table_entidad . " AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
			JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
			JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
			LEFT JOIN " . $this->table_tabla_dato . " AS ITEMSUNAT ON(ITEMSUNAT.ID_Tabla_Dato = PRO.ID_Producto_Sunat AND ITEMSUNAT.No_Relacion = 'Catalogo_Producto_Sunat')
			JOIN unidad_medida AS UM ON(UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
			JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
			JOIN " . $this->table_impuesto_cruce_documento . " AS ICDOCU ON(ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
			JOIN impuesto AS IMP ON(IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
			JOIN medio_pago AS MP ON(MP.ID_Medio_Pago = VC.ID_Medio_Pago)
			JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
			LEFT JOIN tasa_cambio AS TC ON(TC.ID_Empresa = VC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND TC.Fe_Ingreso = VC.Fe_Emision)
			LEFT JOIN (
			SELECT
			VE.ID_Documento_Cabecera,
			VE.ID_Documento_Cabecera_Enlace,
			VC.ID_Tipo_Documento AS ID_Tipo_Documento_Modificar,
			VC.ID_Serie_Documento AS ID_Serie_Documento_Modificar,
			VC.ID_Numero_Documento AS ID_Numero_Documento_Modificar
			FROM
			" . $this->table . " AS VC
			JOIN " . $this->table_documento_enlace . " AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera_Enlace)
			) AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera)
			WHERE
			VC.ID_Tipo_Asiento = 1
			AND VC.ID_Documento_Cabecera = " . $ID;

		if (!$this->db->simple_query($query)){
			$error = $this->db->error();
			return [
				'status' => 'danger',
				'style_modal' => 'modal-danger',
				'sStatus' => 'danger',
				'message' => 'Problemas al generar formato documento electrónico',
				'sMessage' => 'Problemas al generar formato documento electrónico',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			];
		}

		$arrResponseSQL = $this->db->query($query);
		if ($arrResponseSQL->num_rows() > 0){
			return [
				'status' => 'success',
				'style_modal' => 'modal-success',
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			];
		}

		return [
			'status' => 'warning',
			'style_modal' => 'modal-warning',
			'sStatus' => 'warning',
			'sMessage' => 'No hay registro',
			'message' => 'No hay registro',
		];
	}

	public function agregarVenta($arrVentaCabecera, $arrVentaDetalle, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrClienteNuevo)
	{
		$query = "SELECT ID_Tipo_Documento, ID_Serie_Documento_PK, ID_Serie_Documento, Nu_Numero_Documento FROM serie_documento WHERE ID_Serie_Documento_PK = " . $arrVentaCabecera['ID_Serie_Documento_PK'];
		$arrSerieDocumento = $this->db->query($query)->row();
		$arrVentaCabecera['ID_Numero_Documento'] = $arrSerieDocumento->Nu_Numero_Documento;

		if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
			$this->db->trans_rollback();
			return ['status' => 'danger', 'style_modal' => 'modal-danger', 'message' => 'Deben configurar serie, no existe'];
		}

		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND ID_Tipo_Documento = " . $arrSerieDocumento->ID_Tipo_Documento . " AND ID_Serie_Documento = '" . $arrSerieDocumento->ID_Serie_Documento . "' AND ID_Numero_Documento = '" . $arrSerieDocumento->Nu_Numero_Documento . "' LIMIT 1")->row()->existe > 0){
			$this->db->trans_rollback();
			return ['status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe'];
		} else {
			$this->db->trans_begin();
			$iTipoCliente = $arrVentaCabecera['iTipoCliente'];
			unset($arrVentaCabecera['iTipoCliente']);

			$Nu_Correlativo = 0;
			$Fe_Year = ToYear($arrVentaCabecera['Fe_Emision']);
			$Fe_Month = ToMonth($arrVentaCabecera['Fe_Emision']);
			$arrCorrelativoPendiente = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento_pendiente WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' ORDER BY Nu_Correlativo DESC LIMIT 1")->result();

			if (!empty($arrCorrelativoPendiente)){
				$Nu_Correlativo = $arrCorrelativoPendiente[0]->Nu_Correlativo;
				$this->db->where('ID_Empresa', $this->user->ID_Empresa);
				$this->db->where('ID_Tipo_Asiento', 1);
				$this->db->where('Fe_Year', $Fe_Year);
				$this->db->where('Fe_Month', $Fe_Month);
				$this->db->where('Nu_Correlativo', $Nu_Correlativo);
				$this->db->delete('correlativo_tipo_asiento_pendiente');
			} else {
				if($this->db->query("SELECT COUNT(*) AS existe FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->existe > 0){
					$sql_correlativo_libro_sunat = "UPDATE
						correlativo_tipo_asiento
						SET
						Nu_Correlativo=Nu_Correlativo + 1
						WHERE
						ID_Empresa=" . $this->user->ID_Empresa . "
						AND ID_Tipo_Asiento=1
						AND Fe_Year='" . $Fe_Year. "'
						AND Fe_Month='" . $Fe_Month . "'";
											$this->db->query($sql_correlativo_libro_sunat);
										} else {
											$sql_correlativo_libro_sunat = "INSERT INTO correlativo_tipo_asiento (
						ID_Empresa,
						ID_Tipo_Asiento,
						Fe_Year,
						Fe_Month,
						Nu_Correlativo
						) VALUES (
						" . $this->user->ID_Empresa . ",
						1,
						'" . $Fe_Year . "',
						'" . $Fe_Month . "',
						1
						);";
					$this->db->query($sql_correlativo_libro_sunat);
				}
				$Nu_Correlativo = $this->db->query("SELECT Nu_Correlativo FROM correlativo_tipo_asiento WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND Fe_Year = '" . $Fe_Year . "' AND Fe_Month = '" . $Fe_Month . "' LIMIT 1")->row()->Nu_Correlativo;
			}

			if (is_array($arrClienteNuevo)){
			    unset($arrVentaCabecera['ID_Entidad']);
			    //Si no existe el cliente, lo crearemos
			    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
						$arrCliente = [
							'ID_Empresa' => $this->user->ID_Empresa,
							'ID_Organizacion' => $arrVentaCabecera['ID_Organizacion'],
							'Nu_Tipo_Entidad'	=> 0,
							'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
							'Nu_Documento_Identidad' => $arrClienteNuevo['Nu_Documento_Identidad'],
							'No_Entidad' => $arrClienteNuevo['No_Entidad'],
							'Txt_Direccion_Entidad' => $arrClienteNuevo['Txt_Direccion_Entidad'],
							'Nu_Telefono_Entidad'	=> $arrClienteNuevo['Nu_Telefono_Entidad'],
							'Nu_Celular_Entidad' => $arrClienteNuevo['Nu_Celular_Entidad'],
							'Nu_Estado' => 1,
						];
		    		$this->db->insert('entidad', $arrCliente);
		    		$Last_ID_Entidad = $this->db->insert_id();
			    } else {
					$this->db->trans_rollback();
					return ['status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente'];
				}
	    		$arrVentaCabecera = array_merge($arrVentaCabecera, array("ID_Entidad" => $Last_ID_Entidad));
			}

			if ($iTipoCliente == 3) {
				$query = "SELECT ID_Entidad FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND No_Entidad LIKE '%clientes varios%' LIMIT 1"; //1 = ID_Entidad -> Cliente varios
				if (!$this->db->simple_query($query)){
					$this->db->trans_rollback();
					$error = $this->db->error();
					return [
						'sStatus' => 'danger',
						'sMessage' => 'Problemas al obtener datos de clientes varios',
						'sClassModal' => 'modal-danger',
						'sCodeSQL' => $error['code'],
						'sMessageSQL' => $error['message'],
					];
				}
				$arrResponseSQL = $this->db->query($query);
				if ($arrResponseSQL->num_rows() > 0){
					$arrData = $arrResponseSQL->result();
					$arrVentaCabecera = array_merge($arrVentaCabecera, array("ID_Entidad" => $arrData[0]->ID_Entidad));
				} else {
					$this->db->trans_rollback();
					return [
						'sStatus' => 'warning',
						'sMessage' => 'No se encontro clientes varios',
						'sClassModal' => 'modal-warning',
					];
				}
			}

			$arrIdPresupuesto = $arrVentaCabecera['arrIdPresupuesto'];
			$arrNuPresupuesto = [];
			unset($arrVentaCabecera['arrIdPresupuesto']);
			foreach(explode(",", $arrIdPresupuesto) as $IdPresupuesto) {
				if (!trim($IdPresupuesto)) {
					continue;
				}
				$arrDataNuPresupuesto = $this->db->query("SELECT ID_Numero_Documento FROM documento_cabecera where ID_Documento_Cabecera = " . $IdPresupuesto)->result();
				if(!empty($arrDataNuPresupuesto)){
					$arrNuPresupuesto[] = $arrDataNuPresupuesto[0]->ID_Numero_Documento;
				}
			}

			$arrVentaCabecera = array_merge($arrVentaCabecera, array("Nu_Correlativo" => $Nu_Correlativo));
			$this->db->insert($this->table, $arrVentaCabecera);
			$Last_ID_Documento_Cabecera = $this->db->insert_id();

			// Para Notas de Crédito y Débito
			if ($esEnlace == 1){
				$table_documento_enlace = [
					'ID_Empresa' => $this->user->ID_Empresa,
					'ID_Documento_Cabecera'	=> $Last_ID_Documento_Cabecera,
					'ID_Documento_Cabecera_Enlace'=> $ID_Documento_Cabecera_Enlace
				];
				$this->db->insert($this->table_documento_enlace, $table_documento_enlace);
			}

			foreach ($arrVentaDetalle as $row) {
				$documento_detalle[] = [
					'ID_Empresa' => $this->user->ID_Empresa,
					'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
					'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
					'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio' => $this->security->xss_clean($row['Ss_Precio']),
					'Ss_SubTotal' => $this->security->xss_clean($row['Ss_SubTotal']),
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['Ss_Descuento'],
					'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
					'Ss_Impuesto' => $this->security->xss_clean($row['Ss_Impuesto']),
					'Ss_Total' => round($this->security->xss_clean($row['Ss_Total']), 2),
					'ID_OI_Detalle' => $this->security->xss_clean($row['iIdPresupuesto'])
				];
			}
			$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);

			if (!empty($arrIdPresupuesto)) {
				$arrIdPresupuesto = explode(',',$arrIdPresupuesto);
				foreach ($arrIdPresupuesto as $row){
					//verificar que exista presupuesto
					$arrPresupuesto = $this->db->query("SELECT ID_Documento_Cabecera FROM documento_cabecera WHERE ID_Documento_Cabecera= " . $row . " LIMIT 1")->row();
					if ( !is_object($arrPresupuesto) ) {
						$this->db->trans_rollback();
						return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'No existe relación con presupuesto ID -> ' . $row);
					}

					//insertar
					$arrRelacionTabla = array(
						'ID_Empresa' => $arrVentaCabecera['ID_Empresa'],
						'Nu_Relacion_Datos' => 5,//Relación Factura a Presupuesto
						'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
						'ID_Origen_Tabla' => $row,
					);
					if ( $this->db->insert('relacion_tabla', $arrRelacionTabla) <= 0 ) {
						$this->db->trans_rollback();
						return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'No se pudo insertar relación del presupuesto ID -> ' . $row);
					}

					//actualizar
					if($arrVentaCabecera['ID_Tipo_Documento'] == '5') {
						$arrCambiarEstadoDocumentoUPD = [
							'ID_Documento_Cabecera' => $row,
							'Nu_Estado' => 22,
						];
					} else {
						$arrCambiarEstadoDocumentoUPD = [
							'ID_Documento_Cabecera' => $row,
							'Nu_Estado' => 21, //Facturado - Liquidación
						];
					}
					$where_presupuesto = array('ID_Documento_Cabecera' => $row );
		    		if ( $this->db->update('documento_cabecera', $arrCambiarEstadoDocumentoUPD, $where_presupuesto) <= 0 ) {
						$this->db->trans_rollback();
						return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'No se pudo cambiar estado del presupuesto ID -> ' . $row);
					}

					//Cambiar estado a Orden de ingreso a Facturado
					$query_orden_ingreso = "SELECT ID_Origen_Tabla FROM relacion_tabla WHERE ID_Relacion_Enlace_Tabla = " . $row . " AND Nu_Relacion_Datos IN(1,6)";
					if ( !$this->db->simple_query($query_orden_ingreso) ){
						$this->db->trans_rollback();
						return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al relacionar OI con presupuesto');
					}

					$arrOrdenIngreso = $this->db->query($query_orden_ingreso);
					if ( $arrOrdenIngreso->num_rows() > 0 ){
						$arrOrdenIngreso = $arrOrdenIngreso->result();
						foreach ($arrOrdenIngreso as $row2){
							if($arrVentaCabecera['ID_Tipo_Documento'] != '5') {
								$sql = "UPDATE orden_ingreso SET Nu_Estado_Facturacion=1 WHERE ID_Orden_Ingreso=" . $row2->ID_Origen_Tabla;//3 = Facturado
								$this->db->query($sql);
							}
						}
					} else {
						//Cambiar estado a Orden de ingreso a Facturado
						$query_orden_ingreso = "SELECT ID_Relacion_Enlace_Tabla FROM relacion_tabla WHERE ID_Origen_Tabla = " . $row . " AND Nu_Relacion_Datos IN(1,6)";
						if ( !$this->db->simple_query($query_orden_ingreso) ){
							$this->db->trans_rollback();
							return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al relacionar OI con presupuesto');
						}

						$arrOrdenIngreso = $this->db->query($query_orden_ingreso);
						if ( $arrOrdenIngreso->num_rows() > 0 ){
							$arrOrdenIngreso = $arrOrdenIngreso->result();
							foreach ($arrOrdenIngreso as $row2){
								if($arrVentaCabecera['ID_Tipo_Documento'] != '5') {
									$sql = "UPDATE orden_ingreso SET Nu_Estado_Facturacion=1 WHERE ID_Orden_Ingreso=" . $row2->ID_Relacion_Enlace_Tabla;//3 = Facturado
									$this->db->query($sql);
								}
							}
						} else {
							$this->db->trans_rollback();
							return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'No se encontro OI relacion con presupuesto');
						}
					}
				}
				//$this->db->insert_batch('relacion_tabla', $arrRelacionTabla);//presupuesto
				//$this->db->update_batch('documento_cabecera', $arrCambiarEstadoDocumentoUPD, 'ID_Documento_Cabecera');//presupuesto
			}

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return ['status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar'];
			} else {
				$this->db->trans_commit();
				$sql = "UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1 WHERE ID_Serie_Documento_PK='" . $arrVentaCabecera['ID_Serie_Documento_PK'] . "'";
				$this->db->query($sql);
				return ['status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado', 'Last_ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera, 'sEnviarSunatAutomatic' => 'No'];
			}
		}
	}

	public function actualizarVenta($where, $arrVentaCabecera, $arrVentaDetalle, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrClienteNuevo, $arrDeleteDetalle = null)
	{
		$this->db->trans_begin();
		$data = [
			'Fe_Emision' => $arrVentaCabecera['Fe_Emision'],
			'ID_Medio_Pago' => $arrVentaCabecera['ID_Medio_Pago'],
			'ID_Tipo_Medio_Pago' => $arrVentaCabecera['ID_Tipo_Medio_Pago'],
			'Ss_Total_Saldo' => $arrVentaCabecera['Ss_Total_Saldo'],
			'Ss_Saldo_Detraccion' => $arrVentaCabecera['Ss_Saldo_Detraccion'],
			'Ss_Retencion' => $arrVentaCabecera['Ss_Retencion'],
			'Nu_Detraccion' => $arrVentaCabecera['Nu_Detraccion'],
			'Nu_Retencion' => $arrVentaCabecera['Nu_Retencion'],
			'Fe_Vencimiento' => $arrVentaCabecera['Fe_Vencimiento'],
			'Txt_Glosa' => $arrVentaCabecera['Txt_Glosa'],
			'Nu_Presupuesto' => $arrVentaCabecera['Nu_Presupuesto'],
			'Nu_Orden_Compra' => $arrVentaCabecera['Nu_Orden_Compra'],
			'Ss_Total' => $arrVentaCabecera['Ss_Total'],
			'Fe_Entrega_Factura' => $arrVentaCabecera['Fe_Entrega_Factura'],
			'Fe_Entrega_Vencimiento_Factura' => $arrVentaCabecera['Fe_Entrega_Vencimiento_Factura'],
			'ID_Servicio_Presupuesto_Negocio' => $arrVentaCabecera['ID_Servicio_Presupuesto_Negocio'],
		];
		$where = array('ID_Documento_Cabecera' => $where['ID_Documento_Cabecera']);
		$this->db->update('documento_cabecera', $data, $where);

		foreach ($arrVentaDetalle as $row) {
			if(!$row['ID_Documento_Detalle']) {
				if(!$row['ID_Documento_Cabecera']) {
					$this->agregarVenta($arrVentaCabecera, $arrVentaDetalle, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrClienteNuevo);
				} else {
					$documento_detalle[] = [
						'ID_Empresa' => $this->user->ID_Empresa,
						'ID_Documento_Cabecera' => $where['ID_Documento_Cabecera'],
						'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
						'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
						'Ss_Precio' => $this->security->xss_clean($row['Ss_Precio']),
						'Ss_SubTotal' => $this->security->xss_clean($row['Ss_SubTotal']),
						'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
						'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
						'Po_Descuento' => $row['Ss_Descuento'],
						'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
						'Ss_Impuesto' => $this->security->xss_clean($row['Ss_Impuesto']),
						'Ss_Total' => round($this->security->xss_clean($row['Ss_Total']), 2),
						'ID_OI_Detalle' => $this->security->xss_clean($row['iIdPresupuesto'])
					];
				}
			} else {
				if(!@$row['ID_Documento_Cabecera']) {
					$this->agregarVenta($arrVentaCabecera, $arrVentaDetalle, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrClienteNuevo);
				} else {
					$documento_detalle_update = [
						'ID_Empresa' => $this->user->ID_Empresa,
						'ID_Producto' => $this->security->xss_clean($row['ID_Producto']),
						'Qt_Producto' => $this->security->xss_clean($row['Qt_Producto']),
						'Ss_Precio' => $this->security->xss_clean($row['Ss_Precio']),
						'Ss_SubTotal' => $this->security->xss_clean($row['Ss_SubTotal']),
						'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
						'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
						'Po_Descuento' => $row['Ss_Descuento'],
						'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
						'Ss_Impuesto' => $this->security->xss_clean($row['Ss_Impuesto']),
						'Ss_Total' => round($this->security->xss_clean($row['Ss_Total']), 2),
						'ID_OI_Detalle' => $this->security->xss_clean($row['iIdPresupuesto'])
					];
					$where = array('ID_Documento_Detalle' => $row['ID_Documento_Detalle']);
					$this->db->update('documento_detalle', $documento_detalle_update, $where);
				}
			}
		}

		if(!empty($documento_detalle)) {
			$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
		}

		if($arrDeleteDetalle) {
			foreach(explode(',', $arrDeleteDetalle) as $deleteItem) {
				$this->db->where('ID_Documento_Detalle', intval($deleteItem));
				$this->db->delete('documento_detalle');
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
		} else {
			$this->db->trans_commit();
			return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado', 'Last_ID_Documento_Cabecera' => $where['ID_Documento_Cabecera'], 'sEnviarSunatAutomatic' => 'No');
		}
	}

	public function anularVenta($ID, $Nu_Enlace, $Nu_Descargar_Inventario)
	{
		$this->db->trans_begin();
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
		$this->db->delete($this->table_documento_detalle);
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
		$this->db->delete('documento_medio_pago');

		if ($Nu_Enlace == 1){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
			$this->db->delete($this->table_documento_enlace);
		}

		if ($Nu_Descargar_Inventario == 1) {
    		$ID_Tipo_Documento = $this->db->query("SELECT ID_Tipo_Documento FROM documento_cabecera WHERE ID_Empresa=".$this->user->ID_Empresa." AND ID_Documento_Cabecera=".$ID." LIMIT 1")->row()->ID_Tipo_Documento;
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera=".$ID;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa=" . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen=" . $row->ID_Almacen . " AND ID_Producto=" . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa=" . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen=" . $row->ID_Almacen . " AND ID_Producto=" . $row->ID_Producto)->row()->Qt_Producto;
					
	        		if ($ID_Tipo_Documento != 5){//Nota de Crédito
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto + round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		} else {
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		}
				}
        	}
	        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
			$data = array(
				'Qt_Producto' => 0,
				'Ss_Precio' => 0,
				'Ss_SubTotal' => 0,
				'Ss_Costo_Promedio' => 0,
			);
	        $this->db->update('movimiento_inventario', $data);
    	}

		if ($this->db->query("SELECT count(*) AS existe FROM guia_enlace WHERE ID_Empresa=" . $this->user->ID_Empresa . " AND ID_Documento_Cabecera=" . $ID . " LIMIT 1")->row()->existe > 0){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete('guia_enlace');
		}

        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
		$data = array(
			'Nu_Estado' => 7,
			'Ss_Descuento' => 0.00,
			'Ss_Total' => 0.00,
			'Ss_Total_Saldo' => 0.00,
		);
        $this->db->update($this->table, $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al anular');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro anulado');
        }
	}
    
	public function eliminarVenta($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		$this->db->trans_begin();
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
		$this->db->delete($this->table_documento_detalle);
		
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
        $this->db->delete('documento_medio_pago');
        //Validar N/C y N/B
        if ($Nu_Enlace == 1){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete($this->table_documento_enlace);
        }
        
    	if ($Nu_Descargar_Inventario == 1) {
    		$ID_Tipo_Documento = $this->db->query("SELECT ID_Tipo_Documento FROM documento_cabecera WHERE ID_Empresa=".$this->user->ID_Empresa." AND ID_Documento_Cabecera=".$ID." LIMIT 1")->row()->ID_Tipo_Documento;
    		
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera=".$ID;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa=" . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen=" . $row->ID_Almacen . " AND ID_Producto=" . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa=" . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen=" . $row->ID_Almacen . " AND ID_Producto=" . $row->ID_Producto)->row()->Qt_Producto;
					
	        		if ($ID_Tipo_Documento != 5){//Nota de Crédito
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto + round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		} else {
						$stock_producto = array(
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
	        		}
				}
        	}
	        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete('movimiento_inventario');
    	}
        
        $arrCorrelativoPendiente = $this->db->query("SELECT ID_Organizacion, Fe_Emision, Nu_Correlativo FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Documento_Cabecera = " . $ID . " LIMIT 1")->result();
		
		$sql_limipiar_correlativo = "
DELETE FROM correlativo_tipo_asiento_pendiente
WHERE
ID_Empresa = " . $this->user->ID_Empresa . "
AND ID_Tipo_Asiento = 1
AND Fe_Year = '" . ToYear($arrCorrelativoPendiente[0]->Fe_Emision) . "'
AND Fe_Month = '" . ToMonth($arrCorrelativoPendiente[0]->Fe_Emision) . "'
AND Nu_Correlativo = " . $arrCorrelativoPendiente[0]->Nu_Correlativo;
		$this->db->query($sql_limipiar_correlativo);

        $sql_correlativo_pendiente_libro_sunat = "
INSERT INTO correlativo_tipo_asiento_pendiente (
 ID_Empresa,
 ID_Tipo_Asiento,
 Fe_Year,
 Fe_Month,
 Nu_Correlativo
) VALUES (
 " . $this->user->ID_Empresa . ",
 1,
 '" . ToYear($arrCorrelativoPendiente[0]->Fe_Emision) . "',
 '" . ToMonth($arrCorrelativoPendiente[0]->Fe_Emision) . "',
 " . $arrCorrelativoPendiente[0]->Nu_Correlativo . "
);
		";
		$this->db->query($sql_correlativo_pendiente_libro_sunat);

		if ($this->db->query("SELECT count(*) AS existe FROM guia_enlace WHERE ID_Empresa=" . $this->user->ID_Empresa . " AND ID_Documento_Cabecera=" . $ID . " LIMIT 1")->row()->existe > 0){
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
	        $this->db->delete('guia_enlace');
		}
  
        $this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Documento_Cabecera', $ID);
        $this->db->delete($this->table);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
        }
	}
	
	public function get_by_id_anulado($ID){
		$arrData = $this->db->query("
SELECT
 ALMA.Txt_FE_Ruta,
 ALMA.Txt_FE_Token,
 VC.ID_Tipo_Documento,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento
FROM
 documento_cabecera AS VC
 JOIN almacen AS ALMA ON(VC.ID_Almacen = ALMA.ID_Almacen)
WHERE
 VC.ID_Documento_Cabecera = " . $ID . "
LIMIT 1")->result();
		return $arrData;
	}
    
	public function changeStatusSunat($ID, $iTipoStatus, $arrParametrosNubefact){
		$this->db->trans_begin();		
		if ( $iTipoStatus == 8 || $iTipoStatus == 10 ) {
			if ( count($arrParametrosNubefact) > 0 ) {
				$data = array(
					'Nu_Estado' => $iTipoStatus,
					'Txt_Url_PDF' => $arrParametrosNubefact['url_pdf'],
					'Txt_Url_XML' => $arrParametrosNubefact['url_xml'],
					'Txt_Url_CDR' => $arrParametrosNubefact['url_cdr'],
					'Txt_Url_Comprobante' => $arrParametrosNubefact['url_nubefact'],
					'Txt_QR' => $arrParametrosNubefact['Txt_QR'],
					'Txt_Hash' => $arrParametrosNubefact['Txt_Hash'],
				);
			} else {
				$data = array(
					'Nu_Estado' => ($iTipoStatus == 8 ? 9 : 11),
				);
			}
		} else {
			$data = array(
				'Nu_Estado' => $iTipoStatus
			);
		}

		$this->db->update($this->table, $data, array('ID_Documento_Cabecera' => $ID));
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'No se envió a SUNAT', 'arrMessagePSE' => 'No se envió a SUNAT',);
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		}
	}

    public function obtenerEstadoPSE($ID_Documento_Cabecera){
        $query = "SELECT
ALMA.Txt_FE_Ruta,
ALMA.Txt_FE_Token,
VC.ID_Tipo_Documento,
VC.ID_Serie_Documento,
VC.ID_Numero_Documento,
VC.Nu_Estado
FROM
documento_cabecera AS VC
JOIN almacen AS ALMA ON(VC.ID_Almacen = ALMA.ID_Almacen)
WHERE VC.ID_Documento_Cabecera = " . $ID_Documento_Cabecera;
		if ( !$this->db->simple_query($query) ){
			$error = $this->db->error();
			return array(
				'status' => 'danger',
				'style_modal' => 'modal-danger',
				'sStatus' => 'danger',
				'message' => 'Problemas al generar formato documento electrónico',
				'sMessage' => 'Problemas al generar formato documento electrónico',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
				'sql' => $query,
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			return array(
				'status' => 'success',
				'style_modal' => 'modal-success',
				'sStatus' => 'success',
				'arrData' => $arrResponseSQL->result(),
			);
		}

		return array(
			'status' => 'warning',
			'style_modal' => 'modal-warning',
			'sStatus' => 'warning',
			'sMessage' => 'No hay registro',
			'message' => 'No hay registro',
		);
    }

	public function updDocumentoVenta($arrParams){
		$this->db->trans_begin();	
		$Txt_Respuesta_Sunat_FE = json_encode(array(
			'Proveedor' => 'PSE',
			'Enviada_SUNAT' => 'Si',
			'Aceptada_SUNAT' => ($arrParams['codigo_sunat'] == '0' || $arrParams['codigo_sunat'] == '1033' ? 'Si' : 'No'),
			'Codigo_SUNAT' => $arrParams['codigo_sunat'],
			'Mensaje_SUNAT' => utf8_decode($arrParams['message_sunat']),
			'Fecha_Registro' => dateNow('fecha_hora'),
			'Fecha_Envio' => dateNow('fecha_hora'),
		));
        $where = array('ID_Documento_Cabecera' => $arrParams['ID_Documento_Cabecera']);
        $data = array(
			'Nu_Estado' => $arrParams['Nu_Estado'],
			'Txt_Respuesta_Sunat_FE' => $Txt_Respuesta_Sunat_FE,
			'Txt_Url_CDR' => $arrParams['enlace_del_cdr'],
		);
		$this->db->update('documento_cabecera', $data, $where);
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error modificar');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        }
	}

	public function updDocumentoVentaNubefactXML($arrParams){
		$this->db->trans_begin();
        $where = array('ID_Documento_Cabecera' => $arrParams['ID_Documento_Cabecera']);
        $data = array(
			'Txt_Url_XML' => $arrParams['enlace_del_xml'],
		);
		$this->db->update('documento_cabecera', $data, $where);
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error modificar');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        }
	}

	public function updDocumentoVentaCDR($arrParams){
		$this->db->trans_begin();
        $where = array('ID_Documento_Cabecera' => $arrParams['ID_Documento_Cabecera']);
        $data = array(
			'Txt_Url_CDR' => $arrParams['enlace_del_cdr'],
			'Txt_Respuesta_Sunat_FE' => $arrParams['Txt_Respuesta_Sunat_FE'],
			'Nu_Estado' => $arrParams['Nu_Estado'],
		);
		$this->db->update('documento_cabecera', $data, $where);
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error modificar');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        }
	}

	public function getNoteOfCredit($id) {
		$this->db->select('ID_Serie_Documento, ID_Numero_Documento')
		->from($this->table)
		->where('ID_Empresa', $this->empresa->ID_Empresa)
		->where('ID_Organizacion', $this->empresa->ID_Organizacion)
    	->where('ID_Tipo_Asiento', 1)
		->where('ID_Documento_Cabecera', $id)
		->where('ID_Tipo_Documento', 5);
		$query = $this->db->get();
        return @$query->result()[0];
	}
	
	public function getDocumentoCabeceraOfNoteOfCredit($id) {
		$this->db->select('documento_cabecera.ID_Serie_Documento, documento_cabecera.ID_Numero_Documento, documento_cabecera.Fe_Emision, TDOCU.No_Tipo_Documento_Breve')
		->from($this->table_documento_enlace)
		->join($this->table, 'documento_enlace.ID_Documento_Cabecera_Enlace = documento_cabecera.ID_Documento_Cabecera')
		->join($this->table_entidad . ' AS PROVE', 'PROVE.ID_Entidad = documento_cabecera.ID_Entidad', 'join')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = PROVE.ID_Tipo_Documento_Identidad', 'join')
		->join($this->table_tipo_documento. ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = documento_cabecera.ID_Tipo_Documento')
		->where('documento_cabecera.ID_Empresa', $this->empresa->ID_Empresa)
		->where('documento_cabecera.ID_Organizacion', $this->empresa->ID_Organizacion)
		->where('documento_cabecera.ID_Tipo_Asiento', 1)
		->where('documento_enlace.ID_Documento_Cabecera', $id);
		$query = $this->db->get();
		return @$query->result()[0];
	}
	
}
