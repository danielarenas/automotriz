<?php
class OrdenVentaModel extends CI_Model{
	var $table          				= 'documento_cabecera';
	var $table_documento_detalle		= 'documento_detalle';
	var $table_documento_enlace			= 'documento_enlace';
	var $table_tipo_documento			= 'tipo_documento';
	var $table_impuesto_cruce_documento	= 'impuesto_cruce_documento';
	var $table_entidad					= 'entidad';
	var $table_tipo_documento_identidad	= 'tipo_documento_identidad';
	var $table_moneda					= 'moneda';
	var $table_organizacion				= 'organizacion';
	var $table_tabla_dato				= 'tabla_dato';
	var $table_relacion_tabla				= 'relacion_tabla';
	var $table_orden_ingreso				= 'orden_ingreso';
	var $table_serie_documento				= 'serie_documento';
	var $table_producto	= 'producto';
	
    var $column_order = array('');
    var $column_search = array('');
    var $order = array('');
    
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
    	$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
    	
        if(!empty($this->input->post('Filtro_NumeroDocumento'))){
        	$this->db->where('VC.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento'));
        }
        
        if($this->input->post('Filtro_Estado') != ''){
        	$this->db->where('VC.Nu_Estado', $this->input->post('Filtro_Estado'));
        }
        
        if(!empty($this->input->post('Filtro_Contacto'))){
        	$this->db->where('CONTAC.No_Entidad', $this->input->post('Filtro_Contacto'));
        }

		if(!empty($this->input->post('Filtro_Entidad'))){
        	$this->db->where('CLI.No_Entidad', $this->input->post('Filtro_Entidad'));
        }

		if(!empty($this->input->post('Filtro_TiposDocumento'))){
        	$this->db->where('VC.ID_Tipo_Documento', $this->input->post('Filtro_TiposDocumento'));
        }
        
		$this->db->select('VC.Nu_Pago_Contratista_Generado, ITEM.No_Placa_Vehiculo, ITEM.No_Marca_Vehiculo, ITEM.No_Modelo_Vehiculo, VC.ID_Documento_Cabecera, VC.ID_Documento_Cabecera, TDOCU.No_Tipo_Documento, VC.ID_Serie_Documento, VC.ID_Numero_Documento, VC.Fe_Emision, TDOCUIDEN.No_Tipo_Documento_Identidad_Breve, CLI.No_Entidad, CONTAC.No_Entidad AS No_Contacto, CONTAC.Txt_Email_Entidad AS Txt_Email_Contacto, MONE.No_Signo, VC.Ss_Total, VC.Nu_Estado, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, TDOCU.Nu_Enlace, VC.Nu_Descargar_Inventario, TMANTENIMIENTO.No_Descripcion AS No_Descripcion_Tipo_Mantenimiento')
		->from($this->table . ' AS VC')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
		->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = VC.ID_Entidad', 'join')
		->join($this->table_entidad . ' AS CONTAC', 'CONTAC.ID_Entidad = VC.ID_Contacto', 'left')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = CLI.ID_Tipo_Documento_Identidad', 'join')
		->join($this->table_moneda . ' AS MONE', 'MONE.ID_Moneda = VC.ID_Moneda', 'join')
		->join($this->table_producto . ' AS ITEM', 'ITEM.ID_Producto = VC.ID_Producto', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
		->join($this->table_tabla_dato . ' AS TMANTENIMIENTO', 'TMANTENIMIENTO.Nu_Valor = VC.Nu_Tipo_Mantenimiento AND TMANTENIMIENTO.No_Relacion = "Tipos_Mantenimiento"', 'join')
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
    	->where('VC.ID_Tipo_Asiento', 1)
    	->where_in('VC.ID_Tipo_Documento', array('22','23'));
    	//->where_in('VC.ID_Tipo_Documento', array('14','15','16','17','18','19'));//12/02/2021
					
		if(isset($_POST['order']))
        	$this->db->order_by( 'CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
        else if(isset($this->order))
        	$this->db->order_by( 'CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $query = "SELECT
VC.ID_Empresa,
VC.ID_Organizacion,
VC.ID_Almacen,
VC.ID_Documento_Cabecera,
VC.Nu_Estado,
VC.ID_Tipo_Documento,
VC.ID_Serie_Documento,
VC.ID_Numero_Documento,
CLI.ID_Tipo_Documento_Identidad,
CLI.ID_Entidad,
CLI.No_Entidad,
CLI.Nu_Documento_Identidad,
CLI.Txt_Direccion_Entidad,
CLI.Nu_Telefono_Entidad,
CLI.Nu_Celular_Entidad,
CLI.Txt_Email_Entidad,
TDOCU.Nu_Impuesto,
TDOCU.Nu_Enlace,
VC.Fe_Emision,
VC.Fe_Vencimiento,
VC.Fe_Periodo,
VC.ID_Moneda,
VC.ID_Medio_Pago,
VC.Nu_Descargar_Inventario,
VC.ID_Lista_Precio_Cabecera,
CONTAC.ID_Entidad AS ID_Contacto,
CONTAC.ID_Tipo_Documento_Identidad AS ID_Tipo_Documento_Identidad_Contacto,
CONTAC.Nu_Documento_Identidad AS Nu_Documento_Identidad_Contacto,
CONTAC.No_Entidad AS No_Contacto,
CONTAC.Txt_Email_Entidad AS Txt_Email_Contacto,
CONTAC.Nu_Celular_Entidad AS Nu_Celular_Contacto,
CONTAC.Nu_Telefono_Entidad AS Nu_Telefono_Contacto,
VD.ID_Producto,
PRO.Nu_Codigo_Barra,
PRO.No_Producto,
VD.Txt_Nota AS Txt_Nota_Detalle,
ROUND(VD.Ss_Precio, 2) AS Ss_Precio,
VD.Qt_Producto,
VD.ID_Impuesto_Cruce_Documento,
VD.Ss_SubTotal AS Ss_SubTotal_Producto,
VD.Ss_Impuesto AS Ss_Impuesto_Producto,
ROUND(VD.Ss_Descuento, 2) AS Ss_Descuento_Producto,
ROUND(VD.Ss_Descuento_Impuesto, 2) AS Ss_Descuento_Impuesto_Producto,
ROUND(VD.Po_Descuento, 2) AS Po_Descuento_Impuesto_Producto,
ROUND(VD.Ss_Total, 2) AS Ss_Total_Producto,
ICDOCU.Ss_Impuesto,
MP.Nu_Tipo AS Nu_Tipo_Forma_Pago,
IMP.Nu_Tipo_Impuesto,
IMP.No_Impuesto_Breve,
ICDOCU.Po_Impuesto,
VC.Txt_Garantia,
VC.Txt_Glosa,
ROUND(VC.Ss_Descuento, 2) AS Ss_Descuento,
ROUND(VC.Ss_Total, 2) AS Ss_Total,
MONE.No_Moneda,
MONE.No_Signo,
VC.Po_Descuento,
VC.ID_Mesero,
VC.ID_Comision,
MP.No_Medio_Pago,
VC.No_Formato_PDF,
TDOCUIDEN.No_Tipo_Documento_Identidad_Breve,
CAR.ID_Producto AS ID_Vehiculo,
CAR.No_Placa_Vehiculo,
CAR.No_Marca_Vehiculo,
CAR.No_Modelo_Vehiculo,
CAR.No_Color_Vehiculo,
VC.No_Kilometraje,
F.ID_Familia,
F.No_Familia,
VC.Nu_Tipo_Mantenimiento,
TDOCU.No_Tipo_Documento_Breve,
VC.Txt_Resumen_Servicio_Presupuesto
FROM
" . $this->table . " AS VC
LEFT JOIN " . $this->table_documento_detalle . " AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
JOIN " . $this->table_entidad . " AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
LEFT JOIN " . $this->table_entidad . " AS CONTAC ON(CONTAC.ID_Entidad = VC.ID_Contacto)
LEFT JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
LEFT JOIN " . $this->table_impuesto_cruce_documento . " AS ICDOCU ON(ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
LEFT JOIN impuesto AS IMP ON(IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
JOIN medio_pago AS MP ON(MP.ID_Medio_Pago = VC.ID_Medio_Pago)
JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
JOIN producto AS CAR ON(CAR.ID_Producto = VC.ID_Producto)
LEFT JOIN familia AS F ON(PRO.ID_Familia = F.ID_Familia)
WHERE
VC.ID_Tipo_Asiento = 1
AND VC.ID_Documento_Cabecera = " . $ID;
        return $this->db->query($query)->result();
    }
    
    public function agregarVenta($arrOrdenCabecera, $arrOrdenDetalle, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo){		
		$this->db->trans_begin();
		
		if (is_array($arrClienteNuevo)){
		    unset($arrOrdenCabecera['ID_Entidad']);
		    //Si no existe el cliente, lo crearemos
		    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
				$arrCliente = array(
	                'ID_Empresa'					=> $this->empresa->ID_Empresa,
	                'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 0,
	                'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
	                'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrCliente);
	    		$Last_ID_Entidad = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Entidad" => $Last_ID_Entidad));
		}
		
		if (is_array($arrContactoNuevo)){
		    unset($arrOrdenCabecera['ID_Contacto']);
		    //Si no existe el cliente, lo crearemos
		    if( $this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 8 AND ID_Tipo_Documento_Identidad = " . $arrContactoNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrContactoNuevo['Nu_Documento_Identidad'] . "' AND No_Entidad='".$arrContactoNuevo['No_Entidad']."' LIMIT 1")->row()->existe == 0){
				$arrContacto = array(
	                'ID_Empresa'					=> $this->empresa->ID_Empresa,
	                'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 8,//Contacto
	                'ID_Tipo_Documento_Identidad'	=> $arrContactoNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrContactoNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrContactoNuevo['No_Entidad'],
	                'Txt_Email_Entidad' 			=> $arrContactoNuevo['Txt_Email_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrContactoNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrContactoNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrContacto);
	    		$Last_ID_Contacto = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Contacto" => $Last_ID_Contacto));
		}
		
		$ID_Producto = $arrOrdenCabecera['ID_Producto'];
		if (is_array($arrVehiculoNuevo)){
		    unset($arrOrdenCabecera['ID_Producto']);			
			if($this->db->query("SELECT COUNT(*) AS existe FROM producto WHERE ID_Empresa=".$arrOrdenCabecera['ID_Empresa']." AND ID_Entidad='" . $ID_Entidad . "' AND No_Placa_Vehiculo='" . $arrVehiculoNuevo['No_Placa_Vehiculo'] . "' LIMIT 1")->row()->existe == 0){
		    	$arrVehiculoNuevo = array_merge($arrVehiculoNuevo, array("ID_Empresa" => $arrOrdenCabecera['ID_Empresa'], "ID_Entidad" => $ID_Entidad, "No_Producto" => $arrVehiculoNuevo['No_Placa_Vehiculo'], "Nu_Tipo_Producto" => 3, "ID_Tipo_Producto" => 2));
				$this->db->insert('producto', $arrVehiculoNuevo);
	    		$ID_Producto = $this->db->insert_id();
			} else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
			}
		}
		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Producto" => $ID_Producto));
		
		$ID_Origen_Tabla = $arrOrdenCabecera['ID_Origen_Tabla'];
		unset($arrOrdenCabecera['ID_Origen_Tabla']);
		$this->db->insert($this->table, $arrOrdenCabecera);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();
		
		if ( !empty($arrOrdenDetalle) ) {
			foreach ($arrOrdenDetalle as $row) {
				$documento_detalle[] = array(
					'ID_Empresa'					=> $this->user->ID_Empresa,
					'ID_Documento_Cabecera'			=> $Last_ID_Documento_Cabecera,
					'ID_Producto'					=> $this->security->xss_clean($row['ID_Producto']),
					'Txt_Nota'					=> $this->security->xss_clean($row['Txt_Nota']),
					'Qt_Producto'					=> $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio'						=> $this->security->xss_clean($row['Ss_Precio']),
					'Ss_SubTotal' 					=> $this->security->xss_clean($row['Ss_SubTotal']),
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['Ss_Descuento'],
					'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
					'Ss_Impuesto' 					=> $this->security->xss_clean($row['Ss_Impuesto']),
					'Ss_Total' 						=> round($this->security->xss_clean($row['Ss_Total']), 2),
				);
			}
			$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
		}
		
		if ( $ID_Origen_Tabla != 0 ) {
			$arrRelacionTabla = array(
				'ID_Empresa' => $arrOrdenCabecera['ID_Empresa'],
				'Nu_Relacion_Datos' => 1,//Relación Orden de ingreso a Presupuesto
				'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
				'ID_Origen_Tabla' => $ID_Origen_Tabla,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);
		}

		$sql = "UPDATE
serie_documento
SET
Nu_Numero_Documento = Nu_Numero_Documento + 1
WHERE
ID_Empresa=" . $this->user->ID_Empresa . "
AND ID_Organizacion=" . $arrOrdenCabecera['ID_Organizacion'] . "
AND ID_Almacen=" . $arrOrdenCabecera['ID_Almacen'] . "
AND ID_Tipo_Documento=" . $arrOrdenCabecera['ID_Tipo_Documento'] . "
AND ID_Serie_Documento_PK='" . $arrOrdenCabecera['ID_Serie_Documento_PK'] . "'";
		$this->db->query($sql);

        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
        } else {
            $this->db->trans_commit();
            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado', 'Last_ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera);
        }
    }
    
    public function actualizarVenta($where, $arrOrdenCabecera, $arrOrdenDetalle, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo){
		$this->db->trans_begin();
		
		$this->db->query("SET FOREIGN_KEY_CHECKS=OFF;");

		$arrDataModificar = $this->db->query("SELECT ID_Organizacion, ID_Documento_Cabecera, Nu_Descargar_Inventario FROM documento_cabecera WHERE ID_Empresa=" . $where['ID_Empresa'] . " AND ID_Documento_Cabecera=" . $where['ID_Documento_Cabecera'] . " LIMIT 1")->result();
	
		$ID_Documento_Cabecera = $arrDataModificar[0]->ID_Documento_Cabecera;
		$Nu_Descargar_Inventario = $arrDataModificar[0]->Nu_Descargar_Inventario;

		$this->db->delete($this->table_documento_detalle, $where);
		
    	if ($Nu_Descargar_Inventario == 1) {
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera = " . $ID_Documento_Cabecera;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where_stock_producto = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
					$stock_producto = array(
						'ID_Producto'		=> $row->ID_Producto,
						'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
						'Ss_Costo_Promedio'	=> 0.00,
					);
					$this->db->update('stock_producto', $stock_producto, $where_stock_producto);
				}
        	}
			$this->db->where('ID_Documento_Cabecera', $ID_Documento_Cabecera);
	        $this->db->delete('movimiento_inventario');
		}

        $this->db->delete($this->table, $where);
		
		if (is_array($arrClienteNuevo)){
		    unset($arrOrdenCabecera['ID_Entidad']);
		    //Si no existe el cliente, lo crearemos
		    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
				$arrCliente = array(
	                'ID_Empresa'					=> $this->user->ID_Empresa,
	                'ID_Organizacion'				=> $arrDataModificar[0]->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 0,
	                'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
	                'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrCliente);
	    		$Last_ID_Entidad = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Entidad" => $Last_ID_Entidad));
		}
		
		if (is_array($arrContactoNuevo)){
		    unset($arrOrdenCabecera['ID_Contacto']);
		    //Si no existe el cliente, lo crearemos
		    if( $this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 8 AND ID_Tipo_Documento_Identidad = " . $arrContactoNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrContactoNuevo['Nu_Documento_Identidad'] . "' AND No_Entidad='".$arrContactoNuevo['No_Entidad']."' LIMIT 1")->row()->existe == 0){
				$arrContacto = array(
	                'ID_Empresa'					=> $this->user->ID_Empresa,
	                'ID_Organizacion'				=> $arrDataModificar[0]->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 8,//Contacto
	                'ID_Tipo_Documento_Identidad'	=> $arrContactoNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrContactoNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrContactoNuevo['No_Entidad'],
	                'Txt_Email_Entidad' 			=> $arrContactoNuevo['Txt_Email_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrContactoNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrContactoNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrContacto);
	    		$Last_ID_Contacto = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Contacto" => $Last_ID_Contacto));
		}
		
		$ID_Origen_Tabla = $arrOrdenCabecera['ID_Origen_Tabla'];
		unset($arrOrdenCabecera['ID_Origen_Tabla']);
		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Documento_Cabecera" => $arrDataModificar[0]->ID_Documento_Cabecera));
		$this->db->insert($this->table, $arrOrdenCabecera);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();
		
		if ( $ID_Origen_Tabla != 0 ) {
			$where = array( 'Nu_Relacion_Datos' => 1, 'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera, 'ID_Origen_Tabla' => $ID_Origen_Tabla );
			$this->db->delete('relacion_tabla', $where);
			$arrRelacionTabla = array(
				'ID_Empresa' => $arrOrdenCabecera['ID_Empresa'],
				'Nu_Relacion_Datos' => 1,//Relación Orden de ingreso a Presupuesto
				'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
				'ID_Origen_Tabla' => $ID_Origen_Tabla,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);
		}
		
		if ( !empty($arrOrdenDetalle) ) {
			foreach ($arrOrdenDetalle as $row) {
				$documento_detalle[] = array(
					'ID_Empresa'					=> $this->user->ID_Empresa,
					'ID_Documento_Cabecera'			=> $Last_ID_Documento_Cabecera,
					'ID_Producto'					=> $this->security->xss_clean($row['ID_Producto']),
					'Txt_Nota'					=> $this->security->xss_clean($row['Txt_Nota']),
					'Qt_Producto'					=> $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio'						=> $this->security->xss_clean($row['Ss_Precio']),
					'Ss_SubTotal' 					=> $this->security->xss_clean($row['Ss_SubTotal']),
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['Ss_Descuento'],
					'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
					'Ss_Impuesto' 					=> $this->security->xss_clean($row['Ss_Impuesto']),
					'Ss_Total' 						=> round($this->security->xss_clean($row['Ss_Total']), 2),
				);
			}
			$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
		}
		
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
        } else {
			$this->db->query("SET FOREIGN_KEY_CHECKS=ON;");
            $this->db->trans_commit();
	        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado', 'Last_ID_Documento_Cabecera' => "$Last_ID_Documento_Cabecera");
        }
    }
    
	public function eliminarOrdenVenta($ID, $Nu_Descargar_Inventario){
		if($this->db->query("SELECT COUNT(*) AS existe FROM orden_seguimiento WHERE ID_Documento_Cabecera = " . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'La orden de venta tiene seguimientos');
		}else{
			$this->db->trans_begin();
			
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
			$this->db->delete($this->table_documento_detalle);
			
			if ($Nu_Descargar_Inventario == '1') {
				$query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera = " . $ID;
				$arrDetalle = $this->db->query($query)->result();
				foreach ($arrDetalle as $row) {
					if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
						$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
						$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
						$stock_producto = array(
							'ID_Empresa'		=> $row->ID_Empresa,
							'ID_Almacen'		=> $row->ID_Almacen,
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto + round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
					}
				}
				$this->db->where('ID_Empresa', $this->user->ID_Empresa);
				$this->db->where('ID_Documento_Cabecera', $ID);
				$this->db->delete('movimiento_inventario');
			}
			
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
			$this->db->delete($this->table);
			
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
			} else {
            $this->db->trans_commit();
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
			}
		}
	}
    
	public function estadoOrdenVenta($ID, $Nu_Descargar_Inventario, $Nu_Estado){
		$this->db->trans_begin();
        
        $where_orden_venta = array('ID_Documento_Cabecera' => $ID);
        $arrData = array( 'Nu_Estado' => $Nu_Estado );
		$this->db->update('documento_cabecera', $arrData, $where_orden_venta);
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al cambiar estado');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        }
	}
    
	public function duplicarOrdenVenta($ID){
		$this->db->trans_begin();
        
		$query_cabecera = "SELECT
ID_Empresa,
ID_Organizacion,
ID_Almacen,
ID_Entidad,
ID_Contacto,
ID_Tipo_Asiento,
ID_Tipo_Documento,
ID_Matricula_Empleado,
Fe_Emision,
ID_Medio_Pago,
ID_Rubro,
ID_Moneda,
Fe_Vencimiento,
Fe_Periodo,
Nu_Correlativo,
Nu_Descargar_Inventario,
ID_Lista_Precio_Cabecera,
Txt_Glosa,
Ss_Descuento,
Ss_Total,
Ss_Total_Saldo,
Ss_Percepcion,
Fe_Detraccion,
Nu_Detraccion,
Nu_Estado,
Txt_Garantia,
ID_Mesero,
ID_Comision,
No_Formato_PDF,
ID_Serie_Documento,
ID_Serie_Documento_PK,
ID_Numero_Documento,
Po_Descuento,
No_Kilometraje,
Nu_Tipo_Mantenimiento,
Txt_Resumen_Servicio_Presupuesto,
ID_Producto
FROM
documento_cabecera
WHERE
ID_Documento_Cabecera = " . $ID . " LIMIT 1";
		$arrCabecera = $this->db->query($query_cabecera)->result();
		
		foreach ($arrCabecera as $row) {
			$query = "SELECT
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM serie_documento WHERE ID_Serie_Documento_PK=" . $row->ID_Serie_Documento_PK . " AND Nu_Estado=1 LIMIT 1";
			$arrSerieDocumento = $this->db->query($query)->row();
			
			if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
				return array('sStatus' => 'danger', 'sMessage' => 'Deben configurar serie para, no existe');
			}

			if ($this->db->query("SELECT COUNT(*) AS existe FROM documento_cabecera WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Tipo_Asiento = 1 AND ID_Tipo_Documento = " . $row->ID_Tipo_Documento . " AND ID_Serie_Documento = '" . $arrSerieDocumento->ID_Serie_Documento . "' AND ID_Numero_Documento = '" . $arrSerieDocumento->Nu_Numero_Documento . "' LIMIT 1")->row()->existe > 0){
				return array('sStatus' => 'warning', 'sMessage' => 'Ya existe venta ' . $arrSerieDocumento->ID_Serie_Documento . ' - ' . $arrSerieDocumento->Nu_Numero_Documento . ' modificar correlativo en la opción Ventas -> Series' );
			}

			$documento_cabecera = array(
				'ID_Empresa' => $row->ID_Empresa,
				'ID_Organizacion' => $row->ID_Organizacion,
				'ID_Almacen' => $row->ID_Almacen,
				'ID_Entidad' => $row->ID_Entidad,
				'ID_Contacto' => $row->ID_Contacto,
				'ID_Tipo_Asiento' => $row->ID_Tipo_Asiento,
				'ID_Tipo_Documento' => $row->ID_Tipo_Documento,
				'ID_Matricula_Empleado' => $row->ID_Matricula_Empleado,
				'Fe_Emision' => $row->Fe_Emision,
				'ID_Medio_Pago' => $row->ID_Medio_Pago,
				'ID_Rubro' => $row->ID_Rubro,
				'ID_Moneda' => $row->ID_Moneda,
				'Fe_Vencimiento' => $row->Fe_Vencimiento,
				'Fe_Periodo' => $row->Fe_Periodo,
				'Nu_Correlativo' => $row->Nu_Correlativo,
				'Nu_Descargar_Inventario' => $row->Nu_Descargar_Inventario,
				'ID_Lista_Precio_Cabecera' => $row->ID_Lista_Precio_Cabecera,
				'Txt_Glosa' => $row->Txt_Glosa,
				'Ss_Descuento' => $row->Ss_Descuento,
				'Ss_Total' => $row->Ss_Total,
				'Ss_Total_Saldo' => $row->Ss_Total_Saldo,
				'Ss_Percepcion' => $row->Ss_Percepcion,
				'Fe_Detraccion' => $row->Fe_Detraccion,
				'Nu_Detraccion' => $row->Nu_Detraccion,
				'Nu_Estado' => 5,
				'Txt_Garantia' => $row->Txt_Garantia,
				'ID_Mesero' => $row->ID_Mesero,
				'ID_Comision' => $row->ID_Comision,
				'No_Formato_PDF' => $row->No_Formato_PDF,
				'ID_Serie_Documento' => $row->ID_Serie_Documento,
				'ID_Serie_Documento_PK' => $row->ID_Serie_Documento_PK,
				'ID_Numero_Documento' => $arrSerieDocumento->Nu_Numero_Documento,
				'Po_Descuento' => $row->Po_Descuento,
				'No_Kilometraje' => $row->No_Kilometraje,
				'Nu_Tipo_Mantenimiento' => $row->Nu_Tipo_Mantenimiento,
				'Txt_Resumen_Servicio_Presupuesto' => $row->Txt_Resumen_Servicio_Presupuesto,
				'ID_Producto' => $row->ID_Producto,
			);
			
			$sql = "UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1 WHERE ID_Serie_Documento_PK='" . $row->ID_Serie_Documento_PK . "'";
			$this->db->query($sql);
    	}
    	
		$this->db->insert($this->table, $documento_cabecera);
		$ID_Documento_Cabecera = $this->db->insert_id();

        $query_detalle = " SELECT
ID_Empresa,
ID_Producto,
Qt_Producto,
Ss_Precio,
Txt_Nota,
Ss_Descuento,
Ss_Descuento_Impuesto,
Po_Descuento,
Ss_SubTotal,
ID_Impuesto_Cruce_Documento,
Ss_Impuesto,
Ss_Total
FROM
documento_detalle
WHERE
ID_Documento_Cabecera = " . $ID;
		$arrDetalle = $this->db->query($query_detalle)->result();
		
		foreach ($arrDetalle as $row) {
			$documento_detalle[] = array(
				'ID_Empresa' => $row->ID_Empresa,
				'ID_Documento_Cabecera' => $ID_Documento_Cabecera,
				'ID_Producto' => $row->ID_Producto,
				'Qt_Producto' => $row->Qt_Producto,
				'Txt_Nota' => $row->Txt_Nota,
				'Ss_Precio' => $row->Ss_Precio,
				'Ss_Descuento' => $row->Ss_Descuento,
				'Ss_Descuento_Impuesto'	=> $row->Ss_Descuento_Impuesto,
				'Po_Descuento' => $row->Po_Descuento,
				'Ss_SubTotal' => $row->Ss_SubTotal,
				'ID_Impuesto_Cruce_Documento' => $row->ID_Impuesto_Cruce_Documento,
				'Ss_Impuesto' => $row->Ss_Impuesto,
				'Ss_Total' => $row->Ss_Total,
			);
    	}
		$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
 
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al duplicar orden de venta');
        } else {
            $this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro agregado');
        }
	}
    
	public function generarLiquidacion($arrPost){
		$this->db->trans_begin();
		
		$iIdFirst = 0;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			$iIdFirst = $key;
			break;
		}
$sql = "SELECT
ID_Empresa,
ID_Organizacion,
ID_Almacen,
ID_Entidad,
ID_Moneda,
ID_Medio_Pago,
Ss_Total
FROM
documento_cabecera
WHERE ID_Documento_Cabecera = " . $iIdFirst .  " LIMIT 1";
		$arrHeader = $this->db->query($sql)->result();
		$ID_Empresa = $arrHeader[0]->ID_Empresa;
		$ID_Organizacion = $arrHeader[0]->ID_Organizacion;
		$ID_Almacen = $arrHeader[0]->ID_Almacen;
		$ID_Entidad = $arrHeader[0]->ID_Entidad;
		$ID_Moneda = $arrHeader[0]->ID_Moneda;
		$ID_Medio_Pago = $arrHeader[0]->ID_Medio_Pago;
		$Ss_Total = $arrHeader[0]->Ss_Total;

		$query = "SELECT
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM
serie_documento
WHERE
ID_Empresa=" . $ID_Empresa . "
AND ID_Organizacion=" . $ID_Organizacion . "
AND ID_Almacen=" . $ID_Almacen . "
AND ID_Tipo_Documento=24
AND Nu_Estado=1
LIMIT 1";
		$arrSerieDocumento = $this->db->query($query)->row();
				
		if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Deben configurar serie para Liquidación de Venta, no existe');
		}

		$arrHeader = array(
			'ID_Empresa'				=> $ID_Empresa,
			'ID_Organizacion'			=> $ID_Organizacion,
			'ID_Almacen'			    => $ID_Almacen,
			'ID_Entidad'				=> $ID_Entidad,
			'ID_Tipo_Asiento'			=> 1,//Venta
			'ID_Tipo_Documento'			=> 24,//Liquidación de venta
			'ID_Serie_Documento_PK'		=> $arrSerieDocumento->ID_Serie_Documento_PK,
			'ID_Serie_Documento'		=> $arrSerieDocumento->ID_Serie_Documento,
			'ID_Numero_Documento'		=> $arrSerieDocumento->Nu_Numero_Documento,
			'Fe_Emision'				=> dateNow('fecha'),
			'Fe_Emision_Hora'			=> dateNow('fecha_hora'),
			'ID_Moneda'					=> $ID_Moneda,//Soles
			'ID_Medio_Pago'				=> $ID_Medio_Pago,
			'Fe_Vencimiento'			=> dateNow('fecha'),
			'Fe_Periodo' => dateNow('fecha'),
			'Nu_Descargar_Inventario' => 0,
			'Ss_Total' => $Ss_Total,
			'Ss_Total_Saldo' => 0.00,
			'Ss_Vuelto' => 0.00,
			'Nu_Correlativo' => 0,
			'Nu_Estado' => 20,//Pendiente
			'Nu_Transporte_Lavanderia_Hoy' => 0,
			'Nu_Estado_Lavado' => 0,
			'Fe_Entrega' => dateNow('fecha'),
			'Nu_Tipo_Recepcion' => 0,
			'ID_Transporte_Delivery' => 0,
			'Txt_Direccion_Delivery' => '-',
			'Txt_Glosa' => '',
			'No_Formato_PDF' => 'A4',
		);

		$this->db->insert('documento_cabecera', $arrHeader);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();

		$fTotalHeader = 0.00;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			$sql = "SELECT
VC.ID_Empresa,
VC.ID_Tipo_Documento,
PLACA.No_Marca_Vehiculo,
PLACA.No_Modelo_Vehiculo,
PLACA.No_Placa_Vehiculo,
TD.No_Tipo_Documento,
VC.ID_Serie_Documento,
VC.ID_Numero_Documento
FROM
documento_cabecera AS VC
JOIN producto AS PLACA ON(PLACA.ID_Producto = VC.ID_Producto)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = VC.ID_Tipo_Documento)
WHERE VC.ID_Documento_Cabecera = " . $key;
			$arrPresupuesto = $this->db->query($sql)->result();

			$ID_Empresa = $arrPresupuesto[0]->ID_Empresa;
			$ID_Tipo_Documento = $arrPresupuesto[0]->ID_Tipo_Documento;
			$No_Marca_Vehiculo = $arrPresupuesto[0]->No_Marca_Vehiculo;
			$No_Modelo_Vehiculo = $arrPresupuesto[0]->No_Modelo_Vehiculo;
			$No_Placa_Vehiculo = $arrPresupuesto[0]->No_Placa_Vehiculo;
			$No_Tipo_Documento = $arrPresupuesto[0]->No_Tipo_Documento;
			$ID_Serie_Documento = $arrPresupuesto[0]->ID_Serie_Documento;
			$ID_Numero_Documento = $arrPresupuesto[0]->ID_Numero_Documento;

			$sql = "SELECT SUM(Ss_SubTotal) AS Ss_SubTotal, SUM(Ss_Total) AS Ss_Total FROM documento_detalle WHERE ID_Documento_Cabecera = " . $key;
			$arrPresupuestoTotales = $this->db->query($sql)->result();
			$Ss_Total = $arrPresupuestoTotales[0]->Ss_Total;
			$Ss_SubTotal = $arrPresupuestoTotales[0]->Ss_SubTotal;

			$ID_Impuesto = $this->db->query("SELECT ID_Impuesto FROM impuesto WHERE ID_Empresa = " . $ID_Empresa . " AND No_Impuesto='Gravado - Operación Onerosa' LIMIT 1")->row()->ID_Impuesto;
			$ID_Unidad_Medida = $this->db->query("SELECT ID_Unidad_Medida FROM unidad_medida WHERE ID_Empresa = " . $ID_Empresa . " AND No_Unidad_Medida='Unidad (Servicios)' AND Nu_Estado = 1 LIMIT 1")->row()->ID_Unidad_Medida;

			$arrItem = $this->db->query("SELECT ID_Producto FROM producto WHERE ID_Empresa = " . $ID_Empresa . " AND Nu_Codigo_Barra = '" . $key . "' LIMIT 1")->result();

			if ( !empty($arrItem) ) {
				$ID_Item = $arrItem[0]->ID_Producto;
			} else {
				$sNombreItem = 'SERVICIO DE PLANCHADO Y PINTURA VEHICULOS TALLER';
				if ( $ID_Tipo_Documento == 22 )//22 = Presupuesto Mecánica
					$sNombreItem = 'SERVICIO DE MANTENIMIENTO PREVENTIVO MARCA: ' . $No_Marca_Vehiculo . ' MODELO: ' . $No_Modelo_Vehiculo . ' PLACA: ' . $No_Placa_Vehiculo . ' ' . $No_Tipo_Documento . ' ' . $ID_Serie_Documento . ' ' . $ID_Numero_Documento;

				$arrItem = array(
					'ID_Empresa'				=> $ID_Empresa,
					'Nu_Tipo_Producto'			=> 4,//Producto creados por Liquidacion de Prespuesto
					'ID_Tipo_Producto'			=> 1,
					'Nu_Codigo_Barra'			=> $key,//ID_Unico_BD presupuesto
					'ID_Producto_Sunat'			=> 0,
					'No_Producto' => $sNombreItem,
					'Ss_Precio'	=> $Ss_Total,
					'Ss_Costo' => 0,
					'No_Codigo_Interno'			=> '',
					'ID_Impuesto'				=> $ID_Impuesto,
					'Nu_Lote_Vencimiento'		=> '',
					'ID_Unidad_Medida'			=> $ID_Unidad_Medida,
					'ID_Impuesto_Icbper'		=> 0,
					'Nu_Compuesto'				=> 0,
					'Nu_Estado'					=> 0,
					'Txt_Ubicacion_Producto_Tienda'	=> '',
					'Txt_Producto' => '',
					'Nu_Stock_Minimo' => 0,
					'Qt_CO2_Producto' => '',
					'Nu_Receta_Medica' => '',
					'ID_Laboratorio' => 0,
					'ID_Tipo_Pedido_Lavado'	=> 0,
					'Txt_Composicion' => '',
					'No_Imagen_Item' => '',
					'Ss_Precio_Ecommerce_Online_Regular' => 0,
					'Ss_Precio_Ecommerce_Online' => 0,
					'ID_Familia_Marketplace' => 0,
					'ID_Sub_Familia_Marketplace' => 0,
					'ID_Marca_Marketplace' => 0,
				);
				$this->db->insert('producto', $arrItem);
				$ID_Item = $this->db->insert_id();
			}

			$ID_Impuesto_Cruce_Documento = $this->db->query("SELECT IMPDOC.ID_Impuesto_Cruce_Documento FROM impuesto AS IMP JOIN impuesto_cruce_documento AS IMPDOC ON (IMPDOC.ID_Impuesto = IMP.ID_Impuesto) WHERE IMP.ID_Impuesto = " . $ID_Impuesto . " AND IMPDOC.Nu_Estado = 1 LIMIT 1")->row()->ID_Impuesto_Cruce_Documento;

			$documento_detalle[] = array(
				'ID_Empresa' => $ID_Empresa,
				'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
				'ID_Producto' => $ID_Item,
				'Qt_Producto' => 1,
				'Ss_Precio' => $Ss_Total,
				'Ss_SubTotal' => $Ss_SubTotal,
				'Ss_Descuento' => 0,
				'Ss_Descuento_Impuesto' => 0,
				'Po_Descuento' => 0,
				'Txt_Nota' => '',
				'ID_Impuesto_Cruce_Documento' => $ID_Impuesto_Cruce_Documento,
				'Ss_Impuesto' => ($Ss_Total - $Ss_SubTotal),
				'Ss_Total' => $Ss_Total,
				'Nu_Estado_Lavado' => 0,
			);

			$arrRelacionTabla = array(
				'ID_Empresa' => $ID_Empresa,
				'Nu_Relacion_Datos' => 4,//Relación Liquidacion a Presupuesto
				'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
				'ID_Origen_Tabla' => $key,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);

			$fTotalHeader += $Ss_Total;
		}

		$this->db->insert_batch('documento_detalle', $documento_detalle);

		$sql = "UPDATE documento_cabecera SET Txt_Glosa = 'LIQ " . $Last_ID_Documento_Cabecera . "', Ss_Total = " . $fTotalHeader . " WHERE ID_Documento_Cabecera=" . $Last_ID_Documento_Cabecera;
		$this->db->query($sql);

		$sql = "UPDATE serie_documento SET Nu_Numero_Documento=Nu_Numero_Documento+1 WHERE ID_Serie_Documento_PK=" . $arrSerieDocumento->ID_Serie_Documento_PK;
		$this->db->query($sql);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Error al liquidar');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Liquidación generada correctamente');
		}
	}

	public function enlazarOI($arrPost){
		$this->db->trans_begin();
		
		$iIdPresupuesto = 0;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			$iIdPresupuesto = $key;

			if($this->db->query("SELECT COUNT(*) AS existe FROM relacion_tabla WHERE ID_Empresa = " . $this->empresa->ID_Empresa . " AND Nu_Relacion_Datos = 6 AND ID_Relacion_Enlace_Tabla = " . $arrPost['ID_Orden_Ingreso_Existe'] . " AND ID_Origen_Tabla = '" . $iIdPresupuesto . "' LIMIT 1")->row()->existe == 0){
				$arrRelacionTabla = array(
					'ID_Empresa' => $this->empresa->ID_Empresa,
					'Nu_Relacion_Datos' => 6,//Relación de Presupuesto a OI
					'ID_Relacion_Enlace_Tabla' => $arrPost['ID_Orden_Ingreso_Existe'],
					'ID_Origen_Tabla' => $iIdPresupuesto,
				);
				$this->db->insert('relacion_tabla', $arrRelacionTabla);
			} else {
				$this->db->trans_rollback();
				return array('sStatus' => 'warning', 'sMessage' => 'La OI Nro. ' . $arrPost['ID_Orden_Ingreso_Existe'] . ' ya se encuentra enlazada');
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Error al enlazar OI');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Enlazado correctamente con OI');
		}
	}

	public function pagoContratista($arrPost){
		$this->db->trans_begin();
		
		$iIdFirst = 0;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			$iIdFirst = $key;
			break;
		}
$sql = "SELECT
ID_Empresa,
ID_Organizacion,
ID_Almacen,
ID_Entidad,
ID_Moneda,
ID_Medio_Pago,
Ss_Total
FROM
documento_cabecera
WHERE ID_Documento_Cabecera = " . $iIdFirst .  " LIMIT 1";
		$arrHeader = $this->db->query($sql)->result();
		$ID_Empresa = $arrHeader[0]->ID_Empresa;
		$ID_Organizacion = $arrHeader[0]->ID_Organizacion;
		$ID_Almacen = $arrHeader[0]->ID_Almacen;
		$ID_Entidad = $arrHeader[0]->ID_Entidad;
		$ID_Moneda = $arrHeader[0]->ID_Moneda;
		$ID_Medio_Pago = $arrHeader[0]->ID_Medio_Pago;
		$Ss_Total = $arrHeader[0]->Ss_Total;

		$query = "SELECT
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM
serie_documento
WHERE
ID_Empresa=" . $ID_Empresa . "
AND ID_Organizacion=" . $ID_Organizacion . "
AND ID_Almacen=" . $ID_Almacen . "
AND ID_Tipo_Documento=25
AND Nu_Estado=1
LIMIT 1";
		$arrSerieDocumento = $this->db->query($query)->row();
				
		if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Deben configurar serie para Pago de Contratista, no existe');
		}

		$query = "SELECT ID_Entidad FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND No_Entidad LIKE '%clientes varios%' LIMIT 1"; //1 = ID_Entidad -> Cliente varios
		if ( !$this->db->simple_query($query) ){
			$this->db->trans_rollback();
			$error = $this->db->error();
			return array(
				'sStatus' => 'danger',
				'sMessage' => 'Problemas al obtener datos de clientes varios',
				'sClassModal' => 'modal-danger',
				'sCodeSQL' => $error['code'],
				'sMessageSQL' => $error['message'],
			);
		}
		$arrResponseSQL = $this->db->query($query);
		if ( $arrResponseSQL->num_rows() > 0 ){
			$arrData = $arrResponseSQL->result();
			$ID_Entidad = $arrData[0]->ID_Entidad;
		} else {
			$this->db->trans_rollback();
			return array(
				'sStatus' => 'warning',
				'sMessage' => 'No se encontro clientes varios',
				'sClassModal' => 'modal-warning',
			);
		}

		$arrHeader = array(
			'ID_Empresa'				=> $ID_Empresa,
			'ID_Organizacion'			=> $ID_Organizacion,
			'ID_Almacen'			    => $ID_Almacen,
			'ID_Entidad'				=> $ID_Entidad,
			'ID_Tipo_Asiento'			=> 1,//Venta
			'ID_Tipo_Documento'			=> 25,//Pago de contratista
			'ID_Serie_Documento_PK'		=> $arrSerieDocumento->ID_Serie_Documento_PK,
			'ID_Serie_Documento'		=> $arrSerieDocumento->ID_Serie_Documento,
			'ID_Numero_Documento'		=> $arrSerieDocumento->Nu_Numero_Documento,
			'Fe_Emision'				=> dateNow('fecha'),
			'Fe_Emision_Hora'			=> dateNow('fecha_hora'),
			'ID_Moneda'					=> $ID_Moneda,//Soles
			'ID_Medio_Pago'				=> $ID_Medio_Pago,
			'Fe_Vencimiento'			=> dateNow('fecha'),
			'Fe_Periodo' => dateNow('fecha'),
			'Nu_Descargar_Inventario' => 0,
			'Ss_Total' => $Ss_Total,
			'Ss_Total_Saldo' => $Ss_Total,
			'Ss_Vuelto' => 0.00,
			'Nu_Correlativo' => 0,
			'Nu_Estado' => 24,//Pendiente
			'Nu_Transporte_Lavanderia_Hoy' => 0,
			'Nu_Estado_Lavado' => 0,
			'Fe_Entrega' => dateNow('fecha'),
			'Nu_Tipo_Recepcion' => 0,
			'ID_Transporte_Delivery' => 0,
			'Txt_Direccion_Delivery' => '-',
			'Txt_Glosa' => '',
			'No_Formato_PDF' => 'A4',
		);

		$this->db->insert('documento_cabecera', $arrHeader);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();

		$iIdsPresupuesto = '';
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			//Convertir presupuesto a estado pago generado a contratista
			$sql = "UPDATE documento_cabecera SET Nu_Pago_Contratista_Generado = 1 WHERE ID_Documento_Cabecera=" . $key;
			$this->db->query($sql);

			$iIdsPresupuesto .= $key . ',';

			$arrRelacionTabla = array(
				'ID_Empresa' => $ID_Empresa,
				'Nu_Relacion_Datos' => 7,//Relación Presupuesto a Pago de Contratista
				'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
				'ID_Origen_Tabla' => $key,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);
		}
		$iIdsPresupuesto = substr($iIdsPresupuesto, 0, -1);

		$fTotalHeader = 0.00;
		$sql = "SELECT
VC.ID_Documento_Cabecera,
VC.ID_Empresa,
ITEM.ID_Familia,
F.No_Familia,
SUM(VD.Qt_Producto) AS Qt_Producto,
SUM(VD.Ss_Total) AS Ss_Total_Familia,
SUM(VD.Ss_SubTotal) AS Ss_SubTotal_Familia
FROM
documento_cabecera AS VC
JOIN documento_detalle AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
JOIN producto AS ITEM ON(ITEM.ID_Producto = VD.ID_Producto)
JOIN familia AS F ON(F.ID_Familia = ITEM.ID_Familia)
WHERE
VC.ID_Documento_Cabecera IN(" . $iIdsPresupuesto . ")
GROUP BY
VC.ID_Empresa,
ITEM.ID_Familia,
F.No_Familia;";
		$arrPresupuesto = $this->db->query($sql)->result();

		foreach ($arrPresupuesto as $row){
			$ID_Documento_Cabecera = $row->ID_Documento_Cabecera;
			$No_Familia = $row->No_Familia;
			if ( $No_Familia == 'FIBRA' || $No_Familia == 'PINTURA' || $No_Familia == 'PLANCHADO' ) {
				$ID_Empresa = $row->ID_Empresa;
				$ID_Familia = $row->ID_Familia;
				$Qt_Producto = $row->Qt_Producto;
				$Ss_Total_Familia = $row->Ss_Total_Familia;
				$Ss_SubTotal_Familia = $row->Ss_SubTotal_Familia;

				$ID_Impuesto = $this->db->query("SELECT ID_Impuesto FROM impuesto WHERE ID_Empresa = " . $ID_Empresa . " AND No_Impuesto='Gravado - Operación Onerosa' LIMIT 1")->row()->ID_Impuesto;
				$ID_Unidad_Medida = $this->db->query("SELECT ID_Unidad_Medida FROM unidad_medida WHERE ID_Empresa = " . $ID_Empresa . " AND No_Unidad_Medida='Unidad (Bienes)' AND Nu_Estado = 1 LIMIT 1")->row()->ID_Unidad_Medida;

				$sCodigoBarra=$ID_Familia . $arrSerieDocumento->Nu_Numero_Documento;
				$arrItem = $this->db->query("SELECT ID_Producto FROM producto WHERE ID_Empresa = " . $ID_Empresa . " AND Nu_Codigo_Barra = '" . $sCodigoBarra . "' LIMIT 1")->result();

				if ( !empty($arrItem) ) {
					$ID_Item = $arrItem[0]->ID_Producto;
				} else {
					$sNombreItem = 'PAGO CONTRATISTA PRESUPUESTO ID ' . $ID_Documento_Cabecera .  ' - ' . $No_Familia;
					
					$arrItem = array(
						'ID_Empresa'				=> $ID_Empresa,
						'Nu_Tipo_Producto'			=> 4,//Producto creados por Pago de Contratista
						'ID_Tipo_Producto'			=> 1,
						'Nu_Codigo_Barra'			=> $sCodigoBarra,//ID_Unico_BD presupuesto
						'ID_Producto_Sunat'			=> 0,
						'No_Producto' => $sNombreItem,
						'Ss_Precio'	=> $Ss_Total_Familia,
						'Ss_Costo' => 0,
						'No_Codigo_Interno'			=> '',
						'ID_Impuesto'				=> $ID_Impuesto,
						'Nu_Lote_Vencimiento'		=> '',
						'ID_Unidad_Medida'			=> $ID_Unidad_Medida,
						'ID_Familia' => $ID_Familia,
						'ID_Impuesto_Icbper'		=> 0,
						'Nu_Compuesto'				=> 0,
						'Nu_Estado'					=> 0,
						'Txt_Ubicacion_Producto_Tienda'	=> '',
						'Txt_Producto' => '',
						'Nu_Stock_Minimo' => 0,
						'Qt_CO2_Producto' => '',
						'Nu_Receta_Medica' => '',
						'ID_Laboratorio' => 0,
						'ID_Tipo_Pedido_Lavado'	=> 0,
						'Txt_Composicion' => '',
						'No_Imagen_Item' => '',
						'Ss_Precio_Ecommerce_Online_Regular' => 0,
						'Ss_Precio_Ecommerce_Online' => 0,
						'ID_Familia_Marketplace' => 0,
						'ID_Sub_Familia_Marketplace' => 0,
						'ID_Marca_Marketplace' => 0,
					);
					$this->db->insert('producto', $arrItem);
					$ID_Item = $this->db->insert_id();
				}
				

				$ID_Impuesto_Cruce_Documento = $this->db->query("SELECT IMPDOC.ID_Impuesto_Cruce_Documento FROM impuesto AS IMP JOIN impuesto_cruce_documento AS IMPDOC ON (IMPDOC.ID_Impuesto = IMP.ID_Impuesto) WHERE IMP.ID_Impuesto = " . $ID_Impuesto . " AND IMPDOC.Nu_Estado = 1 LIMIT 1")->row()->ID_Impuesto_Cruce_Documento;

				$documento_detalle[] = array(
					'ID_Empresa' => $ID_Empresa,
					'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
					'ID_Producto' => $ID_Item,
					'Qt_Producto' => $Qt_Producto,
					'Ss_Precio' => round(($Ss_Total_Familia / $Qt_Producto), 2),
					'Ss_SubTotal' => $Ss_SubTotal_Familia,
					'Ss_Descuento' => 0,
					'Ss_Descuento_Impuesto' => 0,
					'Po_Descuento' => 0,
					'Txt_Nota' => '',
					'ID_Impuesto_Cruce_Documento' => $ID_Impuesto_Cruce_Documento,
					'Ss_Impuesto' => ($Ss_Total_Familia - $Ss_SubTotal_Familia),
					'Ss_Total' => $Ss_Total_Familia,
					'Nu_Estado_Lavado' => 0,
				);

				$fTotalHeader += $Ss_Total_Familia;
			} // IF
		}// Foreach

		$this->db->insert_batch('documento_detalle', $documento_detalle);

		$sql = "UPDATE documento_cabecera SET Txt_Glosa = 'ID Presupuesto " . $iIdsPresupuesto . "', Ss_Total = " . $fTotalHeader . ", Ss_Total_Saldo = " . $fTotalHeader . " WHERE ID_Documento_Cabecera=" . $Last_ID_Documento_Cabecera;
		$this->db->query($sql);

		$sql = "UPDATE serie_documento SET Nu_Numero_Documento=Nu_Numero_Documento+1 WHERE ID_Serie_Documento_PK=" . $arrSerieDocumento->ID_Serie_Documento_PK;
		$this->db->query($sql);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Error al generar Pago de Contratista');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Pago de Contratista generada correctamente');
		}
	}
}
