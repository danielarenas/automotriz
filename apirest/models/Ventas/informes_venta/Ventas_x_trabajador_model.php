<?php
class Ventas_x_trabajador_model extends CI_Model{

    public function __construct(){
        parent::__construct();
	   }
	
    public function getReporte($arrParams){
      $Fe_Inicio = $arrParams['Fe_Inicio'];
      $Fe_Fin = $arrParams['Fe_Fin'];
      $ID_Tipo_Documento = $arrParams['ID_Tipo_Documento'];
      $ID_Serie_Documento = $arrParams['ID_Serie_Documento'];
      $ID_Numero_Documento = $arrParams['ID_Numero_Documento'];
      $Nu_Estado_Documento = $arrParams['Nu_Estado_Documento'];
      $iIdEmpleado=$arrParams['iIdEmpleado'];
      $sNombreEmpleado=$arrParams['sNombreEmpleado'];
      $iIdItem=$arrParams['iIdItem'];
      $sNombreItem=$arrParams['sNombreItem'];

        $cond_tipo = $ID_Tipo_Documento != "0" ? 'AND VC.ID_Tipo_Documento = ' . $ID_Tipo_Documento : 'AND VC.ID_Tipo_Documento IN(2,3,4,5,6)';
        $cond_serie = $ID_Serie_Documento != "0" ? "AND VC.ID_Serie_Documento = '" . $ID_Serie_Documento . "'" : "";
        $cond_numero = $ID_Numero_Documento != "-" ? "AND VC.ID_Numero_Documento = '" . $ID_Numero_Documento . "'" : "";
        $cond_estado_documento = $Nu_Estado_Documento != "0" ? 'AND VC.Nu_Estado = ' . $Nu_Estado_Documento : "";
        $cond_empleado = ( $iIdEmpleado != '-' && $sNombreEmpleado != '-' ) ? 'AND EMPLE.ID_Entidad = ' . $iIdEmpleado : "";
        $cond_item = ( $iIdItem != '-' && $sNombreItem != '-' ) ? 'AND VD.ID_Producto = ' . $iIdItem : "";
        
$query = "
SELECT
 EMPLE.ID_Entidad,
 EMPLE.Nu_Documento_Identidad,
 EMPLE.No_Entidad,
 TD.No_Tipo_Documento_Breve,
 VC.ID_Tipo_Documento,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento,
 VC.Fe_Emision,
 MONE.ID_Moneda,
 MONE.No_Signo,
 MONE.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Moneda,
 TC.Ss_Compra_Oficial AS Ss_Tipo_Cambio,
 VE.Ss_Tipo_Cambio_Modificar,
 PROD.Nu_Codigo_Barra,
 PROD.No_Producto,
 VD.Qt_Producto,
 VD.Ss_Precio,
 VD.Ss_SubTotal,
 VD.Ss_Descuento,
 VD.Ss_Impuesto,
 VD.Ss_Total,
 VC.Ss_Descuento AS Ss_Descuento_Header,
 VC.Po_Descuento AS Po_Descuento_Header,
 VD.Ss_Descuento AS Ss_Descuento,
 IVDOCU.Ss_Impuesto,
 TDESTADO.No_Descripcion AS No_Estado,
 TDESTADO.No_Class AS No_Class_Estado
FROM
 documento_cabecera AS VC
 LEFT JOIN documento_detalle AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 LEFT JOIN impuesto_cruce_documento AS IVDOCU ON(IVDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
 JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = VC.ID_Tipo_Documento)
 LEFT JOIN matricula_empleado AS MEMPLE ON(VC.ID_Matricula_Empleado = MEMPLE.ID_Matricula_Empleado)
 LEFT JOIN entidad AS EMPLE ON(EMPLE.ID_Entidad = VC.ID_Mesero OR MEMPLE.ID_Entidad = EMPLE.ID_Entidad)
 LEFT JOIN producto AS PROD ON(VD.ID_Producto = PROD.ID_Producto)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
 JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
 LEFT JOIN tasa_cambio AS TC ON(VC.ID_Empresa = TC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND VC.Fe_Emision = TC.Fe_Ingreso)
 LEFT JOIN (
  SELECT
   VE.ID_Documento_Cabecera,
   TC.Ss_Venta_Oficial AS Ss_Tipo_Cambio_Modificar
  FROM
   documento_cabecera AS VC
   JOIN documento_enlace AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera_Enlace)
   JOIN tasa_cambio AS TC ON(TC.ID_Empresa = VC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND TC.Fe_Ingreso = VC.Fe_Emision)
 ) AS VE ON (VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera)
WHERE
 VC.ID_Empresa = " . $this->user->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
 AND VC.ID_Tipo_Asiento = 1
 AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
 " . $cond_tipo . "
 " . $cond_serie . "
 " . $cond_numero . "
 " . $cond_estado_documento . "
 " . $cond_empleado . "
 " . $cond_item . "
ORDER BY
 EMPLE.ID_Entidad,
 VC.Fe_Emision,
 VC.ID_Tipo_Documento,
 VC.ID_Serie_Documento,
 CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC;";
        if ( !$this->db->simple_query($query) ){
          $error = $this->db->error();
          return array(
            'sStatus' => 'danger',
            'sMessage' => 'Problemas al obtener datos',
            'sCodeSQL' => $error['code'],
            'sMessageSQL' => $error['message'],
          );
        }
        $arrResponseSQL = $this->db->query($query);
        if ( $arrResponseSQL->num_rows() > 0 ){
          return array(
            'sStatus' => 'success',
            'arrData' => $arrResponseSQL->result(),
          );
        }
        
        return array(
          'sStatus' => 'warning',
          'sMessage' => 'No se encontro registro',
        );
    }
}
