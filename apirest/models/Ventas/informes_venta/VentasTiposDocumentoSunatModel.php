<?php
class VentasTiposDocumentoSunatModel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}
	
    public function getReporte($ID_Empresa, $Fe_Inicio, $Fe_Fin, $iDocumentStatus){
        $cond_document_status = ($iDocumentStatus == 0 ? "" : "AND VC.Nu_Estado=".$iDocumentStatus);
        $query = "
SELECT
 VC.Fe_Emision,
 BOL.Nu_Cantidad_Trans_BOL,
 BOL.Ss_Total_BOL,
 FACT.Nu_Cantidad_Trans_FACT,
 FACT.Ss_Total_FACT,
 NC.Nu_Cantidad_Trans_NC,
 NC.Ss_Total_NC,
 ND.Nu_Cantidad_Trans_ND,
 ND.Ss_Total_ND
FROM
 documento_cabecera AS VC
 LEFT JOIN (
 SELECT
  VC.Fe_Emision,
  COUNT(*) AS Nu_Cantidad_Trans_BOL,
  SUM(VC.Ss_Total) AS Ss_Total_BOL
 FROM
  documento_cabecera AS VC
 WHERE
  VC.ID_Empresa = " . $this->user->ID_Empresa . "
  AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
  AND VC.ID_Tipo_Asiento = 1
  AND VC.ID_Tipo_Documento = 4
  AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
  " . $cond_document_status . "
 GROUP BY
  VC.Fe_Emision
 ) AS BOL ON (BOL.Fe_Emision = VC.Fe_Emision)
 LEFT JOIN (
 SELECT
  VC.Fe_Emision,
  COUNT(*) AS Nu_Cantidad_Trans_FACT,
  SUM(VC.Ss_Total) AS Ss_Total_FACT
 FROM
  documento_cabecera AS VC
 WHERE
  VC.ID_Empresa = " . $this->user->ID_Empresa . "
  AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
  AND VC.ID_Tipo_Asiento = 1
  AND VC.ID_Tipo_Documento = 3
  AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
  " . $cond_document_status . "
 GROUP BY
  VC.Fe_Emision
 ) AS FACT ON (FACT.Fe_Emision = VC.Fe_Emision)
 LEFT JOIN (
 SELECT
  VC.Fe_Emision,
  COUNT(*) AS Nu_Cantidad_Trans_NC,
  SUM(VC.Ss_Total) AS Ss_Total_NC
 FROM
  documento_cabecera AS VC
 WHERE
  VC.ID_Empresa = " . $this->user->ID_Empresa . "
  AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
  AND VC.ID_Tipo_Asiento = 1
  AND VC.ID_Tipo_Documento = 5
  AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
  " . $cond_document_status . "
 GROUP BY
  VC.Fe_Emision
 ) AS NC ON (NC.Fe_Emision = VC.Fe_Emision)
 LEFT JOIN (
 SELECT
  VC.Fe_Emision,
  COUNT(*) AS Nu_Cantidad_Trans_ND,
  SUM(VC.Ss_Total) AS Ss_Total_ND
 FROM
  documento_cabecera AS VC
 WHERE
  VC.ID_Empresa = " . $this->user->ID_Empresa . "
  AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
  AND VC.ID_Tipo_Asiento = 1
  AND VC.ID_Tipo_Documento = 6
  AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
  " . $cond_document_status . "
 GROUP BY
  VC.Fe_Emision
 ) AS ND ON (ND.Fe_Emision = VC.Fe_Emision)
WHERE
 VC.ID_Empresa = " . $this->user->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->user->ID_Organizacion . "
 AND VC.ID_Tipo_Asiento = 1
 AND VC.ID_Tipo_Documento IN(3,4,5,6)
 AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
 " . $cond_document_status . "
GROUP BY
 VC.Fe_Emision
ORDER BY
VC.Fe_Emision DESC;";
        return $this->db->query($query)->result();
    }
}
