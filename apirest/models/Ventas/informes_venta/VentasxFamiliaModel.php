<?php
class VentasxFamiliaModel extends CI_Model{
	  public function __construct(){
		  parent::__construct();
	  }
	
    public function getReporte($arrParams){
        $Fe_Inicio=$arrParams['Fe_Inicio'];
        $Fe_Fin=$arrParams['Fe_Fin'];
        $iIdFamilia=$arrParams['iIdFamilia'];
        $cond_familia = $iIdFamilia != "0" ? 'AND ITEM.ID_Familia = ' . $iIdFamilia : "";

        $query = "
SELECT
 FAMI.ID_Familia,
 FAMI.No_Familia,
 TD.No_Tipo_Documento_Breve,
 VC.ID_Documento_Cabecera,
 VC.ID_Tipo_Documento,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento,
 VC.Fe_Emision_Hora,
 MONE.ID_Moneda,
 MONE.No_Signo,
 MONE.Nu_Sunat_Codigo AS Nu_Sunat_Codigo_Moneda,
 CLI.No_Entidad,
 TC.Ss_Compra_Oficial AS Ss_Tipo_Cambio,
 VE.Ss_Tipo_Cambio_Modificar,
 UM.No_Unidad_Medida,
 ITEM.Nu_Codigo_Barra,
 ITEM.No_Producto,
 VD.Qt_Producto,
 VD.Ss_Precio,
 VD.Ss_Total,
 TDESTADO.No_Descripcion AS No_Estado,
 TDESTADO.No_Class AS No_Class_Estado,
 VC.Nu_Estado
FROM
 documento_cabecera AS VC
 JOIN documento_detalle AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 JOIN producto AS ITEM ON(VD.ID_Producto = ITEM.ID_Producto)
 JOIN unidad_medida AS UM ON(UM.ID_Unidad_Medida = ITEM.ID_Unidad_Medida)
 JOIN familia AS FAMI ON(ITEM.ID_Familia = FAMI.ID_Familia)
 JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = VC.ID_Tipo_Documento)
 JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
 JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
 LEFT JOIN tasa_cambio AS TC ON(VC.ID_Empresa = TC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND VC.Fe_Emision = TC.Fe_Ingreso)
 LEFT JOIN (
  SELECT
   VE.ID_Documento_Cabecera,
   TC.Ss_Venta_Oficial AS Ss_Tipo_Cambio_Modificar
  FROM
   documento_cabecera AS VC
   JOIN documento_enlace AS VE ON(VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera_Enlace)
   JOIN tasa_cambio AS TC ON(TC.ID_Empresa = VC.ID_Empresa AND TC.ID_Moneda = VC.ID_Moneda AND TC.Fe_Ingreso = VC.Fe_Emision)
 ) AS VE ON (VC.ID_Documento_Cabecera = VE.ID_Documento_Cabecera)
WHERE
 VC.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND VC.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND VC.ID_Tipo_Asiento = 1
 AND VC.ID_Tipo_Documento IN(2,3,4,5,6)
 AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
 " . $cond_familia . "
ORDER BY
 FAMI.No_Familia,
 FAMI.ID_Familia,
 VC.Fe_Emision_Hora DESC,
 VC.ID_Tipo_Documento DESC,
 VC.ID_Serie_Documento DESC,
 CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC;";
        
        if ( !$this->db->simple_query($query) ){
            $error = $this->db->error();
            return array(
                'sStatus' => 'danger',
                'sMessage' => 'Problemas al obtener datos',
                'sCodeSQL' => $error['code'],
                'sMessageSQL' => $error['message'],
            );
        }
        $arrResponseSQL = $this->db->query($query);
        if ( $arrResponseSQL->num_rows() > 0 ){
            return array(
                'sStatus' => 'success',
                'arrData' => $arrResponseSQL->result(),
            );
        }
        
        return array(
            'sStatus' => 'warning',
            'sMessage' => 'No se encontro registro',
        );
    }
}
