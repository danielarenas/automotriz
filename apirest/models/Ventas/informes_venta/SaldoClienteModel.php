<?php
class SaldoClienteModel extends CI_Model{
	  public function __construct(){
		  parent::__construct();
	  }
	
    public function getReporte($arrParams){
        $Fe_Inicio=$arrParams['Fe_Inicio'];
        $Fe_Fin=$arrParams['Fe_Fin'];
        $iIdTipoDocumento=$arrParams['iIdTipoDocumento'];
        $iIdSerieDocumento=$arrParams['iIdSerieDocumento'];
        $iNumeroDocumento=$arrParams['iNumeroDocumento'];
        $iEstadoPago=$arrParams['iEstadoPago'];
        $iIdCliente=$arrParams['iIdCliente'];
        $sNombreCliente=$arrParams['sNombreCliente'];

        $cond_tipo = $iIdTipoDocumento != "0" ? 'AND VC.ID_Tipo_Documento = ' . $iIdTipoDocumento : 'AND VC.ID_Tipo_Documento IN(2,3,4,5,6)';
        $cond_serie = $iIdSerieDocumento != "0" ? "AND VC.ID_Serie_Documento = '" . $iIdSerieDocumento . "'" : "";
        $cond_numero = $iNumeroDocumento != "-" ? "AND VC.ID_Numero_Documento = '" . $iNumeroDocumento . "'" : "";

        $cond_estado_pago = '';
        if ( $iEstadoPago == "1" )// Pendiente
            $cond_estado_pago = 'AND VC.Ss_Total_Saldo > 0.00';
        else if ( $iEstadoPago == "2" )// Cancelado
            $cond_estado_pago = 'AND VC.Ss_Total_Saldo = 0.00';

        $cond_cliente = ( $iIdCliente != '-' && $sNombreCliente != '-' ) ? 'AND CLI.ID_Entidad = ' . $iIdCliente : "";

        $query = "SELECT
VC.ID_Documento_Cabecera,
VC.Fe_Emision,
VC.Fe_Vencimiento,
VC.ID_Tipo_Documento,
TD.No_Tipo_Documento_Breve,
VC.ID_Serie_Documento,
VC.ID_Numero_Documento,
CLI.No_Entidad,
MONE.No_Signo,
VC.Ss_Total,
VC.Ss_Total_Saldo,
TDESTADO.No_Descripcion AS No_Estado,
TDESTADO.No_Class AS No_Class_Estado,
VC.Nu_Detraccion,
VC.Nu_Estado_Detraccion,
VC.Ss_Saldo_Detraccion,
VC.Nu_Retencion,
VC.Ss_Retencion,
VC.Nu_Voucher_Retencion,
VC.Fe_Voucher_Retencion,
VC.Fe_Entrega_Factura,
VC.Fe_Entrega_Vencimiento_Factura
FROM
documento_cabecera AS VC
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = VC.ID_Serie_Documento_PK)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = VC.ID_Tipo_Documento)
JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
WHERE
VC.ID_Empresa = " . $this->empresa->ID_Empresa . "
AND VC.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
AND VC.ID_Tipo_Asiento = 1
AND VC.Fe_Emision BETWEEN '" . $Fe_Inicio . "' AND '" . $Fe_Fin . "'
" . $cond_tipo . "
" . $cond_serie . "
" . $cond_numero . "
" . $cond_estado_pago . "
" . $cond_cliente . "
ORDER BY
VC.Fe_Emision_Hora DESC,
VC.ID_Tipo_Documento DESC,
VC.ID_Serie_Documento DESC,
CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC;";
        
        if ( !$this->db->simple_query($query) ){
            $error = $this->db->error();
            return array(
                'sStatus' => 'danger',
                'sMessage' => 'Problemas al obtener datos',
                'sCodeSQL' => $error['code'],
                'sMessageSQL' => $error['message'],
            );
        }
        $arrResponseSQL = $this->db->query($query);
        if ( $arrResponseSQL->num_rows() > 0 ){
            return array(
                'sStatus' => 'success',
                'arrData' => $arrResponseSQL->result(),
            );
        }
        
        return array(
            'sStatus' => 'warning',
            'sMessage' => 'No se encontro registro',
            'sql' => $query,
        );
    }

	public function pagarDetraccion($arrPost){
        $this->db->trans_begin();
    
        $where = array( 'ID_Documento_Cabecera' => $arrPost['ID_Documento_Cabecera'] );
        $data = array( 'Nu_Estado_Detraccion' => 1, 'Ss_Saldo_Detraccion' => 0.00, 'Fe_Detraccion' => $arrPost['Fe_Detraccion'], 'Nu_Detraccion' => $arrPost['Nu_Detraccion']);
        $this->db->update('documento_cabecera', $data, $where);
        
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Error al pagar detracción');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Detracción completada satisfactoriamente');
        }
	}

	public function pagarRetencion($arrPost){
        $this->db->trans_begin();
    
        $where = array( 'ID_Documento_Cabecera' => $arrPost['ID_Documento_Cabecera-Retencion'] );
        $data = array( 'Ss_Retencion' => 0.00, 'Fe_Voucher_Retencion' => $arrPost['Fe_Voucher_Retencion'], 'Nu_Voucher_Retencion' => $arrPost['Nu_Voucher_Retencion']);
        $this->db->update('documento_cabecera', $data, $where);
        
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Error al pagar detracción');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Detracción completada satisfactoriamente');
        }
    }
}
