<?php
class OrdenIngresoModel extends CI_Model{
	var $table          				= 'orden_ingreso';
	var $table_tipo_documento			= 'tipo_documento';
	var $table_serie_documento			= 'serie_documento';
	var $table_entidad					= 'entidad';
	var $table_tipo_documento_identidad	= 'tipo_documento_identidad';
	var $table_producto	= 'producto';
	var $table_organizacion				= 'organizacion';
	var $table_tabla_dato				= 'tabla_dato';
	var $table_orden_ingreso_etapa = 'orden_ingreso_etapa';
	
    var $column_order = array('');
    var $column_search = array('');
    var $order = array('');
    
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
    	$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
    	
        if($this->input->post('Filtro_area_ingreso')){
        	$this->db->where('VC.ID_Area_Ingreso', $this->input->post('Filtro_area_ingreso'));
        }
		
		if($this->input->post('Filtro_serie_documento')){
        	$this->db->where('VC.ID_Serie_Documento', $this->input->post('Filtro_serie_documento'));
		}
		
        if($this->input->post('Filtro_NumeroDocumento')){
        	$this->db->where('VC.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento'));
        }
        
        if($this->input->post('Filtro_Estado') != ''){
        	$this->db->where('VC.Nu_Estado', $this->input->post('Filtro_Estado'));
        }
        
        if($this->input->post('Filtro_Estado_Facturacion') != ''){
        	$this->db->where('VC.Nu_Estado_Facturacion', $this->input->post('Filtro_Estado_Facturacion'));
        }

        if($this->input->post('Filtro_Entidad')){
        	$this->db->where('CLI.No_Entidad', $this->input->post('Filtro_Entidad'));
        }

        if($this->input->post('Filtro_Placa')){
        	$this->db->where('ITEM.No_Placa_Vehiculo', $this->input->post('Filtro_Placa'));
        }

        if($this->input->post('Filtro_Marca')){
        	$this->db->where('ITEM.No_Marca_Vehiculo', $this->input->post('Filtro_Marca'));
        }

        if($this->input->post('Filtro_Modelo')){
        	$this->db->where('ITEM.No_Modelo_Vehiculo', $this->input->post('Filtro_Modelo'));
        }

        if($this->input->post('Filtro_Kilometraje')){
        	$this->db->where('VC.No_Kilometraje', $this->input->post('Filtro_Kilometraje'));
        }
        
		if(!empty($this->input->post('filtro_nivel_combustible'))){
        	$this->db->where('NCOMB.ID_Tabla_Dato', $this->input->post('filtro_nivel_combustible'));
        }

        //$this->db->select('VC.No_Imagen, VC.No_Imagen_Url, VC.ID_Orden_Ingreso, VC.Nu_Estado, VC.ID_Entidad, VC.ID_Producto, VC.Fe_Emision, VC.Fe_Entrega, VC.Fe_Entrega_Tentativa, CLI.No_Entidad, TDOCU.No_Tipo_Documento, SD.ID_Serie_Documento, VC.ID_Numero_Documento, ITEM.No_Placa_Vehiculo, ITEM.No_Marca_Vehiculo, ITEM.No_Modelo_Vehiculo, VC.No_Kilometraje, VC.No_Nivel_Combustible, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, NCOMB.No_Descripcion AS No_Descripcion_Nivel_Combustible, TDFACTURADO.No_Class AS No_Class_Facturado, TDFACTURADO.No_Descripcion AS No_Descripcion_Facturado, VC.Nu_Estado_Facturacion, VC.ID_Etapa, OIE.No_Etapa, OIE.No_Class_Etapa')
		$this->db->select('VC.No_Imagen, VC.No_Imagen_Url, VC.ID_Orden_Ingreso, VC.Nu_Estado, VC.ID_Entidad, VC.ID_Producto, VC.Fe_Emision, VC.Fe_Entrega, VC.Fe_Entrega_Tentativa, CLI.No_Entidad, TDOCU.No_Tipo_Documento, SD.ID_Serie_Documento, VC.ID_Numero_Documento, ITEM.No_Placa_Vehiculo, ITEM.No_Marca_Vehiculo, ITEM.No_Modelo_Vehiculo, VC.No_Kilometraje, VC.No_Nivel_Combustible, NCOMB.No_Descripcion AS No_Descripcion_Nivel_Combustible, VC.Nu_Estado_Facturacion, VC.ID_Etapa, OIE.No_Etapa, OIE.No_Class_Etapa')
		->from($this->table . ' AS VC')
		->join($this->table_orden_ingreso_etapa . ' AS OIE', 'OIE.ID_Etapa = VC.ID_Etapa', 'left')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Area_Ingreso', 'join')
		->join($this->table_serie_documento . ' AS SD', 'SD.ID_Serie_Documento_PK = VC.ID_Serie_Documento', 'join')
		->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = VC.ID_Entidad', 'join')
		->join($this->table_producto . ' AS ITEM', 'ITEM.ID_Producto = VC.ID_Producto', 'join')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = CLI.ID_Tipo_Documento_Identidad', 'join')
    	->join($this->table_tabla_dato . ' AS NCOMB', 'NCOMB.ID_Tabla_Dato = VC.No_Nivel_Combustible', 'left')
    	/*
		->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoOrdenIngreso"', 'join')
		->join($this->table_tabla_dato . ' AS TDFACTURADO', 'TDFACTURADO.Nu_Valor = VC.Nu_Estado_Facturacion AND TDFACTURADO.No_Relacion = "Tipos_EstadoFacturacion"', 'join')
		*/
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion);

		if(isset($_POST['order']))
        	$this->db->order_by( 'CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
        else if(isset($this->order))
        	$this->db->order_by( 'CONVERT(VC.ID_Numero_Documento, SIGNED INTEGER) DESC' );
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $query = "SELECT
 VC.ID_Empresa,
 VC.ID_Organizacion,
 VC.ID_Almacen,
 VC.ID_Orden_Ingreso,
 VC.Nu_Estado,
 VC.ID_Area_Ingreso,
 VC.ID_Serie_Documento,
 VC.ID_Numero_Documento,
 CLI.ID_Tipo_Documento_Identidad,
 CLI.ID_Entidad,
 CLI.No_Entidad,
 CLI.Nu_Documento_Identidad,
 CLI.Txt_Direccion_Entidad,
 CLI.Nu_Telefono_Entidad,
 CLI.Nu_Celular_Entidad,
 TDOCU.Nu_Impuesto,
 TDOCU.Nu_Enlace,
 VC.Fe_Emision,
 VC.Fe_Entrega,
 VC.Fe_Entrega_Tentativa,
 VC.No_Kilometraje,
 VC.No_Nivel_Combustible,
 VC.Txt_Reparacion,
 PRO.ID_Producto,
 PRO.No_Placa_Vehiculo,
 PRO.No_Marca_Vehiculo,
 PRO.No_Modelo_Vehiculo,
 PRO.No_Color_Vehiculo,
 RT.ID_Relacion_Enlace_Tabla AS ID_Presupuesto,
 VC.Nu_Estado_Facturacion
FROM
 " . $this->table . " AS VC
 JOIN " . $this->table_entidad . " AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
 JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
 JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Area_Ingreso)
 LEFT JOIN relacion_tabla AS RT ON(RT.ID_Origen_Tabla = VC.ID_Orden_Ingreso)
JOIN producto AS PRO ON(PRO.ID_Producto = VC.ID_Producto)
WHERE VC.ID_Orden_Ingreso = " . $ID;
        return $this->db->query($query)->result();
    }
    
    public function agregarVenta($arrOrdenIngresoCabecera, $arrClienteNuevo, $arrVehiculoNuevo){
		$query = "SELECT
ID_Tipo_Documento,
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM
serie_documento
WHERE
ID_Serie_Documento_PK=" . $arrOrdenIngresoCabecera['ID_Serie_Documento'];
		$arrSerieDocumento = $this->db->query($query)->row();

		$arrOrdenIngresoCabecera['ID_Numero_Documento'] = $arrSerieDocumento->Nu_Numero_Documento;
		
		if ( $arrSerieDocumento == '' || empty($arrSerieDocumento) ) {
			$this->db->trans_rollback();
			return array('status' => 'danger', 'style_modal' => 'modal-danger', 'message' => 'Deben configurar serie, no existe');
		}

		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND ID_Area_Ingreso = " . $arrOrdenIngresoCabecera['ID_Area_Ingreso'] . " AND ID_Serie_Documento = '" . $arrSerieDocumento->ID_Serie_Documento . "' AND ID_Numero_Documento = '" . $arrSerieDocumento->Nu_Numero_Documento . "' LIMIT 1")->row()->existe > 0){
			$this->db->trans_rollback();
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		}else{
			$this->db->trans_begin();
			
			$ID_Entidad = $arrOrdenIngresoCabecera['ID_Entidad'];
			if (is_array($arrClienteNuevo)){
				unset($arrOrdenIngresoCabecera['ID_Entidad']);
				//Si no existe el cliente, lo crearemos
				if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
					$arrCliente = array(
						'ID_Empresa'					=> $this->empresa->ID_Empresa,
						'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
						'Nu_Tipo_Entidad'				=> 0,
						'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
						'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
						'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
						'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
						'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
						'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
						'Nu_Estado' 					=> 1,
					);
					$this->db->insert('entidad', $arrCliente);
					$ID_Entidad = $this->db->insert_id();
				} else {
					$this->db->trans_rollback();
					return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
				}
			}
			$arrOrdenIngresoCabecera = array_merge($arrOrdenIngresoCabecera, array("ID_Entidad" => $ID_Entidad));
			
			$ID_Producto = $arrOrdenIngresoCabecera['ID_Producto'];
			if (is_array($arrVehiculoNuevo)){
				unset($arrOrdenIngresoCabecera['ID_Producto']);			
				if($this->db->query("SELECT COUNT(*) AS existe FROM producto WHERE ID_Empresa=".$arrOrdenIngresoCabecera['ID_Empresa']." AND ID_Entidad='" . $ID_Entidad . "' AND No_Placa_Vehiculo='" . $arrVehiculoNuevo['No_Placa_Vehiculo'] . "' LIMIT 1")->row()->existe == 0){
					$arrVehiculoNuevo = array_merge($arrVehiculoNuevo, array("ID_Empresa" => $arrOrdenIngresoCabecera['ID_Empresa'], "ID_Entidad" => $ID_Entidad, "No_Producto" => $arrVehiculoNuevo['No_Placa_Vehiculo'], "Nu_Tipo_Producto" => 3, "ID_Tipo_Producto" => 2));
					$this->db->insert('producto', $arrVehiculoNuevo);
					$ID_Producto = $this->db->insert_id();
				} else {
					$this->db->trans_rollback();
					return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
				}
			}
			$arrOrdenIngresoCabecera = array_merge($arrOrdenIngresoCabecera, array("ID_Producto" => $ID_Producto));
			
			$ID_Origen_Tabla = $arrOrdenIngresoCabecera['ID_Origen_Tabla'];
			unset($arrOrdenIngresoCabecera['ID_Origen_Tabla']);
			$this->db->insert($this->table, $arrOrdenIngresoCabecera);
			$Last_ID_Orden_Ingreso = $this->db->insert_id();
				
			if ( $ID_Origen_Tabla != 0 ) {
				$arrRelacionTabla = array(
					'ID_Empresa' => $arrOrdenIngresoCabecera['ID_Empresa'],
					'Nu_Relacion_Datos' => 1,//Relación Orden de ingreso a Presupuesto
					'ID_Relacion_Enlace_Tabla' => $ID_Origen_Tabla,
					'ID_Origen_Tabla' => $Last_ID_Orden_Ingreso,
				);
				$this->db->insert('relacion_tabla', $arrRelacionTabla);
			}

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
			} else {
				$this->db->trans_commit();
				$this->db->query("UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1 WHERE ID_Serie_Documento_PK=" . $arrOrdenIngresoCabecera['ID_Serie_Documento']);
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado', 'Last_ID_Orden_Ingreso' => $Last_ID_Orden_Ingreso);
			}
		}
    }
    
    public function actualizarVenta($where, $arrOrdenIngresoCabecera, $arrClienteNuevo, $arrVehiculoNuevo){
		$this->db->trans_begin();
		
		$arrDataModificar = $this->db->query("SELECT ID_Orden_Ingreso, ID_Area_Ingreso, ID_Numero_Documento FROM orden_ingreso WHERE ID_Orden_Ingreso=" . $where['ID_Orden_Ingreso'] . " LIMIT 1")->result();
		//$ID_Orden_Ingreso = $arrDataModificar[0]->ID_Orden_Ingreso; //05/03/2021

		$ID_Orden_Ingreso = $arrDataModificar[0]->ID_Orden_Ingreso;
		$ID_Area_Ingreso = $arrDataModificar[0]->ID_Area_Ingreso;
		$ID_Numero_Documento = $arrDataModificar[0]->ID_Numero_Documento;
		
		if ($ID_Area_Ingreso != $arrOrdenIngresoCabecera['ID_Area_Ingreso']) {
			$ID_Numero_Documento = $arrOrdenIngresoCabecera['ID_Numero_Documento'];
			$this->db->query("UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1 WHERE ID_Serie_Documento_PK=" . $arrOrdenIngresoCabecera['ID_Serie_Documento']);
		}
			
        $this->db->delete($this->table, $where);
		
		$arrOrdenIngresoCabecera['ID_Numero_Documento'] = $ID_Numero_Documento;

		if (is_array($arrClienteNuevo)){
		    unset($arrOrdenIngresoCabecera['ID_Entidad']);
		    //Si no existe el cliente, lo crearemos
		    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
				$arrCliente = array(
	                'ID_Empresa'					=> $this->user->ID_Empresa,
	                'ID_Organizacion'				=> $arrDataModificar[0]->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 0,
	                'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
	                'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrCliente);
	    		$Last_ID_Entidad = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenIngresoCabecera = array_merge($arrOrdenIngresoCabecera, array("ID_Entidad" => $Last_ID_Entidad));
		}
		
		$ID_Producto = $arrOrdenIngresoCabecera['ID_Producto'];
		if (is_array($arrVehiculoNuevo)){
		    unset($arrOrdenIngresoCabecera['ID_Producto']);			
			if($this->db->query("SELECT COUNT(*) AS existe FROM producto WHERE ID_Empresa=".$arrOrdenIngresoCabecera['ID_Empresa']." AND ID_Entidad='" . $ID_Entidad . "' AND No_Placa_Vehiculo='" . $arrVehiculoNuevo['No_Placa_Vehiculo'] . "' LIMIT 1")->row()->existe == 0){
		    	$arrVehiculoNuevo = array_merge($arrVehiculoNuevo, array("ID_Empresa" => $arrOrdenIngresoCabecera['ID_Empresa'], "ID_Entidad" => $ID_Entidad, "No_Producto" => $arrVehiculoNuevo['No_Placa_Vehiculo'], "Nu_Tipo_Producto" => 3, "ID_Tipo_Producto" => 2));
				$this->db->insert('producto', $arrVehiculoNuevo);
	    		$ID_Producto = $this->db->insert_id();
			} else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
			}
		}
    	$arrOrdenIngresoCabecera = array_merge($arrOrdenIngresoCabecera, array("ID_Producto" => $ID_Producto));
		
		$arrOrdenIngresoCabecera['ID_Orden_Ingreso'] = $where['ID_Orden_Ingreso'];

		$ID_Origen_Tabla = $arrOrdenIngresoCabecera['ID_Origen_Tabla'];
		unset($arrOrdenIngresoCabecera['ID_Origen_Tabla']);
		$this->db->insert($this->table, $arrOrdenIngresoCabecera);
		$Last_ID_Orden_Ingreso = $this->db->insert_id();
				
		if ( $ID_Origen_Tabla != 0 ) {
			$where = array( 'Nu_Relacion_Datos' => 1, 'ID_Relacion_Enlace_Tabla' => $Last_ID_Orden_Ingreso, 'ID_Origen_Tabla' => $ID_Origen_Tabla );
			$this->db->delete('relacion_tabla', $where);
			$arrRelacionTabla = array(
				'ID_Empresa' => $arrOrdenIngresoCabecera['ID_Empresa'],
				'Nu_Relacion_Datos' => 1,//Relación Orden de ingreso a Presupuesto
				'ID_Relacion_Enlace_Tabla' => $ID_Origen_Tabla,
				'ID_Origen_Tabla' => $Last_ID_Orden_Ingreso,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);
		}

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
        } else {
			$this->db->query("SET FOREIGN_KEY_CHECKS=ON;");
            $this->db->trans_commit();
	        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado', 'Last_ID_Orden_Ingreso' => $Last_ID_Orden_Ingreso);
        }
    }
    
	public function eliminarOrdenIngreso($ID, $Nu_Descargar_Inventario){
		$this->db->trans_begin();
						
		$this->db->where('ID_Empresa', $this->user->ID_Empresa);
		$this->db->where('ID_Orden_Ingreso', $ID);
		$this->db->delete($this->table);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
		} else {
            $this->db->trans_commit();
			return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
		}
	}
    
	public function estadoOrdenIngreso($ID, $Nu_Descargar_Inventario, $Nu_Estado){
		$this->db->trans_begin();
        
        $where_orden_venta = array('ID_Orden_Ingreso' => $ID);
        $arrData = array(
			'Nu_Estado' => $Nu_Estado,
		);
		$this->db->update('documento_cabecera', $arrData, $where_orden_venta);
                
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al cambiar estado');
        } else {
            $this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        }
	}
    
	public function duplicarOrdenIngreso($ID){
		$this->db->trans_begin();
        
        $query_cabecera = "SELECT
 ID_Empresa,
 ID_Organizacion,
 ID_Almacen,
 ID_Entidad,
 ID_Contacto,
 ID_Tipo_Asiento,
 ID_Tipo_Documento,
 ID_Matricula_Empleado,
 Fe_Emision,
 ID_Medio_Pago,
 ID_Rubro,
 ID_Moneda,
 Fe_Vencimiento,
 Fe_Periodo,
 Nu_Correlativo,
 Nu_Descargar_Inventario,
 ID_Lista_Precio_Cabecera,
 Txt_Glosa,
 Ss_Descuento,
 Ss_Total,
 Ss_Total_Saldo,
 Ss_Percepcion,
 Fe_Detraccion,
 Nu_Detraccion,
 Nu_Estado,
 Txt_Garantia,
 ID_Mesero,
 ID_Comision,
 No_Formato_PDF
FROM
 documento_cabecera
WHERE
 ID_Orden_Ingreso = " . $ID;
		$arrCabecera = $this->db->query($query_cabecera)->result();
		
		foreach ($arrCabecera as $row) {
			$documento_cabecera = array(
				'ID_Empresa' => $row->ID_Empresa,
				'ID_Organizacion' => $row->ID_Organizacion,
				'ID_Almacen' => $row->ID_Almacen,
				'ID_Entidad' => $row->ID_Entidad,
				'ID_Contacto' => $row->ID_Contacto,
				'ID_Tipo_Asiento' => $row->ID_Tipo_Asiento,
				'ID_Tipo_Documento' => $row->ID_Tipo_Documento,
				'ID_Matricula_Empleado' => $row->ID_Matricula_Empleado,
				'Fe_Emision' => $row->Fe_Emision,
				'ID_Medio_Pago' => $row->ID_Medio_Pago,
				'ID_Rubro' => $row->ID_Rubro,
				'ID_Moneda' => $row->ID_Moneda,
				'Fe_Vencimiento' => $row->Fe_Vencimiento,
				'Fe_Periodo' => $row->Fe_Periodo,
				'Nu_Correlativo' => $row->Nu_Correlativo,
				'Nu_Descargar_Inventario' => $row->Nu_Descargar_Inventario,
				'ID_Lista_Precio_Cabecera' => $row->ID_Lista_Precio_Cabecera,
				'Txt_Glosa' => $row->Txt_Glosa,
				'Ss_Descuento' => $row->Ss_Descuento,
				'Ss_Total' => $row->Ss_Total,
				'Ss_Total_Saldo' => $row->Ss_Total_Saldo,
				'Ss_Percepcion' => $row->Ss_Percepcion,
				'Fe_Detraccion' => $row->Fe_Detraccion,
				'Nu_Detraccion' => $row->Nu_Detraccion,
				'Nu_Estado' => 5,
				'Txt_Garantia' => $row->Txt_Garantia,
				'ID_Mesero' => $row->ID_Mesero,
				'ID_Comision' => $row->ID_Comision,
				'No_Formato_PDF' => $row->No_Formato_PDF,
			);
    	}
    	
		$this->db->insert($this->table, $documento_cabecera);
		$ID_Orden_Ingreso = $this->db->insert_id();
        
        $query_detalle = " 
SELECT
 ID_Empresa,
 ID_Producto,
 Qt_Producto,
 Ss_Precio,
 Ss_Descuento,
 Ss_Descuento_Impuesto,
 Po_Descuento,
 Ss_SubTotal,
 ID_Impuesto_Cruce_Documento,
 Ss_Impuesto,
 Ss_Total
FROM
 documento_detalle
WHERE
 ID_Orden_Ingreso = " . $ID;
		$arrDetalle = $this->db->query($query_detalle)->result();
		
		foreach ($arrDetalle as $row) {
			$documento_detalle[] = array(
				'ID_Empresa' => $row->ID_Empresa,
				'ID_Orden_Ingreso' => $ID_Orden_Ingreso,
				'ID_Producto' => $row->ID_Producto,
				'Qt_Producto' => $row->Qt_Producto,
				'Ss_Precio' => $row->Ss_Precio,
				'Ss_Descuento' => $row->Ss_Descuento,
				'Ss_Descuento_Impuesto'	=> $row->Ss_Descuento_Impuesto,
				'Po_Descuento' => $row->Po_Descuento,
				'Ss_SubTotal' => $row->Ss_SubTotal,
				'ID_Impuesto_Cruce_Documento' => $row->ID_Impuesto_Cruce_Documento,
				'Ss_Impuesto' => $row->Ss_Impuesto,
				'Ss_Total' => $row->Ss_Total,
			);
    	}
		$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
 
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al duplicar orden de venta');
        } else {
            $this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro agregado');
        }
	}

	public function cambiarEstadoFacturaOI($ID, $Nu_Estado){
        $where = array('ID_Orden_Ingreso' => $ID);
        $arrData = array( 'Nu_Estado_Facturacion' => $Nu_Estado );
		if ($this->db->update('orden_ingreso', $arrData, $where) > 0)
			return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al cambiar estado');
	}

	public function cambiarEstadoEtapaOI($ID, $Nu_Estado){
        $where = array('ID_Orden_Ingreso' => $ID);
        $arrData = array( 'ID_Etapa' => $Nu_Estado );
		if ($this->db->update('orden_ingreso', $arrData, $where) > 0)
			return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
		return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al cambiar etapa');
	}
}
