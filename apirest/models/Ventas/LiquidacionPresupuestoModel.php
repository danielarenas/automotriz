<?php
class LiquidacionPresupuestoModel extends CI_Model{
	var $table                      = 'documento_cabecera';
	var $table_empresa              = 'empresa';
	var $table_organizacion	        = 'organizacion';
	var $table_almacen		        = 'almacen';
	var $table_serie_documento = 'serie_documento';
	var $table_tipo_documento	    = 'tipo_documento';
	var $table_entidad = 'entidad';
	var $table_tabla_dato           = 'tabla_dato';
	
    var $column_order = array('No_Almacen', 'Fe_Emision', 'No_Tipo_Documento_Breve', 'ID_Documento_Cabecera', 'Ss_Total');
    var $column_search = array('');
    var $order = array('ID_Documento_Cabecera' => 'desc');
	
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
		
        if( $this->input->post('Filtro_fecha_liquidacion') == 0){
    		$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
        } else if( $this->input->post('Filtro_fecha_liquidacion') == 1){
    		$this->db->where("VC.Fe_Liquidacion_Envio_Presupuesto BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
        }

		if(!empty($this->input->post('Filtro_SerieDocumento'))){
        	$this->db->where('VC.ID_Serie_Documento', $this->input->post('Filtro_SerieDocumento'));
        }

		if(!empty($this->input->post('Filtro_NumeroDocumento'))){
        	$this->db->where('VC.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento'));
        }

		if(!empty($this->input->post('Filtro_Entidad'))){
        	$this->db->like('CLI.No_Entidad', $this->input->post('Filtro_Entidad'));
        }

		if(!empty($this->input->post('Filtro_Servicio'))){
        	$this->db->where('VC.Txt_Tipo_Servicio_Presupuesto', $this->input->post('Filtro_Servicio'));
        }

		if(!empty($this->input->post('Filtro_Periodo'))){
        	$this->db->where('VC.Txt_Rango_Fecha_Presupuesto', $this->input->post('Filtro_Periodo'));
        }

		if(!empty($this->input->post('Filtro_OC'))){
        	$this->db->where('VC.Txt_Orden_Compra_Presupuesto', $this->input->post('Filtro_OC'));
        }

		if($this->input->post('Filtro_envio_fecha_liquidacion') != 'todos'){
        	$this->db->where('VC.Nu_Envio_Fecha_Liquidacion_Presupuesto', $this->input->post('Filtro_envio_fecha_liquidacion'));
        }

		$this->db->select('VC.Nu_Estado, ALMA.No_Almacen, VC.Fe_Emision, VC.ID_Tipo_Documento, TDOCU.No_Tipo_Documento_Breve, VC.ID_Serie_Documento, VC.ID_Numero_Documento, CLI.No_Entidad, ID_Documento_Cabecera, Ss_Total, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, VC.Txt_Tipo_Servicio_Presupuesto, VC.Txt_Rango_Fecha_Presupuesto, VC.Txt_Orden_Compra_Presupuesto, VC.Nu_Envio_Fecha_Liquidacion_Presupuesto, VC.Fe_Liquidacion_Envio_Presupuesto')
		->from($this->table  .' AS VC')
		->join($this->table_serie_documento . ' AS SD', 'SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK', 'left')
		->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = VC.ID_Entidad', 'join')
        ->join($this->table_empresa . ' AS EMP', 'EMP.ID_Empresa = VC.ID_Empresa', 'join')
		->join($this->table_organizacion . ' AS ORG', 'ORG.ID_Organizacion = VC.ID_Organizacion', 'join')
		->join($this->table_almacen . ' AS ALMA', 'ALMA.ID_Organizacion = VC.ID_Organizacion AND ALMA.ID_Almacen = VC.ID_Almacen', 'join')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
    	->where('VC.ID_Tipo_Documento', 24);
		
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $query = "SELECT
VC.ID_Empresa,
VC.ID_Organizacion,
VC.ID_Almacen,
VC.ID_Documento_Cabecera,
VC.Nu_Estado,
VC.ID_Tipo_Documento,
VC.ID_Serie_Documento,
VC.ID_Numero_Documento,
SD.Nu_Cantidad_Caracteres,
CLI.ID_Tipo_Documento_Identidad,
CLI.ID_Entidad,
CLI.No_Entidad,
CLI.Nu_Documento_Identidad,
CLI.Txt_Direccion_Entidad,
TDOCUIDEN.No_Tipo_Documento_Identidad_Breve,
CAR.No_Placa_Vehiculo,
VC.No_Kilometraje,
OV.ID_Numero_Documento AS ID_Numero_Documento_OV,
OV.Fe_Emision,
TDMANTE.No_Descripcion AS No_Tipo_Mantenimiento,
MONE.No_Moneda,
MONE.No_Signo,
OV.Ss_Total AS Ss_Total_OV,
OV.ID_Documento_Cabecera AS ID_Documento_Cabecera_OV,
ROUND(VC.Ss_Total, 2) AS Ss_Total
FROM
documento_cabecera AS VC
JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
JOIN relacion_tabla AS RT ON(RT.ID_Relacion_Enlace_Tabla = VC.ID_Documento_Cabecera)
JOIN documento_cabecera AS OV ON(OV.ID_Documento_Cabecera = RT.ID_Origen_Tabla)
JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
JOIN tipo_documento AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
JOIN producto AS CAR ON(CAR.ID_Producto = OV.ID_Producto)
JOIN tabla_dato AS TDMANTE ON(TDMANTE.Nu_Valor = OV.Nu_Tipo_Mantenimiento AND TDMANTE.No_Relacion = 'Tipos_Mantenimiento')
WHERE VC.ID_Documento_Cabecera=" . $ID;
        return $this->db->query($query)->result();
	}
	
	public function generarLiquidacion($arrPost){
		$this->db->trans_begin();
		
		$iIdFirst = 0;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			$iIdFirst = $key;
			break;
		}
$sql = "SELECT
ID_Empresa,
ID_Organizacion,
ID_Almacen,
ID_Entidad,
ID_Moneda,
ID_Medio_Pago,
Ss_Total
FROM
documento_cabecera
WHERE ID_Documento_Cabecera = " . $iIdFirst .  " LIMIT 1";
		$arrHeader = $this->db->query($sql)->result();
		$ID_Empresa = $arrHeader[0]->ID_Empresa;
		$ID_Organizacion = $arrHeader[0]->ID_Organizacion;
		$ID_Almacen = $arrHeader[0]->ID_Almacen;
		$ID_Entidad = $arrHeader[0]->ID_Entidad;
		$ID_Moneda = $arrHeader[0]->ID_Moneda;
		$ID_Medio_Pago = $arrHeader[0]->ID_Medio_Pago;
		$Ss_Total = $arrHeader[0]->Ss_Total;

		$where_serie = '';
		if ($arrPost['hidden-ID_Tipo_Documento'] == 2)
			$where_serie = ' AND ID_Serie_Documento LIKE "0%"';
		if ($arrPost['hidden-ID_Tipo_Documento'] == 3)
			$where_serie = ' AND ID_Serie_Documento LIKE "F%"';

		$query = "SELECT
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM
serie_documento
WHERE
ID_Empresa=" . $ID_Empresa . "
AND ID_Organizacion=" . $ID_Organizacion . "
AND ID_Almacen=" . $ID_Almacen . "
AND ID_Tipo_Documento=" . $arrPost['hidden-ID_Tipo_Documento'] . "
AND Nu_Estado=1
AND ID_POS IS NULL
" . $where_serie . "
LIMIT 1";
        $arrSerieDocumento = $this->db->query($query)->row();
        
		$arrHeader = array(
			'ID_Empresa'				=> $ID_Empresa,
			'ID_Organizacion'			=> $ID_Organizacion,
			'ID_Almacen'			    => $ID_Almacen,
			'ID_Entidad'				=> $ID_Entidad,
			'ID_Tipo_Asiento'			=> 1,//Venta
			'ID_Tipo_Documento'			=> $arrPost['hidden-ID_Tipo_Documento'],//Factura        
            'ID_Serie_Documento_PK'		=> $arrSerieDocumento->ID_Serie_Documento_PK,
            'ID_Serie_Documento'		=> $arrSerieDocumento->ID_Serie_Documento,
            'ID_Numero_Documento'		=> $arrSerieDocumento->Nu_Numero_Documento,                
			'Fe_Emision'				=> dateNow('fecha'),
			'Fe_Emision_Hora'			=> dateNow('fecha_hora'),
			'ID_Moneda'					=> $ID_Moneda,//Soles
			'ID_Medio_Pago'				=> $ID_Medio_Pago,
			'Fe_Vencimiento'			=> dateNow('fecha'),
			'Fe_Periodo' => dateNow('fecha'),
			'Nu_Descargar_Inventario' => 0,
			'Ss_Total' => $Ss_Total,
			'Ss_Total_Saldo' => 0.00,
			'Ss_Vuelto' => 0.00,
			'Nu_Correlativo' => 0,
			'Nu_Estado' => 6,//Pendiente
			'Nu_Transporte_Lavanderia_Hoy' => 0,
			'Nu_Estado_Lavado' => 0,
			'Fe_Entrega' => dateNow('fecha'),
			'Nu_Tipo_Recepcion' => 0,
			'ID_Transporte_Delivery' => 0,
			'Txt_Direccion_Delivery' => '-',
			'Txt_Glosa' => '',
			'No_Formato_PDF' => 'A4',
		);

		$this->db->insert('documento_cabecera', $arrHeader);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();

        $fTotalHeader = 0.00;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			//Cambiar estado a facturado presupuesto orden de venta
			$query_presupuesto = "SELECT ID_Origen_Tabla FROM relacion_tabla WHERE ID_Relacion_Enlace_Tabla = " . $key . " AND Nu_Relacion_Datos=4";
			$arrPresupuesto = $this->db->query($query_presupuesto)->result();
			foreach ($arrPresupuesto as $row){
				$sql = "UPDATE documento_cabecera SET Nu_Estado=21 WHERE ID_Documento_Cabecera=" . $row->ID_Origen_Tabla;//21 = Facturado
				$this->db->query($sql);
				
				//Cambiar estado a Orden de ingreso a Facturado
				$query_orden_ingreso = "SELECT ID_Origen_Tabla FROM relacion_tabla WHERE ID_Relacion_Enlace_Tabla = " . $row->ID_Origen_Tabla . " AND Nu_Relacion_Datos=1";
				$arrOrdenIngreso = $this->db->query($query_orden_ingreso)->result();
				foreach ($arrOrdenIngreso as $row2){
					//$sql = "UPDATE orden_ingreso SET Nu_Estado=3 WHERE ID_Orden_Ingreso=" . $row2->ID_Origen_Tabla;//3 = Facturado
					$sql = "UPDATE orden_ingreso SET Nu_Estado_Facturacion=1 WHERE ID_Orden_Ingreso=" . $row2->ID_Origen_Tabla;//3 = Facturado
					$this->db->query($sql);
				}
			}

			//Cambiar estado a liquidación
		    $sql = "UPDATE documento_cabecera SET Nu_Estado=21 WHERE ID_Documento_Cabecera=" . $key;//21 = Facturado
            $this->db->query($sql);
			
            $query_detalle = "SELECT
LV.ID_Documento_Cabecera,
CAR.No_Marca_Vehiculo,
CAR.No_Modelo_Vehiculo,
CAR.No_Placa_Vehiculo,
TD.No_Tipo_Documento,
SD.ID_Serie_Documento,
LV.ID_Numero_Documento,
LV.Ss_Total
FROM
relacion_tabla AS RT
JOIN documento_cabecera AS LV ON(LV.ID_Documento_Cabecera = RT.ID_Origen_Tabla)
JOIN producto AS CAR ON(CAR.ID_Producto = LV.ID_Producto)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = LV.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = LV.ID_Serie_Documento_PK)
WHERE
RT.Nu_Relacion_Datos = 4 AND RT.ID_Relacion_Enlace_Tabla = " . $key;
            $arrDetalle = $this->db->query($query_detalle)->result();

			foreach ($arrDetalle as $row_detalle) {
				$ID_Impuesto = $this->db->query("SELECT ID_Impuesto FROM impuesto WHERE ID_Empresa = " . $ID_Empresa . " AND No_Impuesto='Gravado - Operación Onerosa' LIMIT 1")->row()->ID_Impuesto;
				$ID_Unidad_Medida = $this->db->query("SELECT ID_Unidad_Medida FROM unidad_medida WHERE ID_Empresa = " . $ID_Empresa . " AND No_Unidad_Medida='Unidad (Bienes)' AND Nu_Estado = 1 LIMIT 1")->row()->ID_Unidad_Medida;

				$sCodigoBarra=$row_detalle->ID_Documento_Cabecera;
				$arrItem = $this->db->query("SELECT ID_Producto FROM producto WHERE ID_Empresa = " . $ID_Empresa . " AND Nu_Codigo_Barra = '" . $sCodigoBarra . "' LIMIT 1")->result();

				if ( !empty($arrItem) ) {
					$ID_Item = $arrItem[0]->ID_Producto;
				} else {
					$sNombreItem = 'SERVICIO DE MANTENIMIENTO PREVENTIVO MARCA: ' . $row_detalle->No_Marca_Vehiculo .  ' MODELO: ' . $row_detalle->No_Modelo_Vehiculo . ' PLACA: ' . $row_detalle->No_Placa_Vehiculo . ' - ' . $row_detalle->No_Tipo_Documento . ' - ' . $row_detalle->ID_Serie_Documento . ' - ' . $row_detalle->ID_Numero_Documento;
					
					$arrItem = array(
						'ID_Empresa'				=> $ID_Empresa,
						'Nu_Tipo_Producto'			=> 4,//Producto creados por Pago de Contratista
						'ID_Tipo_Producto'			=> 1,
						'Nu_Codigo_Barra'			=> $sCodigoBarra,//ID_Unico_BD presupuesto
						'ID_Producto_Sunat'			=> 0,
						'No_Producto' => $sNombreItem,
						'Ss_Precio'	=> $row_detalle->Ss_Total,
						'Ss_Costo' => 0,
						'No_Codigo_Interno'			=> '',
						'ID_Impuesto'				=> $ID_Impuesto,
						'Nu_Lote_Vencimiento'		=> '',
						'ID_Unidad_Medida'			=> $ID_Unidad_Medida,
						'ID_Familia' => $ID_Familia,
						'ID_Impuesto_Icbper'		=> 0,
						'Nu_Compuesto'				=> 0,
						'Nu_Estado'					=> 0,
						'Txt_Ubicacion_Producto_Tienda'	=> '',
						'Txt_Producto' => '',
						'Nu_Stock_Minimo' => 0,
						'Qt_CO2_Producto' => '',
						'Nu_Receta_Medica' => '',
						'ID_Laboratorio' => 0,
						'ID_Tipo_Pedido_Lavado'	=> 0,
						'Txt_Composicion' => '',
						'No_Imagen_Item' => '',
						'Ss_Precio_Ecommerce_Online_Regular' => 0,
						'Ss_Precio_Ecommerce_Online' => 0,
						'ID_Familia_Marketplace' => 0,
						'ID_Sub_Familia_Marketplace' => 0,
						'ID_Marca_Marketplace' => 0,
					);
					$this->db->insert('producto', $arrItem);
					$ID_Item = $this->db->insert_id();
				}

				$ID_Impuesto_Cruce_Documento = $this->db->query("SELECT IMPDOC.ID_Impuesto_Cruce_Documento FROM impuesto AS IMP JOIN impuesto_cruce_documento AS IMPDOC ON (IMPDOC.ID_Impuesto = IMP.ID_Impuesto) WHERE IMP.ID_Impuesto = " . $ID_Impuesto . " AND IMPDOC.Nu_Estado = 1 LIMIT 1")->row()->ID_Impuesto_Cruce_Documento;

				$Ss_SubTotal_Detalle = $this->db->query("SELECT Ss_SubTotal FROM documento_detalle WHERE ID_Documento_Cabecera = " . $row_detalle->ID_Documento_Cabecera . " LIMIT 1")->row()->Ss_SubTotal;

				$documento_detalle[] = array(
					'ID_Empresa' => $ID_Empresa,
					'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
					'ID_Producto' => $ID_Item,
					'Qt_Producto' => 1,
					'Ss_Precio' => $row_detalle->Ss_Total,
					'Ss_SubTotal' => $Ss_SubTotal_Detalle,
					'Ss_Descuento' => 0,
					'Ss_Descuento_Impuesto' => 0,
					'Po_Descuento' => 0,
					'Txt_Nota' => "LIQ " . $key,
					'ID_Impuesto_Cruce_Documento' => $ID_Impuesto_Cruce_Documento,
					'Ss_Impuesto' => ($row_detalle->Ss_Total - $Ss_SubTotal_Detalle),
					'Ss_Total' => $row_detalle->Ss_Total,
					'Nu_Estado_Lavado' => 0,
					'ID_OI_Detalle' => $sCodigoBarra,
				);

				$fTotalHeader += $row_detalle->Ss_Total;				
			}
			
			$arrRelacionTabla = array(
				'ID_Empresa' => $ID_Empresa,
				'Nu_Relacion_Datos' => 5,//Relación Liquidacion a Factura
				'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
				'ID_Origen_Tabla' => $key,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);


			/*
            $query_detalle = "SELECT ID_Empresa,ID_Producto,ID_Impuesto_Cruce_Documento FROM documento_detalle WHERE ID_Documento_Cabecera = " . $key . " LIMIT 1";
            $arrDetalle = $this->db->query($query_detalle)->result();
            $ID_Empresa = $arrDetalle[0]->ID_Empresa;
            $ID_Producto = $arrDetalle[0]->ID_Producto;
            $ID_Impuesto_Cruce_Documento = $arrDetalle[0]->ID_Impuesto_Cruce_Documento;

            $sql = "SELECT SUM(Ss_SubTotal) AS Ss_SubTotal, SUM(Ss_Total) AS Ss_Total FROM documento_detalle WHERE ID_Documento_Cabecera = " . $key;
            $arrPresupuestoTotales = $this->db->query($sql)->result();
            $Ss_Total = $arrPresupuestoTotales[0]->Ss_Total;
            $Ss_SubTotal = $arrPresupuestoTotales[0]->Ss_SubTotal;

            $documento_detalle[] = array(
                'ID_Empresa' => $ID_Empresa,
                'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
                'ID_Producto' => $ID_Producto,
                'Qt_Producto' => 1,
                'Ss_Precio' => $Ss_Total,
                'Ss_SubTotal' => $Ss_SubTotal,
                'Ss_Descuento' => 0,
                'Ss_Descuento_Impuesto' => 0,
                'Po_Descuento' => 0,
                'Txt_Nota' => "LIQ " . $key,
                'ID_Impuesto_Cruce_Documento' => $ID_Impuesto_Cruce_Documento,
                'Ss_Impuesto' => ($Ss_Total - $Ss_SubTotal),
                'Ss_Total' => $Ss_Total,
                'Nu_Estado_Lavado' => 0,
            );

            $arrRelacionTabla = array(
                'ID_Empresa' => $ID_Empresa,
                'Nu_Relacion_Datos' => 5,//Relación Liquidacion a Factura
                'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
                'ID_Origen_Tabla' => $key,
            );
            $this->db->insert('relacion_tabla', $arrRelacionTabla);

            $fTotalHeader += $Ss_Total;
			*/
        }

		$sTipoDocumento = 'Factura';
		if($arrPost['hidden-ID_Tipo_Documento']==2)
			$sTipoDocumento = 'Nota de Venta';

		$this->db->insert_batch('documento_detalle', $documento_detalle);

		$sql = "UPDATE documento_cabecera SET Ss_Total = " . $fTotalHeader . " WHERE ID_Documento_Cabecera=" . $Last_ID_Documento_Cabecera;
        $this->db->query($sql);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Error al liquidar');
		} else {
			//$this->db->trans_rollback();
			$this->db->trans_commit();
			$sql = "UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1 WHERE ID_Serie_Documento_PK = " . $arrSerieDocumento->ID_Serie_Documento_PK;
			$this->db->query($sql);
			return array('sStatus' => 'success', 'sMessage' => $sTipoDocumento . ' generada correctamente');
		}
	}

	public function modificarVenta($arrPost){
		$this->db->trans_begin();

		$Fe_Liquidacion_Envio_Presupuesto = $arrPost['Fe_Liquidacion_Envio_Presupuesto-Modificar'];
		if($arrPost['Nu_Envio_Fecha_Liquidacion_Presupuesto']==1)
			$Fe_Liquidacion_Envio_Presupuesto = ToDate($arrPost['Fe_Liquidacion_Envio_Presupuesto']);

		$data = array(
			'Txt_Tipo_Servicio_Presupuesto' => $arrPost['Txt_Tipo_Servicio_Presupuesto'],
			'Txt_Rango_Fecha_Presupuesto' => $arrPost['Txt_Rango_Fecha_Presupuesto'],
			'Txt_Orden_Compra_Presupuesto' => $arrPost['Txt_Orden_Compra_Presupuesto'],
			'Nu_Envio_Fecha_Liquidacion_Presupuesto' => $arrPost['Nu_Envio_Fecha_Liquidacion_Presupuesto'],
			'Fe_Liquidacion_Envio_Presupuesto' => $Fe_Liquidacion_Envio_Presupuesto,
		);
		$where = array('ID_Documento_Cabecera' => $arrPost['ID_Documento_Cabecera-Modificar']);
		$this->db->update('documento_cabecera', $data, $where);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Problemas al modificar');
		} else {
			$this->db->trans_commit();
			return array('sStatus' => 'success', 'sMessage' => 'Registro modificado');
		}
    }
}
