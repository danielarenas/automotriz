<?php
class AgendamientosModel extends CI_Model{
	var $table = 'taller_agendamiento';
	var $table_empresa = 'empresa';
	var $table_entidad = 'entidad';
	var $table_producto = 'producto';
	var $table_tabla_dato = 'tabla_dato';
	
    var $column_order = array('No_Entidad','No_Placa_Vehiculo','Fe_Emision','Hora_Emision','No_Marca_Vehiculo','No_Modelo_Vehiculo','No_Tipo_Servicio','Nu_Kilometraje','Nu_Estado');
    var $column_search = array('');
    var $order = array('Fe_Emision' => 'desc');
	
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
    	$this->db->where("Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");

		$this->db->select('ID_Taller_Agendamiento, CLI.No_Entidad, ITEM.No_Placa_Vehiculo, Fe_Emision, Hora_Emision, ' . $this->table . '.No_Marca_Vehiculo, ' . $this->table . '.No_Modelo_Vehiculo, ' . $this->table . '.No_Tipo_Servicio, ' . $this->table . '.Nu_Kilometraje, ' . $this->table . '.Nu_Estado, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado')
		->from($this->table)
        ->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = ' . $this->table . '.ID_Entidad', 'join')
        ->join($this->table_producto . ' AS ITEM', 'ITEM.ID_Producto = ' . $this->table . '.ID_Producto_Placa', 'join')
        ->join($this->table_empresa . ' AS EMP', 'EMP.ID_Empresa = ' . $this->table . '.ID_Empresa', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = ' . $this->table . '.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
		->where($this->table . '.ID_Empresa', $this->empresa->ID_Empresa);
		
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
    	$this->db->select($this->table . '.*, CLI.No_Entidad, ITEM.No_Placa_Vehiculo');
        $this->db->from($this->table);
        $this->db->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = ' . $this->table . '.ID_Entidad', 'join');
        $this->db->join($this->table_producto . ' AS ITEM', 'ITEM.ID_Producto = ' . $this->table . '.ID_Producto_Placa', 'join');
		$this->db->where('ID_Taller_Agendamiento', $ID);
        $query = $this->db->get();
        return $query->row();
    }
    
    public function agregarSerie($data){
        if ( $this->db->insert($this->table, $data) > 0 )
            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
		return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
    }
    
    public function actualizarSerie($where, $data){
        if ( $this->db->update($this->table, $data, $where) > 0 )
            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
    }
    
	public function eliminarSerie($ID){
        $this->db->where('ID_Taller_Agendamiento', $ID);
        $this->db->delete($this->table);        
        if ( $this->db->affected_rows() > 0 )
            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
	}
    
	public function estadoAgendamiento($ID, $Nu_Estado){
		$this->db->trans_begin();
        
        $where = array('ID_Taller_Agendamiento' => $ID);
        $arrData = array( 'Nu_Estado' => $Nu_Estado );
		$this->db->update($this->table, $arrData, $where);
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al cambiar estado');
        } else {
			$this->db->trans_commit();
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        }
	}
}
