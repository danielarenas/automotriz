<?php
class VehiculoModel extends CI_Model{
	var $table = 'producto';
	var $table_stock_producto = 'stock_producto';
	var $table_entidad = 'entidad';
	var $table_tabla_dato = 'tabla_dato';
	
    var $column_order = array('No_Entidad', 'No_Placa_Vehiculo', 'No_Year_Vehiculo', 'No_Marca_Vehiculo', 'No_Modelo_Vehiculo', 'No_Tipo_Combustible_Vehiculo', 'No_Vin_Vehiculo', 'No_Motor', 'Nu_Kilometraje', 'No_Color_Vehiculo', 'Qt_Producto', 'No_Class_Estado');
    var $column_search = array();
    var $order = array('No_Year_Vehiculo' => 'asc', 'No_Entidad' => 'asc', 'No_Placa_Vehiculo' => 'asc');
    
	private $_batchImport;
	
	public function __construct(){
		parent::__construct();
	}
 
    public function setBatchImport($arrProducto) {
        $this->_batchImport = $arrProducto;
    }
    
    public function importData() {
	    $ID_Empresa = $this->user->ID_Empresa;
		$ID_Ubicacion_Inventario = 1;
		$iIdTipoLavado = 0;
    	$ID_Impuesto = 0;
    	$ID_Marca = 0;
    	$ID_Unidad_Medida = 0;
    	$ID_Familia = 0;
    	$ID_Sub_Familia = 0;
    	$ID_Producto_Sunat = 0;
    	$ID_Familia_Marketplace = 0;
    	$ID_Sub_Familia_Marketplace = 0;
    	$ID_Marca_Marketplace = 0;
        
        foreach ($this->_batchImport as $row) {
        	if ( !empty($row['No_Impuesto']) ) {
	       		if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_impuesto . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Impuesto='" . $row['No_Impuesto'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Impuesto = $this->db->query("SELECT ID_Impuesto FROM " . $this->table_impuesto . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Impuesto='" . $row['No_Impuesto'] . "' LIMIT 1")->row()->ID_Impuesto;
			}
			
        	if ( !empty($row['No_Marca']) ) {
	       		if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Marca='" . $row['No_Marca'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Marca = $this->db->query("SELECT ID_Marca FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Marca='" . $row['No_Marca'] . "' LIMIT 1")->row()->ID_Marca;
        	}
        	
        	if ( !empty($row['No_Unidad_Medida']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_unidad_medida . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Unidad_Medida='" . $row['No_Unidad_Medida'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Unidad_Medida = $this->db->query("SELECT ID_Unidad_Medida FROM " . $this->table_unidad_medida . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Unidad_Medida='" . $row['No_Unidad_Medida'] . "' LIMIT 1")->row()->ID_Unidad_Medida;
        	}
        	
        	if ( !empty($row['No_Familia']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Familia='" . $row['No_Familia'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Familia = $this->db->query("SELECT ID_Familia FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Familia= '" . $row['No_Familia'] . "' LIMIT 1")->row()->ID_Familia;
        	}
        	
        	if ( !empty($row['No_Sub_Familia']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Sub_Familia='" . $row['No_Sub_Familia'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Sub_Familia = $this->db->query("SELECT ID_Sub_Familia FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND No_Sub_Familia= '" . $row['No_Sub_Familia'] . "' LIMIT 1")->row()->ID_Sub_Familia;
        	}
        	
        	if ( !empty($row['Nu_Codigo_Producto_Sunat']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Catalogo_Producto_Sunat' AND Nu_Valor='" . $row['Nu_Codigo_Producto_Sunat'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Producto_Sunat = $this->db->query("SELECT ID_Tabla_Dato FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Catalogo_Producto_Sunat' AND Nu_Valor='" . $row['Nu_Codigo_Producto_Sunat'] . "' LIMIT 1")->row()->ID_Tabla_Dato;
        	}
        	
        	if ( !empty($row['iTipoLavado']) ) {
				if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Tipos_PedidoLavado' AND Nu_Valor='" . $row['iTipoLavado'] . "' LIMIT 1")->row()->existe > 0)
					$iIdTipoLavado = $this->db->query("SELECT Nu_Valor FROM " . $this->table_tabla_dato . " WHERE No_Relacion='Tipos_PedidoLavado' AND Nu_Valor='" . $row['iTipoLavado'] . "' LIMIT 1")->row()->Nu_Valor;
			}

        	if ( !empty($row['No_Familia_Marketplace']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Familia='" . $row['No_Familia_Marketplace'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Familia_Marketplace = $this->db->query("SELECT ID_Familia FROM " . $this->table_familia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Familia= '" . $row['No_Familia_Marketplace'] . "' LIMIT 1")->row()->ID_Familia;
        	}
        	
        	if ( !empty($row['No_Sub_Familia_Marketplace']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Sub_Familia='" . $row['No_Sub_Familia_Marketplace'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Sub_Familia_Marketplace = $this->db->query("SELECT ID_Sub_Familia FROM " . $this->table_subfamilia . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Sub_Familia= '" . $row['No_Sub_Familia_Marketplace'] . "' LIMIT 1")->row()->ID_Sub_Familia;
			}
        	
        	if ( !empty($row['No_Marca_Marketplace']) ) {
	        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Marca='" . $row['No_Marca_Marketplace'] . "' LIMIT 1")->row()->existe > 0)
	        		$ID_Marca_Marketplace = $this->db->query("SELECT ID_Marca FROM " . $this->table_marca . " WHERE ID_Empresa = " . $this->empresa->ID_Empresa_Marketplace . " AND No_Marca= '" . $row['No_Marca_Marketplace'] . "' LIMIT 1")->row()->ID_Marca;
			}
			
        	if ($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa=" . $ID_Empresa . " AND Nu_Codigo_Barra='" . $row['Nu_Codigo_Barra'] . "' LIMIT 1")->row()->existe == 0){
            	$_arrProducto = array(
					'ID_Empresa' => $ID_Empresa,
					'Nu_Tipo_Producto' => $row['Nu_Tipo_Producto'],
					'ID_Tipo_Producto' => $row['ID_Tipo_Producto'],
					'Nu_Codigo_Barra' => $row['Nu_Codigo_Barra'],
					'No_Codigo_Interno' => $row['Nu_Codigo_Producto'],
					'No_Producto' => $row['No_Producto'],
					'ID_Impuesto' => $ID_Impuesto,
					'ID_Ubicacion_Inventario' => $ID_Ubicacion_Inventario,
					'ID_Unidad_Medida' => $ID_Unidad_Medida,
					'ID_Producto_Sunat' => $ID_Producto_Sunat,
					'Nu_Compuesto' => 0,
					'Qt_CO2_Producto' => $row['Qt_CO2_Producto'],
					'Ss_Precio' => $row['fPrecio'],
					'Ss_Costo' => $row['fCosto'],
					'Nu_Stock_Minimo' => $row['iStockMinimo'],
					'ID_Tipo_Pedido_Lavado' => $iIdTipoLavado,
					'Nu_Estado' => (($row['iEstado'] != 0 || $row['iEstado'] == '' || $row['iEstado'] == NULL ) ? 1 : 0),
					'Ss_Precio_Ecommerce_Online_Regular' => $row['Ss_Precio_Ecommerce_Online_Regular'],
					'Ss_Precio_Ecommerce_Online' => $row['Ss_Precio_Ecommerce_Online'],
            	);
				if ( !empty($ID_Marca) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Marca" => $ID_Marca));
				if ( !empty($ID_Familia) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Familia" => $ID_Familia));
				if ( !empty($ID_Sub_Familia) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Sub_Familia" => $ID_Sub_Familia));
				if ( !empty($ID_Familia_Marketplace) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Familia_Marketplace" => $ID_Familia_Marketplace));
				if ( !empty($ID_Sub_Familia_Marketplace) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Sub_Familia_Marketplace" => $ID_Sub_Familia_Marketplace));
				if ( !empty($ID_Marca_Marketplace) )
					$_arrProducto = array_merge($_arrProducto, array("ID_Marca_Marketplace" => $ID_Marca_Marketplace));
				$arrProducto[] = $_arrProducto;
        	} else {
        		$ID_Producto = $this->db->query("SELECT ID_Producto FROM " . $this->table . " WHERE ID_Empresa=" . $ID_Empresa . " AND Nu_Codigo_Barra='" . $row['Nu_Codigo_Barra'] . "' LIMIT 1")->row()->ID_Producto;
        		$_arrProductoUPD = array(
					'ID_Producto' => $ID_Producto,
					'ID_Empresa' => $ID_Empresa,
					'Nu_Tipo_Producto' => $row['Nu_Tipo_Producto'],
					'ID_Tipo_Producto' => $row['ID_Tipo_Producto'],
					'Nu_Codigo_Barra' => $row['Nu_Codigo_Barra'],
					'No_Codigo_Interno' => $row['Nu_Codigo_Producto'],
					'No_Producto' => $row['No_Producto'],
					'ID_Impuesto' => $ID_Impuesto,
					'ID_Ubicacion_Inventario' => $ID_Ubicacion_Inventario,
					'ID_Unidad_Medida' => $ID_Unidad_Medida,
					'ID_Producto_Sunat' => $ID_Producto_Sunat,
					'Nu_Compuesto' => 0,
					'Qt_CO2_Producto' => $row['Qt_CO2_Producto'],
					'Ss_Precio' => $row['fPrecio'],
					'Ss_Costo' => $row['fCosto'],
					'Nu_Stock_Minimo' => $row['iStockMinimo'],
					'ID_Tipo_Pedido_Lavado' => $iIdTipoLavado,
					'Nu_Estado' => (($row['iEstado'] != 0 || $row['iEstado'] == '' || $row['iEstado'] == NULL ) ? 1 : 0),
					'Ss_Precio_Ecommerce_Online_Regular' => $row['Ss_Precio_Ecommerce_Online_Regular'],
					'Ss_Precio_Ecommerce_Online' => $row['Ss_Precio_Ecommerce_Online'],
            	);
				if ( !empty($ID_Marca) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Marca" => $ID_Marca));
				if ( !empty($ID_Familia) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Familia" => $ID_Familia));
				if ( !empty($ID_Sub_Familia) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Sub_Familia" => $ID_Sub_Familia));
				if ( !empty($ID_Familia_Marketplace) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Familia_Marketplace" => $ID_Familia_Marketplace));
				if ( !empty($ID_Sub_Familia_Marketplace) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Sub_Familia_Marketplace" => $ID_Sub_Familia_Marketplace));
				if ( !empty($ID_Marca_Marketplace) )
					$_arrProductoUPD = array_merge($_arrProductoUPD, array("ID_Marca_Marketplace" => $ID_Marca_Marketplace));
				$arrProductoUPD[] = $_arrProductoUPD;
        	}
        }

        $bStatus=false;
        if (isset($arrProducto) && is_array($arrProducto))
    		$this->db->insert_batch($this->table, $arrProducto);
    		if ($this->db->affected_rows() > 0)
    			$bStatus = true;
    	if (isset($arrProductoUPD) && is_array($arrProductoUPD))
    		$this->db->update_batch($this->table, $arrProductoUPD, 'ID_Producto');
    		if ($this->db->affected_rows() > 0)
    			$bStatus = true;

    	unset($arrProducto);
    	unset($arrProductoUPD);
    	
    	return $bStatus;
    }
	
	public function _get_datatables_query(){
        if( $this->input->post('Filtros_Productos') == 'Cliente' ){
            $this->db->like('CLI.No_Entidad', $this->input->post('Global_Filter'));
		}
		
        if( $this->input->post('Filtros_Productos') == 'Placa' ){
            $this->db->like('PRO.No_Placa_Vehiculo', $this->input->post('Global_Filter'));
		}
		
        if( $this->input->post('Filtros_Productos') == 'Year' ){
            $this->db->like('PRO.No_Year_Vehiculo', $this->input->post('Global_Filter'));
		}
		
        if( $this->input->post('Filtros_Productos') == 'Marca' ){
            $this->db->like('PRO.No_Marca_Vehiculo', $this->input->post('Global_Filter'));
		}
		
        if( $this->input->post('Filtros_Productos') == 'Modelo' ){
            $this->db->like('PRO.No_Modelo_Vehiculo', $this->input->post('Global_Filter'));
		}
		
		$this->db->select('PRO.ID_Empresa, PRO.ID_Producto, CLI.ID_Entidad, CLI.No_Entidad, PRO.No_Placa_Vehiculo, PRO.No_Year_Vehiculo, PRO.No_Marca_Vehiculo, PRO.No_Modelo_Vehiculo, PRO.No_Tipo_Combustible_Vehiculo, PRO.No_Vin_Vehiculo, PRO.No_Motor, PRO.Nu_Kilometraje, PRO.No_Color_Vehiculo, STOCK.Qt_Producto AS Qt_Producto, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado')
		->from($this->table . ' AS PRO')
		->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = PRO.ID_Entidad', 'left')
		->join($this->table_stock_producto . ' AS STOCK', 'STOCK.ID_Organizacion = ' . $this->empresa->ID_Organizacion . ' AND STOCK.ID_Almacen = ' . $this->empresa->ID_Almacen . ' AND STOCK.ID_Producto = PRO.ID_Producto', 'left')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = PRO.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_Estados"', 'join')
		->where('PRO.ID_Empresa', $this->empresa->ID_Empresa)
		->where('PRO.Nu_Tipo_Producto', 3);
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table)->where('ID_Tipo_Producto', 3);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $query = "
SELECT
 ITEM.*,
 CLI.No_Entidad
FROM
 " . $this->table . " AS ITEM
 JOIN " . $this->table_entidad . " AS CLI ON(ITEM.ID_Entidad = CLI.ID_Entidad)
WHERE
 ITEM.ID_Producto = " . $ID . " LIMIT 1";
        return $this->db->query($query)->result();
    }
    
    public function agregarProducto($data){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa=".$data['ID_Empresa']." AND ID_Entidad='" . $data['ID_Entidad'] . "' AND No_Placa_Vehiculo='" . $data['No_Placa_Vehiculo'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'Ya existe la placa');
		} else {
			if ( $this->db->insert($this->table, $data) > 0 )
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al insertar');
		}
    }
    
    public function actualizarProducto($where, $data){
		if( $where['ID_Entidad'] != $data['ID_Entidad'] && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Empresa =".$data['ID_Empresa']." AND ID_Entidad='" . $data['ID_Entidad'] . "' AND No_Placa_Vehiculo='" . $data['No_Placa_Vehiculo'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		} else {
			if ( $this->db->update($this->table, $data, $where) > 0)
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al actualizar');
		}
    }
    
	public function eliminarProducto($ID){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_stock_producto . " WHERE ID_Producto=" . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El vehiculo tiene movimientos (s)');
		}else{
			$this->db->where('ID_Producto', $ID);
            $this->db->delete($this->table);
		    if ( $this->db->affected_rows() > 0 )
		        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
	}
}