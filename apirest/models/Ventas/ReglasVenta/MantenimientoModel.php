<?php
class MantenimientoModel extends CI_Model{
    var $table = 'tabla_dato';
    var $table_stock_producto = 'documento_cabecera';
    var $column_order = array('No_Descripcion');
    var $column_search = array();
    var $order = array('ID_Tabla_Dato' => 'asc');

	public function __construct(){
		parent::__construct();
	}

    public function _get_datatables_query(){
        if( $this->input->post('Filtros_Productos') == 'Nombre' ){
            $this->db->like('No_Descripcion', $this->input->post('Global_Filter'));
        }

		$this->db->select('No_Descripcion, ID_Tabla_Dato, Nu_Valor')
            ->from($this->table)
            ->where('No_Relacion', 'Tipos_Mantenimiento');
        if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all(){
        $this->db->from($this->table)->where('No_Relacion', 'Tipos_Mantenimiento');
        return $this->db->count_all_results();
    }

    public function get_by_id($ID){
        $query = "
            SELECT
                *
            FROM
                " . $this->table . "
            WHERE
                ID_Tabla_Dato = " . $ID . " LIMIT 1";
        return $this->db->query($query)->result();
    }

    public function actualizarProducto($where, $data){
		if( $where['ID_Tabla_Dato'] != $data['ID_Tabla_Dato'] && $this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE ID_Tabla_Dato='" . $data['ID_Tabla_Dato'] . "' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		} else {
			if ( $this->db->update($this->table, $data, $where) > 0)
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al actualizar');
		}
    }

    public function agregarProducto($data){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table . " WHERE No_Descripcion='".$data['No_Descripcion']."' LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El registro ya existe');
		} else {
            $lastRow = $this->db->query("SELECT * FROM tabla_dato WHERE No_Relacion = 'Tipos_Mantenimiento' ORDER BY ID_Tabla_Dato DESC LIMIT 1")->row();
            $data['Nu_Valor'] = intval(@$lastRow->Nu_Valor) + 1;
			if ( $this->db->insert($this->table, $data) > 0 )
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado');
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Problemas al insertar');
		}
    }

    public function eliminarProducto($ID){
		if($this->db->query("SELECT COUNT(*) AS existe FROM " . $this->table_stock_producto . " WHERE Nu_Tipo_Mantenimiento=" . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'El tipo de mantenimiento tiene presupuesto(s)');
		}else{
            $this->db->where('No_Relacion', 'Tipos_Mantenimiento');
			$this->db->where('Nu_Valor', $ID);
            $this->db->delete($this->table);
		    if ( $this->db->affected_rows() > 0 )
		        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
		}
        return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
	}
}