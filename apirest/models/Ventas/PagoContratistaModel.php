<?php
class PagoContratistaModel extends CI_Model{
	var $table                      = 'documento_cabecera';
	var $table_documento_detalle = 'documento_detalle';
	var $table_producto = 'producto';
	var $table_familia = 'familia';
	var $table_empresa              = 'empresa';
	var $table_organizacion	        = 'organizacion';
	var $table_almacen		        = 'almacen';
	var $table_serie_documento = 'serie_documento';
	var $table_tipo_documento	    = 'tipo_documento';
	var $table_entidad = 'entidad';
	var $table_tabla_dato           = 'tabla_dato';
	
    var $column_order = array('No_Almacen', 'Fe_Emision', 'No_Tipo_Documento_Breve', 'ID_Serie_Documento', 'ID_Numero_Documento', 'Ss_Total');
    var $column_search = array('');
    var $order = array('ID_Documento_Cabecera' => 'desc');
	
	public function __construct(){ 
		parent::__construct();
	}
	
	public function _get_datatables_query(){        
		$this->db->select('VC.Nu_Estado, ALMA.No_Almacen, VC.Fe_Emision, VC.ID_Tipo_Documento, TDOCU.No_Tipo_Documento_Breve, VC.ID_Serie_Documento, VC.ID_Numero_Documento, CLI.No_Entidad, VC.ID_Documento_Cabecera, VC.Ss_Total, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, F.ID_Familia, F.No_Familia, VC.Qt_Producto')
		->from($this->table  .' AS VC')
		->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = VC.ID_Entidad', 'join')
		->join($this->table_serie_documento . ' AS SD', 'SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK', 'left')
        ->join($this->table_empresa . ' AS EMP', 'EMP.ID_Empresa = VC.ID_Empresa', 'join')
		->join($this->table_familia . ' AS F', 'F.ID_Familia=VC.ID_Familia', 'left')
		->join($this->table_organizacion . ' AS ORG', 'ORG.ID_Organizacion = VC.ID_Organizacion', 'join')
		->join($this->table_almacen . ' AS ALMA', 'ALMA.ID_Organizacion = VC.ID_Organizacion AND ALMA.ID_Almacen = VC.ID_Almacen', 'join')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
    	->where('VC.ID_Tipo_Documento', 25)
    	->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
		
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id_modificar($ID){
        $query = "SELECT DISTINCT
VC.ID_Empresa,
VC.ID_Organizacion,
VC.ID_Almacen,
VC.ID_Documento_Cabecera,
VC.Nu_Estado,
VC.ID_Tipo_Documento,
VC.ID_Serie_Documento,
SD.Nu_Cantidad_Caracteres,
VC.ID_Numero_Documento,
VC.Fe_Emision,
CLI.ID_Tipo_Documento_Identidad,
CLI.ID_Entidad,
CLI.No_Entidad,
CLI.Nu_Documento_Identidad,
CLI.Txt_Direccion_Entidad,
PRO.ID_Producto,
PRO.Nu_Codigo_Barra,
PRO.No_Producto,


/* M036 - I */
/* QUERY CAMPO OI - REPORTE PAGO CONTRATISTA */ 
(SELECT CONCAT(TD.No_Tipo_Documento,' - ',SD.ID_Serie_Documento ,' - ',OI.ID_Numero_Documento) 
FROM
relacion_tabla AS RT
JOIN orden_ingreso AS OI ON(OI.ID_Orden_Ingreso = RT.ID_Relacion_Enlace_Tabla)
JOIN orden_ingreso_etapa AS OIE ON(OIE.ID_Etapa = OI.ID_Etapa)
JOIN tipo_documento AS TD ON(TD.ID_Tipo_Documento = OI.ID_Area_Ingreso)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK = OI.ID_Serie_Documento)
JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = OI.Nu_Estado 
AND TDESTADO.No_Relacion = 'Tipos_EstadoOrdenIngreso')
WHERE RT.Nu_Relacion_Datos = 6
AND RT.ID_Origen_Tabla =OVC.ID_Documento_Cabecera LIMIT 1 
) AS OI,
CAR.No_Placa_Vehiculo,
CAR.No_Marca_Vehiculo,
CAR.No_Modelo_Vehiculo,
CAR.No_Color_Vehiculo,
/* M036 - F */


VD.Ss_Precio,
VD.Qt_Producto,
VD.ID_Impuesto_Cruce_Documento,
VD.Ss_SubTotal AS Ss_SubTotal_Producto,
VD.Ss_Impuesto AS Ss_Impuesto_Producto,
ROUND(VD.Ss_Descuento, 2) AS Ss_Descuento_Producto,
ROUND(VD.Ss_Descuento_Impuesto, 2) AS Ss_Descuento_Impuesto_Producto,
ROUND(VD.Po_Descuento, 2) AS Po_Descuento_Impuesto_Producto,
ROUND(VD.Ss_Total, 2) AS Ss_Total_Producto,
VD.Txt_Nota AS Txt_Nota_Detalle,
ICDOCU.Ss_Impuesto,
IMP.Nu_Tipo_Impuesto,
IMP.No_Impuesto_Breve,
VC.Txt_Glosa,
TDOCU.No_Tipo_Documento_Breve,
TDOCUIDEN.No_Tipo_Documento_Identidad_Breve,
VD.ID_OI_Detalle
FROM
documento_cabecera AS VC
JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
JOIN documento_detalle AS VD ON(VD.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)

/*  M036 - I */
JOIN documento_cabecera AS OVC ON(OVC.ID_Documento_Cabecera = VD.ID_OI_Detalle)
JOIN producto AS CAR ON(CAR.ID_Producto = OVC.ID_Producto)
/* M036 - F  */

JOIN tipo_documento AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
JOIN impuesto_cruce_documento AS ICDOCU ON(ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
JOIN impuesto AS IMP ON(IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
WHERE VC.ID_Documento_Cabecera=" . $ID;
        return $this->db->query($query)->result();
	}

    public function get_by_id($ID){
        $query = "SELECT DISTINCT
VC.ID_Empresa,
VC.ID_Organizacion,
VC.ID_Almacen,
VC.ID_Documento_Cabecera,
VC.ID_Tipo_Documento,
VC.ID_Serie_Documento,
SD.Nu_Cantidad_Caracteres,
VC.ID_Numero_Documento,
TDOCU.No_Tipo_Documento_Breve,
CLI.ID_Tipo_Documento_Identidad,
CLI.ID_Entidad,
CLI.No_Entidad, 
CLI.Nu_Documento_Identidad,
CLI.Txt_Direccion_Entidad,
TDOCUIDEN.No_Tipo_Documento_Identidad_Breve,
MONE.No_Moneda,
MONE.No_Signo,
VD.Qt_Producto, 
/* M040 - I */
VD.Ss_Precio,
OI.Fe_Entrega_Tentativa, 
/* M040 - F */
VD.Ss_Total AS Ss_Total_Detalle,
( 
	SELECT SUM(VDD.Ss_SubTotal) 
			FROM documento_cabecera AS VCC 
			LEFT JOIN documento_detalle  AS VDD ON(VCC.ID_Documento_Cabecera = VDD.ID_Documento_Cabecera) 
			LEFT JOIN producto AS PROD ON(PROD.ID_Producto = VDD.ID_Producto) 
			LEFT JOIN familia AS FAM ON(PROD.ID_Familia = FAM.ID_Familia) 
			where VCC.id_documento_cabecera = OVC.ID_Documento_Cabecera 
			AND FAM.ID_Familia=  VC.ID_Familia 
) AS Precio_Cliente , 
VC.Ss_Total,
CAR.No_Placa_Vehiculo,
CAR.No_Marca_Vehiculo,
CAR.No_Modelo_Vehiculo,
CAR.No_Color_Vehiculo,
OVC.Fe_Emision,
/* REP_CONTRATISTA_FIX - I */
OVC.ID_Numero_Documento as Nro_Correl_Presupuesto,
/* REP_CONTRATISTA_FIX - F */
OVC.ID_Documento_Cabecera AS ID_Documento_Cabecera_OV

FROM
documento_cabecera AS VC
JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
JOIN tipo_documento AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
JOIN serie_documento AS SD ON(SD.ID_Serie_Documento_PK=VC.ID_Serie_Documento_PK)
JOIN entidad AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
JOIN documento_detalle AS VD ON(VD.ID_Documento_Cabecera = VC.ID_Documento_Cabecera)
JOIN documento_cabecera AS OVC ON(OVC.ID_Documento_Cabecera = VD.ID_OI_Detalle)
JOIN producto AS CAR ON(CAR.ID_Producto = OVC.ID_Producto) 

/* M040 - I */
JOIN relacion_tabla RT ON RT.Nu_Relacion_Datos = 6 AND RT.ID_Origen_Tabla =OVC.ID_Documento_Cabecera 
JOIN orden_ingreso AS OI ON(OI.ID_Orden_Ingreso = RT.ID_Relacion_Enlace_Tabla)
/* M040 - F */

WHERE VC.ID_Documento_Cabecera=" . $ID;
        return $this->db->query($query)->result();
	}
	
	public function generarLiquidacion($arrPost){
		$this->db->trans_begin();
		
		$iIdFirst = 0;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			$iIdFirst = $key;
			break;
		}
$sql = "SELECT
ID_Empresa,
ID_Organizacion,
ID_Almacen,
ID_Entidad,
ID_Moneda,
ID_Medio_Pago,
Ss_Total
FROM
documento_cabecera
WHERE ID_Documento_Cabecera = " . $iIdFirst .  " LIMIT 1";
		$arrHeader = $this->db->query($sql)->result();
		$ID_Empresa = $arrHeader[0]->ID_Empresa;
		$ID_Organizacion = $arrHeader[0]->ID_Organizacion;
		$ID_Almacen = $arrHeader[0]->ID_Almacen;
		$ID_Entidad = $arrHeader[0]->ID_Entidad;
		$ID_Moneda = $arrHeader[0]->ID_Moneda;
		$ID_Medio_Pago = $arrHeader[0]->ID_Medio_Pago;
		$Ss_Total = $arrHeader[0]->Ss_Total;

		$query = "SELECT
ID_Serie_Documento_PK,
ID_Serie_Documento,
Nu_Numero_Documento
FROM
serie_documento
WHERE
ID_Empresa=" . $ID_Empresa . "
AND ID_Organizacion=" . $ID_Organizacion . "
AND ID_Almacen=" . $ID_Almacen . "
AND ID_Tipo_Documento=3
AND Nu_Estado=1
AND ID_POS IS NULL
LIMIT 1";
        $arrSerieDocumento = $this->db->query($query)->row();
        
		$arrHeader = array(
			'ID_Empresa'				=> $ID_Empresa,
			'ID_Organizacion'			=> $ID_Organizacion,
			'ID_Almacen'			    => $ID_Almacen,
			'ID_Entidad'				=> $ID_Entidad,
			'ID_Tipo_Asiento'			=> 1,//Venta
			'ID_Tipo_Documento'			=> 3,//Factura        
            'ID_Serie_Documento_PK'		=> $arrSerieDocumento->ID_Serie_Documento_PK,
            'ID_Serie_Documento'		=> $arrSerieDocumento->ID_Serie_Documento,
            'ID_Numero_Documento'		=> $arrSerieDocumento->Nu_Numero_Documento,                
			'Fe_Emision'				=> dateNow('fecha'),
			'Fe_Emision_Hora'			=> dateNow('fecha_hora'),
			'ID_Moneda'					=> $ID_Moneda,//Soles
			'ID_Medio_Pago'				=> $ID_Medio_Pago,
			'Fe_Vencimiento'			=> dateNow('fecha'),
			'Fe_Periodo' => dateNow('fecha'),
			'Nu_Descargar_Inventario' => 0,
			'Ss_Total' => $Ss_Total,
			'Ss_Total_Saldo' => 0.00,
			'Ss_Vuelto' => 0.00,
			'Nu_Correlativo' => 0,
			'Nu_Estado' => 6,//Pendiente
			'Nu_Transporte_Lavanderia_Hoy' => 0,
			'Nu_Estado_Lavado' => 0,
			'Fe_Entrega' => dateNow('fecha'),
			'Nu_Tipo_Recepcion' => 0,
			'ID_Transporte_Delivery' => 0,
			'Txt_Direccion_Delivery' => '-',
			'Txt_Glosa' => '',
			'No_Formato_PDF' => 'A4',
		);

		$this->db->insert('documento_cabecera', $arrHeader);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();

        $fTotalHeader = 0.00;
		foreach ($arrPost['arrIdDocumentoCabecera'] as $key => $value){
			//Cambiar estado a facturado presupuesto orden de venta
			$query_presupuesto = "SELECT ID_Origen_Tabla FROM relacion_tabla WHERE ID_Relacion_Enlace_Tabla = " . $key . " AND Nu_Relacion_Datos=4";
			$arrPresupuesto = $this->db->query($query_presupuesto)->result();
			foreach ($arrPresupuesto as $row){
				$sql = "UPDATE documento_cabecera SET Nu_Estado=21 WHERE ID_Documento_Cabecera=" . $row->ID_Origen_Tabla;//21 = Facturado
				$this->db->query($sql);
				
				//Cambiar estado a Orden de ingreso a Facturado
				$query_orden_ingreso = "SELECT ID_Origen_Tabla FROM relacion_tabla WHERE ID_Relacion_Enlace_Tabla = " . $row->ID_Origen_Tabla . " AND Nu_Relacion_Datos=1";
				$arrOrdenIngreso = $this->db->query($query_orden_ingreso)->result();
				foreach ($arrOrdenIngreso as $row2){
					$sql = "UPDATE orden_ingreso SET Nu_Estado=3 WHERE ID_Orden_Ingreso=" . $row2->ID_Origen_Tabla;//3 = Facturado
					$this->db->query($sql);
				}
			}

			//Cambiar estado a liquidación
		    $sql = "UPDATE documento_cabecera SET Nu_Estado=21 WHERE ID_Documento_Cabecera=" . $key;//21 = Facturado
            $this->db->query($sql);

            $query_detalle = "SELECT ID_Empresa,ID_Producto,ID_Impuesto_Cruce_Documento FROM documento_detalle WHERE ID_Documento_Cabecera = " . $key . " LIMIT 1";
            $arrDetalle = $this->db->query($query_detalle)->result();
            $ID_Empresa = $arrDetalle[0]->ID_Empresa;
            $ID_Producto = $arrDetalle[0]->ID_Producto;
            $ID_Impuesto_Cruce_Documento = $arrDetalle[0]->ID_Impuesto_Cruce_Documento;

            $sql = "SELECT SUM(Ss_SubTotal) AS Ss_SubTotal, SUM(Ss_Total) AS Ss_Total FROM documento_detalle WHERE ID_Documento_Cabecera = " . $key;
            $arrPresupuestoTotales = $this->db->query($sql)->result();
            $Ss_Total = $arrPresupuestoTotales[0]->Ss_Total;
            $Ss_SubTotal = $arrPresupuestoTotales[0]->Ss_SubTotal;

            $documento_detalle[] = array(
                'ID_Empresa' => $ID_Empresa,
                'ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera,
                'ID_Producto' => $ID_Producto,
                'Qt_Producto' => 1,
                'Ss_Precio' => $Ss_Total,
                'Ss_SubTotal' => $Ss_SubTotal,
                'Ss_Descuento' => 0,
                'Ss_Descuento_Impuesto' => 0,
                'Po_Descuento' => 0,
                'Txt_Nota' => "LIQ " . $key,
                'ID_Impuesto_Cruce_Documento' => $ID_Impuesto_Cruce_Documento,
                'Ss_Impuesto' => ($Ss_SubTotal - $Ss_Total),
                'Ss_Total' => $Ss_Total,
                'Nu_Estado_Lavado' => 0,
            );

            $arrRelacionTabla = array(
                'ID_Empresa' => $ID_Empresa,
                'Nu_Relacion_Datos' => 5,//Relación Liquidacion a Factura
                'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
                'ID_Origen_Tabla' => $key,
            );
            $this->db->insert('relacion_tabla', $arrRelacionTabla);

            $fTotalHeader += $Ss_Total;
        }

		$this->db->insert_batch('documento_detalle', $documento_detalle);

		$sql = "UPDATE documento_cabecera SET Ss_Total = " . $fTotalHeader . " WHERE ID_Documento_Cabecera=" . $Last_ID_Documento_Cabecera;
        $this->db->query($sql);
        
		

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('sStatus' => 'danger', 'sMessage' => 'Error al liquidar');
		} else {
			$this->db->trans_commit();
			$sql = "UPDATE serie_documento SET Nu_Numero_Documento = Nu_Numero_Documento + 1
					WHERE
						ID_Empresa=" . $ID_Empresa . "
						AND ID_Organizacion=" . $ID_Organizacion . "
						AND ID_Almacen=" . $ID_Almacen . "
						AND ID_Tipo_Documento=3
						AND Nu_Estado=1
						AND ID_POS IS NULL";
			$this->db->query($sql);
			return array('sStatus' => 'success', 'sMessage' => 'Factura generada correctamente');
		}
	}
    
    public function actualizarPagoContratista($where, $arrCabecera, $arrDetalle){
		$this->db->trans_begin();
		
		$this->db->query("SET FOREIGN_KEY_CHECKS=OFF;");

		$this->db->delete($this->table_documento_detalle, $where);

		$Qt_Total = 0.00;
		$Ss_Total = 0.00;
		if ( !empty($arrDetalle) ) {
			foreach ($arrDetalle as $row) {
				$documento_detalle[] = array(
					'ID_Empresa'					=> $this->user->ID_Empresa,
					'ID_Documento_Cabecera'			=> $where['ID_Documento_Cabecera'],
					'ID_Producto'					=> $this->security->xss_clean($row['ID_Producto']),
					'Qt_Producto'					=> $this->security->xss_clean($row['Qt_Producto']),
					'Ss_Precio'						=> $this->security->xss_clean($row['Ss_Precio']),
					'Ss_SubTotal' 					=> $this->security->xss_clean($row['Ss_SubTotal']),
					'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
					'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
					'Po_Descuento' => $row['Ss_Descuento'],
					'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
					'Ss_Impuesto' 					=> $this->security->xss_clean($row['Ss_Impuesto']),
					'Ss_Total' 						=> round($this->security->xss_clean($row['Ss_Total']), 2),
					'ID_OI_Detalle' => $this->security->xss_clean($row['ID_OI_Detalle']),
				);
				$Qt_Total+=$this->security->xss_clean($row['Qt_Producto']);
				$Ss_Total+=$this->security->xss_clean($row['Ss_Total']);
			}
			$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
		}

		$data = array(
			'Qt_Producto' => $Qt_Total,
			'Ss_Total' => $Ss_Total,
		);
		$where = array('ID_Documento_Cabecera' => $where['ID_Documento_Cabecera']);
		$this->db->update('documento_cabecera', $data, $where);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
        } else {
			$this->db->query("SET FOREIGN_KEY_CHECKS=ON;");
            $this->db->trans_commit();
	        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado', 'Last_ID_Documento_Cabecera' => $where['ID_Documento_Cabecera']);
        }
    }
}
