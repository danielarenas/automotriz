<?php
class OrdenTrabajoModel extends CI_Model{
	var $table          				= 'documento_cabecera';
	var $table_documento_detalle		= 'documento_detalle';
	var $table_documento_enlace			= 'documento_enlace';
	var $table_tipo_documento			= 'tipo_documento';
	var $table_serie_documento			= 'serie_documento';
	var $table_impuesto_cruce_documento	= 'impuesto_cruce_documento';
	var $table_entidad					= 'entidad';
	var $table_tipo_documento_identidad	= 'tipo_documento_identidad';
	var $table_moneda					= 'moneda';
	var $table_organizacion				= 'organizacion';
	var $table_tabla_dato				= 'tabla_dato';
	var $table_relacion_tabla= 'relacion_tabla';
	
    var $column_order = array('');
    var $column_search = array('');
    var $order = array('');
    
	public function __construct(){
		parent::__construct();
	}
	
	public function _get_datatables_query(){
    	$this->db->where("VC.Fe_Emision BETWEEN '" . $this->input->post('Filtro_Fe_Inicio') . "' AND '" . $this->input->post('Filtro_Fe_Fin') . "'");
    	
        if($this->input->post('Filtro_NumeroDocumento')){
        	$this->db->where('VC.ID_Documento_Cabecera', $this->input->post('Filtro_NumeroDocumento'));
		}
		
        if($this->input->post('Filtro_NumeroDocumento_Presupuesto')){
        	$this->db->where('VC2.ID_Numero_Documento', $this->input->post('Filtro_NumeroDocumento_Presupuesto'));
        }
		
        if($this->input->post('Filtro_Estado') != ''){
        	$this->db->where('VC.Nu_Estado', $this->input->post('Filtro_Estado'));
        }
        
        if($this->input->post('Filtro_Contacto')){
        	$this->db->where('CONTAC.No_Entidad', $this->input->post('Filtro_Contacto'));
        }

        if($this->input->post('Filtro_Entidad')){
        	$this->db->where('CLI.No_Entidad', $this->input->post('Filtro_Entidad'));
        }
        
        $this->db->select('VC.ID_Documento_Cabecera, VC.ID_Documento_Cabecera, VC.Fe_Emision, TDOCUIDEN.No_Tipo_Documento_Identidad_Breve, CLI.No_Entidad, CONTAC.No_Entidad AS No_Contacto, CONTAC.Txt_Email_Entidad AS Txt_Email_Contacto, MONE.No_Signo, VC.Ss_Total, VC.Nu_Estado, TDESTADO.No_Class AS No_Class_Estado, TDESTADO.No_Descripcion AS No_Descripcion_Estado, TDOCU.Nu_Enlace, VC.Nu_Descargar_Inventario')
		->from($this->table . ' AS VC')
		->join($this->table_tipo_documento . ' AS TDOCU', 'TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento', 'join')
		->join($this->table_entidad . ' AS CLI', 'CLI.ID_Entidad = VC.ID_Entidad', 'join')
		->join($this->table_entidad . ' AS CONTAC', 'CONTAC.ID_Entidad = VC.ID_Contacto', 'left')
		->join($this->table_relacion_tabla . ' AS RT', 'RT.Nu_Relacion_Datos=2 AND RT.ID_Relacion_Enlace_Tabla = VC.ID_Documento_Cabecera', 'left')
		->join($this->table . ' AS VC2', 'VC2.ID_Documento_Cabecera = RT.ID_Origen_Tabla', 'left')
		->join($this->table_tipo_documento_identidad . ' AS TDOCUIDEN', 'TDOCUIDEN.ID_Tipo_Documento_Identidad = CLI.ID_Tipo_Documento_Identidad', 'join')
		->join($this->table_moneda . ' AS MONE', 'MONE.ID_Moneda = VC.ID_Moneda', 'join')
    	->join($this->table_tabla_dato . ' AS TDESTADO', 'TDESTADO.Nu_Valor = VC.Nu_Estado AND TDESTADO.No_Relacion = "Tipos_EstadoDocumento"', 'join')
		->where('VC.ID_Empresa', $this->empresa->ID_Empresa)
		->where('VC.ID_Organizacion', $this->empresa->ID_Organizacion)
    	->where('VC.ID_Tipo_Asiento', 1)
    	->where('VC.ID_Tipo_Documento', 20);//20 = OT
					
		if(isset($_POST['order']))
        	$this->db->order_by( 'VC.Fe_Emision DESC, VC.ID_Documento_Cabecera DESC' );
        else if(isset($this->order))
        	$this->db->order_by( 'VC.Fe_Emision DESC, VC.ID_Documento_Cabecera DESC' );
    }
	
	function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    public function get_by_id($ID){
        $query = "SELECT
 VC.ID_Empresa,
 VC.ID_Organizacion,
 VC.ID_Almacen,
 VC.ID_Documento_Cabecera,
 VC.Nu_Estado,
 VC.ID_Numero_Documento,
 CLI.ID_Tipo_Documento_Identidad,
 CLI.ID_Entidad,
 CLI.No_Entidad,
 CLI.Nu_Documento_Identidad,
 CLI.Txt_Direccion_Entidad,
 CLI.Nu_Telefono_Entidad,
 CLI.Nu_Celular_Entidad,
 VC.ID_Tipo_Documento,
 TDOCU.Nu_Impuesto,
 TDOCU.Nu_Enlace,
 VC.Fe_Emision,
 VC.Fe_Vencimiento,
 VC.Fe_Periodo,
 VC.ID_Moneda,
 VC.ID_Medio_Pago,
 VC.Nu_Descargar_Inventario,
 VC.ID_Lista_Precio_Cabecera,
 CONTAC.ID_Entidad AS ID_Contacto,
 CONTAC.ID_Tipo_Documento_Identidad AS ID_Tipo_Documento_Identidad_Contacto,
 CONTAC.Nu_Documento_Identidad AS Nu_Documento_Identidad_Contacto,
 CONTAC.No_Entidad AS No_Contacto,
 CONTAC.Txt_Email_Entidad AS Txt_Email_Contacto,
 CONTAC.Nu_Celular_Entidad AS Nu_Celular_Contacto,
 CONTAC.Nu_Telefono_Entidad AS Nu_Telefono_Contacto,
 VD.ID_Producto,
 PRO.Nu_Codigo_Barra,
 PRO.No_Producto,
 VD.Txt_Nota AS Txt_Nota_Detalle,
 ROUND(VD.Ss_Precio, 2) AS Ss_Precio,
 VD.Qt_Producto,
 VD.ID_Impuesto_Cruce_Documento,
 VD.Ss_SubTotal AS Ss_SubTotal_Producto,
 VD.Ss_Impuesto AS Ss_Impuesto_Producto,
 ROUND(VD.Ss_Descuento, 2) AS Ss_Descuento_Producto,
 ROUND(VD.Ss_Descuento_Impuesto, 2) AS Ss_Descuento_Impuesto_Producto,
 ROUND(VD.Po_Descuento, 2) AS Po_Descuento_Impuesto_Producto,
 ROUND(VD.Ss_Total, 2) AS Ss_Total_Producto,
 ICDOCU.Ss_Impuesto,
 MP.Nu_Tipo AS Nu_Tipo_Forma_Pago,
 IMP.Nu_Tipo_Impuesto,
 IMP.No_Impuesto_Breve,
 ICDOCU.Po_Impuesto,
 VC.Txt_Garantia,
 VC.Txt_Glosa,
 ROUND(VC.Ss_Descuento, 2) AS Ss_Descuento,
 ROUND(VC.Ss_Total, 2) AS Ss_Total,
 MONE.No_Moneda,
 MONE.No_Signo,
 VC.Po_Descuento,
 MP.No_Medio_Pago,
 VC.No_Formato_PDF,
 TDOCUIDEN.No_Tipo_Documento_Identidad_Breve,
 CAR.ID_Producto AS ID_Vehiculo,
 CAR.No_Placa_Vehiculo,
 CAR.No_Marca_Vehiculo,
 CAR.No_Modelo_Vehiculo,
 CAR.No_Color_Vehiculo,
 VC.No_Kilometraje,
 F.ID_Familia,
 F.No_Familia,
 VC.ID_Mesero,
 VC.ID_Transporte_Sede_Planta,
 VC.ID_Transporte_Delivery,
 VC.ID_Comision,
 AR.No_Entidad AS No_Asesor_Responsable,
 RPINTURA.No_Entidad AS No_Responsable_Pintura,
 RPLANCHA.No_Entidad AS No_Responsable_Planchado,
 AC.No_Entidad AS No_Asesor_Comision
FROM
 " . $this->table . " AS VC
 JOIN " . $this->table_documento_detalle . " AS VD ON(VC.ID_Documento_Cabecera = VD.ID_Documento_Cabecera)
 JOIN " . $this->table_entidad . " AS CLI ON(CLI.ID_Entidad = VC.ID_Entidad)
 JOIN tipo_documento_identidad AS TDOCUIDEN ON(CLI.ID_Tipo_Documento_Identidad = TDOCUIDEN.ID_Tipo_Documento_Identidad)
 LEFT JOIN " . $this->table_entidad . " AS CONTAC ON(CONTAC.ID_Entidad = VC.ID_Contacto)
 JOIN producto AS PRO ON(PRO.ID_Producto = VD.ID_Producto)
 JOIN " . $this->table_tipo_documento . " AS TDOCU ON(TDOCU.ID_Tipo_Documento = VC.ID_Tipo_Documento)
 JOIN " . $this->table_impuesto_cruce_documento . " AS ICDOCU ON(ICDOCU.ID_Impuesto_Cruce_Documento = VD.ID_Impuesto_Cruce_Documento)
 JOIN impuesto AS IMP ON(IMP.ID_Impuesto = ICDOCU.ID_Impuesto)
 JOIN medio_pago AS MP ON(MP.ID_Medio_Pago = VC.ID_Medio_Pago)
 JOIN moneda AS MONE ON(MONE.ID_Moneda = VC.ID_Moneda)
 JOIN producto AS CAR ON(CAR.ID_Producto = VC.ID_Producto)
 JOIN familia AS F ON(PRO.ID_Familia = F.ID_Familia)
 JOIN " . $this->table_entidad . " AS AR ON(AR.ID_Entidad = VC.ID_Mesero)
 LEFT JOIN " . $this->table_entidad . " AS RPINTURA ON(RPINTURA.ID_Entidad = VC.ID_Transporte_Sede_Planta)
 LEFT JOIN " . $this->table_entidad . " AS RPLANCHA ON(RPLANCHA.ID_Entidad = VC.ID_Transporte_Delivery)
 LEFT JOIN " . $this->table_entidad . " AS AC ON(AC.ID_Entidad = VC.ID_Comision)
WHERE VC.ID_Documento_Cabecera = " . $ID;
        return $this->db->query($query)->result();
	}
    
    public function agregarVenta($arrOrdenCabecera, $arrOrdenDetalle, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo){		
		$this->db->trans_begin();
		
		if (is_array($arrClienteNuevo)){
		    unset($arrOrdenCabecera['ID_Entidad']);
		    //Si no existe el cliente, lo crearemos
		    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
				$arrCliente = array(
	                'ID_Empresa'					=> $this->empresa->ID_Empresa,
	                'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 0,
	                'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
	                'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrCliente);
	    		$Last_ID_Entidad = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Entidad" => $Last_ID_Entidad));
		}
		
		if (is_array($arrContactoNuevo)){
		    unset($arrOrdenCabecera['ID_Contacto']);
		    //Si no existe el cliente, lo crearemos
		    if( $this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 8 AND ID_Tipo_Documento_Identidad = " . $arrContactoNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrContactoNuevo['Nu_Documento_Identidad'] . "' AND No_Entidad='".$arrContactoNuevo['No_Entidad']."' LIMIT 1")->row()->existe == 0){
				$arrContacto = array(
	                'ID_Empresa'					=> $this->empresa->ID_Empresa,
	                'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 8,//Contacto
	                'ID_Tipo_Documento_Identidad'	=> $arrContactoNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrContactoNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrContactoNuevo['No_Entidad'],
	                'Txt_Email_Entidad' 			=> $arrContactoNuevo['Txt_Email_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrContactoNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrContactoNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrContacto);
	    		$Last_ID_Contacto = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Contacto" => $Last_ID_Contacto));
		}
		
		$ID_Producto = $arrOrdenCabecera['ID_Producto'];
		if (is_array($arrVehiculoNuevo)){
		    unset($arrOrdenCabecera['ID_Producto']);			
			if($this->db->query("SELECT COUNT(*) AS existe FROM producto WHERE ID_Empresa=".$arrOrdenCabecera['ID_Empresa']." AND ID_Entidad='" . $ID_Entidad . "' AND No_Placa_Vehiculo='" . $arrVehiculoNuevo['No_Placa_Vehiculo'] . "' LIMIT 1")->row()->existe == 0){
		    	$arrVehiculoNuevo = array_merge($arrVehiculoNuevo, array("ID_Empresa" => $arrOrdenCabecera['ID_Empresa'], "ID_Entidad" => $ID_Entidad, "No_Producto" => $arrVehiculoNuevo['No_Placa_Vehiculo'], "Nu_Tipo_Producto" => 3, "ID_Tipo_Producto" => 2));
				$this->db->insert('producto', $arrVehiculoNuevo);
	    		$ID_Producto = $this->db->insert_id();
			} else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
			}
		}
		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Producto" => $ID_Producto));
		
		$ID_Origen_Tabla = $arrOrdenCabecera['ID_Origen_Tabla'];
		unset($arrOrdenCabecera['ID_Origen_Tabla']);
		$this->db->insert($this->table, $arrOrdenCabecera);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();
		
		if ( $ID_Origen_Tabla != 0 ) {
			$arrRelacionTabla = array(
				'ID_Empresa' => $arrOrdenCabecera['ID_Empresa'],
				'Nu_Relacion_Datos' => 2,//Relación de Presupuesto a OT
				'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
				'ID_Origen_Tabla' => $ID_Origen_Tabla,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);
		}

		foreach ($arrOrdenDetalle as $row) {
			$documento_detalle[] = array(
				'ID_Empresa'					=> $this->user->ID_Empresa,
				'ID_Documento_Cabecera'			=> $Last_ID_Documento_Cabecera,
				'ID_Producto'					=> $this->security->xss_clean($row['ID_Producto']),
				'Qt_Producto'					=> $this->security->xss_clean($row['Qt_Producto']),
				'Txt_Nota'					=> $this->security->xss_clean($row['Txt_Nota']),
				'Ss_Precio'						=> $this->security->xss_clean($row['Ss_Precio']),
				'Ss_SubTotal' 					=> $this->security->xss_clean($row['Ss_SubTotal']),
				'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
				'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
				'Po_Descuento' => $row['Ss_Descuento'],
				'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
				'Ss_Impuesto' 					=> $this->security->xss_clean($row['Ss_Impuesto']),
				'Ss_Total' 						=> round($this->security->xss_clean($row['Ss_Total']), 2),
			);
		}
		$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
			
    	$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al insertar');
        } else {
            $this->db->trans_commit();
            return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro guardado', 'Last_ID_Documento_Cabecera' => $Last_ID_Documento_Cabecera);
        }
    }
    
    public function actualizarVenta($where, $arrOrdenCabecera, $arrOrdenDetalle, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo){
		$this->db->trans_begin();
		
		$this->db->query("SET FOREIGN_KEY_CHECKS=OFF;");

		$arrDataModificar = $this->db->query("SELECT ID_Organizacion, ID_Documento_Cabecera, Nu_Descargar_Inventario FROM documento_cabecera WHERE ID_Empresa=" . $where['ID_Empresa'] . " AND ID_Documento_Cabecera=" . $where['ID_Documento_Cabecera'] . " LIMIT 1")->result();
	
		$ID_Documento_Cabecera = $arrDataModificar[0]->ID_Documento_Cabecera;
		$Nu_Descargar_Inventario = $arrDataModificar[0]->Nu_Descargar_Inventario;

		$this->db->delete($this->table_documento_detalle, $where);
		
    	if ($Nu_Descargar_Inventario == 1) {
	        $query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera = " . $ID_Documento_Cabecera;
	        $arrDetalle = $this->db->query($query)->result();
			foreach ($arrDetalle as $row) {
				if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
					$where_stock_producto = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
					$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
					
					$stock_producto = array(
						'ID_Producto'		=> $row->ID_Producto,
						'Qt_Producto'		=> ($Qt_Producto - round($row->Qt_Producto, 6)),
						'Ss_Costo_Promedio'	=> 0.00,
					);
					$this->db->update('stock_producto', $stock_producto, $where_stock_producto);
				}
        	}
			$this->db->where('ID_Documento_Cabecera', $ID_Documento_Cabecera);
	        $this->db->delete('movimiento_inventario');
		}

        $this->db->delete($this->table, $where);
		
		if (is_array($arrClienteNuevo)){
		    unset($arrOrdenCabecera['ID_Entidad']);
		    //Si no existe el cliente, lo crearemos
		    if($this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 0 AND ID_Tipo_Documento_Identidad = " . $arrClienteNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrClienteNuevo['Nu_Documento_Identidad'] . "' LIMIT 1")->row()->existe == 0){
				$arrCliente = array(
	                'ID_Empresa'					=> $this->user->ID_Empresa,
	                'ID_Organizacion'				=> $arrDataModificar[0]->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 0,
	                'ID_Tipo_Documento_Identidad'	=> $arrClienteNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrClienteNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrClienteNuevo['No_Entidad'],
	                'Txt_Direccion_Entidad' 		=> $arrClienteNuevo['Txt_Direccion_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrClienteNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrClienteNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrCliente);
	    		$Last_ID_Entidad = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El cliente ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Entidad" => $Last_ID_Entidad));
		}
		
		if (is_array($arrContactoNuevo)){
		    unset($arrOrdenCabecera['ID_Contacto']);
		    //Si no existe el cliente, lo crearemos
		    if( $this->db->query("SELECT COUNT(*) AS existe FROM entidad WHERE ID_Empresa = " . $this->user->ID_Empresa . " AND Nu_Tipo_Entidad = 8 AND ID_Tipo_Documento_Identidad = " . $arrContactoNuevo['ID_Tipo_Documento_Identidad'] . " AND Nu_Documento_Identidad = '" . $arrContactoNuevo['Nu_Documento_Identidad'] . "' AND No_Entidad='".$arrContactoNuevo['No_Entidad']."' LIMIT 1")->row()->existe == 0){
				$arrContacto = array(
	                'ID_Empresa'					=> $this->user->ID_Empresa,
	                'ID_Organizacion'				=> $arrDataModificar[0]->ID_Organizacion,
	                'Nu_Tipo_Entidad'				=> 8,//Contacto
	                'ID_Tipo_Documento_Identidad'	=> $arrContactoNuevo['ID_Tipo_Documento_Identidad'],
	                'Nu_Documento_Identidad'		=> $arrContactoNuevo['Nu_Documento_Identidad'],
	                'No_Entidad'					=> $arrContactoNuevo['No_Entidad'],
	                'Txt_Email_Entidad' 			=> $arrContactoNuevo['Txt_Email_Entidad'],
	                'Nu_Telefono_Entidad'			=> $arrContactoNuevo['Nu_Telefono_Entidad'],
	                'Nu_Celular_Entidad'			=> $arrContactoNuevo['Nu_Celular_Entidad'],
	                'Nu_Estado' 					=> 1,
	            );
	    		$this->db->insert('entidad', $arrContacto);
	    		$Last_ID_Contacto = $this->db->insert_id();
		    } else {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-warning', 'message' => 'El contacto ya se encuentra creado, seleccionar Existente');
			}
    		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Contacto" => $Last_ID_Contacto));
		}
		
		$ID_Origen_Tabla = $arrOrdenCabecera['ID_Origen_Tabla'];
		unset($arrOrdenCabecera['ID_Origen_Tabla']);
		$arrOrdenCabecera = array_merge($arrOrdenCabecera, array("ID_Documento_Cabecera" => $arrDataModificar[0]->ID_Documento_Cabecera));
		$this->db->insert($this->table, $arrOrdenCabecera);
		$Last_ID_Documento_Cabecera = $this->db->insert_id();
		
		if ( $ID_Origen_Tabla != 0 ) {
			$where = array( 'Nu_Relacion_Datos' => 2, 'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera, 'ID_Origen_Tabla' => $ID_Origen_Tabla );
			$this->db->delete('relacion_tabla', $where);
			$arrRelacionTabla = array(
				'ID_Empresa' => $arrOrdenCabecera['ID_Empresa'],
				'Nu_Relacion_Datos' => 2,//Relación de Presupuesto a OT
				'ID_Relacion_Enlace_Tabla' => $Last_ID_Documento_Cabecera,
				'ID_Origen_Tabla' => $ID_Origen_Tabla,
			);
			$this->db->insert('relacion_tabla', $arrRelacionTabla);
		}
		
		foreach ($arrOrdenDetalle as $row) {
			$documento_detalle[] = array(
				'ID_Empresa'					=> $this->user->ID_Empresa,
				'ID_Documento_Cabecera'			=> $Last_ID_Documento_Cabecera,
				'ID_Producto'					=> $this->security->xss_clean($row['ID_Producto']),
				'Qt_Producto'					=> $this->security->xss_clean($row['Qt_Producto']),
				'Txt_Nota'					=> $this->security->xss_clean($row['Txt_Nota']),
				'Ss_Precio'						=> $this->security->xss_clean($row['Ss_Precio']),
				'Ss_SubTotal' 					=> $this->security->xss_clean($row['Ss_SubTotal']),
				'Ss_Descuento' => $row['fDescuentoSinImpuestosItem'],
				'Ss_Descuento_Impuesto' => $row['fDescuentoImpuestosItem'],
				'Po_Descuento' => $row['Ss_Descuento'],
				'ID_Impuesto_Cruce_Documento'	=> $this->security->xss_clean($row['ID_Impuesto_Cruce_Documento']),
				'Ss_Impuesto' 					=> $this->security->xss_clean($row['Ss_Impuesto']),
				'Ss_Total' 						=> round($this->security->xss_clean($row['Ss_Total']), 2),
			);
		}
		$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
		
    	$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al modificar');
        } else {
			$this->db->query("SET FOREIGN_KEY_CHECKS=ON;");
            $this->db->trans_commit();
	        return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado', 'Last_ID_Documento_Cabecera' => "$Last_ID_Documento_Cabecera");
        }
    }
    
	public function eliminarOrdenVenta($ID, $Nu_Descargar_Inventario){
		if($this->db->query("SELECT COUNT(*) AS existe FROM orden_seguimiento WHERE ID_Documento_Cabecera = " . $ID . " LIMIT 1")->row()->existe > 0){
			return array('status' => 'warning', 'style_modal' => 'modal-warning', 'message' => 'La orden de venta tiene seguimientos');
		}else{
			$this->db->trans_begin();
			
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
			$this->db->delete($this->table_documento_detalle);
			
			if ($Nu_Descargar_Inventario == '1') {
				$query = "SELECT * FROM movimiento_inventario WHERE ID_Documento_Cabecera = " . $ID;
				$arrDetalle = $this->db->query($query)->result();
				foreach ($arrDetalle as $row) {
					if($this->db->query("SELECT COUNT(*) existe FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto . " LIMIT 1")->row()->existe > 0){
						$where = array('ID_Empresa' => $row->ID_Empresa, 'ID_Organizacion' => $row->ID_Organizacion, 'ID_Almacen' => $row->ID_Almacen, 'ID_Producto' => $row->ID_Producto);
						$Qt_Producto = $this->db->query("SELECT SUM(Qt_Producto) AS Qt_Producto FROM stock_producto WHERE ID_Empresa = " . $row->ID_Empresa . " AND ID_Organizacion = " . $row->ID_Organizacion . " AND ID_Almacen = " . $row->ID_Almacen . " AND ID_Producto = " . $row->ID_Producto)->row()->Qt_Producto;
						$stock_producto = array(
							'ID_Empresa'		=> $row->ID_Empresa,
							'ID_Almacen'		=> $row->ID_Almacen,
							'ID_Producto'		=> $row->ID_Producto,
							'Qt_Producto'		=> ($Qt_Producto + round($row->Qt_Producto, 6)),
							'Ss_Costo_Promedio'	=> 0.00,
						);
						$this->db->update('stock_producto', $stock_producto, $where);
					}
				}
				$this->db->where('ID_Empresa', $this->user->ID_Empresa);
				$this->db->where('ID_Documento_Cabecera', $ID);
				$this->db->delete('movimiento_inventario');
			}
			
			$this->db->where('ID_Empresa', $this->user->ID_Empresa);
			$this->db->where('ID_Documento_Cabecera', $ID);
			$this->db->delete($this->table);
			
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al eliminar');
			} else {
				return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro eliminado');
			}
		}
	}
    
	public function estadoOrdenVenta($ID, $Nu_Descargar_Inventario, $Nu_Estado){
		$this->db->trans_begin();
        
        $where_orden_venta = array('ID_Documento_Cabecera' => $ID);
        $arrData = array(
			'Nu_Estado' => $Nu_Estado,
		);
		$this->db->update('documento_cabecera', $arrData, $where_orden_venta);
                
    	$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al cambiar estado');
        } else {
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro modificado');
        }
	}
    
	public function duplicarOrdenVenta($ID){
		$this->db->trans_begin();
        
        $query_cabecera = " 
SELECT
 ID_Empresa,
 ID_Organizacion,
 ID_Almacen,
 ID_Entidad,
 ID_Contacto,
 ID_Tipo_Asiento,
 ID_Tipo_Documento,
 ID_Matricula_Empleado,
 Fe_Emision,
 ID_Medio_Pago,
 ID_Rubro,
 ID_Moneda,
 Fe_Vencimiento,
 Fe_Periodo,
 Nu_Correlativo,
 Nu_Descargar_Inventario,
 ID_Lista_Precio_Cabecera,
 Txt_Glosa,
 Ss_Descuento,
 Ss_Total,
 Ss_Total_Saldo,
 Ss_Percepcion,
 Fe_Detraccion,
 Nu_Detraccion,
 Nu_Estado,
 Txt_Garantia,
 ID_Mesero,
 ID_Comision,
 No_Formato_PDF
FROM
 documento_cabecera
WHERE
 ID_Documento_Cabecera = " . $ID;
		$arrCabecera = $this->db->query($query_cabecera)->result();
		
		foreach ($arrCabecera as $row) {
			$documento_cabecera = array(
				'ID_Empresa' => $row->ID_Empresa,
				'ID_Organizacion' => $row->ID_Organizacion,
				'ID_Almacen' => $row->ID_Almacen,
				'ID_Entidad' => $row->ID_Entidad,
				'ID_Contacto' => $row->ID_Contacto,
				'ID_Tipo_Asiento' => $row->ID_Tipo_Asiento,
				'ID_Tipo_Documento' => $row->ID_Tipo_Documento,
				'ID_Matricula_Empleado' => $row->ID_Matricula_Empleado,
				'Fe_Emision' => $row->Fe_Emision,
				'ID_Medio_Pago' => $row->ID_Medio_Pago,
				'ID_Rubro' => $row->ID_Rubro,
				'ID_Moneda' => $row->ID_Moneda,
				'Fe_Vencimiento' => $row->Fe_Vencimiento,
				'Fe_Periodo' => $row->Fe_Periodo,
				'Nu_Correlativo' => $row->Nu_Correlativo,
				'Nu_Descargar_Inventario' => $row->Nu_Descargar_Inventario,
				'ID_Lista_Precio_Cabecera' => $row->ID_Lista_Precio_Cabecera,
				'Txt_Glosa' => $row->Txt_Glosa,
				'Ss_Descuento' => $row->Ss_Descuento,
				'Ss_Total' => $row->Ss_Total,
				'Ss_Total_Saldo' => $row->Ss_Total_Saldo,
				'Ss_Percepcion' => $row->Ss_Percepcion,
				'Fe_Detraccion' => $row->Fe_Detraccion,
				'Nu_Detraccion' => $row->Nu_Detraccion,
				'Nu_Estado' => 5,
				'Txt_Garantia' => $row->Txt_Garantia,
				'ID_Mesero' => $row->ID_Mesero,
				'ID_Comision' => $row->ID_Comision,
				'No_Formato_PDF' => $row->No_Formato_PDF,
			);
    	}
    	
		$this->db->insert($this->table, $documento_cabecera);
		$ID_Documento_Cabecera = $this->db->insert_id();
        
        $query_detalle = " 
SELECT
 ID_Empresa,
 ID_Producto,
 Qt_Producto,
 Ss_Precio,
 Ss_Descuento,
 Ss_Descuento_Impuesto,
 Po_Descuento,
 Ss_SubTotal,
 ID_Impuesto_Cruce_Documento,
 Ss_Impuesto,
 Ss_Total
FROM
 documento_detalle
WHERE
 ID_Documento_Cabecera = " . $ID;
		$arrDetalle = $this->db->query($query_detalle)->result();
		
		foreach ($arrDetalle as $row) {
			$documento_detalle[] = array(
				'ID_Empresa' => $row->ID_Empresa,
				'ID_Documento_Cabecera' => $ID_Documento_Cabecera,
				'ID_Producto' => $row->ID_Producto,
				'Qt_Producto' => $row->Qt_Producto,
				'Ss_Precio' => $row->Ss_Precio,
				'Ss_Descuento' => $row->Ss_Descuento,
				'Ss_Descuento_Impuesto'	=> $row->Ss_Descuento_Impuesto,
				'Po_Descuento' => $row->Po_Descuento,
				'Ss_SubTotal' => $row->Ss_SubTotal,
				'ID_Impuesto_Cruce_Documento' => $row->ID_Impuesto_Cruce_Documento,
				'Ss_Impuesto' => $row->Ss_Impuesto,
				'Ss_Total' => $row->Ss_Total,
			);
    	}
		$this->db->insert_batch($this->table_documento_detalle, $documento_detalle);
 
    	$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return array('status' => 'error', 'style_modal' => 'modal-danger', 'message' => 'Error al duplicar orden de venta');
        } else {
        	return array('status' => 'success', 'style_modal' => 'modal-success', 'message' => 'Registro agregado');
        }
	}
}
