<?php
class KardexModel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}
    
	public function getTiposLibroSunat($ID_Tipo_Asiento){
		$query = "SELECT * FROM asiento_libro_sunat_detalle WHERE ID_Tipo_Asiento = " . $ID_Tipo_Asiento;
		return $this->db->query($query)->result();
	}
	
    public function kardex($arrParams){
        $ID_Producto = $arrParams['ID_Producto'];
        $where_producto = '';
        if ($ID_Producto > 0)
            $where_producto = 'AND K.ID_Producto = ' . $ID_Producto;
        
        $query = "
SELECT
 K.ID_Inventario,
 PRO.ID_Producto,
 PRO.Nu_Codigo_Barra,
 PRO.No_Codigo_Interno,
 PRO.No_Producto,
 TMOVI.Nu_Tipo_Movimiento,
 CVCAB.Fe_Emision,
 TDOCU.Nu_Sunat_Codigo AS Tipo_Documento_Sunat_Codigo,
 TDOCU.No_Tipo_Documento_Breve,
 CVCAB.ID_Tipo_Documento,
 CVCAB.ID_Serie_Documento,
 CVCAB.ID_Numero_Documento,
 TMOVI.Nu_Sunat_Codigo AS Tipo_Operacion_Sunat_Codigo,
 TMOVI.No_Tipo_Movimiento,
 CLIPROV.Nu_Documento_Identidad,
 CLIPROV.No_Entidad,
 K.Qt_Producto,
 TP.Sunat_Codigo_PLE AS TP_Sunat_Codigo,
 TP.No_Tipo_Producto AS TP_Sunat_Nombre,
 UM.Nu_Sunat_Codigo AS UM_Sunat_Codigo,
 TDESTADO.No_Descripcion AS No_Estado,
 TDESTADO.No_Class AS No_Class_Estado,
 ALMA.Nu_Codigo_Establecimiento_Sunat
FROM
 movimiento_inventario AS K
 JOIN almacen AS ALMA ON(ALMA.ID_Almacen = K.ID_Almacen)
 JOIN documento_cabecera AS CVCAB ON(K.ID_Documento_Cabecera = CVCAB.ID_Documento_Cabecera)
 JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = CVCAB.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
 JOIN tipo_documento AS TDOCU ON(TDOCU.ID_Tipo_Documento = CVCAB.ID_Tipo_Documento)
 JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
 JOIN entidad AS CLIPROV ON(CLIPROV.ID_Entidad = CVCAB.ID_Entidad)
 JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
 JOIN tipo_producto AS TP ON(TP.ID_Tipo_Producto = PRO.ID_Tipo_Producto)
 JOIN unidad_medida AS UM ON(UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
WHERE
 K.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND K.ID_Almacen = " . $arrParams['ID_Almacen'] . "
 AND CVCAB.Fe_Emision BETWEEN '" . $arrParams['dInicio'] . "' AND '" . $arrParams['dFin'] . "'
 AND PRO.Nu_Tipo_Producto = 1
 AND CVCAB.ID_Tipo_Documento != 20
 " . $where_producto . "
ORDER BY
 PRO.No_Producto ASC,
 CVCAB.Fe_Emision_Hora ASC,
 CVCAB.ID_Tipo_Documento ASC,
 CVCAB.ID_Serie_Documento ASC,
 CONVERT(CVCAB.ID_Numero_Documento, SIGNED INTEGER) ASC;";

        if ( !$this->db->simple_query($query) ){
            $error = $this->db->error();
            return array(
                'sStatus' => 'danger',
                'sMessage' => 'Problemas al obtener datos',
                'sCodeSQL' => $error['code'],
                'sMessageSQL' => $error['message'],
            );
        }

        $arrDocumento = $this->db->query($query)->result();
        
        $query = "
SELECT
 K.ID_Inventario,
 PRO.ID_Producto,
 PRO.Nu_Codigo_Barra,
 PRO.No_Codigo_Interno,
 PRO.No_Producto,
 TMOVI.Nu_Tipo_Movimiento,
 GESCAB.Fe_Emision,
 TDOCU.Nu_Sunat_Codigo AS Tipo_Documento_Sunat_Codigo,
 TDOCU.No_Tipo_Documento_Breve,
 GESCAB.ID_Tipo_Documento,
 GESCAB.ID_Serie_Documento,
 GESCAB.ID_Numero_Documento,
 TMOVI.Nu_Sunat_Codigo AS Tipo_Operacion_Sunat_Codigo,
 TMOVI.No_Tipo_Movimiento,
 CLIPROV.Nu_Documento_Identidad,
 CLIPROV.No_Entidad,
 K.Qt_Producto,
 TP.Sunat_Codigo_PLE AS TP_Sunat_Codigo,
 TP.No_Tipo_Producto AS TP_Sunat_Nombre,
 UM.Nu_Sunat_Codigo AS UM_Sunat_Codigo,
 TDESTADO.No_Descripcion AS No_Estado,
 TDESTADO.No_Class AS No_Class_Estado,
 ALMA.Nu_Codigo_Establecimiento_Sunat
FROM
 movimiento_inventario AS K
 JOIN almacen AS ALMA ON(ALMA.ID_Almacen = K.ID_Almacen)
 JOIN guia_cabecera AS GESCAB ON(K.ID_Guia_Cabecera = GESCAB.ID_Guia_Cabecera)
 JOIN tabla_dato AS TDESTADO ON(TDESTADO.Nu_Valor = GESCAB.Nu_Estado AND TDESTADO.No_Relacion = 'Tipos_EstadoDocumento')
 JOIN tipo_documento AS TDOCU ON(TDOCU.ID_Tipo_Documento = GESCAB.ID_Tipo_Documento)
 JOIN tipo_movimiento AS TMOVI ON(TMOVI.ID_Tipo_Movimiento = K.ID_Tipo_Movimiento)
 JOIN entidad AS CLIPROV ON(CLIPROV.ID_Entidad = GESCAB.ID_Entidad)
 JOIN producto AS PRO ON(PRO.ID_Producto = K.ID_Producto)
 JOIN tipo_producto AS TP ON(TP.ID_Tipo_Producto = PRO.ID_Tipo_Producto)
 JOIN unidad_medida AS UM ON(UM.ID_Unidad_Medida = PRO.ID_Unidad_Medida)
WHERE
 K.ID_Empresa = " . $this->empresa->ID_Empresa . "
 AND K.ID_Organizacion = " . $this->empresa->ID_Organizacion . "
 AND K.ID_Almacen = " . $arrParams['ID_Almacen'] . "
 AND GESCAB.Fe_Emision BETWEEN '" . $arrParams['dInicio'] . "' AND '" . $arrParams['dFin'] . "'
 AND PRO.Nu_Tipo_Producto = 1
 AND GESCAB.ID_Tipo_Documento != 20
 " . $where_producto . "
ORDER BY
 PRO.No_Producto ASC,
 GESCAB.Fe_Emision ASC,
 GESCAB.ID_Tipo_Documento ASC,
 GESCAB.ID_Serie_Documento ASC,
 CONVERT(GESCAB.ID_Numero_Documento, SIGNED INTEGER) ASC;";

        if ( !$this->db->simple_query($query) ){
            $error = $this->db->error();
            return array(
                'sStatus' => 'danger',
                'sMessage' => 'Problemas al obtener datos',
                'sCodeSQL' => $error['code'],
                'sMessageSQL' => $error['message'],
            );
        }

        $arrGuia = $this->db->query($query)->result();
        $arrData = array_merge($arrDocumento, $arrGuia);

        $orderID_Producto = array();
        foreach ($arrData as $key => $row) {
            $orderID_Producto[$key] = $row->ID_Producto;
        }
        array_multisort($orderID_Producto, SORT_ASC, $arrData);

        if ( count($arrData) > 0 ){
            return array(
                'sStatus' => 'success',
                'arrData' => $arrData,
            );
        }
        
        return array(
            'sStatus' => 'warning',
            'sMessage' => 'No se encontro registro',
        );
    }
}
