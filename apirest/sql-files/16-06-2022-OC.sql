INSERT INTO `tipo_documento` (`ID_Tipo_Documento`, `No_Tipo_Documento`, `No_Tipo_Documento_Breve`, `Nu_Es_Sunat`, `Nu_Sunat_Codigo`, `Nu_Impuesto`, `Nu_Enlace`, `Nu_Enlace_Modificar`, `Nu_Venta`, `Nu_Compra`, `Nu_Estado`, `Nu_Orden`, `Nu_Cotizacion_Venta`, `Nu_Orden_Compra`, `Nu_Orden_Ingreso`, `ID_Tipo_Rentanilidad`) VALUES ('78', 'Orden de Compra', 'Orden de Compra', '0', '00', '0', '0', '0', '1', '1', '1', '0', '0', '0', '1', '0');
INSERT INTO `serie_documento` (`ID_Empresa`, `ID_Organizacion`, `ID_Tipo_Documento`, `ID_Serie_Documento_PK`, `ID_Serie_Documento`, `Nu_Numero_Documento`, `Nu_Cantidad_Caracteres`, `Nu_Estado`, `ID_Almacen`) VALUES ('51', '26', '78', '196', 'OC', '1', '5', '1', '22');

ALTER TABLE `documento_cabecera`
ADD COLUMN `Fe_Recepcion` DATE NULL AFTER `Qt_Producto`,
ADD COLUMN `Fe_Pago` DATE NULL AFTER `Fe_Recepcion`,
ADD COLUMN `Nu_Operacion` VARCHAR(20) NULL AFTER `Fe_Pago`,
ADD COLUMN `Nu_Estado_Conta` TINYINT(1) NULL AFTER `Nu_Operacion`,
ADD COLUMN `Nu_Orden_Ingreso` VARCHAR(20) NULL AFTER `Nu_Estado_Conta`,
ADD COLUMN `Txt_Comentario` TEXT NULL AFTER `Nu_Orden_Ingreso`,
ADD COLUMN `Nu_Estado_OC` TINYINT(1) NULL AFTER `Txt_Comentario`,
ADD COLUMN `Ss_SubTotal` DECIMAL(20,2) NULL DEFAULT NULL AFTER `Nu_Estado_OC`,
ADD COLUMN `Ss_Impuesto` DECIMAL(20,2) NULL DEFAULT NULL AFTER `Ss_SubTotal`;


ALTER TABLE `documento_cabecera`
ADD COLUMN `ServicioBien` TINYINT(1) NULL AFTER `Ss_Impuesto`;
