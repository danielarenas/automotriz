<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico?ver=3.0">
    <link rel="apple-touch-icon-precomposed" sizes="192x192" href="assets/ico/android-chrome-512x512.png?ver=3.0">
    <link rel="apple-touch-icon-precomposed" sizes="192x192" href="assets/ico/android-chrome-192x192.png?ver=3.0">
    <link rel="apple-touch-icon-precomposed" sizes="32x32" href="assets/ico/favicon-32x32.png?ver=3.0">
    <link rel="apple-touch-icon-precomposed" sizes="16x16" href="assets/ico/favicon-16x16.png?ver=3.0">
    <link rel="apple-touch-icon-precomposed" sizes="16x16" href="assets/ico/apple-touch-icon.png?ver=3.0">
    <link rel="manifest" href="/site.webmanifest">
    <title>laesystems | Login</title>
    <meta name="author" content="laesystems">
    <meta name="Subject" content="Creamos soluciones innovadoras">
    <meta name="Copyright" content="Copyright © laesystems. Todos los derechos reservados.">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/bootstrap/dist/css/bootstrap.min.css"); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/font-awesome/css/font-awesome.min.css"); ?>">
    <!-- Selected -->
    <link rel="stylesheet" href="<?php echo base_url("bower_components/select2/dist/css/select2.min.css"); ?>">
    <!-- laesystems -->
    <link rel="stylesheet" href="<?php echo base_url("assets/css/login.css?ver=6.2"); ?>">
    <!--  Android 5 Chrome Color-->
    <meta name="theme-color" content="#000000">
    <meta name="msapplication-navbutton-color" content="#000000"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000000" />  
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000000" />  
    <!-- iOS Safari --> 
    <meta name="apple-mobile-web-app-capable" content="yes">    
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
</head>
<body>
    <div class="fondo_pantalla">
        <div class="container-fluid">
            <div class="row">            
            <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
            <div class="col-md-8 col-lg-6">
                <div class="panel panel-default">
                	<div class="panel-heading">
                        <div class="row row-login-logo">
                			<div class="col-md-12 col-lg-12 text-center">
                        	    <img class="img-logo" src="<?php echo base_url("assets/img/logos/logo_automotriz.png?ver=2.0") ?>" alt="laesystems" title="laesystems">
                            </div>
                        </div>
                	</div>
                	<div class="panel-body">
                        <?php
                        $attributes = array('id' => 'form-login');
                        echo form_open('', $attributes, '');
                        ?>
                            <div id="div-login" class="row">
                    			<div class="col-sm-12">
                    			    <div class="form-group">
                    					<div class="input-group">
                    						<span class="input-group-addon"><i class="fa fa-user fa-lg" aria-hidden="true"></i></span>
                    						<input type="text" id="txt-usuario" name="No_Usuario" class="form-control" placeholder="Ingresar usuario" autofocus>
                    					</div>
                    					<span class="help-block" id="error"></span>
                    				</div>
                    			</div>
                    			
                    			<div class="col-sm-12">
                    			    <div class="form-group">
                    					<div class="input-group">
                    						<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                    						<input type="password" id="txt-password" name="No_Password" class="form-control pwd" autocomplete="off" placeholder="Ingresar contraseña">
                    						<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    					</div>
                    					<span class="help-block" id="error"></span>
                    				</div>
                    			</div>
                    			
                    			<div class="col-sm-12">
                    			    <div class="form-group">
                                        <div class="div-msg"></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-12">
                    			    <div class="form-group">
                    					<button type="submit" id="btn-login" class="btn btn-success btn-md btn-block">Iniciar sesión</button>
                    					<button type="button" id="btn-recuperar_cuenta" class="btn btn-link btn-md btn-block">Olvidé mi contraseña</button>
                    				</div>
                    			</div>
                    		</div>
                        <?php echo form_close(); ?>
                        <?php
                        $attributes = array('id' => 'form-login_empresa');
                        echo form_open('', $attributes, '');
                        ?>
                            <div id="div-empresa" class="row">
                                <input type="hidden" id="txt-usuario_empresa" name="No_Usuario" class="form-control">
                                <input type="hidden" id="txt-password_empresa" name="No_Password" class="form-control">
                    			
                                <div class="col-sm-12">
                    			    <div class="form-group">
                                        <label>Empresa</label>
                		  				<select id="cbo-Empresas" name="ID_Empresa" class="form-control required select2" style="width: 100%;"></select>
                                        <span class="help-block" id="error"></span>
                    				</div>
                    			</div>

                                <div class="col-sm-12">
                    			    <div class="form-group">
                                        <label>Organizacion</label>
                		  				<select id="cbo-organizacion" name="ID_Organizacion" class="form-control required select2" style="width: 100%;"></select>
                                        <span class="help-block" id="error"></span>
                    				</div>
                    			</div>
                    			
                    			<div class="col-sm-12">
                    			    <div class="form-group">
                                        <div class="div-msg"></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-12">
                    			    <div class="form-group">
                    					<button type="submit" id="btn-login_empresa" class="btn btn-success btn-md btn-block">Entrar</button>
                    					<button type="button" class="btn btn-link btn-md btn-block btn-login_return">Regresar al login</button>
                    				</div>
                    			</div>
                    		</div>
                        <?php echo form_close(); ?>
                        <?php
                        $attributes = array('id' => 'form-recuperar_cuenta');
                        echo form_open('', $attributes, '');
                        ?>
                            <div id="div-recuperar_cuenta" class="row">
                    			<div class="col-sm-12">
                    			    <div class="form-group">
                    					<div class="input-group">
                    						<span class="input-group-addon"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i></span>
                    						<input type="text" id="txt-email" name="Txt_Email_Recovery" class="form-control" placeholder="Ingresar correo">
                    					</div>
                    					<span class="help-block" id="error"></span>
                    				</div>
                    			</div>
                    			
                    			<div class="col-sm-12">
                    			    <div class="form-group">
                                        <div class="div-msg"></div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-12">
                    			    <div class="form-group">
                    					<button type="submit" id="btn-send_correo" class="btn btn-success btn-md btn-block">Recuperar cuenta</button>
                    					<button type="button" class="btn btn-link btn-md btn-block btn-login_return">Regresar al login</button>
                    				</div>
                    			</div>
                    		</div>
                        <?php echo form_close(); ?>
                	</div>
                </div>
            </div>
            </div><!--FIN row-->
        </div><!--FIN Container-->
        <footer>
            <div id="footer">
                <div id="footerLinks" class="footerNode text-secondary">
                    <span id="ftrCopy"><a href="https://www.laesystems.com/terminos_y_condiciones_laesystems.pdf" target="_blank" rel="noopener noreferrer" alt="laesystems" title="laesystems" style="text-decoration: none; text-align:center; color: #0cacbd;">Términos de Servicio y Condiciones de Uso</a></span>
                    <span id="ftrCopy"><a href="https://www.laesystems.com" target="_blank" alt="laesystems" title="laesystems" style="text-decoration: none; color: #0cacbd"><span style="color: #0cacbd">laesystems</span>2016</a></span>
                </div>
            </div>
        </footer>
    </div>
    <!-- laesystems -->
    <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-3.2.1.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.validate.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("bower_components/select2/dist/js/select2.full.min.js"); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/js/inicio.js?ver=2.1"); ?>"></script>
	<script> var base_url = '<?php echo base_url(); ?>'; </script>
</body>