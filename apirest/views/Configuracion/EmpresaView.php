<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-md-3">
                <div class="form-group">
    		  				<select id="cbo-Filtros_Empresas" name="Filtros_Empresas" class="form-control">
    		  				  <option value="Empresa">Razón Social</option>
    		  				  <option value="RUC">RUC</option>
    		  				</select>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" id="txt-Global_Filter" name="Global_Filter" class="form-control" maxlength="100" placeholder="Buscar" value="" autocomplete="off" autocomplete="off">
                </div>
              </div>
              
              <div class="col-md-3">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                <button type="button" class="btn btn-success btn-block" onclick="agregarEmpresa()"><i class="fa fa-plus-circle"></i> Agregar</button>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-Empresa" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Proveedor FE</th>
                  <th>Ecommerce</th>
                  <th>Ruc</th>
                  <th>Razón Social</th>
                  <th class="no-sort_left">Dirección</th>
                  <th class="no-sort">Estado</th>
                  <th class="no-sort"></th>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  <!-- Modal -->
  <?php
  $attributes = array('id' => 'form-Empresa', 'enctype' => 'multipart/form-data');
  echo form_open('', $attributes);
  ?>
  <div class="modal fade" id="modal-Empresa" role="dialog">
  <div class="modal-dialog modal-lg">
  	<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center"></h4>
      </div>
      
    	<div class="modal-body">
    	  <input type="hidden" id="txt-EID_Empresa" name="EID_Empresa" value="">
    	  <input type="hidden" id="txt-ENu_Documento_Identidad" name="ENu_Documento_Identidad" value="">
  
        <div class="row">
          <?php if ( $this->user->No_Usuario == 'root' ){ ?>
          <div class="col-xs-12 col-sm-3">
            <div class="form-group">
              <label>Proveedor FE <span class="label-advertencia">*</span></label>
              <select id="cbo-tipo_proveedor_fe" name="Nu_Tipo_Proveedor_FE" class="form-control required" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
          <?php } else { ?>
            <input type="hidden" id="cbo-tipo_proveedor_fe" name="Nu_Tipo_Proveedor_FE" class="form-control" value="<?php echo $this->empresa->Nu_Tipo_Proveedor_FE; ?>">
          <?php } ?>

          <div class="col-xs-9 col-sm-3">
            <label>RUC <span class="label-advertencia"> *</span></label>
            <div class="form-group">
              <input type="tel" id="txt-Nu_Documento_Identidad" name="Nu_Documento_Identidad" class="form-control required input-number" maxlength="11" placeholder="Ingresar número" autocomplete="off">
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-3 col-sm-1 text-center">
            <div class="form-group">
              <label>Api</label>
              <button type="button" id="btn-cloud-api_empresa" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
            </div>
          </div>

          <div class="col-xs-12 col-md-3">
            <div class="form-group">
              <label>¿Es Ecoomerce? <span class="label-advertencia">*</span></label>
  	  				<select id="cbo-tipo_ecommerce_empresa" name="Nu_Tipo_Ecommerce_Empresa" class="form-control required"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-12 col-md-2">
            <div class="form-group">
              <label>Estado <span class="label-advertencia">*</span></label>
  	  				<select id="cbo-Estado" name="Nu_Estado" class="form-control required"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
          
        <div class="row div-row-empresas-marketplace">
          <div class="col-xs-12 col-md-12">
            <div class="form-group">
              <label>Empresas Marketplace <span class="label-advertencia">*</span></label>
              <select id="cbo-empresa-marketplace" name="ID_Empresa_Marketplace" class="form-control required"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-7">
            <div class="form-group">
              <label>Razón Social <span class="label-advertencia"> *</span></label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-building" aria-hidden="true"></i></span>
                <input type="text" name="No_Empresa" class="form-control required" placeholder="Ingresar nombre" maxlength="100" autocomplete="off">
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-md-5">
            <div class="form-group">
              <label>Nombre Comercial </label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-building" aria-hidden="true"></i></span>
                <input type="text" name="No_Empresa_Comercial" class="form-control" placeholder="Ingresar nombre" maxlength="100" autocomplete="off">
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
        
  		  <div class="row">
          <div class="col-xs-12 col-md-8">
            <div class="form-group">
              <label>Domicilio Fiscal <span class="label-advertencia">*</span></label>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                <input type="text" name="Txt_Direccion_Empresa" placeholder="Ingresar dirección" class="form-control required" autocomplete="off">
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-12 col-md-4 div-row-nubefact">
            <div class="form-group">
              <label>Ubigeo INEI <span class="label-advertencia">*</span></label>
		  				<select id="cbo-ubigeo_inei" name="ID_Ubigeo_Inei" title="Se encuentra en la ficha RUC" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
        
        <div class="row div-row-nubefact">
          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>País <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Paises" name="ID_Pais" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>Departamento <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Departamentos" name="ID_Departamento" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>Provincia <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Provincias" name="ID_Provincia" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>Distrito <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Distritos" name="ID_Distrito" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>

  		  <div class="row div-row-nubefact">
          <div class="col-xs-12 col-sm-3">
            <div class="form-group">
              <label>Usuario SOL <span class="label-advertencia">*</span></label>
              <input type="text" name="Txt_Usuario_Sunat_Sol" placeholder="Ingresar dirección" class="form-control required" autocomplete="off" value="MODDATOS">
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-3">
            <div class="form-group">
              <label>Contraseña SOL <span class="label-advertencia">*</span></label>
              <input type="text" name="Txt_Password_Sunat_Sol" placeholder="Ingresar dirección" class="form-control required" autocomplete="off" value="moddatos">
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-3">
            <label>Archivo Certificado <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                <label class="btn btn-default" for="file-certificado_digital">
                  <input type="file" id="file-certificado_digital" name="certificado_digital" multiple=false accept=".pfx" required style="display:none" onchange="$('#upload-file-certificado_digital').html(this.files[0].name)">Buscar...
                </label>
                <span class='label label-info' id="upload-file-certificado_digital"></span>
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-3">
            <div class="form-group">
              <label>Contraseña Certificado <span class="label-advertencia">*</span></label>
              <input type="text" name="Txt_Password_Firma_Digital" placeholder="Ingresar dirección" class="form-control required" autocomplete="off" value="123456">
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
      </div>
      
    	<div class="modal-footer">
          <div class="col-xs-6 col-md-6">
            <div class="form-group">
              <button type="button" class="btn btn-danger btn-md btn-block" data-dismiss="modal"><span class="fa fa-sign-out"></span> Salir</button>
            </div>
          </div>
          <div class="col-xs-6 col-md-6">
            <div class="form-group">
              <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar </button>
            </div>
          </div>
      </div>
    </div>
  </div>
  </div>
  <!-- /.Modal -->
  <?php echo form_close(); ?>
</div>
<!-- /.content-wrapper -->