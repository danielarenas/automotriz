<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <?php
  $sCssDisplayRoot='style="display:none"';
  if ( $this->user->No_Usuario == 'root' ){
    $sCssDisplayRoot='';
  }
  ?>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <?php
              if ( $this->user->No_Usuario == 'root' ){ ?>
              <div class="col-md-6">
                <label>Empresa</label>
                <div class="form-group">
                  <select id="cbo-filtro_empresa" name="ID_Empresa" class="form-control select2 required" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              <div class="col-md-6">
                <label>Organización</label>
                <div class="form-group">
                  <select id="cbo-filtro_organizacion" name="ID_Organizacion" class="form-control select2 required" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              <?php } else { ?>
                <input type="hidden" id="cbo-filtro_empresa" name="ID_Empresa" class="form-control" value="<?php echo $this->user->ID_Empresa; ?>">
                <input type="hidden" id="cbo-filtro_organizacion" name="ID_Organizacion" class="form-control" value="<?php echo $this->user->ID_Organizacion; ?>">
              <?php } ?>

              <div class="col-md-3">
                <div class="form-group">
    		  				<select id="cbo-Filtros_Sistemas" name="Filtros_Sistemas" class="form-control">
    		  				  <option value="Sistema">Nombre Dominio</option>
    		  				</select>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" id="txt-Global_Filter" name="Global_Filter" class="form-control" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-md-3">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                <button type="button" class="btn btn-success btn-block" onclick="agregarSistema()"><i class="fa fa-plus-circle"></i> Agregar</button>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-Sistema" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <?php if ( $this->user->No_Usuario == 'root' ){ ?>
                  <th>Empresa</th>
                  <th>Organización</th>
                  <?php } ?>
                  <th>F. Inicio</th>
                  <th>Dominio</th>
                  <th>Celular</th>
                  <th>Correo</th>
                  <th class="no-sort">Estado</th>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-Sistema');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" name="EID_Empresa" class="form-control">
          	  <input type="hidden" name="EID_Configuracion" class="form-control">
          	  <input type="hidden" name="ENo_Dominio_Empresa" class="form-control">
          	  <input type="hidden" name="ENo_Foto_Boleta" class="form-control">
          	  <input type="hidden" name="ENo_Foto_Factura" class="form-control">
          	  <input type="hidden" name="ENo_Foto_NCredito" class="form-control">
          	  <input type="hidden" name="ENo_Foto_Guia" class="form-control">
              <input type="hidden" name="hidden-nombre_logo" class="form-control" value="">
          	  <input type="hidden" id="hidden-nombre_imagen_logo_empresa" name="No_Imagen_Logo_Empresa" class="form-control" value="">
              
              <div class="row" <?php echo $sCssDisplayRoot; ?>>
                <div class="col-xs-12">
                  <div class="form-group">
                    <label>Empresa <span class="label-advertencia">*</span></label>
        	  				<select id="cbo-Empresas" name="ID_Empresa" class="form-control select2 required" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-2" <?php echo $sCssDisplayRoot; ?>>
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Fecha de inicio de sistema">F. Inicio <span class="label-advertencia">*</span></label>
                    <div class="input-group date">
                      <input type="text" name="Fe_Inicio_Sistema" class="form-control date-picker-invoice required" data-toggle="tooltip" data-placement="bottom" title="Fecha de inicio de sistema" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                    </div>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-1">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Enviar los registros de la opción Facturas de venta automáticamente a Sunat (Si / No)">Sunat <span class="label-advertencia">*</span></label>
        	  				<select id="cbo-enviar_sunat_automatic" name="Nu_Enviar_Sunat_Automatic" class="form-control required" data-toggle="tooltip" data-placement="bottom" title="Enviar los registros de la opción Facturas de venta automáticamente a Sunat (Si / No)"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-2">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Vender productos con stock en negativo (Si / No)">Validar Stock</label>
        	  				<select id="cbo-activar_stock" name="Nu_Validar_Stock" class="form-control required" data-toggle="tooltip" data-placement="bottom" title="Vender productos con stock en negativo (Si / No)"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-6 col-sm-2 col-md-1">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Se usuará cuando se necesite alertar al sistema de los productos con fecha de vencimiento actual o pasado el número de días configurados">Día(s)</label>
                    <input type="tel" name="Nu_Dia_Limite_Fecha_Vencimiento" class="form-control input-number" maxlength="1" data-toggle="tooltip" data-placement="bottom" title="Se usuará cuando se necesite alertar al sistema de los productos con fecha de vencimiento actual o pasado el número de días configurados" placeholder="Lote vencimiento" value="0" autocomplete="off">
                  </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-2">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Mostrar logo en el ticket (Si / No)">Logo Ticket <span class="label-advertencia">*</span></label>
        	  				<select id="cbo-logo_ticket" name="Nu_Logo_Empresa_Ticket" class="form-control required" data-toggle="tooltip" data-placement="bottom" title="Mostrar logo en el ticket (Si / No)"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-6 col-sm-4 col-md-1">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="El alto solo se va a considerar para el logo del ticket">Alto <span class="label-advertencia">*</span></label>
                    <input type="tel" name="Nu_Height_Logo_Ticket" data-toggle="tooltip" data-placement="bottom" title="El alto solo se va a considerar para el logo del ticket" class="form-control input-number required" maxlength="3" placeholder="" value="" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-6 col-sm-4 col-md-1">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="El ancho se va a considerar para los todos los formatos (PDF y Ticket)">Ancho <span class="label-advertencia">*</span></label>
                    <input type="tel" name="Nu_Width_Logo_Ticket" data-toggle="tooltip" data-placement="bottom" title="El ancho se va a considerar para los todos los formatos (PDF y Ticket)" class="form-control input-number required" maxlength="3" placeholder="" value="" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-6 col-sm-4 col-md-2">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Arque de Punto de Venta">Arqueo <span class="label-advertencia">*</span></label>
        	  				<select id="cbo-arqueo_punto_venta" name="Nu_Imprimir_Liquidacion_Caja" class="form-control required" data-toggle="tooltip" data-placement="bottom" title="Arque de Punto de Venta"></select>
                    <!--<input type="tel" name="Nu_Imprimir_Liquidacion_Caja" class="form-control input-number required" maxlength="1" title="Formato ticket de liquidación de caja (1) Familia y (2) Item" placeholder="" value="" autocomplete="off">-->
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <?php
                $sNombreGrupo = strtoupper($this->user->No_Grupo);
                if ( $this->user->No_Usuario == 'root' || ($sNombreGrupo == 'GERENCIA' || $sNombreGrupo == 'GERENTE GENERAL' || $sNombreGrupo == 'GERENTE') ){ ?>
                <div class="col-xs-6 col-sm-2 col-md-2">
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Activar descuento en el Punto de Venta (Si / No)">Dscto. Punto Venta <span class="label-advertencia">*</span></label>
                    <select id="cbo-activar_descuento_punto_venta" name="Nu_Activar_Descuento_Punto_Venta" class="form-control required" data-toggle="tooltip" data-placement="bottom" title="Activar descuento en el Punto de Venta (Si / No)"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                <?php } else { ?>
                  <input type="hidden" id="cbo-activar_descuento_punto_venta" name="Nu_Activar_Descuento_Punto_Venta" class="form-control" value="<?php echo $this->empresa->Nu_Tipo_Rubro_Empresa; ?>">
                <?php } ?>

                <?php
                if ( $this->user->No_Usuario == 'root' ){ ?>
                <div class="col-xs-6 col-sm-2 col-md-3">
                  <div class="form-group">
                    <label title="Rubro empresa">Rubro <span class="label-advertencia">*</span></label>
                    <select id="cbo-tipo_rubro_empresa" name="Nu_Tipo_Rubro_Empresa" title="Rubro Empresa" class="form-control select2 required" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                <?php } else { ?>
                  <input type="hidden" id="cbo-tipo_rubro_empresa" name="Nu_Tipo_Rubro_Empresa" class="form-control" value="<?php echo $this->empresa->Nu_Tipo_Rubro_Empresa; ?>">
                <?php } ?>

                <div class="col-xs-6 col-sm-4 col-md-2" <?php echo $sCssDisplayRoot; ?>>
                  <div class="form-group">
                    <label data-toggle="tooltip" data-placement="bottom" title="Verificar que la opción Punto de Venta se pueda vender desde puntos autorizados (Si / No)">Autorización Venta <span class="label-advertencia">*</span></label>
                    <select id="cbo-autorizacion_punto_venta" name="Nu_Verificar_Autorizacion_Venta" class="form-control required" data-toggle="tooltip" data-placement="bottom" title="Verificar que la opción Punto de Venta se pueda vender desde puntos autorizados (Si / No)"></select>
                    <!--<input type="tel" name="Nu_Verificar_Autorizacion_Venta" class="form-control input-number required" maxlength="1" title="Verificar autorizacion de venta (1) Si / (0) No" placeholder="" value="" autocomplete="off">-->
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-6 col-sm-3 col-md-3" <?php echo $sCssDisplayRoot; ?>>
                  <div class="form-group">
                    <label>Estado <span class="label-advertencia">*</span></label>
        	  				<select id="cbo-Estado" name="Nu_Estado" class="form-control required"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
              
      			  <div class="row" <?php echo $sCssDisplayRoot; ?>>
                <div class="col-md-12">
        			    <div id="panel-DetalleProductosOrdenVenta" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-cloud-download"></i> <b>Token para acceder (RENIEC, RUC y Tasa de cambio SUNAT)</b></div>
                    <div class="panel-body">
                      <div class="row">
                				<div class="col-sm-12">
                          <div class="form-group">
                            <label>Token</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="blue fa fa-lock" aria-hidden="true"></i></span>
                              <input type="password" id="Txt_Token" name="Txt_Token" placeholder="Ingresar token" class="form-control required pwd" autocomplete="off">
                              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                            <span class="help-block" id="error"></span>
                          </div>
                				</div>
                			</div>
                		</div>
                	</div>
                </div>
              </div>
                				  
      			  <div class="row" style="display:none">
                <div class="col-md-12">
        			    <div id="panel-DetalleProductosOrdenVenta" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Formatos de impresión</b></div>
                    <div class="panel-body">
                      <div class="row">
                				<div class="col-xs-3 col-sm-6 col-md-3">
                          <label>Boleta</label>
                				  <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                              <label class="btn btn-default" for="my-file-selector_boleta">
                                <input type="file" id="my-file-selector_boleta" name="No_Foto_Boleta" multiple=false accept=".png,.jpeg,.jpg" required style="display:none" onchange="$('#upload-file-info_boleta').html(this.files[0].name)">Subir archivo
                              </label>
                              <span class='label label-info' id="upload-file-info_boleta"></span>
                            </div>
                          </div>
                        </div>
                        
                				<div class="col-xs-3 col-sm-6 col-md-3">
                          <label>Factura</label>
                				  <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                              <label class="btn btn-default" for="my-file-selector_factura">
                                <input type="file" id="my-file-selector_factura" name="No_Foto_Factura" multiple=false accept=".png,.jpeg,.jpg" required style="display:none" onchange="$('#upload-file-info_factura').html(this.files[0].name)">Subir archivo
                              </label>
                              <span class='label label-info' id="upload-file-info_factura"></span>
                            </div>
                          </div>
                        </div>
                        
                				<div class="col-xs-3 col-sm-6 col-md-3">
                          <label>Nota de Crédito</label>
                				  <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                              <label class="btn btn-default" for="my-file-selector_ncredito">
                                <input type="file" id="my-file-selector_ncredito" name="No_Foto_NCredito" multiple=false accept=".png,.jpeg,.jpg" required style="display:none" onchange="$('#upload-file-info_ncredito').html(this.files[0].name)">Subir archivo
                              </label>
                              <span class='label label-info' id="upload-file-info_ncredito"></span>
                            </div>
                          </div>
                        </div>
                        
                				<div class="col-xs-3 col-sm-6 col-md-3">
                          <label>Guía de Remisión</label>
                				  <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-cloud-upload" aria-hidden="true"></i></span>
                              <label class="btn btn-default" for="my-file-selector_guia">
                                <input type="file" id="my-file-selector_guia" name="No_Foto_Guia" multiple=false accept=".png,.jpeg,.jpg" required style="display:none" onchange="$('#upload-file-info_guia').html(this.files[0].name)">Subir archivo
                              </label>
                              <span class='label label-info' id="upload-file-info_guia"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                        
              <!-- Orden de Venta -->
      			  <div class="row">
                <div class="col-md-12">
        			    <div id="panel-DetalleProductosOrdenVenta" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Formato para Facturación Electrónica - Representación Interna - Cotización - Orden de Compra</b></div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Dominio</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-building" aria-hidden="true"></i></span>
                              <input type="text" id="txt-No_Dominio_Empresa" name="No_Dominio_Empresa" class="form-control" placeholder="Ingresar nombre" maxlength="50" autocomplete="off">
                            </div>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
              
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Correo</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                              <input type="text" name="Txt_Email_Empresa" placeholder="Ingresar correo" class="form-control" maxlength="250" autocomplete="off">
                            </div>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-md-3">
                          <label>Celular</label>
                          <div class="form-group">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                              <input type="text" name="Nu_Celular_Empresa" class="form-control" placeholder="Ingresar número" maxlength="250" autocomplete="off">
                            </div>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>Teléfono</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                              <input type="text" name="Nu_Telefono_Empresa" class="form-control" placeholder="Ingresar número" maxlength="250" autocomplete="off">
                            </div>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>
                        
                      <div class="row">
                        <div class="col-md-12">
                          <label>Slogan </label>
                          <div class="form-group">
                            <input type="text" name="Txt_Slogan_Empresa" class="form-control" placeholder="Ingresar nombre" autocomplete="off">
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>
                    
            	        <div class="row">
                        <div class="col-md-4">
                          <div class="form-group">
                            <div class="well well-sm">
                              <i class="fa fa-warning"></i> Indicaciones:
                              <br>- La imágen se usuará para los siguientes formatos:<br>
                                <b>Cotización<br>
                                Facturación Eletrónica<br>
                                Representación Interna<br>
                                Orden de compra</b>
                              <br>- Formato: <b>PNG</b>
                              <br>- Tamaño imagén <b>Alto: 512px y Ancho: 512px</b>
                              <br>- Peso Máximo <b>1024 KB</b>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-8">
                          <label>Términos y condiciones (Solo para ticket)</label>
                          <div class="form-group">
                            <textarea name="Txt_Terminos_Condiciones_Ticket" class="form-control" placeholder="Descripción" value="" autocomplete="off"></textarea>
                          </div>
                        </div>

                        <div class="col-md-8">
                          <label>Términos y condiciones (Cotización - Orden de Compra)</label>
                          <div class="form-group">
                            <textarea name="Txt_Terminos_Condiciones" class="form-control" placeholder="Descripción" value="" autocomplete="off"></textarea>
                          </div>
                        </div>
                      </div>
                      
            	        <div class="row">
            	          <div class="col-md-4 text-center divDropzone"></div>
                        <div class="col-md-12">
                          <label>Cuentas Bancarias (Facturación Electrónica - Representación Interna - Cotización - Orden de Compra)</label>
                          <div class="form-group">
                            <textarea name="Txt_Cuentas_Bancarias" class="form-control" placeholder="Ingresar número(s) de cuenta(s) bancaria(s)" value="" autocomplete="off"></textarea>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <label>Nota (Cotización - Orden de Compra)</label>
                          <div class="form-group">
                            <textarea name="Txt_Nota" class="form-control" placeholder="Descripción" value="" autocomplete="off"></textarea>
                          </div>
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>
              </div>
              <!-- ./ Orden Compra -->
            
      			  <div class="row">
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->