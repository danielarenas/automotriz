<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-12 col-sm-12 col-md-2">
                <div class="form-group">
                  <label>Fecha</label>
    		  				<select id="cbo-tipo_consulta_fecha" class="form-control" style="width: 100%;">
    		  				  <option value="0" selected="selected">Actual</option>
    		  				  <option value="1">Histórico</option>
    		  				</select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2 div-fecha_historica">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2 div-fecha_historica">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Tipo</label>
    		  				<select id="cbo-filtros_tipos_documento" class="form-control" style="width: 100%;"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Serie</label>
    		  				<select id="cbo-filtros_series_documento" class="form-control" style="width: 100%;"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <input type="tel" id="txt-Filtro_NumeroDocumento" class="form-control input-number" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-2">
                <div class="form-group">
                  <label>Estado Documento</label>
    		  				<select id="cbo-estado_documento" class="form-control" style="width: 100%;">
    		  				  <option value="0" selected="selected">Todos</option>
    		  				  <option value="6">Completado</option>
    		  				  <option value="8">Completado Enviado</option>
    		  				  <option value="9">Completado Error</option>
    		  				  <option value="7">Anulado</option>
    		  				  <option value="10">Anulado Enviado</option>
    		  				  <option value="11">Anulado Error</option>
    		  				</select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label>Cliente</label>
                  <input type="hidden" id="txt-AID" class="form-control">
                  <input type="text" id="txt-Filtro_Entidad" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre / tipo doc. identidad" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-2">
                <div class="form-group">
                  <label>Recepción</label>
    		  				<select id="cbo-tipo_recepcion_cliente" class="form-control">
    		  				  <option value="0" selected="selected">Todos</option>
    		  				  <option value="1">Empresa</option>
    		  				  <option value="2">Delivery</option>
        				  </select>
                </div>
              </div>
            </div>
              
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <button type="button" id="btn-html_venta_punto_venta" class="btn btn-default btn-block btn-generar_venta_punto_venta" data-type="html"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <button type="button" id="btn-pdf_venta_punto_venta" class="btn btn-default btn-block btn-generar_venta_punto_venta" data-type="pdf"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <button type="button" id="btn-excel_venta_punto_venta" class="btn btn-default btn-block btn-generar_venta_punto_venta" data-type="excel"><i class="fa fa-file-excel-o color_icon_excel"></i> Excel</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div id="div-venta_punto_venta" class="table-responsive">
            <table id="table-venta_punto_venta" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="text-center">F. Emisión</th>
                  <th class="text-center">Cajero</th>
                  <th class="text-center">Tipo</th>
                  <th class="text-center">Serie</th>
                  <th class="text-center">Número</th>
                  <th class="text-center">Cliente</th>
                  <th class="text-center">T.C.</th>
                  <th class="text-center">Total S/</th>
                  <th class="text-center">Total M. Ex.</th>
                  <th class="text-center">Saldo</th>
                  <th class="text-center">Estado</th>
                  <th class="text-center"></th><!-- imprimir -->
                  <th class="text-center"></th><!-- ver -->
                  <th class="text-center"></th><!-- cobrar -->
                  <?php //echo ($this->empresa->Nu_Tipo_Rubro_Empresa!=3 ? '<th class="text-center"></th>' : ''); ?><!-- cobrar -->
                  <?php echo ($this->empresa->Nu_Tipo_Rubro_Empresa==3 ? '<th class="text-center"></th>' : ''); ?><!-- entregar pedido lavanderia -->
                  <?php echo ($this->empresa->Nu_Tipo_Rubro_Empresa==3 ? '<th class="text-center"></th>' : ''); ?><!-- facturar orden lavanderia -->
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div><!-- /. div-venta_punto_venta -->
          
          <?php
          $attributes = array('id' => 'form-entregar_pedido');
          echo form_open('', $attributes);
          ?>
          <!-- modal entregar pedido -->
          <div class="modal fade modal-entregar_pedido" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="text-center" id="modal-header-entregar_pedido-title"></h4>
                </div>

                <div class="modal-body">
                  <input type="hidden" name="iIdDocumentoCabecera" class="form-control">
                  <input type="hidden" name="iIdDocumentoMedioPago" class="form-control" value="0">
                  <input type="hidden" name="iEstadoLavadoRecepcionCliente" class="form-control" value="3">
                  <input type="hidden" id="hidden-entregar_pedido-fsaldo" name="fSaldoCliente" class="form-control" value="0">
  
                  <div class="row">
                    <div class="col-sm-12">
                      <label id="entregar_pedido-modal-body-cliente"></label>
                    </div>
                    <div class="col-sm-12">
                      <label id="entregar_pedido-modal-body-saldo_cliente"></label>
                    </div>
                  </div>

                  <div class="row div-forma_pago">
                    <div class="col-sm-3">
                      <label>Forma Pago</label>
                      <div class="form-group">
                        <select id="cbo-modal_forma_pago_entrega_pedido" name="iFormaPago" class="form-control" style="width: 100%;"></select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    
                    <div class="col-sm-3 div-modal_datos_tarjeta_credito">
                      <label>Tarjeta</label>
                      <div class="form-group">
                        <select id="cbo-entregar_pedido-modal_tarjeta_credito" name="iTipoMedioPago" class="form-control" style="width: 100%;"></select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    
                    <div class="col-sm-3">
                      <label>Pago cliente</label>
                      <div class="form-group">
                        <input type="tel" class="form-control input-decimal" id="tel-entregar_pedido-fPagoCliente" name="fPagoCliente" value="" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                  </div><!-- ./ row importes -->
                  
                  <div class="row div-modal_datos_tarjeta_credito">
                    <div class="col-sm-3">
                      <div class="form-group">
                        <input type="tel" id="tel-nu_referencia" name="iNumeroTransaccion" class="form-control input-number" value="" maxlength="10" placeholder="No. Operación" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <input type="tel" id="tel-nu_ultimo_4_digitos_tarjeta" name="iNumeroTarjeta" class="form-control input-number" minlength="4" maxlength="4" value="" placeholder="últimos 4 dígitos" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                  </div><!-- ./ row tarjeta -->
                  
                  <div class="row">
                    <div class="col-sm-4">
                      <label>¿Recibe la misma persona?</label>
                      <div class="form-group">
                        <select id="cbo-modal_quien_recibe" name="iCrearEntidad" class="form-control" style="width: 100%;">
                          <option value="1" selected>Si</option>
                          <option value="0">No</option>
                        </select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>

                    <div class="col-sm-8 div-recibe_otra_persona">
                      <label>Nombre(s) y apellidos</label>
                      <div class="form-group">
                        <input type="text" name="sNombreRecepcion" class="form-control" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                  </div><!-- ./ row entrega al cliente -->
                </div>
                <div class="modal-footer">
                  <div class="col-xs-6">
                    <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
                  </div>
                  <div class="col-xs-6">
                    <button type="button" id="btn-entregar_pedido" class="btn btn-primary btn-md btn-block pull-center">Entregar</button>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /. modal entregar pedido -->
          <?php echo form_close(); ?>

          <?php
          $attributes = array('id' => 'form-cobrar_cliente');
          echo form_open('', $attributes);
          ?>
          <!-- modal cobrar cliente -->
          <div class="modal fade modal-cobrar_cliente" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="text-center" id="modal-header-cobrar_cliente-title"></h4>
                </div>

                <div class="modal-body">
                  <input type="hidden" name="iIdDocumentoCabecera" class="form-control">
                  <input type="hidden" name="iIdDocumentoMedioPago" class="form-control" value="0">
                  <input type="hidden" name="iEstadoLavadoRecepcionCliente" class="form-control" value="2">
                  <input type="hidden" id="hidden-cobrar_cliente-fsaldo" name="fSaldoCliente" class="form-control" value="0">
                  
                  <div class="row">
                    <div class="col-sm-12">
                      <label id="cobrar_cliente-modal-body-cliente"></label>
                    </div>
                    <div class="col-sm-12">
                      <label id="cobrar_cliente-modal-body-saldo_cliente"></label>
                    </div>
                  </div>

                  <div class="row div-forma_pago">
                    <div class="col-sm-3">
                      <label>Forma Pago</label>
                      <div class="form-group">
                        <select id="cbo-modal_forma_pago" name="iFormaPago" class="form-control" style="width: 100%;"></select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    
                    <div class="col-sm-3 div-modal_datos_tarjeta_credito">
                      <label>Tarjeta</label>
                      <div class="form-group">
                        <select id="cbo-cobrar_cliente-modal_tarjeta_credito" name="iTipoMedioPago" class="form-control" style="width: 100%;"></select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    
                    <div class="col-sm-3">
                      <label>Pago cliente</label>
                      <div class="form-group">
                        <input type="tel" class="form-control input-decimal" id="tel-cobrar_cliente-fPagoCliente" name="fPagoCliente" value="" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                  </div><!-- ./ row importes -->
                  
                  <div class="row div-modal_datos_tarjeta_credito">
                    <div class="col-sm-3">
                      <div class="form-group">
                        <input type="tel" id="tel-nu_referencia" name="iNumeroTransaccion" class="form-control input-number" value="" maxlength="10" placeholder="No. Operación" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <input type="tel" id="tel-nu_ultimo_4_digitos_tarjeta" name="iNumeroTarjeta" class="form-control input-number" minlength="4" maxlength="4" value="" placeholder="últimos 4 dígitos" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                  </div><!-- ./ row -->
                </div>
                <div class="modal-footer">
                  <div class="col-xs-6">
                    <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
                  </div>
                  <div class="col-xs-6">
                    <button type="button" id="btn-cobrar_cliente" class="btn btn-primary btn-md btn-block pull-center">Cobrar</button>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /. modal cobrar cliente -->
          <?php echo form_close(); ?>
          
          <?php
          $attributes = array('id' => 'form-facturar_orden_lavanderia');
          echo form_open('', $attributes);
          ?>
          <!-- modal facturar orden -->
          <div class="modal fade modal-facturar_orden_lavanderia" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="text-center" id="modal-header-facturar_orden_lavanderia-title"></h4>
                </div>

                <div class="modal-body">
                  <input type="hidden" name="iIdDocumentoCabecera" class="form-control">
                  <input type="hidden" name="iIdDocumentoMedioPago" class="form-control" value="0">
                  <input type="hidden" name="iEstadoLavadoRecepcionCliente" class="form-control" value="2">
                  <input type="hidden" id="hidden-facturar_oden_lavanderia-fsaldo" class="form-control" value="0">
                  <input type="hidden" id="hidden-facturar_oden_lavanderia-iEstadoLavadoRecepcionCliente" name="Nu_Estado_Lavado_Recepcion_Cliente" class="form-control" value="0">
                  
                  <div class="row">
                    <div class="col-sm-12">
                      <label id="facturar_orden_lavanderia-modal-body-cliente"></label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-3">
                      <label>Documento</label>
                      <div class="form-group">
                        <select id="modal-cbo-tipo_documento" name="tipo_documento" class="form-control">
                          <option value="4" data-nu_cantidad_caracteres="8">Boleta</option>
                          <option value="3" data-nu_cantidad_caracteres="11">Factura</option>
                        </select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    
                    <div class="col-sm-3">
                      <label id="label-tipo_documento_identidad" title="Si existe cliente ingresar Nombre / # Doc. Ident.">DNI</label>
                      <div class="form-group">
                        <input type="hidden" id="txt-ID_Tipo_Documento_Identidad" name="ID_Tipo_Documento_Identidad" class="form-control">
                        <input type="hidden" id="txt-AID" name="AID" class="form-control">
                        <input type="hidden" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control">
                        <input type="text" id="txt-ACodigo" name="Nu_Documento_Identidad" class="form-control autocompletar input-Mayuscula input-codigo_barra hotkey-cancelar_venta hotkey-cobrar_cliente" onkeyup="api_sunat_reniec(this.value);" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="" title="Si existe cliente ingresar Nombre / # Doc. Ident." maxlength="8" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <label>Nombre <span id="span-no_nombres_cargando"></span></label>
                      <div class="form-group">
                        <input type="hidden" id="hidden-nu_numero_documento_identidad" class="form-control" value="">
                        <input type="hidden" id="hidden-estado_entidad" name="Nu_Estado_Entidad" class="form-control" value="0">
                        <input type="text" id="txt-ANombre" name="No_Entidad" class="form-control hotkey-cobrar_cliente hotkey-cancelar_venta hotkey-limpiar_item hotkey-focus_item" autocomplete="off">
                      </div>
                    </div>
                    
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label id="label_correo">Correo</label>
                        <input type="email" id="txt-Txt_Email_Entidad_Cliente" name="Txt_Email_Entidad" placeholder="opcional" class="form-control" autocomplete="off">
                        <span class="hide" id="span-email" style="color: #dd4b39;">Ingresa un email válido</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <div class="col-xs-6">
                    <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
                  </div>
                  <div class="col-xs-6">
                    <button type="button" id="btn-facturar_orden_lavanderia" class="btn btn-primary btn-md btn-block pull-center">Facturar</button>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /. modal facturar orden -->
          <?php echo form_close(); ?>          
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->