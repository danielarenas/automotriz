<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    
    <!-- Row Lista de POS -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header">
            <div class="row">
              <div class="col-xs-12">
                <h4 id="h4-verificar_autorizacion_venta"></h4>
                <ul class="list-group row ul-lista_pos">
                </ul>
                <h4 id="h4-msg_punto_venta"></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /. Row -->
    <!-- Modal Personal -->
    <?php
    $attributes = array('id' => 'form-matricula_personal_apertura_caja');
    echo form_open('', $attributes);
    ?>
    <div class="modal fade modal-personal" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" id="hidden-id_pos" value="">
            <input type="hidden" id="hidden-id_personal" value="">
            <input type="hidden" id="hidden-id_tipo_operacion_caja" value=""><!-- Apertura de caja -->

            <div class="row">  
              <div class="col-xs-6 col-sm-8">
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <label>Nota</label>
                    <textarea name="area-txt_nota_caja" rows="4" class="form-control hotkey-btn-add_matricular_personal_apertua_caja" placeholder="Ingresar datos (opcional)"></textarea>
                  </div>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4">
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <input type="password" id="tel-nu_documento_identidad_personal" class="form-control input-decimal hotkey-btn-add_matricular_personal_apertua_caja" value="" autocomplete="off" placeholder="Ingresar PIN" maxlength="4">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <select id="cbo-moneda" class="form-control select2" style="width: 100%;"></select>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <input type="text" id="txt-ss_apertura_caja" class="form-control input-decimal hotkey-btn-add_matricular_personal_apertua_caja" maxlength="13" autocomplete="off" placeholder="Ingresar monto">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-sm-6">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir (ESC)</button>
                </div>
              </div>
              <div class="col-xs-6 col-sm-6">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-save_personal" class="btn btn-primary btn-md btn-block pull-center">Guardar</button>
                </div>
              </div>
            </div>
            <!-- /. Row Input Personal -->
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /. Modal Personal -->
    <?php echo form_close(); ?>
    <!-- Modal Verificar Personal de inicio de caja por PIN -->
    <div class="modal fade modal-inicio_sesion_caja_x_personal" id="modal-default">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-body">
            <input type="hidden" id="hidden-id_matricula_empleado" value="">
            <input type="hidden" id="hidden-id_moneda_caja_pos" value="">
            <div class="row">
              <div class="col-xs-4"></div>
              <div class="col-xs-4">
                <label>PIN</label>
                <div class="form-group">
                  <input type="password" id="tel-Nu_Pin_Caja" class="form-control input-number hotkey-btn-add_matricular_personal_apertua_caja" value="" autocomplete="off" placeholder="Ingresar número" maxlength="4">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              <div class="col-xs-4"></div>

              <div class="col-xs-12">
                <div class="form-group">
                  <button type="button" id="btn-ingresar_punto_venta" class="btn btn-primary btn-md btn-block pull-center">Ingresar</button>
                </div>
              </div>
              <div class="col-xs-12">
                <div class="form-group">
                  <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir (ESC)</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /. Modal Verificar Personal de inicio de caja por PIN -->
  </section>
  <!-- /. Main content -->
</div>
<!-- /.content-wrapper -->