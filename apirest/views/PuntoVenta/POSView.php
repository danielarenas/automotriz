<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>

  <!-- Main content -->
  <section class="content" style="background: #eeeeee;">
    <div class="row">
      <?php
      if ( isset($this->session->userdata['arrDataPersonal']) && $this->session->userdata['arrDataPersonal']['sStatus']=='success' ) { ?>
      <!-- Lateral Izquierda -->
      <div class="col-xs-12 col-sm-5">
        <div class="row" style="display:none; background: #fcfcfc !important; padding: 10px; margin-bottom: 15px; margin-right: 0px; border-radius: 0.3em;">
          <?php
           if ( isset($this->session->userdata['arrDataPersonal']) && $this->session->userdata['arrDataPersonal']['sStatus']=='success' ) {
            $arrDataPersonal = $this->session->userdata['arrDataPersonal']['arrData'][0];
          ?>
          <div class="col-xs-4">
            <label>Cajero(a)</label>
            <div class="form-group">
              <input type="hidden" id="hidden-id_matricula_personal" value="<?php echo $arrDataPersonal->ID_Matricula_Empleado; ?>">
              <label style="font-weight: normal"><?php echo '(' . $arrDataPersonal->Nu_Caja . ') ' . $arrDataPersonal->No_Entidad; ?></label>
            </div>
          </div>

          <div class="col-xs-4">
            <label>F. Apertura Caja</label>
            <div class="form-group">
              <label style="font-weight: normal"><?php echo $arrDataPersonal->Fe_Matricula; ?></label>
            </div>
          </div>

          <div class="col-xs-4">
            <div class="form-group">
              <input type="hidden" id="hidden-id_moneda_caja_pos" value="<?php echo $this->session->userdata['arrDataPersonal']['arrData']['ID_Moneda']; ?>">
            </div>
          </div>
          <?php } ?>
        </div>

        <div class="row" style="background: #fcfcfc !important; padding: 10px; margin-bottom: 20px; margin-right: 0px; border-radius: 0.3em;">
          <div class="col-xs-6">
            <label>Almacén</label>
            <div class="form-group">
              <select id="cbo-almacen" class="form-control required" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
              <input type="hidden" id="hidden-id_almacen" value="<?php echo $this->empresa->ID_Almacen; ?>">
            </div>
          </div>
          <div class="col-xs-6">
            <label>Lista de Precio</label>
            <div class="form-group">
              <select id="cbo-lista_precios" class="form-control" style="width: 100%;"></select>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label>Producto</label>
              <input type="hidden" id="txt-Nu_Compuesto" class="form-control" value="">
              <input type="hidden" id="txt-ID_Producto" class="form-control">
              <input type="hidden" id="txt-Nu_Codigo_Barra" class="form-control">
              <input type="hidden" id="txt-Ss_Precio" class="form-control">
              <input type="hidden" id="txt-Ss_Precio_Interno" class="form-control">
              <input type="hidden" id="txt-ID_Impuesto_Cruce_Documento" class="form-control">
              <input type="hidden" id="txt-Nu_Tipo_Impuesto" class="form-control">
              <input type="hidden" id="txt-Ss_Impuesto" class="form-control">
              <input type="hidden" id="txt-Qt_Producto" class="form-control">
              <input type="hidden" id="txt-nu_tipo_item" class="form-control">
              <div class="input-group">
                <input type="text" id="txt-No_Producto" class="form-control autocompletar_lector_codigo_barra hotkey-limpiar_item hotkey-cancelar_venta" data-global-class_method="AutocompleteController/getAllProduct" data-global-table="producto" onkeyup="autocompleteItemsAlternativos(this.value);" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off">
                <div class="input-group-btn">
                  <button type="button" class="btn btn-primary" id="btn-crear_item" title="Crear item" alt="Crear item"><i class="fa fa-plus-circle" title="Crear item" alt="Crear item"></i></button>
                </div>
                <!-- /btn-group -->
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-12">
            <label>Descuento</label>
            <div class="form-group">
              <select id="cbo-descuento" class="form-control hotkey-cobrar_cliente">
                <option value="1" selected>Importe</option>
                <option value="2">Porcentaje</option>
              </select>
            </div>
          </div>
        </div>

        <div class="col-xs-12" style="height: 312px; overflow-y: auto; display:none">
          <div class="col-xs-12">
            <ul class="list-group row div-lista_cuadro_items"></ul>
          </div>
        </div>
        <!-- ./ row-lista-productos -->
        
        <!-- listar productos alternativos -->
        <?php
        $sCssDisplay='style="display:none"';
        if ( $this->empresa->Nu_Tipo_Rubro_Empresa==1 ){//1 = Farmacia
          $sCssDisplay='';
        }?>
        <div class="row" <?php echo $sCssDisplay; ?>>
          <div class="col-xs-12 div-items_alternativos" style="height: 210px; overflow-y: auto;"><!-- 275 -->
            <table id="table-items_alternativos" class="table table-striped">
              <thead>
                <tr>
                  <th colspan="3" style="padding-top: 5px; padding-bottom: 5px;">Porductos Alternativos</th>
                </tr>
                <tr>
                  <th style="padding-top: 5px; padding-bottom: 5px;">Cantidad</th>
                  <th style="padding-top: 5px; padding-bottom: 5px;">Nombre</th>
                  <th style="padding-top: 5px; padding-bottom: 5px;">Precio</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <br>
          <br>
          <br>
        </div>
        <!-- ./ listar productos alternativos -->
        <!-- div row alertas -->
        <div class="row" id="div-row-alertas"></div>
      </div>
      <!-- ./ Lateral Izquierda -->

      <!-- Lateral Derecha -->
      <div class="col-xs-12 col-sm-7 sidenav" style="background: #fcfcfc !important; padding: 10px; margin-bottom: 15px; border-radius: 0.3em;">
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-3">
            <label>Cliente</label>
            <div class="form-group">
              <select id="cbo-tipo_cliente" name="tipo_cliente" class="form-control hotkey-cobrar_cliente" style="width: 100%;">
                <?php echo $this->empresa->Nu_Tipo_Rubro_Empresa!=3 ? '<option value="1" title="Solo para boletas y ventas menores < 700.00">Rápido (Boleta)</option>' : ''; ?>
                <option value="3">Nuevo / Existe</option>
              </select>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-sm-4 col-md-3 div-nuevo_cliente">
            <label>Comprobante</label>
            <div class="form-group">
              <select id="cbo-tipo_documento" name="tipo_documento" class="form-control hotkey-cobrar_cliente" style="width: 100%;">
                <?php echo $this->empresa->Nu_Tipo_Rubro_Empresa!=3 ? '<option value="4" data-nu_cantidad_caracteres="8">Boleta (DNI)</option>' : ''; ?>
                <?php echo $this->empresa->Nu_Tipo_Rubro_Empresa!=3 ? '<option value="3" data-nu_cantidad_caracteres="11">Factura</option>' : ''; ?>
                <?php echo $this->empresa->Nu_Tipo_Rubro_Empresa!=3 ? '<option value="5" data-nu_cantidad_caracteres="15">Boleta (OTROS)</option>' : ''; ?>
                <option value="2"><?php echo ($this->empresa->Nu_Tipo_Rubro_Empresa!=3 ? 'Interno' : 'Orden Lavado') ?></option>
              </select>
            </div>
          </div>

          <div class="col-xs-6 col-sm-4 col-md-3 div-nuevo_cliente">
            <label id="label-tipo_documento_identidad" title="Si existe cliente ingresar Nombre / # Doc. Ident.">DNI</label>
            <div class="form-group">
              <input type="hidden" id="txt-ID_Tipo_Documento_Identidad" class="form-control">
              <input type="hidden" id="txt-AID" name="AID" class="form-control">
              <input type="hidden" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control">
              <input type="text" id="txt-ACodigo" name="Nu_Documento_Identidad" class="form-control autocompletar input-Mayuscula input-codigo_barra hotkey-cancelar_venta hotkey-cobrar_cliente" onkeyup="api_sunat_reniec(this.value);" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="" title="Si existe cliente ingresar Nombre / # Doc. Ident." maxlength="8" autocomplete="off">
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-sm-4 col-md-3 div-nuevo_cliente">
            <div class="form-group">
              <label>Celular</label>
              <input type="tel" id="txt-Nu_Celular_Entidad_Cliente" name="Nu_Celular_Entidad" class="form-control hotkey-cobrar_cliente hotkey-cancelar_venta hotkey-limpiar_item hotkey-focus_item" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off" placeholder="opcional">
              <span class="hide" id="span-celular" style="color: #dd4b39;">Ingresa un celular válido</span>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-4 col-md-6 div-nuevo_cliente">
            <div class="form-group">
              <label id="label_correo">Correo</label>
              <input type="email" id="txt-Txt_Email_Entidad_Cliente" name="Txt_Email_Entidad" placeholder="opcional" class="form-control hotkey-cobrar_cliente hotkey-cancelar_venta hotkey-limpiar_item hotkey-focus_item" autocomplete="off">
              <span class="hide" id="span-email" style="color: #dd4b39;">Ingresa un email válido</span>
            </div>
          </div>

          <div class="col-xs-12 col-sm-4 col-md-6 div-nuevo_cliente">
            <label>Nombre <span id="span-no_nombres_cargando"></span></label>
            <div class="form-group">
              <input type="hidden" id="hidden-nu_numero_documento_identidad" class="form-control" value="">
              <input type="hidden" id="hidden-estado_entidad" class="form-control" value="0">
              <input type="text" id="txt-ANombre" name="No_Entidad" class="form-control hotkey-cobrar_cliente hotkey-cancelar_venta hotkey-limpiar_item hotkey-focus_item" autocomplete="off">
            </div>
          </div>

          <div class="col-xs-5 col-sm-5 col-md-5" style="display:none">
            <label>Dirección</label>
            <div class="form-group">
              <label id="label-txt_direccion" style="font-weight: normal"></label>
            </div>
          </div>

          <div class="col-xs-2 col-sm-5 col-md-5" style="display:none">
            <label>Estado</label>
            <div class="form-group">
              <label id="label-txt_estado_cliente" style="font-weight: normal"></label>
            </div>
          </div>
        </div>
        
        <!-- listar productos agregados 275px -->
        <div class="row">
          <div class="col-xs-12 div-lista_compra_items" style="height: 317px; overflow-y: auto;"><!-- 350px -->
            <table id="table-detalle_productos_pos" class="table table-striped">
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- ./ listar productos agregados -->
        <?php
        $sCssDisplayLavanderia='style="display:none"';
        if ( $this->empresa->Nu_Tipo_Rubro_Empresa==3 ){//3 = Lavandería
          $sCssDisplayLavanderia='';
        }
        ?>
        <div class="row" <?php echo $sCssDisplayLavanderia; ?>>
          <div class="col-xs-6 col-md-5">
            <label>Envío</label>
            <div class="form-group">
              <select id="cbo-tipo_envio_lavado" name="Nu_Transporte_Lavanderia_Hoy" class="form-control">
                <option value="1" selected>Transporte Vapi</option>
                <option value="2">Servicio Tercerizado Externo</option>
                <option value="3">Servicio Tercerizado Interno</option>
                <option value="4">Empresa</option>
              </select>
            </div>
          </div>
          <div class="col-xs-6 col-md-3">
            <label>F. Entrega</label>
            <div class="input-group date">
              <input type="text" id="txt-fe_entrega" class="form-control input-datepicker-today-to-more required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
            </div>
          </div>
          <div class="col-xs-12 col-md-4">
            <label>Recepción</label>
            <div class="form-group">
              <select id="cbo-recepcion" class="form-control">
                <option value="1" selected>Empresa</option>
                <option value="2">Delivery</option>
              </select>
            </div>
          </div>
        </div>
        <!-- Totales -->
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <button type="button" id="btn-pagar" class="btn btn-primary btn-lg btn-block">
                <div class="col-xs-7 text-left">
                  <h4>COBRAR</h4>
                </div>
                <div class="col-xs-5 text-right">
                  <input type="hidden" class="hidden-gravada" value="" autocomplete="off">
                  <input type="hidden" class="hidden-exonerada" value="" autocomplete="off">
                  <input type="hidden" class="hidden-inafecta" value="" autocomplete="off">
                  <input type="hidden" class="hidden-grautita" value="" autocomplete="off">
                  <input type="hidden" class="input-total_detalle_productos_pos" value="" autocomplete="off">
                  <h4 class="label-total_detalle_productos_pos">0.00</h4>
                </div>
              </button>
            </div>
          </div>
        </div>
        <!-- ./ Totales -->

        <!-- Atajos de teclado -->
        <div class="row">
          <div class="col-xs-6">
            <button type="button" id="btn-atajos_teclado" class="btn btn-link btn-lg btn-block">Atajos de teclado</button>
          </div>
          <div class="col-xs-6">
            <button type="button" id="btn-add_nota_global" class="btn btn-link btn-lg btn-block">Nota</button>
          </div>
        </div>
        <!-- ./ Atajos de teclado -->
      </div>
      <!-- /. Lateral Derecha -->
    </div>
    <!-- /.row -->
    <!-- modal atajos de teclado -->    
    <div class="modal fade modal-atajos_teclado" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="text-center">Atajos de teclado</h4>
            <ul>
              <li>[ESC] => Cancelar Venta</li>
              <li>[F2] => Limpiar caja producto</li>
              <li>[F4] => Establecer el cursor en la búsqueda de productos</li>
              <li>[Intro / Enter] => Cobrar / Agregar forma pago del cliente / Generar comprobante</li>
            </ul>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div><!-- /.modal atajos de teclado -->
    
    <!-- modal Nota o Glosa -->    
    <div class="modal fade modal-add_nota_global" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="text-center">Nota</h4>
            <textarea name="Txt_Glosa" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div><!-- /.modal Nota o Glosa -->
    
    <?php
    $attributes = array('id' => 'form-crear_item');
    echo form_open('', $attributes);
    ?>
    <!-- formulario crear item -->    
    <div class="modal fade modal-crear_item" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="text-center">Crear Item</h4>
          </div>

          <div class="modal-body">
            <div class="row">
              <div class="col-xs-3">
                <label>Grupo <span class="label-advertencia">*</span></label>
                <div class="form-group div-modal-grupoItem">
                  <select id="cbo-modal-grupoItem" class="form-control">
                    <option value="1">Producto</option>
                    <option value="0">Servicio</option>
                  </select>
                </div>
              </div>
              
              <div class="col-xs-4 div-Producto">
                <div class="form-group">
                  <label>Tipo Producto</label>
                  <select id="cbo-modal-tipoItem" class="form-control"></select>
                </div>
              </div>
              
              <div class="col-xs-5">
                <label>Código barra / UPC <span class="label-advertencia">*</span></label>
                <div class="form-group">
                  <input type="text" id="txt-modal-upcItem" class="form-control input-codigo_barra input-Mayuscula" placeholder="Ingresar código" maxlength="20" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-8" style="display:none">
                <label>Producto SUNAT <span class="label-advertencia">*</span></label>
                <div class="form-group">
                  <input type="hidden" id="hidden-ID_Tabla_Dato" name="ID_Tabla_Dato" class="form-control">
                  <input type="text" id="txt-No_Descripcion" name="No_Descripcion" class="form-control autocompletar_producto_sunat" placeholder="Ingresar nombre" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-8">
                <label>Nombre <span class="label-advertencia">*</span></label>
                <div class="form-group">
                  <textarea name="textarea-modal-nombreItem" class="form-control required" placeholder="Ingresar nombre" maxlength="250" autocomplete="off" aria-required="true"></textarea>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-2">
                <label>Precio <span class="label-advertencia">*</span></label>
                <div class="form-group">
                  <input type="tel" id="txt-modal-precioItem" class="form-control required input-decimal" maxlength="13" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-2">
                <label>Costo</label>
                <div class="form-group">
                  <input type="tel" id="txt-modal-costoItem" class="form-control required input-decimal" maxlength="13" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6">
                <label>Impuesto <span class="label-advertencia">*</span></label>
                <div class="form-group">
                  <select id="cbo-modal-impuestoItem" class="form-control"></select>
                </div>
              </div>
              
              <div class="col-xs-6">
                <label>Unidad Medida <span class="label-advertencia">*</span></label>
                <div class="form-group">
                  <select id="cbo-modal-unidad_medidaItem" class="form-control"></select>
                </div>
              </div>
            </div>
          </div>
            
          <div class="modal-footer">
            <div class="col-xs-6">
              <button type="button" id="btn-modal-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
            </div>
            <div class="col-xs-6">
              <button type="button" id="btn-modal-crear_item" class="btn btn-primary btn-md btn-block pull-center btn-generar_pedido" data-type="generar_ticket"><i class="fa fa-save"></i> Guardar</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div><!-- /.modal crear item -->
    <!-- /. formulario crear item -->
    <?php echo form_close(); ?>

    <!-- Modal delivery -->
    <div class="modal fade modal-delivery" id="modal-default">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="text-center">Delivery</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-xs-12">
                <label>Transporte</label>
                <div class="form-group">
                  <select id="cbo-transporte" class="form-control"></select>
                </div>
              </div>
              <div class="col-xs-12">
                <label>Dirección</label>
                <div class="form-group">
                  <textarea name="Txt_Direccion_Delivery" class="form-control"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="col-xs-12">
              <div class="form-group">
                <button type="button" id="btn-salir" class="btn btn-primary btn-md btn-block pull-center" data-dismiss="modal">Aceptar</button>
              </div>
            </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /. Modal delivery -->    
    <!-- modal informacion del item -->
    <div class="modal fade modal-info_item" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="text-center" id="modal-header-info_item-title"></h4>
          </div>
          <div class="modal-body" id="modal-body-info_item"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal informacion del item -->
    <?php
    $attributes = array('id' => 'form-modal_venta_pos_forma_pago');
    echo form_open('', $attributes);
    ?>
    <!-- Forma Pago POS Modal -->
    <div class="modal fade modal_forma_pago" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-3">
                <label>Forma Pago</label>
                <div class="form-group">
                  <select id="cbo-modal_forma_pago" class="form-control" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-sm-3 div-modal_datos_tarjeta_credito">
                <label>Tarjeta Crédito</label>
                <div class="form-group">
                  <select id="cbo-modal_tarjeta_credito" class="form-control" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-sm-3">
                <label>Pago cliente</label>
                <div class="form-group">
                  <input type="tel" class="form-control input-decimal send-forma_pago_pos input-modal_monto hotkey-cancelar_venta" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-sm-3">
                <label>&nbsp;</label>
                <div class="form-group">
                  <button type="button" id="btn-add_forma_pago" class="btn btn-primary btn-md btn-block">Agregar</button>
                </div>
              </div>
            </div><!-- ./ row -->
            
            <div class="row div-modal_datos_tarjeta_credito">
              <div class="col-sm-3">
                <label>Opcional</label>
                <div class="form-group">
                  <input type="tel" id="tel-nu_referencia" class="form-control input-number" value="" maxlength="10" placeholder="No. Operación" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              <div class="col-sm-3">
                <label>Opcional</label>
                <div class="form-group">
                  <input type="tel" id="tel-nu_ultimo_4_digitos_tarjeta" class="form-control input-number" minlength="4" maxlength="4" value="" placeholder="últimos 4 dígitos" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
            </div>

            <div class="row div-billete_soles">
              <div class="col-sm-3">
                <div class="form-group">
                  <button type="button" class="btn btn-outline-secondary btn-block billete-soles" value="10">S/ 10.00</button>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <button type="button" class="btn btn-outline-secondary btn-block billete-soles" value="20">S/ 20.00</button>
                </div>
              </div>
                
              <div class="col-sm-3">
                <div class="form-group">
                  <button type="button" class="btn btn-outline-secondary btn-block billete-soles" value="50">S/ 50.00</button>
                </div>
              </div>
                
              <div class="col-sm-3">
                <div class="form-group">
                  <button type="button" class="btn btn-outline-secondary btn-block billete-soles" value="100">S/ 100.00</button>
                </div>
              </div>
            </div><!-- ./ row -->
            
            <div class="row">
              <div class="col-sm-12">
                <div id="div-modal_forma_pago" class="table-responsive">
                  <table id="table-modal_forma_pago" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th style="display:none;" class="text-left"></th>
                        <th style="display:none;" class="text-left"></th>
                        <th style="display:none;" class="text-left"></th>
                        <th class="text-center">F. PAGO</th>
                        <th class="text-center">TARJETA</th>
                        <th class="text-center">MONTO</th>
                        <th style="width: 20px; text-align:center;"><i class="fa fa-trash-o fa-lg icon-clear_all_forma_pago_pos" style="cursor: pointer;"></i></th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                      <tr>
                        <th class="text-right" colspan="2">PAGO DEL CLIENTE</th>
                        <th class="text-right">
                          <input type="hidden" class="input-modal_forma_pago_monto_total" value="0.00" autocomplete="off">
                          <label class="label-modal_forma_pago_monto_total">0.00</label>
                        </th>
                      </tr>
                      <tr>
                        <th class="text-right" colspan="2">TOTAL A COBRAR</th>
                        <th class="text-right">
                          <input type="hidden" class="input-total_detalle_productos_pos" value="0.00" autocomplete="off">
                          <label class="label-total_detalle_productos_pos">0.00</label>
                        </th>
                      </tr>
                      <tr>
                        <th class="text-right th-label-vuelto" colspan="2">VUELTO</th>
                        <th class="text-right th-label-vuelto">
                          <input type="hidden" class="input-vuelto_pos" value="0.00" autocomplete="off">
                          <label class="label-vuelto_pos">0.00</label>
                        </th>
                      </tr>
                      <tr>
                        <th class="text-right th-label-saldo" colspan="2">SALDO</th>
                        <th class="text-right th-label-saldo">
                          <input type="hidden" class="input-saldo_pos_cliente" value="0.00" autocomplete="off">
                          <label class="label-saldo_pos_cliente">0.00</label>
                        </th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="col-xs-6">
              <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
            </div>
            <div class="col-xs-6">
              <button type="button" id="btn-ticket" class="btn btn-primary btn-md btn-block pull-center btn-generar_pedido" data-type="generar_ticket">Generar comprobante</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /. Forma Pago POS modal -->
    <?php echo form_close(); ?>
    <?php } else { ?>
      <div class="col-xs-12">
        Primero debe aperturar caja
      </div>
    <?php } ?>
  </section>
  <!-- /. Main content -->
</div>
<!-- /.content-wrapper -->