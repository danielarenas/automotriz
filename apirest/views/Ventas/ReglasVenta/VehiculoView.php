<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
      <!-- ./New box-header -->
    </div>
    <?php
    if ( !empty($sStatus) ){
      $sClassModal = 'success';
      $sMessage = 'Datos cargados satisfactoriamente';
      if ( (int)$iCantidadNoProcesados > 0 ){
        $sMessage .= '. Pero tiene ' . $iCantidadNoProcesados . ' registro(s) no procesados';
      }
      if ( $sStatus == 'error-sindatos' ) {
        $sMessage = 'Llenar los campos obligatorios o los valores no son iguales a las columna del excel';
        $sClassModal = 'danger';  
      } else if ( $sStatus == 'error-bd' ) {
        $sMessage = 'Problemas al generar excel';
        $sClassModal = 'danger';  
      } else if ( $sStatus == 'error-archivo_no_existe' ) {
        $sMessage = 'El archivo no existe';
        $sClassModal = 'danger';  
      } else if ( $sStatus == 'error-copiar_archivo' ) {
        $sMessage = 'Error al copiar archivo al servidor';
        $sClassModal = 'danger';  
      }
    ?>
      <div class="modal fade in modal-<?php echo $sClassModal; ?>" id="modal-message_excel" role="dialog" style="display: block;">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo $sMessage; ?></h4>
            </div>
            <div class="modal-footer">
              <button type="button" id="btn-cerrar_modal_excel" class="btn btn-outline pull-right" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-md-3">
                <div class="form-group">
    		  				<select id="cbo-Filtros_Productos" name="Filtros_Productos" class="form-control">
    		  				  <option value="Cliente">Nombre Cliente</option>
                    <option value="Placa">Placa</option>
                    <option value="Year">Año</option>
                    <option value="Marca">Marca</option>
                    <option value="Modelo">Modelo</option>
    		  				</select>
                </div>
              </div>
              
              <div class="col-md-5">
                <div class="form-group">
                  <input type="text" id="txt-Global_Filter" name="Global_Filter" class="form-control" maxlength="250" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                <button type="button" class="btn btn-success btn-block" onclick="agregarProducto()"><i class="fa fa-plus-circle"></i> Agregar</button>
                <?php endif; ?>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                  <!--<button type="button" class="btn btn-default btn-block" onclick="importarExcelProductos()"><i class="fa fa-file-excel-o color_icon_excel"></i> Importar</button>-->
                <?php endif; ?>
              </div>
            </div>
          </div>
          <!-- ./box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-Producto" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Placa</th>
                  <th>Año</th>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Combustible</th>
                  <th>VIN</th>
                  <th>Nro. Motor</th>
                  <th>Kilometraje</th>
                  <th>Color</th>
                  <th>Stock</th>
                  <th>Estado</th>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                    <!--<th class="no-sort"></th>-->
                  <?php endif; ?>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-Vehiculo');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" id="txt-EID_Entidad" name="EID_Entidad" class="form-control">
          	  <input type="hidden" id="txt-EID_Producto" name="EID_Producto" class="form-control">
          	  <input type="hidden" id="ID_Tipo_Producto" name="ID_Tipo_Producto" class="form-control" value="3"><!--3 = carro -->
    	  
              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <div class="form-group">
                    <label>Nombre Cliente</label>
                    <input type="hidden" id="txt-AID" name="AID" class="form-control required">
                    <input type="text" id="txt-ANombre" name="ID_Entidad" class="form-control autocompletar " data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre" value="" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Placa</label>
                    <input type="text" name="No_Placa_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="6">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-md-3">
                  <label>Api</label>
                  <div class="form-group">
                    <button type="button" id="btn-cloud-api_sunarp" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div><!--row-->
              
              <div class="row">
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Año Fabricación</label>
                    <input type="text" name="No_Year_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="4">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Marca</label>
                    <input type="text" name="No_Marca_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Modelo</label>
                    <input type="text" name="No_Modelo_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Tipo Combustible</label>
                    <input type="text" name="No_Tipo_Combustible_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div><!--row-->
              
              <div class="row">
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>VIN</label>
                    <input type="text" name="No_Vin_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Nro. Serie Motor</label>
                    <input type="text" name="No_Motor" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Kilometraje</label>
                    <input type="text" name="Nu_Kilometraje" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="16">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-md-3">
                  <div class="form-group">
                    <label>Color</label>
                    <input type="text" name="No_Color_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="20">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div><!--row-->

      			  <div class="row">
      			    <br>
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->