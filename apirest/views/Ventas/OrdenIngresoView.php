<!--  M0009 - Inicio -->
<script>  
  /* document.body.onkeydown = function(e){
     
     if (e.keyCode === 13) {
      e.preventDefault();
       document.getElementById("btn-filter").click();  
     }
   };
   */ 

   // REQ-4 - INICIO 
 function EnterFunction(event) {
  // onkeypress="EnterFunction(event)" 
  
    if(event.which === 13){  
      event.preventDefault();
      document.getElementById("btn-filter").click();
    } 
  }



   // REQ-4 - FIN
      
</script> 

   <!--  M0009 - Fin -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio Ingreso</label>
                  <div class="input-group date">
                     <!--  REQ-4 - Inicio -->
                    <input type="text" id="txt-Filtro_Fe_Inicio" onkeypress="EnterFunction(event)"  class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  <!--  REQ-4 - Fin-->
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin Ingreso</label>
                  <div class="input-group date">
                     <!--  REQ-4 - Inicio -->
                    <input type="text" id="txt-Filtro_Fe_Fin" onkeypress="EnterFunction(event)"  class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  <!--  REQ-4 - Fin-->
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Area de Ingreso</label>
    		  				<select id="cbo-Filtro_area_ingreso" class="form-control">
    		  				  <option value="" selected>Todos</option>
        				    <option value="5">Registrado</option>
        				    <option value="0">Entregado</option>
        				    <option value="1">Revisado</option>
        				    <option value="2">Aceptado</option>
        				    <option value="3">Rechazado</option>
        				  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>Serie</label>
                  <select id="cbo-Filtro_serie_documento" class="form-control required" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-4 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <!--  REQ-4 - Inicio -->
                  <input type="tel" id="txt-Filtro_NumeroDocumento" onkeypress="EnterFunction(event)"  class="form-control input-number" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                <!--  REQ-4 - Fin-->
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Estado</label>
    		  				<select id="cbo-Filtro_Estado" class="form-control">
    		  				  <option value="" selected>Todos</option>
        				    <option value="1">En Taller</option>
        				    <option value="2">Entregado</option>
        				    <option value="3">Facturado</option>
        				  </select>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Placa</label>
                  <!--  REQ-4 - Inicio -->
                  <input type="tel" id="txt-Filtro_Placa" onkeypress="EnterFunction(event)"  class="form-control" placeholder="Buscar" value="" autocomplete="off">
                <!--  REQ-4 - Fin-->
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Marca</label>
                  <!--  REQ-4 - Inicio -->
                  <input type="tel" id="txt-Filtro_Marca" onkeypress="EnterFunction(event)"  class="form-control" placeholder="Buscar" value="" autocomplete="off">
                <!--  REQ-4 - Fin-->
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Modelo</label>
                  <!--  REQ-4 - Inicio -->
                  <input type="tel" id="txt-Filtro_Modelo" onkeypress="EnterFunction(event)"  class="form-control" placeholder="Buscar" value="" autocomplete="off">
                <!--  REQ-4 - Fin-->
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Kilometraje</label>
                  <!--  REQ-4 - Inicio -->
                  <input type="tel" id="txt-Filtro_Kilometraje" onkeypress="EnterFunction(event)"  class="form-control" placeholder="Buscar" value="" autocomplete="off">
                <!--  REQ-4 - Fin-->
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>Combustible</label>
                  <select id="cbo-filtro_nivel_combustible" class="form-control required" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Estado Facturacion</label>
    		  				<select id="cbo-Filtro_Estado_Facturacion" class="form-control">
    		  				  <option value="" selected>Todos</option>
        				    <option value="0">Pendiente</option>
        				    <option value="1">Facturado</option>
        				    <option value="2">Anulado</option>
                    <option value="3">Reclamo</option>
        				  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <label>Busca por cualquier columna</label>
                    <input type="text" id="myInput" placeholder="Busca por cualquier columna" title="Busca por cualquier columna" class="form-control">
                </div>
              </div>

              <div class="col-xs-6 col-sm-8 col-md-8">
                <div class="form-group">
                  <label>Cliente</label>
                  <!--  REQ-4 - Inicio -->
                  <input type="text" id="txt-Filtro_Entidad" onkeypress="EnterFunction(event)"  class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar Nombre / Nro. Documento de Identidad" value="" autocomplete="off">
                  <!--  REQ-4 - Fin-->
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                    <button type="button" class="btn btn-success btn-block" onclick="agregarOrdenIngreso()"><i class="fa fa-plus-circle"></i> Agregar</button>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-OrdenIngreso" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="no-sort">ID</th>
                  <th class="no-sort">Tipo</th>
                  <th class="no-sort">Serie</th>
                  <th class="no-sort">Número</th>
                  <th class="no-sort">F. Ingreso</th>
                  <th class="no-sort">F. Prevista Entrega</th>
                  <th class="no-sort">F. Salida</th>
                  <th class="no-sort_left">Cliente</th>
                  <th class="no-sort_left">Placa</th>
                  <th class="no-sort_left">Marca</th>
                  <th class="no-sort_left">Modelo</th>
                  <th class="no-sort_left">Kilometraje</th>
                  <th class="no-sort_left">Nivel Combustible</th>
                  <th class="no-sort">Estado</th>
                  <th class="no-sort">Etapa</th>
                  <th class="no-sort">Cambiar Etapa</th>
                  <th class="no-sort">Dias en Taller</th>
                  <th class="no-sort">Estado Facturacion</th>
                  <th class="no-sort">Presupuesto</th>
                  <th class="no-sort">Total Presupuesto</th>
                  <th class="no-sort">O.T.</th>
                  <th class="no-sort">Facturado</th>
                  <th class="no-sort">Total Facturado</th>
                  <th class="no-sort">Ingreso Compra</th>
                  <th class="no-sort">Total Ingreso Compra</th>
                  <th class="no-sort">Salida Inventario</th>
                  <th class="no-sort">Total Ingreso Salida Inventario</th>
                  <th class="no-sort" title="Presupuesto - Ingreso Compra - Ingreso Salida Inventario">Margen (Ingreso - Gasto)</th>
                  <th class="no-sort">Margen %</th>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-OrdenIngreso');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" name="EID_Orden_Ingreso" class="form-control">
              <input type="hidden" name="Nu_Estado" class="form-control">
              <input type="hidden" name="Nu_Estado_Facturacion" class="form-control">
          	  <input type="hidden" id="hidden-sTypeAction" name="sTypeAction" class="form-control" value="">
              <input type="hidden" id="hidden-nombre_imagen" name="No_Imagen" class="form-control" value="">
              <input type="hidden" id="hidden-nombre_imagen_url" name="No_Imagen_Url" class="form-control" value="">
          	  		  
      			  <div class="row">
                <div class="col-sm-12 col-md-12">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento</b></div>
                    <div class="panel-body">                      
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Ingreso <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Emision" name="Fe_Emision" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Prevista Entrega <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Entrega" name="Fe_Entrega" class="form-control required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="form-group">
                          <label>Generar Salida <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-generar_salida" class="form-control" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-2 div-fecha_salida">
                        <div class="form-group">
                          <label>F. Salida</label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Entrega_Tentativa" name="Fe_Entrega_Tentativa" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-4 div-almacen">
                        <div class="form-group">
                          <label>Taller</label>
                          <select id="cbo-almacen" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                          <label>Area de ingreso <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-area_ingreso" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="form-group">
                          <label>Serie <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-serie_documento" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                        
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>Número <span class="label-advertencia">*</span></label>
                          <input type="tel" id="txt-ID_Numero_Documento" name="ID_Numero_Documento" class="form-control required input-number" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group">
                          <label>ID Presupuesto</label>
                          <input type="text" id="txt-ID_Presupesto_Existe" name="ID_Presupesto_Existe" class="form-control autocompletar_presupuesto" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-2" style="display:none">
                        <div class="form-group">
                          <label>¿Descargar Stock?</label>
                          <select id="cbo-descargar_stock" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Documento -->
              </div>
              
      			  <div class="row">
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-user"></i> <b>Cliente</b></div>
                    <div class="panel-body">
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <br>
                        <div class="form-group">
                          <label>Cliente</label>
                          <input type="hidden" id="txt-AID" name="AID" class="form-control required">
                          <input type="text" id="txt-ANombre" name="ANombre" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar Nombre / Nro. Documento de Identidad" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-7 div-cliente_existente">
                        <div class="form-group">
                          <label>Número Documento Identidad <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-ACodigo" name="ACodigo" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <div class="form-group">
                          <label>Dirección <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <!-- Cliente Nuevo -->
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Tipo Doc. Identidad <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-TiposDocumentoIdentidadCliente" name="ID_Tipo_Documento_Identidad" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label id="label-Nombre_Documento_Identidad_Cliente">DNI</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-Nu_Documento_Identidad_Cliente" name="Nu_Documento_Identidad_Cliente" class="form-control required input-Mayuscula input-codigo_barra" placeholder="Ingresar número" value="" maxlength="8" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-2 text-center div-cliente_nuevo">
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_orden_venta_cliente" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
              
                      <div class="col-xs-12 col-md-10 div-cliente_nuevo">
                        <div class="form-group">
                          <label id="label-No_Entidad_Cliente">Nombre(s) y Apellidos</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-No_Entidad_Cliente" name="No_Entidad_Cliente" class="form-control required" placeholder="Ingresar nombre" value="" autocomplete="off" maxlength="100">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-12 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" id="txt-Txt_Direccion_Entidad_Cliente" name="Txt_Direccion_Entidad_Cliente" class="form-control" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-4 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Telefono</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Telefono_Entidad_Cliente" name="Nu_Telefono_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 9999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-5 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Celular</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Celular_Entidad_Cliente"  name="Nu_Celular_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Cliente -->
                
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-car"></i> <b>Vehículo</b></div>
                    <div class="panel-body">
                      <input type="hidden" id="txt-ID_Tipo_Asiento" class="form-control" value="1">
                      
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addVehiculo" id="radio-contacto_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addVehiculo" id="radio-contacto_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 col-md-12 div-contacto_existente">
                        <br>
                        <input type="hidden" id="txt-ID_Placa" name="" class="form-control">
                        <div class="form-group">
                          <label>Placa</label>
                          <input type="text" id="txt-No_Placa_Vehiculo_existe" name="" class="form-control autocompletar_placa_x_cliente" value="" autocomplete="off" placeholder="Ingresar número" maxlength="6">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-4 div-contacto_existente">
                        <div class="form-group">
                          <label>Marca <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-marca_existe" name="" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-4 div-contacto_existente">
                        <div class="form-group">
                          <label>Modelo <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-modelo_existe" name="" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-4 div-contacto_existente">
                        <div class="form-group">
                          <label>Color <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-color_existe" vame="" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <!-- Vehículo Nuevo -->
                      <div class="col-xs-12 col-sm-6 col-md-4 div-contacto_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Placa</label>
                          <input type="text" name="No_Placa_Vehiculo" id="txt-No_Placa_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="6">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-4 text-center div-contacto_nuevo">
                        <br>
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_sunarp" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                  
                      <div class="col-xs-12 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <br>
                          <label>Año Fabricación</label>
                          <input type="text" name="No_Year_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="4">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Marca</label>
                          <input type="text" name="No_Marca_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Modelo</label>
                          <input type="text" name="No_Modelo_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Tipo Combustible</label>
                          <input type="text" name="No_Tipo_Combustible_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <label>VIN</label>
                          <input type="text" name="No_Vin_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Nro. Serie Motor</label>
                          <input type="text" name="No_Motor" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Color</label>
                          <input type="text" name="No_Color_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="20">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div><!-- panel body-->
                  </div>
                </div><!-- ./Vehiculo -->
              </div>

      			  <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3">
                  <div class="form-group">
                    <label>Nivel de Combustible</label>
                      <select id="cbo-nivel_combustible" class="form-control required" style="width: 100%;"></select>
                      <!--<input type="text" name="No_Nivel_Combustible" class="form-control" placeholder="Nivel" value="" autocomplete="off">-->
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-9 col-sm-9 col-md-3">
                  <div class="form-group">
                    <label>Kilometraje</label>
                      <input type="text" name="No_Kilometraje" class="form-control" placeholder="Kilometraje" value="" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <label>Reparación</label>
                      <textarea name="Txt_Reparacion" class="form-control" placeholder="Ingresar información" value="" autocomplete="off"></textarea>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
  
    	        <div class="row div-imagen"><br>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="well well-sm">
                    <i class="fa fa-warning"></i> Indicaciones:
                    <br>- Imagen: <b>opcional</b>
                    <br>- Formato: <b>PNG</b>
                    <br>- Fondo de imágen(es): <b>SIN FONDO / BLANCO / TRANSPARENTE</b>
                    <br>- Tamaños: <br>
                      &nbsp;&nbsp;* Pequeño: <b>Ancho: 199px</b> y <b>Alto: 199px</b><br>
                      &nbsp;&nbsp;* Mediano: <b>Ancho: 340px</b> y <b>Alto: 340px</b>
                    <br>- Peso Máximo: <b>200 KB</b>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-8 col-md-8 text-center divDropzone"></div>
              </div>

      			  <div class="row">
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->