<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
      <!-- ./New box-header -->
    </div>
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Consultar == 1) : ?>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              <?php endif; ?>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <form id="form-liquidacion_prespuesto" enctype="multipart/form-data" method="post" role="form" autocomplete="off">
              <table id="table-Serie" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Taller</th>
                    <th>F. Emisión</th>
                    <th>OI</th>
                    <th>Tipo</th>
                    <th>Serie</th>
                    <th>Número</th>
                    <th>Cliente</th>
                    <th>Servicio</th>
                    <th>Cantidad</th>
                    <th>Total</th>
                    <th class="no-sort">Estado</th>
                    <th class="no-sort"></th><!--PDF-->
                    <th class="no-sort"></th>
                  </tr>
                </thead>
              </table>
            </form>
          </div>

          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-PagoContratista');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" name="EID_Empresa" class="form-control">
          	  <input type="hidden" name="EID_Documento_Cabecera" class="form-control">
          	  		  
      			  <div class="row">
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento</b></div>
                    <div class="panel-body">                                            
                      <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                          <label>Almacen</label>
                          <select id="cbo-almacen" class="form-control required" style="width: 100%;" disabled></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="form-group">
                          <label>Tipo <span class="label-advertencia">*</span></label>
                          <select id="cbo-TiposDocumento" class="form-control required" style="width: 100%;" disabled></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                          <label>Serie <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-serie_documento" class="form-control required" style="width: 100%;" disabled></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                        
                      <div class="col-xs-6 col-sm-4 col-md-4">
                        <div class="form-group">
                          <label>Número <span class="label-advertencia">*</span></label>
                          <input type="tel" id="txt-ID_Numero_Documento" name="ID_Numero_Documento" class="form-control required input-number" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="form-group">
                          <label>F. Emisión <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Emision" name="Fe_Emision" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Documento -->
                
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-user"></i> <b>Cliente</b></div>
                    <div class="panel-body">                      
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <br>
                        <div class="form-group">
                          <label>Nombre Cliente</label>
                          <input type="hidden" id="txt-AID" name="AID" class="form-control required">
                          <input type="text" id="txt-ANombre" name="ANombre" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre" value="" autocomplete="off" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-7 div-cliente_existente">
                        <div class="form-group">
                          <label>Número Documento Identidad <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-ACodigo" name="ACodigo" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <div class="form-group">
                          <label>Dirección <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Cliente -->
              </div>
              
      			  <div class="row">
                <div class="col-md-12">
        			    <div id="panel-DetalleProductosPagoContratista" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-shopping-cart"></i> <b>Detalle</b></div>
                    <div class="panel-body">                      
      			          <div class="row">
                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table id="table-DetalleProductosPagoContratista" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th style="display:none;" class="text-left"></th>
                                  <th class="text-center" style="width: 10%;">Cantidad</th>
                                  <th class="text-center" style="width: 35%;">Item</th>
                                  <th class="text-center" style="width: 10%;">Precio</th>
                                  <th class="text-center" style="width: 15%;">Impuesto Tributario</th>
                                  <th class="text-center" style="display:none;">Sub Total</th>
                                  <th class="text-center" style="width: 10%;">% Dscto</th>
                                  <th class="text-center">Total</th>
                                  <th class="text-center"></th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Detalle -->
              </div>
              
      			  <div class="row">
      			    <div class="col-md-12">
      			      <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-comment-o"></i> <b>Garantía y Glosa</b></div>
                    <div class="panel-body">
                      <textarea name="Txt_Glosa" class="form-control" placeholder="Glosa" value="" autocomplete="off"></textarea>
                    </div>
                  </div>
                </div>
              </div>
                
      			  <div class="row"><!-- Totales -->
      			    <div class="col-md-8"></div>
                <div class="col-md-4">
      			    <div class="panel panel-default">
                  <div class="panel-heading"><i class="fa fa-money"></i> <b>Totales</b></div>
                  <div class="panel-body">
                    <table class="table" id="table-OrdenVentaTotal">              
                      <tr>
                        <td><label>OP. Gravadas</label></td>
                        <td class="text-right">
    	  					        <input type="hidden" class="form-control" id="txt-subTotal" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-subTotal">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Inafectas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-inafecto" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-inafecto">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Exoneradas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-exonerada" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-exonerada">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Gratuitas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-gratuita" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-gratuita">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Descuento Total (-)</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-descuento" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-descuento">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>I.G.V. %</label></td>
                        <td class="text-right">
                            <input type="hidden" class="form-control" id="txt-impuesto" value="0.00"/>
                            <span class="span-signo"></span> <span id="span-impuesto">0.00</span>
                        </td>
                      </tr>
                        
                      <tr>
                        <td><label>Total</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-total" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-total">0.00</span>
                        </td>
                      </tr>
                    </table><!-- ./Totales -->
                  </div>
                </div>
              </div>
              
      			  <div class="row">
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->