<!--  M0009 - Inicio -->
<script>  /* document.body.onkeydown = function(e){
  
  if (e.keyCode === 13) {
    e.preventDefault();
     document.getElementById("btn-filter").click();
   } 
 }; 

*/  

// REQ-4 - INICIO 
function EnterFunction(event) {
// onkeypress="EnterFunction(event)" 

if(event.which === 13){  
  event.preventDefault();
  document.getElementById("btn-filter").click();
} 
}
// REQ-4 - FIN

 </script>   
 <!--  M0009 - Fin -->
 


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <!-- REQ-4 - I -->
                    <input type="text" id="txt-Filtro_Fe_Inicio" onkeypress="EnterFunction(event)"  class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  <!-- REQ-4 - F -->
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <!-- REQ-4 - I -->
                    <input type="text" id="txt-Filtro_Fe_Fin" onkeypress="EnterFunction(event)"  class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  <!-- REQ-4 - F -->
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-8 col-md-2 hidden">
                <div class="form-group">
                  <label>Nombre Contacto</label>
                  <input type="text" id="txt-Filtro_Contacto" class="form-control autocompletar_contacto" placeholder="Ingresar nombre" maxlength="50" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Tipo</label>
    		  				<select id="cbo-Filtro_TiposDocumento"  class="form-control select2" multiple="multiple"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Serie</label>
    		  				<select id="cbo-Filtro_serie_documento"  class="form-control select2" multiple="multiple"></select>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <input type="text" id="txt-Filtro_NumeroDocumento" class="form-control tagify-number" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Estado</label>
    		  				<select id="cbo-Filtro_Estado" class="form-control select2" multiple="multiple">
    		  				  <option value="0" selected>Todos</option>
        				    <option value="5">Pendiente</option>
        				    <option value="22">Aprobado</option>
        				    <option value="23">No Aprobado</option>
        				    <option value="21">Facturado</option>
                    <option value="27">Cotización</option>
        				  </select>
                </div>
              </div>
            </div>
            <div class="row div-Filtros">
              <div class="col-xs-6 col-sm-4 col-md-4">
                <div class="form-group">
                  <label>Placa</label>
                  <input type="text" id="txt-Filtro_Placa" class="form-control tagify" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-4">
                <div class="form-group">
                  <label>Marca</label>
                  <input type="text" id="txt-Filtro_Marca" class="form-control tagify" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-4">
                <div class="form-group">
                  <label>Modelo</label>
                  <input type="text" id="txt-Filtro_Modelo" class="form-control tagify" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-8 col-md-12">
                <div class="form-group">
                  <label>Cliente</label>
                  <select id="cbo-Filtro_Entidad" class="form-control select2" multiple="multiple" data-placeholder="Ingresar Nombre / Nro. Documento Identidad" data-ajax--url="../../AutocompleteController/getAllClientMultiple" data-ajax--cache="false">
                  </select>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <label>Busca por cualquier columna</label>
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Busca por cualquier columna" title="Busca por cualquier columna" class="form-control">
                </div>
              </div>
            </div>

            <div class="row div-Filtros text-center">
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                    <button type="button" class="btn btn-success btn-block" onclick="agregarOrdenVenta()"><i class="fa fa-plus-circle"></i> Agregar</button>
                </div>
              </div>
              

              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                    <button type="button" class="btn btn-warning btn-block" onclick="enlazarOI();">Enlazar OI</button>
                </div>
              </div>

              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                    <button type="button" id="btn-generar_liquidacion" class="btn btn-info btn-block">Generar Liquidación</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                    <button type="button" id="btn-generar_pago_contratista" class="btn btn-danger btn-block" onclick="pagoContratista();">Pago Contratista</button>
                </div>
              </div>

               <!-- M041-2  FIX-2 - I -->
               </br>
              </br>
              </br>
              </br>
              <input type="hidden" id="Txt_btn_reporte"  value='0'  class="form-control">
              <input type="hidden" id="Txt_ID_Usuario"  value=<?php echo $this->session->userdata['usuario']->ID_Usuario; ?>  class="form-control">
              <input type="hidden" id="Txt_Fecha_Efectiva" value=<?php echo date('Y-m-d H:i:s'); ?>   class="form-control">              
              <div class="col-xs-6 col-sm-6 col-md-4">
                <div class="form-group">
                  <label>&nbsp;</label>
                    <button type="submit" id="btn-reporteserviciocli" class="btn btn-success btn-block" ><i class=""></i> Reporte Servicio al Cliente</button>
                </div>
              </div>
              <!-- M041-2 FIX-2 - F -->

              <?php endif; ?>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <form id="form-oden_venta" enctype="multipart/form-data" method="post" role="form" autocomplete="off">
              <table id="table-OrdenVenta" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="no-sort">Check OI</th>
                    <th class="no-sort">Check Liq.</th>
                    <th class="no-sort">Check Contratista</th>
                    <th class="no-sort">ID</th>
                    <th class="no-sort">F. Emisión</th>
                    <th class="no-sort_left">Tipo</th>
                    <th class="no-sort_left">Serie</th>
                    <th class="no-sort_left">Número</th>
                    <th class="no-sort_left">Cliente</th>
                    <th class="no-sort_left">Placa</th>
                    <th class="no-sort_left">Marca</th>
                    <th class="no-sort_left">Modelo</th>
                    <th class="no-sort">M</th>
                    <th class="no-sort_right">Total</th>
                    <th class="no-sort">Cambiar Estado</th>
                    <th class="no-sort">Estado Presupuesto</th>
                    <th class="no-sort">Tipo</th><!--tipo mantenimiento-->
                    <th class="no-sort">OI</th>
                    <th class="no-sort">OI Estado</th>
                    <th class="no-sort">OT</th>
                    <th class="no-sort">Liquidación</th>
                    <th class="no-sort">Pago Contratista</th>
                    <th class="no-sort">Factura</th>
                    <th class="no-sort">OI Etapa</th>
                    <th class="no-sort">OI F. Salida</th>
                    <th class="no-sort">Fecha Facturacion</th>
                    <th class="no-sort">Dias Facturacion</th>
                    <th class="no-sort">Carta compromiso</th>  
                    <!-- OC_MODAL_3 -->
                    <th class="no-sort">Total OC</th> <!-- total OC -->
                    <th class="no-sort"></th> <!-- Facturar -->
                    <th class="no-sort"></th> <!-- Pdf or Excel -->
                    <th class="no-sort"></th> <!-- Generar vale -->
                    <th class="no-sort"></th> <!-- Generar Oc -->
                    <th class="no-sort"></th> <!-- Modificar -->
                    <th class="no-sort"></th> <!-- Eliminar -->
                    <th class="no-sort"></th> <!-- Duplicar -->
                  </tr>
                </thead>
              </table>

              <!-- Modal enlzar_oi -->
              <div class="modal fade modal-enlzar_oi" id="modal-default">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-xs-12">
                          <label>ID Orden Ingreso</label>
                          <div class="form-group">
                            <input type="text" id="txt-ID_Orden_Ingreso_Existe" name="ID_Orden_Ingreso_Existe" class="form-control autocompletar_oi" placeholder="Ingresar ID OI" value="" autocomplete="off">  
                            <span class="help-block" id="error"></span>
                          </div>
                          <div id="CONTENT_Orden_Ingreso_Existe"></div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <button type="button" id="btn-enlaza_oi" class="btn btn-success btn-md btn-block pull-center">Enviar</button>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /. Modal enlzar_oi -->

              <div class="modal fade modal-compromiso" id="modal-default">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <input type="hidden" class="hidden-modal_compromiso" value="" autocomplete="off">
                    <div class="modal-body" id="div-modal-body-compromiso">
                    </div>
                    <div class="modal-footer">
                      <div class="col-xs-6">
                        <button type="button" id="btn-modal-salir-compromiso" class="btn btn-danger btn-block pull-center" data-dismiss="modal">Cancelar (ESC)</button>
                      </div>
                      <div class="col-xs-6">
                        <button type="button" id="btn-modal-facturar-compromiso" class="btn btn-primary btn-block pull-center" onclick="pdfCreateCartaCompromiso()">Generar</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </form>
          </div>
          <!-- /.box-body -->

          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-OrdenVenta');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" name="EID_Empresa" class="form-control">
          	  <input type="hidden" name="EID_Documento_Cabecera" class="form-control">
          	  <input type="hidden" name="ENu_Estado" class="form-control">
              <input type="hidden" name="ID_Origen_Tabla" class="form-control">

      			  <div class="row">
                <div class="col-sm-12 col-md-12">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento</b></div>
                    <div class="panel-body">
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>Tipo <span class="label-advertencia">*</span></label>
                          <select id="cbo-TiposDocumento" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="form-group">
                          <label>Serie <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-serie_documento" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-6 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>Número <span class="label-advertencia">*</span></label>
                          <input type="tel" id="txt-ID_Numero_Documento" name="ID_Numero_Documento" class="form-control required input-number" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Emisión <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Emision" name="Fe_Emision" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Vencimiento <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Vencimiento" name="Fe_Vencimiento" class="form-control required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Entrega <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Entrega" name="Fe_Entrega" class="form-control required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                          <label>Medio Pago <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-MediosPago" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                
                      <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="form-group">
                          <label>Moneda <span class="label-advertencia">*</span></label>
                          <select id="cbo-Monedas" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-2"  style="display:none">
                        <div class="form-group">
                          <label>PDF</label>
                          <select id="cbo-formato_pdf" class="form-control required" style="width: 100%;">
                            <option value="A4" selected="selected">A4</option>
                            <option value="A5">A5</option>
                            <option value="TICKET">Ticket</option>
                          </select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-2" style="display:none">
                        <div class="form-group">
                          <label>¿Descargar Stock?</label>
                          <select id="cbo-descargar_stock" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-8 col-md-4 div-almacen">
                        <div class="form-group">
                          <label>Almacen</label>
                          <select id="cbo-almacen" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-3">
                        <label>Tipo Mantenimiento <span class="label-advertencia">*</span></label>
                        <div class="form-group">
            		  				<select id="cbo-tipo_mantenimiento" name="Nu_Tipo_Mantenimiento" class="form-control required"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-12">
                        <div class="form-group">
                          <label>Personal / Usuario</label>
            		  				<select id="cbo-vendedor" name="ID_Mesero" class="form-control select2" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-2"  style="display:none">
                        <div class="form-group">
                          <label>Porcentaje</label>
            		  				<select id="cbo-porcentaje" name="Po_Comision" class="form-control"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Documento -->
              </div>
              
      			  <div class="row">
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-user"></i> <b>Cliente</b></div>
                    <div class="panel-body">
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <br>
                        <div class="form-group">
                          <label>Nombre Cliente</label>
                          <input type="hidden" id="txt-AID" name="AID" class="form-control required">
                          <input type="text" id="txt-ANombre" name="ANombre" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-7 div-cliente_existente">
                        <div class="form-group">
                          <label>Número Documento Identidad <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-ACodigo" name="ACodigo" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <div class="form-group">
                          <label>Dirección <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <!-- Cliente Nuevo -->
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Tipo Doc. Identidad <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-TiposDocumentoIdentidadCliente" name="ID_Tipo_Documento_Identidad" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label id="label-Nombre_Documento_Identidad_Cliente">DNI</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-Nu_Documento_Identidad_Cliente" name="Nu_Documento_Identidad_Cliente" class="form-control required input-Mayuscula input-codigo_barra" placeholder="Ingresar número" value="" maxlength="8" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-2 text-center div-cliente_nuevo">
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_orden_venta_cliente" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
              
                      <div class="col-xs-12 col-md-10 div-cliente_nuevo">
                        <div class="form-group">
                          <label id="label-No_Entidad_Cliente">Nombre(s) y Apellidos</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-No_Entidad_Cliente" name="No_Entidad_Cliente" class="form-control required" placeholder="Ingresar nombre" value="" autocomplete="off" maxlength="100">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-12 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" id="txt-Txt_Direccion_Entidad_Cliente" name="Txt_Direccion_Entidad_Cliente" class="form-control" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-4 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Telefono</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Telefono_Entidad_Cliente" name="Nu_Telefono_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 9999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-5 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Celular</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Celular_Entidad_Cliente"  name="Nu_Celular_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Cliente -->
                
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-car"></i> <b>Vehículo</b></div>
                    <div class="panel-body">
                      <input type="hidden" id="txt-ID_Tipo_Asiento" class="form-control" value="1">
                      
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addVehiculo" id="radio-vehiculo_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addVehiculo" id="radio-vehiculo_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 col-md-12 div-vehiculo_existente">
                        <input type="hidden" id="txt-ID_Placa" name="" class="form-control">
                        <div class="form-group">
                          <label>Placa</label>
                          <input type="text" id="txt-No_Placa_Vehiculo_existe" name="" class="form-control autocompletar_placa_x_cliente" value="" autocomplete="off" placeholder="Ingresar número" maxlength="6">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-4 div-vehiculo_existente">
                        <div class="form-group">
                          <label>Marca <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-marca_existe" name="" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-4 div-vehiculo_existente">
                        <div class="form-group">
                          <label>Modelo <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-modelo_existe" name="" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-4 div-vehiculo_existente">
                        <div class="form-group">
                          <label>Color <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-color_existe" vame="" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <!-- Vehículo Nuevo -->
                      <div class="col-xs-12 col-sm-6 col-md-4 div-vehiculo_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Placa</label>
                          <input type="text" name="No_Placa_Vehiculo" id="txt-No_Placa_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="6">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-4 text-center div-vehiculo_nuevo">
                        <br>
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_sunarp" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                  
                      <div class="col-xs-12 col-md-4 div-vehiculo_nuevo">
                        <div class="form-group">
                          <br>
                          <label>Año Fabricación</label>
                          <input type="text" name="No_Year_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="4">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-vehiculo_nuevo">
                        <div class="form-group">
                          <label>Marca</label>
                          <input type="text" name="No_Marca_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-vehiculo_nuevo">
                        <div class="form-group">
                          <label>Modelo</label>
                          <input type="text" name="No_Modelo_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-vehiculo_nuevo">
                        <div class="form-group">
                          <label>Tipo Combustible</label>
                          <input type="text" name="No_Tipo_Combustible_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-vehiculo_nuevo">
                        <div class="form-group">
                          <label>VIN</label>
                          <input type="text" name="No_Vin_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-vehiculo_nuevo">
                        <div class="form-group">
                          <label>Nro. Serie Motor</label>
                          <input type="text" name="No_Motor" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="30">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-4 div-vehiculo_nuevo">
                        <div class="form-group">
                          <label>Color</label>
                          <input type="text" name="No_Color_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar número" maxlength="20">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Vehiculo -->

                <input type="hidden" id="txt-AID_Contacto" name="AID_Contacto" class="form-control required">
              </div>
              
      			  <div class="row">
                <div class="col-md-12">
        			    <div id="panel-DetalleProductosOrdenVenta" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-shopping-cart"></i> <b>Detalle</b></div>
                    <div class="panel-body">
      			          <div class="row">
          	            <input type="hidden" name="Nu_Tipo_Lista_Precio" value="1" class="form-control">
                        <div class="col-xs-12">
                          <label>Lista de Precio <span class="label-advertencia">*</span></label>
                          <div class="form-group">
                            <select id="cbo-lista_precios" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-9">
                          <label>Producto / Servicio <span class="label-advertencia">*</span></label>
                          <div class="form-group">
                            <input type="hidden" id="txt-Nu_Tipo_Registro" class="form-control" value="1"><!-- Venta -->
                            <input type="hidden" id="txt-Nu_Tipo_Producto" class="form-control" value="2"><!-- No muestra los productos de tipo interno -->
                            <input type="hidden" id="txt-Nu_Compuesto" class="form-control" value="">
                            <input type="hidden" id="txt-ID_Producto" class="form-control">
                            <input type="hidden" id="txt-Nu_Codigo_Barra" class="form-control">
                            <input type="hidden" id="txt-Ss_Precio" class="form-control">
                            
                            <!--M041 - I -->
                            <input type="hidden" id="txt-Ss_Costo" class="form-control"> 
                            <!--M041 - F -->
                            
                            <input type="hidden" id="txt-ID_Impuesto_Cruce_Documento" class="form-control">
                            <input type="hidden" id="txt-Nu_Tipo_Impuesto" class="form-control">
                            <input type="hidden" id="txt-Ss_Impuesto" class="form-control">
                            <input type="hidden" id="txt-Qt_Producto" class="form-control">
                            <input type="hidden" id="txt-nu_tipo_item" class="form-control">
                            <input type="text" id="txt-No_Producto" class="form-control autocompletar_detalle" data-global-class_method="AutocompleteController/getAllProduct" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off">
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-3">
                          <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="button" id="btn-addProductoOrdenVenta" class="btn btn-success btn-md btn-block"><i class="fa fa-plus-circle"></i> Agregar Item Detalle</button>
                          </div>
                        </div>
                      </div>
                      
      			          <div class="row">
                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table id="table-DetalleProductosOrdenVenta" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th style="display:none;" class="text-left"></th>
                                  <th class="text-center" style="width: 10%;">Cantidad</th>
                                  <th class="text-center" style="width: 20%;">Item</th>
                                  <th class="text-center">Descripción</th>
                                  <th class="text-center" style="width: 10%;">Valor Unit.</th>
                                  <th class="text-center" style="width: 10%;">Precio</th>
                                  <!-- M041 - I -->
                                  <th class="text-center" style="width: 10%;">Costo</th>
                                  <!-- M041 - F -->
                                  <th class="text-center" style="width: 15%;">Impuesto Tributario</th>
                                  <th class="text-center" style="display:none;">Sub Total</th>
                                  <th class="text-center" style="width: 10%;">% Dscto</th>
                                  <th class="text-center" style="width: 10%;">Total</th>
                                  <th class="text-center"></th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Detalle -->
              </div>
              
      			  <div class="row">
      			    <div class="col-md-12">
      			      <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-comment-o"></i> <b>Garantía y Glosa</b></div>
                    <div class="panel-body">
                      <input type="text" name="Txt_Garantia" class="form-control" placeholder="Garantía" value="" autocomplete="off">
                      <br>
                      <textarea name="Txt_Glosa" class="form-control" placeholder="Glosa" value="" autocomplete="off"></textarea>
                      <br>
                      <textarea name="Txt_Resumen_Servicio_Presupuesto" class="form-control" placeholder="Resumen del Tipo de Servicio" value="" autocomplete="off"></textarea>
                    </div>
                  </div>
                </div>
              </div>
                
      			  <div class="row"><!-- Totales -->
      			    <div class="col-md-8"></div>
                <div class="col-md-4">
      			    <div class="panel panel-default">
                  <div class="panel-heading"><i class="fa fa-money"></i> <b>Totales</b></div>
                  <div class="panel-body">
                    <table class="table" id="table-OrdenVentaTotal">
                      <tr>
                        <td><label>% Descuento</label></td>
                        <td class="text-right">
    	  					        <input type="tel" class="form-control input-decimal" id="txt-Ss_Descuento" name="Ss_Descuento" size="3" value="" autocomplete="off" />
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Gravadas</label></td>
                        <td class="text-right">
    	  					        <input type="hidden" class="form-control" id="txt-subTotal" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-subTotal">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Inafectas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-inafecto" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-inafecto">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Exoneradas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-exonerada" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-exonerada">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Gratuitas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-gratuita" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-gratuita">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Descuento Total (-)</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-descuento" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-descuento">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>I.G.V. %</label></td>
                        <td class="text-right">
                            <input type="hidden" class="form-control" id="txt-impuesto" value="0.00"/>
                            <span class="span-signo"></span> <span id="span-impuesto">0.00</span>
                        </td>
                      </tr>
                        
                      <tr>
                        <td><label>Total</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-total" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-total">0.00</span>
                        </td>
                      </tr>
                    </table><!-- ./Totales -->
                  </div>
                </div>
              </div>
              
      			  <div class="row">
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->