<!DOCTYPE html>
<html>
	<head>
    <meta http-equiv=Content-Type content=text/html; charset=UTF-8/>
		<title>laesystems</title>
      <style type=text/css>
        .table_pdf {
          width: 100%;
        }
    
        .tr-thead_border th{
          font-size: 5px;
          border: solid 1px #000000;
        }
        
        .tr-thead th.title {
          color: #000000;
          font-size: 16px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-thead th.sub_title_fecha {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-thead th.sub_title_nro {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-sub_thead th.title {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-sub_thead th.content {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_nombres {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-sub_thead th.contacto_cargo {
          color: #838585;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_celular {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_correo {
          color: #34bdad;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-theadFormat th{
          font-weight: bold;
          font-family: Arial;
        }
        
        .tr-header-detalle th{
          font-size: 8px;
          font-family: Arial;
          background-color: #F2F5F5;
        }
        
        .tr-header th{
          font-size: 8px;
          font-family: Arial;
          background-color: #F2F5F5;
        }

        .tr-footer th{
          font-size: 8px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-totales th{
          font-size: 8px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-fila_impar td{
          font-size: 6px;
          font-family: Arial;
          background-color: #F2F5F5;
        }
        
        .tr-fila_par td{
          font-size: 6px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
      </style>
    </head>
    <body>
      <br/>
      <?php
			  $Po_IGV = "";
        foreach($arrData as $row)
          $Po_IGV = "18 %";
      ?>
	  	<table class="table_pdf" border="0">
        <thead>
          <tr class="tr-sub_thead">
            <th class="text-left title" style="width: 60%;"><b>CLIENTE</b></th>
            <th class="text-left content" style="width: 40%;"><b>FECHA EMISIÓN: </b><?php echo ToDateBD($arrData[0]->Fe_Emision); ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->No_Tipo_Documento_Identidad_Breve . ': ' .$arrData[0]->Nu_Documento_Identidad; ?></th>
            <th class="text-left content" style="width: 40%;"><b>MONEDA: </b><?php echo $arrData[0]->No_Moneda; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->No_Entidad; ?></th>
            <th class="text-left content" style="width: 40%;"><?php echo (!empty($Po_IGV) ? '<b>IGV: </b>' . $Po_IGV : '-'); ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->Txt_Direccion_Entidad; ?></th>
            <th class="text-left content" style="width: 40%;"></th>
          </tr>
        </thead>
      </table>
      <br/>
      <br/>
      <br/>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-theadFormat tr-header tr-header-detalle">
            <th class="text-center" style="width: 11%">&nbsp;<br/>OI<br/></th>
            <th class="text-center" style="width: 11%">&nbsp;<br/>PLACA<br/></th>
            <th class="text-center" style="width: 11%">&nbsp;<br/>KILOMERAJE<br/></th>
            <th class="text-center" style="width: 11%">&nbsp;<br/>NRO. PROFORMA<br/></th>
            <th class="text-center" style="width: 11%">&nbsp;<br/>FECHA<br/></th>
            <th class="text-center" style="width: 11%">&nbsp;<br/>TIPO MANTENIMIENTO<br/></th>
            <th class="text-center" style="width: 11.5%">&nbsp;<br/>MONTO (SIN IGV)<br/></th>
            <th class="text-center" style="width: 11.5%">&nbsp;<br/>MONTO (INC IGV)<br/></th>
            <th class="text-center" style="width: 11%">&nbsp;<br/>FACTURA<br/></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $iCounter = 0; 
            $sNameClassTrDetalle = '';
            foreach($arrData as $row) {
              $sNameClassTrDetalle = ($iCounter%2) == 0 ? 'tr-fila_par' : 'tr-fila_impar';
              
              $liquidacion = '';
              $arrParams = array(
                'iRelacionDatos' => 4,
                'iIdOrigenTabla' => $row->ID_Documento_Cabecera_OV
              );
              $arrResponseRelacion = $HelperModel->getRelacionTablaPresupuestoALiquidacion($arrParams);
              if ($arrResponseRelacion['sStatus'] == 'success') {
                foreach ($arrResponseRelacion['arrData'] as $row_relacion)
                  $liquidacion .= $row_relacion->ID_Documento_Cabecera;
              }
      
              $facturacion = '';
              $arrParams = array(
                'iRelacionDatos' => 5,
                'iIdOrigenTabla' => $row->ID_Documento_Cabecera_OV
              );
              $arrResponseRelacion = $HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
              if ($arrResponseRelacion['sStatus'] == 'success') {
                foreach ($arrResponseRelacion['arrData'] as $row_relacion)
                  $facturacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
              }

              if ( empty($facturacion) && !empty($liquidacion) ) {
                $facturacion = '';
                $arrParams = array(
                  'iRelacionDatos' => 5,
                  'iIdOrigenTabla' => $liquidacion
                );
                $arrResponseRelacion = $HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
                if ($arrResponseRelacion['sStatus'] == 'success') {
                  foreach ($arrResponseRelacion['arrData'] as $row_relacion)
                    $facturacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
                }
              }

              $orden_ingreso = '';
              $arrParams = array(
                'iRelacionDatos' => 6,
                'iIdOrigenTabla' => $row->ID_Documento_Cabecera_OV
              );
              $arrResponseRelacion = $this->HelperModel->getRelacionTablaMultiplePresupuestoAndOI($arrParams);
              if ($arrResponseRelacion['sStatus'] == 'success') {
                foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
                  $orden_ingreso .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
                }
              }
            ?>
              <tr class="<?php echo $sNameClassTrDetalle; ?>">
                <td class="text-left" style="width: 11%">&nbsp;<br/><?php echo $orden_ingreso; ?><br/></td>
                <td class="text-left" style="width: 11%">&nbsp;<br/><?php echo $row->No_Placa_Vehiculo; ?><br/></td>
                <td class="text-left" style="width: 11%">&nbsp;<br/><?php echo $row->No_Kilometraje; ?><br/></td>
                <td class="text-left" style="width: 11%">&nbsp;<br/><?php echo $row->ID_Numero_Documento_OV; ?><br/></td>
                <td class="text-left" style="width: 11%">&nbsp;<br/><?php echo ToDateBD($row->Fe_Emision); ?><br/></td>
                <td class="text-left" style="width: 11%">&nbsp;<br/><?php echo $row->No_Tipo_Mantenimiento; ?><br/></td>
                <td class="text-right" style="width: 11.5%">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat($row->Ss_Total_OV / 1.18, 2, '.', ','); ?><br/></td>
                <td class="text-right" style="width: 11.5%">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat($row->Ss_Total_OV, 2, '.', ','); ?><br/></td>
                <td class="text-center" style="width: 11%">&nbsp;<br><?php echo $facturacion; ?><br/></td>
              </tr>
              <?php
              $iCounter++;
            } ?>
        </tbody>
      </table>
	  	<table class="table_pdf" cellpadding="2">
        <thead>
          <tr class="tr-theadFormat tr-totales">
            <th class="text-right" style="width: 66%;">TOTAL</th>
            <th class="text-right" style="width: 11.5%;"><?php echo numberFormat($arrData[0]->Ss_Total / 1.18, 2, '.', ','); ?></th>
            <th class="text-right" style="width: 11.5%;"><?php echo numberFormat($arrData[0]->Ss_Total, 2, '.', ','); ?></th>
          </tr>
          <?php
          if (!empty($arrData[0]->Txt_Glosa)) {
          ?>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3">&nbsp;</th>
          </tr>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3"><b>OBSERVACIONES: <?php echo $arrData[0]->Txt_Glosa; ?></b></th>
          </tr>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3">&nbsp;</th>
          </tr>
          <?php
          }?>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3"></th>
          </tr>
        </thead>
      </table>
      <br/>
      <br/>
      <?php if (!empty($this->empresa->Txt_Cuentas_Bancarias) || !empty($this->empresa->Txt_Nota)) { ?>
	  	<table class="table_pdf" cellpadding="4">
        <thead>
          <?php if (!empty($this->empresa->Txt_Cuentas_Bancarias)) { ?>
          <tr class="tr-theadFormat tr-header tr-header-detalle">
            <th class="text-left">Cuentas Bancarias</th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content"><?php echo $this->empresa->Txt_Cuentas_Bancarias; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content"><br/></th>
          </tr>
          <?php } ?>
      </table>
      <br/>
      <br/>
      <?php } ?>
    </body>
</html>