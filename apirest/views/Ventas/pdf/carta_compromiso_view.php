<?php
setlocale(LC_ALL, 'es_ES');
$months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
$month = $months[date('n')-1];
?>
<!DOCTYPE html>
<html lang="es-Es">
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>laesystems</title>
      <style type=text/css>
        @page {
          margin: 0cm 0cm;
        }
        body {
          margin: 0;
          padding: 0;
          font-size: 12pt;
          font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif;
          line-height: 24pt;
        }
        table {
          background: #FFF;
          border-spacing: 0;
          width: 100%;
        }
        .title {
          text-align: center;
          font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
          margin: 0 auto;
          font-size: 1.3em;
          text-decoration: underline;
          font-weight: bold;
          height: 70px;
          vertical-align: middle;
        }
        .date {
          text-align: right;
          height: 70px;
        }
        .body {
          text-align: justify;
        }
      </style>
    </head>
    <body>
      <table border="0" style="margin: 0; padding: 0;">
        <tr>
          <td style="line-height: 50px;">&nbsp;</td>
        </tr>
      </table>
      <table border="0" style="margin: 0; padding: 0;">
        <tr style="height: calc(260mm);">
          <td style="vertical-align: top;">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td class="title">CARTA DE CONFORMIDAD</td>
              </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 170mm;margin-bottom: 2%; ">
              <tr>
                <td class="date">Lima, <?=date('d')?> de <?=$month?> de <?=date('Y')?>.</td>
              </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" style="margin: auto; width: 170mm;">
              <tr>
                <td class="body">Por medio del presente hago de su conocimiento que la empresa <strong>MAFISA MOTORS S.A.C.,</strong> garantiza el servicio de 
                <strong><?=strtoupper(base64_decode($_GET['tipo']))?> <?php if (!empty($_GET['actividades'])) echo '(' . base64_decode($_GET['actividades']) . ')' ?></strong> 
                de la unidad en mención, realizado a la unidad 
                <strong><?=$arrData[0]->No_Marca_Vehiculo?></strong> 
                de placa 
                <strong><?=$arrData[0]->No_Placa_Vehiculo?>,</strong> 
                la unidad fue entregada a tiempo y sin ningún inconveniente. Así mismo, informó el cliente estar conforme con el servicio brindado y 
                aceptando recibir el vehículo tal y como ingreso a taller sin faltarle nada. 
                </td>
              </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" style="margin: auto; width: 170mm;">
              <tr>
                <td class="last_body"><br/><br/>Reiterando la conformidad y garantía del servicio.</td>
              </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" style="margin: auto; width: 170mm;">
              <tr>
                <td class="thanks"><br/><br/>Por su atención, gracias.</td>
              </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="40" style="margin: auto; width: 170mm">
              <tr>
                <td style="vertical-align: text-top; text-align: center; width: 50%;line-height: 12px;"><br/><br/><br/><br/><br/>
                  <div style="background-color: #000;width: 50px;line-height: 1px;">&nbsp;</div>
                  <div style="width: 250px;margin: auto;"><strong>MAFISA MOTORS</strong></div>
                  <div style="width: 250px;margin: auto;"><strong>SUPERVISOR</strong></div>
                </td>
                <td style="vertical-align: text-top;width: 50%;line-height: 12px;"><br/><br/><br/><br/><br/>
                  <div style="background-color: #000;width: 100%;line-height: 1px;">&nbsp;</div>
                  <div style="text-align: center;width: 250px;margin: auto;"><strong><?=$arrData[0]->No_Entidad?></strong></div>
                  <div style="width: 250px;margin: auto;"><strong>NOMBRE:</strong> <?=$arrData[0]->No_Contacto?></div>
                  <div style="width: 250px;margin: auto;"><strong>DNI:</strong> <?=$arrData[0]->Nu_Documento_Identidad_Contacto?></div>
                  <div style="width: 250px;margin: auto;"><strong>CELULAR:</strong> <?=$arrData[0]->Nu_Celular_Contacto?></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
			</table>
    </body>
</html>