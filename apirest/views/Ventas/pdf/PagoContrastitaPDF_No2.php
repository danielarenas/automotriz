
<!-- M040 - 2 - I  -->  
<!DOCTYPE html>
<html>
	<head>  
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>laesystems</title>
      <style type=text/css> 
        .table_pdf {
          width: 100%;
        }
     
        .tr-thead_border th{
          font-size: 5px;
          border: solid 1px #000000;
        }
        
        .tr-thead th.title {
          color: #000000;
          font-size: 16px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-thead th.sub_title_fecha {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-thead th.sub_title_nro {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-sub_thead th.title {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-sub_thead th.content {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_nombres {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-sub_thead th.contacto_cargo {
          color: #838585;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_celular {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_correo {
          color: #34bdad;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-theadFormat th{
          font-weight: bold;
          font-family: Arial;
        }
        
        .tr-header-detalle th{
          font-size: 6px;
          font-family: Arial;
          background-color: #F2F5F5;
        }
        
        .tr-header th{
          font-size: 8px;
          font-family: Arial;
          background-color: #F2F5F5;
        }

        .tr-footer th{
          font-size: 8px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-totales th{
          font-size: 8px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-fila_impar td{
          font-size: 6px;
          font-family: Arial;
          background-color: #F2F5F5;
        }
        
        .tr-fila_par td{
          font-size: 6px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
      </style>
    </head>
    <body>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-thead">
            <th class="text-left title"><br/><br/><br/><?php echo $arrData[0]->No_Tipo_Documento_Breve; ?></th>
            <th class="text-center sub_title_fecha">
              <div>
                &nbsp;<br>
                <div style="border-top: 2.7px solid #34bdad">
                  &nbsp;<br>
                  Día <?php echo dateNow('dia'); ?> de <?php echo getNameMonth(dateNow('mes')); ?> del <?php echo dateNow('año'); ?>
                </div>
              </div>
            </th>
            <th class="text-right sub_title_nro">
              <div>
                &nbsp;<br>
                <div style="border-top: 2.7px solid #34bdad">
                  &nbsp;<br>
                  Nro. <?php echo $arrData[0]->ID_Serie_Documento . ' - ' . $arrData[0]->ID_Numero_Documento; ?>
                </div>
              </div>
            </th>
          </tr>
        </thead>
      </table>
      <br/>
      <br/>
      <br/>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-sub_thead">
            <th class="text-left title" style="width: 40%">CLIENTE</th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content"><?php echo $arrData[0]->No_Entidad; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content"><?php echo $arrData[0]->No_Tipo_Documento_Identidad_Breve . ': ' . $arrData[0]->Nu_Documento_Identidad; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content"><?php echo $arrData[0]->Txt_Direccion_Entidad; ?></th>
          </tr>
        </thead>
      </table>
      <br/>
      <br/>
      <br/>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-theadFormat tr-header tr-header-detalle">
            <th class="text-left" style="width: 6%;font-size:5.5px;">&nbsp;<br/>OI<br/></th>
            <th class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/>MARCA<br/></th>
            <th class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/>MODELO<br/></th>
            <th class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/>PLACA<br/></th>
            <th class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/>COLOR<br/></th>
            <th class="text-right" style="width: 5%;font-size:5px;">&nbsp;<br/>CANT<br/></th>
            <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/>PRECIO UNIT.<br/></th>
            <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/>MONTO (SIN IGV)<br/></th>
            <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/>MONTO (INC IGV)<br/></th>
            <th class="text-right" style="width: 8%;font-size:5px;">&nbsp;<br/>PRESUPUESTO<br/></th>
            <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/>PRE CLIENTE (SIN IGV)<br/></th>
            <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/>PRE CLIENTE (CON IGV)<br/></th>
            <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/>GANANCIA<br/></th>
            <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/>GANANCIA %<br/></th>
            <th class="text-center" style="width: 6%;font-size:5px;">&nbsp;<br/>FECHA<br/></th>
             <th class="text-center" style="width: 6%;font-size:5px;">&nbsp;<br/>FECHA   SALIDA<br/></th>
            <th class="text-center" style="width: 6%;font-size:5px;">&nbsp;<br/>FACTURA<br/></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $iCounter = 0; 
            $sNameClassTrDetalle = '';
            $fTotalCantidad =0.00;
            foreach($arrData as $row) {
              $sNameClassTrDetalle = ($iCounter%2) == 0 ? 'tr-fila_par' : 'tr-fila_impar';
              
              $liquidacion = '';
              $arrParams = array(
                'iRelacionDatos' => 4,
                'iIdOrigenTabla' => $row->ID_Documento_Cabecera_OV
              );
              $arrResponseRelacion = $HelperModel->getRelacionTablaPresupuestoALiquidacion($arrParams);
              if ($arrResponseRelacion['sStatus'] == 'success') {
                foreach ($arrResponseRelacion['arrData'] as $row_relacion)
                  $liquidacion .= $row_relacion->ID_Documento_Cabecera;
              }
      
              $facturacion = '';
              $arrParams = array(
                'iRelacionDatos' => 5,
                'iIdOrigenTabla' => $row->ID_Documento_Cabecera_OV
              );
              $arrResponseRelacion = $HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
              if ($arrResponseRelacion['sStatus'] == 'success') {
                foreach ($arrResponseRelacion['arrData'] as $row_relacion)
                  $facturacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
              }

              if ( empty($facturacion) && !empty($liquidacion) ) {
                $facturacion = '';
                $arrParams = array(
                  'iRelacionDatos' => 5,
                  'iIdOrigenTabla' => $liquidacion
                );
                $arrResponseRelacion = $HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
                if ($arrResponseRelacion['sStatus'] == 'success') {
                  foreach ($arrResponseRelacion['arrData'] as $row_relacion)
                    $facturacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
                }
              }

              $orden_ingreso = '';
              $arrParams = array(
                'iRelacionDatos' => 6,
                'iIdOrigenTabla' => $row->ID_Documento_Cabecera_OV
              );
              $arrResponseRelacion = $this->HelperModel->getRelacionTablaMultiplePresupuestoAndOI($arrParams);
              if ($arrResponseRelacion['sStatus'] == 'success') {
                foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
                  $orden_ingreso .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
                }
              }

              $fTotalCantidad+=$row->Qt_Producto;
            ?>
              <tr class="<?php echo $sNameClassTrDetalle; ?>">
                <td class="text-left" style="width: 6%;font-size:5.5px;">&nbsp;<br/><?php echo $orden_ingreso; ?><br/></td>
                <td class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Marca_Vehiculo; ?><br/></td>
                <td class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Modelo_Vehiculo; ?><br/></td>
                <td class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Placa_Vehiculo; ?><br/></td>
                <td class="text-left" style="width: 5%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Color_Vehiculo; ?><br/></td>
                <td class="text-right" style="width: 5%;text-align: center;font-size:5px;">&nbsp;<br/><?php echo numberFormat($row->Qt_Producto, 2, '.', ','); ?><br/></td> 
                <th class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat($row->Ss_Precio, 2, '.', ','); ?><br/></th>
                <td class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat($row->Ss_Total_Detalle/ 1.18, 2, '.', ','); ?><br/></td>
                <td class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat($row->Ss_Total_Detalle, 2, '.', ','); ?><br/></td>
                <!-- REP_CONTRATISTA_FIX - I  -->  
                <!-- td class="text-right" style="width: 8%;text-align: center;font-size:5px;">&nbsp;<br/><php echo $row->ID_Documento_Cabecera_OV; ><br/></td-->
                <td class="text-right" style="width: 8%;text-align: center;font-size:5px;">&nbsp;<br/><?php echo $row->Nro_Correl_Presupuesto; ?><br/></td>
                <!-- REP_CONTRATISTA_FIX - F  -->  
                <td class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat($row->Precio_Cliente, 2, '.', ','); ?><br/></td>
                <td class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat(($row->Precio_Cliente)*1.18, 2, '.', ','); ?><br/></td>
                <!-- REP_CONTRATISTA_FIX - I  --> 
                <td class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo $row->No_Signo . ' ' . numberFormat(($row->Precio_Cliente - ($row->Ss_Total_Detalle/ 1.18) ), 2, '.', ','); ?><br/></td>
                <td class="text-right" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo numberFormat((($row->Precio_Cliente - ($row->Ss_Total_Detalle/ 1.18) )/$row->Precio_Cliente)*100, 2, '.', ','). ' %'; ?><br/></td> 
                 <!-- REP_CONTRATISTA_FIX - F  --> 
                <td class="text-center" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo ToDateBD($row->Fe_Emision); ?><br/></td>
                 <th class="text-center" style="width: 6%;font-size:5px;">&nbsp;<br/><?php echo ToDateBD($row->Fe_Entrega_Tentativa); ?><br/></th>
                <td class="text-center" style="width: 6%;font-size:5px;">&nbsp;<br><?php echo $facturacion; ?><br/></td>
              </tr>
              <?php
              $iCounter++;
            } ?>
        </tbody>
      </table>
	  	<table class="table_pdf" cellpadding="2">
        <thead>
          <tr class="tr-theadFormat tr-totales">
            <th class="text-right" style="width: 25%;font-size:6px;">TOTAL</th>
            <th class="text-right" style="width: 5%;font-size:6px;"><?php echo numberFormat($fTotalCantidad, 2, '.', ','); ?></th>
            <th class="text-right" style="width: 12%;font-size:6px;"><?php echo numberFormat($arrData[0]->Ss_Total / 1.18, 2, '.', ','); ?></th>
            <th class="text-right" style="width: 6%;font-size:6px;"><?php echo numberFormat($arrData[0]->Ss_Total, 2, '.', ','); ?></th>
          </tr>
          <?php
          if (!empty($arrData[0]->Txt_Glosa)) {
          ?>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3">&nbsp;</th>
          </tr>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3"><b>OBSERVACIONES: <?php echo $arrData[0]->Txt_Glosa; ?></b></th>
          </tr>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3">&nbsp;</th>
          </tr>
          <?php
          }?>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3"></th>
          </tr>
        </thead>
      </table>
      <br/>
      <br/>
      <?php if (!empty($this->empresa->Txt_Cuentas_Bancarias) || !empty($this->empresa->Txt_Nota)) { ?>
	  	<table class="table_pdf" cellpadding="4">
        <thead>
          <?php if (!empty($this->empresa->Txt_Cuentas_Bancarias)) { ?>
          <tr class="tr-theadFormat tr-header tr-header-detalle">
            <th class="text-left">Cuentas Bancarias</th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content"><?php echo $this->empresa->Txt_Cuentas_Bancarias; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content"><br/></th>
          </tr>
          <?php } ?>
      </table>
      <br/>
      <br/>
      <?php } ?>
    </body>
</html>