<!DOCTYPE html>
<html>
	<head>
    <meta http-equiv=Content-Type content=text/html; charset=UTF-8/>
		<title>Vista Preliminar</title>
      <style type=text/css>
        .table_pdf {
          width: 100%;
        }
    
        .tr-thead th.title {
          color: #000000;
          font-size: 19px;
          font-family: "Arial", Helvetica, sans-serif;
          font-weight: bold;
        }

        .tr-thead th.sub_title_fecha {
          color: #000000;
          font-size: 10px;
          font-family: "Arial", Helvetica, sans-serif;
        }
        
        .tr-thead th.sub_title_nro {
          color: #000000;
          font-size: 10px;
          font-family: "Arial", Helvetica, sans-serif;
          font-weight: bold;
        }

        .tr-sub_thead th.title {
          color: #000000;
          font-size: 10px;
          font-family: "Arial", Helvetica, sans-serif;
          font-weight: bold;
        }

        .tr-sub_thead th.content {
          font-size: 9px;
          font-family: "Arial", Helvetica, sans-serif;
        }
        
        .tr-theadFormat th{
          font-weight: bold;
          font-family: Arial;
        }
        
        .tr-header-detalle th{
          font-size: 9px;
          font-family: Arial;
          background-color: #F2F5F5;
        }

        .tr-header th{
          font-size: 9px;
          font-family: "Arial", Helvetica, sans-serif;
          background-color: #F2F5F5;
        }
        
        .tr-footer th{
          font-size: 9px;
          font-family: "Arial", Helvetica, sans-serif;
          background-color: #FFFFFF;
        }
        
        .tr-totales th{
          font-size: 9px;
          font-family: "Arial", Helvetica, sans-serif;
          background-color: #FFFFFF;
        }
        
        .tr-fila_impar td{
          font-size: 8px;
          font-family: Arial;
          background-color: #F2F5F5;
        }
        
        .tr-fila_par td{
          font-size: 8px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-importe_letras{
          font-size: 9px;
          font-family: "Arial", Helvetica, sans-serif;
          background-color: #FFFFFF;
        }
        
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
      </style>
    </head>
    <body>
      <br/>
      <?php
			  $Po_IGV = "";
        foreach($arrData as $row)
          $Po_IGV = "18 %";
      ?>
	  	<table class="table_pdf" border="0">
        <thead>
          <tr class="tr-sub_thead">
            <th class="text-left title" style="width: 60%;"><b>CLIENTE</b></th>
            <th class="text-left content" style="width: 40%;"><b>FECHA EMISIÓN: </b><?php echo ToDateBD($arrData[0]->Fe_Emision); ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->No_Tipo_Documento_Identidad_Breve . ': ' .$arrData[0]->Nu_Documento_Identidad; ?></th>
            <th class="text-left content" style="width: 40%;"><b>FECHA DE VENC: </b><?php echo ToDateBD($arrData[0]->Fe_Vencimiento); ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->No_Entidad; ?></th>
            <th class="text-left content" style="width: 40%;"><b>MONEDA: </b><?php echo $arrData[0]->No_Moneda; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->Txt_Direccion_Entidad; ?></th>
            <th class="text-left content" style="width: 40%;"><?php echo (!empty($Po_IGV) ? '<b>IGV: </b>' . $Po_IGV : '-'); ?></th>
          </tr>
        </thead>
      </table>
      <br/>
        <?php if (!empty($arrData[0]->Txt_Glosa)) { ?>
        <tr class="tr-importe_letras">
          <th class="text-center" rowspan="2" colspan="2">&nbsp;</th>
        </tr>
        <tr class="tr-importe_letras">
          <th class="text-left" rowspan="2" colspan="2"><strong>OBSERVACIONES: <?php echo $arrData[0]->Txt_Glosa; ?></strong></th>
        </tr>
        <tr class="tr-importe_letras">
          <th class="text-center" rowspan="2" colspan="2">&nbsp;</th>
        </tr>
        <?php } ?>
      <br/>
	  	<table class="table_pdf" cellpadding="5">
        <thead>
          <tr class="tr-theadFormat tr-header tr-header-detalle">
            <th class="text-center" style="width: 15%">CANTIDAD</th>
            <th class="text-center" style="width: 60%">DESCRIPCIÓN</th>
            <th class="text-right" style="width: 10%">PRECIO</th>
            <th class="text-right" style="width: 15%">IMPORTE</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $Ss_Gravada = 0.00;
            $Ss_Exonerada = 0.00;
            $Ss_Inafecto = 0.00;
            $Ss_Gratuita = 0.00;
            $Ss_IGV = 0.00;
            $Ss_Total = 0.00;           
            $iCounter = 0; 
            $sNameClassTrDetalle = '';
            foreach($arrData as $row) {
              $sNameClassTrDetalle = ($iCounter%2) == 0 ? 'tr-fila_par' : 'tr-fila_impar';

              if ($row->Nu_Tipo_Impuesto == 1){//IGV
                $Ss_IGV += $row->Ss_Impuesto_Producto;              
                $Ss_Gravada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
                $Ss_Inafecto += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
                $Ss_Exonerada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
                $Ss_Gratuita = $row->Ss_SubTotal_Producto;
              }
                
              $Ss_Total += $row->Ss_Total_Producto;
            ?>
              <tr class="<?php echo $sNameClassTrDetalle; ?>">
                <td class="text-center" style="width: 15%"><?php echo numberFormat($row->Qt_Producto, 3, '.', ','); ?></td>
                <td class="text-left" style="width: 60%"><?php echo $row->No_Producto; ?></td>
                <td class="text-right" style="width: 10%"><?php echo numberFormat($row->Ss_Precio, 2, '.', ','); ?></td>
                <td class="text-right" style="width: 15%"><?php echo numberFormat($row->Ss_Total_Producto, 2, '.', ','); ?></td>
              </tr>
              <?php
              $iCounter++;
            } ?>
        </tbody>
      </table>
	  	<table class="table_pdf" cellpadding="2">
        <thead>
          <?php if ($Ss_Gravada > 0.00) { ?>
          <tr class="tr-theadFormat tr-totales">
            <th class="text-right" style="width: 65%;"></th>
            <th class="text-right" style="width: 20%;">GRAVADA</th>
            <th class="text-right" style="width: 15%;"><?php echo numberFormat($Ss_Gravada, 2, '.', ','); ?></th>
          </tr>
          <?php } ?>
          <?php if ($Ss_Inafecto > 0.00) { ?>
          <tr class="tr-theadFormat tr-totales">
            <th class="text-right" style="width: 65%"></th>
            <th class="text-right" style="width: 20%">INAFECTO</th>
            <th class="text-right" style="width: 15%"><?php echo numberFormat($Ss_Inafecto, 2, '.', ','); ?></th>
          </tr>
          <?php } ?>
          <?php if ($Ss_Exonerada > 0.00) { ?>
          <tr class="tr-theadFormat tr-totales">
            <th class="text-right" style="width: 65%"></th>
            <th class="text-right" style="width: 20%">EXONERADA</th>
            <th class="text-right" style="width: 15%"><?php echo numberFormat($Ss_Exonerada, 2, '.', ','); ?></th>
          </tr>
          <?php } ?>
          <?php if ($Ss_IGV > 0.00) { ?>
          <tr class="tr-theadFormat tr-totales">
            <th class="text-right" style="width: 65%"></th>
            <th class="text-right" style="width: 20%">IGV</th>
            <th class="text-right" style="width: 15%"><?php echo numberFormat($Ss_IGV, 2, '.', ','); ?></th>
          </tr>
          <?php } ?>
          <tr class="tr-theadFormat tr-totales">
            <th class="text-right" style="width: 65%;"></th>
            <th class="text-right" style="width: 20%;">TOTAL</th>
            <th class="text-right" style="width: 15%;"><?php echo numberFormat($Ss_Total, 2, '.', ','); ?></th>
          </tr>
          <tr class="tr-importe_letras">
            <br/>
            <th class="text-center" colspan="3">IMPORTE EN LETRAS: <?php echo strtoupper($totalEnLetras); ?></th>
          </tr>
          <tr class="tr-importe_letras">
            <th class="text-center" colspan="3"></th>
          </tr>
          <?php
    			$cadena_de_texto = $arrData[0]->Txt_Garantia;
    			$cadena_buscada = '-';
    			$posicion_coincidencia = strpos($cadena_de_texto, $cadena_buscada);
    			if ( strlen($arrData[0]->Txt_Garantia) > 5 && $posicion_coincidencia !== false) { ?>
        	  <?php $arrCadena = explode(',',$arrData[0]->Txt_Garantia);
      		  foreach ($arrCadena as $row) { ?>
      		   <tr class="tr-importe_letras">
      			  <th class="text-left" colspan="3"><b>GUÍA DE REMISIÓN REMITENTE: </b><?php echo $row; ?></th>
      			 </tr>
      			<?php
      			} ?>
          <?php
          }
          if ($arrData[0]->Nu_Detraccion == 1) {
          ?>
          <tr class="tr-importe_letras">
            <th class="text-left" colspan="3"><strong>OPERACIÓN SUJETA AL SPOT: DETRACCIÓN</strong></th>
          </tr>
          <?php } ?>
        </thead>
      </table>
      <br/>
      <br/>
	  	<table class="table_pdf" cellpadding="4">
        <thead>
          <tr class="tr-sub_thead">
            <th class="text-center content"><b>Copia para control administrativo</b></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-center content">Autorizado mediante R.I. NRO. 034-005-0005315</th>
          </tr>
        </thead>
      </table>
    </body>
</html>