<!DOCTYPE html>
<html>
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>laesystems</title>
      <style type=text/css>
        .table_pdf {
          width: 100%;
          border-collapse: collapse;
        }
    
        .tr-thead_border th{
          font-size: 5px;
          border: solid 1px #000000;
        }
        
        .tr-thead th.title {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
          background-color: #eeca19;
          vertical-align: middle;
          line-height: 9px;
          height: 15px;
        }

        .tr-thead th.title_cliente {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
          background-color: #C1C1BE;
          vertical-align: middle;
          line-height: 8px;
          height: 15px;
        }

        .tr-thead th.sub_title_fecha {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          background-color: #eeca19;
        }
        
        .tr-thead th.sub_title_nro {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
          text-align: center;
        }

        .tr-sub_thead th.title {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-sub_thead th.content {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
          padding: 0;
          margin: 0;
        }

        .tr-sub_thead th.contacto_nombres {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-sub_thead th.contacto_cargo {
          color: #838585;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_celular {
          color: #000000;
          font-size: 8px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_correo {
          color: #34bdad;
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-theadFormat th{
          font-weight: bold;
          font-family: Arial;
        }
        
        .tr-theadFormat2 th{
          font-weight: normal;
          font-family: Arial;
        }
        
        .tr-header-detalle th{
          font-size: 8px;
          font-family: Arial;
        }
        
        .tr-header th{
          font-size: 8px;
          font-family: Arial;
        }

        .tr-footer th{
          font-size: 8px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-totales th{
          font-size: 8px;
          font-family: Arial;
          background-color: #FFFFFF;
        }

        .tr-totales td{
          font-size: 6px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-fila_impar td{
          font-size: 6px;
          font-family: Arial;
          background-color: #C1C1BE;
          vertical-align: middle;
          line-height: 5px;
          height: 15px;
        }
        
        .tr-fila_par td{
          font-size: 6px;
          font-family: Arial;
          background-color: #FFFFFF;
          vertical-align: middle;
        }
        
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}

        .color_empresa{
          background-color: #eeca19;
          background: #eeca19;
        }
        .white-borders {
          border: 1px solid white;
        }
        .right-black-border {
          border-right-color: black;
        }
        .left-black-border {
          border-left-color: black;
        }
        .top-black-border {
          border-top-color: black;
        }
        .bottom-black-border {
          border-bottom-color: black;
        }
      </style>
    </head>
    <body>
	  	<table class="table_pdf" border="1" cellpadding="1" cellspacing="0">
        <thead>
          <tr class="tr-thead">
            <th class="text-center title" colspan="2"><?php echo mb_strtoupper($arrData[0]->No_Tipo_Documento_Breve, 'UTF-8'); ?></th>
            <th class="text-center color_empresa title"><?php echo $arrData[0]->ID_Serie_Documento . '-' . $arrData[0]->ID_Numero_Documento; ?></th>
          </tr>
        </thead>
      </table>
      <br>
	  	<table class="table_pdf" border="0" cellpadding="0" cellspacing="0">
        <thead>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="border: 1px solid white; border-left-color:black; width: 60%;"><b>&nbsp;&nbsp;Fecha Emisión:</b> <?php echo ToDateBD($arrData[0]->Fe_Emision); ?></th>
            <th class="text-left content" style="border: 1px solid white; border-right-color:black; width: 40%"><b>Celular Asesor:</b> <?php echo $arrData[0]->Nu_Celular_Entidad_Empleado; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="border: 1px solid white; border-left-color:black; width: 60%"><b>&nbsp;&nbsp;Asesor:</b> <?php echo $arrData[0]->No_Entidad_Empleado; ?></th>
            <th class="text-left content" style="border: 1px solid white; border-right-color:black; width: 40%"><b>Correo:</b> <?php echo $arrData[0]->Txt_Email_Entidad_Empleado; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" colspan="2" style="border-left-color:black; border-right-color:black;"></th>
          </tr>
        </thead>
      </table>
	  	<table class="table_pdf" border="1" cellpadding="4" cellspacing="0">
        <thead>
          <tr class="tr-thead">
            <th class="text-center title_cliente" style="border: 1px solid black; border-top-color:black; width: 55%"><b>DATOS CLIENTE</b></th>
            <th class="text-center title_cliente" style="width: 45%"><b>DATOS VEHÍCULO</b></th>
          </tr>
        </thead>
      </table>
      <br>
	  	<table class="table_pdf" border="0" cellpadding="4" cellspacing="0">
        <thead>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="border: 1px solid white; border-left-color:black; width: 7%">Cliente:</th>
            <th class="text-left content" style="width: 48%"><?php echo $arrData[0]->No_Entidad; ?></th>
            <th class="text-left content" style="border: 1px solid white; border-left-color:black; width: 22.5%">Placa: <?php echo $arrData[0]->No_Placa_Vehiculo; ?></th>
            <th class="text-left content" style="border: 1px solid white; border-right-color:black; width: 22.5%">Color: <?php echo $arrData[0]->No_Color_Vehiculo; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="border: 1px solid white; border-left-color:black; width: 7%"><?php echo $arrData[0]->No_Tipo_Documento_Identidad_Breve . ': '; ?></th>
            <th class="text-left content" style="width: 48%"><?php echo $arrData[0]->Nu_Documento_Identidad; ?></th>
            <th class="text-left content" style="border: 1px solid white; border-left-color:black; width: 22.5%">Marca: <?php echo $arrData[0]->No_Marca_Vehiculo; ?></th>
            <th class="text-left content" style="border: 1px solid white; border-right-color:black; width: 22.5%">KM: 
              <?php 
              $orden_ingreso = '';
              $arrParams = array(
                'iRelacionDatos' => 6,
                'iIdOrigenTabla' => $arrData[0]->ID_Documento_Cabecera
              );
              $arrResponseRelacion = $this->HelperModel->getRelacionTablaMultiplePresupuestoAndOI($arrParams);
              if ($arrResponseRelacion['sStatus'] == 'success') {
                $orden_ingreso = $arrResponseRelacion['arrData'][0]->No_Kilometraje;
              }
              echo $orden_ingreso;
              ?>
            </th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="border: 1px solid white; border-bottom-color:black; border-left-color:black; width: 7%">&nbsp;</th>
            <th class="text-left content" style="border: 1px solid white; border-right-color:black; border-bottom-color:black; width: 48%">&nbsp;</th>
            <th class="text-left content" style="border: 1px solid white; border-left-color:black; border-bottom-color:black; border-right-color:black; width: 45%">Modelo: <?php echo $arrData[0]->No_Modelo_Vehiculo; ?></th>
          </tr>
        </thead>
      </table>
      <br><br><br>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-theadFormat tr-header tr-header-detalle">
            <th class="text-center" style="width: 5%"></th>
            <th class="text-center" style="width: 55%">DESCRIPCIÓN</th>
            <th class="text-center" style="width: 15%">Cant</th>
            <th class="text-center" style="width: 10%">P. Unit</th>
            <th class="text-center" style="width: 15%">Sub Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $Ss_Gravada = 0.00;
            $Ss_Exonerada = 0.00;
            $Ss_Inafecto = 0.00;
            $Ss_Gratuita = 0.00;
            $Ss_IGV = 0.00;
            $Ss_Total = 0.00;
            $iCounter = 0; 
            $sNameClassTrDetalle = '';
            $iIdFamilia = 0;
            $arrDataLinea=array();
            $Ss_Total_Familia = 0.00;
            $Ss_SubTotal_Familia = 0.00;
            $Ss_Impuesto=0.00;
            foreach($arrData as $row) {
              $sNameClassTrDetalle = ($iCounter%2) == 0 ? 'tr-fila_par' : 'tr-fila_impar';
              if ( $iIdFamilia != $row->ID_Familia ) {
                if ($iCounter != 0) {
                  ?>
                  <br />
                  <tr class="tr-fila_impar">
                    <td class="text-left" style="width: 60%; background-color: #C1C1BE; line-height: 12px;" colspan="2"></td>
                    <td class="text-center" style="width: 15%;  background-color: #C1C1BE; line-height: 12px;"></td>
                    <td class="text-left" style="width: 10%; line-height: 12px;">&nbsp;&nbsp;Sub Total</td>
                    <td class="text-right" style="width: 3%; line-height: 12px;">S/</td>
                    <td class="text-right" style="width: 12%; line-height: 12px;"><?php echo numberFormat($Ss_SubTotal_Familia, 2, '.', ','); ?>&nbsp;&nbsp;</td>
                  </tr>
                  <br />
                  <?php
                  $row_linea['Ss_SubTotal_Familia'] = $Ss_SubTotal_Familia;
                  $row_linea['Ss_Total_Familia'] = $Ss_Total_Familia;
                  $arrDataLinea[] = $row_linea;
                  $Ss_Total_Familia = 0.00;
                  $Ss_SubTotal_Familia = 0.00;
                }?>
                <tr class="tr-fila_impar">
                  <td colspan="6" class="text-left"><b>&nbsp;<br/><?php echo $row->No_Familia; ?></b></td>
                </tr>
                <?php
                $iIdFamilia = $row->ID_Familia;
                $row_linea = array();
                $row_linea['No_Familia'] = $row->No_Familia;
              }

              if ($row->Nu_Tipo_Impuesto == 1){//IGV
                
                $Ss_Impuesto = $row->Ss_Impuesto;
                $Ss_IGV += $row->Ss_Impuesto_Producto;              
                $Ss_Gravada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
                $Ss_Inafecto += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
                $Ss_Exonerada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
                $Ss_Gratuita += $row->Ss_SubTotal_Producto;
              }
      
              $Ss_Total += $row->Ss_Total_Producto;
              $Ss_Total_Familia += $row->Ss_Total_Producto;
              $Ss_SubTotal_Familia += $row->Ss_SubTotal_Producto;
              $notaDetalle = (!empty($row->Txt_Nota_Detalle)) ? nl2br(trim($row->Txt_Nota_Detalle)) : '';
              $notaDetalle = implode("<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", explode("<br />", $notaDetalle));
              ?>
              <tr class="tr-fila_par" >
                <td class="text-left" style="width: 5%"></td>
                <td class="text-left" style="width: 55%"><?php echo $row->No_Producto . (!empty($notaDetalle) ? ':<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $notaDetalle : ''); ?></td>
                 <!-- C-03 - Inicio -->
                 <!-- <td class="text-center" style="width: 15%" ?php echo floor(($row->Qt_Producto*100))/100;; ?><br/></td> -->
                 <td class="text-center" style="width: 15%"><?php echo number_format(  $row->Qt_Producto , 2, '.', '')   ; ?><br/></td>
                 <!-- C-03 - Fin -->
                <td class="text-center" style="width: 10%"><?php echo "S/ ".numberFormat($row->Ss_Precio / $row->Ss_Impuesto, 2, '.', ','); ?><br/></td>
                <td class="text-center" style="width: 15%" colspan="2"><?php echo "S/ ".numberFormat($row->Ss_SubTotal_Producto, 2, '.', ','); ?><br/></td>
              </tr>
              <?php
              $iCounter++;
            }
            ?>
            <br />
            <tr class="tr-fila_impar" nobr="true">
              <td class="text-left" style="width: 60%; background-color: #C1C1BE; line-height: 12px;" colspan="2"></td>
              <td class="text-center" style="width: 15%;  background-color: #C1C1BE; line-height: 12px;"></td>
              <td class="text-left" style="width: 10%; line-height: 12px;">&nbsp;&nbsp;Sub Total</td>
              <td class="text-right" style="width: 3%; line-height: 12px;">S/</td>
              <td class="text-right" style="width: 12%; line-height: 12px;"><?php echo numberFormat($Ss_SubTotal_Familia, 2, '.', ','); ?>&nbsp;&nbsp;</td>
            </tr>
            <?php
            
            $row_linea['Ss_SubTotal_Familia'] = $Ss_SubTotal_Familia;
            $row_linea['Ss_Total_Familia'] = $Ss_Total_Familia;
            $arrDataLinea[] = $row_linea;
            $Ss_Total_Familia = 0.00;
            $Ss_SubTotal_Familia = 0.00;
            
            ?>
        </tbody>
      </table>
      <br><br><br>
	  	<table class="table_pdf" cellpadding="2">
        <thead>
          <tr class="tr-theadFormat2 tr-header">
            <th class="text-center" style="border: 1px solid white; border-right-color:black; width: 30%; background-color: #ffffff">&nbsp;</th>
            <th class="text-center" style="border: 1px solid white; border-left-color:black; border-top-color:black; border-bottom-color:black; border-right-color:black; width: 70%; background-color: #C1C1BE"><b>Totales</b></th>
          </tr>
        </thead>
        <tbody>
          <?php
            foreach($arrDataLinea as $row) {?>
              <tr class="tr-theadFormat2 tr-totales">
                <td class="text-center" style="border: 1px solid white; border-right-color:black; width: 30%"></td>
                <td class="text-right" style="border: 1px solid white; border-left-color:black; border-bottom-color:black; border-right-color:black; width: 55%;">Sub Total <?php echo $row['No_Familia']; ?>&nbsp;&nbsp;</td>
                <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-left-color:black; width: 3%;"><?php echo $arrData[0]->No_Signo ?></td>
                <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-right-color:black; width: 12%"><?php echo numberFormat($row['Ss_SubTotal_Familia'], 2, '.', ','); ?></td>
              </tr>
            <?php }
            if($arrData[0]->Ss_Descuento > 0.00 && $Ss_Impuesto > 0){
              $Ss_Gravada = $arrData[0]->Ss_Total / $Ss_Impuesto;
              $Ss_IGV = $arrData[0]->Ss_Total - $Ss_Gravada;
            }
          ?>
          <?php if($arrData[0]->Ss_Descuento>0.00){ ?>
          <tr class="tr-theadFormat2 tr-totales">
            <td class="text-right white-borders right-black-border" style="width: 30%"></td>
            <td class="text-right" style="border: 1px solid white; border-left-color:black; border-bottom-color:black; border-right-color:black; width: 55%;">Descuento (<?php echo $arrData[0]->Po_Descuento; ?>%)&nbsp;&nbsp;</td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-left-color:black; width: 3%;"><?php echo $arrData[0]->No_Signo ?></td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-right-color:black; width: 12%"><?php echo numberFormat($arrData[0]->Ss_Descuento, 2, '.', ','); ?></td>
          </tr>
          <?php } ?>
          <?php if ($Ss_Gravada > 0.00) { ?>
          <tr class="tr-theadFormat2 tr-totales">
            <td class="text-right white-borders right-black-border" style="width: 30%"></td>
            <td class="text-right" style="border: 1px solid black; width: 55%; background-color: #FFFFFF">Suma Total Sin Igv&nbsp;&nbsp;</td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-left-color:black; width: 3%;"><?php echo $arrData[0]->No_Signo ?></td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-right-color:black; width: 12%"><?php echo numberFormat($Ss_Gravada, 2, '.', ','); ?></td>
          </tr>
          <?php } ?>
          <?php if($Ss_Inafecto>0.00){ ?>
          <tr class="tr-theadFormat2 tr-totales">
            <td class="text-right white-borders right-black-border" style="width: 30%"></td>
            <td class="text-right" style="border: 1px solid black; width: 55%; background-color: #FFFFFF">Inafecto&nbsp;&nbsp;</td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-left-color:black; width: 3%;"><?php echo $arrData[0]->No_Signo ?></td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-right-color:black; width: 12%"><?php echo  numberFormat($Ss_Inafecto, 2, '.', ','); ?></td>
          </tr>
          <?php } ?>
          <?php if($Ss_Exonerada>0.00){ ?>
          <tr class="tr-theadFormat2 tr-totales">
            <td class="text-right white-borders right-black-border" style="width: 30%"></td>
            <td class="text-right" style="border: 1px solid black; width: 55%; background-color: #FFFFFF">Exonerada&nbsp;&nbsp;</td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-left-color:black; width: 3%;"><?php echo $arrData[0]->No_Signo ?></td>
            <td class="text-right" style="border: 1px solid white; border-bottom-color:black; border-right-color:black; width: 12%"><?php echo  numberFormat($Ss_Exonerada, 2, '.', ','); ?></td>
          </tr>
          <?php } ?>
          <?php if($Ss_IGV>0.00){ ?>
          <tr class="tr-theadFormat2 tr-totales">
            <td class="text-right white-borders right-black-border" style="width: 30%"></td>
            <td class="text-right white-borders bottom-black-border right-black-border" style="width: 55%;">Igv&nbsp;&nbsp;</td>
            <td class="text-right white-borders bottom-black-border" style="width: 3%;"><?php echo $arrData[0]->No_Signo ?></td>
            <td class="text-right white-borders bottom-black-border right-black-border" style="width: 12%"><?php echo  numberFormat($Ss_IGV, 2, '.', ','); ?></td>
          </tr>
          <?php } ?>
          <?php if($Ss_Total>0.00){ ?>
          <tr class="tr-theadFormat2 tr-totales">
            <td class="text-right white-borders right-black-border" style="width: 30%"></td>
            <td class="text-right white-borders bottom-black-border right-black-border" style="width: 55%;">Suma Total con Igv&nbsp;&nbsp;</td>
            <td class="text-right white-borders bottom-black-border" style="width: 3%;"><?php echo $arrData[0]->No_Signo ?></td>
            <td class="text-right white-borders bottom-black-border right-black-border" style="width: 12%"><?php echo numberFormat($arrData[0]->Ss_Total, 2, '.', ','); ?></td>
          </tr>
          <?php } ?>
        </tbody>
      
        
      </table>
	  	
      <br/><br/>
      <?php if ( !empty($arrData[0]->Txt_Glosa) || !empty($arrData[0]->Txt_Garantia)) { ?>
	  	<table class="table_pdf" cellpadding="4">
        <thead>
          <?php if (!empty($arrData[0]->Txt_Garantia)) { ?>
          <tr class="tr-theadFormat tr-header">
            <th class="text-left">Garantía</th>
          </tr>
          <tr class="tr-sub_thead tr-header">
            <th class="text-left content"><?php echo $arrData[0]->Txt_Garantia; ?></th>
          </tr>
          <tr class="tr-sub_thead tr-header">
            <th class="text-left content"><br/></th>
          </tr>
          <?php } ?>
          <?php if (!empty($arrData[0]->Txt_Glosa)) { ?>
          <tr class="tr-theadFormat tr-header">
            <th class="text-left content">Observaciones</th>
          </tr>
          <tr class="tr-sub_thead tr-header">
            <th class="text-left content"><?php echo $arrData[0]->Txt_Glosa; ?></th>
          </tr>
          <tr class="tr-sub_thead tr-header">
            <th class="text-left content"><br/></th>
          </tr>
          <?php } ?>
        </thead>
      </table>
      <?php } ?>
      <br><br><br><br><br><br><br><br>
    </body>
</html>