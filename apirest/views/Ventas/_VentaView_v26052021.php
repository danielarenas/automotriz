<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>              
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Tipo</label>
    		  				<select id="cbo-Filtro_TiposDocumento" class="form-control"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>Serie</label>
                  <select id="cbo-Filtro_SeriesDocumento" class="form-control"></select>
                </div>
              </div>
              
              <div class="col-xs-4 col-sm-5 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <input type="tel" id="txt-Filtro_NumeroDocumento" class="form-control input-number" maxlength="8" placeholder="" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-8 col-sm-5 col-md-2">
                <div class="form-group">
                  <label>Estado</label>
    		  				<select id="cbo-Filtro_Estado" class="form-control">
    		  				  <option value="" selected="selected">Todos</option>
    		  				  <option value="6">Completado</option>
    		  				  <option value="8">Completado Enviado</option>
    		  				  <option value="9">Completado Error</option>
    		  				  <option value="7">Anulado</option>
    		  				  <option value="10">Anulado Enviado</option>
    		  				  <option value="11">Anulado Error</option>
        				  </select>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-7 col-md-8">
                <div class="form-group">
                  <label>Cliente</label>
                  <input type="text" id="txt-Filtro_Entidad" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre / # documento de identidad" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                    <button type="button" class="btn btn-success btn-block" onclick="agregarVenta()"><i class="fa fa-plus-circle"></i> Agregar</button>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-Venta" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="no-sort_left">F. Emisión</th>
                  <th class="no-sort">Tipo</th>
                  <th class="no-sort">Serie</th>
                  <th class="no-sort_right">Número</th>
                  <th class="no-sort_left">Cliente</th>
                  <th class="no-sort">M</th>
                  <th class="no-sort_right">Forma Pago</th>
                  <th class="no-sort_right">Tipo Forma Pago</th>
                  <th class="no-sort_right">Total</th>
                  <th class="no-sort_right">Saldo</th>
                  <th class="no-sort_right">Detracción</th>
                  <th class="no-sort_right">Retención</th>
                  <th class="no-sort">Pago</th>
                  <th class="no-sort">Estado</th>
                  <th class="no-sort"></th><!-- send SUNAT -->
                  <th class="no-sort"></th>
                  <th class="no-sort"></th>
                  <th class="no-sort"></th><!-- dropdown -->
                  <th class="no-sort"></th><!-- Representación interna -->
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <?php
          $attributes = array('id' => 'form-cobrar_cliente');
          echo form_open('', $attributes);
          ?>
          <!-- modal cobrar cliente -->
          <div class="modal fade modal-cobrar_cliente" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="text-center" id="modal-header-cobrar_cliente-title"></h4>
                </div>

                <div class="modal-body">
                  <input type="hidden" name="iIdDocumentoCabecera" class="form-control">
                  <input type="hidden" name="iEstadoLavadoRecepcionCliente" class="form-control" value="2">
                  <input type="hidden" id="hidden-cobrar_cliente-fsaldo" name="fSaldoCliente" class="form-control" value="0">
                  
                  <div class="row">
                    <div class="col-sm-12">
                      <label id="cobrar_cliente-modal-body-cliente"></label>
                    </div>
                    <div class="col-sm-12">
                      <label id="cobrar_cliente-modal-body-saldo_cliente"></label>
                    </div>
                  </div>

                  <div class="row div-forma_pago">
                    <div class="col-sm-3">
                      <label>Forma Pago</label>
                      <div class="form-group">
                        <select id="cbo-modal_forma_pago" name="iFormaPago" class="form-control" style="width: 100%;"></select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    
                    <div class="col-sm-3 div-modal_datos_tarjeta_credito">
                      <label>Tarjeta</label>
                      <div class="form-group">
                        <select id="cbo-cobrar_cliente-modal_tarjeta_credito" name="iTipoMedioPago" class="form-control" style="width: 100%;"></select>
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    
                    <div class="col-sm-3">
                      <label>Pago cliente</label>
                      <div class="form-group">
                        <input type="tel" class="form-control input-decimal" id="tel-cobrar_cliente-fPagoCliente" name="fPagoCliente" value="" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                  </div><!-- ./ row importes -->
                  
                  <div class="row div-modal_datos_tarjeta_credito">
                    <div class="col-sm-3">
                      <div class="form-group">
                        <input type="tel" id="tel-nu_referencia" name="iNumeroTransaccion" class="form-control input-number" value="" maxlength="10" placeholder="No. Operación" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <input type="tel" id="tel-nu_ultimo_4_digitos_tarjeta" name="iNumeroTarjeta" class="form-control input-number" minlength="4" maxlength="4" value="" placeholder="últimos 4 dígitos" autocomplete="off">
                        <span class="help-block" id="error"></span>
                      </div>
                    </div>
                  </div><!-- ./ row tarjeta de crédito -->
                </div>
                <div class="modal-footer">
                  <div class="col-xs-6">
                    <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
                  </div>
                  <div class="col-xs-6">
                    <button type="button" id="btn-cobrar_cliente" class="btn btn-primary btn-md btn-block pull-center">Cobrar</button>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /. modal cobrar cliente -->
          <?php echo form_close(); ?>
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-Venta');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" id="txt-EID_Empresa" name="EID_Empresa" class="form-control">
          	  <input type="hidden" id="txt-EID_Documento_Cabecera" name="EID_Documento_Cabecera" class="form-control">
          	  <input type="hidden" id="txt-ID_Documento_Cabecera_Orden" name="ID_Documento_Cabecera_Orden" class="form-control">
              <input type="hidden" id="hidden-arrIdPresupuesto" name="arrIdPresupuesto" class="form-control" value="">
              
      			  <div class="row">
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento</b></div>
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8">
                          <div class="form-group">
                            <label>Almacén <span class="label-advertencia">*</span></label>
                            <select id="cbo-almacen" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label>¿Descargar Stock?</label>
                            <select id="cbo-descargar_stock" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label>Tipo <span class="label-advertencia">*</span></label>
                            <select id="cbo-TiposDocumento" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      
                        <div class="col-xs-6 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label>Series <span class="label-advertencia">*</span></label>
                            <select id="cbo-SeriesDocumento" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label>Número <span class="label-advertencia">*</span></label>
                            <input type="tel" id="txt-ID_Numero_Documento" name="ID_Numero_Documento" class="form-control required input-number" disabled>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label>F. Emisión <span class="label-advertencia">*</span></label>
                            <div class="input-group date">
                              <input type="text" id="txt-Fe_Emision" name="Fe_Emision" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            </div>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-4 col-md-4">
                          <div class="form-group">
                            <label>Forma Pago <span class="label-advertencia">*</span></label>
                            <select id="cbo-MediosPago" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-4 col-md-4 div-MediosPago">
                          <div class="form-group">
                            <label>F. Vencimiento <span class="label-advertencia">*</span></label>
                            <div class="input-group date">
                              <input type="text" id="txt-Fe_Vencimiento" class="form-control required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            </div>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-4 col-md-4 div-modal_datos_tarjeta_credito">
                          <label>Tipo</label> (opcional)
                          <div class="form-group">
                            <select id="cbo-tarjeta_credito" class="form-control" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-3">
                          <div class="form-group">
                            <label>Moneda <span class="label-advertencia">*</span></label>
                            <select id="cbo-Monedas" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-5 col-md-3">
                          <label>Detracción</label><br>
                          
                          <div class="col-xs-6">
                            <label style="cursor: pointer;"><input type="radio" name="radio-addDetraccion" class="flat-red" id="radio-InactiveDetraccion" onclick="addDetraccion(this.value);" value="0" checked> No</label>
                          </div>
                          
                          <div class="col-xs-6" title="Solo si es mayor o igual a 700">
                            <label style="cursor: pointer;" title="Solo si es mayor o igual a 700"><input type="radio" name="radio-addDetraccion" class="flat-red" id="radio-ActiveDetraccion" onclick="addDetraccion(this.value);" value="1"> Si</label>
                          </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-5 col-md-3">
                          <label>Retención</label><br>
                          
                          <div class="col-xs-6">
                            <label style="cursor: pointer;"><input type="radio" name="radio-addRetencion" class="flat-red" id="radio-InactiveRetencion" onclick="addRetencion(this.value);" value="0" checked> No</label>
                          </div>
                          
                          <div class="col-xs-6" title="Solo si es menor a 700">
                            <label style="cursor: pointer;" title="Solo si es menor a 700"><input type="radio" name="radio-addRetencion" class="flat-red" id="radio-ActiveRetencion" onclick="addRetencion(this.value);" value="1"> Si</label>
                          </div>
                        </div>
                  
                        <div class="col-xs-12 col-sm-4 col-md-3">
                          <div class="form-group">
                            <label>PDF</label>
                            <select id="cbo-formato_pdf" class="form-control required" style="width: 100%;">
                              <option value="A4" selected="selected">A4</option>
                              <option value="A5">A5</option>
                              <option value="TICKET">Ticket</option>
                            </select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>                      
                      
                      <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group">
                            <label>Guía(s) de Remisión</label>
                            <textarea name="Txt_Garantia" class="form-control" placeholder="Ejemplo: 001-123 y multiple: 001-333, 001-444" title="Formato: serie-número separado por signo, si es más de uno coma(,)"></textarea>
                          </div>
                        </div>
                        
                        <div class="col-xs-4">
                          <div class="form-group">
                            <label>Presupuesto <span class="label-advertencia">*</span></label>
                            <input type="tel" id="txt-Nu_Presupuesto" name="Nu_Presupuesto" class="form-control required input-number" maxlength="20" autocomplete="off">
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>
                        
                      <div class="row">
                        <div class="col-xs-4">
                          <div class="form-group">
                            <label>O/C <span class="label-advertencia">*</span></label>
                            <input type="tel" id="txt-Nu_Orden_Compra" name="Nu_Orden_Compra" class="form-control required input-number" maxlength="20" autocomplete="off">
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-4">
                          <div class="form-group">
                            <label>F. Entrega Factura <span class="label-advertencia">*</span></label>
                            <input type="text" id="txt-Fe_Entrega_Factura" name="Fe_Entrega_Factura" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                  
                        <div class="col-xs-4">
                          <div class="form-group">
                            <label>Servicio</label>
                            <select id="cbo-tipos_servicios" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- /. Datos del documento -->
                
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-user"></i> <b>Cliente</b></div>
                    <div class="panel-body">
                      <div class="col-xs-4 text-center" title="Solo para boletas y ventas menores < 700.00">
                        <label style="cursor: pointer;" title="Solo para boletas y ventas menores < 700.00"><input type="radio" name="addCliente" id="radio-cliente_varios" class="flat-red" value="3" title="Solo para boletas y ventas menores < 700.00"> Rápido</label>
                      </div>
                      
                      <div class="col-xs-4 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-4 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 div-cliente_existente">
                        <br>
                        <div class="form-group">
                          <label>Nombre Cliente</label>
                          <input type="hidden" id="txt-AID" name="AID" class="form-control required">
                          <input type="text" id="txt-ANombre" name="ANombre" class="form-control autocompletar " data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-7 div-cliente_existente">
                        <div class="form-group">
                          <label>Número Documento Identidad</label>
                          <input type="text" id="txt-ACodigo" name="ACodigo" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12" style="display:none">
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      <!-- /. Cliente Existente -->
                      
                      <!-- Cliente Nuevo -->
                      <div class="col-xs-6 col-sm-5 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Tipo Doc. Identidad <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-TiposDocumentoIdentidadCliente" name="ID_Tipo_Documento_Identidad" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-6 col-sm-5 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label id="label-Nombre_Documento_Identidad_Cliente">DNI</label></span>
                          <input type="text" id="txt-Nu_Documento_Identidad_Cliente" name="Nu_Documento_Identidad_Cliente" class="form-control input-Mayuscula input-codigo_barra" placeholder="Ingresar número" value="" maxlength="8" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-2 col-md-2 text-center div-cliente_nuevo">
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_venta_cliente" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
              
                      <div class="col-xs-12 col-md-10 div-cliente_nuevo">
                        <div class="form-group">
                          <label id="label-No_Entidad_Cliente">Nombre(s) y Apellidos</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-No_Entidad_Cliente" name="No_Entidad_Cliente" class="form-control" placeholder="Ingresar nombre" value="" autocomplete="off" maxlength="100">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-9 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" id="txt-Txt_Direccion_Entidad_Cliente" name="Txt_Direccion_Entidad_Cliente" class="form-control" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-3 div-cliente_nuevo">
                        <div class="form-group estado">
                          <label>Estado <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-Estado" name="Nu_Estado" class="form-control required">
            		  				  <option value="1">Activo</option>
            		  				  <option value="0">Inactivo</option>
            		  				</select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-3 col-md-4" style="display:none">
                        <div class="form-group">
                          <label>Telefono</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Telefono_Entidad_Cliente" name="Nu_Telefono_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 9999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-5" style="display:none">
                        <div class="form-group">
                          <label>Celular</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Celular_Entidad_Cliente"  name="Nu_Celular_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      <!-- /. Cliente Nuevo -->
                    </div>
                  </div>
                </div><!-- panel-cliente -->
                <!-- Vendedor -->
      			    <div class="col-sm-12 col-md-6">
      			      <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-comment-o"></i> <b>Vendedor</b></div>
                    <div class="panel-body">
                      <div class="col-xs-12 col-sm-8">
                        <div class="form-group">
                          <label>Personal</label>
            		  				<select id="cbo-vendedor" name="ID_Mesero" class="form-control"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                          <label>Porcentaje</label>
            		  				<select id="cbo-porcentaje" name="Po_Comision" class="form-control"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /. Vendedor -->
              </div><!-- ./Cabecera -->
              
      			  <div class="row">
                <div class="col-sm-12 col-md-12">
        			    <div class="panel panel-default div-DocumentoModificar">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento a Modificar</b></div>
                    <div class="panel-body">
                      <div class="col-xs-6 col-sm-4 col-md-2">
                        <div class="form-group">
                          <input type="hidden" id="txt-ID_Documento_Guardado">
                          <label>Tipo <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-TiposDocumentoModificar" class="form-control" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    
                      <div class="col-xs-6 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>Series <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-SeriesDocumentoModificar" class="form-control" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>Número</label><span class="label-advertencia"> *</span>
                          <div class="input-group">
                            <input type="tel" id="txt-ID_Numero_Documento_Modificar" class="form-control input-number" maxlength="8" autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                          <input type="hidden" id="txt-ID_Documento_Guardado">
                          <label>Motivo Modificar <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-MotivoReferenciaModificar" class="form-control" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <p class="div-mensaje_verificarExisteDocumento"></p>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                          <button type="button" id="btn-verificarExisteDocumento" class="btn btn-success btn-md btn-block">Verificar <i class="fa fa-check"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

      			  <div class="row">
                <div class="col-md-12">
        			    <div id="panel-DetalleProductosVenta" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-shopping-cart"></i> <b>Detalle</b></div>
                    <div class="panel-body">
      			          <div class="row">
          	            <input type="hidden" name="Nu_Tipo_Lista_Precio" value="1" class="form-control"><!-- 1 = Venta -->
                        <div class="col-xs-12">
                          <label>Lista de Precio <span class="label-advertencia">*</span></label>
                          <div class="form-group">
                            <select id="cbo-lista_precios" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-6">
                          <div class="form-group">
                            <label>Producto / Servicio <span class="label-advertencia">*</span></label>
                            <input type="hidden" id="txt-Nu_Tipo_Registro" class="form-control" value="1"><!-- Venta -->
                            <input type="hidden" id="txt-Nu_Tipo_Producto" class="form-control" value="2"><!-- No muestra los productos de tipo interno -->
                            <input type="hidden" id="txt-Nu_Compuesto" class="form-control" value="">
                            <input type="hidden" id="txt-ID_Producto" class="form-control">
                            <input type="hidden" id="txt-Nu_Codigo_Barra" class="form-control">
                            <input type="hidden" id="txt-Ss_Precio" class="form-control">
                            <input type="hidden" id="txt-ID_Impuesto_Cruce_Documento" class="form-control">
                            <input type="hidden" id="txt-Nu_Tipo_Impuesto" class="form-control">
                            <input type="hidden" id="txt-Ss_Impuesto" class="form-control">
                            <input type="hidden" id="txt-Qt_Producto" class="form-control">
                            <input type="hidden" id="txt-nu_tipo_item" class="form-control">
                            <input type="hidden" name="arrIdPresupuesto_Prespuesto_Autocomplete" class="form-control">
                            <textarea name="Txt_Glosa_Prespuesto_Autocomplete" style="display:none" class="form-control"></textarea>
                            <input type="text" id="txt-No_Producto" class="form-control autocompletar_detalle" data-global-class_method="AutocompleteController/getAllProduct" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off">
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-3">
                          <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="button" id="btn-addProducto" class="btn btn-success btn-md btn-block"><i class="fa fa-plus-circle"></i> Agregar Item Detalle</button>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-3">
                          <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="button" id="btn-crearItem" class="btn btn-primary btn-md btn-block"><i class="fa fa-plus-circle"></i> Crear Item</button>
                          </div>
                        </div>
                      </div>
                      
      			          <div class="row">
                      <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="table-DetalleProductosVenta" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th style="display:none;" class="text-left"></th>
                              <th class="text-center" style="width: 10%;">Cantidad</th>
                              <th class="text-center" style="width: 30%;">Item</th>
                              <th class="text-center" style="width: 10%;">Valor Unit.</th>
                              <th class="text-center" style="width: 10%;">Precio</th>
                              <th class="text-center" style="width: 15%;">Impuesto Tributario</th>
                              <th class="text-center" style="display:none;">Sub Total</th>
                              <th class="text-center" style="width: 10%;">% Dscto</th>
                              <th class="text-center">Total</th>
                              <th class="text-center"></th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Detalle -->
              </div>
              
      			  <div class="row">
      			    <div class="col-md-12">
      			      <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-comment-o"></i> <b>Observaciones</b></div>
                    <div class="panel-body">
                      <textarea name="Txt_Glosa" class="form-control"></textarea>
                    </div>
                  </div>
                </div>
              </div>
                
      			  <div class="row"><!-- Totales -->
      			    <div class="col-md-8"></div>
                <div class="col-md-4">
      			    <div class="panel panel-default">
                  <div class="panel-heading"><i class="fa fa-money"></i> <b>Totales</b></div>
                  <div class="panel-body">
                    <table class="table" id="table-VentaTotal">
                      <tr>
                        <td><label>% Descuento</label></td>
                        <td class="text-right">
    	  					        <input type="tel" class="form-control input-decimal" id="txt-Ss_Descuento" name="Ss_Descuento" size="3" value="" autocomplete="off" />
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Gravadas</label></td>
                        <td class="text-right">
    	  					        <input type="hidden" class="form-control" id="txt-subTotal" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-subTotal">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Inafectas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-inafecto" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-inafecto">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Exoneradas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-exonerada" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-exonerada">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Gratuitas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-gratuita" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-gratuita">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Descuento Total (-)</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-descuento" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-descuento">0.00</span>
                        </td>
                      </tr>

                      <tr>
                        <td><label>I.G.V. %</label></td>
                        <td class="text-right">
                            <input type="hidden" class="form-control" id="txt-impuesto" value="0.00"/>
                            <span class="span-signo"></span> <span id="span-impuesto">0.00</span>
                        </td>
                      </tr>
                        
                      <tr>
                        <td><label>Total</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-total" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-total">0.00</span>
                        </td>
                      </tr>
                    </table><!-- ./Totales -->
                  </div>
                </div>
              </div>
              
      			  <div class="row">
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->