<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
      <!-- ./New box-header -->
    </div>
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-12 col-sm-4 col-md-3">
                <label>¿Buscar por Fecha de Liquidación? </label>
                <div class="form-group">
                  <select id="cbo-Filtro_fecha_liquidacion" class="form-control" style="width: 100%;">
                    <option value="0">No</option>
                    <option value="1">Si</option>
                  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-4 col-md-2">
                <label>¿Envío Liquidación? </label>
                <div class="form-group">
                  <select id="cbo-Filtro_envio_fecha_liquidacion" class="form-control" style="width: 100%;">
                    <option value="todos">Todos</option>
                    <option value="0">No</option>
                    <option value="1">Si</option>
                  </select>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-1">
                <div class="form-group">
                  <label>Serie</label>
                  <input type="tel" id="txt-Filtro_SerieDocumento" class="form-control input-number" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <input type="tel" id="txt-Filtro_NumeroDocumento" class="form-control input-number" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-8 col-md-12">
                <div class="form-group">
                  <label>Cliente</label>
                  <input type="text" id="txt-Filtro_Entidad" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar Nombre / Nro. Documento Identidad" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Servicio</label>
                  <input type="tel" id="txt-Filtro_Servicio" class="form-control" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Periodo</label>
                  <input type="tel" id="txt-Filtro_Periodo" class="form-control" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>O/C</label>
                  <input type="tel" id="txt-Filtro_OC" class="form-control" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>

              <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Consultar == 1) : ?>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                    <button type="button" id="btn-generar_liquidacion" class="btn btn-info btn-block">Generar Factura</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                    <button type="button" id="btn-generar_liquidacion_nota" class="btn btn-success btn-block">Generar Nota de Venta</button>
                </div>
              </div>
              <?php endif; ?>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive">
            <form id="form-liquidacion_prespuesto" enctype="multipart/form-data" method="post" role="form" autocomplete="off">
              <input type="hidden" id="hidden-ID_Tipo_Documento" name="hidden-ID_Tipo_Documento" class="form-control" value="3" autocomplete="off">
              <table id="table-Serie" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th></th>
                    <th>Almacén</th>
                    <th>F. Emisión</th>
                    <th>Tipo</th>
                    <th>Serie</th>
                    <th>Número</th>
                    <th>Cliente</th>
                    <th>Servicio</th>
                    <th>Periodo</th>
                    <th>¿Envío?</th>
                    <th>F. Entrega Liquidación</th>
                    <th>Vence</th>
                    <th>Total</th>
                    <th>O/C Cliente</th>
                    <th class="no-sort">Estado</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
              </table>
            </form>
          </div>
          
          <?php
          $attributes = array('id' => 'form-modificar');
          echo form_open('', $attributes);
          ?>
          <!-- modal facturar orden -->
          <div class="modal fade modal-modificar" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="text-center" id="modal-header-modificar-title"></h4>
                  <label id="modificar-modal-body-cliente"></label>
                </div>
                <div class="modal-body">
                  <input type="hidden" name="ID_Documento_Cabecera-Modificar" class="form-control" value="">
                  <input type="hidden" name="Fe_Liquidacion_Envio_Presupuesto-Modificar" class="form-control" value="">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <label>Servicio <span class="label-advertencia">*</span></label>
                      <div class="form-group">
                        <textarea name="Txt_Tipo_Servicio_Presupuesto" class="form-control"></textarea>
                      </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <label>Rango Fecha <span class="label-advertencia">*</span></label>
                      <div class="form-group">
                        <textarea name="Txt_Rango_Fecha_Presupuesto" class="form-control"></textarea>
                      </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <label>Orden Compra <span class="label-advertencia">*</span></label>
                      <div class="form-group">
                        <input type="text" id="txt-Txt_Orden_Compra_Presupuesto" name="Txt_Orden_Compra_Presupuesto" class="form-control" placeholder="O/C Cliente" value="" autocomplete="off">
                      </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <label>¿Envío Liquidación? <span class="label-advertencia">*</span></label>
                      <div class="form-group">
                        <select id="cbo-transporte" name="Nu_Envio_Fecha_Liquidacion_Presupuesto" class="form-control" style="width: 100%;">
                          <option value="0">No</option>
                          <option value="1">Si</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <label>F. Entrega Liquidación</label>
                      <div class="form-group">
                        <div class="input-group date" style="width:100%">
                          <input type="text" id="txt-Fe_Liquidacion_Envio_Presupuesto" name="Fe_Liquidacion_Envio_Presupuesto" class="form-control required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <button type="button" id="btn-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Salir</button>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <button type="button" id="btn-modificar" class="btn btn-primary btn-md btn-block pull-center">Modificar</button>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /. modal facturar orden -->
          <?php echo form_close(); ?>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->