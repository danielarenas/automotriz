<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php //array_debug($this->empresa->ID_Empresa); ?>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
      <!-- ./New box-header -->
    </div>
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Consultar == 1) : ?>
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                <?php endif; ?>
                </h3>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                  <label>&nbsp;</label>
                  <button type="button" class="btn btn-success btn-block" onclick="agregarSerie()"><i class="fa fa-plus-circle"></i> Agregar</button>
                <?php endif; ?>
                </h3>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive">
            <table id="table-Serie" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Placa</th>
                  <th>F. Emision</th>
                  <th>Hora Emision</th>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Tipo Servicio</th>
                  <th>Kilometraje</th>
                  <th>Estado</th>
                  <th>Generar OI</th>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                    <!--<th class="no-sort"></th>-->
                  <?php endif; ?>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  <!-- Modal -->
  <?php
  $attributes = array('id' => 'form-Serie');
  echo form_open('', $attributes);
  ?>
  <div class="modal fade" id="modal-Serie" role="dialog">
  <div class="modal-dialog">
  	<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center"></h4>
      </div>
      
    	<div class="modal-body">
    	  <input type="hidden" name="EID_Taller_Agendamiento" class="form-control required">
    	  
			  <div class="row">
          <div class="col-sm-12 col-md-6">
            <label>Cliente <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <input type="hidden" id="txt-AID" name="ID_Entidad" class="form-control required">
              <input type="text" id="txt-ANombre" name="ID_Entidad_Name" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre" value="" autocomplete="off">
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-sm-12 col-md-6">
            <label>Placa <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <input type="hidden" id="txt-ID_Placa" name="ID_Producto_Placa" class="form-control">
              <input type="text" id="txt-No_Placa_Vehiculo_existe" name="ID_Producto_Placa_Name" class="form-control autocompletar_placa_x_cliente" value="" autocomplete="off" placeholder="Ingresar número" maxlength="6">
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-6 col-sm-6 col-md-6">
            <label>F. Emisión <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <div class="input-group date">
                <input type="text" id="txt-Fe_Emision" name="Fe_Emision" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-sm-6 col-md-6">
            <label>Hora Emision<span class="label-advertencia">*</span></label>
            <div class="form-group">
              <input type="text" id="txt-Hora_Emision" name="Hora_Emision" class="form-control required" placeholder="00:00">
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
              <label>Marca</label>
              <input type="text" name="No_Marca_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar Marca" maxlength="30">
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
              <label>Modelo</label>
              <input type="text" name="No_Modelo_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="Ingresar Modelo" maxlength="30">
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
              <label>Tipo Servicio</label>
              <input type="text" name="No_Tipo_Servicio" class="form-control" value="" autocomplete="off" placeholder="Ingresar Tipo de Servicio" maxlength="30">
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
              <label>Kilometraje</label>
              <input type="text" name="Nu_Kilometraje" class="form-control" value="" autocomplete="off" placeholder="Ingresar Kilometraje" maxlength="16">
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
      </div>
      
    	<div class="modal-footer">
			  <div class="row">
          <div class="col-xs-6 col-md-6">
            <div class="form-group">
              <button type="button" class="btn btn-danger btn-md btn-block" data-dismiss="modal"><span class="fa fa-sign-out"></span> Salir</button>
            </div>
          </div>
          <div class="col-xs-6 col-md-6">
            <div class="form-group">
              <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <?php echo form_close(); ?>
  <!-- /.Modal -->
</div>
<!-- /.content-wrapper -->