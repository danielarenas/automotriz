
<script>

var bEstadoValidacion, timesClicked = 0;
var arrImpuestosProducto = '{ "arrImpuesto" : [';
var arrImpuestosProductoDetalle;
    
// Obtenemos ID Cabecera 
$(document).ready(function () {  

      // OC_MODAL_7 - INICIO
      $('#table-DetalleProductosOrdenCompra tbody' ).on('input', '.txt-Ss_Precio_oc', function(){
     
     var fila = $(this).parents("tr");
     var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
     var precio = fila.find(".txt-Ss_Precio_oc").val();
     var cantidad = fila.find(".txt-Qt_Producto_oc").val();
     var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
     var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
     var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
     var descuento = fila.find(".txt-Ss_Descuento").val();
     var total_producto = fila.find(".txt-Ss_Total_Producto").val();
     var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;
     
     if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0){
       $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
       $( '#table-DetalleProductosOrdenCompra tfoot' ).empty();
       if (nu_tipo_impuesto == 1){//CON IGV
         fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
         fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
         fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". ") );
         fila.find(".txt-Ss_Total_Producto").val( (parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". ") );
         
         var $Ss_SubTotal = 0.00;
         var $Ss_Descuento = 0.00;
         var $Ss_IGV = 0.00;
         var $Ss_Total = 0.00;
         $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
           var rows = $(this);
           var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
           var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
           var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
           var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
           var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());
     
           $Ss_Total += $Ss_Total_Producto;
 
           if(isNaN($Ss_Descuento_Producto))
             $Ss_Descuento_Producto = 0;
             
           if (Nu_Tipo_Impuesto == 1){
             $Ss_SubTotal += $Ss_SubTotal_Producto;
             $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
           }
           
           $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
         });  
         $( '#txt-subTotal_oc' ).val( $Ss_SubTotal.toFixed(2) );
         $( '#span-subTotal_oc' ).text( $Ss_SubTotal.toFixed(2) );
         
         $( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
         $( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
         
         $( '#txt-impuesto_oc' ).val( $Ss_IGV.toFixed(2) );
         $( '#span-impuesto_oc' ).text( $Ss_IGV.toFixed(2) );
         
         $( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
         $( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
       } else if (nu_tipo_impuesto == 2){//Inafecto
         fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
         fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad)  - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
 
         var $Ss_Inafecto = 0.00;
         var $Ss_Descuento = 0.00;
         var $Ss_Total = 0.00;
         
         $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
           var rows = $(this);
           var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
           var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
           var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
 
           if(isNaN($Ss_Descuento_Producto))
             $Ss_Descuento_Producto = 0;
             
           if (Nu_Tipo_Impuesto == 2)
             $Ss_Inafecto += $Ss_SubTotal_Producto;
           
           $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
           $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
         });  
         
         $( '#txt-inafecto_oc' ).val( $Ss_Inafecto.toFixed(2) );
         $( '#span-inafecto_oc' ).text( $Ss_Inafecto.toFixed(2) );
         
         $( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
         $( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
         
         $( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
         $( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
       } else if (nu_tipo_impuesto == 3){//Exonerada
         fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
         fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
         
         var $Ss_Exonerada = 0.00;
         var $Ss_Descuento = 0.00;
         var $Ss_Total = 0.00;
         $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
           var rows = $(this);
           var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
           var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
           var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
 
           if(isNaN($Ss_Descuento_Producto))
             $Ss_Descuento_Producto = 0;
             
           if (Nu_Tipo_Impuesto == 3)
             $Ss_Exonerada += $Ss_SubTotal_Producto;
           
           $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
           $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
         }); 
         
         $( '#txt-exonerada_oc' ).val( $Ss_Exonerada.toFixed(2) );
         $( '#span-exonerada_oc' ).text( $Ss_Exonerada.toFixed(2) );
         
         $( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
         $( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
         
         $( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
         $( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
       } else if (nu_tipo_impuesto == 4){//Gratuita
         fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
         fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
         
         var $Ss_Gratuita = 0.00;
         var $Ss_Descuento = 0.00;
         var $Ss_Total = 0.00;
         $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
           var rows = $(this);
           var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
           var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
           var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
 
           if(isNaN($Ss_Descuento_Producto))
             $Ss_Descuento_Producto = 0;
             
           if (Nu_Tipo_Impuesto == 4)
             $Ss_Gratuita += $Ss_SubTotal_Producto;
           
           $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
           $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
         }); 
         
         $( '#txt-gratuita_oc' ).val( $Ss_Gratuita.toFixed(2) );
         $( '#span-gratuita_oc' ).text( $Ss_Gratuita.toFixed(2) );
         
         $( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
         $( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
         
         $( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
         $( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
       } else {//Sin ningun tipo de impuesto
         fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
         fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
         fila.find(".txt-Ss_Total_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". ") );
 
         var $Ss_Subtotal = 0.00;
         var $Ss_Descuento = 0.00;
         var $Ss_Total = 0.00;
         $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
           var rows = $(this);
           var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
           var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
 
           if(isNaN($Ss_Descuento_Producto))
             $Ss_Descuento_Producto = 0;
             
           if (considerar_igv == 0)
             $Ss_Subtotal += $Ss_SubTotal_Producto;
           
           $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
           $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
         });
         
         $( '#txt-subTotal_oc' ).val( $Ss_Subtotal.toFixed(2) );
         $( '#span-subTotal_oc' ).text( $Ss_Subtotal.toFixed(2) );
         
         $( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
         $( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
         
         $( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
         $( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
       }
     }
 
   });
 
      // OC_MODAL_7 - FIN


        // OC_MODAL_5 - INICIO
  $('#table-DetalleProductosOrdenCompra tbody' ).on('input', '.txt-Qt_Producto_oc', function(){
     
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio_oc").val();
    var cantidad = fila.find(".txt-Qt_Producto_oc").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;
    
    if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0){
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $( '#table-DetalleProductosOrdenCompra tfoot' ).empty();
  		if (nu_tipo_impuesto == 1){//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". ") );
  		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". ") );
        
        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());
    
          $Ss_Total += $Ss_Total_Producto;

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 1){
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }
          
          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
        });  
        $( '#txt-subTotal_oc' ).val( $Ss_SubTotal.toFixed(2) );
    		$( '#span-subTotal_oc' ).text( $Ss_SubTotal.toFixed(2) );
    		
    		$( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-impuesto_oc' ).val( $Ss_IGV.toFixed(2) );
    		$( '#span-impuesto_oc' ).text( $Ss_IGV.toFixed(2) );
    		
    		$( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
  		} else if (nu_tipo_impuesto == 2){//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
    		fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad)  - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        
        $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });  
        
        $( '#txt-inafecto_oc' ).val( $Ss_Inafecto.toFixed(2) );
    		$( '#span-inafecto_oc' ).text( $Ss_Inafecto.toFixed(2) );
    		
    		$( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
  		} else if (nu_tipo_impuesto == 3){//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        }); 
        
        $( '#txt-exonerada_oc' ).val( $Ss_Exonerada.toFixed(2) );
    		$( '#span-exonerada_oc' ).text( $Ss_Exonerada.toFixed(2) );
    		
    		$( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 4){//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        }); 
        
        $( '#txt-gratuita_oc' ).val( $Ss_Gratuita.toFixed(2) );
    		$( '#span-gratuita_oc' ).text( $Ss_Gratuita.toFixed(2) );
    		
    		$( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
  		} else {//Sin ningun tipo de impuesto
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". ") );

        var $Ss_Subtotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
          var rows = $(this);
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (considerar_igv == 0)
            $Ss_Subtotal += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });
        
        $( '#txt-subTotal_oc' ).val( $Ss_Subtotal.toFixed(2) );
    		$( '#span-subTotal_oc' ).text( $Ss_Subtotal.toFixed(2) );
    		
    		$( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
  		}
    }

  });

  $( '#table-DetalleProductosOrdenCompra tbody' ).on('click', '#btn-deleteProducto_oc', function(){
    $(this).closest('tr').remove ();
    
    var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
    var $Ss_SubTotal = 0.00;
    var $Ss_Exonerada = 0.00;
    var $Ss_Inafecto = 0.00;
    var $Ss_Gratuita = 0.00;
    var $Ss_IGV = 0.00;
    var $Ss_Total = 0.00;
    var iCantDescuento = 0;
    var globalImpuesto = 0;
    var $Ss_Descuento_p = 0;
    $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
      var rows = $(this);
      var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
      var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
      var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
      var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
      var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

      $Ss_Total += $Ss_Total_Producto;

      if (iGrupoImpuesto == 1) {
        $Ss_SubTotal += $Ss_SubTotal_Producto;
        $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
        globalImpuesto = fImpuesto;
      } else if (iGrupoImpuesto == 2) {
        $Ss_Inafecto += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else if (iGrupoImpuesto == 3) {
        $Ss_Exonerada += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else {
        $Ss_Gratuita += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      }
        
      if(isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;
        
      $Ss_Descuento_p += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
    });
    
    if ($Ss_Descuento > 0.00) {
      var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
      if ($Ss_SubTotal > 0.00) {
        $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
        $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
        $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
        $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
      }

      if ($Ss_Inafecto > 0.00) {
        $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
        $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
        $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
      }
      
      if ($Ss_Exonerada > 0.00) {
        $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
        $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
        $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
      }
      
      if ($Ss_Gratuita > 0.00) {
        $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
        $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
        $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
      }
      
      $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
      $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
    } else
      $Ss_Descuento = $Ss_Descuento_p;

    if(isNaN($Ss_Descuento))
      $Ss_Descuento = 0.00; // AQUI
    
    $( '#txt-subTotal_oc' ).val( $Ss_SubTotal.toFixed(2) );
    $( '#span-subTotal_oc' ).text( $Ss_SubTotal.toFixed(2) );
    
    $( '#txt-exonerada_oc' ).val( $Ss_Exonerada.toFixed(2) );
    $( '#span-exonerada_oc' ).text( $Ss_Exonerada.toFixed(2) );
    
    $( '#txt-inafecto_oc' ).val( $Ss_Inafecto.toFixed(2) );
    $( '#span-inafecto_oc' ).text( $Ss_Inafecto.toFixed(2) );
    
    $( '#txt-gratuita_oc' ).val( $Ss_Gratuita.toFixed(2) );
    $( '#span-gratuita_oc' ).text( $Ss_Gratuita.toFixed(2) );

    $( '#txt-impuest_oc' ).val( $Ss_IGV.toFixed(2) );
    $( '#span-impuesto_oc' ).text( $Ss_IGV.toFixed(2) );
  	
  	$( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
  	$( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );

		$( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
		$( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
		
    if ($( '#table-DetalleProductosOrdenCompra >tbody >tr' ).length == 0)
	      $( '#table-DetalleProductosOrdenCompra' ).hide();
	})
	
  /*
  $('#table-OrdenCompraTotal' ).on('input', '#txt-Ss_Descuento', function(){
    var $Ss_Descuento_Producto = 0.00;
    $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
      var rows = $(this);
      $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
      
      if(isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;
      
      $Ss_Descuento_Producto += $Ss_Descuento_Producto;
    })

    if ($Ss_Descuento_Producto == 0) {
  		var $Ss_Descuento = parseFloat($(this).val());
      var $Ss_SubTotal = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var globalImpuesto = 0;
      $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
        var rows = $(this);
        var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
        var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
        var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
        var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());
  
        $Ss_Total += $Ss_Total_Producto;
  
        if (iGrupoImpuesto == 1) {
          $Ss_SubTotal += $Ss_SubTotal_Producto;
          $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          globalImpuesto = fImpuesto;
        } else if (iGrupoImpuesto == 2) {
          $Ss_Inafecto += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else if (iGrupoImpuesto == 3) {
          $Ss_Exonerada += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else {
          $Ss_Gratuita += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        }
      });
      
      if ($Ss_Descuento > 0.00) {
        var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
        if ($Ss_SubTotal > 0.00) {
          $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
          $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
          $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
          $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
        }

        if ($Ss_Inafecto > 0.00) {
          $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
          $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
          $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
        }
        
        if ($Ss_Exonerada > 0.00) {
          $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
          $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
          $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
        }
      
        if ($Ss_Gratuita > 0.00) {
          $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
          $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
          $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
        }
        
        $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
        $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
      }
      
      $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      $( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      
      $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
      
      $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
      $( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
      
      $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
      $( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );

      $( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
      $( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
    	
    	$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    	$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
  
  		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
  		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
    }
  })
  */
  
 $(document).on("click","#btn-modal_light",function(){
   var nombre = $(this).data('id-cabecera');
   $("#id-cabecera").val(nombre);

       // OBTENER TODA LA DATA AJAX_EDIT
       url = base_url + 'Ventas/OrdenVentaController/ajax_edit/' + nombre;
       $.getJSON( url, function( data ) {
        // OC_MODAL_4 - INICIO
         $("#txt-ID_Placa_oc").val(data.arrEdit[0].ID_Vehiculo);
         $("#txt-No_Placa_Vehiculo_exist_oc").val(data.arrEdit[0].No_Placa_Vehiculo);
         $("#txt-ID_Presupesto_Exist_oc").val(data.arrEdit[0].ID_Documento_Cabecera); 
        // OC_MODAL_7 - INICIO
         $( '#radio-servicio' ).prop('checked', true).iCheck('update');
         $( '#radio-bien' ).prop('checked', false).iCheck('update');
      // OC_MODAL_7 - FIN

       //console.log( data );
       })

       // Lista oi 
       url = base_url + 'HelperController/Cust_getRelacionTablaMultiplePresupuestoAndOI';
       $.post( url, {
                     iRelacionDatos :  6 ,                          
                     iIdOrigenTabla: $("#id-cabecera").val()                       
                   }, function( responseOI ){

           $("#txt-ID_OI_Exist_oc").val(responseOI[0].ID_Orden_Ingreso) ;

       }, 'JSON');  

       url = base_url + 'HelperController/getImpuestos';
      $.post( url , function( response ){
        arrImpuestosProducto = '';
        arrImpuestosProductoDetalle = '';
        for (var i = 0; i < response.length; i++)
          arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
        arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';
        
        // $( '#modal-loader' ).modal('hide');
      }, 'JSON');

 })   


 $( '#btn-modal-guardar-oc' ).click(function(){ 
 

    var arrDetalleOrdenCompra = [];
    var arrValidarNumerosEnCero = [];
    var $counterNumerosEnCero = 0;
    var tr_foot = '';
    

    $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
      var rows = $(this);
      
      var $ID_Producto                  = rows.find("td:eq(0)").text();
      var $Qt_Producto                  = $('td:eq(1) input', this).val();
      var $Ss_Precio                    = $('td:eq(3) input', this).val();
      var $ID_Impuesto_Cruce_Documento  = $('td:eq(4) select', this).val();
      var $Ss_SubTotal                  = $('td:eq(5) input', this).val();
      var $Ss_Descuento                 = $('td:eq(6) input', this).val();
      var $Ss_Total                     = $('td:eq(7) input', this).val();
      var $fDescuentoSinImpuestosItem = rows.find(".td-fDescuentoSinImpuestosItem").text();
      var $fDescuentoImpuestosItem = rows.find(".td-fDescuentoImpuestosItem").text();
      
      if (parseFloat($Ss_Precio) == 0 || parseFloat($Qt_Producto) == 0 || parseFloat($Ss_Total) == 0){
        arrValidarNumerosEnCero[$counterNumerosEnCero] = $ID_Producto;
        $('#tr_detalle_producto' + $ID_Producto).addClass('danger');
      }
      
      var obj = {};
      
      obj.ID_Producto	                = $ID_Producto;
      obj.Ss_Precio	                  = $Ss_Precio;
      obj.Qt_Producto	                = $Qt_Producto;
      obj.ID_Impuesto_Cruce_Documento	= $ID_Impuesto_Cruce_Documento ;   //34
      obj.Ss_SubTotal	                = $Ss_SubTotal;
      obj.Ss_Descuento	              = $Ss_Descuento;
      obj.Ss_Impuesto	                = $Ss_Total - $Ss_SubTotal;
      obj.Ss_Total	                  = $Ss_Total;
      obj.fDescuentoSinImpuestosItem = $fDescuentoSinImpuestosItem;
      obj.fDescuentoImpuestosItem	= $fDescuentoImpuestosItem;
      arrDetalleOrdenCompra.push(obj);
      $counterNumerosEnCero++;
    });
    
    bEstadoValidacion = validatePreviousDocumentToSaveOrderPurchase();
    
    if ( arrDetalleOrdenCompra.length == 0){
  		$( '#panel-DetalleProductosOrdenCompra' ).removeClass('panel-default');
  		$( '#panel-DetalleProductosOrdenCompra' ).addClass('panel-danger');
  		
      $( '#txt-No_Producto_oc' ).closest('.form-group').find('.help-block').html('Documento <b>sin detalle</b>');
  	  $( '#txt-No_Producto_oc' ).closest('.form-group').removeClass('has-success').addClass('has-error');
  	  
		  scrollToError( $("html, body"), $( '#txt-No_Producto_oc' ) );
    } else if (arrValidarNumerosEnCero.length > 0) {
      tr_foot +=
      "<tfoot>"
        +"<tr class='danger'>"
          +"<td colspan='9' class='text-center'>Item(s) con <b>precio / cantidad / total en cero</b></td>"
        +"</tr>"
      +"<tfoot>";
      $( '#table-DetalleProductosOrdenCompra >tbody' ).after(tr_foot);
    } else if (bEstadoValidacion) {
  		$( '#panel-DetalleProductosOrdenCompra' ).removeClass('panel-danger');
  		$( '#panel-DetalleProductosOrdenCompra' ).addClass('panel-default');
  		
  		var arrOrdenCompraCabecera = Array();
  		arrOrdenCompraCabecera = {  
  		  'EID_Empresa'                 : 51 , //$( '[name="EID_Empresa"]' ).val(),
  		  'EID_Documento_Cabecera'      : '' , // $( '[name="EID_Documento_Cabecera"]' ).val(),
  		  'ENu_Estado'                  : 1 , //$( '[name="ENu_Estado"]' ).val(),
  		  'ID_Serie_Documento'          : $( '#cbo-serie_doc_oc' ).val(),
        'ID_Serie_Documento_PK'          : $( '#cbo-serie_doc_oc' ).find(':selected').data('id_serie_documento_pk'),
  		  'ID_Numero_Documento'         : $( '#txt-ID_Num_Doc_oc' ).val(),

         // Las fechas tomsn como referencia Fe_Emision con la fecha vigente de cuando se registro 
  		
  		  'Fe_Emision'                  : $( '#txt-Fe_Emision_oc' ).val(),
  		  'Fe_Vencimiento'              : $( '#txt-Fe_Emision_oc' ).val(),
        'Fe_Recepcion'                : $( '#txt-Fe_Emision_oc' ).val(),
        'Fe_Pago'                     : $( '#txt-Fe_Emision_oc' ).val(),

        // 19 - soles 
  		  'ID_Moneda'                   : 19 ,  //$( '#cbo-Monedas' ).val(),
        // 47 - contado 
  		  'ID_Medio_Pago'               : 47 , //$( '#cbo-MediosPago' ).val(),
  		  'Nu_Descargar_Inventario'     : 0,
  		  'ID_Almacen'                  : 0,
  		  'ID_Entidad'                  : $( '#txt-AID_oc' ).val(),
        'ID_Producto'                 : $( '#txt-ID_Placa_oc' ).val(),
  		  'ID_Contacto'                 : $( '#txt-AID_oc' ).val(),
  		  'Txt_Garantia'                : ' ' , // $( '[name="Txt_Garantia"]' ).val(),
  		  'Txt_Glosa'                   : ' ' , // $( '[name="Txt_Glosa"]' ).val(),
        'Txt_Comentario'              : ' ' , // $( '[name="Txt_Comentario"]' ).val(),
  		  'Po_Descuento'                : '0' , // $( '#txt-Ss_Descuento' ).val(),
  		  'Ss_Descuento'                : $( '#txt-descuento_oc' ).val(),
  		  'Ss_Total'                    : $( '#txt-total_oc' ).val(),
        'Ss_SubTotal'                 : $( '#txt-subTotal_oc' ).val(),
        'Ss_Impuesto'                 : $( '#txt-impuesto_oc' ).val(),
  		  'ID_Lista_Precio_Cabecera'    : 0 , // $( '#cbo-lista_precios' ).val(),
        'Nu_Orden_Ingreso'            : $( '#txt-ID_OI_Exist_oc' ).val(),
        'Nu_Presupuesto'              : $( '#txt-ID_Presupesto_Exist_oc' ).val(),
        'Nu_Operacion'                : '0' , //$( '#txt-Nu_Operacion' ).val(),
        'Nu_Estado_Conta'             : $( '#txt-estado_conta_oc' ).val(),
        'Nu_Estado_OC'                : $( '#txt-estado_oc' ).val(), 
        // OC_MODAL_7 - INICIO
        'ServicioBien'                : $('[name="ServicioBien"]:checked').attr('value')
        // OC_MODAL_7 - FIN
  		};
  		
      var arrProveedorNuevo = {};
      var arrContactoNuevo = {};
      
      $( '#btn-modal-guardar-oc' ).text('');
      $( '#btn-modal-guardar-oc' ).attr('disabled', true);
      $( '#btn-modal-guardar-oc' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      
      //$( '#modal-loader' ).modal('show');
  
      url = base_url + 'Logistica/OrdenCompraController/crudOrdenCompra';
    	$.ajax({
        type		  : 'POST',
        dataType	: 'JSON',
    		url		    : url,
    		data		  : {
          // OC_MODAL_3 - INICIO
          registra_deta_presupuesto_oc : 'Y',
          // OC_MODAL_3 - FIN

          // OC_MODAL_7 - INICIO
          Creado_Por : $( '#Txt_Creado_Por' ).val(),
          Usuario_Creador : $( '#Txt_Usuario_oc' ).val(),
          // OC_MODAL_7 - FIN

    		  arrOrdenCompraCabecera : arrOrdenCompraCabecera,
    		  arrDetalleOrdenCompra : arrDetalleOrdenCompra, 
           
        
    		},
    		success : function( response ){

           
          /*
    		  $( '#modal-loader' ).modal('hide');
    		  
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
      	  $( '#modal-message' ).modal('show');
      	  */
          
    		  if (response.status == 'success'){
    		    accion_orden_compra='';

          // 03_08-2 INICIO 

        // OBTENER TODA LA DATA AJAX_EDIT
        /* url = base_url + 'Ventas/OrdenVentaController/ajax_edit/' + nombre;
       $.getJSON( url, function( data ) {
        // OC_MODAL_4 - INICIO
         $("#txt-ID_Placa_oc").val(data.arrEdit[0].ID_Vehiculo);
         $("#txt-No_Placa_Vehiculo_exist_oc").val(data.arrEdit[0].No_Placa_Vehiculo);
         $("#txt-ID_Presupesto_Exist_oc").val(data.arrEdit[0].ID_Documento_Cabecera); 
        // OC_MODAL_7 - INICIO
         $( '#radio-servicio' ).prop('checked', true).iCheck('update');
         $( '#radio-bien' ).prop('checked', false).iCheck('update');
      // OC_MODAL_7 - FIN

       //console.log( data );
       })
      
       $( '#radio-servicio' ).prop('checked', true).iCheck('update');
         $( '#radio-bien' ).prop('checked', false).iCheck('update');

       // Lista oi 
       url = base_url + 'HelperController/Cust_getRelacionTablaMultiplePresupuestoAndOI';
       $.post( url, {
                     iRelacionDatos :  6 ,                          
                     iIdOrigenTabla: $("#id-cabecera").val()                       
                   }, function( responseOI ){

           $("#txt-ID_OI_Exist_oc").val(responseOI[0].ID_Orden_Ingreso) ;

       }, 'JSON');  

       url = base_url + 'HelperController/getImpuestos';
      $.post( url , function( response ){
        arrImpuestosProducto = '';
        arrImpuestosProductoDetalle = '';
        for (var i = 0; i < response.length; i++)
          arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
        arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';
        
        // $( '#modal-loader' ).modal('hide');
      }, 'JSON');
*/
      // 03_08-2 FIN 




                // OC_MODAL_5-2 - INICIO 
                // OC_MODAL_5-3 - INICIO
                 $( '#GenerarOCModal' ).modal('hide');
                 
                //$( '#modal-loader' ).modal('hide');
                $( '#modal-message' ).modal('show');    	
              $( '.modal-message' ).addClass(response.style_modal);
              $( '.modal-title-message' ).text(response.message);
              setTimeout(function() {$('#modal-message').modal('hide'); }, 5100);

              // 03_08-3 INICIO
              window.location.reload()
              // document.getElementById("btn-filter").click();
              // 03_08-3 FIN

                /*
                reload_table_orden_venta();
              } else {
                $( '.modal-message' ).addClass(response.style_modal);
                $( '.modal-title-message' ).text(response.message);
                //if ( response.message_nubefact.length > 0 )
                  //$( '.modal-title-message' ).text(response.message_nubefact);
                setTimeout(function() {$('#modal-message').modal('hide');}, 4000);
              */
             }

                  /*
                  $('#txt-Filtro_Entidad').val('');
                  $('#txt-Filtro_NumeroDocumento').val('');
                  $('#txt-Filtro_SerieDocumento').val('');
                  
                  
                  $( '#form-OrdenCompra' )[0].reset();
                  $( '.div-AgregarEditar' ).hide();
                  $( '.div-Listar' ).show();
                  $( '.modal-message' ).addClass(response.style_modal);
                  $( '.modal-title-message' ).text(response.message);
                  setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
                  // reload_table_orden_compra();
                } else {
                  $( '.modal-message' ).addClass(response.style_modal);
                  $( '.modal-title-message' ).text(response.message);
                  setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
                }
                */
       // OC_MODAL_5-3 - FIN

          $( '#btn-modal-guardar-oc' ).text('');
          $( '#btn-modal-guardar-oc' ).append( '<span class="fa fa-save"></span> Guardar (ENTER)' );
          $( '#btn-modal-guardar-oc' ).attr('disabled', false);
    		},
        error: function (jqXHR, textStatus, errorThrown) {

          $( '#modal-loader' ).modal('hide');
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
    	    
      	  $( '#modal-message' ).modal('show');
    	    $( '.modal-message' ).addClass( 'modal-danger' );
    	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
    	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
    	    
    	    //Message for developer
          console.log(jqXHR.responseText);
    	    
          $( '#btn-modal-guardar-oc' ).text('');
          $( '#btn-modal-guardar-oc' ).append( '<span class="fa fa-save"></span> Guardar' );
          $( '#btn-modal-guardar-oc' ).attr('disabled', false);
        }
    	});
    }

 })

 var _ID_Producto = '';
 var option_impuesto_producto = '';
 $( '#btn-addProductoOrden_oc' ).click(function(){

   var $ID_Producto                  = $( '#txt-ID_Producto_oc' ).val();
   var $Nu_Codigo_Barra              = $( '#txt-Nu_Codigo_Barra_oc' ).val();
   var $No_Producto                  = $( '#txt-No_Producto_oc' ).val();
   var $Ss_Precio                    = parseFloat($( '#txt-Ss_Precio_oc' ).val());
   var $ID_Impuesto_Cruce_Documento  = $( '#txt-ID_Impuesto_Cruce_Documento_oc' ).val();
   var $Nu_Tipo_Impuesto             = $( '#txt-Nu_Tipo_Impuesto_oc' ).val();
   var $Ss_Impuesto                  = $( '#txt-Ss_Impuesto_oc' ).val();
   
   bEstadoValidacion = validatePreviousDocumentToSaveOrderPurchase();

   if ( $ID_Producto.length === 0 || $No_Producto.length === 0) {
   $( '#txt-No_Producto_oc' ).closest('.form-group').find('.help-block').html('Ingresar producto');
   $( '#txt-No_Producto_oc' ).closest('.form-group').removeClass('has-success').addClass('has-error');
 } else if (bEstadoValidacion) {
   _ID_Producto = '';
   option_impuesto_producto = '';
   
   // OC_MODAL_5 - INICIO
   var obj = JSON.parse(arrImpuestosProducto);
   for (var x = 0; x < obj.arrImpuesto.length; x++){
     var selected = '';
     if ($ID_Impuesto_Cruce_Documento == obj.arrImpuesto[x].ID_Impuesto_Cruce_Documento)
       selected = 'selected="selected"';
     option_impuesto_producto += "<option value='" + obj.arrImpuesto[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + obj.arrImpuesto[x].Nu_Tipo_Impuesto + "' data-impuesto_producto='" + obj.arrImpuesto[x].Ss_Impuesto + "' " + selected + ">" + obj.arrImpuesto[x].No_Impuesto + "</option>";
   }
   
   
   $Ss_Precio = isNaN($Ss_Precio) ? 0 : $Ss_Precio;

   $Ss_Total_Producto = $Ss_Precio;
   $Ss_SubTotal_Producto = parseFloat($Ss_Precio / $Ss_Impuesto);
   

   var table_detalle_producto =
   "<tr id='tr_detalle_producto" + $ID_Producto + "'>"
     +"<td style='display:none;' class='text-left'>" + $ID_Producto + "</td>"
     +"<td class='text-right'><input type='tel' id=" + $ID_Producto + " class='txt-Qt_Producto_oc form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='1' autocomplete='off'></td>"
     +"<td class='text-left'>" + $Nu_Codigo_Barra + " " + $No_Producto + "</td>"
     +"<td class='text-right'><input type='text' class='txt-Ss_Precio_oc form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='" + $Ss_Precio + "' autocomplete='off'></td>"
     +"<td class='text-right'>"
       +"<select class='cbo-ImpuestosProducto form-control required' style='width: 100%;'>"
         +option_impuesto_producto
       +"</select>"
     +"</td>"
     +"<td style='display:none;' class='text-right'><input type='tel' class='txt-Ss_SubTotal_Producto form-control' value='" + $Ss_SubTotal_Producto.toFixed(2) + "' autocomplete='off' disabled></td>"
     +"<td class='text-right'><input type='tel' class='txt-Ss_Descuento form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='' autocomplete='off'></td>"
     +"<td class='text-right'><input type='text' class='txt-Ss_Total_Producto form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='" + $Ss_Total_Producto.toFixed(2) + "' autocomplete='off'></td>"
     +"<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>0.00</td>"
     +"<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>0.00</td>"
     +"<td class='text-center'><button type='button' id='btn-deleteProducto_oc' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-2x' aria-hidden='true'> </i></button></td>"
   + "</tr>";
   
   if( isExistTableTemporalProductoOrden($ID_Producto) ){
     $( '#txt-No_Producto_oc' ).closest('.form-group').find('.help-block').html('Ya existe producto <b>' + $No_Producto + '</b>');
     $( '#txt-No_Producto_oc' ).closest('.form-group').removeClass('has-success').addClass('has-error');
     $( '#txt-No_Producto_oc' ).focus();
     
     $( '#txt-ID_Producto_oc' ).val('');
     $( '#txt-No_Producto_oc' ).val('');
     $( '#txt-Ss_Precio_oc' ).val('');
   } else {
     $( '#txt-ID_Producto_oc' ).val('');
     $( '#txt-No_Producto_oc' ).val('');
     $( '#txt-Ss_Precio_oc' ).val('');
     
     $( '#table-DetalleProductosOrdenCompra' ).show();
     $( '#table-DetalleProductosOrdenCompra >tbody' ).append(table_detalle_producto);
     
     $( '#' + $ID_Producto ).focus();
     $( '#' + $ID_Producto ).select();
     
     var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
     var $Ss_SubTotal = 0.00;
     var $Ss_Exonerada = 0.00;
     var $Ss_Inafecto = 0.00;
     var $Ss_IGV = 0.00;
     var $Ss_Total = 0.00;
     var iCantDescuento = 0;
     var globalImpuesto = 0;
     var $Ss_Descuento_p = 0;
     $("#table-DetalleProductosOrdenCompra > tbody > tr").each(function(){
       var rows = $(this);
       var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
       var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
       var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
       var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
       var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());
 
       $Ss_Total += $Ss_Total_Producto;
 
       if (iGrupoImpuesto == 1) {
         $Ss_SubTotal += $Ss_SubTotal_Producto;
         $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
         globalImpuesto = fImpuesto;
       } else if (iGrupoImpuesto == 2) {
         $Ss_Inafecto += $Ss_SubTotal_Producto;
         globalImpuesto += 0;
       } else {
         $Ss_Exonerada += $Ss_SubTotal_Producto;
         globalImpuesto += 0;
       }
         
       if(isNaN($Ss_Descuento_Producto))
         $Ss_Descuento_Producto = 0;
         
       $Ss_Descuento_p += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
     });
     
     if ($Ss_SubTotal > 0.00 || $Ss_Inafecto > 0.00 || $Ss_Exonerada > 0.00) {
       if ($Ss_Descuento > 0.00) {
         var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0;
         if ($Ss_SubTotal > 0.00) {
           $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
           $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
           $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
           $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
         }
   
         if ($Ss_Inafecto > 0.00) {
           $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
           $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
           $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
         }
         
         if ($Ss_Exonerada > 0.00) {
           $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
           $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
           $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
         }
         
         $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada;
         $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada;
       } else
         $Ss_Descuento = $Ss_Descuento_p;
   
       if(isNaN($Ss_Descuento))
         $Ss_Descuento = 0.00;
       
       $( '#txt-subTotal_oc' ).val( $Ss_SubTotal.toFixed(2) );
       $( '#span-subTotal_oc' ).text( $Ss_SubTotal.toFixed(2) );
       
       $( '#txt-exonerada_oc' ).val( $Ss_Exonerada.toFixed(2) );
       $( '#span-exonerada_oc' ).text( $Ss_Exonerada.toFixed(2) );
       
       $( '#txt-inafecto_oc' ).val( $Ss_Inafecto.toFixed(2) );
       $( '#span-inafecto_oc' ).text( $Ss_Inafecto.toFixed(2) );
         
       $( '#txt-impuesto_oc' ).val( $Ss_IGV.toFixed(2) );
       $( '#span-impuesto_oc' ).text( $Ss_IGV.toFixed(2) );
       
       $( '#txt-descuento_oc' ).val( $Ss_Descuento.toFixed(2) );
       $( '#span-descuento_oc' ).text( $Ss_Descuento.toFixed(2) );
   
       $( '#txt-total_oc' ).val( $Ss_Total.toFixed(2) );
       $( '#span-total_oc' ).text( $Ss_Total.toFixed(2) );
     }
     
   
   }
 }
        

 }) 

 function validatePreviousDocumentToSaveOrderPurchase(){
   bEstadoValidacion = true;

     if ( $( '#cbo-serie_doc_oc' ).val() == 0){
       $( '#cbo-serie_doc_oc' ).closest('.form-group').find('.help-block').html('Ingresar serie');
       $( '#cbo-serie_doc_oc' ).closest('.form-group').removeClass('has-success').addClass('has-error');
       bEstadoValidacion = false;
       scrollToError( $("html, body"), $( '#cbo-serie_doc_oc' ) );

     } else if ( $( '#txt-ID_Num_Doc_oc' ).val().length === 0){
       $( '#txt-ID_Num_Doc_oc' ).closest('.form-group').find('.help-block').html('Ingresar número');
       $( '#txt-ID_Num_Doc_oc' ).closest('.form-group').removeClass('has-success').addClass('has-error');
       bEstadoValidacion = false;
       scrollToError( $("html, body"), $( '#txt-ID_Num_Doc_oc' ) );

     }  else if ( $( '#txt-ACodigo_oc' ).val().length === 0) {
       $( '#txt-ANombre_oc' ).closest('.form-group').find('.help-block').html('Seleccionar proveedor');
       $( '#txt-ANombre_oc' ).closest('.form-group').removeClass('has-success').addClass('has-error');
       bEstadoValidacion = false;
       scrollToError( $("html, body"), $( '#txt-ANombre_oc' ) );

     } 
     
     return bEstadoValidacion;
}

   function isExistTableTemporalProductoOrden($ID_Producto){
     return Array.from($('tr[id*=tr_detalle_producto]'))
       .some(element => ($('td:nth(0)',$(element)).html()===$ID_Producto));
   }

  // PREP_OC-2 - I 

 // Lista almacen
 url = base_url + 'HelperController/getAlmacenes';
 $.post( url, {}, function( responseAlmacen ){
 var iCantidadRegistros = responseAlmacen.length;
 var selected = '';
 var iIdAlmacen = 0;
 if (iCantidadRegistros == 1) {
  $( '#cbo-almacen_oc' ).html( '<option value="' + responseAlmacen[0]['ID_Almacen'] + '" ' +  '>' + responseAlmacen[0]['No_Almacen'] + '</option>' );
   var arrParamsListaPrecio = {
     ID_Almacen : responseAlmacen[0]['ID_Almacen'],
   }
 } else {
   $( '#cbo-almacen_oc' ).html( '<option value="0">- Seleccionar -</option>');
   for (var i = 0; i < iCantidadRegistros; i++) {
     
     $('#cbo-almacen_oc' ).append( '<option value="' + responseAlmacen[i]['ID_Almacen'] + '" ' +  '>' + responseAlmacen[i]['No_Almacen'] + '</option>' );
   }
 }
}, 'JSON');

// PREP_OC-2 - F 
    
url = base_url + 'HelperController/getListaPrecio';
  //$.post( url, {Nu_Tipo_Lista_Precio : $( '[name="Nu_Tipo_Lista_Precio"]' ).val(), ID_Organizacion: $( '#header-a-id_organizacion' ).val(), ID_Almacen : 0}, function( responseLista ){
  $.post( url, {Nu_Tipo_Lista_Precio : 2, ID_Organizacion: 26, ID_Almacen : 0}, function( responseLista ){
    var iCantidadRegistrosListaPrecios = responseLista.length;
    if ( iCantidadRegistrosListaPrecios == 1 ) {
      $( '#cbo-listprecios_oc' ).html( '<option value="' + responseLista[0].ID_Lista_Precio_Cabecera + '">' + responseLista[0].No_Lista_Precio + '</option>' );
    } else if ( iCantidadRegistrosListaPrecios > 1 ) {
      $( '#cbo-listprecios_oc' ).html( '<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < iCantidadRegistrosListaPrecios; i++)
        $( '#cbo-listprecios_oc' ).append( '<option value="' + responseLista[i].ID_Lista_Precio_Cabecera + '">' + responseLista[i].No_Lista_Precio + '</option>' );
    } else {
      $( '#cbo-listprecios_oc' ).html( '<option value="0">- Sin lista precio -</option>');
    }
  }, 'JSON');
   

   $( '#cbo-serie_doc_oc' ).html('');
     url = base_url + 'HelperController/getSeriesDocumento';
     $.post( url, {
                         ID_Organizacion :  26,                          
                         ID_Tipo_Documento: 78
                     }, function( response ){
         if (response.length == 0)
         $( '#cbo-serie_doc_oc' ).html('<option value="0" selected="selected">Sin serie</option>');
         else {
         $( '#cbo-serie_doc_oc' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
         for (var i = 0; i < response.length; i++)
             $( '#cbo-serie_doc_oc' ).append( '<option value="' + response[i].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[i].ID_Serie_Documento_PK + '>' + response[i].ID_Serie_Documento + '</option>' );
         }
     }, 'JSON');

      // bloqueo fecha emision 
      $( "#txt-Fe_Emision_oc" ).prop( "disabled", true );

         $( '#cbo-serie_doc_oc' ).change(function(){
         $( '#txt-ID_Num_Doc_oc' ).val('');
         if ( $(this).val() != '') {
         url = base_url + 'HelperController/getNumeroDocumento';
             $.post( url, { ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Tipo_Documento: 78 , ID_Serie_Documento: $(this).val() }, function( response ){
             if (response.length == 0)
                 $( '#txt-ID_Num_Doc_oc' ).val('');
             else
                 $( '#txt-ID_Num_Doc_oc' ).val(response.ID_Numero_Documento);
             }, 'JSON');
         }
         })

});
 </script>

<!-- Modal --> 
<div class="modal fade" id="GenerarOCModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document" style="width: 850px !important;">
 <div class="modal-content">
   <div class="modal-body">
 <div class="row">
 <div class="col-sm-12 col-md-12">
   <div class="panel panel-default">
     <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento de Orden Compra  <?php echo $_POST['data']?></b></div>
     <div class="panel-body">
       <div class="row">
       <!-- // CONTROLES -->
        
        <!--  // id presupuesto - id-cabecera -->
        <div class="col-xs-9 col-sm-6 col-md-4"  style="display:none" >
       <div class="form-group">
       <input type="text" id="id-cabecera"  name="id-cabecera" class="form-control input-number" disabled> 
       </div>
       </div>
       </br> </br>

        <!--  // combo serie - cbo-serie_doc_oc -->
         <div class="col-xs-6 col-sm-6 col-md-4">
         <div class="form-group">            
         <label>Serie OC<span class="label-advertencia">*</span></label>
         <select id="cbo-serie_doc_oc" class="form-control required" style="width: 100%;"></select>
         <span class="help-block" id="error"></span>
         </div>
         </div>

       <!--   // numero documento - txt-ID_Num_Doc_oc -->
       <div class="col-xs-9 col-sm-6 col-md-4">
       <div class="form-group">
       <label>Número OC<span class="label-advertencia">*</span></label>
       <input type="tel" id="txt-ID_Num_Doc_oc" name="ID_Num_Doc_oc" class="form-control required input-number" disabled>
       <span class="help-block" id="error"></span>
       </div>
       </div>
       
      <!--  // fecha emision - txt-Fe_Emi  -->
       <div class="col-xs-12 col-sm-6 col-md-4">
       <div class="form-group">
       <label>F. Emisión OC<span class="label-advertencia">*</span></label>
       <div class="input-group date">
      <input type="text" id="txt-Fe_Emision_oc" name="Fe_Emision_oc" class="form-control date-picker-invoice required" >
       </div>
       <span class="help-block" id="error"></span>
       </div>
       </div>         

       <!--  OC_MODAL_7 - INICIO   -->
       <div class="col-xs-12 col-sm-2 col-md-2 text-center">
            <label>Servicio <input type="radio" name="ServicioBien" id="radio-servicio" class="flat-red" value="1"></label>
        </div>

        <div class="col-xs-12 col-sm-2 col-md-2 text-center">
            <label>Bien <input type="radio" name="ServicioBien" id="radio-bien" class="flat-red" value="0"></label>
        </div>
      <!--  OC_MODAL_7 - FIN  -->

      <!--  // almacen - cbo-almacen_oc  -->
       </br>
       </br>
       </br>
       </br>
         <div class="col-xs-12 col-sm-6 col-md-4 div-almacen" style="display:none">
           <div class="form-group">
             <select id="cbo-almacen_oc" class="form-control " style="width: 100%;"></select>
           </div>
         </div>
       <!-- / placa - txt-ID_Placa_oc txt-No_Placa_Vehiculo_exist_oc -->
           <div class="col-xs-12 col-sm-2 col-md-2" style="display:none">
                     <input type="hidden" id="txt-ID_Placa_oc"  class="form-control">
                     <div class="form-group">
                       <input type="text" id="txt-No_Placa_Vehiculo_exist_oc" name="" class="form-control autocompletar_placa_x_cliente" autocomplete="off" placeholder="placa" maxlength="6">
                      </div>
             </div>
       <!-- // presupuesto txt-ID_Presupesto_Exist_oc -->
         <div class="col-xs-12 col-sm-2 col-md-2" style="display:none">
                     <div class="form-group">
                     <input type="text" id="txt-ID_Presupesto_Exist_oc" name="ID_Presupesto_Exist_oc" placeholder="presupuesto"  class="form-control autocompletar_presupuesto" autocomplete="off">
                     </div>
         </div>
       <!-- // oi txt-ID_OI_Existe txt-ID_OI_Exist_oc -->
         <div class="col-xs-12 col-sm-2 col-md-2" style="display:none">
                     <div class="form-group">
                     <input type="text" id="txt-ID_OI_Exist_oc" class="form-control" placeholder="OI" value="" autocomplete="off">
                     </div>
             </div>
       <!-- // fecha recepcion txt-Fe_Recep_oc -->
         <div class="col-xs-12 col-sm-4 col-md-2" style="display:none">
                     <div class="form-group">
                       <div class="input-group date">
                         <input type="text" id="txt-Fe_Recep_oc" name="Fe_Recep_oc" class="form-control date-picker-invoice required"  >                    
                       </div>
                       <span class="help-block" id="error"></span>
                     </div>
         </div>
       <!-- // estado - generada - 1  -->
         <div class="col-xs-12 col-sm-4 col-md-2" style="display:none">
         <div class="form-group">             
             <input type="text" id="txt-estado_oc" name="txt-estado_oc" placeholder="estado" value=1 class="form-control"  >             
         </div>
         </div>
       <!-- / estado conta - pendiente - 1  -->
       <div class="col-xs-12 col-sm-4 col-md-2" style="display:none">
       <div class="form-group">             
           <input type="text" id="txt-estado_conta_oc" name="txt-estado_conta_oc" placeholder="estado conta" value=1 class="form-control"  >             
       </div>
       </div>
       </div>  
     </div>
     

   </div>
 </div>
</div>
<!-- // proveedores  -->
<div class="row">
<div class="col-sm-12 col-md-12">
 <div class="panel panel-default">
   <div class="panel-heading"><i class="fa fa-user"></i> <b>Proveedor OC</b></div>
   <div class="panel-body"> 
     
     <div class="col-xs-12 col-md-12 div-cliente_existente">
       <br>
       <div class="form-group">
         <label>Nombre Proveedor <span class="label-advertencia">*</span></label>
         <input type="hidden" id="txt-AID_oc" name="AID_oc" class="form-control required">
         <input type="text" id="txt-ANombre_oc" name="ANombre_oc" class="form-control autocompletar_ProveeEnPresup" data-global-class_method="AutocompleteController/getAllProvider" data-global-table="entidad" placeholder="Ingresar proveedor">
         <span class="help-block" id="error"></span>
       </div>
     </div>
           
     <div class="col-xs-4 col-sm-4 col-md-4 div-cliente_existente">
       <div class="form-group">
         <label>Número Documento Identidad <span class="label-advertencia">*</span></label>
         <input type="text" id="txt-ACodigo_oc" name="ACodigo_oc" class="form-control required" disabled>
         <span class="help-block" id="error"></span>
       </div>
     </div>
           
     <div class="col-xs-6 col-sm-6 col-md-6 div-cliente_existente">
       <div class="form-group">
         <label>Dirección</label>
         <input type="text" id="txt-DireccEntid_oc" name="Txt_DireccEntid_oc" class="form-control" disabled>
         <span class="help-block" id="error"></span>
       </div>
     </div>
     </div>
     </div>


     <div class="row">
     <div class="col-sm-12 col-md-12">
     <div id="panel-DetalleProductosOrdenVenta" class="panel panel-default">
       <div class="panel-heading"><i class="fa fa-shopping-cart"></i> <b>Detalle</b></div>
       <div class="panel-body">
         <div class="row">
           <input type="hidden" name="NuTipoListaPrecio_oc" value="1" class="form-control">
           <div class="col-xs-12">
             <label>Lista de Precio <span class="label-advertencia">*</span></label>
             <div class="form-group">
               <select id="cbo-listprecios_oc" class="form-control required" style="width: 100%;"></select>
               <span class="help-block" id="error"></span>
             </div>
           </div>

           <div class="col-xs-12 col-md-9">
             <label>Producto / Servicio <span class="label-advertencia">*</span></label>
             <div class="form-group">
           
                   <input type="hidden" id="txt-Activar_Almacen_oc" class="form-control" value="1">
                   <input type="hidden" id="txt-Nu_Tipo_Registro_oc" class="form-control" value="0"><!-- Compra -->
                   <input type="hidden" id="txt-Nu_Compuesto_oc" class="form-control" value="0">
                   <input type="hidden" id="txt-ID_Producto_oc" class="form-control">
                   <input type="hidden" id="txt-Nu_Codigo_Barra_oc" class="form-control">
                   <input type="hidden" id="txt-Ss_Precio_oc" class="form-control">
                   <input type="hidden" id="txt-ID_Impuesto_Cruce_Documento_oc" class="form-control">
                   <input type="hidden" id="txt-Nu_Tipo_Impuesto_oc" class="form-control">
                   <input type="hidden" id="txt-Ss_Impuesto_oc" class="form-control">
                   <input type="hidden" id="txt-Qt_Producto_oc" class="form-control">
                   <input type="hidden" id="txt-nu_tipo_item_ov" class="form-control">
                   <input type="hidden" id="txt-No_Unidad_Medida_oc" class="form-control">
                   <!-- OC_MODAL_7 - INICIO -->
                   <input type="hidden" id="Txt_Usuario_oc"  value=<?php echo $this->session->userdata['usuario']->No_Usuario; ?>  class="form-control">
                   <input type="hidden" id="Txt_Creado_Por"  value= "Y"  class="form-control">     
                  <!-- OC_MODAL_7 - INICIO -->
                <input type="text" id="txt-No_Producto_oc" class="form-control autocompletar_detalle_de_producto_oc" data-global-class_method="AutocompleteController/getAllProduct" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off" >
              <span class="help-block" id="error"></span>
             </div>
           </div>
           
           <div class="col-xs-12 col-md-3">  
             <div class="form-group">
               <label>&nbsp;</label>
               <button type="button" id="btn-addProductoOrden_oc"      class="btn btn-success btn-md btn-block"><i class="fa fa-plus-circle"></i> Agregar Item Detalle</button>          
             </div>
           </div>
         </div>
         
         <div class="row">
           <div class="col-md-12">
             <div class="table-responsive">
               <table id="table-DetalleProductosOrdenCompra" class="table table-striped table-bordered">
                 <thead>
                   <tr>
                       <th style="display:none;" class="text-left"></th>
                       <th class="text-center" style="width: 10%;">Cantidad</th>
                       <th class="text-center" style="width: 35%;">Item</th>
                       <th class="text-center" style="width: 10%;">Costo</th>
                       <th class="text-center" style="width: 15%;">Impuesto Tributario</th>
                       <th class="text-center" style="display:none;">Sub Total</th>
                       <th class="text-center" style="width: 10%;">% Dscto</th>
                       <th class="text-center">Total</th>
                       <th class="text-center"></th>
                  
                 </tr>
               </thead>
                   <tbody>
                   </tbody>
               
               </table>
             </div>
           </div>
         </div>
       </div>
     </div>
</div>
   </div><!-- ./Detalle -->
           <div class="row"><!-- Totales -->
             <div class="col-md-8"></div>
             <div class="col-md-4">
             <div class="panel panel-default">
               <div class="panel-heading"><i class="fa fa-money"></i> <b>Totales</b></div>
               <div class="panel-body">
                 <table class="table" id="table-OrdenVentaTotal">
                   <tr style="display:none">
                     <td><label>% Descuento</label></td>
                     <td class="text-right"  >
                       <input type="tel" class="form-control input-decimal" id="txt-SsDescuento_oc" name="SsDescuento_oc" size="3" value="" autocomplete="off" />
                     </td>
                   </tr>
                   
                   <tr>
                     <td><label>OP. Gravadas</label></td>
                     <td class="text-right">
                       <input type="hidden" class="form-control" id="txt-subTotal_oc" value="0.00"/>
                       <span class="span-signo"></span> <span id="span-subTotal_oc">0.00</span>
                     </td>
                   </tr>
                   
                   <tr>
                     <td><label>OP. Inafectas</label></td>
                     <td class="text-right">
                       <input type="hidden" class="form-control" id="txt-inafecto_oc" value="0.00"/>
                       <span class="span-signo"></span> <span id="span-inafecto_oc">0.00</span>
                     </td>
                   </tr>
                   
                   <tr>
                     <td><label>OP. Exoneradas</label></td>
                     <td class="text-right">
                       <input type="hidden" class="form-control" id="txt-exonerada_oc" value="0.00"/>
                       <span class="span-signo"></span> <span id="span-exonerada_oc">0.00</span>
                     </td>
                   </tr>
                   
                   <tr>
                     <td><label>Gratuitas</label></td>
                     <td class="text-right">
                       <input type="hidden" class="form-control" id="txt-gratuita_oc" value="0.00"/>
                       <span class="span-signo"></span> <span id="span-gratuita_oc">0.00</span>
                     </td>
                   </tr>
                   
                   <tr>
                     <td><label>Descuento Total (-)</label></td>
                     <td class="text-right">
                       <input type="hidden" class="form-control" id="txt-descuento_oc" value="0.00"/>
                       <span class="span-signo"></span> <span id="span-descuento_oc">0.00</span>
                     </td>
                   </tr>
                   
                   <tr>
                     <td><label>I.G.V. %</label></td>
                     <td class="text-right">
                         <input type="hidden" class="form-control" id="txt-impuesto_oc" value="0.00"/>
                         <span class="span-signo"></span> <span id="span-impuesto_oc">0.00</span>
                     </td>
                   </tr>
                     
                   <tr>
                     <td><label>Total</label></td>
                     <td class="text-right">
                       <input type="hidden" class="form-control" id="txt-total_oc" value="0.00"/>
                       <span class="span-signo"></span> <span id="span-total_oc">0.00</span>
                     </td>
                   </tr>
                 </table><!-- ./Totales -->

               </div>
             </div>
           </div>

           <!-- PREP_OC-2 - I  -->
           <div class="modal-footer">
               <div class="col-xs-6">
                 <button type="button" id="btn-modal-salir-oc" class="btn btn-danger btn-block pull-center" data-dismiss="modal">Cancelar</button>
               </div>
               <div class="col-xs-6">
                 <button type="button" id="btn-modal-guardar-oc" class="btn btn-primary btn-block pull-center">Guardar OC</button>
               </div>
           </div>
           <!-- PREP_OC-2 - F  -->

</div>
</div>
