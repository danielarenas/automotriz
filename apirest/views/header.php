<?php //defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>laesystems | admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/bootstrap/dist/css/bootstrap.min.css'; ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/font-awesome/css/font-awesome.min.css'; ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/Ionicons/css/ionicons.min.css'; ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'; ?>">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/select2/dist/css/select2.min.css'; ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/morris.js/morris.css'; ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/jvectormap/jquery-jvectormap.css'; ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/bootstrap-daterangepicker/daterangepicker.css'; ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'; ?>">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url() . 'plugins/iCheck/all.css'; ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url() . 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'dist/css/AdminLTE.min.css'; ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'dist/css/skins/_all-skins.min.css'; ?>">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- laesystems -->
  <meta name="Author" content="laesystems">
  <meta name="Subject" content="Desarrollamos soluciones innovadoras">
  <meta name="Copyright" content="Copyright © laesystems. Todos los derechos reservados.">

  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dropzone/css/dropzone.min.css'; ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/select2/css/select2.min.css'; ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/tagify/css/tagify.css'; ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/style.css?ver=1.10'; ?>">

  <!-- Favicon and touch icons -->
  <link rel="shortcut icon" href="<?php echo base_url() . 'assets/ico/favicon.ico'; ?>">
  <link rel="apple-touch-icon-precomposed" sizes="192x192" href="<?php echo base_url() . 'assets/ico/android-chrome-192x192.png'; ?>">
  <link rel="apple-touch-icon-precomposed" sizes="32x32" href="<?php echo base_url() . 'assets/ico/favicon-32x32.png'; ?>">
  <link rel="apple-touch-icon-precomposed" sizes="16x16" href="<?php echo base_url() . 'assets/ico/favicon-16x16.png'; ?>">
  <link rel="apple-touch-icon-precomposed" sizes="16x16" href="<?php echo base_url() . 'assets/ico/apple-touch-icon.png'; ?>">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/datatables/buttons.bootstrap.min.css'; ?>">
</head>
<body class="hold-transition sidebar-mini skin-purple">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url() . 'InicioController'; ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Menú</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Aumotriz</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown messages-menu" id="header-a-estado_sistema" value="<?php echo $this->empresa->ID_Empresa; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:<?php echo $this->empresa->Nu_Estado_Sistema == 1 ? '#51ff20' : '#c10000'; ?>"><?php echo $this->empresa->Nu_Estado_Sistema == 1 ? 'Producción' : 'Demostración'; ?></a>
          </li>
          <!-- Empresa y organizacion -->
          <li class="dropdown messages-menu" id="header-a-id_empresa" value="<?php echo $this->empresa->ID_Empresa; ?>">
            <a href="#" class="dropdown-toggle hidden-xs" data-toggle="dropdown">
              <?php echo $this->empresa->No_Empresa; ?>
            </a>
          </li>
          <li class="dropdown messages-menu" id="header-a-id_organizacion" value="<?php echo $this->empresa->ID_Organizacion; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php echo $this->empresa->No_Organizacion; ?></span>
            </a>          
          </li>
          <?php
          if ( isset($this->session->userdata['arrDataPersonal']) && $this->session->userdata['arrDataPersonal']['sStatus']=='success') {
          ?>
          <li class="dropdown messages-menu" id="header-a-id_matricula_empleado" value="<?php echo $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php echo $this->session->userdata['arrDataPersonal']['arrData'][0]->No_Entidad ?></span>
            </a>
          </li>
          <?php
          }
          ?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url() . 'assets/img/usuarios/foto_usuario.png'; ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata['usuario']->No_Nombres_Apellidos; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url() . 'assets/img/usuarios/foto_usuario.png'; ?>" class="img-circle" alt="User Image">
                <p>
                  <?php echo $this->session->userdata['usuario']->No_Nombres_Apellidos; ?>
                  <small>
                    <?php $date = date_create($this->session->userdata['usuario']->Fe_Creacion);
                    echo '<b>Miembro desde:</b> ' . date_format($date,"d/m/Y H:i:s"); ?>
                  </small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-12 text-center hidden-xs">
                    <label>Empresa</label>
                    <div class="form-group">
                    <a href="#"><?php echo $this->empresa->No_Empresa . ' (' . $this->empresa->No_Descripcion . ')'; ?></a>
                    </div>
                  </div>
                  
                  <div class="col-xs-6 text-left">
                    <label>Organización</label>
                    <div class="form-group">
                      <a href="#"><?php echo $this->empresa->No_Organizacion; ?></a>
                    </div>
                  </div>
                  
                  <div class="col-xs-6 text-left">
                    <label>Perfil</label>
                    <div class="form-group">
                      <a href="#"><?php echo $this->session->userdata['usuario']->No_Grupo; ?></a>
                    </div>
                  </div>
                </div><!-- /.row -->
                <?php
                if ( isset($this->session->userdata['arrDataPersonal']) && $this->session->userdata['arrDataPersonal']['sStatus']=='success' ) {
                  $arrDataPersonal = $this->session->userdata['arrDataPersonal']['arrData'][0];
                ?>
                <div class="row">
                  <div class="col-xs-12 text-center">
                    <label>Cajero(a)</label>
                    <div class="form-group">
                      <input type="hidden" id="hidden-id_matricula_personal" value="<?php echo $arrDataPersonal->ID_Matricula_Empleado; ?>">
                      <label style="font-weight: normal"><?php echo '(' . $arrDataPersonal->Nu_Caja . ') ' . $arrDataPersonal->No_Entidad; ?></label>
                    </div>
                  </div>

                  <div class="col-xs-12 text-center">
                    <label>F. Apertura Caja</label>
                    <div class="form-group">
                      <label style="font-weight: normal"><?php echo allTypeDate($arrDataPersonal->Fe_Matricula, '-', 0); ?></label>
                    </div>
                  </div>
                </div><!-- /.row -->
                <?php } ?>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="col-xs-12 text-center">
                  <a href="<?php echo base_url() . 'LoginController/logout' ?>" class="btn btn-danger btn-flat btn-mobile_cerrar_sesion"><span class="fa fa-sign-out"></span> Cerrar sesión</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() . 'assets/img/usuarios/foto_usuario.png'; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata['usuario']->No_Nombres_Apellidos; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVEGACIÓN MENÚ</li>
	      <?php
	        foreach($this->menu as $arrMenuPadre):
            $menu_padre = explode('/', $this->router->directory);
            $menu_padre = $menu_padre[0];
            $No_Class_Li_Padre = "";
            if ($menu_padre != $arrMenuPadre->No_Class_Controller && $arrMenuPadre->Nu_Cantidad_Menu_Padre > 0)
              $No_Class_Li_Padre = "treeview";
            else if ($menu_padre == $arrMenuPadre->No_Class_Controller && $arrMenuPadre->Nu_Cantidad_Menu_Padre > 0)
              $No_Class_Li_Padre = "active treeview";
            else if ($this->router->class == $arrMenuPadre->No_Class_Controller && $arrMenuPadre->Nu_Cantidad_Menu_Padre == 0)
              $No_Class_Li_Padre = "active";
	      ?>
	      <li class="<?php echo $No_Class_Li_Padre; ?>">
      	  <?php if ($arrMenuPadre->ID_Padre == 0){ ?>
      	    <a title="<?php echo $arrMenuPadre->No_Menu; ?>" href="<?php echo base_url() . $arrMenuPadre->No_Menu_Url; ?>">
              <i class="<?php echo $arrMenuPadre->Txt_Css_Icons; ?>"></i>
		      		<span><?php echo $arrMenuPadre->No_Menu; ?></span>
		      		<?php if($arrMenuPadre->Nu_Cantidad_Menu_Padre > 0): ?>
				      <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
				      <?php endif; ?>
            </a>
		      	<?php if($arrMenuPadre->Nu_Cantidad_Menu_Padre > 0): ?>
            <ul class="treeview-menu">
				      <?php
				        foreach($arrMenuPadre->Hijos as $arrHijos):
				          $No_Class_Li = "";
				          if ($this->router->directory != $arrHijos->No_Class_Controller && $arrHijos->Nu_Cantidad_Menu_Hijos > 0)
				            $No_Class_Li = "treeview";
				          else if ($this->router->directory == $arrHijos->No_Class_Controller && $arrHijos->Nu_Cantidad_Menu_Hijos > 0)
				            $No_Class_Li = "treeview active";
				          else if ($this->router->class == $arrHijos->No_Class_Controller && $arrHijos->Nu_Cantidad_Menu_Hijos == 0)
				            $No_Class_Li = "active";
				      ?>
				          <li class="<?php echo $No_Class_Li; ?>">
                    <a title="<?php echo $arrHijos->No_Menu; ?>" href="<?php echo base_url() . $arrHijos->No_Menu_Url; ?>">
                      <i class="<?php echo $arrHijos->Txt_Css_Icons; ?>"></i> <?php echo $arrHijos->No_Menu; ?>
        		      		<?php if($arrHijos->Nu_Cantidad_Menu_Hijos > 0): ?>
        				      <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        				      <?php endif; ?>
                    </a>
                    <?php if($arrHijos->Nu_Cantidad_Menu_Hijos > 0): ?>
                    <ul class="treeview-menu">
				              <?php foreach($arrHijos->SubHijos as $arrSubHijos): ?>
                      <li class="<?php echo ($this->router->class == $arrSubHijos->No_Class_Controller ? 'active' : ''); ?>">
                        <a title="<?php echo $arrSubHijos->No_Menu; ?>" href="<?php echo base_url() . $arrSubHijos->No_Menu_Url; ?>">
                          <i class="<?php echo $arrSubHijos->Txt_Css_Icons; ?>"></i> <?php echo $arrSubHijos->No_Menu; ?>
                        </a>
                      </li>
                      <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                  </li>
              <?php
                endforeach; ?>
            </ul>
            <?php endif; ?>
			    <?php } ?>
			    </li>
	      <?php endforeach; ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>