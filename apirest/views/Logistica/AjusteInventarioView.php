<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
      <!-- ./New box-header -->
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                  <label>&nbsp;</label>
                <button type="button" class="btn btn-success btn-block" onclick="agregarAjusteInventario()"><i class="fa fa-plus-circle"></i> Agregar</button>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-AjusteInventario" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Almacén</th>
                  <th class="text-center">F. Ajuste</th>
                  <th>Cant. Item(s)</th>
                  <th class="no-sort"></th>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-AjusteInventario');
            echo form_open('', $attributes);
            ?>
              <div class="box-header box-header-new">
                <div class="row">
                  <br>
                  <div class="col-md-12">
                    <div class="callout callout-warning">
                      <p>Solo se procesarán los registros que en la columna <b>Stock Físico</b> tengan el número <b>0</b> o <b>mayor a cero</b></p>
                    </div>
                  </div>
                </div>

                <div class="row">                  
                  <div class="col-md-12">
                    <label>Item</label>
                    <div class="form-group">
                      <input type="text" id="txt-Global_Filter_Producto" class="form-control" placeholder="Buscar por Nombre / UPC / SKU" value="" autocomplete="off">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <table id="table-AjusteInventarioAgregar" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center">UPC</th>
                      <th class="text-center">Nombre</th>
                      <th class="text-center">Stock Actual</th>
                      <th class="text-center">Stock Físico</th>
                      <th class="text-center">Diferencia</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
          	  <div class="row">
          	    <br>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-procesar_ajuste" class="btn btn-success btn-block" onclick="guardarAjusteInventario()"><i class="fa fa-save"></i> Procesar Ajuste</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-Ver">
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <h4 id="h4-title-ver_ajuste_inventario" class="text-center"></h4>
              </div>
              <table id="table-AjusteInventarioVer" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center">UPC</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Diferencia</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="row">
              <br>
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <button type="button" id="btn-cancelar_ver_ajuste_inventario" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->