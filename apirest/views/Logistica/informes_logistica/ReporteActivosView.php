<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
   
  <!-- Main content -->
  <section class="content"> 
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new">
            <div class="row div-Filtros">
              <br>                      
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2 hidden">
                <div class="form-group">
                  <label>Tipo</label>
    		  				<select id="cbo-filtros_tipos_documento" class="form-control" style="width: 100%;"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Serie</label>
                  <input type="text" id="txt-Filtro_SerieDocumento" class="form-control input-Mayuscula input-codigo_barra" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <input type="tel" id="txt-Filtro_NumeroDocumento" class="form-control input-number" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-12 col-md-2">
                <div class="form-group">
                  <label>Estado</label>
    		  				<select id="cbo-estado_documento" class="form-control" style="width: 100%;">
    		  				  <option value="0" selected="selected">Todos</option>
    		  				  <option value="6">Completado</option>
    		  				  <option value="7">Anulado</option>
    		  				</select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>OI Detalle</label>
                  <input type="tel" id="txt-Filtro_OI" class="form-control input-number" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="form-group">
                  <!-- M044 - I -->
                  <label>Cliente</label>
                  <!-- <label>Proveedor</label> -->
                  <!-- M044 - F -->
                  <input type="hidden" id="txt-AID" class="form-control">
                  <!-- M044 - I -->
                  <!-- <input type="text" id="txt-Filtro_Entidad" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllProvider" data-global-table="entidad" placeholder="Ingresar nombre / # documento identidad" value="" autocomplete="off"> -->
                  <input type="text" id="txt-Filtro_Entidad" class="form-control autocompletar" data-global-class_method="AutocompleteController/getTxtClientes" data-global-table="entidad" placeholder="# documento identidad / Ingresar nombre " value="" autocomplete="off">
                  <!-- M044 - F -->
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                  <label>Producto</label>
                  <input type="hidden" id="txt-ID_Producto" class="form-control">
                  <input type="text" id="txt-No_Producto" class="form-control autocompletar_detalle" data-global-class_method="AutocompleteController/globalAutocomplete" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-12">
                <label></label>
                <div class="form-group">
                  <label><input type="radio" name="radio-tipo-reporte-compras_x_proveedor" class="flat-red" value="0" checked> Detallado</label>
                  &nbsp;&nbsp;<label><input type="radio" name="radio-tipo-reporte-compras_x_proveedor" class="flat-red" value="1"> Resumido</label>
                </div>                          
              </div>
            </div>
              
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <button type="button" id="btn-html_compras_x_proveedor" class="btn btn-default btn-block btn-generar_compras_x_proveedor" data-type="html"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <button type="button" id="btn-pdf_compras_x_proveedor" class="btn btn-default btn-block btn-generar_compras_x_proveedor" data-type="pdf"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-4">
                <div class="form-group">
                  <button type="button" id="btn-excel_compras_x_proveedor" class="btn btn-default btn-block btn-generar_compras_x_proveedor" data-type="excel"><i class="fa fa-file-excel-o color_icon_excel"></i> Excel</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div id="div-compras_x_proveedor" class="table-responsive">
            <table id="table-compras_x_proveedor" class="table table-striped table-bordered">
              <?php
              $iColspanProducto=8;
              if ( $this->empresa->Nu_Tipo_Rubro_Empresa == 1) {
                $iColspanProducto=10;
              }
              ?>
              <thead>
                <tr>
                  <th class="text-center" style="width: 10%">Fecha</th>
                  <th class="text-center" style="width: 8%" colspan="3">Documento</th>
                  <th class="text-center" >Moneda</th>
                  <th class="text-center" colspan="6">Producto</th>                  
                  <th class="text-center" rowspan="2">Estado</th>
                  <th class="text-center" colspan="7">Activo</th>
                </tr>
                <tr>
                  <th class="text-center" style="width: 10%">Emisión</th>
                  <th class="text-center">Tipo</th>
                  <th class="text-center">Serie</th>
                  <th class="text-center">Número</th>
                  <th class="text-center">Signo</th>
                  <th class="text-center">Descripción</th>
                  <!-- M023 - FIX - I --> 
                  <th class="text-center">SubCategoria</th>
                  <th class="text-center">Marca</th>                  
                  <th class="text-center">Modelo o Tipo</th>         
                  <th class="text-center">Contenido</th>
                  <!-- M023 - FIX - F -->
                  <th class="text-center">Cantidad</th>
                  <th class="text-center">Precio</th>
                  <th class="text-center">ID</th>
                  <th class="text-center">F. Salida</th>
                  
                  <!-- M023 - I -->
                  <th class="text-center">Receptor</th>
                  <th class="text-center">Solicitante Loc.</th>                  
                  <th class="text-center">Tipo Activo</th>
                  <!-- M023 - F -->

                  <th class="text-center">Area Ingreso</th>
                  <th class="text-center">Estado</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->