<!DOCTYPE html>
<html>
	<head>
    <meta http-equiv=Content-Type content=text/html; charset=UTF-8/>
		<title>Vista Preliminar</title>
      <style type=text/css>
        .table_pdf {
          width: 100%;
        }
        
        .tr-theadFormatTitle th{
          font-weight: bold;
          font-size: 9px;
        }
        
        .tr-theadFormat th{
          font-weight: bold;
        }
        
        .tr-theadFormat_header th{
          background-color: #F2F5F5;
        }
        
        .tr-theadFormat_footer th{
          background-color: #E7E7E7;
        }
        
        .tr-thead th{
          font-size: 5px;
          border: solid 0.5px #000000;
        }
        
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
      </style>
    </head>
    <body>
      <br/>
      <table class="table_pdf">
        <thead>
          <tr class="tr-theadFormat">
            <td align="left"><?php echo $this->empresa->No_Empresa; ?></td>
            <td align="right"><?php echo $arrCabecera['No_Almacen']; ?></td>
          </tr>
          <tr class="tr-theadFormatTitle">
            <th align="center" colspan="2">Stock Valorizado</th>
          </tr>
          <tr class="tr-theadFormat">
            <th align="center" colspan="2">&nbsp;</th>
          </tr>
          <tr class="tr-theadFormat">
            <td align="center" colspan="2">Desde: <?php echo $arrCabecera['Fe_Inicio'] . ' Hasta: ' . $arrCabecera['Fe_Fin']; ?></td>
          </tr>
        </thead>
      </table>
      <br/>
      <br/>
      <br/>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-thead tr-theadFormat">
            <th class="text-center">Código Barra</th>
            <th class="text-center">Nombre</th>
            <th class="text-center">Unidad Medida</th>
            <th class="text-center">Stock</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if ( count($arrDetalle) > 0) {
            $ID_Linea = '';
            $counter = 0;
            $sum_linea_cantidad = 0.000000;
            $sum_cantidad = 0.000000;
            foreach($arrDetalle as $row) {
              if ($ID_Linea != $row->ID_Linea) {
                if ($counter != 0) { ?>
                  <tr class="tr-theadFormat tr-theadFormat_footer">
                    <th class="text-right" colspan="3">Total Linea</th>
                    <th class="text-right"><?php echo numberFormat($sum_linea_cantidad, 6, '.', ','); ?></th>
                  </tr>
                  <?php
                  $sum_linea_cantidad = 0.000000;
                } ?>
                <tr class="tr-theadFormat tr-theadFormat_header">
                  <th class="text-right">Linea</th>
                  <th class="text-left" colspan="3"><?php echo $row->No_Linea; ?></th>
                </tr>
                <?php
                $ID_Linea = $row->ID_Linea;
              } ?>
              <tr class="tr-theadFormat">
                <td class="text-right"><?php echo $row->Nu_Codigo_Barra; ?></td>
                <td class="text-right"><?php echo $row->No_Producto; ?></td>
                <td class="text-right"><?php echo $row->No_Unidad_Medida; ?></td>
                <td class="text-right"><?php echo numberFormat($row->Qt_Producto, 6, '.', ','); ?></td>
              </tr>
              <?php
              $sum_linea_cantidad += $row->Qt_Producto;
              $sum_cantidad += $row->Qt_Producto;
              $counter++;
            } ?>
            <tr class="tr-theadFormat tr-theadFormat_footer">
              <th class="text-right" colspan="3">Total Linea</th>
              <th class="text-right"><?php echo numberFormat($sum_linea_cantidad, 6, '.', ','); ?></th>
            </tr>
            <tr class="tr-theadFormat tr-theadFormat_footer">
              <th class="text-right" colspan="3">Total General</th>
              <th class="text-right"><?php echo numberFormat($sum_cantidad, 6, '.', ','); ?></th>
            </tr>
          <?php
          } else { ?>
          <tr class="tr-theadFormat">
            <td class="text-center" colspan="4">No hay registros</td>
          </tr>
          <?php
          } ?>
        </tbody>
      </table>
    </body>
</html>