<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
      <!-- ./New box-header -->
    </div>
    <?php
    if ( !empty($sStatus) ){
      $sClassModal = 'success';
      $sMessage = 'Datos cargados satisfactoriamente';
      if ( (int)$iCantidadNoProcesados > 0 ){
        $sMessage .= '. Pero tiene ' . $iCantidadNoProcesados . ' registro(s) no procesados';
      }
      if ( $sStatus == 'error-sindatos' ) {
        $sMessage = 'Llenar los campos obligatorios o los valores no son iguales a las columna del excel';
        $sClassModal = 'danger';  
      } else if ( $sStatus == 'error-bd' ) {
        $sMessage = 'Problemas al generar excel';
        $sClassModal = 'danger';  
      } else if ( $sStatus == 'error-archivo_no_existe' ) {
        $sMessage = 'El archivo no existe';
        $sClassModal = 'danger';  
      } else if ( $sStatus == 'error-copiar_archivo' ) {
        $sMessage = 'Error al copiar archivo al servidor';
        $sClassModal = 'danger';  
      }
    ?>
      <div class="modal fade in modal-<?php echo $sClassModal; ?>" id="modal-message_excel" role="dialog" style="display: block;">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?php echo $sMessage; ?></h4>
            </div>
            <div class="modal-footer">
              <button type="button" id="btn-cerrar_modal_excel" class="btn btn-outline pull-right" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-md-3">
                <div class="form-group">
    		  				<select id="cbo-Filtros_Productos" name="Filtros_Productos" class="form-control">
    		  				  <option value="Producto">Nombre Producto</option>
    		  				  <option value="CodigoBarra">Código Barra</option>
                    <option value="Grupo">Grupo</option>

                    <!-- M011 - I  -->
                    <option value="Area">Area</option>
                    <!-- M011 - F  -->

                    <option value="Categoria">Categoría</option>
                    <option value="UnidadMedida">Unidad Medida</option>
                    <option value="Marca">Marca</option>
                    <option value="Impuesto">Impuesto</option>
                    <option value="Stock">Stock</option>
    		  				</select>
                </div>
              </div>
              
              <div class="col-md-5">
                <div class="form-group">
                  <input type="text" id="txt-Global_Filter" name="Global_Filter" class="form-control" maxlength="250" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                <button type="button" class="btn btn-success btn-block" onclick="agregarProducto()"><i class="fa fa-plus-circle"></i> Agregar</button>
                <?php endif; ?>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                  <button type="button" class="btn btn-default btn-block" onclick="importarExcelProductos()"><i class="fa fa-file-excel-o color_icon_excel"></i> Importar</button>
                <?php endif; ?>
              </div>
              
              
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <label>Busca por cualquier columna</label>
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Busca por cualquier columna" title="Busca por cualquier columna" class="form-control">
                </div>
              </div>
            </div>
          </div>
          <!-- ./box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-Producto" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Grupo</th>

                  <!-- M011 - INICIO -1 -->
                  <th>Area</th>
                  <!-- M011 - FIN -1 -->

                  <th>Unidad Medida</th>
                  <th>Categoría</th>
                  
                  <!-- M013 - INICIO -1 -->
                  <th>Sub Categoría</th>
                  <!-- M013 - FIN -1 -->

                  <th>Marca</th>
                  <th>SKU</th>
                  <th>Nombre</th>
                  <th>Impuesto</th>
                  <th class="sort_right">Precio</th>
                  <th class="sort_right">Costo</th>
                  <th class="sort_right">Stock</th>
                  <th>Stock Mínimo</th>
                  <th>Ubicación</th>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-Producto');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" id="txt-EID_Empresa" name="EID_Empresa" class="form-control">
          	  <input type="hidden" id="txt-EID_Producto" name="EID_Producto" class="form-control">
          	  <input type="hidden" id="txt-ENu_Codigo_Barra" name="ENu_Codigo_Barra" class="form-control">
          	  <input type="hidden" id="hidden-nombre_imagen" name="No_Imagen_Item" class="form-control">
    	  
              <?php
                $sCssDisplayViewHideFarmacia='style="display:none"';
                $sCssDisplayViewHideTiendaGranel='style="display:none"';
                $sCssDisplayViewHideLavanderia='style="display:none"';
                $sCssDisplayViewHideGeneral='';
                if ( $this->empresa->Nu_Tipo_Rubro_Empresa == 1 ){//1 = Farmacia
                  $sCssDisplayViewHideFarmacia='';
                }
                if ( $this->empresa->Nu_Tipo_Rubro_Empresa == 2 ){//2 = Tienda a granel
                  $sCssDisplayViewHideTiendaGranel='';
                }
                if ( $this->empresa->Nu_Tipo_Rubro_Empresa == 3 ){//3 = Lavanderia
                  $sCssDisplayViewHideLavanderia='';
                }

                if ( $this->empresa->Nu_Tipo_Rubro_Empresa == 3 ){//3 = Lavanderia
                  $sCssDisplayViewHideGeneral='style="display:none"';
                }
                
                $sCssDisplayViewHideEcommerceMarketplace='style="display:none"';
                if ( $this->empresa->ID_Empresa_Marketplace > 0 ){// Es decir; si tiene una empresa asocioada
                  $sCssDisplayViewHideEcommerceMarketplace='';
                }
              ?>

              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-2">
                  <div class="form-group">
                    <label>Grupo <span class="label-advertencia">*</span></label>
                    <select id="cbo-TiposItem" name="Nu_Tipo_Producto" class="form-control required" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <!--  M011 - INICIO -->  
                <div class="col-xs-12 col-md-2"> 
                  <div class="form-group"> 
                    <label>Area<span class="label-advertencia">*</span></label>
                    <select id="cbo-AreaAlmacen" name="Nu_Area_Almacen" class="form-control required" style="width: 100%;"> </select> 
                    <span class="help-block" id="error"></span>
                  </div> 
                </div> 
                <!--  M011 - FIN --> 

                <div class="col-xs-12 col-sm-4 col-md-3 div-Producto">
                  <div class="form-group">
                    <label>Tipo Producto <span class="label-advertencia">*</span></label>
                    <select id="cbo-TiposExistenciaProducto" name="ID_Tipo_Producto" class="form-control" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-4 col-md-2" style="display: none;">
                  <div class="form-group">
                    <label>Ubicación Inv.</label>
                    <select id="cbo-UbicacionesInventario" name="ID_Ubicacion_Inventario" class="form-control" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>Código de SKU <span class="label-advertencia">*</span></label>
                    <input type="text" id="txt-Nu_Codigo_Barra" name="Nu_Codigo_Barra" class="form-control input-codigo_barra input-Mayuscula" placeholder="Ingresar sku" maxlength="20" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-2 div-Producto hidden">
                  <div class="form-group">
                    <label>Código Interno</label>
                    <input type="text" id="txt-No_Codigo_Interno" name="No_Codigo_Interno" class="form-control input-codigo_barra input-Mayuscula" placeholder="Ingresar sku" maxlength="20" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3">
                  <div class="form-group">
                    <label>Afectación Tributaria <span class="label-advertencia">*</span></label>
                    <select id="cbo-Impuestos" name="ID_Impuesto" class="form-control required" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
              
      			  <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4" style="display: none;">
                  <div class="form-group">
                    <label>Producto SUNAT <span class="label-advertencia">*</span></label>
                    <input type="hidden" id="hidden-ID_Tabla_Dato" name="ID_Tabla_Dato" class="form-control">
                    <input type="text" id="txt-No_Descripcion" name="No_Descripcion" class="form-control autocompletar_producto_sunat" placeholder="Ingresar nombre" value="" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-8">
                  <div class="form-group">
                    <label>Nombre <span class="label-advertencia">*</span></label>
                    <textarea name="No_Producto" class="form-control required" placeholder="Ingresar nombre" maxlength="250"></textarea>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-6 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>P. Venta</label>
                    <input type="text" name="Ss_Precio" class="form-control input-decimal" maxlength="13" autocomplete="off" placeholder="Precio">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-6 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>P. Compra</label>
                    <input type="text" name="Ss_Costo" class="form-control input-decimal" maxlength="13" autocomplete="off" placeholder="Precio">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
      	  			
      			  <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-2" <?php echo $sCssDisplayViewHideFarmacia; ?>>
                  <div class="form-group">
                    <label>Lote vencimiento</label>
      		  				<select id="cbo-lote_vencimiento" name="Nu_Lote_Vencimiento" class="form-control"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>Unidad Medida <span class="label-advertencia">*</span></label>
                    <select id="cbo-UnidadesMedida" name="ID_Unidad_Medida" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>Marca</label>
                    <select id="cbo-Marcas" name="ID_Marca" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="form-group">
                    <label>Categoría <span class="label-advertencia">*</span></label>
                    <select id="cbo-categoria" name="ID_Familia" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="form-group">
                    <label>Sub Categoría</label>
                    <select id="cbo-sub_categoria" name="ID_Sub_Familia" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2 div-Producto" <?php echo $sCssDisplayViewHideGeneral; ?>>
                  <div class="form-group">
                    <label>Stock mínimo</label>
      		  				<input type="tel" id="tel-Nu_Stock_Minimo" class="form-control input-number" maxlength="3" value="" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-1 div-Producto">
                  <div class="form-group">
                    <label>Icbper <span class="label-advertencia">*</span></label>
                    <select id="cbo-impuesto_icbper" name="ID_Impuesto_Icbper" class="form-control required" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-4 col-sm-4 col-md-2">
                  <div class="form-group">
                    <label>Estado <span class="label-advertencia">*</span></label>
      		  				<select id="cbo-Estado" name="Nu_Estado" class="form-control required"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
              
      			  <div class="row" <?php echo $sCssDisplayViewHideEcommerceMarketplace; ?>>
                <div class="col-xs-6 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>Precio Online Regular</label>
                    <input type="text" name="Ss_Precio_Ecommerce_Online_Regular" class="form-control input-decimal" maxlength="10" autocomplete="off" placeholder="Precio">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-6 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>Precio Online <span class="label-advertencia">*</span></label>
                    <input type="text" name="Ss_Precio_Ecommerce_Online" class="form-control required input-decimal" maxlength="10" autocomplete="off" placeholder="Precio">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="form-group">
                    <label>Categoría Ecommerce <span class="label-advertencia">*</span></label>
      		  				<select id="cbo-categoria_marketplace" name="ID_Familia_Marketplace" class="form-control required select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="form-group">
                    <label>Sub Categoría Ecommerce</label>
                    <select id="cbo-sub_categoria_marketplace" name="ID_Sub_Familia_Marketplace" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2">
                  <div class="form-group">
                    <label>Marca Ecommerce</label>
                    <select id="cbo-marca_marketplace" name="ID_Marca_Marketplace" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-2" <?php echo $sCssDisplayViewHideFarmacia; ?>>
                  <div class="form-group">
                    <label>Receta médica</label>
      		  				<select id="cbo-receta_medica" name="Nu_Receta_Medica" class="form-control required"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-2" <?php echo $sCssDisplayViewHideFarmacia; ?>>
                  <div class="form-group">
                    <label>Laboratorio</label>
                    <select id="cbo-laboratorio" name="ID_Laboratorio" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>  

                <div class="col-xs-6 col-sm-6 col-md-3" <?php echo $sCssDisplayViewHideFarmacia; ?>>
                  <div class="form-group">
                    <label>Composición</label>
                    <select id="cbo-composicion" name="Txt_Composicion" class="form-control select2" multiple="multiple" style="width: 100%;" placeholder="- Seleccionar -"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-2" <?php echo $sCssDisplayViewHideTiendaGranel; ?>>
                  <div class="form-group">
                    <label>CO2</label>
                    <input type="tel" id="tel-Qt_CO2_Producto" name="Qt_CO2_Producto" class="form-control input-decimal" placeholder="Ingresar cantidad" value="" autocomplete="off">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3" <?php echo $sCssDisplayViewHideLavanderia; ?>>
                  <div class="form-group">
                    <label>Ubicación de Planta<span class="label-advertencia">*</span></label>
                    <select id="cbo-tipo_pedido_lavado" name="ID_Tipo_Pedido_Lavado" class="form-control required" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-2" style="display:none" <?php echo $sCssDisplayViewHideGeneral; ?>>
                  <div class="form-group">
                    <label>Es Compuesto <span class="label-advertencia">*</span></label>
      		  				<select id="cbo-Compuesto" name="Nu_Compuesto" class="form-control required"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3 div-Producto">
                  <div class="form-group">
                    <label>Ubicación producto</label>
                    <input type="text" id="txt-Txt_Ubicacion_Producto_Tienda" class="form-control" value="" autocomplete="off" placeholder="Nro. anaquel - Nivel y más información">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
<!--
Modelo - No_Modelo_Vehiculo 
Color -	No_Color_Vehiculo 
Talla - No_Motor
-->
                <div class="col-xs-12 col-sm-12 col-md-3 div-Producto">
                  <div class="form-group">
                    <label>Modelo o Tipo</label>
                    <input type="text" id="txt-No_Modelo_Vehiculo" name="No_Modelo_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3 div-Producto">
                  <div class="form-group">
                    <label>Presentación o Color</label>
                    <input type="text" id="txt-No_Color_Vehiculo" name="No_Color_Vehiculo" class="form-control" value="" autocomplete="off" placeholder="" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3 div-Producto">
                  <div class="form-group">
                    <label>Contenido o Talla</label>
                    <input type="text" id="txt-No_Motor" name="No_Motor" class="form-control" value="" autocomplete="off" placeholder="" maxlength="30">
                    <span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>

              <div class="row div-Compuesto">
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title"><i class="fa fa-tag"></i> Enlaces de Item</h3>
                    </div>
                    <div class="box-body">
                      <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                          <label>Nombre item</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                            <input type="hidden" id="txt-AID" name="AID" class="form-control">
                            <input type="hidden" id="txt-ACodigo" name="ACodigo" class="form-control">
                            <input type="text" id="txt-ANombre" name="ANombre" class="form-control autocompletar" data-global-class_method="AutocompleteController/globalAutocomplete" data-global-table="producto" placeholder="Ingresar nombre" value="" autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-3">
                        <div class="form-group">
                          <label>Cantidad Descargar</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-balance-scale" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Qt_Producto_Descargar" name="Qt_Producto_Descargar" class="form-control input-decimal" placeholder="Ingresar cantidad" value="1" autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-3">
                        <div class="form-group">
                          <label></label>
                          <div class="input-group">
                            <button type="button" id="btn-addProductosEnlaces" class="btn btn-success"><i class="fa fa-plus-circle"></i> Agregar Producto</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="table-responsive div-Compuesto">
                      <table id="table-Producto_Enlace" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th style='display:none;' class="text-left">ID</th>
                            <th class="text-left">Código Barra</th>
                            <th class="text-left">Nombre</th>
                            <th class="text-right">Cantidad</th>
                            <th class="text-center"></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div><!-- ./Compuesto -->

      			  <div class="row div-Producto">
      			    <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                    <label>Descripción</label>
                    <textarea name="Txt_Producto" class="form-control"></textarea>
                  </div>
                </div>
              </div>
              
    	        <div class="row div-Producto"><br>
                <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="well well-sm">
                    <i class="fa fa-warning"></i> Indicaciones:
                    <br>- Imagen: <b>opcional</b>
                    <br>- Formato: <b>PNG</b>
                    <br>- Fondo de imágen(es): <b>SIN FONDO / BLANCO / TRANSPARENTE</b>
                    <br>- Tamaños: <br>
                      &nbsp;&nbsp;* Pequeño: <b>Ancho: 199px</b> y <b>Alto: 199px</b><br>
                      &nbsp;&nbsp;* Mediano: <b>Ancho: 340px</b> y <b>Alto: 340px</b>
                    <br>- Peso Máximo: <b>200 KB</b>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-6 col-md-6 text-center divDropzone"></div>
              </div>
              
      			  <div class="row">
      			    <br/>
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->