<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>

  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
      <!-- ./New box-header -->
    </div>
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new">
            <div class="row div-Filtros">
              <br>
              <div class="col-md-6">
                <label>Empresa</label>
                <div class="form-group">
                  <select id="cbo-filtro_empresa" name="ID_Empresa" class="form-control select2" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              <div class="col-md-6">
                <label>Organización</label>
                <div class="form-group">
                  <select id="cbo-filtro_organizacion" name="ID_Organizacion" class="form-control select2" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
    		  				<select id="cbo-Filtros_Almacenes" name="Filtros_Almacenes" class="form-control">
    		  				  <option value="Almacen">Nombre Almacén</option>
    		  				  <option value="Organizacion">Nombre Organización</option>
    		  				</select>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" id="txt-Global_Filter" name="Global_Filter" class="form-control" maxlength="100" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-md-3">
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                <button type="button" class="btn btn-success btn-block" onclick="agregarAlmacen()"><i class="fa fa-plus-circle"></i> Agregar</button>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive">
            <table id="table-Almacen" class="table table-striped table-bordered">
              <thead>
              <tr>
                <th>Proveedor FE</th>
                <th>Ecommerce</th>
                <th>Empresa</th>
                <th>Organización</th>
                <th>Almacén</th>
                <th>Dirección</th>
                <th>Pago Sistema</th>
                <th class="no-sort">Estado</th>
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                  <th class="no-sort"></th>
                <?php endif; ?>
                <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                  <th class="no-sort"></th>
                <?php endif; ?>
              </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  <!-- Modal -->
  <?php
  $attributes = array('id' => 'form-Almacen');
  echo form_open('', $attributes);
  ?>
  <div class="modal fade" id="modal-Almacen" role="dialog">
  <div class="modal-dialog modal-lg">
  	<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center"></h4>
      </div>
      
    	<div class="modal-body">
    	  <input type="hidden" name="EID_Organizacion" class="form-control required">
    	  <input type="hidden" name="EID_Almacen" class="form-control required">
    	  <input type="hidden" name="ENo_Almacen" class="form-control required">
    	  
			  <div class="row">
          <div class="col-md-6">
            <label>Empresa <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <select id="cbo-Empresas" name="ID_Empresa" class="form-control select2 required" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label>Organización <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Organizaciones" name="ID_Oganizacion" class="form-control select2 required" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
			    
          <div class="col-md-5">
            <label>Nombre <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <input type="text" id="txt-No_Almacen" name="No_Almacen" placeholder="Ingresar descripción" class="form-control required" autocomplete="off" maxlength="100">
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-md-3">
            <label>Cód. Establecimiento SUNAT <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <input type="text" id="txt-Nu_Codigo_Establecimiento_Sunat" name="Nu_Codigo_Establecimiento_Sunat" placeholder="Ingresar código" class="form-control input-Mayuscula input-codigo_barra" value="0000" maxlength="30" autocomplete="off">
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-md-2">
            <div class="form-group">
              <label>Pago Sistema <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Estado_Pago_Sistema" name="Nu_Estado_Pago_Sistema" class="form-control required" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-md-2">
            <div class="form-group">
              <label>Estado <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Estado" name="Nu_Estado" class="form-control required" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>País <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Paises" name="ID_Pais" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>Departamento <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Departamentos" name="ID_Departamento" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>Provincia <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Provincias" name="ID_Provincia" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>

          <div class="col-xs-6 col-md-3">
            <div class="form-group">
              <label>Distrito <span class="label-advertencia">*</span></label>
		  				<select id="cbo-Distritos" name="ID_Distrito" class="form-control select2" style="width: 100%;"></select>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
        
			  <div class="row">
          <div class="col-md-12">
            <label>Dirección <span class="label-advertencia">*</span></label>
            <div class="form-group">
              <input type="hidden" id="txt-direccion-lat" name="Nu_Latitud_Maps" class="form-control">
              <input type="hidden" id="txt-direccion-lng" name="Nu_Longitud_Maps" class="form-control">
              <input type="text" id="txt-direccion" name="Txt_Direccion_Almacen" placeholder="Ingresar descripción breve" class="form-control" autocomplete="off">
              <span class="help-block" id="error"></span>
              <div id="map" class="hidden"></div>
            </div>
          </div>
        </div>
        
			  <div class="row div-row-nubefact">
          <div class="col-md-6">
            <div class="form-group">
              <label>Ruta</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="blue fa fa-key" aria-hidden="true"></i></span>
                <input type="text" name="Txt_FE_Ruta" placeholder="Ingresar Ruta" class="form-control required" autocomplete="off">
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group">
              <label>Token</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="blue fa fa-key" aria-hidden="true"></i></span>
                <input type="text" name="Txt_FE_Token" placeholder="Ingresar Token" class="form-control required" autocomplete="off">
              </div>
              <span class="help-block" id="error"></span>
            </div>
          </div>
        </div>
      </div>
      
    	<div class="modal-footer">
			  <div class="row">
          <div class="col-xs-6 col-md-6">
            <div class="form-group">
              <button type="button" class="btn btn-danger btn-md btn-block" data-dismiss="modal"><span class="fa fa-sign-out"></span> Salir</button>
            </div>
          </div>
          <div class="col-xs-6 col-md-6">
            <div class="form-group">
              <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <?php echo form_close(); ?>
  <!-- /.Modal -->
</div>
<!-- /.content-wrapper -->