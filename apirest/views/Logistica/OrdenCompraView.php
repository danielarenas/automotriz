<script>
  document.body.onkeydown = function(e) {
    if (e.keyCode === 13) {
      e.preventDefault();
      document.getElementById("btn-filter").click();
    }
  };
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>              
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Serie</label>
                  <select id="cbo-Filtro_serie_documento" class="form-control select2" multiple="multiple"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <input type="tel" id="txt-Filtro_NumeroDocumento" class="form-control tagify-number" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Estado</label>
    		  				<select id="cbo-Filtro_Estado" class="form-control select2" multiple="multiple">
    		  				  <option value="0" selected>Todos</option>
        				    <option value="1">Generada</option>
        				    <option value="2">Recibida</option>
        				  </select>
                </div>
              </div>

              <div class="col-xs-12 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Estado Cont</label>
    		  				<select id="cbo-Filtro_EstadoCont" class="form-control select2"  multiple="multiple">
    		  				  <option value="0" selected>Todos</option>
        				    <option value="1">Pendiente</option>
        				    <option value="2">Observada</option>
        				    <option value="3">Pagada</option>
        				  </select>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-8">
                <div class="form-group">
                  <label>Nombre Proveedor</label>
                  <select id="cbo-Filtro_Entidad" class="form-control select2" multiple="multiple" data-placeholder="Ingresar Nombre" data-ajax--url="../../AutocompleteController/getAllProviderMultiple" data-ajax--cache="false"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                    <button type="button" class="btn btn-success btn-block" onclick="agregarOrdenCompra()"><i class="fa fa-plus-circle"></i> Agregar</button>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-OrdenCompra" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="no-sort">OC</th>
                  <th class="no-sort">F. Emisión</th>
                  <th class="no-sort_left">Proveedor</th>
                  <th class="no-sort">Placa</th>
                  <th class="no-sort">N. Pres</th>
                  <th class="no-sort">OI</th>
                  <th class="no-sort">Sub Total</th>
                  <th class="no-sort_right">IGV</th>
                  <th class="no-sort">Total</th>
                  <th class="no-sort">Estado</th>
                  <th class="no-sort">F. Recep.</th>
                  <th class="no-sort">Estado Conta</th>
                  <!-- OC_MODAL_5-1 - INICIO -->
                  <th class="no-sort">Estado OC</th>
                  <!-- OC_MODAL_5-1 - FIN -->
                  <th class="no-sort">F. Pago</th>
                  <!-- 03_08-4 INICIO -->
                  <th class="no-sort">Nro. Operación</th>
                  <!-- 03_08-4 FIN -->
                  <th class="no-sort"></th>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Editar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Eliminar == 1) : ?>
                    <th class="no-sort"></th>
                  <?php endif; ?>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-OrdenCompra');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" name="EID_Empresa" class="form-control">
          	  <input type="hidden" name="EID_Documento_Cabecera" class="form-control">
          	  <input type="hidden" name="ENu_Estado" class="form-control">
          	  		  
      			  <div class="row">
                <div class="col-sm-12 col-md-12">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento</b></div>
                    <div class="panel-body">                      
                      <div class="row">
                      <div class="col-xs-6 col-sm-3 col-md-1">
                        <div class="form-group">
                          <label>Serie <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-serie_documento" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-6 col-sm-3 col-md-2">
                        <div class="form-group">
                          <label>Número <span class="label-advertencia">*</span></label>
                          <input type="tel" id="txt-ID_Numero_Documento" name="ID_Numero_Documento" class="form-control required input-number" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Emisión <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Emision" name="Fe_Emision" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Vencimiento <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Vencimiento" name="Fe_Vencimiento" class="form-control required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-3">
                        <div class="form-group">
                          <label>Medio Pago <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-MediosPago" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                
                      <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group">
                          <label>Moneda <span class="label-advertencia">*</span></label>
                          <select id="cbo-Monedas" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-xs-12 col-sm-2 col-md-2">
                        <input type="hidden" id="txt-ID_Placa" name="" class="form-control">
                         <!--   // OC_MODAL_6 - INICIO -->
                         <div class="form-group"> 
                          <label>Placa <!-- span class="label-advertencia">*</span--></label>
                          <input type="text" id="txt-No_Placa_Vehiculo_existe" name="" disabled  class="form-control autocompletar_placa_x_cliente" value="" autocomplete="off" placeholder="Ingresar número" maxlength="6">
                          <!-- span class="help-block" id="error"></span-->
                        </div>
                        <!-- // OC_MODAL_6 - FIN -->
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group">
                          <!--   // OC_MODAL_6 - INICIO -->
                          <label>ID Presupuesto</label>
                          <input type="text" id="txt-ID_Presupesto_Existe" name="ID_Presupesto_Existe" disabled class="form-control autocompletar_presupuesto" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <!-- span class="help-block" id="error"></span-->
                          <!-- // OC_MODAL_6 - FIN -->
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group">
                          <!--   // OC_MODAL_6 - INICIO -->
                          <label>ID OI</label>
                          <input type="text" id="txt-ID_OI_Existe" disabled class="form-control" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <!-- span class="help-block" id="error"></span -->
                          <!-- // OC_MODAL_6 - FIN -->
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Recepción</label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Recepcion" name="Fe_Recepcion" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>Estado <span class="label-advertencia">*</span></label>
                          <select id="cbo-Estado" class="form-control required">
                            <option value="1">Generada</option>
                            <option value="2">Recibida</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>Estado Cont <span class="label-advertencia">*</span></label>
                          <select id="cbo-EstadoCont" class="form-control required">
                            <option value="1">Pendiente</option>
                            <option value="2">Observada</option>
                            <option value="3">Pagada</option>
                          </select>
                        </div>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-xs-12 col-sm-4 col-md-2">
                        <div class="form-group">
                          <label>F. Pago</label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Pago" name="Fe_Pago" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-2">
                        <div class="form-group">
                          <label>N Operación</label>
                          <input type="text" id="txt-Nu_Operacion" class="form-control" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-2 text-center">
                        <label>Servicio <input type="radio" name="ServicioBien" id="radio-servicio" class="flat-red" value="1"></label>
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-2 text-center">
                        <label>Bien <input type="radio" name="ServicioBien" id="radio-bien" class="flat-red" value="0"></label>
                      </div>

                      </div>
                    </div>
                  </div>
                </div><!-- ./Documento -->
              </div>
              
      			  <div class="row">
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-user"></i> <b>Proveedor</b></div>
                    <div class="panel-body">
                      <div class="col-xs-6 text-center">
                        <label><input type="radio" name="addProveedor" id="radio-cliente_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-6 text-center">
                        <label><input type="radio" name="addProveedor" id="radio-cliente_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <br>
                        <div class="form-group">
                          <label>Nombre Proveedor <span class="label-advertencia">*</span></label>
                          <input type="hidden" id="txt-AID" name="AID" class="form-control required">
                          <input type="text" id="txt-ANombre" name="ANombre" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllProvider" data-global-table="entidad" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-7 div-cliente_existente">
                        <div class="form-group">
                          <label>Número Documento Identidad <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-ACodigo" name="ACodigo" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <!-- Proveedor Nuevo -->
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Tipo Doc. Identidad <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-TiposDocumentoIdentidadProveedor" name="ID_Tipo_Documento_Identidad" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label id="label-Nombre_Documento_Identidad_Proveedor">DNI</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-Nu_Documento_Identidad_Proveedor" name="Nu_Documento_Identidad_Proveedor" class="form-control required input-Mayuscula input-codigo_barra" placeholder="Ingresar número" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-2 text-center div-cliente_nuevo">
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_orden_compra_cliente" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
              
                      <div class="col-xs-12 col-md-10 div-cliente_nuevo">
                        <div class="form-group">
                          <label id="label-No_Entidad_Proveedor">Nombre(s) y Apellidos</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-No_Entidad_Proveedor" name="No_Entidad_Proveedor" class="form-control required" placeholder="Ingresar nombre" value="" autocomplete="off" maxlength="100">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-12 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" id="txt-Txt_Direccion_Entidad_Proveedor" name="Txt_Direccion_Entidad_Proveedor" class="form-control" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-5 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Celular</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Celular_Entidad_Proveedor"  name="Nu_Celular_Entidad_Proveedor" class="form-control" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-4 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Telefono</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Telefono_Entidad_Proveedor" name="Nu_Telefono_Entidad_Proveedor" class="form-control" data-inputmask="'mask': ['999 9999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Proveedor -->
                
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-user"></i> <b>Contacto</b></div>
                    <div class="panel-body">
                      <input type="hidden" id="txt-ID_Tipo_Asiento" class="form-control" value="2">
                      
                      <div class="col-xs-6 text-center">
                        <label><input type="radio" name="addContacto" id="radio-contacto_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-6 text-center">
                        <label><input type="radio" name="addContacto" id="radio-contacto_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 col-md-6 div-contacto_existente hidden">
                        <div class="form-group id_tipo_documento_identidad">
                          <label>Tipo Doc. Identidad</label>
            		  				<select id="cbo-TiposDocumentoIdentidadContacto_existe" name="ID_Tipo_Documento_Identidad" class="form-control" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-6 div-contacto_existente hidden">
                        <div class="form-group">
                          <label id="label-Nombre_Documento_Identidad">DNI</label>
                          <input type="text" id="txt-Nu_Documento_Identidad_existe" name="Nu_Documento_Identidad" class="form-control input-Mayuscula input-codigo_barra" placeholder="Ingresar número" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-12 div-contacto_existente">
                        <br>
                        <div class="form-group">
                          <label id="label-No_Contacto">Nombre(s) y Apellidos</label>
                          <input type="hidden" id="txt-AID_Contacto" name="AID_Contacto" class="form-control">
                          <input type="text" id="txt-No_Contacto_existe" name="No_Contacto" class="form-control autocompletar_contacto" placeholder="Ingresar nombre" maxlength="50" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
          
                      <div class="col-xs-12 col-md-12 div-contacto_existente">
                        <div class="form-group">
                          <label>Correo</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="blue fa fa-envelope" aria-hidden="true"></i></span>
                            <input type="text" id="txt-Txt_Email_Contacto_existe" name="Txt_Email_Contacto" placeholder="Ingresar correo" class="form-control" autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                
                      <div class="col-xs-12 col-sm-5 col-md-5 div-contacto_existente">
                        <div class="form-group">
                          <label>Celular</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Celular_Contacto_existe" name="Nu_Celular_Contacto" class="form-control" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-4 div-contacto_existente">
                        <div class="form-group">
                          <label>Teléfono</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Telefono_Contacto_existe" name="Nu_Telefono_Contacto" class="form-control" data-inputmask="'mask': ['999 9999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <!-- Contacto Nuevo -->
                      <div class="col-xs-12 col-sm-6 col-md-6 div-contacto_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Tipo Doc. Identidad</label>
            		  				<select id="cbo-TiposDocumentoIdentidadContacto" name="ID_Tipo_Documento_Identidad" class="form-control" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-6 div-contacto_nuevo">
                        <br>
                        <div class="form-group">
                          <label id="label-Nombre_Documento_Identidad">DNI</label>
                          <input type="text" id="txt-Nu_Documento_Identidad" name="Nu_Documento_Identidad" class="form-control input-Mayuscula input-codigo_barra" placeholder="Ingresar número" autocomplete="off" maxlength="8">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-2 text-center div-contacto_nuevo">
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_orden_compra_contacto" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                  
                      <div class="col-xs-12 col-md-10 div-contacto_nuevo">
                        <div class="form-group">
                          <label id="label-No_Contacto">Nombre(s) y Apellidos</label>
                          <input type="text" id="txt-No_Contacto" name="No_Contacto" class="form-control" placeholder="Ingresar nombre" maxlength="50" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
          
                      <div class="col-xs-12 col-md-12 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Correo</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="blue fa fa-envelope" aria-hidden="true"></i></span>
                            <input type="text" id="txt-Txt_Email_Contacto" name="Txt_Email_Contacto" placeholder="Ingresar correo" class="form-control" autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                
                      <div class="col-xs-12 col-sm-5 col-md-5 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Celular</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Celular_Contacto" name="Nu_Celular_Contacto" class="form-control" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                
                      <div class="col-xs-12 col-sm-5 col-md-4 div-contacto_nuevo">
                        <div class="form-group">
                          <label>Teléfono</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Telefono_Contacto" name="Nu_Telefono_Contacto" class="form-control" data-inputmask="'mask': ['999 9999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Contacto -->
              </div>
              
      			  <div class="row">
                <div class="col-md-12">
        			    <div id="panel-DetalleProductosOrdenCompra" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-shopping-cart"></i> <b>Detalle</b></div>
                    <div class="panel-body">
      			          <div class="row">
          	            <input type="hidden" name="Nu_Tipo_Lista_Precio" value="2" class="form-control">
                        <div class="col-xs-12">
                          <label>Lista de Precio <span class="label-advertencia">*</span></label>
                          <div class="form-group">
                            <select id="cbo-lista_precios" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-9">
                          <label>Producto / Servicio <span class="label-advertencia">*</span></label>
                          <div class="form-group">
                            <input type="hidden" id="txt-Activar_Almacen" class="form-control" value="1">
                            <input type="hidden" id="txt-Nu_Tipo_Registro" class="form-control" value="0"><!-- Compra -->
                            <input type="hidden" id="txt-Nu_Compuesto" class="form-control" value="0">
                            <input type="hidden" id="txt-ID_Producto" class="form-control">
                            <input type="hidden" id="txt-Nu_Codigo_Barra" class="form-control">
                            <input type="hidden" id="txt-Ss_Precio" class="form-control">
                            <input type="hidden" id="txt-ID_Impuesto_Cruce_Documento" class="form-control">
                            <input type="hidden" id="txt-Nu_Tipo_Impuesto" class="form-control">
                            <input type="hidden" id="txt-Ss_Impuesto" class="form-control">
                            <!-- OC_MODAL_6 - INICIO -->               
                            <input type="hidden" id="Txt_ID_Usuario"  value=<?php echo $this->session->userdata['usuario']->No_Usuario; ?>  class="form-control">
                            <input type="hidden" id="Txt_Creado_Por"  value="Y"  class="form-control">
                            <input type="text" id="txt-No_Producto" class="form-control autocompletar_detalle_oc_v2" data-global-class_method="AutocompleteController/getAllProduct" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off">
                            <!-- input type="text" id="txt-No_Producto" class="form-control autocompletar_detalle" data-global-class_method="AutocompleteController/getAllProduct" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off" -->
                            <!-- OC_MODAL_6 - FIN -->
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-3">
                          <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="button" id="btn-addProductoOrden" class="btn btn-success btn-md btn-block"><i class="fa fa-plus-circle"></i> Agregar Item Detalle</button>
                          </div>
                        </div>
                      </div>
                      
      			          <div class="row">
                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table id="table-DetalleProductosOrdenCompra" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th style="display:none;" class="text-left"></th>
                                  <th class="text-center" style="width: 10%;">Cantidad</th>
                                  <th class="text-center" style="width: 35%;">Item</th>
                                  <th class="text-center" style="width: 10%;">Precio</th>
                                  <th class="text-center" style="width: 15%;">Impuesto Tributario</th>
                                  <th class="text-center" style="display:none;">Sub Total</th>
                                  <th class="text-center" style="width: 10%;">% Dscto</th>
                                  <th class="text-center">Total</th>
                                  <th class="text-center"></th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Detalle -->
              </div>
              
      			  <div class="row">
      			    <div class="col-md-12">
      			      <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-comment-o"></i> <b>Garantía y Glosa</b></div>
                    <div class="panel-body">
                      <input type="text" name="Txt_Garantia" class="form-control" placeholder="Garantía" value="" autocomplete="off">
                      <br>
                      <textarea name="Txt_Glosa" class="form-control" placeholder="Glosa" value="" autocomplete="off"></textarea>
                      <br>
                      <textarea name="Txt_Comentario" class="form-control" placeholder="Comentario" value="" autocomplete="off"></textarea>
                    </div>
                  </div>
                </div>
              </div>
                
      			  <div class="row"><!-- Totales -->
      			    <div class="col-md-8"></div>
                <div class="col-md-4">
      			    <div class="panel panel-default">
                  <div class="panel-heading"><i class="fa fa-money"></i> <b>Totales</b></div>
                  <div class="panel-body">
                    <table class="table" id="table-OrdenCompraTotal">
                      <tr>
                        <td><label>% Descuento</label></td>
                        <td class="text-right">
    	  					        <input type="tel" class="form-control input-decimal" id="txt-Ss_Descuento" name="Ss_Descuento" size="3" value="" autocomplete="off" />
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Gravadas</label></td>
                        <td class="text-right">
    	  					        <input type="hidden" class="form-control" id="txt-subTotal" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-subTotal">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Inafectas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-inafecto" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-inafecto">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Exoneradas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-exonerada" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-exonerada">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Gratuitas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-gratuita" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-gratuita">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Descuento Total (-)</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-descuento" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-descuento">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>I.G.V. %</label></td>
                        <td class="text-right">
                            <input type="hidden" class="form-control" id="txt-impuesto" value="0.00"/>
                            <span class="span-signo"></span> <span id="span-impuesto">0.00</span>
                        </td>
                      </tr>
                        
                      <tr>
                        <td><label>Total</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-total" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-total">0.00</span>
                        </td>
                      </tr>
                    </table><!-- ./Totales -->
                  </div>
                </div>
              </div>
              
      			  <div class="row">
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->