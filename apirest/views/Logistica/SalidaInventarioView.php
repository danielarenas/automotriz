<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header"><?php //array_debug($this->MenuModel->verificarAccesoMenuCRUD()); ?>
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new div-Listar">
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Tipo</label>
    		  				<select id="cbo-Filtro_TiposDocumento" class="form-control"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Serie</label>
    		  				<select id="cbo-Filtro_SeriesDocumento" class="form-control"></select>
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Número</label>
                  <input type="tel" id="txt-Filtro_NumeroDocumento" class="form-control input-number" maxlength="20" placeholder="Buscar" value="" autocomplete="off">
                </div>
              </div>
              
              <div class="col-xs-6 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Estado</label>
    		  				<select id="cbo-Filtro_Estado" class="form-control">
    		  				  <option value="" selected>Todos</option>
        				    <option value="6">Completado</option>
        				    <option value="7">Anulado</option>
        				  </select>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="form-group">
                  <label>Cliente</label>
                  <input type="text" id="txt-Filtro_Entidad" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre / # documento de identidad" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <button type="button" id="btn-filter" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                </div>
              </div>
              
              <div class="col-xs-6 col-md-2">
                <div class="form-group">
                  <label>&nbsp;</label>
                  <?php if ($this->MenuModel->verificarAccesoMenuCRUD()->Nu_Agregar == 1) : ?>
                    <button type="button" class="btn btn-success btn-block" onclick="agregarCompra()"><i class="fa fa-plus-circle"></i> Agregar</button>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="table-responsive div-Listar">
            <table id="table-Compra" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="no-sort_left">F. Emisión</th>
                  <th class="no-sort">Tipo</th>
                  <th class="no-sort">Serie</th>
                  <th class="no-sort_right">Número</th>
                  <th class="no-sort_left">Cliente</th>
                  <th class="no-sort">M</th>
                  <th class="no-sort_right">Total</th>
                  <th class="no-sort">Placa</th>
                  <th class="no-sort">OI</th>
                  <th class="no-sort">Estado</th>
                  <th class="no-sort">Activo</th>
                  <th class="no-sort"></th>
                  <th class="no-sort"></th>
                  <th class="no-sort"></th>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.box-body -->
          
          <div class="box-body div-AgregarEditar">
            <?php
            $attributes = array('id' => 'form-Compra');
            echo form_open('', $attributes);
            ?>
          	  <input type="hidden" id="txt-EID_Guia_Cabecera" name="EID_Guia_Cabecera" class="form-control">
          	  <input type="hidden" id="txt-ENu_Estado" name="ENu_Estado" class="form-control">
			  
      			  <div class="row">
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-book"></i> <b>Documento</b></div>
                    <div class="panel-body">
                      <div class="col-xs-12 col-sm-12 col-md-12 div-almacen">
                        <div class="form-group">
                          <label>Almacen</label>
                          <select id="cbo-almacen" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="form-group">
                          <label>Tipo <span class="label-advertencia">*</span></label>
                          <select id="cbo-TiposDocumento" name="ID_Tipo_Documento" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    
                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="form-group">
                          <label>Serie <span class="label-advertencia">*</span></label>
                          <select id="cbo-SeriesDocumento" name="ID_Serie_Documento" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="form-group">
                          <label>Número <span class="label-advertencia">*</span></label>
                            <input type="tel" id="txt-ID_Numero_Documento" name="ID_Numero_Documento" class="form-control required input-number" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="form-group">
                          <label>F. Emisión <span class="label-advertencia">*</span></label>
                          <div class="input-group date">
                            <input type="text" id="txt-Fe_Emision" name="Fe_Emision" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                                            
                      <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="form-group">
                          <label>Moneda <span class="label-advertencia">*</span></label>
                          <select id="cbo-Monedas" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-4 div-stock">
                        <div class="form-group">
                          <label>¿Descargar Stock?</label>
                          <select id="cbo-descargar_stock" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-7 col-md-7">
                        <label>Tipo de Movimiento <span class="label-advertencia">*</span></label>
                        <div class="form-group">
            		  				<select id="cbo-tipo_movimiento" name="ID_Tipo_Movimiento" class="form-control required"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-5 col-md-5 hide">
                        <label>Tipo Mantenimiento <span class="label-advertencia">*</span></label>
                        <div class="form-group">
            		  				<select id="cbo-tipo_mantenimiento" name="Nu_Tipo_Mantenimiento" class="form-control required"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="col-sm-12 col-md-6">
        			    <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-user"></i> <b>Cliente</b></div>
                    <div class="panel-body">
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_existente" class="flat-red" value="0"> Existente</label>
                      </div>
                      
                      <div class="col-xs-6 text-center">
                        <label style="cursor: pointer;"><input type="radio" name="addCliente" id="radio-cliente_nuevo" class="flat-red" value="1"> Nuevo</label>
                      </div>
                      
                      <div class="col-xs-12 col-md-12 div-cliente_existente">
                        <br>
                        <div class="form-group">
                          <label>Cliente <span class="label-advertencia">*</span></label>
                          <input type="hidden" id="txt-AID" name="AID" class="form-control required">
                          <input type="text" id="txt-ANombre" name="ANombre" class="form-control autocompletar" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-7 div-cliente_existente">
                        <div class="form-group">
                          <label>Número Documento Identidad</label>
                          <input type="text" id="txt-ACodigo" name="ACodigo" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                            
                      <div class="col-xs-12 col-md-12" style="display:none">
                        <div class="form-group">
                          <label>Dirección <span class="label-advertencia">*</span></label>
                          <input type="text" id="txt-Txt_Direccion_Entidad" name="Txt_Direccion_Entidad" class="form-control" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <!-- Cliente Nuevo -->
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label>Tipo Doc. Identidad <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-TiposDocumentoIdentidadCliente" name="ID_Tipo_Documento_Identidad" class="form-control required" style="width: 100%;"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-6 col-md-6 div-cliente_nuevo">
                        <br>
                        <div class="form-group">
                          <label id="label-Nombre_Documento_Identidad_Cliente">DNI</label></span>
                          <input type="tel" id="txt-Nu_Documento_Identidad_Cliente" name="Nu_Documento_Identidad_Cliente" class="form-control input-Mayuscula input-codigo_barra" placeholder="Ingresar número" value="" autocomplete="off" maxlength="8">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-12 col-md-2 text-center div-cliente_nuevo">
                        <label>Api</label>
                        <div class="form-group">
                          <button type="button" id="btn-cloud-api_compra_cliente" class="btn btn-success btn-block btn-md"><i class="fa fa-cloud-download fa-lg"></i></button>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
              
                      <div class="col-xs-12 col-md-10 div-cliente_nuevo">
                        <div class="form-group">
                          <label id="label-No_Entidad_Cliente">Nombre(s) y Apellidos</label><span class="label-advertencia"> *</span>
                          <input type="text" id="txt-No_Entidad_Cliente" name="No_Entidad_Cliente" class="form-control required" placeholder="Ingresar nombre" value="" autocomplete="off" maxlength="100">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-md-9 div-cliente_nuevo">
                        <div class="form-group">
                          <label>Dirección</label>
                          <input type="text" id="txt-Txt_Direccion_Entidad_Cliente" name="Txt_Direccion_Entidad_Cliente" class="form-control" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-md-3 div-cliente_nuevo">
                        <div class="form-group estado">
                          <label>Estado <span class="label-advertencia">*</span></label>
            		  				<select id="cbo-Estado" name="Nu_Estado" class="form-control required">
            		  				  <option value="1">Activo</option>
            		  				  <option value="0">Inactivo</option>
            		  				</select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-3 col-md-4" style="display:none">
                        <div class="form-group">
                          <label>Telefono</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Telefono_Entidad_Cliente" name="Nu_Telefono_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 9999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      
                      <div class="col-xs-12 col-sm-4 col-md-5" style="display:none">
                        <div class="form-group">
                          <label>Celular</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input type="tel" id="txt-Nu_Celular_Entidad_Cliente"  name="Nu_Celular_Entidad_Cliente" class="form-control" data-inputmask="'mask': ['999 999 999']" data-mask autocomplete="off">
                          </div>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                      <!-- /. Cliente Nuevo -->
                    </div>
                  </div>
                </div><!-- /. Cliente -->
                
                <!-- Flete -->
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="row">
                        <div class="col-xs-4 col-sm-2 col-md-1 text-left">
                          <b>Flete</b>
                        </div>

                        <div class="col-xs-4 col-sm-2 col-md-1 text-left div-flete" data-estado="1">
                          <label style="cursor: pointer;"><input type="radio" name="radio-addFlete" id="radio-flete_si" class="flat-red" value="1"> Si</label>
                        </div>
                      
                        <div class="col-xs-4 col-sm-3 col-md-1 text-left div-flete" data-estado="0">
                          <label style="cursor: pointer;"><input type="radio" name="radio-addFlete" id="radio-flete_no" class="flat-red" value="0"> No</label>
                        </div>
                      </div>
                    </div>

                    <div class="panel-body" id="div-addFlete">
                      <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                          <label>Transportista <span class="label-advertencia">*</span></label>
                          <input type="hidden" id="txt-AID_Transportista" name="AID_Transportista" class="form-control required">
                          <input type="text" id="txt-ANombre_Transportista" name="ANombre_Transportista" class="form-control autocompletar_transportista" data-global-class_method="AutocompleteController/getAllClient" data-global-table="entidad" placeholder="Ingresar nombre / nro. de documento identidad" value="" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-2">
                        <label>Placa <span class="label-advertencia">*</span></label>
                        <div class="form-group">
                          <input type="text" id="txt-No_Placa" name="No_Placa" class="form-control required input-Mayuscula input-codigo_barra" maxlength="6" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-2">
                        <label>F. Traslado</label>
                        <div class="form-group">
                          <input type="text" name="Fe_Traslado" class="form-control date-picker-invoice required" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <label>Motivo traslado <span class="label-advertencia">*</span></label>
                        <div class="form-group">
            		  				<select id="cbo-motivo_traslado" name="ID_Motivo_Traslado" class="form-control required"></select>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-2 col-md-2 div-cliente_existente">
                        <div class="form-group">
                          <label>Nro. Doc. Identidad</label>
                          <input type="text" id="txt-ACodigo_Transportista" name="ACodigo" class="form-control required" disabled>
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-4 col-md-4">
                        <label>Licencia</label>
                        <div class="form-group">
                          <input type="tel" id="txt-No_Licencia" name="No_Licencia" class="form-control input-number" maxlength="10" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-6">
                        <label>Certificado de Inscripción</label>
                        <div class="form-group">
                          <input type="text" id="txt-No_Certificado_Inscripcion" name="No_Certificado_Inscripcion" class="form-control input-codigo_barra" autocomplete="off">
                          <span class="help-block" id="error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./ Flete -->
              </div><!-- ./ Cabecera -->

      			  <div class="row">
                <div class="col-md-12">
        			    <div id="panel-DetalleProductos" class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-shopping-cart"></i> <b>Detalle</b></div>
                    <div class="panel-body">
      			          <div class="row">
          	            <input type="hidden" name="Nu_Tipo_Lista_Precio" value="2" class="form-control"><!-- 2 = Compra -->
                        <div class="col-xs-12">
                          <label>Lista de Precio <span class="label-advertencia">*</span></label>
                          <div class="form-group">
                            <select id="cbo-lista_precios" class="form-control required" style="width: 100%;"></select>
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-9">
                          <div class="form-group">
                            <label>Producto / Servicio <span class="label-advertencia">*</span></label>
                            <input type="hidden" id="txt-Nu_Tipo_Registro" class="form-control" value="1"><!-- Compra -->
                            <input type="hidden" id="txt-Nu_Compuesto" class="form-control" value="0">
                            <input type="hidden" id="txt-ID_Producto" class="form-control">
                            <input type="hidden" id="txt-Nu_Codigo_Barra" class="form-control">
                            <input type="hidden" id="txt-Ss_Precio" class="form-control">
                            <input type="hidden" id="txt-ID_Impuesto_Cruce_Documento" class="form-control">
                            <input type="hidden" id="txt-Nu_Tipo_Impuesto" class="form-control">
                            <input type="hidden" id="txt-Ss_Impuesto" class="form-control">
                            <input type="hidden" id="txt-Qt_Producto" class="form-control">
                            <input type="hidden" id="txt-nu_tipo_item" class="form-control">
                            <input type="text" id="txt-No_Producto" class="form-control autocompletar_detalle" data-global-class_method="AutocompleteController/getAllProduct" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off">
                            <span class="help-block" id="error"></span>
                          </div>
                        </div>
                        
                        <div class="col-xs-12 col-md-3">
                          <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="button" id="btn-addProductoCompra" class="btn btn-success btn-md btn-block"><i class="fa fa-plus-circle"></i> Agregar Item Detalle</button>
                          </div>
                        </div>
                      </div>
                      
      			          <div class="row">
                      <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="table-DetalleProductos" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th style="display:none;" class="text-left"></th>
                              <th class="text-center" style="width: 7%;">Cantidad</th>
                              <th class="text-center" style="width: 20%;">Item</th>
                              <th class="text-center" style="width: 8%;">Precio</th>
                              <th class="text-center">Impuesto</th>
                              <th class="text-center" style="display:none;">Sub Total</th>
                              <th class="text-center" style="width: 8%;">% Dscto</th>
                              <th class="text-center" style="width: 12%;">Total</th>
                              <th class="text-center">Código Interno</th><!-- Nro. Lote -->
                              <th class="text-center" style="width: 12%;">F. Depreciación</th><!-- F. Vcto. Lote-->
                              <th class="text-center">Area Ingreso</th><!-- Nro. Lote -->
                              <th class="text-center">Estado</th><!-- Nro. Lote -->
                              <th class="text-center"></th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div><!-- ./Detalle -->
              </div>
              
      			  <div class="row div-Glosa">
      			    <div class="col-md-12">
      			      <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-comment-o"></i> <b>Observaciones</b></div>
                    <div class="panel-body">
                      <div class="col-md-12">
                        <textarea name="Txt_Glosa" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
      			  <div class="row">
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="button" id="btn-cancelar" class="btn btn-danger btn-md btn-block"><span class="fa fa-close"></span> Cancelar (ESC)</button>
                  </div>
                </div>
                <div class="col-xs-6 col-md-6">
                  <div class="form-group">
                    <button type="submit" id="btn-save" class="btn btn-success btn-md btn-block btn-verificar"><i class="fa fa-save"></i> Guardar (ENTER)</button>
                  </div>
                </div>
              </div>
                
      			  <div class="row" style="display:none"><!-- Totales -->
      			    <div class="col-md-8 div-total"></div>
                <div class="col-md-4 div-total">
      			      <div class="panel panel-default">
                  <div class="panel-heading"><i class="fa fa-money"></i> <b>Totales</b></div>
                  <div class="panel-body">
                    <table class="table" id="table-CompraTotal">
                      <tr>
                        <td><label>% Descuento</label></td>
                        <td class="text-right">
    	  					        <input type="tel" class="form-control input-decimal" id="txt-Ss_Descuento" name="Ss_Descuento" size="3" value="" autocomplete="off" />
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Gravadas</label></td>
                        <td class="text-right">
    	  					        <input type="hidden" class="form-control" id="txt-subTotal" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-subTotal">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Inafectas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-inafecto" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-inafecto">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>OP. Exoneradas</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-exonerada" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-exonerada">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Gratuita</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-gratuita" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-gratuita">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Descuento Total (-)</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-descuento" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-descuento">0.00</span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>I.G.V. %</label></td>
                        <td class="text-right">
                            <input type="hidden" class="form-control" id="txt-impuesto" value="0.00"/>
                            <span class="span-signo"></span> <span id="span-impuesto">0.00</span>
                        </td>
                      </tr>
                        
                      <tr>
                        <td><label>Percepción</label></td>
                        <td class="text-right">
    	  					        <input type="tel" class="form-control input-decimal" id="txt-Ss_Percepcion" name="Ss_Percepcion" size="3" value="" autocomplete="off" />
                        </td>
                      </tr>
                      
                      <tr>
                        <td><label>Total</label></td>
                        <td class="text-right">
                          <input type="hidden" class="form-control" id="txt-total" value="0.00"/>
                          <span class="span-signo"></span> <span id="span-total">0.00</span>
                        </td>
                      </tr>
                    </table><!-- ./Totales -->
                  </div>
                </div>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <?php
    $attributes = array('id' => 'form-addActivo');
    echo form_open('', $attributes);
    ?>  
    <div class="modal fade modal-modal_activo">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="text-center">Agregar activo</h4>
          </div>

          <div class="modal-body">
            <input type="hidden" id="EID_Empresa_ModalActivo" name="EID_Empresa_ModalActivo" class="form-control">
            <input type="hidden" id="EID_Organizacion_ModalActivo" name="EID_Organizacion_ModalActivo" class="form-control">
            <input type="hidden" id="EID_Almacen_ModalActivo" name="EID_Almacen_ModalActivo" class="form-control">
            <input type="hidden" id="EID_Guia_Cabecera_ModalActivo" name="EID_Guia_Cabecera_ModalActivo" class="form-control">

            <div class="row">
              <div class="col-xs-12 col-md-12">
                <h4 id="labelDocumentoModalAcitvo" class="text-center"></h4>
              <br>
              </div>
              <div class="col-xs-12 col-md-4">
                <label>Producto</label>
                <div class="form-group">
                  <select id="cbo-ProductoDetalle" class="form-control select2" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-12 col-md-2">
                <label>F. Salida</label>
                <div class="form-group">
                  <input type="text" class="pos-input txt-Fe_Salida_Item form-control" placeholder="Opcional" value="" autocomplete="off">
                </div>
              </div>

              <div class="col-xs-12 col-md-2">
                <label>Area Ingreso</label>
                <div class="form-group">
                  <select id="cbo-AreaIngresoDetalle" class="form-control select2" style="width: 100%;"></select>    
                  <span class="help-block" id="error"></span>
                </div>
              </div>
                  <!--  M022 - INICIO --> 
                  <div class="col-xs-12 col-md-2"> 
                  <label>Tipo Activo</label> 
                  <div class="form-group"> 
                    <select id="cbo-TipoActivoDetalle" class="form-control required" style="width: 100%;"> 
                      <option value="2">Mayor</option> 
                      <option value="1">Menor</option> 
                    </select> 
                  </div> 
                </div> 
                <!--  M022 - FIN --> 

              <div class="col-xs-12 col-md-2">
                <label>Estado</label>
                <div class="form-group">
                  <select id="cbo-ActivoDetalle" class="form-control required" style="width: 100%;">
                    <option value="1">Activo</option>
                    <option value="0">Baja</option>
                  </select>
                </div>
              </div>


                  <!--  M021 - INICIO -->  
                  
                  <div class="col-xs-12 col-md-4">
                  <label>Cliente</label>
                  <div class="form-group">
                    <select id="cbo-ClienteDetalle_1" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-md-4">
                  <label> Receptor</label>
                  <div class="form-group">
                    <select id="cbo-ClienteDetalle_2" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>

                <div class="col-xs-12 col-md-4">
                  <label>Solicitante Cambio</label>
                  <div class="form-group">
                    <select id="cbo-ClienteDetalle_3" class="form-control select2" style="width: 100%;"></select>
                    <span class="help-block" id="error"></span>
                  </div>
                </div>


                <!--  M021 - FIN --> 


              <div class="col-xs-12 col-md-2">
                <label>&nbsp;</label>
                <div class="form-group">
                  <button type="button" id="btn-addProductoDetalle" class="btn btn-success btn-md btn-block">Agregar</button>
                </div>
              </div>
            </div><!--row-->

            <div class="row">
              <div class="col-xs-12 col-md-12">                    
                <div class="table-responsive div-detalleActivo">
                  <table id="table-detalleActivo" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th style='display:none;' class="text-left">ID</th>
                        <th class="text-left">Item</th>
                        <th class="text-left">F. Salida</th>
                        <th class="text-left">Area de Ingreso</th>

                <!--  M022 - INICIO --> 
                <th class="text-left">Tipo de Activo</th> 
                <!--  M022 - FIN --> 

                        <th class="text-left">Estado</th>

                <!--  M021 - INICIO --> 
                <th class="text-left">Cliente</th> 
                <th class="text-left">Receptor</th> 
                <th class="text-left">Solicitante Cambio</th> 
                <!--  M021 - FIN --> 

                        <th class="text-center"></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div><!--row-->
          </div><!--modal-body-->
            
          <div class="modal-footer">
            <div class="col-xs-6">
              <button type="button" id="btn-modal-salir" class="btn btn-danger btn-md btn-block pull-center" data-dismiss="modal">Cancelar</button>
            </div>
            <div class="col-xs-6">
              <button type="button" id="btn-modal-add_activo" class="btn btn-primary btn-md btn-block pull-center">Guardar</button>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div><!-- /.modal crear item -->
    <?php echo form_close(); ?>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->