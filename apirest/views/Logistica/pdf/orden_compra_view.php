<!DOCTYPE html>
<html>
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>laesystems</title>
      <style type=text/css>  
        .bold {
          font-weight: bold;
        }
        .center {
          text-align: center;
          vertical-align: middle;
        }
        .border-top {
          border-top: 0.5px solid black;
        }
        .border-bottom {
          border-bottom: 0.5px solid black;
        }
        .border-left {
          border-left: 0.5px solid black;
        }
        .border-right {
          border-right: 0.5px solid black;
        }
        .border-all {
          border: 1px solid black;
        }
        .text-left {
          text-align: left;
        }
        .text-center {
          text-align: center;
        }
        .text-right {
          text-align: right;
        }
      </style>
    </head>
    <body>
        <table cellpadding="4">
          <tbody>
            <tr>
            <td colspan="3" class="center bold border-left border-right border-top" style="font-family: 'Times New Roman'; font-size: 9px">MAFISA MOTORS S.A.C.- <span style = "color:red;"><?= $arrData[0]->Estado_Documento == '1' ? '' : 'Anulado'; ?></span></td>
            </tr>
            <tr>
              <td colspan="8" class="center border-left border-right">AV. NICOLAS ARRIOLA Nº 3050</td>
            </tr>
          </tbody>
        </table>
        <table cellpadding="1">
          <tbody>
            <tr>
              <td class="border-left border-right" colspan="8"></td>
            </tr>
            <tr>
              <td class="bold border-left" colspan="2"> GIRAR FACTURA A NOMBRE:</td>
              <td class="" colspan="2">MAFISA MOTORS S.A.C.</td>
              <td class=""></td>
              <td class="center border-top border-left border-right" colspan="3">Nº OC - <?= $arrData[0]->ID_Numero_Documento; ?></td>
            </tr>
            <tr>
              <td class="bold border-left" colspan="2"> RUC:</td>
              <td class="" colspan="2">20552846363</td>
              <td class=""></td>
              <td class="bold border-top border-left" colspan="2"> Fecha de emisión:</td>
              <td class="center border-top border-right border-left"><?= ToDateBD($arrData[0]->Fe_Emision); ?></td>
            </tr>
            <tr>
              <td class="bold border-left" colspan="2"> AREA SOLICITANTE:</td>            
              <!-- 08_08  - INICIO -->
              <td class="" colspan="2"><?= $arrData[0]->No_Grupo_CP; ?></td>
              <!-- td class="" colspan="2"><= strtoupper($user->No_Grupo); ?></td-->            
              <!-- 08_08  - FIN -->
              <td class=""></td>
              <td class="bold border-top border-left" colspan="2"> Nº OI</td>              
              <!-- OC_MODAL_8 - INICIO -->
              <td class="center border-top border-right border-left"><?= $arrData[0]->Serie_OI .'-' .$arrData[0]->Num_Serie_OI ; ?></td>
              <!-- td class="center border-top border-right border-left"><= $arrData[0]->Nu_Orden_Ingreso; ?></td -->
              <!-- OC_MODAL_8 - FIN -->
            </tr>
            <tr>
              <td class="bold border-left" colspan="2"> RESPONSABLE:</td>
              <!-- OC_MODAL_8 - INICIO -->
              <td class="" colspan="2"><?= $arrData[0]->No_Nombres_Apellidos; ?></td>
              <!-- td class="" colspan="2"><= $user->No_Nombres_Apellidos; ?></td -->
              <!-- OC_MODAL_8 - FIN -->
              <td class=""></td>
              <td class="bold border-top border-left" colspan="2"> Nº Presupuesto</td>              
              <!-- OC_MODAL_8 - INICIO -->
              <td class="center border-top border-right border-left"><?= $arrData[0]->Serie_Presup .'-'. $arrData[0]->Num_Serie_Presup ; ?></td>
              <!-- td class="center border-top border-right border-left"><= $arrData[0]->Nu_Presupuesto; ?></td -->
              <!-- OC_MODAL_8 - FIN -->
            </tr>
            <tr>
              <td class="bold border-left" colspan="2"> TELEFONO RESPONSABLE:</td>
              <!-- OC_MODAL_8 - INICIO -->
              <td class="" colspan="2"><?= $arrData[0]->Nu_Celular; ?></td>
              <!-- td class="" colspan="2"><= $user->Nu_Celular; ?></td -->
              <!-- OC_MODAL_8 - FIN -->
              <td class=""></td>
              <td class="center border-top border-left border-right border-bottom" colspan="3" rowspan="2">Este número debe aparecer en todas las guías y facturas que tengan relación con esta orden <br></td>
            </tr>
            <tr>
              <td class="border-left" colspan="2"></td>
              <td class="" colspan="2"></td>
              <td class=""></td>
            </tr>
            <tr>
              <td class="border-bottom border-left border-right" colspan="8"></td>
            </tr>
          </tbody>
        </table>
        <table cellpadding="1">
          <tbody>
          <tr>
            <td class="bold border-left"> Proveedor:</td>
            <td class="border-right" colspan="7"><?= strtoupper($arrData[0]->No_Entidad); ?></td>
          </tr>
          <tr>
            <td class="bold border-left"> Dirección</td>
            <td class="border-right" colspan="7"><?= strtoupper($arrData[0]->Txt_Direccion_Entidad); ?></td>
          </tr>
          <tr>
            <td class="bold border-left"> RUC:</td>
            <td class="border-right" colspan="7"><?= $arrData[0]->Nu_Documento_Identidad; ?></td>
          </tr>
          <tr>
            <td class="bold border-left"> Telefono:</td>
            <!-- OC_MODAL_8 - INICIO -->
            <td class="border-right" colspan="7"><?= $arrData[0]->Nu_Celular_Contacto; ?></td>
            <!-- td class="border-right" colspan="7"><= $arrData[0]->Nu_Telefono_Entidad; ?></td -->
              <!-- OC_MODAL_8 - FIN -->
          </tr>
          <tr>
            <td class="bold border-left"> Nombre:</td>
            <td class="border-right" colspan="7"><?= $arrData[0]->No_Contacto; ?></td>
          </tr>
          <tr>
            <td class="border-left border-right" colspan="8"></td>
          </tr>
          <tr>
            <td class="bold border-left border-right" colspan="8"> Sirvase emitir factura de compra por la siguiente descripcion:</td>
          </tr>
          <tr>
            <td class="border-left border-right" colspan="8"></td>
          </tr>
          </tbody>
        </table>
        <table>
          <tbody>
            <tr style="width: 100%;">
              <td class="border-left" style="width: 12%;"></td>
              <td style="width: 8%;">SERVICIO</td>
              <td class="border-all center" style="width: 5%;"><?= $arrData[0]->ServicioBien == '1' ? "X" : ""; ?></td>
              <td style="width: 2%;"></td>
              <td style="width: 8%;">BIEN</td>
              <td class="border-all center" style="width: 5%;"><?= $arrData[0]->ServicioBien == '0' ? "X" : ""; ?></td>
              <td colspan="7" class="border-right" style="width: 60%;"></td>
            </tr>
            <tr>
              <td colspan="13" class="border-left border-right"></td>
            </tr>
            <tr>
              <td class="border-left" style="width: 12%;"> Datos del vehículo:</td>
              <td style="width: 8%;">MARCA</td>
              <td class="border-all center" style="width: 10%;"><?= $arrData[0]->PL_No_Marca_Vehiculo; ?></td>
              <td style="width: 2%;"></td>
              <td style="width: 8%;">MODELO</td>
              <td class="border-all center" style="width: 10%;"><?= $arrData[0]->PL_No_Modelo_Vehiculo; ?></td>
              <td style="width: 2%;"></td>
              <td style="width: 8%;">PLACA</td>
              <td class="border-all center" style="width: 10%;"><?= $arrData[0]->PL_No_Placa_Vehiculo; ?></td>
              <td style="width: 2%;"></td>
              <td style="width: 8%;">AÑO</td>
              <td class="border-all center" style="width: 10%;"><?= $arrData[0]->PL_No_Year_Vehiculo; ?></td>
              <td class="border-right" style="width: 10%;"></td>
            </tr>
            <tr>
              <td colspan="13" class="border-left border-right"></td>
            </tr>
            <tr>
              <td colspan="13" class="border-left border-right"></td>
            </tr>
          </tbody>
        </table>
        <table cellpadding="1">
          <thead>
            <tr>
              <th class="bold center border-left border-top border-bottom" style="width: 5%;">ITEM</th>
              <th class="bold center border-left border-top border-bottom" colspan="3" style="width: 52%;">Descripción</th>
              <th class="bold center border-left border-top border-bottom" style="width: 10%;">Código</th>
              <th class="bold center border-left border-top border-bottom" style="width: 7%;">Cantidad</th>
              <th class="bold center border-left border-top border-bottom" colspan="2" style="width: 13%;">Prec Unitario</th>
              <th class="bold center border-left border-top border-right border-bottom" colspan="2" style="width: 13%;">Monto Total</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $Ss_Gravada = 0.00;
            $Ss_Exonerada = 0.00;
            $Ss_Inafecto = 0.00;
            $Ss_Gratuita = 0.00;
            $Ss_IGV = 0.00;
            $Ss_Total = 0.00;           
            $iCounter = 1; 
            $sNameClassTrDetalle = '';
            foreach($arrData as $row) {
              if ($row->Nu_Tipo_Impuesto == 1){//IGV
                $Ss_IGV += $row->Ss_Impuesto_Producto;
                $Ss_Gravada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
                $Ss_Inafecto += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
                $Ss_Exonerada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
                $Ss_Gratuita += $row->Ss_SubTotal_Producto;
              }
              $Ss_Descuento = floatval($row->Ss_Descuento);
              $Ss_Total += $row->Ss_Total_Producto;
            ?>
              <tr>
                <td class="center border-left border-bottom" style="width: 5%;"><?= $iCounter ?></td>
                <td class="center border-left border-bottom" colspan="3" style="width: 52%;"><?= $row->No_Producto; ?></td>
                <td class="center border-left border-bottom" style="width: 10%;"><?= $row->Nu_Codigo_Barra; ?></td>
                <td class="center border-left border-bottom" style="width: 7%;"><?= intval($row->Qt_Producto) ?></td>
                <td class="center border-left border-bottom text-left" style="width: 2%;"> S/</td>
                <td class="center border-bottom text-right" style="width: 11%;"><?= numberFormat($row->Ss_Precio, 2, '.', ',') ?>&nbsp;&nbsp;</td>
                <td class="center border-left border-bottom text-left" style="width: 2%;"> S/</td>
                <td class="center border-bottom border-right text-right" style="width: 11%;"><?= numberFormat($row->Ss_Total_Producto, 2, '.', ',') ?>&nbsp;&nbsp;</td>
              </tr>
              <?php
              $iCounter++;
            } ?>
            <?php
              while ($iCounter < 16) {
            ?>
              <tr>
                <td class="center border-left border-bottom"></td>
                <td class="center border-left border-bottom" colspan="3"></td>
                <td class="border-left border-bottom"></td>
                <td class="center border-left border-bottom"></td>
                <td class="center border-left border-bottom text-left"></td>
                <td class="center border-bottom text-right"></td>
                <td class="center border-left border-bottom text-left"></td>
                <td class="center border-bottom border-right text-right"></td>
              </tr>
            <?php
                $iCounter++;
              }
            ?>
            <tr>
              <td class="border-left border-right" colspan="10"></td>
            </tr>
            <tr>
              <td class="border-left" colspan="6"></td>
              <td class="" colspan="2">Sub. Total</td>
              <td class="center border-left border-top text-left"> S/</td>
              <td class="border-right border-top text-right"><?= numberFormat($Ss_Gravada, 2, '.', ','); ?>&nbsp;&nbsp;</td>
            </tr>
            <tr>
              <td class="border-left" colspan="6"></td>
              <td colspan="2" style="color: red;">Descuento</td>
              <td class="center border-left border-top"> <?= ($Ss_Descuento > 0) ? "S/" : ""; ?></td>
              <td class="border-right border-top text-right"><?= ($Ss_Descuento > 0) ? numberFormat($row->Ss_Descuento, 2, '.', ',') : ""; ?>&nbsp;&nbsp;</td>
            </tr>
            <tr>
              <td class="border-left" colspan="6"></td>
              <td colspan="2">Neto</td>
              <td class="center border-left border-top"></td>
              <td class="border-right border-top text-right"></td>
            </tr>
            <tr>
              <td class="border-left" colspan="6"></td>
              <td colspan="2">IGV</td>
              <td class="center border-left border-top text-left"> S/</td>
              <td class="border-right border-top text-right"><?= numberFormat($Ss_IGV, 2, '.', ','); ?>&nbsp;&nbsp;</td>
            </tr>
            <tr>
              <td class="border-left" colspan="6"></td>
              <td colspan="2" class="text-right">Total General&nbsp;&nbsp;</td>
              <td class="center border-left border-top border-bottom text-left"> S/</td>
              <td class="border-right border-top border-bottom text-right"><?= numberFormat($Ss_Total, 2, '.', ','); ?>&nbsp;&nbsp;</td>
            </tr>
            <tr>
              <td class="border-bottom border-right border-left" colspan="10"> Son: <?= strtoupper($totalEnLetras); ?></td>
            </tr>
            <tr>
              <td class="border-bottom border-right border-left" colspan="10"> <strong>Tipo de pago</strong>: Factura <?= $arrData[0]->Nu_Dias_Credito?> dias</td>
            </tr>
            <tr>
              <td class="border-bottom border-right border-left" colspan="10" rowspan="4">
Para bienes y Servicios : <br>
Sírvase entregarnos la Mercadería en conjunto con: FACTURA ORIGINAL, COPIA DE LA ORDEN DE COMPRA y GUÍA DE REMISIÓN en nuestras oficinas en: AV. NICOLAS ARRIOLA N° 3050 - SAN LUIS - LIMA en el horario de 9:00 am 12:30 pm y 2:00 pm a 5:00 pm de lunes a viernes y de 9:00 am a 12:00 pm Sábados. <br>
<strong>RECEPCIÓN FACTURAS: </strong><br>
El horario establecido de atención para el registro de facturas de proveedores:  Lunes a miércoles de 9:00 am 12:00 pm y 2:00 pm a 4:00 pm
En ambos casos seguir los lineamientos del procedimiento MM-PR-003 (Solicitr al responsable de pago de proveedores) <br>
<strong>IMPORTANTE: </strong><br>
Mafisa Motors S.A.C  se reserva el derecho de dejar sin efecto la orden de compra si el proveedor no cumpliese con el plazo de entrega, o las mercaderías no correspondiesen a lo acordado en esta orden de compra, sin indemnización de gasto alguno. <br>
Toda factura deberá estar acompañada de una copia de orden de compra cuyo número deberá indicarse en la factura y Guía de Remisión. <br>
Estos documentos no podrán amparar más de una factura. <br>
El plazo de pago de la factura se contará desde la fecha de aceptación de la misma (procedimiento MM-PR-003). <br>
Mafisa Motors S.A.C. pagará la(s) factura (s) correspondiente a la presente orden de compra/contrato en plazo establecido con las condiciones de pago, reservándose el derecho de ejercer en el mismo plazo la facultad de reclamar contra el contenido de la factura. <br>
<br>
Consulta de pagos proveedores:  finanzas@mafisamotors.com.pe TlF: 970 892 775 <br>
<br>
              </td>
            </tr>
          </tbody>
        </table>
        <table cellpadding="6">
          <tbody>
            <tr>
              <td class="border-bottom border-left" colspan="2" rowspan="2"></td>
              <td class="border-bottom border-left" colspan="3" rowspan="2"></td>
              <td class="border-bottom border-left" colspan="2" rowspan="2"></td>
              <td class="bold border-left border-right center"></td>
            </tr>
            <tr>
              <td class="border-left border-right bold center">Nº OC - <?= $arrData[0]->ID_Numero_Documento; ?></td>
            </tr>
            <tr>
              <td class="border-bottom border-left center" colspan="2">Responsable de AREA </td>
              <td class="border-bottom border-left center" colspan="3">Autorización de compra</td>
              <td class="border-bottom border-left center" colspan="2">Conformidad del bien/servicio</td>
              <td class="border-bottom border-left border-right"></td>
            </tr>
          </tbody>
        </table>
    </body>
</html>