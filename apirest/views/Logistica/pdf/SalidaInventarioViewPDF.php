<!DOCTYPE html>
<html>
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>laesystems</title>
      <style type=text/css>
        .table_pdf {
          width: 100%;
        }
    
        .tr-thead_border th{
          font-size: 5px;
          border: solid 1px #000000;
        }
        
        .tr-thead th.title {
          color: #000000;
          font-size: 20px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-thead th.sub_title_fecha {
          color: #000000;
          font-size: 11px;
          font-family: Arial;
        }
        
        .tr-thead th.sub_title_nro {
          color: #000000;
          font-size: 11px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-sub_thead th.title {
          color: #000000;
          font-size: 12px;
          font-family: Arial;
          font-weight: bold;
        }

        .tr-sub_thead th.content {
          color: #000000;
          font-size: 10.5px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_nombres {
          color: #000000;
          font-size: 11px;
          font-family: Arial;
        }
        
        .tr-sub_thead th.contacto_cargo {
          color: #838585;
          font-size: 11px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_celular {
          color: #000000;
          font-size: 11px;
          font-family: Arial;
        }

        .tr-sub_thead th.contacto_empresa_correo {
          color: #34bdad;
          font-size: 11px;
          font-family: Arial;
        }
        
        .tr-theadFormat th{
          font-weight: bold;
          font-family: Arial;
        }
        
        .tr-header-detalle th{
          font-size: 11px;
          font-family: Arial;
          background-color: #F2F5F5;
        }
        
        .tr-header th{
          font-size: 11px;
          font-family: Arial;
          background-color: #F2F5F5;
        }

        .tr-footer th{
          font-size: 11px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-totales th{
          font-size: 11px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .tr-fila_impar td{
          font-size: 10.5px;
          font-family: Arial;
          background-color: #F2F5F5;
        }
        
        .tr-fila_par td{
          font-size: 10.5px;
          font-family: Arial;
          background-color: #FFFFFF;
        }
        
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
      </style>
    </head>
    <body>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-thead">
            <th class="text-left title"><br/><br/><br/>Salida de Inventario</th>
            <th class="text-center sub_title_fecha">
              <div>
                &nbsp;<br>
                <div style="border-top: 2.7px solid #34bdad">
                  &nbsp;<br>
                  Día <?php echo dateNow('dia'); ?> de <?php echo getNameMonth(dateNow('mes')); ?> del <?php echo dateNow('año'); ?>
                </div>
              </div>
            </th>
            <th class="text-right sub_title_nro">
              <div>
                &nbsp;<br>
                <div style="border-top: 2.7px solid #34bdad">
                  &nbsp;<br>
                  Nro. <?php echo $arrData[0]->No_Tipo_Documento . ' ' . $arrData[0]->ID_Serie_Documento . ' ' . $arrData[0]->ID_Numero_Documento; ?>
                </div>
              </div>
            </th>
          </tr>
        </thead>
      </table>
      <br/>
      <br/>
      <br/>
      <?php
			  $Po_IGV = "";
        foreach($arrData as $row)
          $Po_IGV = "18 %";
      ?>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-sub_thead">
            <th class="text-left title" style="width: 60%;"><b>CLIENTE</b></th>
            <th class="text-left content" style="width: 40%;"><b>FECHA EMISIÓN: </b><?php echo ToDateBD($arrData[0]->Fe_Emision); ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->No_Tipo_Documento_Identidad_Breve . ': ' .$arrData[0]->Nu_Documento_Identidad; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->No_Entidad; ?></th>
            <th class="text-left content" style="width: 40%;"><b>MONEDA: </b><?php echo $arrData[0]->No_Moneda; ?></th>
          </tr>
          <tr class="tr-sub_thead">
            <th class="text-left content" style="width: 60%;"><?php echo $arrData[0]->Txt_Direccion_Entidad; ?></th>
            <th class="text-left content" style="width: 40%;"><?php echo (!empty($Po_IGV) ? '<b>IGV: </b>' . $Po_IGV : '-'); ?></th>
          </tr>
          <?php if ($arrData[0]->vale_placa && $arrData[0]->vale_oi) { ?>
            <tr class="tr-sub_thead">
              <th class="text-left content" style="width: 100%;"><b>PLACA: </b><?= $arrData[0]->vale_placa ?></th>
            </tr>
            <tr class="tr-sub_thead">
              <th class="text-left content" style="width: 100%;"><b>ORDEN DE INGRESO: </b><?= $arrData[0]->vale_oi ?></th>
            </tr>
          <?php } ?>
        </thead>
      </table>
      <br/>
      <br/>
      <br/>
	  	<table class="table_pdf">
        <thead>
          <tr class="tr-theadFormat tr-header tr-header-detalle">
            <th class="text-center" style="width: 20%">&nbsp;<br/>CANT.<br/></th>
            <th class="text-center" style="width: 80%">&nbsp;<br/>DESCRIPCIÓN<br/></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $Ss_Gravada = 0.00;
            $Ss_Exonerada = 0.00;
            $Ss_Inafecto = 0.00;
            $Ss_Gratuita = 0.00;
            $Ss_IGV = 0.00;
            $Ss_Total = 0.00;           
            $iCounter = 0; 
            $sNameClassTrDetalle = '';
            foreach($arrData as $row) {
              $sNameClassTrDetalle = ($iCounter%2) == 0 ? 'tr-fila_par' : 'tr-fila_impar';

              if ($row->Nu_Tipo_Impuesto == 1){//IGV
                $Ss_IGV += $row->Ss_Impuesto_Producto;              
                $Ss_Gravada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
                $Ss_Inafecto += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
                $Ss_Exonerada += $row->Ss_SubTotal_Producto;
              } else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
                $Ss_Gratuita += $row->Ss_SubTotal_Producto;
              }
      
              $Ss_Total += $row->Ss_Total_Producto;
            ?>
              <tr class="<?php echo $sNameClassTrDetalle; ?>">
                <td class="text-center" style="width: 20%">&nbsp;<br/><?php echo numberFormat($row->Qt_Producto, 3, '.', ','); ?><br/></td>
                <td class="text-left" style="width: 80%">&nbsp;<br/><?php echo $row->No_Producto; ?><br/></td>
              </tr>
              <?php
              $iCounter++;
            } ?>
        </tbody>
      </table>
      <br/>
      <br/>
      <?php if ( !empty($arrData[0]->Txt_Glosa) ) { ?>
	  	<table class="table_pdf" cellpadding="4">
        <thead>
          <tr class="tr-theadFormat tr-header">
            <th class="text-left content">Glosa</th>
          </tr>
          <tr class="tr-sub_thead tr-header">
            <th class="text-left content"><?php echo $arrData[0]->Txt_Glosa; ?></th>
          </tr>
          <tr class="tr-sub_thead tr-header">
            <th class="text-left content"><br/></th>
          </tr>
        </thead>
      </table>
      <?php } ?>
    </body>
</html>