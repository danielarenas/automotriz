<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new">
            <div class="row div-Filtros">
              <br>
              <br>
              <div class="col-xs-12 col-md-12">
                <label>Libro <span class="label-advertencia">*</span></label>
                <div class="form-group">
                  <select id="cbo-TiposLibroSunatKardex" class="form-control"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
            </div>
            
            <div class="row div-Filtros">              
              <div class="col-xs-12 col-md-4">
                <div class="form-group">
                  <label>Almacen</label>
    		  				<select id="cbo-Almacenes_filtro_kardex" class="form-control select2 required" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
          
              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Inicio</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Inicio" class="form-control date-picker-report" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>

              <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="form-group">
                  <label>F. Fin</label>
                  <div class="input-group date">
                    <input type="text" id="txt-Filtro_Fe_Fin" class="form-control date-picker-invoice txt-Filtro_Fe_Fin" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                  </div>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-12 col-md-4">
                <label>Todos los productos</label>
                <div class="form-group">
        				  <select id="cbo-FiltrosProducto" class="form-control">
        				    <option value="0">Si</option>
        				    <option value="1">No</option>
        				  </select>
                </div>
              </div>

              <div class="col-xs-12 col-md-12 div-productos">
                <div class="form-group">
                  <label>Producto</label>
                  <input type="hidden" id="txt-Nu_Tipo_Registro" class="form-control" value="3"><!-- Venta -->
                  <input type="hidden" id="txt-ID_Producto" class="form-control">
                  <input type="text" id="txt-No_Producto" class="form-control autocompletar_detalle" data-global-class_method="AutocompleteController/globalAutocompleteReport" data-global-table="producto" placeholder="Ingresar nombre / código de barra / código sku" value="" autocomplete="off">
                  <span class="help-block" id="error"></span>
                </div>
              </div>
            </div>
            
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-4 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-html_kardex" class="btn btn-default btn-block btn-generar_kardex" data-type="html"><i class="fa fa-table"></i> HTML</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-pdf_kardex" class="btn btn-default btn-block btn-generar_kardex" data-type="pdf"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-excel_kardex" class="btn btn-default btn-block btn-generar_kardex" data-type="excel"><i class="fa fa-file-excel-o color_icon_excel"></i> Excel</button>
                </div>
              </div>
              
              <div class="col-xs-12 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-txt_kardex" class="btn btn-default btn-block btn-generar_kardex" data-type="txt"><i class="fa fa-files-o"></i> Libro Electrónico</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div id="div-Kardex" class="table-responsive">
            <table id="table-Kardex" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="text-center">F. Emisión</th>
                  <th class="text-center">Cod. Tipo</th>
                  <th class="text-center">Tipo</th>
                  <th class="text-center">Serie</th>
                  <th class="text-center">Número</th>
                  <th class="text-center">Operación</th>
                  <th class="text-center">Movimiento</th>
                  <th class="text-center">Nro. Doc. Identidad</th>
                  <th class="text-center">Cliente / Proveedor</th>
                  <th class="text-center">Entrada</th>
                  <th class="text-center">Salida</th>
                  <th class="text-center">Saldo Final</th>
                  <th class="text-center">Estado</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->