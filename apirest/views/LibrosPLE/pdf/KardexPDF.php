<!DOCTYPE html>
<html>
	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>laesystems</title>
        <style type="text/css">
            .table_pdf {
            width: 100%;
            }
            
            .tr-theadFormatTitle th{
            font-weight: bold;
            font-size: 9px;
            }
            
            .tr-theadFormat th{
            font-weight: bold;
            }
            
            .tr-theadFormat_header th{
            background-color: #F2F5F5;
            }
            
            .tr-theadFormat_footer th{
            background-color: #E7E7E7;
            }
            
            .tr-thead th{
            font-size: 5px;
            border: solid 0.5px #000000;
            }
            
            .text-left{text-align: left;}
            .text-center{text-align: center;}
            .text-right{text-align: right;}
        </style>
    </head>
    <body>
        <br/>
        <table class="table_pdf">
            <thead>
                <tr class="tr-theadFormat">
                    <th>FORMATO 12.1: REGISTRO DEL INVENTARIO PERMANENTE EN UNIDADES FÍSICAS- DETALLE DEL INVENTARIO PERMANENTE EN UNIDADES FÍSICAS</th>
                </tr>
            </thead>
        </table>
        <br/>
        <br/>
	  	<table class="table_pdf">
        <?php
        if ( $arrDetalle['sStatus'] == 'success' ) {
            $ID_Producto = 0;
            $counter = 0;
            $sum_Producto_Qt_Entrada = 0.00;
            $sum_Producto_Qt_Salida = 0.00;
            $sum_General_Qt_Entrada = 0.00;
            $sum_General_Qt_Salida = 0.00;
            $Qt_Producto_Saldo_Movimiento = 0.00; ?>
            <tbody>
            <?php
            $arrFechaInicio = explode('-', $arrCabecera['dInicio']);
            $fYear = $arrFechaInicio[0];
            $fMonth = $arrFechaInicio[1];
            foreach($arrDetalle['arrData'] as $row) {
                if ($ID_Producto != $row->ID_Producto) {
                    $Qt_Producto_Saldo_Movimiento = $row->Qt_Producto_Inicial;
                    if ($counter != 0) { ?>
                        <tr class="tr-theadFormat">
                            <th class="text-right" colspan="6"></th>
                            <th class="text-right">TOTALES</th>
                            <th class="text-right"><?php echo $sum_Producto_Qt_Entrada; ?></th>
                            <th class="text-right"><?php echo $sum_Producto_Qt_Salida; ?></th>
                        </tr>
                        <br><br>
                    <?php
                    }
                    ?>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">PERIODO: </td>
                        <th class="text-left" colspan="8"><?php echo $fMonth . ' ' . $fYear; ?></th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">RUC: </td>
                        <th class="text-left" colspan="8"><?php echo $this->empresa->Nu_Documento_Identidad; ?></th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL: </td>
                        <th class="text-left" colspan="8"><?php echo $this->empresa->No_Empresa; ?></th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">ESTABLECIMIENTO (1): </td>
                        <th class="text-left" colspan="8"><?php echo $arrCabecera['Txt_Direccion_Almacen']; ?></th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">CÓDIGO DE LA EXISTENCIA: </td>
                        <th class="text-left" colspan="8"><?php echo $row->TP_Sunat_Codigo; ?></th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">TIPO (TABLA 5): </td>
                        <th class="text-left" colspan="8"><?php echo $row->TP_Sunat_Nombre; ?></th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">DESCRIPCIÓN: </td>
                        <th class="text-left" colspan="8"><?php echo $row->No_Producto; ?></th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <td class="text-left" colspan="3">CÓDIGO DE LA UNIDAD DE MEDIDA (TABLA 6): </td>
                        <th class="text-left" colspan="8"><?php echo $row->UM_Sunat_Codigo; ?></th>
                    </tr>
                    <br/>
                    <tr class="tr-thead tr-theadFormat">
                        <th class="text-center" colspan="4">DOCUMENTO DE TRASLADO, COMPROBANTE DE PAGO, DOCUMENTO INTERNO O SIMILAR</th>
                        <th class="text-center" rowspan="2">TIPO DE OPERACIÓN (TABLA 12)</th>
                        <th class="text-center" rowspan="2">NRO. DOC. IDENTIDAD</th>
                        <th class="text-center" rowspan="2">CLIENTE / PROVEEDOR</th>
                        <th class="text-center" rowspan="2">ENTRADAS</th>
                        <th class="text-center" rowspan="2">SALIDAS</th>
                        <th class="text-center" rowspan="2">SALDO FINAL</th>
                        <th class="text-center" rowspan="2">ESTADO</th>
                    </tr>
                    <tr class="tr-thead tr-theadFormat">
                        <th class="text-center">FECHA</th>
                        <th class="text-center">TIPO (TABLA 10)</th>
                        <th class="text-center">SERIE</th>
                        <th class="text-center">NÚMERO</th>
                    </tr>
                    <tr class="tr-theadFormat">
                        <th class="text-right" colspan="7"></th>
                        <th class="text-right" colspan="2">SALDO INICIAL</th>
                        <th class="text-right"><?php echo numberFormat($Qt_Producto_Saldo_Movimiento, 2, '.', ''); ?></th>
                    </tr>
                <?php
                    $ID_Producto = $row->ID_Producto;
                    $sum_Producto_Qt_Entrada = 0.00;
                    $sum_Producto_Qt_Salida = 0.00;
                } // /. if producto
                ?>
                <tr class="tr-theadFormat">
                    <td class="text-center"><?php echo $row->Fe_Emision; ?></td>
                    <td class="text-center"><?php echo $row->Tipo_Documento_Sunat_Codigo; ?></td>
                    <td class="text-center"><?php echo $row->ID_Serie_Documento; ?></td>
                    <td class="text-center"><?php echo $row->ID_Numero_Documento; ?></td>
                    <td class="text-center"><?php echo $row->Tipo_Operacion_Sunat_Codigo; ?></td>
                    <td class="text-center"><?php echo $row->Nu_Documento_Identidad; ?></td>
                    <td class="text-center"><?php echo $row->No_Entidad; ?></td>
                    <?php
                    if ($row->Nu_Tipo_Movimiento == 0){ ?>
                        <td class="text-right"><?php echo numberFormat($row->Qt_Producto, 2, '.', ''); ?></td>
                        <td class="text-right">0</td>
                    <?php
                        $Qt_Producto_Saldo_Movimiento += $row->Qt_Producto;
                        $sum_Producto_Qt_Entrada += $row->Qt_Producto;
                    } else { ?>
                        <td class="text-right">0</td>
                        <td class="text-right"><?php echo numberFormat($row->Qt_Producto, 2, '.', ''); ?></td>
                    <?php
                        $Qt_Producto_Saldo_Movimiento -= $row->Qt_Producto;
                        $sum_Producto_Qt_Salida += $row->Qt_Producto;
                    }
                    ?>
                    <td class="text-right"><?php echo numberFormat($Qt_Producto_Saldo_Movimiento, 2, '.', ''); ?></td>
                    <td class="text-center"><span class="label label-<?php echo $row->No_Class_Estado; ?>"><?php echo $row->No_Estado; ?></span></td>
                </tr>
                <?php $counter++; ?>
  			<?php
            } // ./ foreach arrdata
            ?>
            </tbody>
            <tfoot>
                <tr class="tr-theadFormat">
                    <th class="text-right" colspan="7">TOTALES</th>
                    <th class="text-right"><?php echo $sum_Producto_Qt_Entrada; ?></th>
                    <th class="text-right"><?php echo $sum_Producto_Qt_Salida; ?></th>
                </tr>
            </tfoot>
            <?php
        } else { ?>
            <tbody>
                <tr>
                    <th class="text-right" colspan="12"><?php echo $arrDetalle['sMessage']; ?></th>
                </tr>
            </tbody>
            <?php
        } // /. if - else respuesta model ?>
        </table>
    </body>
</html>