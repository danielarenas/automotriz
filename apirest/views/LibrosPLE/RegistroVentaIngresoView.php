<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"></section>
  
  <!-- Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3>
            <i class="<?php echo $this->MenuModel->verificarAccesoMenuCRUD()->Txt_Css_Icons; ?>" aria-hidden="true"></i> <?php echo $this->MenuModel->verificarAccesoMenuCRUD()->No_Menu; ?>
          </h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-content">
          <!-- box-header -->
          <div class="box-header box-header-new">
            <div class="row div-Filtros">
              <br>
              <div class="col-md-7">
                <div class="form-group">
                  <label>Libro</label>
                  <select id="cbo-TiposLibroSunat" class="form-control"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-6 col-md-5">
                <div class="form-group">
                  <label>Organización</label>
    		  				<select id="cbo-organizaciones" class="form-control" style="width: 100%;"></select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Tipo Vista Boleta</label>
                  <select id="cbo-tipo_vista_venta" class="form-control">
        				    <option value="0" selected>Agrupado</option>
        				    <option value="1">Detallado</option>
                  </select>
                  <span class="help-block" id="error"></span>
                </div>
              </div>
              
              <div class="col-xs-4 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Año</label>
			            <?php echo Select('cbo-year', 'year', 'year', YearsYMD($this->empresa->Fe_Inicio_Sistema), date('Y'), true, ''); ?>
                </div>
              </div>
              
              <div class="col-xs-8 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Mes</label>
			            <?php echo Select('cbo-mes', 'valor', 'mes', Months(), date('m'), false, ''); ?>
                </div>
              </div>
            </div>

            <div class="row div-Filtros">
              <div class="col-xs-8 col-sm-4 col-md-2">
                <div class="form-group">
                  <label>Ordenar por</label>
                  <select id="cbo-ordenar" class="form-control">
                    <option value="0" selected>Fecha Emision</option>
                    <option value="1">Fecha Sistema</option>
                    <option value="2">Fecha Periodo</option>
                    <option value="3">Series</option>
                  </select>
                </div>
              </div>

              <div class="col-xs-4 col-md-3">
                <div class="form-group">
                  <label></label>
                  <button type="button" id="btn-modificar" class="btn btn-default btn-block"><i class="fa fa-table"></i> Modificar Correlativo</button>
                </div>
              </div>
            </div>
            
            <div class="row div-Filtros">
              <br>
              <div class="col-xs-4 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-html" class="btn btn-default btn-block btn-generar" data-type="html"><i class="fa fa-table"></i> HTML</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-pdf" class="btn btn-default btn-block btn-generar" data-type="pdf"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>
                </div>
              </div>
              
              <div class="col-xs-4 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-excel" class="btn btn-default btn-block btn-generar" data-type="excel"><i class="fa fa-file-excel-o color_icon_excel"></i> Excel</button>
                </div>
              </div>
              
              <div class="col-xs-12 col-md-3">
                <div class="form-group">
                  <button type="button" id="btn-txt" class="btn btn-default btn-block btn-generar" data-type="txt"><i class="fa fa-files-o"></i> Libro Electrónico</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div id="div-RegistroVentaeIngresos" class="table-responsive">
            <table id="table-RegistroVentaeIngresos" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th class="text-center">F. Emisión</th>
                  <th class="text-center">Tipo</th>
                  <th class="text-center">Serie</th>
                  <th class="text-center">Número</th>
                  <th class="text-center"># D.I</th>
                  <th class="text-center">Razon Social</th>
                  <th class="text-center">Gravadas</th>
                  <th class="text-center">Descuento (-)</th>
                  <th class="text-center">I.G.V</th>
                  <th class="text-center">Descuento I.G.V (-)</th>
                  <th class="text-center">Inafecta</th>
                  <th class="text-center">Exonerada</th>
                  <th class="text-center">Gratuita</th>
                  <th class="text-center">Exportación</th>
                  <th class="text-center">Total</th>
                  <th class="text-center">Moneda</th>
                  <th class="text-center">T.C.</th>
                  <th class="text-center">F. Emisión (Modifica)</th>
                  <th class="text-center">Tipo (Modifica)</th>
                  <th class="text-center">Serie (Modifica)</th>
                  <th class="text-center">Número (Modifica)</th>
                  <th class="text-center">Estado</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->