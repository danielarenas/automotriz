<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  
  <!-- Section Main content -->
  <section class="content">
    <!-- New box-header -->
    <div class="row">
      <div class="col-xs-12">
        <div class="div-content-header">
          <h3><i class="fa fa-home"></i> Escritorio</h3>
        </div>
      </div>
    </div>
    <!-- ./New box-header -->
    <?php
    //array_debug($this->empresa);
    //array_debug($this->session->userdata['almacen']);
    //array_debug($this->session->usuario);
    //array_debug(dateNow('prev_date_end'));
    //array_debug($this->notificaciones->cantidad_comprobantes_adicional_consumido_reseller_pse);
    $Y = dateNow('año');
    $M = dateNow('mes');
    $fecha_pago = date($Y.'-'.$M.'-'.'07', strtotime(dateNow('fecha')));
    $date1 = new DateTime(dateNow('fecha'));
    $date2 = new DateTime($fecha_pago);
    $diff = $date1->diff($date2);

    if ( $diff->days >= 2 && $diff->days <= 6 && dateNow('fecha') <= $Y.'-'.$M.'-'.'06' ) { ?>
      <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Pago de servicio:</strong> Tu pago vence el día 05 de cada mes, si no se cancela, el día 06 no se podrá vender con el sistema. <br><b>OMITIR este mensaje si ya pagó su servicio.</b><br><br><button type="button" class="btn btn-primary btn-block btn-lae_pagos">Como pagar servicio</button>
      </div>
    <?php
    }

    if ( $diff->days <= 1 ) {
      if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 && $this->empresa->Nu_Estado_Pago_Sistema == 0 ) { ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Advertencia:</strong> Las ventas se guardarán pero no serán enviadas a SUNAT por falta de pago a partir del día 06, deben de regularizar su pago o de lo contrario al momento que envíen sus comprobantes por la opción <b>Ventas -> Facturas de Venta</b> y su fecha de emisión supere el límite deberán de considerarse como anulado en su declaración tributaria. <button type="button" class="btn btn-success btn-lae_pagos">Pagar</button>
      </div>
      <?php
      } else if ( $this->empresa->Nu_Tipo_Proveedor_FE == 2 && $this->empresa->Nu_Estado_Pago_Sistema == 0 ) { ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Advertencia:</strong> Las ventas no se guardarán en el sistema, tienen hasta el 05 para poder regularizar o de lo contrario al completar la venta, esta se perderá. <button type="button" class="btn btn-success btn-lae_pagos">Pagar</button>
      </div><?php
      } else if ( $this->empresa->Nu_Tipo_Proveedor_FE == 3 && $this->empresa->Nu_Estado_Pago_Sistema == 0 ) { ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Advertencia:</strong> No se guardará información en el sistema, ya se venció el pago. <button type="button" class="btn btn-success btn-lae_pagos">Pagar</button>
      </div><?php
      }
    } else if ( $date1 > $date2 ) {
      if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 && $this->empresa->Nu_Estado_Pago_Sistema == 0 ) { ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Advertencia:</strong> Las ventas se guardarán pero no serán enviadas a SUNAT por falta de pago, ya se venció el pago, al enviar sus comprobantes por la opción <b>Ventas -> Facturas de Venta</b> y su fecha de emisión supere el límite deberán de considerarse como anulado en su declaración tributaria. <button type="button" class="btn btn-success btn-lae_pagos">Pagar</button>
      </div>
      <?php
      } else if ( $this->empresa->Nu_Tipo_Proveedor_FE == 2 && $this->empresa->Nu_Estado_Pago_Sistema == 0 ) { ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Advertencia:</strong> Las ventas no se guardarán en el sistema, ya se venció el pago, al enviar sus comprobantes por la opción <b>Ventas -> Facturas de Venta</b> y su fecha de emisión supere el límite deberán de considerarse como anulado en su declaración tributaria. <button type="button" class="btn btn-success btn-lae_pagos">Pagar</button>
      </div><?php
      } else if ( $this->empresa->Nu_Tipo_Proveedor_FE == 3 && $this->empresa->Nu_Estado_Pago_Sistema == 0 ) { ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Advertencia:</strong> No se guardará información en el sistema, ya se venció el pago. <button type="button" class="btn btn-success btn-lae_pagos">Pagar</button>
      </div><?php
      }
    }
    ?>
    
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-7">
        <div class="row">
          <!-- col Ventas -->
          <div class="col-xs-6"><br>
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <a href="<?php echo base_url() . 'Ventas/VentaController/listarVentas' ?>" class="small-box-footer" style="color: aliceblue">
                  <h3><?php echo (is_object($arrHeader) ? $arrHeader->ventas : 0); ?></h3>
                  <p>Ventas</p>
                </a>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="<?php echo base_url() . 'Ventas/VentaController/listarVentas' ?>" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- col Ventas -->
          <div class="col-xs-6"><br>
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <a href="<?php echo base_url() . 'Logistica/ReglasLogistica/ProductoController/listarProductos' ?>" style="color: aliceblue">
                  <h3><?php echo (is_object($arrHeader) ? $arrHeader->productos : 0); ?></h3>
                  <p>Productos</p>
                </a>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?php echo base_url() . 'Logistica/ReglasLogistica/ProductoController/listarProductos' ?>" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- col Ventas -->
          <div class="col-xs-6"><br>
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <a href="<?php echo base_url() . 'Ventas/ReglasVenta/ClienteController/listarClientes' ?>" style="color: aliceblue">
                  <h3><?php echo (is_object($arrHeader) ? $arrHeader->clientes : 0); ?></h3>
                  <p>Clientes</p>
                </a>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="<?php echo base_url() . 'Ventas/ReglasVenta/ClienteController/listarClientes' ?>" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- col Ventas -->
          <div class="col-xs-6"><br>
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <a href="<?php echo base_url() . 'Logistica/CompraController/listarCompras' ?>" style="color: aliceblue">
                  <h3><?php echo (is_object($arrHeader) ? $arrHeader->compras : 0); ?></h3>
                  <p>Compras</p>
                </a>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo base_url() . 'Logistica/CompraController/listarCompras' ?>" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
      </div><!--col-xs-8-->
      
      <div class="col-xs-12 col-sm-12 col-md-5 hidden">
        <div class="row">
          <div class="col-xs-12">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-aqua" style="height: auto; background: -webkit-linear-gradient(left, #1E88E5, #00B8D4);">
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username" style="text-align:center">Notificaciones</h3>
              </div>
              <div id="div-notificaciones" class="box-footer no-padding" title="Debe de presionar F5 para recargar la información">
                <ul class="nav nav-stacked">
                  <?php
                    $sNombreVersionSistema='Actualizado';
                    $sClassVersionSistema='';
                    $sIconActualizandoSistema='';
                    if (NUEVA_VERSION_SISTEMA != $this->empresa->No_Version_Sistema) {
                      $sNombreVersionSistema='Nueva ' . NUEVA_VERSION_SISTEMA;
                      $sClassVersionSistema='aVersionSistema';
                      if ($this->empresa->Nu_Estado_Actualizacion_Version_Sistema == 1) {//1=Actualizando
                        $sClassVersionSistema='';
                        $sIconActualizandoSistema='<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>';
                      }
                    } 
                  ?> 
                  <li><a class="<?php echo $sClassVersionSistema; ?>" data-numero_version_sistema="<?php echo NUEVA_VERSION_SISTEMA; ?>" title="Descargar nueva versión del sistema" href="#">Versión de Sistema <span class="pull-right badge bg-blue"><?php echo $sNombreVersionSistema; ?> <?php echo $sIconActualizandoSistema; ?></span></a></li>
                  <li><a href="#">Documentos error SUNAT <span class="pull-right badge bg-red"><?php echo $arrHeader->documento_error; ?></span></a></li>
                  <li><a href="#">Documentos pendientes SUNAT <span class="pull-right badge bg-red"><?php echo $arrHeader->documento_pendiente; ?></span></a></li>
                  <li><a href="#">Ventas Completadas Hoy <span class="pull-right badge bg-green"><?php echo $arrHeader->documento_completado; ?></span></a></li>
                  <li><a href="#">Ventas Anuladas Hoy <span class="pull-right badge bg-yellow"><?php echo $arrHeader->documento_anulado; ?></span></a></li>
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
        </div>
      </div>
    </div>
    <!-- /.row Totales por cantidad -->
  </section>
  <!-- /. Section content -->
</div>
<!-- /.content-wrapper -->