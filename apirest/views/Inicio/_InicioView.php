<div id="div-inicio-filtro-reporte-grafico" class="col-md-10" class="tab-pane">
  <canvas id="canvas-graficaBar"></canvas>
</div>
<?php
$fTotalBFND = 0.00;
$fTotalNC = 0.00;
foreach ($reporte['Tabla'] as $row){
  $fTotalBFND += $row->venta_bfnd;
  $fTotalNC += $row->venta_nc;
}
?>
<div class="box-footer">
  <div class="row">
    <div class="col-xs-12 col-sm-4 col-md-2">
      <div class="description-block border-right">
        <span class="description-percentage text-blue"><i class="fa fa-caret-up"></i></span>
        <h5 class="description-header">S/ <?php echo $fTotalBFND; ?></h5>
        <span class="description-text">Total<br>Boleta<br>Factura<br>N. Débito</span>
      </div>
      <!-- /.description-block -->
    </div>
    <!-- /.col-xs-12 col-sm-4-->
    <div class="col-xs-12 col-sm-4 col-md-2">
      <div class="description-block border-right">
        <span class="description-percentage text-yellow"><i class="fa fa-caret-down"></i></span>
        <h5 class="description-header">S/ <?php echo $fTotalNC; ?></h5>
        <span class="description-text">Total<br>N. Crédito</span>
      </div>
      <!-- /.description-block -->
    </div>
    <!-- /.col-xs-12 col-sm-4-->
    <div class="col-xs-12 col-sm-4 col-md-2">
      <div class="description-block border-right">
        <span class="description-percentage text-green"><i class="fa fa-caret-up"></i></span>
        <h5 class="description-header">S/ <?php echo $fTotalBFND - $fTotalNC; ?></h5>
        <span class="description-text">Total Venta Neta</span>
      </div>
      <!-- /.description-block -->
    </div>
    <!-- /.col-xs-12 col-sm-4-->
  </div>
  <!-- /.row -->
</div>
<!-- /.box footer total Boleta - Factura - ND - NC -->

<br>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Productos más vendidos</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <ul class="products-list product-list-in-box">
          <?php
          if ( count($reporte['arrProductosVendidos']) == 0 ) { ?>
            <li class="item">
              <div class="product-info">
                <span class="product-description">No hay registros</span>
              </div>
            </li>
          <?php
          } else {
            foreach ($reporte['arrProductosVendidos'] as $row){ ?>
              <li class="item">
                <div class="product-img">
                  <img src="dist/img/default-50x50.gif" alt="Product Image">
                </div>
                <div class="product-info">
                  <a href="javascript:void(0)" class="product-title"><?php echo $row->No_Marca; ?>
                  <span class="label label-success pull-right"><?php echo $row->Qt_Producto; ?></span></a>
                  <span class="product-description">
                  <?php echo $row->No_Producto; ?>
                  </span>
                </div>
              </li>
              <?php
            }
          } ?>
        </ul>
        <!-- /.ul-items -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box-principal productos mas vendidos -->
  </div>
  <!-- /.col-xs-12 col-sm-12 col-md-6 productos mas vendidos -->

  <div class="col-xs-12 col-sm-12 col-md-6">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-users"></i> Mejores Clientes</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Cantidad</th>
              <th>Total Neto</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ( count($reporte['arrMejoresClientes']) == 0 ) { ?>
              <tr>
                <td colspan="3" class="text-center">No hay registros</td>
              </tr>
            <?php
            } else {
              foreach ($reporte['arrMejoresClientes'] as $row){ ?>
                <tr>
                  <td><?php echo $row->No_Razsocial; ?></td>
                  <td><?php echo $row->Qt_Producto; ?></td>
                  <td>S/ <?php echo $row->venta_neta; ?></td>
                </tr>
                <?php
              }
            } ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box-principal ordenes de venta -->
  </div>
  <!-- /.col-xs-12 col-sm-12 col-md-6 ordenes de venta -->
</div>
<!-- /.row producto mas vendidos y mejores clientes -->

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title"><i class="fa fa-clone"></i> Órdenes de Ventas</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive">
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
              <th>F. Emisión</th>
              <th>Orden</th>
              <th>Cliente</th>
              <th>Contacto</th>
              <th>M</th>
              <th>Total</th>
              <th>Estado</th>
            </tr>
            <?php
            if ( count($reporte['arrOrdenesVenta']) == 0 ) { ?>
              <tr>
                <td colspan="9" class="text-center">No hay registros</td>
              </tr>
            <?php
            } else {
              foreach ($reporte['arrOrdenesVenta'] as $row){ ?>
                <tr>
                  <td><?php echo ToDateBD($row->Fe_Emision); ?></td>
                  <td><?php echo ToDateBD($row->Fe_Vencimiento); ?></td>
                  <td><?php echo ToDateBD($row->Fe_Periodo); ?></td>
                  <td><?php echo $row->ID_Documento_Cabecera; ?></td>
                  <td><?php echo $row->No_Entidad; ?></td>
                  <td><?php echo $row->No_Contacto; ?></td>
                  <td class="text-right"><?php echo $row->No_Signo; ?></td>
                  <td class="text-right"><?php echo $row->Ss_Total; ?></td>
                  <td><span class="label label-<?php echo $row->No_Class_Estado; ?>"><?php echo $row->No_Descripcion_Estado; ?></span></td>
                </tr>
              <?php
              }
            } ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="col-xs-12">
          <a class="btn btn-success btn-block" href="<?php echo base_url() . 'Ventas/OrdenVentaController/listarOrdenesVenta' ?>" class="small-box-footer">Ver más <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- /.box-footer -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col-xs-12 -->
</div>
<!-- /.row ultimas 5 ordenes de venta -->