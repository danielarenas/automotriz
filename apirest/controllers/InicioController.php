<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class InicioController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('ConfiguracionModel');
	}

	public function index(){
        $dToday = dateNow('fecha');
		$dLastDay = date('t',strtotime($dToday));
		/*
		$arrRowInicio = $this->ConfiguracionModel->inicio();
		
		$arrGrafico = array(
			"dInicial" => date('Y') . '-' . date('m') . '-01',
			"dFinal" => date('Y') . '-' . date('m') . '-' . $dLastDay,
			"iIDMoneda" => 1,
		);
		$arrRowGrafico = $this->ConfiguracionModel->reporteGrafico($arrGrafico);
*/
		$this->load->view('header');
		$this->load->view('Inicio/InicioView',
			array(
				//"arrHeader" => $arrRowInicio,
				"arrHeader" => array(),
				"sToday" => '01 ' . getNameMonth(date('m')) . ', ' . date('Y') . ' - ' . $dLastDay . ' ' . getNameMonth(date('m')) . ', ' . date('Y'),
				//"reporte" => $arrRowGrafico,
				"reporte" => array(),
				"dInicial" => date('Y') . date('m') . '-01',
				"dFinal" => date('Y') . date('m') . '-' . $dLastDay,
				"iIDMoneda" => 1,
			)
		);
		$this->load->view('footer', array("js_inicio" => true));
		//$this->load->view('footer_assets_inicio');
	}
    
	public function enviarCorreoSoporteMigracionSistema(){
		$this->load->library('email');

		$data = array();
		$data["sRazonSocialEmpresa"] = $this->empresa->No_Empresa;
		$data["sNombreOrganizacion"] = 'Prueba';
		$data["dSolicitud"] = dateNow('fecha');
		$data["sUsuario"] = $this->user->No_Usuario;
		$data["sVersionCliente"] = $this->empresa->No_Version_Sistema;
		$data["sVersionNueva"] = NUEVA_VERSION_SISTEMA;

		$this->email->from('noreply@laesystems.com', $this->empresa->No_Empresa);//de
		$this->email->to('soporte@laesystems.com');//para
		$this->email->subject('Actualización de versión ' . $this->empresa->No_Version_Sistema . ' a ' . NUEVA_VERSION_SISTEMA);
		$message = $this->load->view('correos/actualizacion_sistema', $data, true);
		$this->email->message($message);
		$this->email->set_newline("\r\n");

		//$isSend = $this->email->send(); // GODADDY
		//log_message('error', $this->email->print_debugger());
		$isSend = TRUE; // C9
		
		$peticion = array(
			'status' => 'error',
			'style_modal' => 'modal-danger',
			'message' => 'No se pudo realizar la actualización, inténtelo más tarde.',
		);
		if($isSend) {
			$arrResponseEstadoActualizacionVersionSistema = $this->actualizarEstadoActualizacionVersionSistema($this->user->ID_Empresa, 1);//1=Actualizando
			if ( $arrResponseEstadoActualizacionVersionSistema['status']=='success' ) {
				$peticion = array(
					'status' => 'success',
					'style_modal' => 'modal-success',
					'message' => 'Se esta actualizando la nueva versión del sistema, se le notificará en cuanto haya culminado.',
				);
				echo json_encode($peticion);
				exit();
			}
			$peticion = $arrResponseEstadoActualizacionVersionSistema;
		}
		echo json_encode($peticion);
	}
	
	public function actualizarEstadoActualizacionVersionSistema($iIDEmpresa, $iEstadoActualizacionVersionSistema){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		$data = array(
		    'Nu_Estado_Actualizacion_Version_Sistema' => $iEstadoActualizacionVersionSistema,
		);
		$where = array('ID_Empresa' => $iIDEmpresa);
		return $this->ConfiguracionModel->actualizarEstadoActualizacionVersionSistema($where, $data);
	}
	
	public function Ajax($action){
		if (!$this->input->is_ajax_request()) exit('No direct script access allowed');
		switch($action){
			case "Reporte":
				$arrReporteGraficoPOST = $this->input->post('arrReporteGrafico');
				$arrRowGrafico = $this->ConfiguracionModel->reporteGrafico($arrReporteGraficoPOST);
				
				$titulo = 'Reporte Mensual';
				echo $this->load->view('Inicio/_InicioView', array(
					"reporte"   => $arrRowGrafico,
					"dInicial"  => $arrReporteGraficoPOST['dInicial'],
					"dFinal"    => $arrReporteGraficoPOST['dFinal'],
					"iIDMoneda" => $arrReporteGraficoPOST['iIDMoneda'],
				), true);
				$this->load->view('footer_assets_inicio');
				break;
		}
	}
}
