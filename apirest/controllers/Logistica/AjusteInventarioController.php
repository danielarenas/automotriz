<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AjusteInventarioController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Logistica/AjusteInventarioModel');
	}
	
	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/AjusteInventarioView');
			$this->load->view('footer', array("js_ajuste_inventario" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->AjusteInventarioModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action = 'delete';
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
            $rows[] = $row->No_Almacen;
            $rows[] = allTypeDate($row->Fe_Emision_Hora, '-', 0);
            $rows[] = $row->Nu_Cantidad;
			$rows[] = '<button class="btn btn-xs btn-link" alt="Ver Ajuste Inventario" title="Ver Ajuste Inventario" href="javascript:void(0)" onclick="verAjusteInventario(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-list" aria-hidden="true"> Ver</i></button>';
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->AjusteInventarioModel->count_all(),
	        'recordsFiltered' => $this->AjusteInventarioModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
	
	public function verAjusteProcesado($ID){
        echo json_encode($this->AjusteInventarioModel->verAjusteProcesado($this->security->xss_clean($ID)));
	}
	
	public function getItemsAjusteInvetario(){
		echo json_encode($this->AjusteInventarioModel->getItemsAjusteInvetario($this->input->post()));
	}
	
	public function guardarAjusteInventario(){
		echo json_encode($this->AjusteInventarioModel->procesarAjusteInventario($this->input->post()));
	}
}
