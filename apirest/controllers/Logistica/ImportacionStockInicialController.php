<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImportacionStockInicialController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Logistica/ImportacionStockInicialModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
	}
	
	public function importarExcelStockInicialProductos(){
		if (isset($_FILES['excel-archivo_stock_inicial_productos']['name']) && isset($_FILES['excel-archivo_stock_inicial_productos']['type']) && isset($_FILES['excel-archivo_stock_inicial_productos']['tmp_name'])) {
		    $archivo = $_FILES['excel-archivo_stock_inicial_productos']['name'];
		    $tipo = $_FILES['excel-archivo_stock_inicial_productos']['type'];
		    $destino = "bak_" . $archivo;
		    
		    if (copy($_FILES['excel-archivo_stock_inicial_productos']['tmp_name'], $destino)) {
		        if (file_exists($destino)) {
					$this->load->library('Excel');
		    		$objReader = new PHPExcel_Reader_Excel2007();
		    		$objPHPExcel = $objReader->load($destino);
		            $objPHPExcel->setActiveSheetIndex(0);
		            
		            $iCantidadRegistros = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
		            
		            $column = array(
		                'CODIGO_BARRA_PRODUCTO' => 'A',
		                'NOMBRE_PRODUCTO' => 'B',
		                'IMPUESTO_PRODUCTO' => 'C',
		                'PRECIO_COMPRA_PRODUCTO' => 'D',
		                'STOCK_INICIAL_FISICO_PRODUCTO' => 'E',
		            );
		            
	                $arrStockInicialProductos = array();
	                $iCantidadNoProcesados = 0;
                	for ($i = 2; $i <= $iCantidadRegistros; $i++) {
	                	$sCodigoBarraProducto = strtoupper(filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['CODIGO_BARRA_PRODUCTO'] . $i)->getCalculatedValue())));
						$sCodigoBarraProducto = quitarCaracteresEspeciales($sCodigoBarraProducto);
						
	                	$sNombreImpuesto = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['IMPUESTO_PRODUCTO'] . $i)->getCalculatedValue()));
						
	                	$fPrecioCompra = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['PRECIO_COMPRA_PRODUCTO'] . $i)->getCalculatedValue()));
						$fPrecioCompra = quitarCaracteresEspeciales($fPrecioCompra);

	                	$fStockInicialFisicoProducto = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['STOCK_INICIAL_FISICO_PRODUCTO'] . $i)->getCalculatedValue()));
						$fStockInicialFisicoProducto = quitarCaracteresEspeciales($fStockInicialFisicoProducto);

	                	if (!empty($fStockInicialFisicoProducto) && !empty($fStockInicialFisicoProducto)) {
		                	$arrStockInicialProductos[] = array(
								'Nu_Codigo_Barra' => $sCodigoBarraProducto,
								'No_Impuesto' => $sNombreImpuesto,
								'Ss_Precio' => $fPrecioCompra,
								'Qt_Producto' => $fStockInicialFisicoProducto,
		                	);
	                	} else {
                        	$iCantidadNoProcesados++;
                        }
                	}// /. for arrExcel
                	
					unlink($destino);
                	$bResponse=false;
                	if ( count($arrStockInicialProductos) > 0 ) {
						$this->ImportacionStockInicialModel->setBatchImport($arrStockInicialProductos);
						$arrResponse = $this->ImportacionStockInicialModel->importData();
						$arrStockInicialCabecera = $arrResponse['arrStockInicialCabecera'];
						
						unset($arrStockInicialProductos);
						if ($arrResponse['status'] == 'success') {
							$arrStockInicialCabecera['ID_Tipo_Movimiento'] = 7;//Saldo Inicial
							
							$arrResponseMovimientoInventario = $this->MovimientoInventarioModel->crudMovimientoInventario(
								$arrStockInicialCabecera['ID_Almacen'],
								$arrResponse['Last_ID_Documento_Cabecera'],
								0,
								$arrResponse['arrStockInicialDetalle'],
								$arrStockInicialCabecera['ID_Tipo_Movimiento'],
								0,
								'',
								0,
								1
							);
							
							if ($arrResponseMovimientoInventario['status'] == 'success') {
								redirect('Logistica/ImportacionStockInicialController/listar/' . $arrResponseMovimientoInventario['status'] . '/' . $iCantidadNoProcesados);
								unset($arrResponseMovimientoInventario);
								exit();
							} else {
								unset($arrResponse);
								$sStatus = 'error-bd';
								redirect('Logistica/ImportacionStockInicialController/listar/' . $sStatus . '/' . 0 . '/' . $arrResponseMovimientoInventario['message']);
								unset($arrResponseMovimientoInventario);
								exit();
							}
						} else {
							$sStatus = 'error-bd';
							redirect('Logistica/ImportacionStockInicialController/listar/' . $sStatus . '/' . 0 . '/' . $arrResponse['message']);
							unset($arrResponse);
							exit();
						}
                	} else {
	                	unset($arrStockInicialProductos);
                	
                		$sStatus = 'error-sindatos';
						redirect('Logistica/ImportacionStockInicialController/listar/' . $sStatus);
                		exit();
                	}
		        } else {
        	        $sStatus = 'error-archivo_no_existe';
					redirect('Logistica/ImportacionStockInicialController/listar/' . $sStatus);
					exit();
		        }
		    } else {
		        $sStatus = 'error-copiar_archivo';
				redirect('Logistica/ImportacionStockInicialController/listar/' . $sStatus);
				exit();
		    }
		}
	}

	public function listar($sStatus='', $iCantidadNoProcesados='', $sMessageErrorBD=''){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/ImportacionStockInicialView', array('sStatus' => $sStatus, 'iCantidadNoProcesados' => $iCantidadNoProcesados, 'sMessageErrorBD' => $sMessageErrorBD));
			$this->load->view('footer', array("js_importacion_stock_inicial" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->ImportacionStockInicialModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action = 'delete';
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
            $rows[] = $row->No_Almacen;
            $rows[] = ToDateBD($row->Fe_Emision);
            $rows[] = $row->Tipo_Operacion_Sunat_Codigo;
            $rows[] = $row->No_Tipo_Movimiento;
            $rows[] = $row->No_Entidad;
            $rows[] = $row->Nu_Codigo_Barra;
            $rows[] = $row->No_Producto;
            $rows[] = $row->Ss_Precio;
            $rows[] = $row->Qt_Producto;
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->ImportacionStockInicialModel->count_all(),
	        'recordsFiltered' => $this->ImportacionStockInicialModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
	}
	
	public function verificarImportacionStockInicial(){
		echo json_encode($this->ImportacionStockInicialModel->verificarImportacionStockInicial());
	}
}
