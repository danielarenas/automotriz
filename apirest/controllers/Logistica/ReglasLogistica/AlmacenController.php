<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlmacenController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Logistica/ReglasLogistica/AlmacenModel');
	}

	public function listarAlmacenes(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/ReglasLogistica/AlmacenView');
			$this->load->view('footer', array("js_almacen" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->AlmacenModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
			$rows[] = '<span class="label label-' . $row->No_Class_Proveedor_FE . '">' . $row->No_Descripcion_Proveedor_FE . '</span>';
			$rows[] = '<span class="label label-' . $row->No_Class_Ecommerce . '">' . $row->No_Descripcion_Ecommerce . '</span>';
			$rows[] = $row->No_Empresa;
			$rows[] = $row->No_Organizacion;
            $rows[] = $row->No_Almacen;
            $rows[] = $row->Txt_Direccion_Almacen;
            $rows[] = ($row->Nu_Estado_Pago_Sistema == 1 ? '<span class="label label-success">Cancelado</span>' : '<span class="label label-danger">Pendiente</span>');
            $rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verAlmacen(\'' . $row->ID_Almacen . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarAlmacen(\'' . $row->ID_Almacen . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->AlmacenModel->count_all(),
	        'recordsFiltered' => $this->AlmacenModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
	
	public function ajax_edit($ID){
        echo json_encode($this->AlmacenModel->get_by_id($this->security->xss_clean($ID)));
    }
    
	public function crudAlmacen(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		$data = array(
			'ID_Organizacion' => $this->input->post('ID_Oganizacion'),
			'No_Almacen' => $this->input->post('No_Almacen'),
			'Nu_Codigo_Establecimiento_Sunat' => $this->input->post('Nu_Codigo_Establecimiento_Sunat'),
			'Txt_Direccion_Almacen'	=> $this->input->post('Txt_Direccion_Almacen'),
			'ID_Pais' => $this->input->post('ID_Pais'),
			'ID_Departamento' => $this->input->post('ID_Departamento'),
			'ID_Provincia' => $this->input->post('ID_Provincia'),
			'ID_Distrito' => $this->input->post('ID_Distrito'),
			'Nu_Estado_Pago_Sistema' => $this->input->post('Nu_Estado_Pago_Sistema'),
			'Nu_Estado'	=> $this->input->post('Nu_Estado'),
			'Txt_FE_Ruta' => $this->input->post('Txt_FE_Ruta'),
			'Txt_FE_Token' => $this->input->post('Txt_FE_Token'),
			'Nu_Latitud_Maps' => $this->input->post('Nu_Latitud_Maps'),
			'Nu_Longitud_Maps' => $this->input->post('Nu_Longitud_Maps'),
		);
		echo json_encode(
		($this->input->post('EID_Organizacion') != '' && $this->input->post('EID_Almacen') != '') ?
			$this->AlmacenModel->actualizarAlmacen(array('ID_Organizacion' => $this->input->post('EID_Organizacion'), 'ID_Almacen' => $this->input->post('EID_Almacen')), $data, $this->input->post('ENo_Almacen'))
		:
			$this->AlmacenModel->agregarAlmacen($data)
		);
	}
    
	public function eliminarAlmacen($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->AlmacenModel->eliminarAlmacen($this->security->xss_clean($ID)));
	}
}
