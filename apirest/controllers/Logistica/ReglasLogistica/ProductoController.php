<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class ProductoController extends CI_Controller {
	
	private $upload_path = '../../../public_html/assets/images/productos/';
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Logistica/ReglasLogistica/ProductoModel');
	}
	
	public function importarExcelProductos(){
		if (isset($_FILES['excel-archivo_producto']['name']) && isset($_FILES['excel-archivo_producto']['type']) && isset($_FILES['excel-archivo_producto']['tmp_name'])) {
		    $archivo = $_FILES['excel-archivo_producto']['name'];
		    $tipo = $_FILES['excel-archivo_producto']['type'];
		    $destino = "bak_" . $archivo;
		    
		    if (copy($_FILES['excel-archivo_producto']['tmp_name'], $destino)) {
		        if (file_exists($destino)) {
					$this->load->library('Excel');
		    		$objReader = new PHPExcel_Reader_Excel2007();
		    		$objPHPExcel = $objReader->load($destino);
		            $objPHPExcel->setActiveSheetIndex(0);
		            
		            $iCantidadRegistros = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
		            
		            $column = array(
		                'GRUPO_PRODUCTO' 		=> 'A',
		                'TIPO_PRODUCTO'			=> 'B',
		                'UBICACION_INVENTARIO'	=> 'C',
		                'CODIGO_BARRA'			=> 'D',
		                'CODIGO_PRODUCTO'		=> 'E',
		                'NOMBRE'				=> 'F',
		                'GRUPO_IMPUESTO'		=> 'G',
		                'UNIDAD_MEDIDA'			=> 'H',
		                'MARCA'					=> 'I',
		                'CATEGORIA'				=> 'J',
		                'SUB_CATEGORIA'			=> 'K',
		                'CODIGO_PRODUCTO_SUNAT'	=> 'L',
						'CANTIDAD_CO2_PRODUCTO'	=> 'M',
						'PRECIO' => 'N',
						'COSTO'	=> 'O',
						'STOCK_MINIMO' => 'P',
						'TIPO_LAVADO' => 'Q',
						'ESTADO' => 'R',
						'PRECIO_ECOMMERCE_ONLINE_REGULAR' => 'S',
						'PRECIO_ECOMMERCE_ONLINE' => 'T',
						'CATEGORIA_MARKETPLACE' => 'U',
						'SUB_CATEGORIA_MARKETPLACE' => 'V',
						'MARCA_MARKETPLACE' => 'W',
		            );
		            
	                $arrProducto = array();
	                $iCantidadNoProcesados = 0;
                	for ($i = 2; $i <= $iCantidadRegistros; $i++) {
	                	$iID_Grupo_Producto = $objPHPExcel->getActiveSheet()->getCell($column['GRUPO_PRODUCTO'] . $i)->getCalculatedValue();
	                	$iID_Grupo_Producto = filter_var(trim($iID_Grupo_Producto));
	                	
	                	$ID_Tipo_Producto = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['TIPO_PRODUCTO'] . $i)->getCalculatedValue()));
	                	
	                	$No_Ubicacion_Inventario = $objPHPExcel->getActiveSheet()->getCell($column['UBICACION_INVENTARIO'] . $i)->getCalculatedValue();
	                	$No_Ubicacion_Inventario = ucfirst(filter_var(trim($No_Ubicacion_Inventario)));
	                	
	                	$Nu_Codigo_Barra = $objPHPExcel->getActiveSheet()->getCell($column['CODIGO_BARRA'] . $i)->getCalculatedValue();
	                	$Nu_Codigo_Barra = strtoupper(filter_var(trim($Nu_Codigo_Barra)));
	                	
	                	$Nu_Codigo_Producto = $objPHPExcel->getActiveSheet()->getCell($column['CODIGO_PRODUCTO'] . $i)->getCalculatedValue();
	                	$Nu_Codigo_Producto = strtoupper(filter_var(trim($Nu_Codigo_Producto)));
						
	                	$No_Producto = nl2br(trim($objPHPExcel->getActiveSheet()->getCell($column['NOMBRE'] . $i)->getCalculatedValue()));
                        $No_Producto = quitarCaracteresEspeciales($No_Producto);
                        
	                	$No_Impuesto = $objPHPExcel->getActiveSheet()->getCell($column['GRUPO_IMPUESTO'] . $i)->getCalculatedValue();
	                	$No_Impuesto = filter_var(trim($No_Impuesto));
	                	
	                	$No_Unidad_Medida = $objPHPExcel->getActiveSheet()->getCell($column['UNIDAD_MEDIDA'] . $i)->getCalculatedValue();
	                	$No_Unidad_Medida = filter_var(trim($No_Unidad_Medida));
	                	
	                	$No_Marca = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['MARCA'] . $i)->getCalculatedValue()));
	                	$No_Marca = quitarCaracteresEspeciales($No_Marca);
	                	
	                	$No_Familia = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['CATEGORIA'] . $i)->getCalculatedValue()));
	                	$No_Familia = quitarCaracteresEspeciales($No_Familia);
	                	
	                	$No_Sub_Familia = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['SUB_CATEGORIA'] . $i)->getCalculatedValue()));
	                	$No_Sub_Familia = quitarCaracteresEspeciales($No_Sub_Familia);
	                	
	                	$Nu_Codigo_Producto_Sunat = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['CODIGO_PRODUCTO_SUNAT'] . $i)->getCalculatedValue()));
	                	$Nu_Codigo_Producto_Sunat = quitarCaracteresEspeciales($Nu_Codigo_Producto_Sunat);
	                	
	                	$Qt_CO2_Producto = $objPHPExcel->getActiveSheet()->getCell($column['CANTIDAD_CO2_PRODUCTO'] . $i)->getCalculatedValue();
						$Qt_CO2_Producto = strtoupper(filter_var(trim($Qt_CO2_Producto)));
						
	                	$fPrecio = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['PRECIO'] . $i)->getCalculatedValue()));
						$fPrecio = quitarCaracteresEspeciales($fPrecio);
						
	                	$fCosto = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['COSTO'] . $i)->getCalculatedValue()));
						$fCosto = quitarCaracteresEspeciales($fCosto);
						
	                	$iStockMinimo = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['STOCK_MINIMO'] . $i)->getCalculatedValue()));
						$iStockMinimo = quitarCaracteresEspeciales($iStockMinimo);
						
	                	$iTipoLavado = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['TIPO_LAVADO'] . $i)->getCalculatedValue()));
						$iTipoLavado = quitarCaracteresEspeciales($iTipoLavado);
						
	                	$iEstado = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['ESTADO'] . $i)->getCalculatedValue()));
						$iEstado = quitarCaracteresEspeciales($iEstado);
						
	                	$Ss_Precio_Ecommerce_Online_Regular = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['PRECIO_ECOMMERCE_ONLINE_REGULAR'] . $i)->getCalculatedValue()));
	                	$Ss_Precio_Ecommerce_Online_Regular = quitarCaracteresEspeciales($Ss_Precio_Ecommerce_Online_Regular);
	                	
	                	$Ss_Precio_Ecommerce_Online = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['PRECIO_ECOMMERCE_ONLINE'] . $i)->getCalculatedValue()));
	                	$Ss_Precio_Ecommerce_Online = quitarCaracteresEspeciales($Ss_Precio_Ecommerce_Online);
	                	
	                	$No_Familia_Marketplace = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['CATEGORIA_MARKETPLACE'] . $i)->getCalculatedValue()));
	                	$No_Familia_Marketplace = quitarCaracteresEspeciales($No_Familia_Marketplace);
	                	
	                	$No_Sub_Familia_Marketplace = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['SUB_CATEGORIA_MARKETPLACE'] . $i)->getCalculatedValue()));
	                	$No_Sub_Familia_Marketplace = quitarCaracteresEspeciales($No_Sub_Familia_Marketplace);
	                	
	                	$No_Marca_Marketplace = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['MARCA_MARKETPLACE'] . $i)->getCalculatedValue()));
	                	$No_Marca_Marketplace = quitarCaracteresEspeciales($No_Marca_Marketplace);
	                	
	                	if ( 
							($iID_Grupo_Producto == 0 || $iID_Grupo_Producto == 1 || $iID_Grupo_Producto == 2)
							&& !empty($Nu_Codigo_Barra)
							&& !empty($No_Producto)
							&& !empty($No_Impuesto)
							&& !empty($No_Unidad_Medida)
							&& !empty($fPrecio)
						) {
		                	$arrProducto[] = array(
								'Nu_Tipo_Producto' => $iID_Grupo_Producto,
								'ID_Tipo_Producto' => $ID_Tipo_Producto,
								'No_Ubicacion_Inventario' => $No_Ubicacion_Inventario,
								'Nu_Codigo_Barra' => $Nu_Codigo_Barra,
								'Nu_Codigo_Producto' => $Nu_Codigo_Producto,
								'No_Producto' => $No_Producto,
								'No_Impuesto' => $No_Impuesto,
								'No_Marca' => $No_Marca,
								'No_Unidad_Medida' => $No_Unidad_Medida,
								'No_Familia' => $No_Familia,
								'No_Sub_Familia' => $No_Sub_Familia,
								'Nu_Codigo_Producto_Sunat' => $Nu_Codigo_Producto_Sunat,
								'Qt_CO2_Producto' => $Qt_CO2_Producto,
								'fPrecio' => $fPrecio,
								'fCosto' => $fCosto,
								'iStockMinimo' => $iStockMinimo,
								'iTipoLavado' => $iTipoLavado,
								'iEstado' => $iEstado,
								'Ss_Precio_Ecommerce_Online_Regular' => $Ss_Precio_Ecommerce_Online_Regular,
								'Ss_Precio_Ecommerce_Online' => $Ss_Precio_Ecommerce_Online,
								'No_Familia_Marketplace' => $No_Familia_Marketplace,
								'No_Sub_Familia_Marketplace' => $No_Sub_Familia_Marketplace,
								'No_Marca_Marketplace' => $No_Marca_Marketplace,
		                	);
	                	} else {
                        	$iCantidadNoProcesados++;
                        }
                	}
                	
                	$bResponse=false;
                	if ( count($arrProducto) > 0 ) {
		                $this->ProductoModel->setBatchImport($arrProducto);
		                $bResponse = $this->ProductoModel->importData();
                	} else {
	            		unlink($destino);
	                	unset($arrProducto);
                	
                		$sStatus = 'error-sindatos';
						redirect('Logistica/ReglasLogistica/ProductoController/listarProductos/' . $sStatus);
                		exit();
                	}
                	
            		unlink($destino);
                	unset($arrProducto);
                	
                	if ($bResponse){
                		$sStatus = 'success';
						redirect('Logistica/ReglasLogistica/ProductoController/listarProductos/' . $sStatus . '/' . $iCantidadNoProcesados);
                	} else {
                		$sStatus = 'error-bd';
						redirect('Logistica/ReglasLogistica/ProductoController/listarProductos/' . $sStatus);
                	}
		        } else {
        	        $sStatus = 'error-archivo_no_existe';
					redirect('Logistica/ReglasLogistica/ProductoController/listarProductos/' . $sStatus);
		        }
		    } else {
		        $sStatus = 'error-copiar_archivo';
				redirect('Logistica/ReglasLogistica/ProductoController/listarProductos/' . $sStatus);
		    }
		}
	}

	public function listarProductos($sStatus='', $iCantidadNoProcesados=''){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/ReglasLogistica/ProductoView', array('sStatus' => $sStatus, 'iCantidadNoProcesados' => $iCantidadNoProcesados));
			$this->load->view('footer', array("js_producto" => true));
		}
	}

	public function ajax_list(){
		$arrData = $this->ProductoModel->get_datatables();
		$data = [];
		$no = $this->input->post('start');
		$action = 'delete';
		foreach ($arrData as $row) {
			settype($row->Qt_Producto, "double");
			$no++;
			$rows = [];
			$rows[] = $row->No_Descripcion_Grupo;
			$rows[] = $row->No_Area_Almacen;
			$rows[] = $row->No_Unidad_Medida;
			$rows[] = $row->No_Familia;
			$rows[] = $row->No_Sub_Familia;
			$rows[] = $row->No_Marca;
			$rows[] = $row->Nu_Codigo_Barra;
			$rows[] = $row->No_Producto;
			$rows[] = $row->No_Impuesto_Breve;
			$rows[] = $row->Ss_Precio;
			$rows[] = $row->Ss_Costo;
			$rows[] = $row->Qt_Producto;
			$rows[] = $row->Nu_Stock_Minimo;
			$rows[] = $row->Txt_Ubicacion_Producto_Tienda;
			$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verProducto(\'' . $row->ID_Producto . '\', \'' . $row->Nu_Codigo_Barra . '\', \'' . $row->No_Imagen_Item . '\', \'' . $row->Nu_Version_Imagen . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarProducto(\'' . $row->ID_Empresa . '\', \'' . $row->ID_Producto . '\', \'' . $row->Nu_Codigo_Barra . '\', \'' . $row->Nu_Compuesto . '\', \'' . $action . '\', \'' . $row->No_Imagen_Item . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
			$data[] = $rows;
		}

		$output = [
			'draw' => $this->input->post('draw'),
			'recordsTotal' => $this->ProductoModel->count_all(),
			'recordsFiltered' => $this->ProductoModel->count_filtered(),
			'data' => $data,
		];

		echo json_encode($output);
	}

	public function uploadMultiple(){
		$arrResponse = array(
			'sStatus' => 'error',
			'sMessage' => 'problemas con imagén',
			'sClassModal' => 'modal-danger',
		);

    	if (!empty($_FILES)){
			$path = $this->upload_path . $this->empresa->Nu_Documento_Identidad;
			if(!is_dir($path)){
			    mkdir($path,0755,TRUE);
			}
			
			if ( !file_exists($path . '/' . cambiarCaracteresEspecialesImagen($_FILES['file']['name'])) ){
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'png|jpg|jpeg|webp';
				$config['max_size'] = 400;//400 KB
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('file')){
					$arrResponse = array(
						'sStatus' => 'error',
						'sMessage' => strip_tags($this->upload->display_errors()) . ' No se guardó imagén' . $path,
						'sClassModal' => 'modal-danger',
					);
				} else {
					$data = array('Nu_Version_Imagen' => $this->input->post('iVersionImage'));
					$where = array('ID_Producto' => $this->input->post('iIdProducto') );
					$this->ProductoModel->actualizarVersionImagen($where, $data);

					$arrUrlImagePath = explode('..', $path);
					$arrUrlImage = explode('/principal',base_url());
					$url_image = $arrUrlImage[0] . $arrUrlImagePath[1];
					$arrResponse = array(
						'sStatus' => 'success',
						'sMessage' => 'imagén guardada',
						'sClassModal' => 'modal-success',
						'sNombreImagenItem' => $url_image . '/' . cambiarCaracteresEspecialesImagen($_FILES['file']['name']),
					);
				}
			} else {
				$arrUrlImagePath = explode('..', $path);
				$arrUrlImage = explode('/principal',base_url());
				$url_image = $arrUrlImage[0] . $arrUrlImagePath[1];				
				$arrResponse = array(
					'sStatus' => 'success',
					'sMessage' => 'La imagen ya fue guardada',
					'sClassModal' => 'modal-success',
					//'sNombreImagenItem' => $_FILES['file']['name'],
					'sNombreImagenItem' => $url_image . '/' . cambiarCaracteresEspecialesImagen($_FILES['file']['name']),
				);
			}
    	}
    	echo json_encode($arrResponse);
    }
    
    public function removeFileImage(){
    	$nameFileImage = $this->input->post('nameFileImage');
		$path = $this->upload_path . $this->empresa->Nu_Documento_Identidad . '/';
		if ( $nameFileImage && file_exists($path . $nameFileImage) ){
    		unlink($path . $nameFileImage);
    	}
    }
	
	public function get_image(){
		$sUrlImage = $this->input->post('sUrlImage');
		$arrUrlImage = explode('/', $sUrlImage);

		//array_debug($arrUrlImage);
		// archivo
		$path = $this->upload_path . $this->empresa->Nu_Documento_Identidad . '/' . $arrUrlImage[5];

		// url
		$path_url = 'https://laesystems.com/assets/images/productos/' . $this->empresa->Nu_Documento_Identidad . '/' . $arrUrlImage[5];
		//array_debug($path);

    	$arrfilesImages = array();
		if ( file_exists($path) ){
			$arrfilesImages[] = array(
				'name' => $path_url,
				'size' => filesize($path),
			);
		}

		echo json_encode($arrfilesImages);
	}

	public function ajax_edit($ID){
		echo json_encode($this->ProductoModel->get_by_id($this->security->xss_clean($ID)));
	}

	public function ajax_edit_enlace($ID){
		echo json_encode($this->ProductoModel->get_by_id_enlace($this->security->xss_clean($ID)));
	}

	public function crudProducto(){
		if (!$this->input->is_ajax_request()) {
			exit('No se puede Agregar/Editar y acceder');
		}

		$sComposicion = '';
		if ( isset($_POST['arrProducto']['Txt_Composicion']) ) {
			$arrComposicion = $this->security->xss_clean($_POST['arrProducto']['Txt_Composicion']);
			$iCount = count($arrComposicion);
			for ($i=0; $i<$iCount; $i++) {
				$sComposicion .= $arrComposicion[$i].',';
			}
			$sComposicion = substr($sComposicion, 0, -1);
		}

		$data_producto = [
			'ID_Empresa' => $this->user->ID_Empresa,
			'Nu_Tipo_Producto' => $this->security->xss_clean($_POST['arrProducto']['Nu_Tipo_Producto']),
			'Nu_Area_Almacen'	=> $this->security->xss_clean($_POST['arrProducto']['Nu_Area_Almacen']),
			'ID_Tipo_Producto' => $this->security->xss_clean($_POST['arrProducto']['ID_Tipo_Producto']),
			'ID_Ubicacion_Inventario'	=> $this->security->xss_clean($_POST['arrProducto']['ID_Ubicacion_Inventario']),
			'Nu_Codigo_Barra'	=> $this->security->xss_clean(strtoupper($_POST['arrProducto']['Nu_Codigo_Barra'])),
			'ID_Producto_Sunat'	=> $this->security->xss_clean($_POST['arrProducto']['ID_Producto_Sunat']),
			'No_Producto'	=> $this->security->xss_clean(nl2br($_POST['arrProducto']['No_Producto'])),
			'Ss_Precio'	=> $this->security->xss_clean(nl2br($_POST['arrProducto']['Ss_Precio'])),
			'Ss_Costo' => $this->security->xss_clean(nl2br($_POST['arrProducto']['Ss_Costo'])),
			'No_Codigo_Interno'	=> $this->security->xss_clean(strtoupper($_POST['arrProducto']['No_Codigo_Interno'])),
			'ID_Impuesto'	=> $this->security->xss_clean($_POST['arrProducto']['ID_Impuesto']),
			'Nu_Lote_Vencimiento'	=> $this->security->xss_clean($_POST['arrProducto']['Nu_Lote_Vencimiento']),
			'ID_Unidad_Medida' => $this->security->xss_clean($_POST['arrProducto']['ID_Unidad_Medida']),
			'ID_Impuesto_Icbper' => $this->security->xss_clean($_POST['arrProducto']['ID_Impuesto_Icbper']),
			'Nu_Compuesto' => $this->security->xss_clean($_POST['arrProducto']['Nu_Compuesto']),
			'Nu_Estado' => $this->security->xss_clean($_POST['arrProducto']['Nu_Estado']),
			'Txt_Ubicacion_Producto_Tienda'	=> $this->security->xss_clean($_POST['arrProducto']['Txt_Ubicacion_Producto_Tienda']),
			'Txt_Producto' => $this->security->xss_clean(nl2br($_POST['arrProducto']['Txt_Producto'])),
			'Nu_Stock_Minimo' => $this->security->xss_clean($_POST['arrProducto']['Nu_Stock_Minimo']),
			'Qt_CO2_Producto' => $this->security->xss_clean($_POST['arrProducto']['Qt_CO2_Producto']),
			'Nu_Receta_Medica' => $this->security->xss_clean($_POST['arrProducto']['Nu_Receta_Medica']),
			'ID_Laboratorio' => $this->security->xss_clean($_POST['arrProducto']['ID_Laboratorio']),
			'ID_Tipo_Pedido_Lavado'	=> $this->security->xss_clean($_POST['arrProducto']['ID_Tipo_Pedido_Lavado']),
			'Txt_Composicion' => $sComposicion,
			'No_Imagen_Item' => $this->security->xss_clean($_POST['arrProducto']['No_Imagen_Item']),
			'Ss_Precio_Ecommerce_Online_Regular' => $this->security->xss_clean($_POST['arrProducto']['Ss_Precio_Ecommerce_Online_Regular']),
			'Ss_Precio_Ecommerce_Online' => $this->security->xss_clean($_POST['arrProducto']['Ss_Precio_Ecommerce_Online']),
			'ID_Familia_Marketplace' => $this->security->xss_clean($_POST['arrProducto']['ID_Familia_Marketplace']),
			'ID_Sub_Familia_Marketplace' => $this->security->xss_clean($_POST['arrProducto']['ID_Sub_Familia_Marketplace']),
			'ID_Marca_Marketplace' => $this->security->xss_clean($_POST['arrProducto']['ID_Marca_Marketplace']),
			'No_Modelo_Vehiculo' => $this->security->xss_clean($_POST['arrProducto']['No_Modelo_Vehiculo']),
			'No_Color_Vehiculo' => $this->security->xss_clean($_POST['arrProducto']['No_Color_Vehiculo']),
			'No_Motor' => $this->security->xss_clean($_POST['arrProducto']['No_Motor']),
		];

		if (!empty($_POST['arrProducto']['ID_Marca'])) {
			$data_producto = array_merge($data_producto, array("ID_Marca" => $_POST['arrProducto']['ID_Marca']));
		}
		if (!empty($_POST['arrProducto']['ID_Familia'])) {
			$data_producto = array_merge($data_producto, array("ID_Familia" => $_POST['arrProducto']['ID_Familia']));
		}
		if (!empty($_POST['arrProducto']['ID_Sub_Familia'])) {
			$data_producto = array_merge($data_producto, array("ID_Sub_Familia" => $_POST['arrProducto']['ID_Sub_Familia']));
		}
		echo json_encode(($this->security->xss_clean($_POST['arrProducto']['EID_Empresa']) != '' && $this->security->xss_clean($_POST['arrProducto']['EID_Producto']) != '') ?
			$this->ProductoModel->actualizarProducto(array('ID_Empresa' => $this->security->xss_clean($_POST['arrProducto']['EID_Empresa']), 'ID_Producto' => $this->security->xss_clean($_POST['arrProducto']['EID_Producto'])), $data_producto, $this->security->xss_clean($_POST['arrProducto']['ENu_Codigo_Barra']), $_POST['arrProductoEnlace'])
		:	$this->ProductoModel->agregarProducto($data_producto, $_POST['arrProductoEnlace'])
		);
	}

	public function eliminarProducto($ID_Empresa, $ID, $Nu_Codigo_Barra, $Nu_Compuesto, $sNombreImagenItem=''){
		if (!$this->input->is_ajax_request()) {
			exit('No se puede eliminar y acceder');
		}
		echo json_encode($this->ProductoModel->eliminarProducto($this->security->xss_clean($ID_Empresa), $this->security->xss_clean($ID), $this->security->xss_clean($Nu_Codigo_Barra), $this->security->xss_clean($Nu_Compuesto), $this->security->xss_clean($sNombreImagenItem)));
	}
}
