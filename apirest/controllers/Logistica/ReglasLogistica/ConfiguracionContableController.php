<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfiguracionContableController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Logistica/ReglasLogistica/ConfiguracionContableModel');
	}
	
	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/ReglasLogistica/ConfiguracionContableView');
			$this->load->view('footer', array("js_configuracion_contable" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->ConfiguracionContableModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action = 'delete';
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
            $rows[] = ($row->Nu_Tipo_Cuenta == 0 ? 'Debe' : 'Haber');
            $rows[] = $row->Nu_Cuenta_Contable;
            $rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verConfiguracionContable(\'' . $row->ID_Configuracion_Cuenta_Contable . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarConfiguracionContable(\'' . $row->ID_Configuracion_Cuenta_Contable . '\', \'' . $action . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->ConfiguracionContableModel->count_all(),
	        'recordsFiltered' => $this->ConfiguracionContableModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
	
	public function ajax_edit($ID){
        echo json_encode($this->ConfiguracionContableModel->get_by_id($this->security->xss_clean($ID)));
    }
    
	public function crudConfiguracionContable(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		$data = array(
			'ID_Empresa' => $this->input->post('ID_Empresa'),
			'Nu_Tipo_Cuenta' => $this->input->post('Nu_Tipo_Cuenta'),
			'Nu_Cuenta_Contable' => $this->input->post('Nu_Cuenta_Contable'),
			'Nu_Estado'	=> $this->input->post('Nu_Estado'),
		);
		echo json_encode(
		($this->input->post('EID_Configuracion_Cuenta_Contable') != '') ?
			$this->ConfiguracionContableModel->actualizarConfiguracionContable(array('ID_Configuracion_Cuenta_Contable' => $this->input->post('EID_Configuracion_Cuenta_Contable')), $data, $this->input->post('ENu_Tipo_Cuenta'), $this->input->post('ENu_Cuenta_Contable'))
		:
			$this->ConfiguracionContableModel->agregarConfiguracionContable($data)
		);
	}
    
	public function eliminarConfiguracionContable($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->ConfiguracionContableModel->eliminarConfiguracionContable($this->security->xss_clean($ID)));
	}
}
