<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfiguracionContableProductoController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Logistica/ReglasLogistica/ConfiguracionContableProductoModel');
	}
	
	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/ReglasLogistica/ConfiguracionContableProductoView');
			$this->load->view('footer', array("js_configuracion_contable_producto" => true));
		}
	}

	public function ajax_list(){
		$arrData = $this->ConfiguracionContableProductoModel->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		$action = 'delete';
		foreach ($arrData as $row) {
			$no++;
			$rows = array();
			$rows[] = ($row->Nu_Tipo_Cuenta == 0 ? 'Debe' : 'Haber');
			$rows[] = $row->Nu_Cuenta_Contable;
			$rows[] = $row->No_Producto;
			$rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verConfiguracionContableProducto(\'' . $row->ID_Configuracion_Cuenta_Contable_Producto . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarConfiguracionContableProducto(\'' . $row->ID_Configuracion_Cuenta_Contable_Producto . '\', \'' . $action . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
			$data[] = $rows;
		}
		$output = array(
			'draw' => $this->input->post('draw'),
			'recordsTotal' => $this->ConfiguracionContableProductoModel->count_all(),
			'recordsFiltered' => $this->ConfiguracionContableProductoModel->count_filtered(),
			'data' => $data,
		);
		echo json_encode($output);
	}

	public function ajax_edit($ID){
        echo json_encode($this->ConfiguracionContableProductoModel->get_by_id($this->security->xss_clean($ID)));
    }
    
	public function crudConfiguracionContableProducto(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		$data = array(
			'ID_Empresa' => $this->input->post('ID_Empresa'),
			'ID_Configuracion_Cuenta_Contable' => $this->input->post('ID_Configuracion_Cuenta_Contable'),
			'ID_Producto' => $this->input->post('ID_Producto'),
			'Nu_Estado'	=> $this->input->post('Nu_Estado'),
		);
		echo json_encode(
		($this->input->post('EID_Configuracion_Cuenta_Contable_Producto') != '') ?
			$this->ConfiguracionContableProductoModel->actualizarConfiguracionContableProducto(array('ID_Configuracion_Cuenta_Contable_Producto' => $this->input->post('EID_Configuracion_Cuenta_Contable_Producto')), $data, $this->input->post('EID_Configuracion_Cuenta_Contable'), $this->input->post('EID_Producto'))
		:
			$this->ConfiguracionContableProductoModel->agregarConfiguracionContableProducto($data)
		);
	}
    
	public function eliminarConfiguracionContableProducto($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->ConfiguracionContableProductoModel->eliminarConfiguracionContableProducto($this->security->xss_clean($ID)));
	}
}
