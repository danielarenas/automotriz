<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class OrdenCompraController extends CI_Controller {
	private $file_path = './assets/images/logos/';
	
	function __construct(){  
    	parent::__construct();	
		$this->load->library('session'); 
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Logistica/OrdenCompraModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
	}

	public function listarOrdenesCompra(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/OrdenCompraView');
			$this->load->view('footer', array("js_orden_compra" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->OrdenCompraModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
			$rows[] = $row->ID_Numero_Documento;
            $rows[] = ToDateBD($row->Fe_Emision);			 
			$rows[] = $row->No_Entidad;
			$rows[] = $row->No_Placa_Vehiculo;
			// OC_MODAL_6 - INICIO
            // $rows[] = $row->Nu_Presupuesto 
            // $rows[] = $row->Nu_Orden_Ingreso;
			$presupuesto_fld='';
			$ordeningreso_fld='';
			if ($row->Serie_Presup != '' && $row->Num_Serie_Presup != ''){
				$presupuesto_fld = $row->Serie_Presup.'-'.$row->Num_Serie_Presup ;
			}
			if ($row->Serie_OI != '' && $row->Num_Serie_OI != ''){
				$ordeningreso_fld = $row->Serie_OI.'-'.$row->Num_Serie_OI ;
			}
            $rows[] = $presupuesto_fld;
            $rows[] = $ordeningreso_fld;
			// OC_MODAL_6 - FIN
            $rows[] = numberFormat($row->Ss_SubTotal, 2, '.', ',');
            $rows[] = numberFormat($row->Ss_Impuesto, 2, '.', ',');
            $rows[] = numberFormat($row->Ss_Total, 2, '.', ',');
			$labelEstadoOC = "";
			switch($row->Nu_Estado_OC) {
				case 1: $labelEstadoOC = "Generada";break;
				case 2: $labelEstadoOC = "Recibida";break;
			}
            $rows[] = '<span class="label label-success">'.$labelEstadoOC.'</span>';
			$rows[] = ToDateBD($row->Fe_Recepcion);
			$labelEstadoConta = "";
			switch($row->Nu_Estado_Conta) {
				case 1: $labelEstadoConta = "Pendiente";break;
				case 2: $labelEstadoConta = "Observada";break;
				case 3: $labelEstadoConta = "Pagada";break;
			}
				// 03_08-3 INICIO 
				$links_habilitado=1;
				if ($row->Nu_Estado_Conta == 3 ) {
					$links_habilitado=0;
				}
				// 03_08-3 FIN
            $rows[] = '<span class="label label-success">'.$labelEstadoConta.'</span>';
			
			// OC_MODAL_5-1 - INICIO 
			 $Estado_Documento_OC = '';
			if ($row->Estado_Documento == 1) {
				$Estado_Documento_OC = 'Activo';
			} else{
				// 09_08 INICIO
				$Estado_Documento_OC = 'Anulado';
				// 09_08 FIN 
			}
			$rows[] = $Estado_Documento_OC;  
			// OC_MODAL_5-1 - FIN 

			$rows[] = ToDateBD($row->Fe_Pago);
			// 03_08-4 INICIO 
			$rows[] = $row->Nu_Operacion;
			// 03_08-4 FIN
            $rows[] = '<button class="btn btn-xs btn-link" alt="PDF" title="PDF" href="javascript:void(0)" onclick="pdfOrdenCompra(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>';
			if ($row->Nu_Estado == 5 || $row->Nu_Estado == 0 || $row->Nu_Estado == 1) {
				// 03_08-3 INICIO 
				// OC_MODAL_5-2 - INICIO
				// $rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarOrdenCompra(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
				if ($links_habilitado == 1 ) {
					if ($row->Estado_Documento == 1 ) {
						
							$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verOrdenCompra(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
							$rows[] = '<button class="btn btn-xs btn-link" alt="Anular" title="Anular" href="javascript:void(0)" onclick="eliminarOrdenCompra(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Anular</i></button>';
						}else{
							$rows[] = ''; 
							$rows[] = ''; 
						
					}	
				}else{
					$rows[] = ''; 
					$rows[] = ''; 
				}


				// OC_MODAL_5-2 - FIN
				// 03_08-3 FIN
			} else {
				$rows[] = '';
				$rows[] = '';
			}
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->OrdenCompraModel->count_all(),
	        'recordsFiltered' => $this->OrdenCompraModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
    
	public function ajax_edit($ID){
        $data = $this->OrdenCompraModel->get_by_id($this->security->xss_clean($ID));
        $arrImpuesto = $this->HelperModel->getImpuestos($arrPost = '');
        $output = array(
        	'arrEdit' => $data,
        	'arrImpuesto' => $arrImpuesto,
        );
        echo json_encode($output);
    }
    
	public function crudOrdenCompra(){

		
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');

		$arrProveedorNuevo = '';
		if (isset($_POST['arrProveedorNuevo'])){
			$arrProveedorNuevo = array(
				'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrProveedorNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrProveedorNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad'					=> $this->security->xss_clean($_POST['arrProveedorNuevo']['No_Entidad']),
				'Txt_Direccion_Entidad'			=> $this->security->xss_clean($_POST['arrProveedorNuevo']['Txt_Direccion_Entidad']),
				'Nu_Telefono_Entidad'			=> $this->security->xss_clean($_POST['arrProveedorNuevo']['Nu_Telefono_Entidad']),
				'Nu_Celular_Entidad'			=> $this->security->xss_clean($_POST['arrProveedorNuevo']['Nu_Celular_Entidad']),
			);
		}

		$arrContactoNuevo = '';
		if (isset($_POST['arrContactoNuevo'])){
			$Nu_Telefono_Contacto = '';
			if ( $_POST['arrContactoNuevo']['Nu_Telefono_Entidad'] && strlen($_POST['arrContactoNuevo']['Nu_Telefono_Entidad']) === 8){
		        $Nu_Telefono_Contacto = explode(' ', $_POST['arrContactoNuevo']['Nu_Telefono_Entidad']);
		        $Nu_Telefono_Contacto = $Nu_Telefono_Contacto[0].$Nu_Telefono_Contacto[1];
			}
			
			$Nu_Celular_Contacto = '';
			if ( $_POST['arrContactoNuevo']['Nu_Celular_Entidad'] && strlen($_POST['arrContactoNuevo']['Nu_Celular_Entidad']) === 11){
		        $Nu_Celular_Contacto = explode(' ', $_POST['arrContactoNuevo']['Nu_Celular_Entidad']);
		        $Nu_Celular_Contacto = $Nu_Celular_Contacto[0].$Nu_Celular_Contacto[1].$Nu_Celular_Contacto[2];
			}
			$arrContactoNuevo = array(
				'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrContactoNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrContactoNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad'					=> $this->security->xss_clean($_POST['arrContactoNuevo']['No_Entidad']),
				'Nu_Telefono_Entidad'			=> $Nu_Telefono_Contacto,
				'Nu_Celular_Entidad'			=> $Nu_Celular_Contacto,
				'Txt_Email_Entidad'				=> $this->security->xss_clean($_POST['arrContactoNuevo']['Txt_Email_Entidad']),
			);
		}
		
		$iDescargarStock = $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Nu_Descargar_Inventario']);

		$arrOrdenCompraCabecera = array(
			'ID_Empresa'					=> $this->empresa->ID_Empresa,
			'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
			'ID_Tipo_Asiento'				=> 2,//Compra
			'ID_Tipo_Documento'				=> 12,//Proforma Compra
			'ID_Serie_Documento'			=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Serie_Documento']),
			'ID_Serie_Documento_PK'			=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Serie_Documento_PK']),
			'ID_Numero_Documento'			=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Numero_Documento']),
			'Nu_Correlativo'				=> 0,
			'Fe_Emision'					=> ToDate($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Fe_Emision'])),
			'Fe_Vencimiento'				=> ToDate($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Fe_Vencimiento'])),
			'Fe_Recepcion'					=> ToDate($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Fe_Recepcion'])),
			'Fe_Pago'						=> ToDate($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Fe_Pago'])),
			'ID_Moneda'						=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Moneda']),
			'ID_Medio_Pago'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Medio_Pago']),
			'Nu_Descargar_Inventario'		=> $iDescargarStock,
			'ID_Entidad'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Entidad']),
			'ID_Producto'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Producto']),
			'ID_Contacto'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Contacto']),
			'Txt_Garantia'					=> nl2br($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Txt_Garantia'])),
			'Txt_Glosa'						=> nl2br($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Txt_Glosa'])),
			'Txt_Comentario'				=> nl2br($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Txt_Comentario'])),
			'Po_Descuento'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Po_Descuento']),
			'Ss_Descuento'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Ss_Descuento']),
			'Ss_Total'						=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Ss_Total']),
			'Ss_SubTotal'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Ss_SubTotal']),
			'Ss_Impuesto'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Ss_Impuesto']),
			'Nu_Estado'						=> ($this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ENu_Estado']) != '' ? $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ENu_Estado']) : 5),
			'Nu_Orden_Ingreso'				=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Nu_Orden_Ingreso']),
			'Nu_Presupuesto'				=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Nu_Presupuesto']),
			'Nu_Operacion'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Nu_Operacion']),
			'Nu_Estado_Conta'				=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Nu_Estado_Conta']),
			'Nu_Estado_OC'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['Nu_Estado_OC']),
			'ServicioBien'					=> $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ServicioBien']),
			// OC_MODAL_5-1 - INICIO
			'Estado_Documento'				=> 1 ,
			// OC_MODAL_5-1 - FIN
		);

		if ( $_POST['arrOrdenCompraCabecera']['ID_Lista_Precio_Cabecera'] != 0 )
			$arrOrdenCompraCabecera = array_merge($arrOrdenCompraCabecera, array("ID_Lista_Precio_Cabecera" => $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Lista_Precio_Cabecera'])));

		if ( !empty($_POST['arrOrdenCompraCabecera']['ID_Almacen']) ){
			$arrOrdenCompraCabecera = array_merge($arrOrdenCompraCabecera, array("ID_Almacen" => $_POST['arrOrdenCompraCabecera']['ID_Almacen']));
		}
			
		if ( $iDescargarStock == 1 ) {
			$arrOrdenCompraCabecera = array_merge($arrOrdenCompraCabecera, array("ID_Almacen" => $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['ID_Almacen'])));
		}

		echo json_encode(
		( $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['EID_Empresa']) != '' && $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['EID_Documento_Cabecera']) != '') ?
			$this->actualizarCompra_Inventario(array('ID_Empresa' => $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['EID_Empresa']), 'ID_Documento_Cabecera' => $this->security->xss_clean($_POST['arrOrdenCompraCabecera']['EID_Documento_Cabecera'])), $arrOrdenCompraCabecera, $_POST['arrDetalleOrdenCompra'], $arrOrdenCompraCabecera['Nu_Descargar_Inventario'], $arrProveedorNuevo, $arrContactoNuevo)
		:
			$this->agregarCompra_Inventario($arrOrdenCompraCabecera, $_POST['arrDetalleOrdenCompra'], $arrOrdenCompraCabecera['Nu_Descargar_Inventario'], $arrProveedorNuevo, $arrContactoNuevo)
		);
	} 

	public function agregarCompra_Inventario($arrOrdenCompraCabecera = '', $arrDetalleOrdenCompra = '', $Nu_Descargar_Inventario = '', $arrProveedorNuevo = '', $arrContactoNuevo = ''){
		$responseCompra = $this->OrdenCompraModel->agregarCompra($arrOrdenCompraCabecera, $arrDetalleOrdenCompra, $arrProveedorNuevo, $arrContactoNuevo);
		if ($responseCompra['status'] == 'success') {
 
			// OC_MODAL_6-2 - INICIO
			// OC_MODAL_7 - INICIO
			// 03_08-5 INICIO
			// var_dump($_POST['Creado_Por'] .'-'.$_POST['Usuario_Creador'].'-'.$responseCompra['Last_ID_Documento_Cabecera']);
			if ($_POST['Creado_Por'] == 'Y' ){				
				$AgregaUser = $this->OrdenCompraModel->Cust_AgregaUsuarioCreador( $_POST['Usuario_Creador'], $responseCompra['Last_ID_Documento_Cabecera']);
			}
			// OC_MODAL_7 - FIN
			// OC_MODAL_6-2 - FIN
			// 03_08-5 FIN
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrOrdenCompraCabecera['ID_Tipo_Movimiento'] = 2;//Compra
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrOrdenCompraCabecera['ID_Almacen'], $responseCompra['Last_ID_Documento_Cabecera'], 0, $arrDetalleOrdenCompra, $arrOrdenCompraCabecera['ID_Tipo_Movimiento'], 0, '', 0, 1);
			}
			
			return $responseCompra;
		} else {
			return $responseCompra;
		}
	}

	public function actualizarCompra_Inventario($arrWhereCompra = '', $arrOrdenCompraCabecera = '', $arrDetalleOrdenCompra = '', $Nu_Descargar_Inventario = '', $arrProveedorNuevo = '', $arrContactoNuevo){
		$responseCompra = $this->OrdenCompraModel->actualizarCompra($arrWhereCompra, $arrOrdenCompraCabecera, $arrDetalleOrdenCompra, $arrProveedorNuevo, $arrContactoNuevo);
		if ($responseCompra['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrOrdenCompraCabecera['ID_Tipo_Movimiento'] = 2;//Compra
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrOrdenCompraCabecera['ID_Almacen'], $responseCompra['Last_ID_Documento_Cabecera'], 0, $arrDetalleOrdenCompra, $arrOrdenCompraCabecera['ID_Tipo_Movimiento'], 1, $arrWhereCompra, 0, 1);
			}
			return $responseCompra;
		} else {
			return $responseCompra;
		}
	}
	
	public function eliminarOrdenCompra($ID, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenCompraModel->eliminarOrdenCompra($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	
	public function estadoOrdenCompra($ID, $Nu_Descargar_Inventario, $Nu_Estado){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenCompraModel->estadoOrdenCompra($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario), $this->security->xss_clean($Nu_Estado)));
	}
	
	public function duplicarOrdenCompra($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenCompraModel->duplicarOrdenCompra($this->security->xss_clean($ID)));
	}

	public function getOrdenCompraPDF($ID){
        $data = $this->OrdenCompraModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('EnLetras', 'el');
		$EnLetras = new EnLetras();
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Logistica/pdf/orden_compra_view', array(
			'arrData' => $data,
			'user' => $this->user,
			'totalEnLetras'	=> $EnLetras->ValorEnLetras($data[0]->Ss_Total, $data[0]->No_Moneda),
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - orden compra ' . $data[0]->ID_Numero_Documento);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
		
		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='logomafisa.jpeg';
		}

		$pdf->Image($this->file_path . $sNombreLogo, 60, 11, 14, 14, '', '', '', false, 300, '', false, false, 0);

        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_orden_compra_ Nro. " . $data[0]->ID_Numero_Documento . ".pdf";
		$pdf->Output($file_name, 'I');
	}
}