<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class StockValorizadoController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Logistica/informes_logistica/StockValorizadoModel');
	}

	public function reporte(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/informes_logistica/StockValorizadoView');
			$this->load->view('footer', array("js_stock_valorizado" => true));
		}
	}
	
    private function getReporte($ID_Empresa, $ID_Almacen, $iTipoFecha, $Fe_Inicio, $Fe_Fin, $iTipoStock, $ID_Linea, $ID_Producto){
        if ($iTipoFecha === '0') {//tabla stock_producto
            $arrData = $this->StockValorizadoModel->getStockValorizado($ID_Empresa, $ID_Almacen, $iTipoStock, $ID_Linea, $ID_Producto);
            $data = array();
            foreach ($arrData as $row) {
                $rows = array();
                $rows['ID_Linea'] = $row->ID_Linea;
                $rows['No_Linea'] = $row->No_Linea;
                $rows['ID_Producto'] = $row->ID_Producto;
                $rows['Nu_Codigo_Barra'] = $row->Nu_Codigo_Barra;
                $rows['No_Producto'] = $row->No_Producto;
                $rows['No_Unidad_Medida'] = $row->No_Unidad_Medida;
                $rows['Qt_Producto'] = $row->Qt_Producto;
                $data[] = (object)$rows;
            }
        } else {//tabla documento y guia
            $arrData = $this->StockValorizadoModel->getStockValorizadoxFecha($ID_Empresa, $ID_Almacen, $Fe_Inicio, $Fe_Fin, $ID_Linea, $ID_Producto);
            $data = array();
            foreach ($arrData as $row) {
                $rows = array();
                $rows['ID_Linea'] = $row->ID_Linea;
                $rows['No_Linea'] = $row->No_Linea;
                $rows['ID_Producto'] = $row->ID_Producto;
                $rows['Nu_Codigo_Barra'] = $row->Nu_Codigo_Barra;
                $rows['No_Producto'] = $row->No_Producto;
                $rows['No_Unidad_Medida'] = $row->No_Unidad_Medida;
                $rows['Qt_Producto'] = $this->StockValorizadoModel->getStockValorizadoxProducto($ID_Empresa, $ID_Almacen, $Fe_Inicio, $Fe_Fin, $row->ID_Producto);
                if ($iTipoFecha === '1' && $iTipoStock === '0' && $rows['Qt_Producto'] > 0)
                    $data[] = (object)$rows;
                else
                    $data[] = (object)$rows;
            }
        }
        return $data;
    }
    
	public function sendReporte(){
        echo json_encode(
            $this->getReporte(
                $this->user->ID_Empresa,
                $this->input->post('ID_Almacen'),
                $this->input->post('iTipoFecha'),
                $this->input->post('Fe_Inicio'),
                $this->input->post('Fe_Fin'),
                $this->input->post('iTipoStock'),
                $this->input->post('ID_Linea'),
                $this->input->post('ID_Producto')
            )
        );
    }
    
	public function sendReportePDF($ID_Almacen, $iTipoFecha, $Fe_Inicio, $Fe_Fin, $iTipoStock, $ID_Linea, $ID_Producto, $No_Almacen){
        $this->load->library('FormatoLibroSunatPDF');
		
        $ID_Empresa             = $this->user->ID_Empresa;
        $ID_Almacen             = $this->security->xss_clean($ID_Almacen);
        $iTipoFecha             = $this->security->xss_clean($iTipoFecha);
        $Fe_Inicio              = $this->security->xss_clean($Fe_Inicio);
        $Fe_Fin                 = $this->security->xss_clean($Fe_Fin);
        $iTipoStock             = $this->security->xss_clean($iTipoStock);
        $ID_Linea               = $this->security->xss_clean($ID_Linea);
        $ID_Producto            = $this->security->xss_clean($ID_Producto);
        $No_Almacen             = $this->security->xss_clean($No_Almacen);
        
		$fileNamePDF = "Stock_Valorizado_" . $Fe_Inicio . "_" . $Fe_Fin . ".pdf";
        
		$pdf = new FormatoLibroSunatPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$arrFecha = localtime(time(),true);

		$iYear = (1900 + $arrFecha['tm_year']);
		$iMonth = (strlen(1 + $arrFecha['tm_mon']) > 1 ? $arrFecha['tm_mon'] : '0' . (1 + $arrFecha['tm_mon']));
		$iDay = (strlen($arrFecha['tm_mday']) > 1 ? $arrFecha['tm_mday'] : '0' . $arrFecha['tm_mday']);
		
    	$Fe_Hoy = $iDay . '/' . $iMonth . '/' . $iYear;
    	
        $arrCabecera = array (
            "No_Almacen" => $No_Almacen,
            "Fe_Inicio" => ($iTipoFecha === '0' ? $Fe_Hoy : ToDateBD($Fe_Inicio)),
            "Fe_Fin" => ($iTipoFecha === '0' ? $Fe_Hoy : ToDateBD($Fe_Fin)),
        );
        
		ob_start();
		$file = $this->load->view('Logistica/informes_logistica/StockValorizadoPDF', array(
			'arrCabecera' => $arrCabecera,
			'arrDetalle' => $this->getReporte($ID_Empresa, $ID_Almacen, $iTipoFecha, $Fe_Inicio, $Fe_Fin, $iTipoStock, $ID_Linea, $ID_Producto),
		));
		$html = ob_get_contents();
		ob_end_clean();

        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
        $pdf->setFont('helvetica', '', 5);
        
		$pdf->AddPage('P', 'A4');
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Output($fileNamePDF, 'I');
	}
    
	public function sendReporteEXCEL($ID_Almacen, $iTipoFecha, $Fe_Inicio, $Fe_Fin, $iTipoStock, $ID_Linea, $ID_Producto, $No_Almacen){
        $this->load->library('Excel');
	    
        $ID_Empresa             = $this->user->ID_Empresa;
        $ID_Almacen             = $this->security->xss_clean($ID_Almacen);
        $iTipoFecha             = $this->security->xss_clean($iTipoFecha);
        $Fe_Inicio              = $this->security->xss_clean($Fe_Inicio);
        $Fe_Fin                 = $this->security->xss_clean($Fe_Fin);
        $iTipoStock             = $this->security->xss_clean($iTipoStock);
        $ID_Linea               = $this->security->xss_clean($ID_Linea);
        $ID_Producto            = $this->security->xss_clean($ID_Producto);
        $No_Almacen             = $this->security->xss_clean($No_Almacen);
        
		$arrFecha = localtime(time(),true);

		$iYear = (1900 + $arrFecha['tm_year']);
		$iMonth = (strlen(1 + $arrFecha['tm_mon']) > 1 ? $arrFecha['tm_mon'] : '0' . (1 + $arrFecha['tm_mon']));
		$iDay = (strlen($arrFecha['tm_mday']) > 1 ? $arrFecha['tm_mday'] : '0' . $arrFecha['tm_mday']);
		
    	$Fe_Hoy = $iDay . '/' . $iMonth . '/' . $iYear;
    	
		$fileNameExcel = "Stock_Valorizado_" . ($iTipoFecha === '0' ? $Fe_Hoy : ToDateBD($Fe_Inicio)) . "_" . ($iTipoFecha === '0' ? $Fe_Hoy : ToDateBD($Fe_Fin)) . ".xls";
		
        $data = $this->getReporte($ID_Empresa, $ID_Almacen, $iTipoFecha, $Fe_Inicio, $Fe_Fin, $iTipoStock, $ID_Linea, $ID_Producto);

	    $objPHPExcel = new PHPExcel();
	    
	    $objPHPExcel->getActiveSheet()->setTitle('Stock Valorizado');
        
	    $hoja_activa = 0;
	    
        $BStyle_top = array(
          'borders' => array(
            'top' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_left = array(
          'borders' => array(
            'left' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_right = array(
          'borders' => array(
            'right' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_bottom = array(
          'borders' => array(
            'bottom' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $style_align_center = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        
        $style_align_right = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );
        
        $style_align_left = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        
	    //Title
	    $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex($hoja_activa)
        ->setCellValue('A1', $this->empresa->No_Empresa)
        ->setCellValue('D1', $No_Almacen)
        ->setCellValue('B2', 'Stock Valorizado')
        ->setCellValue('B3', 'Desde: ' . ($iTipoFecha === '0' ? $Fe_Hoy : ToDateBD($Fe_Inicio)) . ' Hasta: ' . ($iTipoFecha === '0' ? $Fe_Hoy : ToDateBD($Fe_Fin)));
        
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($style_align_center);
        $objPHPExcel->getActiveSheet()->getStyle('B3')->applyFromArray($style_align_center);
        $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('B2:C2');
        $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('B3:C3');
        $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        
	    //Header
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth("15");
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("30");
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("15");
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth("15");

        $objPHPExcel->getActiveSheet()->getStyle('A5:D5')->applyFromArray($BStyle_top);
        $objPHPExcel->getActiveSheet()->getStyle('A5:D5')->applyFromArray($BStyle_bottom);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($BStyle_right);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5:D5')->getFont()->setBold(true);
        
        $objPHPExcel->setActiveSheetIndex($hoja_activa)
        ->setCellValue('A5', 'Código Barra')
        ->setCellValue('B5', 'Nombre')
        ->setCellValue('C5', 'Unidad Medida')
        ->setCellValue('D5', 'Stock')
        ;
        
        $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($style_align_center);
        $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($style_align_center);
        $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($style_align_center);
        $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($style_align_center);
        
        $objPHPExcel->getActiveSheet()->freezePane('A6');//LINEA HORIZONTAL PARA SEPARAR CABECERA Y DETALLE
        
        $fila = 6;
        
        if ( count($data) > 0) {
            $ID_Linea = '';
            $counter = 0;
            $sum_linea_cantidad = 0.000000;
            $sum_cantidad = 0.000000;
            foreach ($data as $row) {
                if ($ID_Linea != $row->ID_Linea) {
                    if ($counter != 0) {
                        $objPHPExcel->setActiveSheetIndex($hoja_activa)
                        ->setCellValue('C' . $fila, 'Total Linea')
                        ->setCellValue('D' . $fila, numberFormat($sum_linea_cantidad, 6, '.', ','));
                        
                        $objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':' . 'D' . $fila)->applyFromArray($style_align_right);
                        
                        $objPHPExcel->getActiveSheet()
                        ->getStyle('A' . $fila . ':' . 'D' . $fila)
                        ->applyFromArray(
                            array(
                                'fill' => array(
                                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                    'color' => array('rgb' => 'E7E7E7')
                                )
                            )
                        );
                        $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':' . 'D' . $fila)->getFont()->setBold(true);
                    
                        $fila++;
                        
                        $sum_linea_cantidad = 0.000000;
                    }
                    
                    $objPHPExcel->setActiveSheetIndex($hoja_activa)
                    ->setCellValue('A' . $fila, 'Linea')
                    ->setCellValue('B' . $fila, $row->No_Linea)
                    ;
                    
                    $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('A' . $fila . ':' . 'D' . $fila);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':' . 'D' . $fila)->applyFromArray($style_align_left);
                    
                    $objPHPExcel->getActiveSheet()
                    ->getStyle('A' . $fila . ':' . 'D' . $fila)
                    ->applyFromArray(
                        array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'F2F5F5')
                            )
                        )
                    );
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':' . 'D' . $fila)->getFont()->setBold(true);
                    
                    $ID_Linea = $row->ID_Linea;
                    $fila++;
                }

                $objPHPExcel->setActiveSheetIndex($hoja_activa)
                ->setCellValue('A' . $fila, $row->Nu_Codigo_Barra)
                ->setCellValue('B' . $fila, $row->No_Producto)
                ->setCellValue('C' . $fila, $row->No_Unidad_Medida)
                ->setCellValue('D' . $fila, numberFormat($row->Qt_Producto, 6, '.', ','))
                ;
                
                $objPHPExcel->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($style_align_center);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $fila)->applyFromArray($style_align_left);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $fila)->applyFromArray($style_align_center);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $fila)->applyFromArray($style_align_right);
            
                $sum_linea_cantidad += $row->Qt_Producto;
                $sum_cantidad += $row->Qt_Producto;
                $fila++;
                $counter++;
            }
            
            $objPHPExcel->setActiveSheetIndex($hoja_activa)
            ->setCellValue('C' . $fila, 'Total Linea')
            ->setCellValue('D' . $fila, numberFormat($sum_linea_cantidad, 6, '.', ','));
            
            $objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':' . 'D' . $fila)->applyFromArray($style_align_right);

            $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $fila . ':' . 'D' . $fila)
            ->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'E7E7E7')
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':' . 'D' . $fila)->getFont()->setBold(true);
            
            $fila++;
            $objPHPExcel->setActiveSheetIndex($hoja_activa)
            ->setCellValue('C' . $fila, 'Total General')
            ->setCellValue('D' . $fila, numberFormat($sum_cantidad, 6, '.', ','));
            
            $objPHPExcel->getActiveSheet()->getStyle('C' . $fila . ':' . 'D' . $fila)->applyFromArray($style_align_right);

            $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $fila . ':' . 'D' . $fila)
            ->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'E7E7E7')
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':' . 'D' . $fila)->getFont()->setBold(true);
        }
        
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $fileNameExcel . '"');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
	}
}
