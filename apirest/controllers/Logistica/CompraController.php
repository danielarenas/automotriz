<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class CompraController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Logistica/CompraModel');
		$this->load->model('Logistica/MovimientoInventarioModel');

		// STOCK_PROD_UPDT_2 - INICIO
		$this->load->model('Logistica/SalidaInventarioModel');
		// STOCK_PROD_UPDT_2 - FIN

	}

	public function listarCompras(){
		if(!$this->MenuModel->verificarAccesoMenu()) {
			redirect('Inicio/InicioView');
		}
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/CompraView');
			$this->load->view('footer', array("js_compra" => true));
		}
	}

	public function download_asiento() {
		$this->load->library('Excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle('Asiento de Compras');
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth("7");
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("7");
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("8");
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth("12");
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth("8");
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth("15");
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('25');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('15');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth("12");
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth("18");
		$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth("16");
		$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth("15");
		$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth("15");
		$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth("15");
		$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth("15");
		$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth("15");
		$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth("10");
		$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth("12");
		$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth("10");

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AG1');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => '002060')));
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setRGB ('FFFFFF');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'ASIENTO DE COMPRAS');

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'comp_i');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 't_comp');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', 'serie');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', 'numero');
		$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', 'f_emision');
		$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', 't_doc');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G2', 'numero_doc');
		$objPHPExcel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H2', 'cliente');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I2', 'concepto');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J2', 't_moneda');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K2', 'sub_total');
		$objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L2', 'igv');
		$objPHPExcel->getActiveSheet()->getStyle('L2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M2', 'inafecto');
		$objPHPExcel->getActiveSheet()->getStyle('M2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N2', 'total');
		$objPHPExcel->getActiveSheet()->getStyle('N2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O2', 'estado');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P2', 't_comp_r');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q2', 'serie_r');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R2', 'numero_r');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S2', 'f_emision_r');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T2', 'lin_negocio');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U2', 'cen_costo');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V2', 'ap');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('W2', 'cuenta');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X2', 'destino');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y2', 'f_vencimiento');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z2', 'f_detraccion');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA2', 'numero_detr');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB2', 'monto_detr');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC2', 'porcen_detr');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD2', 'f_contable');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE2', 'anio_dua');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF2', 'com_no_domi');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AG2', 't_cambio');

		$objPHPExcel->getActiveSheet()->getStyle('A2:AG2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A2:AG2')->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => '4472c4')));
		$objPHPExcel->getActiveSheet()->getStyle('A2:AG2')->getFont()->getColor()->setRGB ('FFFFFF');

		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
		$fila = 2;
		$arrData = $this->CompraModel->_get_asiento_query();

		foreach ($arrData as $row) {
			$fila++;
			$varCompInterno = '';
			if($row->No_Signo === 'S/' && $row->No_Tipo_Documento_Breve === 'Factura') {
				$varCompInterno = 'I11';
			} else if($row->No_Signo === 'US$' && $row->No_Tipo_Documento_Breve === 'Factura') {
				$varCompInterno = 'I12';
			} else if($row->No_Signo === 'S/' && $row->No_Tipo_Documento_Breve === 'N/Crédito') {
				$varCompInterno = 'S11';
			} else if($row->No_Signo === 'US$' && $row->No_Tipo_Documento_Breve === 'N/Crédito') {
				$varCompInterno = 'S12';
			}

			$var_t_comp = '00';
			if($row->No_Tipo_Documento_Breve === 'Factura') {
				$var_t_comp = '01';
			} else if($row->No_Tipo_Documento_Breve === 'B/Venta') {
				$var_t_comp = '03';
			} else if($row->No_Tipo_Documento_Breve === 'N/Crédito') {
				$var_t_comp = '07';
			} else if($row->No_Tipo_Documento_Breve === 'N/Débito') {
				$var_t_comp = '06';
			}

			/* $var_t_doc = '00';
			if($row->No_Tipo_Documento_Identidad_Breve === 'DNI') {
				$var_t_doc = '01';
			} else if($row->No_Tipo_Documento_Identidad_Breve === 'RUC') {
				$var_t_doc = '06';
			} */

			// $NCSigno = $row->No_Tipo_Documento_Breve === 'N/Crédito' ? '-' : '';
			$noteOfCredit = [];
			if ($row->IDTipo_Documento == 5 || $row->IDTipo_Documento == 6) {
				$noteOfCredit = $this->CompraModel->getDocumentoCabeceraOfNoteOfCredit($row->ID_Documento_Cabecera);
				if (!$noteOfCredit) {
					$noteOfCredit = null;
				}
			}
			
			$var_t_compv2 = '';
			if($noteOfCredit->No_Tipo_Documento_Breve === 'Factura') {
				$var_t_compv2 = '01';
			} else if($noteOfCredit->No_Tipo_Documento_Breve === 'B/Venta') {
				$var_t_compv2 = '03';
			} else if($noteOfCredit->No_Tipo_Documento_Breve === 'N/Crédito') {
				$var_t_compv2 = '07';
			} else if($noteOfCredit->No_Tipo_Documento_Breve === 'N/Débito') {
				$var_t_compv2 = '06';
			}

			$startOIDetalle = substr($row->ID_OI_Detalle, 0, 1);
			$var_lin_negocio = '';
			if($startOIDetalle) {
				$var_lin_negocio = $row->ID_OI_Detalle;
			} else {
				$var_lin_negocio = $row->Nu_Codigo_Barra;
			}
			$datef_emision = date_create($row->Fe_Emision);
			$datef_emisionv2 = date_create($noteOfCredit->Fe_Emision);
			$datef_vencimiento = date_create($row->Fe_Vencimiento);
			$datef_periodo = date_create($row->Fe_Periodo);
			$fecha_emisionv2 = date_format($datef_emisionv2,"d/m/Y");
			$fecha_emision = date_format($datef_emision,"d/m/Y");
			$fecha_vencimiento = date_format($datef_vencimiento,"d/m/Y");
			$fecha_periodo = date_format($datef_periodo,"d/m/Y");

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $fila, $varCompInterno)
				->setCellValue('B' . $fila, $var_t_comp)
				->setCellValue('C' . $fila, $row->ID_Serie_Documento)
				->setCellValue('D' . $fila, $row->ID_Numero_Documento)
				->setCellValue('E' . $fila, $fecha_emision)
				->setCellValue('F' . $fila, $row->No_Tipo_Documento_Identidad_Breve)
				->setCellValue('G' . $fila, $row->Nu_Documento_Identidad)
				->setCellValue('H' . $fila, $row->No_Entidad)
				->setCellValue('I' . $fila, $row->NO_Producto)
				->setCellValue('J' . $fila, $row->No_Signo === 'S/' ? 'S' : 'D')
				->setCellValue('K' . $fila, numberFormat($row->Ss_SubTotal, 2, '.', ','))
				->setCellValue('L' . $fila, numberFormat($row->Ss_TotalD - $row->Ss_SubTotal, 2, '.', ','))
				->setCellValue('N' . $fila, numberFormat($row->Ss_TotalD, 2, '.', ','))
				->setCellValue('O' . $fila, $row->Nu_Estado == 6 ? 'Activo' : ($row->Nu_Estado == 7 ? 'Anulado' : ''))
				->setCellValue('P' . $fila, $var_t_compv2 ?? '')
				->setCellValue('Q' . $fila, $noteOfCredit ? $noteOfCredit->ID_Serie_Documento : '')
				->setCellValue('R' . $fila, $noteOfCredit ? $noteOfCredit->ID_Numero_Documento : '')
				->setCellValue('S' . $fila, $noteOfCredit ? $fecha_emisionv2 : '')
				->setCellValue('T' . $fila, $var_lin_negocio) // AGREGADO
				->setCellValue('U' . $fila, $row->centro_costo ?? '130')
				->setCellValue('V'.  $fila, 0)
				->setCellValue('W' . $fila, $row->Nu_Cuenta_Contable)
				->setCellValue('Y' . $fila, $row->No_Medio_Pago == 'Contado' ? $fecha_emision : $fecha_vencimiento)
				->setCellValue('AD' . $fila, $fecha_periodo ? $fecha_periodo : $fecha_emision);
				// $row->ID_OI_Detalle

				$objPHPExcel->getActiveSheet()->getStyle('D' . $fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle('G' . $fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle('I' . $fila)->getAlignment()->setIndent(1);
				$objPHPExcel->getActiveSheet()->getStyle('H' . $fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objPHPExcel->getActiveSheet()->getStyle('K' . $fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle('L' . $fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle('M' . $fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objPHPExcel->getActiveSheet()->getRowDimension($fila)->setRowHeight(20);
		}

		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="asiento_compra.xls"');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		ob_start();
		$objWriter->save("php://output");
		$xlsData = ob_get_contents();
		ob_end_clean();

		$response =  [
			'op' => 'ok',
			'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
    ];

		die(json_encode($response));
	}

	public function ajax_list(){
		$arrData = $this->CompraModel->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		$action_anular = 'anular';
		$action_delete = 'delete';
		foreach ($arrData as $row) {
			$no++;
			$rows = array();
			$rows[] = $row->Nu_Correlativo_Compra;
			$rows[] = ToDateBD($row->Fe_Emision);
			$rows[] = $row->No_Tipo_Documento_Breve;
			$rows[] = $row->ID_Serie_Documento;
			$rows[] = $row->ID_Numero_Documento;
			$rows[] = $row->No_Entidad;
			$rows[] = $row->No_Signo;
			$rows[] = numberFormat($row->Ss_Total, 2, '.', ',');
			$rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			$rows[] = $row->ID_Orden_Ingreso;

			$btn_modificar = '';
			$btn_anular = '';
			$btn_eliminar = '';
			if ( $row->Nu_Estado == 6 ) {
				$btn_modificar = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verCompra(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			}
			$rows[] = $btn_modificar;
			if ( empty($row->ID_Documento_Cabecera_Enlace) ){
				if ( $row->Nu_Estado == 6 ) {
					$btn_anular = '<button class="btn btn-xs btn-link" alt="Anular" title="Anular" href="javascript:void(0)" onclick="anularCompra(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Enlace . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action_anular . '\')"><i class="fa fa-minus-circle" aria-hidden="true"> Anular</i></button>';
					$btn_eliminar = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarCompra(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Enlace . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action_delete . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
				}
			}
			$rows[] = $btn_anular . $btn_eliminar;
			$data[] = $rows;
		}
			$output = array(
				'draw' => $this->input->post('draw'),
				'recordsTotal' => $this->CompraModel->count_all(),
				'recordsFiltered' => $this->CompraModel->count_filtered(),
				'data' => $data,
			);
			echo json_encode($output);
	}

	public function ajax_edit($ID){
		$data = $this->CompraModel->get_by_id($this->security->xss_clean($ID));
		$arrImpuesto = $this->HelperModel->getImpuestos($arrPost = '');
		$output = array(
			'arrEdit' => $data,
			'arrImpuesto' => $arrImpuesto,
		);
		echo json_encode($output);
	}

	public function crudCompra(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');
		$response = array('status' => 'success', 'ID_Documento_Cabecera_Enlace' => '');
		if ( $_POST['arrCompraCabecera']['esEnlace'] == 1 )
			$response = $this->HelperModel->documentExistVerify($this->security->xss_clean($_POST['arrCompraModificar']['ID_Documento_Guardado']), $this->security->xss_clean($_POST['arrCompraModificar']['ID_Tipo_Documento_Modificar']), $this->security->xss_clean($_POST['arrCompraModificar']['ID_Serie_Documento_Modificar']), $this->security->xss_clean($_POST['arrCompraModificar']['ID_Numero_Documento_Modificar']), $_POST['arrCompraModificar']);
		
		if ( $response['status'] != 'danger' ) {
			$arrProveedorNuevo = '';
			if (isset($_POST['arrProveedorNuevo'])){
				$arrProveedorNuevo = array(
					'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrProveedorNuevo']['ID_Tipo_Documento_Identidad']),
					'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrProveedorNuevo']['Nu_Documento_Identidad'])),
					'No_Entidad'					=> $this->security->xss_clean($_POST['arrProveedorNuevo']['No_Entidad']),
					'Txt_Direccion_Entidad'			=> $this->security->xss_clean($_POST['arrProveedorNuevo']['Txt_Direccion_Entidad']),
					'Nu_Telefono_Entidad'			=> $this->security->xss_clean($_POST['arrProveedorNuevo']['Nu_Telefono_Entidad']),
					'Nu_Celular_Entidad'			=> $this->security->xss_clean($_POST['arrProveedorNuevo']['Nu_Celular_Entidad']),
				);
			}
			
			$iDescargarStock = $this->security->xss_clean($_POST['arrCompraCabecera']['Nu_Descargar_Inventario']);

			$arrCompraCabecera = array(
				'ID_Empresa'				=> $this->empresa->ID_Empresa,
				'ID_Organizacion'			=> $this->empresa->ID_Organizacion,
				'ID_Entidad'				=> $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Entidad']),
				'ID_Tipo_Asiento'			=> 2,
				'ID_Tipo_Documento'			=> $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Tipo_Documento']),
				'ID_Serie_Documento'		=> $this->security->xss_clean(strtoupper($_POST['arrCompraCabecera']['ID_Serie_Documento'])),
				'ID_Numero_Documento'		=> $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Numero_Documento']),
				'Fe_Emision'				=> ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Emision'])),
				'Fe_Emision_Hora'			=> dateNow('fecha_hora'),
				'ID_Medio_Pago'				=> $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Medio_Pago']),
				'ID_Rubro'					=> 1,//Compras
				'ID_Moneda'					=> $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Moneda']),
				'Fe_Vencimiento'			=> ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Vencimiento'])),
				'Fe_Periodo'				=> ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Periodo'])),
				'Nu_Descargar_Inventario'	=> $iDescargarStock,
				'Txt_Glosa'					=> $this->security->xss_clean($_POST['arrCompraCabecera']['Txt_Glosa']),
				'Po_Descuento'				=> $this->security->xss_clean($_POST['arrCompraCabecera']['Po_Descuento']),
				'Ss_Descuento'				=> $this->security->xss_clean($_POST['arrCompraCabecera']['Ss_Descuento']),
				'Ss_Total'					=> $this->security->xss_clean($_POST['arrCompraCabecera']['Ss_Total']),
				'Ss_Percepcion'				=> $this->security->xss_clean($_POST['arrCompraCabecera']['Ss_Percepcion']),
				'Fe_Detraccion'				=> ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Detraccion'])),
				'Nu_Detraccion'				=> $this->security->xss_clean($_POST['arrCompraCabecera']['Nu_Detraccion']),
				'Nu_Estado'					=> 6,
				'ID_Almacen' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Almacen']),
				'ID_Tipo_Documento_Rentanilidad' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Tipo_Documento_Rentanilidad']),
				'ID_Orden_Ingreso' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Orden_Ingreso']),
				'Fe_Emision_Anterior' => ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Emision_Anterior'])),
			);
			
			if ( $_POST['arrCompraCabecera']['ID_Lista_Precio_Cabecera'] != 0 )
				$arrCompraCabecera = array_merge($arrCompraCabecera, array("ID_Lista_Precio_Cabecera" => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Lista_Precio_Cabecera'])));

			if ( isset($_POST['arrCompraCabecera']['ID_Documento_Cabecera_Orden']) ) {
				$_POST['arrCompraCabecera']['esEnlace'] = 1;
				$response['ID_Documento_Cabecera_Enlace'] = $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Documento_Cabecera_Orden']);
			}
			
			echo json_encode(
			($this->security->xss_clean($_POST['arrCompraCabecera']['EID_Empresa']) != '' && $this->security->xss_clean($_POST['arrCompraCabecera']['EID_Documento_Cabecera']) != '') ?
				$this->actualizarCompra_Inventario(array('ID_Empresa' => $this->security->xss_clean($_POST['arrCompraCabecera']['EID_Empresa']), 'ID_Documento_Cabecera' => $this->security->xss_clean($_POST['arrCompraCabecera']['EID_Documento_Cabecera'])), $arrCompraCabecera, $_POST['arrDetalleCompra'], $_POST['arrCompraCabecera']['esEnlace'], $response['ID_Documento_Cabecera_Enlace'], $arrCompraCabecera['Nu_Descargar_Inventario'], $arrProveedorNuevo)
			:
				$this->agregarCompra_Inventario($arrCompraCabecera, $_POST['arrDetalleCompra'], $_POST['arrCompraCabecera']['esEnlace'], $response['ID_Documento_Cabecera_Enlace'], $arrCompraCabecera['Nu_Descargar_Inventario'], $arrProveedorNuevo)
			);
		} else {
			echo json_encode($response);
		}
	}
 
	public function agregarCompra_Inventario($arrCompraCabecera = '', $arrDetalleCompra = '', $esEnlace = '', $ID_Documento_Cabecera_Enlace = '', $Nu_Descargar_Inventario = '', $arrProveedorNuevo = ''){
		$responseCompra = $this->CompraModel->agregarCompra($arrCompraCabecera, $arrDetalleCompra, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrProveedorNuevo);
		if ($responseCompra['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrCompraCabecera['ID_Tipo_Movimiento'] = 2;//Compra
				if ($arrCompraCabecera['ID_Tipo_Documento'] == 5)//NC
					$arrCompraCabecera['ID_Tipo_Movimiento'] = 18;//SALIDA POR DEVOLUCIÓN AL PROVEEDOR
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrCompraCabecera['ID_Almacen'], $responseCompra['Last_ID_Documento_Cabecera'], 0, $arrDetalleCompra, $arrCompraCabecera['ID_Tipo_Movimiento'], 0, '', 0, 1);
			}

			// STOCK_PROD_UPDT_2 - INICIO 
				if( $_POST['Actualiza_Stock'] == 'INGRESO_COMPRA' ) {
					$Rspta_ActualizaStock = $this->SalidaInventarioModel->Cust_ActualizaStock( $this->empresa->ID_Empresa  , 
					$this->empresa->ID_Organizacion  , $arrCompraCabecera['ID_Almacen'] ,$arrDetalleCompra ); 
				}
			// STOCK_PROD_UPDT_2 - FIN

			return $responseCompra;
		} else {
			return $responseCompra;
		}
	}

	public function actualizarCompra_Inventario($arrWhereCompra = '', $arrCompraCabecera = '', $arrDetalleCompra = '', $esEnlace = '', $ID_Documento_Cabecera_Enlace = '', $Nu_Descargar_Inventario = '', $arrProveedorNuevo = ''){
		$responseCompra = $this->CompraModel->actualizarCompra($arrWhereCompra, $arrCompraCabecera, $arrDetalleCompra, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrProveedorNuevo);
		if ($responseCompra['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrCompraCabecera['ID_Tipo_Movimiento'] = 2;//Compra
				if ($arrCompraCabecera['ID_Tipo_Documento'] == 5)//NC
					$arrCompraCabecera['ID_Tipo_Movimiento'] = 18;//SALIDA POR DEVOLUCIÓN AL PROVEEDOR
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrCompraCabecera['ID_Almacen'], $responseCompra['Last_ID_Documento_Cabecera'], 0, $arrDetalleCompra, $arrCompraCabecera['ID_Tipo_Movimiento'], 1, $arrWhereCompra, 0, 1);
			}

			// STOCK_PROD_UPDT_2 - INICIO 
			if( $_POST['Actualiza_Stock'] == 'INGRESO_COMPRA' ) {
				$Rspta_ActualizaStock = $this->SalidaInventarioModel->Cust_ActualizaStock( $this->empresa->ID_Empresa  , 
				$this->empresa->ID_Organizacion  , $arrCompraCabecera['ID_Almacen'] ,$arrDetalleCompra ); 
			}
			// STOCK_PROD_UPDT_2 - FIN

			//return $responseCompra;
		} else {
			return $responseCompra;
		}
	}
    
	public function anularCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->CompraModel->anularCompra($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Enlace), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	
	public function eliminarCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->CompraModel->eliminarCompra($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Enlace), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
}
