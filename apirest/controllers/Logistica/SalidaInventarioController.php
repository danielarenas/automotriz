<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class SalidaInventarioController extends CI_Controller {
	private $file_path = '../assets/images/logos/';
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('MenuModel');
		$this->load->model('Logistica/SalidaInventarioModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
	}

	public function listarSalidasInventario(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Logistica/SalidaInventarioView');
			$this->load->view('footer', array("js_salida_inventario" => true));
		}
	}
	
	public function ajax_list(){
		$sMethod = 'listarSalidasInventario';
		$arrData = $this->SalidaInventarioModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action_anular = 'anular';
        $action_delete = 'delete';
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
            $rows[] = ToDateBD($row->Fe_Emision);
            $rows[] = $row->No_Tipo_Documento_Breve;
            $rows[] = $row->ID_Serie_Documento;
            $rows[] = $row->ID_Numero_Documento;
            $rows[] = $row->No_Entidad;
            $rows[] = $row->No_Signo;
            $rows[] = numberFormat($row->Ss_Total, 2, '.', ',');
			$placaAndOI = $this->HelperModel->getPlacaAndOIByGuiaCabeceraID($row->ID_Guia_Cabecera);
			$rows[] = $placaAndOI['No_Placa_Vehiculo'];
			$rows[] = $placaAndOI['Orden_Ingreso'];
            $rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			
			$rows[] = '<button class="btn btn-xs btn-link" alt="Crear Activo" title="Crear Activo" href="javascript:void(0)" onclick="crearActivo(\'' . $row->ID_Guia_Cabecera . '\')">Crear Activo</button>';

			$btn_modificar = '';
			$btn_descargar_stock = '';
			$btn_anular = '';
			$btn_eliminar = '';
			
			if ( $this->MenuModel->verificarAccesoMenuInterno($sMethod)->Nu_Editar == 1) {
				if ( $row->Nu_Estado == 5 || $row->Nu_Estado == 12 ) {
					$btn_modificar = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verCompra(\'' . $row->ID_Guia_Cabecera . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
				}
			}
			$rows[] = $btn_modificar;
			
			if ( $this->MenuModel->verificarAccesoMenuInterno($sMethod)->Nu_Eliminar == 1) {
				if ( $row->Nu_Estado == 6 || $row->Nu_Estado == 13 )
					$btn_descargar_stock = '<button class="btn btn-xs btn-link" alt="Descargar stock" title="Descargar stock" href="javascript:void(0)" onclick="verCompra(\'' . $row->ID_Guia_Cabecera . '\')"><i class="fa fa-pencil" aria-hidden="true"> Descargar stock</i></button>';

				if ( empty($row->ID_Guia_Cabecera_Enlace) ){
					if ( $row->Nu_Estado == 6 ) {
						$btn_anular = '<button class="btn btn-xs btn-link" alt="Anular" title="Anular" href="javascript:void(0)" onclick="anularCompra(\'' . $row->ID_Guia_Cabecera . '\', \'' . $row->Nu_Enlace . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action_anular . '\')"><i class="fa fa-minus-circle" aria-hidden="true"> Anular</i></button>';
						$btn_eliminar = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarCompra(\'' . $row->ID_Guia_Cabecera . '\', \'' . $row->Nu_Enlace . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action_delete . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
					}
				}
			}
			$rows[] = $btn_descargar_stock . $btn_anular . $btn_eliminar;
			$rows[] = '<button class="btn btn-xs btn-link" alt="PDF" title="PDF" href="javascript:void(0)" onclick="pdfSalidaInventario(\'' . $row->ID_Guia_Cabecera . '\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>';

			

            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->SalidaInventarioModel->count_all(),
	        'recordsFiltered' => $this->SalidaInventarioModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
    
	public function ajax_edit($ID){
        $data = $this->SalidaInventarioModel->get_by_id($this->security->xss_clean($ID));
        $arrImpuesto = $this->HelperModel->getImpuestos($arrPost = '');
		$arrAreaIngreso = $this->HelperModel->getTiposDocumentos(13);
        $output = array(
        	'arrEdit' => $data,
        	'arrImpuesto' => $arrImpuesto,
        	'arrAreaIngreso' => $arrAreaIngreso,
        );
        echo json_encode($output);
    }
    
	public function crudCompra(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');

		$arrClienteNuevo = '';
		if (isset($_POST['arrClienteNuevo'])){
			$arrClienteNuevo = array(
				'ID_Tipo_Documento_Identidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad' => $this->security->xss_clean(strtoupper($_POST['arrClienteNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['No_Entidad']),
				'Txt_Direccion_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['Txt_Direccion_Entidad']),
				'Nu_Telefono_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Telefono_Entidad']),
				'Nu_Celular_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Celular_Entidad']),
			);
		}

		$Nu_Estado = ($this->security->xss_clean($_POST['arrCompraCabecera']['Nu_Estado']));
		//if (empty($_POST['arrCompraCabecera']['ENu_Estado']))
			//$Nu_Estado = 13;

		$response['ID_Guia_Cabecera_Enlace'] = '';
		$_POST['arrCompraCabecera']['esEnlace'] = 0;//Se usa para relacionar pero actualmente no se ha implementado por eso estará en 0
		$arrCompraCabecera = array(
			'ID_Empresa' => $this->empresa->ID_Empresa,
			'ID_Organizacion' => $this->empresa->ID_Organizacion,
			'ID_Almacen' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Almacen']),
			'ID_Tipo_Asiento' => 3,//Guías Salida
			'ID_Tipo_Documento' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Tipo_Documento']),
			'ID_Serie_Documento' => $this->security->xss_clean(strtoupper($_POST['arrCompraCabecera']['ID_Serie_Documento'])),
			'ID_Serie_Documento_PK' => $this->security->xss_clean(strtoupper($_POST['arrCompraCabecera']['ID_Serie_Documento_PK'])),
			'ID_Numero_Documento' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Numero_Documento']),
			'Fe_Emision' => ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Emision'])),
			'Fe_Periodo' => ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Emision'])),
			'ID_Moneda' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Moneda']),
			'Nu_Descargar_Inventario' => $this->security->xss_clean($_POST['arrCompraCabecera']['Nu_Descargar_Inventario']),
			'ID_Tipo_Movimiento' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Tipo_Movimiento']),
			'ID_Entidad' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Entidad']),
			'iFlete' => $this->security->xss_clean($_POST['arrCompraCabecera']['iFlete']),
			'ID_Entidad_Transportista' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Entidad_Transportista']),
			'No_Placa' => $this->security->xss_clean(strtoupper($_POST['arrCompraCabecera']['No_Placa'])),
			'Fe_Traslado' => ToDate($this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Traslado'] != '' ? $this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Traslado']) : $this->security->xss_clean($_POST['arrCompraCabecera']['Fe_Emision']))),
			'ID_Motivo_Traslado' => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Motivo_Traslado']),
			'No_Licencia' => $this->security->xss_clean($_POST['arrCompraCabecera']['No_Licencia']),
			'No_Certificado_Inscripcion' => $this->security->xss_clean($_POST['arrCompraCabecera']['No_Certificado_Inscripcion']),
			'Txt_Glosa' => $this->security->xss_clean($_POST['arrCompraCabecera']['Txt_Glosa']),
			'Ss_Total' => $this->security->xss_clean($_POST['arrCompraCabecera']['Ss_Total']),
			'Nu_Estado' => $Nu_Estado,
			//'Nu_Tipo_Mantenimiento' => $this->security->xss_clean($_POST['arrCompraCabecera']['Nu_Tipo_Mantenimiento']),
		);
		
		if ( $_POST['arrCompraCabecera']['ID_Lista_Precio_Cabecera'] != 0 )
			$arrCompraCabecera = array_merge($arrCompraCabecera, array("ID_Lista_Precio_Cabecera" => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Lista_Precio_Cabecera'])));
		
		if ( isset($_POST['arrCompraCabecera']['ID_Origen_Tabla']) && $_POST['arrCompraCabecera']['ID_Origen_Tabla'] != 0 )
			$arrCompraCabecera = array_merge($arrCompraCabecera, array("ID_Origen_Tabla" => $this->security->xss_clean($_POST['arrCompraCabecera']['ID_Origen_Tabla'])));
		
		echo json_encode(
		($this->security->xss_clean($_POST['arrCompraCabecera']['EID_Guia_Cabecera']) != '') ?
			$this->actualizarCompra_Inventario(array('ID_Guia_Cabecera' => $this->security->xss_clean($_POST['arrCompraCabecera']['EID_Guia_Cabecera'])), $arrCompraCabecera, $_POST['arrDetalleCompra'], $_POST['arrCompraCabecera']['esEnlace'], $response['ID_Guia_Cabecera_Enlace'], $arrCompraCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo)
		:
			$this->agregarCompra_Inventario($arrCompraCabecera, $_POST['arrDetalleCompra'], $_POST['arrCompraCabecera']['esEnlace'], $response['ID_Guia_Cabecera_Enlace'], $arrCompraCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo)
		);
	}

	public function agregarCompra_Inventario($arrCompraCabecera = '', $arrDetalleCompra = '', $esEnlace = '', $ID_Guia_Cabecera_Enlace = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = ''){
		$responseCompra = $this->SalidaInventarioModel->agregarCompra($arrCompraCabecera, $arrDetalleCompra, $esEnlace, $ID_Guia_Cabecera_Enlace, $arrClienteNuevo);
		if ($responseCompra['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1')//1 = Si
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrCompraCabecera['ID_Almacen'], 0, $responseCompra['Last_ID_Guia_Cabecera'], $arrDetalleCompra, $arrCompraCabecera['ID_Tipo_Movimiento'], 0, '', 0, 1);

			// STOCK_PROD_UPDT - INICIO
			/* if( $_POST['Actualiza_Stock_en_Vale'] == 'vale' ||  $_POST['Actualiza_Stock_en_Vale'] == 'salida_inventario'  ) {
				$Rspta_ActualizaStock = $this->SalidaInventarioModel->Cust_ActualizaStock( $this->empresa->ID_Empresa  , 
				$this->empresa->ID_Organizacion  , $_POST['arrCompraCabecera']['ID_Almacen']  ,$arrDetalleCompra ); 
				} */
				// STOCK_PROD_UPDT - FIN   

				return $responseCompra;
		} else {
			return $responseCompra;
		}
	} 

	public function actualizarCompra_Inventario($arrWhereCompra = '', $arrCompraCabecera = '', $arrDetalleCompra = '', $esEnlace = '', $ID_Guia_Cabecera_Enlace = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = ''){
		$responseCompra = $this->SalidaInventarioModel->actualizarCompra($arrWhereCompra, $arrCompraCabecera, $arrDetalleCompra, $esEnlace, $ID_Guia_Cabecera_Enlace, $arrClienteNuevo);
		if ($responseCompra['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1')//1 = Si
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrCompraCabecera['ID_Almacen'], 0, $responseCompra['Last_ID_Guia_Cabecera'], $arrDetalleCompra, $arrCompraCabecera['ID_Tipo_Movimiento'], 1, $arrWhereCompra, 0, 1);
			
			// STOCK_PROD_UPDT - INICIO
			/* if( $_POST['Actualiza_Stock_en_Vale'] == 'vale' ||  $_POST['Actualiza_Stock_en_Vale'] == 'salida_inventario'  ) {
				$Rspta_ActualizaStock = $this->SalidaInventarioModel->Cust_ActualizaStock( $this->empresa->ID_Empresa  , 
				$this->empresa->ID_Organizacion  , $_POST['arrCompraCabecera']['ID_Almacen']  ,$arrDetalleCompra ); 
				} */ 
				// STOCK_PROD_UPDT - FIN 

			return $responseCompra;
		} else {
			return $responseCompra;
		}
	}
    
	public function anularCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->SalidaInventarioModel->anularCompra($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Enlace), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	
	public function eliminarCompra($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->SalidaInventarioModel->eliminarCompra($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Enlace), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	public function getSalidaInventarioPDF($ID){
        $data = $this->SalidaInventarioModel->get_by_id($this->security->xss_clean($ID));
		if (@$data[0]) {
			$placaAndOI = $this->HelperModel->getPlacaAndOIByGuiaCabeceraID($ID);
			$data[0]->vale_placa = $placaAndOI['No_Placa_Vehiculo'];
			$data[0]->vale_oi = $placaAndOI['Orden_Ingreso'];
		}
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Logistica/pdf/SalidaInventarioViewPDF', array(
			'arrData' => $data,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - Salida Inventario ' . $data[0]->ID_Numero_Documento);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
		
		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.png';
		}
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 20%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 80%; text-align: left;">';
					$format_header .= '<p>';
						$format_header .= '<br>';
						$format_header .= '<a style="text-decoration: none;" href="https://www.' . $this->empresa->No_Dominio_Empresa . '" target="_blank"><label style="color: #000000; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->No_Dominio_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #868686; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Nu_Celular_Empresa . '</label><br>';
						$format_header .= '<label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label><br>';
						//$format_header .= '<a style="text-decoration: none;" href="mailto:' . $this->empresa->Txt_Email_Empresa . '" target="_top" lt="Correo LAE System" title="Correo LAE System" data-attr="email"><label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #979797; font-size: 12px; font-style: italic; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Slogan_Empresa . '</label>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_salida_inventario_ Nro. " . $data[0]->ID_Numero_Documento . ".pdf";
		$pdf->Output($file_name, 'I');
	}
	
	public function getDetalle(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
        echo json_encode($this->SalidaInventarioModel->getDetalle($this->input->post()));
	}
	
	public function saveActivo(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
        echo json_encode($this->SalidaInventarioModel->saveActivo($this->input->post()));
	}
}
