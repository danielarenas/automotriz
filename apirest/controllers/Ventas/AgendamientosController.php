<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AgendamientosController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Ventas/AgendamientosModel');
	}

	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/AgendamientosView');
			$this->load->view('footer', array("js_agendamientos" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->AgendamientosModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action = 'delete';
		$dropdown='';
        foreach ($arrData as $row) {
            $no++;
			$rows = array();
			$rows[] = $row->No_Entidad;
            $rows[] = $row->No_Placa_Vehiculo;
            $rows[] = ToDateBD($row->Fe_Emision);
            $rows[] = $row->Hora_Emision;
			$rows[] = $row->No_Marca_Vehiculo;
			$rows[] = $row->No_Modelo_Vehiculo;
			$rows[] = $row->No_Tipo_Servicio;
			$rows[] = $row->Nu_Kilometraje;
			
			$dropdown = '<div class="dropdown">
				<button class="btn btn-' . $row->No_Class_Estado . ' dropdown-toggle" type="button" data-toggle="dropdown">' . $row->No_Descripcion_Estado . '
				<span class="caret"></span></button>
				<ul class="dropdown-menu">
				<li><a alt="Revisado" title="Revisado" href="javascript:void(0)" onclick="estadoAgendamiento(\'' . $row->ID_Taller_Agendamiento . '\', 22);">Aprobado</a></li>
				<li><a alt="Aceptado" title="Aceptado" href="javascript:void(0)" onclick="estadoAgendamiento(\'' . $row->ID_Taller_Agendamiento . '\', 23);">No Aprobado</a></li>
				<li><a alt="Aceptado" title="Aceptado" href="javascript:void(0)" onclick="estadoAgendamiento(\'' . $row->ID_Taller_Agendamiento . '\', 26);">Anulado</a></li>
				</ul>
			</div>';
			$rows[] = $dropdown;
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Facturar" title="Facturar" href="javascript:void(0)" onclick="addOI()">Genarar OI</button>';
			
			$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verSerie(\'' . $row->ID_Taller_Agendamiento . '\')"><i class="fa fa-2x fa-pencil" aria-hidden="true"></i></button>';
			//$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarSerie(\'' . $row->ID_Taller_Agendamiento . '\', \'' . $action . '\')"><i class="fa fa-2x fa-trash-o" aria-hidden="true"></i></button>';
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->AgendamientosModel->count_all(),
	        'recordsFiltered' => $this->AgendamientosModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
	
	public function ajax_edit($ID_Serie_Documento_PK){
		echo json_encode($this->AgendamientosModel->get_by_id($this->security->xss_clean($ID_Serie_Documento_PK)));
    }
    
	public function crudSerie(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		$data = array(
			'ID_Empresa' => $this->empresa->ID_Empresa,
			'ID_Entidad' => $this->input->post('ID_Entidad'),
			'ID_Producto_Placa' => $this->input->post('ID_Producto_Placa'),
			'Fe_Emision' => ToDate($this->input->post('Fe_Emision')),
			'Hora_Emision' => $this->input->post('Hora_Emision'),
			'No_Marca_Vehiculo' => $this->input->post('No_Marca_Vehiculo'),
			'No_Modelo_Vehiculo' => $this->input->post('No_Modelo_Vehiculo'),
			'No_Tipo_Servicio' => $this->input->post('No_Tipo_Servicio'),
			'Nu_Kilometraje' => $this->input->post('Nu_Kilometraje'),
		);
		echo json_encode(
		($this->input->post('EID_Taller_Agendamiento') != '') ?
			$this->AgendamientosModel->actualizarSerie(array('ID_Taller_Agendamiento' => $this->input->post('EID_Taller_Agendamiento')), $data)
		:
			$this->AgendamientosModel->agregarSerie($data)
		);
	}
    
	public function eliminarSerie($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->AgendamientosModel->eliminarSerie($this->security->xss_clean($ID)));
	}

	public function estadoAgendamiento($ID, $Nu_Estado){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->AgendamientosModel->estadoAgendamiento($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Estado)));
	}
}
