<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class OrdenTrabajoController extends CI_Controller {
	private $file_path = '../assets/images/logos/';
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Ventas/OrdenTrabajoModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
	}

	public function listarOrdenesTrabajo(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/OrdenTrabajoView');
			$this->load->view('footer', array("js_orden_trabajo" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->OrdenTrabajoModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action = 'delete';
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
            $rows[] = ToDateBD($row->Fe_Emision);
            $rows[] = $row->ID_Documento_Cabecera;
            $rows[] = $row->No_Entidad;
            //$rows[] = $row->No_Contacto;
            $rows[] = $row->No_Signo;
            $rows[] = numberFormat($row->Ss_Total, 2, '.', ',');
            $rows[] = 
            '<div class="dropdown">
				<button class="btn btn-' . $row->No_Class_Estado . ' dropdown-toggle" type="button" data-toggle="dropdown">' . $row->No_Descripcion_Estado . '
				<span class="caret"></span></button>
				<ul class="dropdown-menu">
				  <li><a alt="Entregado" title="Modificar" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 0);">Entregado</a></li>
				  <li><a alt="Revisado" title="Revisado" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 1);">Revisado</a></li>
				  <li><a alt="Aceptado" title="Aceptado" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 2);">Aceptado</a></li>
				  <li><a alt="Rechazado" title="Rechazado" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 3);">Rechazado</a></li>
				</ul>
			</div>';
			$orden_trabajo = '';
			$iIdOrdenTrabajo = '';
			$iID_Tipo_Documento = 0;
			$arrParams = array(
				'iRelacionDatos' => 2,//Presupuesto a orden de trabajo
				'iIdOrigenTabla' => $row->ID_Documento_Cabecera
			);
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaOTtoPresupuesto($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$orden_trabajo .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
					$iIdOrdenTrabajo = $row_relacion->ID_Documento_Cabecera;
					$iID_Tipo_Documento = $row_relacion->ID_Tipo_Documento;//22 = presupuesto mecanica  y 23 = presupuesto pyp
				}
			}
			$rows[] = $orden_trabajo;
            $rows[] = '<button class="btn btn-xs btn-link" alt="PDF" title="PDF" href="javascript:void(0)" onclick="pdfOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $iID_Tipo_Documento . '\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>';
			$btn_facturar = '';
            //if ($row->Nu_Estado != 3)
				//$btn_facturar = '<button class="btn btn-xs btn-link" alt="Facturar" title="Facturar" href="javascript:void(0)" onclick="facturarOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-book" aria-hidden="true"> Facturar</i></button>';
			if ($row->Nu_Estado == 5 || $row->Nu_Estado == 0 || $row->Nu_Estado == 1 || $row->Nu_Estado == 2) {
				$asunto = 'Cotización Nro. ' . $row->ID_Documento_Cabecera . ' ' . $this->empresa->No_Empresa . ' | ' . $this->empresa->Nu_Documento_Identidad;
				$rows[] = '<button class="btn btn-xs btn-link" alt="Enviar Correo" title="Enviar Correo" href="javascript:void(0)" onclick="enviarCorreo(\'' . $row->ID_Documento_Cabecera . '\', \'' . $this->user->Txt_Email . '\', \'' . $row->Txt_Email_Contacto . '\', \'' . $asunto . '\')"><i class="fa fa-envelope" aria-hidden="true"> Correo</i></button>';
				$rows[] = '<button class="btn btn-xs btn-link" alt="Duplicar" title="Duplicar" href="javascript:void(0)" onclick="duplicarOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-clone" aria-hidden="true"> Duplicar</i></button>';
			}
			if ($row->Nu_Estado == 5 || $row->Nu_Estado == 0 || $row->Nu_Estado == 1) {
				$rows[] = $btn_facturar;
				$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $iIdOrdenTrabajo . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
				$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
			} else {
				$rows[] = '';
				$rows[] = $btn_facturar;
				$rows[] = '';
				$rows[] = '';
			}
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->OrdenTrabajoModel->count_all(),
	        'recordsFiltered' => $this->OrdenTrabajoModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
    
	public function ajax_edit($ID){
        $data = $this->OrdenTrabajoModel->get_by_id($this->security->xss_clean($ID));
        $arrImpuesto = $this->HelperModel->getImpuestos($arrPost = '');
        $output = array(
        	'arrEdit' => $data,
        	'arrImpuesto' => $arrImpuesto,
        );
        echo json_encode($output);
    }
    
	public function crudOrdenVenta(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');

		$arrClienteNuevo = '';
		if (isset($_POST['arrClienteNuevo'])){
			$arrClienteNuevo = array(
				'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrClienteNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrClienteNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad'					=> $this->security->xss_clean($_POST['arrClienteNuevo']['No_Entidad']),
				'Txt_Direccion_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Txt_Direccion_Entidad']),
				'Nu_Telefono_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Telefono_Entidad']),
				'Nu_Celular_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Celular_Entidad']),
			);
		}
		
		$arrContactoNuevo = '';
		if (isset($_POST['arrContactoNuevo'])){
			$Nu_Telefono_Contacto = '';
			if ( $_POST['arrContactoNuevo']['Nu_Telefono_Entidad'] && strlen($_POST['arrContactoNuevo']['Nu_Telefono_Entidad']) === 8){
		        $Nu_Telefono_Contacto = explode(' ', $_POST['arrContactoNuevo']['Nu_Telefono_Entidad']);
		        $Nu_Telefono_Contacto = $Nu_Telefono_Contacto[0].$Nu_Telefono_Contacto[1];
			}
			
			$Nu_Celular_Contacto = '';
			if ( $_POST['arrContactoNuevo']['Nu_Celular_Entidad'] && strlen($_POST['arrContactoNuevo']['Nu_Celular_Entidad']) === 11){
		        $Nu_Celular_Contacto = explode(' ', $_POST['arrContactoNuevo']['Nu_Celular_Entidad']);
		        $Nu_Celular_Contacto = $Nu_Celular_Contacto[0].$Nu_Celular_Contacto[1].$Nu_Celular_Contacto[2];
			}
			$arrContactoNuevo = array(
				'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrContactoNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrContactoNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad'					=> $this->security->xss_clean($_POST['arrContactoNuevo']['No_Entidad']),
				'Nu_Telefono_Entidad'			=> $Nu_Telefono_Contacto,
				'Nu_Celular_Entidad'			=> $Nu_Celular_Contacto,
				'Txt_Email_Entidad'				=> $this->security->xss_clean($_POST['arrContactoNuevo']['Txt_Email_Entidad']),
			);
		}
		
		$arrVehiculoNuevo = '';
		if (isset($_POST['arrVehiculoNuevo'])){
			$arrVehiculoNuevo = array(
				'No_Placa_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Placa_Vehiculo']),
				'No_Year_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Year_Vehiculo']),
				'No_Marca_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Marca_Vehiculo']),
				'No_Modelo_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Modelo_Vehiculo']),
				'No_Tipo_Combustible_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Tipo_Combustible_Vehiculo']),
				'No_Vin_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Vin_Vehiculo']),
				'No_Motor' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Motor']),
				'No_Color_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Color_Vehiculo']),
			);
		}
		
		$iDescargarStock = $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Nu_Descargar_Inventario']);

		$arrOrdenVentaCabecera = array(
			'ID_Empresa'					=> $this->empresa->ID_Empresa,
			'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
			'ID_Tipo_Asiento'				=> 1,//Venta
			'ID_Tipo_Documento'				=> 20,//Proforma
			'Nu_Correlativo'				=> 0,
			'Fe_Emision'					=> ToDate($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Fe_Emision'])),
			'Fe_Vencimiento'				=> ToDate($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Fe_Vencimiento'])),
			'Fe_Periodo'					=> ToDate($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Fe_Entrega'])),
			'ID_Moneda'						=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Moneda']),
			'ID_Medio_Pago'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Medio_Pago']),
			'Nu_Descargar_Inventario'		=> $iDescargarStock,
			'ID_Entidad'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Entidad']),
			'ID_Contacto'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Contacto']),
			'Txt_Garantia'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Txt_Garantia']),
			'Txt_Glosa'						=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Txt_Glosa']),
			'Po_Descuento'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Po_Descuento']),
			'Ss_Descuento'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Ss_Descuento']),
			'Ss_Total'						=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Ss_Total']),
			'Nu_Estado'						=> ($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ENu_Estado']) != '' ? $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ENu_Estado']) : 5),
			'ID_Mesero'	=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Mesero']),
			'ID_Comision' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Comision']),
			'No_Formato_PDF' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['No_Formato_PDF']),
			'ID_Producto' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Producto']),
			'ID_Mesero' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Mesero']),//Asesor responsable
			'ID_Transporte_Sede_Planta' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Transporte_Sede_Planta']),//Responsable Pintura
			'ID_Transporte_Delivery' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Transporte_Delivery']),//Responsable Planchado
			'ID_Comision' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Comision']),//Asesor = personal de mafisa opcional es para lo comisión
			'ID_Origen_Tabla' => (isset($_POST['arrOrdenVentaCabecera']['ID_Origen_Tabla']) ? $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Origen_Tabla']) : ''),
		);

		if ( $_POST['arrOrdenVentaCabecera']['ID_Lista_Precio_Cabecera'] != 0 )
			$arrOrdenVentaCabecera = array_merge($arrOrdenVentaCabecera, array("ID_Lista_Precio_Cabecera" => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Lista_Precio_Cabecera'])));

		if ( $iDescargarStock == 1 ) {
			$arrOrdenVentaCabecera = array_merge($arrOrdenVentaCabecera, array("ID_Almacen" => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Almacen'])));
		}

		echo json_encode(
		( $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Empresa']) != '' && $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Documento_Cabecera']) != '') ?
			$this->actualizarVenta_Inventario(array('ID_Empresa' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Empresa']), 'ID_Documento_Cabecera' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Documento_Cabecera'])), $arrOrdenVentaCabecera, $_POST['arrDetalleOrdenVenta'], $arrOrdenVentaCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo)
		:
			$this->agregarVenta_Inventario($arrOrdenVentaCabecera, $_POST['arrDetalleOrdenVenta'], $arrOrdenVentaCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo)
		);
	}

	public function agregarVenta_Inventario($arrOrdenVentaCabecera = '', $arrDetalleOrdenVenta = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = '', $arrContactoNuevo = '', $arrVehiculoNuevo = ''){
		$responseVenta = $this->OrdenTrabajoModel->agregarVenta($arrOrdenVentaCabecera, $arrDetalleOrdenVenta, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo);
		if ($responseVenta['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrOrdenVentaCabecera['ID_Tipo_Movimiento'] = 1;//Venta
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrOrdenVentaCabecera['ID_Almacen'], $responseVenta['Last_ID_Documento_Cabecera'], 0, $arrDetalleOrdenVenta, $arrOrdenVentaCabecera['ID_Tipo_Movimiento'], 0, '', 1, 1);
			}
			return $responseVenta;
		} else {
			return $responseVenta;
		}
	}

	public function actualizarVenta_Inventario($arrWhereVenta = '', $arrOrdenVentaCabecera = '', $arrDetalleOrdenVenta = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = '', $arrContactoNuevo = '', $arrVehiculoNuevo = ''){
		$responseVenta = $this->OrdenTrabajoModel->actualizarVenta($arrWhereVenta, $arrOrdenVentaCabecera, $arrDetalleOrdenVenta, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo);
		if ($responseVenta['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrOrdenVentaCabecera['ID_Tipo_Movimiento'] = 1;//Venta
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrOrdenVentaCabecera['ID_Almacen'], $responseVenta['Last_ID_Documento_Cabecera'], 0, $arrDetalleOrdenVenta, $arrOrdenVentaCabecera['ID_Tipo_Movimiento'], 1, $arrWhereVenta, 1, 1);
			}
			return $responseVenta;
		} else {
			return $responseVenta;
		}
	}
	
	public function eliminarOrdenVenta($ID, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenTrabajoModel->eliminarOrdenVenta($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	
	public function estadoOrdenVenta($ID, $Nu_Descargar_Inventario, $Nu_Estado){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenTrabajoModel->estadoOrdenVenta($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario), $this->security->xss_clean($Nu_Estado)));
	}
	
	public function duplicarOrdenVenta($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenTrabajoModel->duplicarOrdenVenta($this->security->xss_clean($ID)));
	}

	public function getOrdenVentaPDF($ID, $iIdTipoDocumento){
        $data = $this->OrdenTrabajoModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		if ( $iIdTipoDocumento == 23 ) {//23 = pyp
			$file = $this->load->view('Ventas/pdf/OrdenTrabajoViewPDF', array(
				'arrData' => $data,
			));
		} else {
			$file = $this->load->view('Ventas/pdf/OrdenTrabajoViewMecanicaPDF', array(
				'arrData' => $data,
			));
		}
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - cotizacion Nro. ' . $data[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
		
		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.png';
		}
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 20%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 80%; text-align: left;">';
					$format_header .= '<p>';
						$format_header .= '<br>';
						$format_header .= '<a style="text-decoration: none;" href="https://'.$this->empresa->No_Dominio_Empresa.'" target="_blank"><label style="color: #000000; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->No_Dominio_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #868686; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Nu_Celular_Empresa . '</label><br>';
						$format_header .= '<label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label><br>';
						//$format_header .= '<a style="text-decoration: none;" href="mailto:' . $this->empresa->Txt_Email_Empresa . '" target="_top" lt="Correo LAE System" title="Correo LAE System" data-attr="email"><label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #979797; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Slogan_Empresa . '</label>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_cotizacion_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".pdf";
		$pdf->Output($file_name, 'I');
	}

	public function enviarCorreo(){
        $arrData = $this->OrdenTrabajoModel->get_by_id($this->input->post('iIdOrden'));

		$this->load->library('email');

		$data = array();

		$data["No_Documento"] = 'Cotización Nro. ' . $this->input->post('iIdOrden');
		$data["Fe_Emision"] = ToDateBD($arrData[0]->Fe_Emision);
		$data["Fe_Vencimiento"] = ToDateBD($arrData[0]->Fe_Vencimiento);
		$data["No_Signo"] = $arrData[0]->No_Signo;
		$data["Ss_Total"] = $arrData[0]->Ss_Total;
		
		$data["No_Entidad"] = $arrData[0]->No_Entidad;
		
		$data["No_Empresa"]	= $this->empresa->No_Empresa;
		$data["Nu_Documento_Identidad_Empresa"] = $this->empresa->Nu_Documento_Identidad;
		
		$message = $this->load->view('correos/orden_venta', $data, true);
		$asunto = $this->input->post('sAsunto');
		
		$this->email->from($this->input->post('sCorreoDe'), $this->empresa->No_Empresa);//de
		$this->email->to($this->input->post('sCorreoPara'));//para
		$this->email->subject($asunto);
		$this->email->message($message);

		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Ventas/pdf/OrdenTrabajoViewPDF', array(
			'arrData' => $arrData,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems_cotizacion_' . $arrData[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
		
    	$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.jpg';
		}
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 20%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 80%; text-align: left;">';
					$format_header .= '<p>';
						$format_header .= '<br>';
						$format_header .= '<a style="text-decoration: none;" href="https://' . $this->empresa->No_Dominio_Empresa . '" target="_blank"><label style="color: #000000; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->No_Dominio_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #868686; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Nu_Celular_Empresa . '</label><br>';
						$format_header .= '<label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label><br>';
						//$format_header .= '<a style="text-decoration: none;" href="mailto:' . $this->empresa->Txt_Email_Empresa . '" target="_top" lt="Correo LAE System" title="Correo LAE System" data-attr="email"><label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #979797; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Slogan_Empresa . '</label>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_cotizacion_" . $arrData[0]->ID_Documento_Cabecera . "_" . $arrData[0]->Nu_Documento_Identidad . ".pdf";
		$pdfdoc = $pdf->Output(__DIR__ . $file_name, "F");
		
		$this->email->attach(__DIR__ . $file_name);

		$this->email->set_newline("\r\n");

		$isSend = $this->email->send();
		
		if($isSend) {
			unlink(__DIR__ . $file_name);
			$peticion = array(
				'status' => 'success',
				'style_modal' => 'modal-success',
				'message' => 'Correo enviado',
			);
			echo json_encode($peticion);
			exit();
		} else {
			unlink(__DIR__ . $file_name);
			$peticion = array(
				'status' => 'error',
				'style_modal' => 'modal-danger',
				'message' => 'No se pudo enviar el correo, inténtelo más tarde.',
				'sMessageErrorEmail' => $this->email->print_debugger(),
			);
			echo json_encode($peticion);
			exit();
		}// if - else envio email
	}
}