<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LiquidacionPresupuestoController extends CI_Controller {
	private $file_path = '../assets/images/logos/';
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Ventas/LiquidacionPresupuestoModel');
	}

	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/LiquidacionPresupuestoView');
			$this->load->view('footer', array("js_liquidacion_prespuesto" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->LiquidacionPresupuestoModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action = 'delete';
        foreach ($arrData as $row) {
            $no++;
			$rows = array();
			
			if ( $row->Nu_Estado == 21 ) {//21=facturado
				$rows[] = '';
			} else {
				$rows[] = "<input type='checkbox' id='" . $row->ID_Documento_Cabecera . "' class='check-iIdDocumentoCabecera' name='arrIdDocumentoCabecera[" . $row->ID_Documento_Cabecera . "]'>";
			}

			$rows[] = $row->No_Almacen;
            $rows[] = ToDateBD($row->Fe_Emision);
			$rows[] = $row->No_Tipo_Documento_Breve;
			$rows[] = $row->ID_Serie_Documento;
			$rows[] = $row->ID_Numero_Documento;
			$rows[] = $row->No_Entidad;
			$rows[] = $row->Txt_Tipo_Servicio_Presupuesto;
			$rows[] = $row->Txt_Rango_Fecha_Presupuesto;
			$rows[] = ($row->Nu_Envio_Fecha_Liquidacion_Presupuesto == 0 ? '<span class="label label-danger">No</span>' : '<span class="label label-success">Si</span>');
			$rows[] = (!empty($row->Fe_Liquidacion_Envio_Presupuesto) ? ToDateBD($row->Fe_Liquidacion_Envio_Presupuesto) : '');

			$sMensajeVencimiento='';
			if (!empty($row->Fe_Liquidacion_Envio_Presupuesto)) {
				$dActual=date_create(dateNow('fecha'));
				$dEmision=date_create($row->Fe_Liquidacion_Envio_Presupuesto);
				$iDiferenciaDias=date_diff($dActual,$dEmision);
				$sMensajeVencimiento = $iDiferenciaDias->format("%a") . ' día(s)';
				if ( dateNow('fecha') > $row->Fe_Liquidacion_Envio_Presupuesto ) {
					$dActual=date_create($row->Fe_Liquidacion_Envio_Presupuesto);
					$dEmision=date_create(dateNow('fecha'));
					$iDiferenciaDias=date_diff($dActual,$dEmision);
					$sMensajeVencimiento = $iDiferenciaDias->format("%a") . ' día(s) vencido';
				}
				$sMensajeVencimiento = '<span class="label label-danger">' . $sMensajeVencimiento . '</span>';
			}
			
			$rows[] = ($row->No_Descripcion_Estado == 'Facturado' ? '' : $sMensajeVencimiento);

			$rows[] = $row->Ss_Total;
			$rows[] = $row->Txt_Orden_Compra_Presupuesto;
			$rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="PDF" title="PDF" href="javascript:void(0)" onclick="pdfLiquidacionPresupuesto(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>';
			
            $arrParams = json_encode(array(
				'sTipoCodificacion' => 'json',
				'ID_Documento_Cabecera' => $row->ID_Documento_Cabecera,
				'No_Tipo_Documento_Breve' => str_replace(" ", "-", $row->No_Tipo_Documento_Breve),
				'ID_Serie_Documento' => $row->ID_Serie_Documento,
				'ID_Numero_Documento' => $row->ID_Numero_Documento,
				'No_Entidad' => str_replace(" ", "-", $row->No_Entidad),
				'Fe_Emision' => $row->Fe_Emision,
			), JSON_UNESCAPED_UNICODE);
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick=modificarVenta(\'' . $arrParams . '\')><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';;
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->LiquidacionPresupuestoModel->count_all(),
	        'recordsFiltered' => $this->LiquidacionPresupuestoModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
	
	public function generarLiquidacion(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->LiquidacionPresupuestoModel->generarLiquidacion($this->input->post()));
	}

	public function generarPDF($ID){
        $data = $this->LiquidacionPresupuestoModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Ventas/pdf/LiquidacioViewPDF', array(
			'arrData' => $data,
			'HelperModel' => $this->HelperModel,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - liquidacion Nro. ' . $data[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
	
		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.png';
		}
		$sCssFontFamily='Arial';
		$format_header = '<table>';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 60%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
					$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->No_Empresa . '</label><br>';
					$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->Txt_Direccion_Empresa . '</label><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 40%; text-align: center;">';
					$format_header .= '<p style="border: 0.5px solid #ffffff; border-radius: 50%; background-color:#F2F5F5;"><br>';
						$format_header .= '<label style="color: #2a2a2a ; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">RUC ' . $this->empresa->Nu_Documento_Identidad . '</label><br>';
						$format_header .= '<label style="color: #2a2a2a; font-weight:bold; font-size: 18px; font-family: "' . $sCssFontFamily . '", Times, serif;">LIQUIDACIÓN</label><br>';
						$format_header .= '<label style="color: #2a2a2a; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $data[0]->ID_Serie_Documento . '-' . autocompletarConCeros('', $data[0]->ID_Numero_Documento, $data[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT) . '</label><br>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
			$format_header .= '<tr><td rowspan="4" style="width: 60%; text-align: left;">&nbsp;</td></tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_liquidacion_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".pdf";
		$pdf->Output($file_name, 'I');
	}
    
	public function modificarVenta(){
        if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->LiquidacionPresupuestoModel->modificarVenta($this->input->post()));
	}
}
