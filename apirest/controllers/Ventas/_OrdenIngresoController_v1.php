<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class OrdenIngresoController extends CI_Controller {

	private $upload_path = 'assets/images/orden_ingreso/';

	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Ventas/OrdenIngresoModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
	}

    public function uploadOnly(){
    	$arrResponse = array(
			'sStatus' => 'error',
			'sMessage' => 'problemas con imagén',
			'sClassModal' => 'modal-danger',
		);
		
    	if (!empty($_FILES)){
			$path = $this->upload_path . $this->empresa->Nu_Documento_Identidad;
			if(!is_dir($path)){
			    mkdir($path,0755,TRUE);
			}
			
			if ( !file_exists($path . '/' . cambiarCaracteresEspecialesImagen($_FILES['file']['name'])) ){
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'png|jpeg|jpg|svg|pdf';
				$config['max_size'] = 1024;
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('file')){
					$arrResponse = array(
						'sStatus' => 'error',
						'sMessage' => strip_tags($this->upload->display_errors()) . ' No se guardó imagén',
						'sClassModal' => 'modal-danger',
					);
				} else {
					/*
					$arrUrlImagePath = explode('..', $path);
					$arrUrlImage = explode('/principal',base_url());
					$url_image = $arrUrlImage[0] . $arrUrlImagePath[1];
					*/
					$url_image = base_url() . $path;

					$arrResponse = array(
						'sStatus' => 'success',
						'sMessage' => 'imagén guardada',
						'sClassModal' => 'modal-success',
						'sNombreImagen' => $_FILES["file"]["name"],
						'sNombreImagenUrl' => $url_image . '/' . cambiarCaracteresEspecialesImagen($_FILES['file']['name']),
					);
				}
			} else {
				/*
				$arrUrlImagePath = explode('..', $path);
				$arrUrlImage = explode('/principal',base_url());
				$url_image = $arrUrlImage[0] . $arrUrlImagePath[1];
				*/
				$url_image = base_url() . $path;

				$arrResponse = array(
					'sStatus' => 'success',
					'sMessage' => 'La imagen ya fue guardada',
					'sClassModal' => 'modal-success',
					'sNombreImagen' => $_FILES["file"]["name"],
					'sNombreImagenUrl' => $url_image . '/' . cambiarCaracteresEspecialesImagen($_FILES['file']['name']),
				);
			}
    	}
    	echo json_encode($arrResponse);
	}

    public function removeFileImage(){
    	$nameFileImage = $this->input->post('nameFileImage');
		$path = $this->upload_path . $this->empresa->Nu_Documento_Identidad . '/';
		if ( $nameFileImage && file_exists($path . $nameFileImage) ){
    		unlink($path . $nameFileImage);
    	}
    }

	public function get_image(){
		$path = $this->upload_path . $this->empresa->Nu_Documento_Identidad . '/' . $this->input->post('sNombreImage');
    	$arrfilesImages = array();
		if ( file_exists($path) ){
			$arrfilesImages[] = array(
				'name' => $path,
				'size' => filesize($path),
			);
		}
		echo json_encode($arrfilesImages);
	}

	public function listarOrdenesIngreso(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/OrdenIngresoView');
			$this->load->view('footer', array("js_orden_ingreso" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->OrdenIngresoModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
		$action = 'delete';
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
			$rows[] = $row->ID_Orden_Ingreso;
			$rows[] = $row->No_Tipo_Documento;
			$rows[] = $row->ID_Serie_Documento;
			$rows[] = $row->ID_Numero_Documento;
            $rows[] = ToDateBD($row->Fe_Emision);
            $rows[] = ToDateBD($row->Fe_Entrega);
			$rows[] = ToDateBD($row->Fe_Entrega_Tentativa);
            $rows[] = $row->No_Entidad;
			$rows[] = $row->No_Placa_Vehiculo;
			$rows[] = $row->No_Marca_Vehiculo;
			$rows[] = $row->No_Modelo_Vehiculo;
            $rows[] = $row->No_Kilometraje;
            $rows[] = $row->No_Descripcion_Nivel_Combustible;
			$rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			
			$dropdown = '<div class="dropdown">
				<button class="btn btn-' . $row->No_Class_Facturado . ' dropdown-toggle" type="button" data-toggle="dropdown">' . $row->No_Descripcion_Facturado . '
				<span class="caret"></span></button>
				<ul class="dropdown-menu">
				<li><a alt="Facturado" title="Facturado" href="javascript:void(0)" onclick="cambiarEstadoFacturaOI(\'' . $row->ID_Orden_Ingreso . '\', 1);">Facturado</a></li>
				<li><a alt="Anulado" title="Anulado" href="javascript:void(0)" onclick="cambiarEstadoFacturaOI(\'' . $row->ID_Orden_Ingreso . '\', 2);">Anulado</a></li>
				<li><a alt="Reclamo" title="Reclamo" href="javascript:void(0)" onclick="cambiarEstadoFacturaOI(\'' . $row->ID_Orden_Ingreso . '\', 3);">Reclamo</a></li>
				</ul>
			</div>';
			$rows[] = $dropdown;

			$facturacion = '';
			$ID_Presusupuesto = 0;
			$ID_Liquidacion = 0;
			$btn_presupuesto = '';
			$fTotalPresupuesto = 0.00;
			$fTotalFacturado = 0.00;
			$arrParams = array(
				'iRelacionDatos' => 6,
				'ID_Relacion_Enlace_Tabla' => $row->ID_Orden_Ingreso
			);
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoToOI($arrParams);

			if ($arrResponseRelacion['sStatus'] == 'success') {
				$facturacion = '';
				$ID_Presusupuesto = 0;
				$ID_Liquidacion = 0;
				$fTotalFacturado = 0.00;
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$ID_Presusupuesto = $row_relacion->ID_Documento_Cabecera;
					$btn_presupuesto .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
					$fTotalPresupuesto += $row_relacion->Ss_Total;
					
					$arrParams = array(
						'iRelacionDatos' => 5,
						'iIdOrigenTabla' => $ID_Presusupuesto
					);
					$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
					if ($arrResponseRelacion['sStatus'] == 'success') {
						foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
							$facturacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
							$fTotalFacturado += $row_relacion->Ss_Total;
						}
					}

					
					$ID_Liquidacion = 0;
					$liquidacion = '';
					$arrParams = array(
						'iRelacionDatos' => 4,
						'iIdOrigenTabla' => $ID_Presusupuesto
					);
					$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoALiquidacion($arrParams);
					if ($arrResponseRelacion['sStatus'] == 'success') {
						foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
							$ID_Liquidacion = $row_relacion->ID_Documento_Cabecera;
							$liquidacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento;
						}
					}

					if ( empty($facturacion) && !empty($liquidacion) ) {
						$facturacion = '';
						$fTotalFacturado = 0.00;
						$arrParams = array(
							'iRelacionDatos' => 5,
							'iIdOrigenTabla' => $ID_Liquidacion
						);
						$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
						if ($arrResponseRelacion['sStatus'] == 'success') {
							foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
								$facturacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
								$fTotalFacturado += $row_relacion->Ss_Total;
							}
						}
					}
				}
			}

			if ( empty($btn_presupuesto) ) {
				$btn_presupuesto = '<button class="btn btn-xs btn-link" alt="Presupuesto" title="Presupuesto" href="javascript:void(0)" onclick="generarPresupuesto(\'' . $row->ID_Orden_Ingreso . '\',\'' . $row->ID_Entidad . '\',\'' . $row->ID_Producto . '\')"><i class="fa fa-book" aria-hidden="true"> Presupuesto</i></button>';
			}

			$rows[] = $btn_presupuesto;
			$rows[] = numberFormat($fTotalPresupuesto, 2, '.', ',');
			
			$orden_trabajo = '';
			$arrParams = array(
				'iRelacionDatos' => 2,//Presupuesto a orden de trabajo
				'iIdOrigenTabla' => $ID_Presusupuesto
			);
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoToOT($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$orden_trabajo .= 'OT - ' . $row_relacion->ID_Documento_Cabecera . '<br>';
				}
			}

			$mensaje_ot = 'Falta agregar item';
			if (!empty($orden_trabajo))
				$mensaje_ot = $orden_trabajo;
			$rows[] = $mensaje_ot;

			$rows[] = $facturacion;
			$rows[] = numberFormat($fTotalFacturado, 2, '.', ',');

			// Ingreso de Compra Cabecera ID O.I.
			$ingreso_compra = '';
			$fTotalIngresoCompra = 0.00;
			$arrParams = array(
				'ID_Orden_Ingreso' => $row->ID_Orden_Ingreso
			);
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaOItoIngresoCompra($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$ingreso_compra .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
					$fTotalIngresoCompra += $row_relacion->Ss_Total;
				}
			}
			$rows[] = $ingreso_compra;
			$rows[] = numberFormat($fTotalIngresoCompra, 2, '.', ',');
			// ./ Ingreso de Compra Cabecera ID O.I.

			$btn_modificar = '';
			$btn_eliminar = '';
			if ( $row->Nu_Estado != '3' ){
				$btn_modificar = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verOrdenIngreso(\'' . $row->ID_Orden_Ingreso . '\', \'' . $row->No_Imagen . '\', \'' . $row->No_Imagen_Url . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
				$btn_eliminar = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarOrdenIngreso(\'' . $row->ID_Orden_Ingreso . '\', \'' . $action . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
			}

			$rows[] = $btn_modificar;
			$rows[] = $btn_eliminar;
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->OrdenIngresoModel->count_all(),
	        'recordsFiltered' => $this->OrdenIngresoModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
    
	public function ajax_edit($ID){
        echo json_encode($this->OrdenIngresoModel->get_by_id($this->security->xss_clean($ID)));
    }
    
	public function crudOrdenIngreso(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');

		$arrClienteNuevo = '';
		if (isset($_POST['arrClienteNuevo'])){
			$arrClienteNuevo = array(
				'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrClienteNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrClienteNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad'					=> $this->security->xss_clean($_POST['arrClienteNuevo']['No_Entidad']),
				'Txt_Direccion_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Txt_Direccion_Entidad']),
				'Nu_Telefono_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Telefono_Entidad']),
				'Nu_Celular_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Celular_Entidad']),
			);
		}
		
		$arrVehiculoNuevo = '';
		if (isset($_POST['arrVehiculoNuevo'])){
			$arrVehiculoNuevo = array(
				'No_Placa_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Placa_Vehiculo']),
				'No_Year_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Year_Vehiculo']),
				'No_Marca_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Marca_Vehiculo']),
				'No_Modelo_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Modelo_Vehiculo']),
				'No_Tipo_Combustible_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Tipo_Combustible_Vehiculo']),
				'No_Vin_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Vin_Vehiculo']),
				'No_Motor' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Motor']),
				'No_Color_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Color_Vehiculo']),
			);
		}
		
		$Fe_Emision = ToDate($this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['Fe_Emision']));
		$Fe_Entrega_Tentativa = '0000-00-00';
		if ( $_POST['arrOrdenIngresoCabecera']['Nu_Estado'] != 1 )
			$Fe_Entrega_Tentativa = ToDate($this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['Fe_Entrega_Tentativa']));

		$arrOrdenIngresoCabecera = array(
			'ID_Empresa' => $this->empresa->ID_Empresa,
			'ID_Organizacion' => $this->empresa->ID_Organizacion,
			'ID_Almacen' => $this->empresa->ID_Almacen,
			'Fe_Emision' => $Fe_Emision,
			'Nu_Estado_Facturacion' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['Nu_Estado_Facturacion']),
			'Fe_Entrega' => ToDate($this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['Fe_Entrega'])),
			'Fe_Entrega_Tentativa' => $Fe_Entrega_Tentativa,
			'ID_Area_Ingreso' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['ID_Area_Ingreso']),
			'ID_Serie_Documento' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['ID_Serie_Documento']),
			'ID_Numero_Documento' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['ID_Numero_Documento']),
			'ID_Entidad' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['ID_Entidad']),
			'ID_Producto' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['ID_Producto']),
			'No_Kilometraje' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['No_Kilometraje']),
			'No_Nivel_Combustible' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['No_Nivel_Combustible']),
			'Txt_Reparacion' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['Txt_Reparacion']),
			'Nu_Estado' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['Nu_Estado']),
			'No_Imagen' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['No_Imagen']),
			'No_Imagen_Url' => $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['No_Imagen_Url']),
			'ID_Origen_Tabla' => (isset($_POST['arrOrdenIngresoCabecera']['ID_Origen_Tabla']) ? $this->security->xss_clean($_POST['arrOrdenIngresoCabecera']['ID_Origen_Tabla']) : ''),
		);

		echo json_encode(
		(empty($_POST['arrOrdenIngresoCabecera']['EID_Orden_Ingreso'])) ?
			$this->OrdenIngresoModel->agregarVenta($arrOrdenIngresoCabecera, $arrClienteNuevo, $arrVehiculoNuevo)
		:
			$this->OrdenIngresoModel->actualizarVenta(array('ID_Orden_Ingreso' => $_POST['arrOrdenIngresoCabecera']['EID_Orden_Ingreso']), $arrOrdenIngresoCabecera, $arrClienteNuevo, $arrVehiculoNuevo)
		);
	}
	
	public function eliminarOrdenIngreso($ID, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenIngresoModel->eliminarOrdenIngreso($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	
	public function estadoOrdenIngreso($ID, $Nu_Descargar_Inventario, $Nu_Estado){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenIngresoModel->estadoOrdenIngreso($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario), $this->security->xss_clean($Nu_Estado)));
	}
	
	public function duplicarOrdenIngreso($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenIngresoModel->duplicarOrdenIngreso($this->security->xss_clean($ID)));
	}

	public function getOrdenIngresoPDF($ID){
        $data = $this->OrdenIngresoModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Ventas/pdf/OrdenTrabajoViewPDF', array(
			'arrData' => $data,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - cotizacion Nro. ' . $data[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
		
		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.png';
		}
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 20%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 80%; text-align: left;">';
					$format_header .= '<p>';
						$format_header .= '<br>';
						$format_header .= '<a style="text-decoration: none;" href="https://'.$this->empresa->No_Dominio_Empresa.'" target="_blank"><label style="color: #000000; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->No_Dominio_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #868686; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Nu_Celular_Empresa . '</label><br>';
						$format_header .= '<label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label><br>';
						//$format_header .= '<a style="text-decoration: none;" href="mailto:' . $this->empresa->Txt_Email_Empresa . '" target="_top" lt="Correo LAE System" title="Correo LAE System" data-attr="email"><label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #979797; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Slogan_Empresa . '</label>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_cotizacion_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".pdf";
		$pdf->Output($file_name, 'I');
	}

	public function enviarCorreo(){
        $arrData = $this->OrdenIngresoModel->get_by_id($this->input->post('iIdOrden'));

		$this->load->library('email');

		$data = array();

		$data["No_Documento"] = 'Cotización Nro. ' . $this->input->post('iIdOrden');
		$data["Fe_Emision"] = ToDateBD($arrData[0]->Fe_Emision);
		$data["Fe_Vencimiento"] = ToDateBD($arrData[0]->Fe_Vencimiento);
		$data["No_Signo"] = $arrData[0]->No_Signo;
		$data["Ss_Total"] = $arrData[0]->Ss_Total;
		
		$data["No_Entidad"] = $arrData[0]->No_Entidad;
		
		$data["No_Empresa"]	= $this->empresa->No_Empresa;
		$data["Nu_Documento_Identidad_Empresa"] = $this->empresa->Nu_Documento_Identidad;
		
		$message = $this->load->view('correos/orden_venta', $data, true);
		$asunto = $this->input->post('sAsunto');
		
		$this->email->from($this->input->post('sCorreoDe'), $this->empresa->No_Empresa);//de
		$this->email->to($this->input->post('sCorreoPara'));//para
		$this->email->subject($asunto);
		$this->email->message($message);

		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Ventas/pdf/OrdenTrabajoViewPDF', array(
			'arrData' => $arrData,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems_cotizacion_' . $arrData[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
		
    	$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.jpg';
		}
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 20%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 80%; text-align: left;">';
					$format_header .= '<p>';
						$format_header .= '<br>';
						$format_header .= '<a style="text-decoration: none;" href="https://' . $this->empresa->No_Dominio_Empresa . '" target="_blank"><label style="color: #000000; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->No_Dominio_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #868686; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Nu_Celular_Empresa . '</label><br>';
						$format_header .= '<label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label><br>';
						//$format_header .= '<a style="text-decoration: none;" href="mailto:' . $this->empresa->Txt_Email_Empresa . '" target="_top" lt="Correo LAE System" title="Correo LAE System" data-attr="email"><label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #979797; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Slogan_Empresa . '</label>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_cotizacion_" . $arrData[0]->ID_Documento_Cabecera . "_" . $arrData[0]->Nu_Documento_Identidad . ".pdf";
		$pdfdoc = $pdf->Output(__DIR__ . $file_name, "F");
		
		$this->email->attach(__DIR__ . $file_name);

		$this->email->set_newline("\r\n");

		$isSend = $this->email->send();
		
		if($isSend) {
			unlink(__DIR__ . $file_name);
			$peticion = array(
				'status' => 'success',
				'style_modal' => 'modal-success',
				'message' => 'Correo enviado',
			);
			echo json_encode($peticion);
			exit();
		} else {
			unlink(__DIR__ . $file_name);
			$peticion = array(
				'status' => 'error',
				'style_modal' => 'modal-danger',
				'message' => 'No se pudo enviar el correo, inténtelo más tarde.',
				'sMessageErrorEmail' => $this->email->print_debugger(),
			);
			echo json_encode($peticion);
			exit();
		}// if - else envio email
	}

	public function cambiarEstadoFacturaOI($ID, $Nu_Estado){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenIngresoModel->cambiarEstadoFacturaOI($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Estado)));
	}
}