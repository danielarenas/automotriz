<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class VentaController extends CI_Controller {
	private $file_path = '../assets/images/logos/';
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Ventas/VentaModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
		$this->load->model('DocumentoElectronicoModel');
	}

	public function listarVentas(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/VentaView');
			$this->load->view('footer', array("js_venta" => true));
		}
	}
	 
	public function ajax_list(){
		$sMethod = 'listarVentas';
		$arrData = $this->VentaModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action_anular = 'anular';
        $action_delete = 'delete';
        foreach ($arrData as $row) {
            $no++;
			$rows = array();
			$rows[] = ToDateBD($row->Fe_Emision);
			$rows[] = $row->No_Tipo_Documento_Breve;
            $rows[] = $row->ID_Serie_Documento;
            $rows[] = $row->ID_Numero_Documento;
            $rows[] = $row->No_Entidad;
            $rows[] = $row->No_Signo;
			$rows[] = $row->No_Medio_Pago;
			$rows[] = $row->No_Tipo_Medio_Pago;
			//	M02 - 2 - INICIO - total en negativo en nc 
            //$rows[] = numberFormat($row->Ss_Total, 2, '.', ',');
			if ($row->ID_Tipo_Documento==5){
            $rows[] = numberFormat($row->Ss_Total*(-1), 2, '.', ',');
			}else{
			$rows[] = numberFormat($row->Ss_Total, 2, '.', ',');
			}
			//	M02 - 2 - FIN 
            $rows[] = numberFormat($row->Ss_Total_Saldo, 2, '.', ',');
			$rows[] = numberFormat($row->Ss_Saldo_Detraccion, 2, '.', ',');
			$rows[] = numberFormat($row->Ss_Retencion, 2, '.', ',');
			$sEstadoPago = 'pendiente';
			$sEstadoPagoClass = 'warning';
			if ($row->Ss_Total_Saldo == 0.00) {
				$sEstadoPago = 'cancelado';
				$sEstadoPagoClass = 'success';
			}				
            $rows[] = '<span class="label label-' . $sEstadoPagoClass . '">' . $sEstadoPago . '</span>';
            
			$arrEstadoDocumento = $this->HelperModel->obtenerEstadoDocumentoArray($row->Nu_Estado);
			$btn_estado_documento_pse = '';
			if($this->empresa->Nu_Tipo_Proveedor_FE == 1 && ($row->Nu_Estado==9 || $row->Nu_Estado==11)) {
				$btn_estado_documento_pse = '<br><button id="btn-consultar_pse-' . $row->ID_Documento_Cabecera . '" class="btn btn-xs btn-link" alt="Verificar estado sunat" title="Verificar estado sunat" href="javascript:void(0)" onclick="obtenerEstadoPSE(\'' . $row->ID_Documento_Cabecera . '\')">Consultar estado</button>';
			}
			$rows[] = '<span class="label label-' . $arrEstadoDocumento['No_Class_Estado'] . '">' . $arrEstadoDocumento['No_Estado'] . '</span>' . $btn_estado_documento_pse;

			$rows[] = $row->Nu_Presupuesto;
			$rows[] = $row->Nu_Orden_Compra;
			$rows[] = (!empty($row->Fe_Entrega_Factura) ? ToDateBD($row->Fe_Entrega_Factura) : '');
			$rows[] = (!empty($row->Fe_Entrega_Vencimiento_Factura) ? ToDateBD($row->Fe_Entrega_Vencimiento_Factura) : '');
			$rows[] = $row->No_Servicio_Presupuesto_Negocio;
			$noteOfCredit = null;
			if ($row->ID_Documento_Cabecera_VE) {
				$noteOfCredit = $this->VentaModel->getNoteOfCredit($row->ID_Documento_Cabecera_VE);
				if (!$noteOfCredit) {
					$noteOfCredit = null;
				}
			} elseif ($row->ID_Tipo_Documento == 5) {
				$noteOfCredit = $this->VentaModel->getDocumentoCabeceraOfNoteOfCredit($row->ID_Documento_Cabecera);
				if (!$noteOfCredit) {
					$noteOfCredit = null;
				}
			}
			$rows[] = ($noteOfCredit) ? "$noteOfCredit->ID_Serie_Documento - $noteOfCredit->ID_Numero_Documento" : '';

			$sTipoBajaSunat = 'Interno';
			if (($row->ID_Tipo_Documento == 4 || $row->ID_Tipo_Documento == 5 || $row->ID_Tipo_Documento == 6) && substr($row->ID_Serie_Documento,0,1) == 'B' )
				$sTipoBajaSunat = 'RC';
			else if (($row->ID_Tipo_Documento == 3 || $row->ID_Tipo_Documento == 5 || $row->ID_Tipo_Documento == 6) && substr($row->ID_Serie_Documento,0,1) == 'F' ) 
				$sTipoBajaSunat = 'RA';

            $btn_send_sunat = '';
            if ( ($row->Nu_Estado == 6 || $row->Nu_Estado == 7 || $row->Nu_Estado == 9 || $row->Nu_Estado == 11) && $row->ID_Tipo_Documento != 2 && $this->empresa->Nu_Tipo_Proveedor_FE != 3 )//Action send SUNAT
            	$btn_send_sunat = '<button id="btn-sunat-' . $row->ID_Documento_Cabecera . '" class="btn btn-xs btn-link" alt="Enviar a Sunat" title="Enviar a Sunat" href="javascript:void(0)" onclick="sendFacturaVentaSunat(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Estado . '\', \'' . $sTipoBajaSunat . '\')"><i class="fa fa-cloud-upload"></i> Sunat</i></button>';
            $rows[] = $btn_send_sunat;
            
			$btn_modificar = '';
			if ( $this->MenuModel->verificarAccesoMenuInterno($sMethod)->Nu_Editar == 1) {
            	if ( $row->ID_POS=='' && ($row->Nu_Estado == 6 || $row->Nu_Estado == 9) )
					$btn_modificar = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verFacturaVenta(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			}
			$rows[] = $btn_modificar;
			
			$btn_anular = '';
			$btn_eliminar = '';
			if ( $this->MenuModel->verificarAccesoMenuInterno($sMethod)->Nu_Editar == 1) {
				if ($row->ID_Documento_Cabecera_Enlace=='') {
					$btn_anular = '';
					if ( $row->Nu_Estado == 8 || ($row->Nu_Estado == 6 && $row->ID_Tipo_Documento == 2) || $this->empresa->Nu_Tipo_Proveedor_FE == 3 ) {
						$btn_anular = '<button class="btn btn-xs btn-link" alt="Anular" title="Anular" href="javascript:void(0)" onclick="anularFacturaVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Enlace . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action_anular . '\', \'' . $row->Nu_Estado . '\', \'' . $sTipoBajaSunat . '\')"><i class="fa fa-minus-circle" aria-hidden="true"> Anular</i></button>';
					}

					$btn_eliminar = '';
					if ($row->Nu_Estado == 6)//Estado SUNAT
						$btn_eliminar = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarFacturaVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Enlace . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action_delete . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
				}
			}
			$rows[] = $btn_anular . $btn_eliminar;
			
			$btn_opciones = '';
			if ($row->Nu_Estado == 8) {
				$btn_opciones = '
				<div class="btn-group">
					<button alt="Opciones" title="Opciones" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Opciones <span class="caret"></span></button>
					<ul class="dropdown-menu">';
				if (!empty($row->Txt_Url_PDF))
					$btn_opciones .= '<li><a alt="Descargar PDF" title="Descargar PDF" href="' . $row->Txt_Url_PDF . '" target="_blank">PDF</a></li>';
				if (!empty($row->Txt_Url_XML))
					$btn_opciones .= '<li><a alt="Descargar XML" title="Descargar XML" href="' . $row->Txt_Url_XML . '" target="_blank">XML</a></li>';
				if (!empty($row->Txt_Url_CDR))
					$btn_opciones .= '<li><a alt="Descargar CDR" title="Descargar CDR" href="' . $row->Txt_Url_CDR . '" target="_blank">CDR</a></li>';
				if ( $row->Nu_Tipo_Medio_Pago == 1 && $row->Ss_Total_Saldo > 0 ) {
					//$btn_opciones .= '<li><a alt="Cobrar" title="Cobrar" href="javascript:void(0)" onclick="cobrarCliente(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Ss_Total_Saldo . '\', \'' . $row->No_Entidad . '\', \'' . $row->No_Tipo_Documento_Breve . '\', \'' . $row->ID_Serie_Documento . '\', \'' . $row->ID_Numero_Documento . '\', \'' . $row->No_Signo . '\')">Cobrar</a></li>';
				}
				$btn_opciones .= '
						<li><a alt="Enviar Email" title="Enviar Email" href="javascript:void(0)" onclick="sendCorreoFacturaVentaSUNAT(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->id_cliente . '\')">Email</a></li>
					</ul>
				</div>';
			} else if ( $row->Nu_Estado == 10 && !empty($row->Txt_Url_PDF) && !empty($row->Txt_Url_XML) ) {
				$btn_opciones = '
				<div class="btn-group">
					<button alt="Opciones" title="Opciones" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Opciones <span class="caret"></span></button>
					<ul class="dropdown-menu">
						<li><a alt="Descargar PDF" title="Descargar PDF" href="' . $row->Txt_Url_PDF . '" target="_blank">PDF</a></li>
						<li><a alt="Descargar XML" title="Descargar XML" href="' . $row->Txt_Url_XML . '" target="_blank">XML</a></li>
					</ul>
				</div>';
			} else if ( ($row->Nu_Estado == 9 || $row->Nu_Estado == 11) && !empty($row->Txt_Respuesta_Sunat_FE)) {
				$btn_opciones = '
				<div class="btn-group">
					<button alt="Opciones" title="Mensaje" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Mensaje <span class="caret"></span></button>
					<ul class="dropdown-menu">
						<li>' . $row->Txt_Respuesta_Sunat_FE . '</li>
					</ul>
				</div>';
			} else if ( $row->ID_Tipo_Documento == 2 && $row->Nu_Tipo_Medio_Pago == 1 && $row->Ss_Total_Saldo > 0 ) {
				//$btn_opciones = '<button class="btn btn-xs btn-link" alt="Cobrar" title="Cobrar" href="javascript:void(0)" onclick="cobrarCliente(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Ss_Total_Saldo . '\', \'' . $row->No_Entidad . '\', \'' . $row->No_Tipo_Documento_Breve . '\', \'' . $row->ID_Serie_Documento . '\', \'' . $row->ID_Numero_Documento . '\', \'' . $row->No_Signo . '\')">Cobrar</button>';
			}

			if ($row->Nu_Tipo_Medio_Pago == 1 && $row->Ss_Total_Saldo > 0) {
				$btn_opciones .= '<li><a alt="Cobrar" title="Cobrar" href="javascript:void(0)" onclick="cobrarCliente(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Ss_Total_Saldo . '\', \'' . $row->No_Entidad . '\', \'' . $row->No_Tipo_Documento_Breve . '\', \'' . $row->ID_Serie_Documento . '\', \'' . $row->ID_Numero_Documento . '\', \'' . $row->No_Signo . '\')">Cobrar</a></li>';
			}

			$rows[] = $btn_opciones;
            
            $btn_representacion_interna = '<button class="btn btn-xs btn-link" alt="Representación Interna PDF" title="Representación Interna PDF" href="javascript:void(0)" onclick="verRepresentacionInternaPDF(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> Ver</i></button>';
            if ($row->Nu_Estado == 8 || $row->Nu_Estado == 7 || $row->Nu_Estado == 10 || $row->Nu_Estado == 11)
                $btn_representacion_interna = '';
            $rows[] = $btn_representacion_interna; 
            
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->VentaModel->count_all(),
	        'recordsFiltered' => $this->VentaModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }

	public function ajax_edit($ID){
		$data = $this->VentaModel->get_by_id($this->security->xss_clean($ID));
		if ($data['sStatus'] == 'success') {
			$arrImpuesto = $this->HelperModel->getImpuestos('');
			$output = [
				'arrEdit' => $data['arrData'],
				'arrImpuesto' => $arrImpuesto,
			];
			echo json_encode($output);
		} else {
			echo json_encode($data);
		}
	}

	public function crudVenta(){
		if (!$this->input->is_ajax_request()) {
			exit('No se puede Agregar/Editar y acceder');
		}
		$response = array('status' => 'success', 'ID_Documento_Cabecera_Enlace' => '');
		if ($_POST['arrVentaCabecera']['esEnlace'] == 1) {
			$response = $this->HelperModel->documentExistVerify($this->security->xss_clean($_POST['arrVentaModificar']['ID_Documento_Guardado']), $this->security->xss_clean($_POST['arrVentaModificar']['ID_Tipo_Documento_Modificar']), $this->security->xss_clean($_POST['arrVentaModificar']['ID_Serie_Documento_Modificar']), $this->security->xss_clean($_POST['arrVentaModificar']['ID_Numero_Documento_Modificar']), $_POST['arrVentaModificar']);
		}

		if ($response['status'] != 'danger') {
			$arrClienteNuevo = '';
			if (isset($_POST['arrClienteNuevo'])){
				$arrClienteNuevo = array(
					'ID_Tipo_Documento_Identidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['ID_Tipo_Documento_Identidad']),
					'Nu_Documento_Identidad' => $this->security->xss_clean(strtoupper($_POST['arrClienteNuevo']['Nu_Documento_Identidad'])),
					'No_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['No_Entidad']),
					'Txt_Direccion_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['Txt_Direccion_Entidad']),
					'Nu_Telefono_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Telefono_Entidad']),
					'Nu_Celular_Entidad' => $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Celular_Entidad']),
				);
			}

			$fDetraccion = 0.00;
			$fRetencion = 0.00;
			$fTotalSaldo = 0.00;
			$fTotalFactura = $this->security->xss_clean($_POST['arrVentaCabecera']['Ss_Total']);

			if ($_POST['arrVentaCabecera']['iTipoFormaPago'] == '1') {
				$fTotalSaldo = $this->security->xss_clean($_POST['arrVentaCabecera']['Ss_Total']);
			}
			if ($_POST['arrVentaCabecera']['Nu_Detraccion'] == '1' && $fTotalFactura >= 700.00 ) {
				$fDetraccion = ($fTotalFactura * 0.12);
				if($fTotalSaldo>0.00) {
					$fTotalSaldo = $fTotalFactura;
					$fTotalSaldo = ($fTotalSaldo - $fDetraccion);
				}
			}
			if ($_POST['arrVentaCabecera']['Nu_Retencion'] == '1' && $fTotalFactura < 700.00) {
				$fRetencion = ($fTotalFactura * 0.03);
				if($fTotalSaldo>0.00) {
					$fTotalSaldo = $fTotalFactura;
					$fTotalSaldo = ($fTotalSaldo - $fRetencion);
				}
			}

			$arrVentaCabecera = array(
				'ID_Empresa' => $this->empresa->ID_Empresa,
				'ID_Organizacion' => $this->empresa->ID_Organizacion,
				'ID_Entidad' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Entidad']),
				'ID_Tipo_Asiento' => 1,
				'ID_Tipo_Documento' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Tipo_Documento']),
				'ID_Serie_Documento_PK' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Serie_Documento_PK']),
				'ID_Serie_Documento' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Serie_Documento']),
				'ID_Numero_Documento' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Numero_Documento']),
				'Fe_Emision' => ToDate($this->security->xss_clean($_POST['arrVentaCabecera']['Fe_Emision'])),
				'ID_Moneda'	=> $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Moneda']),
				'ID_Medio_Pago' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Medio_Pago']),
				'Fe_Vencimiento' => ToDate($this->security->xss_clean($_POST['arrVentaCabecera']['Fe_Vencimiento'])),
				'Fe_Periodo' => ToDate($this->security->xss_clean($_POST['arrVentaCabecera']['Fe_Emision'])),
				'Fe_Emision_Hora' => dateNow('fecha_hora'),
				'Nu_Descargar_Inventario' => $this->security->xss_clean($_POST['arrVentaCabecera']['Nu_Descargar_Inventario']),
				'ID_Almacen' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Almacen']),
				'Txt_Glosa' => $this->security->xss_clean($_POST['arrVentaCabecera']['Txt_Glosa']),
				'Nu_Detraccion' => $this->security->xss_clean($_POST['arrVentaCabecera']['Nu_Detraccion']),
				'Nu_Retencion' => $this->security->xss_clean($_POST['arrVentaCabecera']['Nu_Retencion']),
				'Po_Descuento' => $this->security->xss_clean($_POST['arrVentaCabecera']['Po_Descuento']),
				'Ss_Descuento' => $this->security->xss_clean($_POST['arrVentaCabecera']['Ss_Descuento']),
				'Ss_Saldo_Detraccion' => $fDetraccion,
				'Ss_Retencion' => $fRetencion,
				'Ss_Total' => $this->security->xss_clean($_POST['arrVentaCabecera']['Ss_Total']),
				'Ss_Total_Saldo' => $fTotalSaldo,
				'Nu_Estado' => 6,
				'Nu_Codigo_Motivo_Referencia' => (isset($_POST['arrVentaModificar']['Nu_Codigo_Motivo_Referencia']) ? $this->security->xss_clean($_POST['arrVentaModificar']['Nu_Codigo_Motivo_Referencia']) : '' ),
				'No_Formato_PDF' => $this->security->xss_clean($_POST['arrVentaCabecera']['No_Formato_PDF']),
				'Txt_Garantia' => $this->security->xss_clean($_POST['arrVentaCabecera']['Txt_Garantia']),
				'ID_Mesero'	=> $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Mesero']),
				'ID_Comision' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Comision']),
				'iTipoCliente' => $this->security->xss_clean($_POST['arrVentaCabecera']['iTipoCliente']),
				'ID_Tipo_Medio_Pago' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Tipo_Medio_Pago']),
				'Nu_Presupuesto' => $this->security->xss_clean($_POST['arrVentaCabecera']['Nu_Presupuesto']),
				'Nu_Orden_Compra' => $this->security->xss_clean($_POST['arrVentaCabecera']['Nu_Orden_Compra']),
				'Fe_Entrega_Factura' => (!empty($_POST['arrVentaCabecera']['Fe_Entrega_Factura']) ? ToDate($this->security->xss_clean($_POST['arrVentaCabecera']['Fe_Entrega_Factura'])) : ''),
				'Fe_Entrega_Vencimiento_Factura' => (!empty($_POST['arrVentaCabecera']['Fe_Entrega_Vencimiento_Factura']) ? ToDate($this->security->xss_clean($_POST['arrVentaCabecera']['Fe_Entrega_Vencimiento_Factura'])) : ''),
				'ID_Servicio_Presupuesto_Negocio' => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Servicio_Presupuesto_Negocio']),
				'arrIdPresupuesto' => substr($this->security->xss_clean($_POST['arrVentaCabecera']['arrIdPresupuesto']), 0, -1),
			);

			if ($_POST['arrVentaCabecera']['ID_Lista_Precio_Cabecera'] != 0) {
				$arrVentaCabecera = array_merge($arrVentaCabecera, array("ID_Lista_Precio_Cabecera" => $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Lista_Precio_Cabecera'])));
			}
			if (isset($_POST['arrVentaCabecera']['ID_Documento_Cabecera_Orden']) && !empty($_POST['arrVentaCabecera']['ID_Documento_Cabecera_Orden'])) {
				$_POST['arrVentaCabecera']['esEnlace'] = 1;
				$response['ID_Documento_Cabecera_Enlace'] = $this->security->xss_clean($_POST['arrVentaCabecera']['ID_Documento_Cabecera_Orden']);
			}

			if($_POST['arrVentaCabecera']['ID_Tipo_Documento'] == '5') {
				foreach($_POST['arrDetalleVenta'] as $detalleVentaP) {
					$arrDataNuPresupuesto = $this->db->query("SELECT ID_Numero_Documento FROM documento_cabecera where ID_Documento_Cabecera = " . $detalleVentaP['iIdPresupuesto'])->result();
					if(!empty($arrDataNuPresupuesto)){
						$this->HelperModel->updateEstadoDocumentoFacturado($arrDataNuPresupuesto[0]->ID_Numero_Documento);
					}
				}
			}

			echo json_encode(
			($this->security->xss_clean($_POST['arrVentaCabecera']['EID_Empresa']) != '' && $this->security->xss_clean($_POST['arrVentaCabecera']['EID_Documento_Cabecera']) != '') ?
				$this->actualizarVenta_Inventario(array('ID_Empresa' => $this->security->xss_clean($_POST['arrVentaCabecera']['EID_Empresa']), 'ID_Documento_Cabecera' => $this->security->xss_clean($_POST['arrVentaCabecera']['EID_Documento_Cabecera'])), $arrVentaCabecera, $_POST['arrDetalleVenta'], $_POST['arrVentaCabecera']['esEnlace'], $response['ID_Documento_Cabecera_Enlace'], $arrVentaCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo, $_POST['arrVentaCabecera']['delete_ID_Documento_Detalle'])
				: $this->agregarVenta_Inventario($arrVentaCabecera, $_POST['arrDetalleVenta'], $_POST['arrVentaCabecera']['esEnlace'], $response['ID_Documento_Cabecera_Enlace'], $arrVentaCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo)
			);
		} else {
			echo json_encode($response);
		}
	}

	public function agregarVenta_Inventario($arrVentaCabecera = '', $arrDetalleVenta = '', $esEnlace = '', $ID_Documento_Cabecera_Enlace = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = ''){
		$responseVenta = $this->VentaModel->agregarVenta($arrVentaCabecera, $arrDetalleVenta, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrClienteNuevo);
		if ($responseVenta['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrVentaCabecera['ID_Tipo_Movimiento'] = 1;//Venta
				if ($arrVentaCabecera['ID_Tipo_Documento'] == '5') { //N/C
					$arrVentaCabecera['ID_Tipo_Movimiento'] = 17;//ENTRADA POR DEVOLUCIÓN DEL CLIENTE
				}
				$response = $this->MovimientoInventarioModel->crudMovimientoInventario($arrVentaCabecera['ID_Almacen'], $responseVenta['Last_ID_Documento_Cabecera'], 0, $arrDetalleVenta, $arrVentaCabecera['ID_Tipo_Movimiento'], 0, '', 1, 1);
				if ( $arrVentaCabecera['ID_Tipo_Documento'] != 2 ) {
					if ($this->empresa->Nu_Enviar_Sunat_Automatic==0) {
						return $response;
					} else {//1=Enviar a sunat automaticamente
						$response = $this->sendFacturaVentaSunat($responseVenta['Last_ID_Documento_Cabecera'], 6, 'php', 'RC');
						return array_merge($response, array('sEnviarSunatAutomatic' => 'Si'));
					}
				} else {
					return $response;
				}
			}// ./ Generar Inventario
			if ( $arrVentaCabecera['ID_Tipo_Documento'] != 2 ) {
				if ($this->empresa->Nu_Enviar_Sunat_Automatic==0) {
					return $responseVenta;
				} else {//1=Enviar a sunat automaticamente
					$response = $this->sendFacturaVentaSunat($responseVenta['Last_ID_Documento_Cabecera'], 6, 'php', 'RC');
					return array_merge($response, array('sEnviarSunatAutomatic' => 'Si'));
				}
			} else {
				return $response;
			}
		} else {
			return $responseVenta;
		}
	}

	public function actualizarVenta_Inventario($arrWhereVenta = '', $arrVentaCabecera = '', $arrDetalleVenta = '', $esEnlace = '', $ID_Documento_Cabecera_Enlace = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = '', $arrDeleteDetalle = null){
		$responseVenta = $this->VentaModel->actualizarVenta($arrWhereVenta, $arrVentaCabecera, $arrDetalleVenta, $esEnlace, $ID_Documento_Cabecera_Enlace, $arrClienteNuevo, $arrDeleteDetalle);
		if ($responseVenta['status'] == 'success') {
			if ($Nu_Descargar_Inventario == '1'){
				$arrVentaCabecera['ID_Tipo_Movimiento'] = 1; //Venta
				if ($arrVentaCabecera['ID_Tipo_Documento'] == '5') {
					$arrVentaCabecera['ID_Tipo_Movimiento'] = 17; //ENTRADA POR DEVOLUCIÓN DEL CLIENTE
				}
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrVentaCabecera['ID_Almacen'], $responseVenta['Last_ID_Documento_Cabecera'], 0, $arrDetalleVenta, $arrVentaCabecera['ID_Tipo_Movimiento'], 1, $arrWhereVenta, 1, 1);
			}
			return $responseVenta;
		} else {
			return $responseVenta;
		}
	}

	public function anularVenta($ID, $Nu_Enlace, $Nu_Descargar_Inventario, $iEstado, $sTipoBajaSunat){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		if ( $sTipoBajaSunat != 'Interno' ) {
			$iEstado = ($iEstado == 8 || $iEstado == 7 ? 7 : $iEstado );
			if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 ) {//Nubefact
				$arrResponseAnular = $this->VentaModel->anularVenta($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Enlace), $this->security->xss_clean($Nu_Descargar_Inventario));
				if ($arrResponseAnular['status']=='error') {
					echo json_encode($arrResponseAnular);
					exit();
				}
				$this->sendFacturaVentaSunat($this->security->xss_clean($ID), $iEstado, 'json', $sTipoBajaSunat);
			} else {
				$this->sendFacturaVentaSunat($this->security->xss_clean($ID), $iEstado, 'json', $sTipoBajaSunat, $Nu_Enlace, $Nu_Descargar_Inventario);
			}
		} else {			
			echo json_encode($this->VentaModel->anularVenta($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Enlace), $this->security->xss_clean($Nu_Descargar_Inventario)));
		}
	}
	
	public function eliminarVenta($ID, $Nu_Enlace, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->VentaModel->eliminarVenta($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Enlace), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	
	public function imprimirVenta($ID){
		$data = $this->VentaModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('EnLetras', 'el');
		$EnLetras = new EnLetras();
		$this->load->view('Ventas/impresiones/impresion_comprobante', array(
			'venta' 		=> $data,
			'totalEnLetras'	=> $EnLetras->ValorEnLetras($data[0]->Ss_Total, $data[0]->No_Moneda),
		));
	}
    
	public function sendFacturaVentaSunat($ID, $Nu_Estado, $sTypeResponse='json', $sTipoBajaSunat = 'RC', $Nu_Enlace='', $Nu_Descargar_Inventario=''){
		if ( $this->empresa->Nu_Tipo_Proveedor_FE == 2 && $this->empresa->Nu_Estado_Pago_Sistema == 1 ) {//Sunat
			$arrParams = array(
				'iCodigoProveedorDocumentoElectronico' => 1,
				'iEstadoVenta' => $Nu_Estado,
				'iIdDocumentoCabecera' => $ID,
				'sEmailCliente' => '',
				'sTipoRespuesta' => $sTypeResponse,
				'sTipoBajaSunat' => $sTipoBajaSunat,
			);
			
			$response = $this->DocumentoElectronicoModel->generarFormatoDocumentoElectronicoSunat($arrParams);
			$this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $response, $arrParams );
			
			if ( $Nu_Estado == 7 || $Nu_Estado == 11 ) {
				$arrResponseAnular = $this->VentaModel->anularVenta($ID, $Nu_Enlace, $Nu_Descargar_Inventario);
				if ($arrResponseAnular['status']=='error') {	
					if ($sTypeResponse=='php') {
						echo json_encode($arrResponseAnular);
						exit();
					} else {
						return $arrResponseAnular;
					}
				}
				$Nu_Estado = ( $response['sStatus'] ? 10 : 11);
				$arrParams = array_merge($arrParams, array('iEstadoVenta' => $Nu_Estado));
				$this->DocumentoElectronicoModel->cambiarEstadoDocumentoElectronico( $arrParams );
			}

			if ($sTypeResponse=='php') {
				return $response;
			} else {
				echo json_encode($response);
			}
		} else if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 && $this->empresa->Nu_Estado_Pago_Sistema == 1 ) { //Nubefact
			// Parametros de entrada
			$ID = $this->security->xss_clean($ID);
			$Nu_Estado	= $this->security->xss_clean($Nu_Estado);
			
			if ($Nu_Estado == 7 || $Nu_Estado == 11) {// Estado -> Anulado
				$arrData = $this->VentaModel->get_by_id_anulado($ID);

				$iTipoComprobante = 2;//Boleta
				if ($arrData[0]->ID_Tipo_Documento == 3)//Factura
					$iTipoComprobante = 1;
				else if ($arrData[0]->ID_Tipo_Documento == 5)//N/Crédito
					$iTipoComprobante = 3;
				else if ($arrData[0]->ID_Tipo_Documento == 6)//N/Débito
					$iTipoComprobante = 4;
				
				$data = array(
					"operacion"	=> "generar_anulacion",
					"tipo_de_comprobante" => $iTipoComprobante,
					"serie" => $arrData[0]->ID_Serie_Documento,
					"numero" => $arrData[0]->ID_Numero_Documento,
					"motivo" => "ERROR DEL SISTEMA",
					"codigo_unico" => "",
				);
				
				$ruta = $arrData[0]->Txt_FE_Ruta;
				$token = $arrData[0]->Txt_FE_Token;
			} else {
				$arrData = $this->VentaModel->get_by_id($ID);
				if ( $arrData['sStatus'] == 'success' ) {
					$arrData = $arrData['arrData'];
					$ruta = $arrData[0]->Txt_FE_Ruta;
					
					$token = $arrData[0]->Txt_FE_Token;
					
					$i = 0;
					$Ss_SubTotal_Producto = 0.00;
					$Ss_Impuesto_Producto = 0.00;
					$Ss_IGV_Producto = 0.00;
					$Ss_IGV_Producto_Linea = 0.00;
					$Ss_Descuento_Producto = 0.00;
					$Ss_Total_Producto = 0.00;
					$Ss_Gravada = 0.00;
					$Ss_Exonerada = 0.00;
					$Ss_Inafecto = 0.00;
					$Ss_Gratuita = 0.00;
					$Ss_IGV = 0.00;
					$Ss_Total = 0.00;
					$option_impuesto_producto = '';
					
					$fDescuento_Producto = 0;
					$fDescuento_Total_Producto = 0;
					$globalImpuesto = 0;
					$iDescuentoGravada = 0;
					$iDescuentoInafecto = 0;
					$iDescuentoExonerada = 0;
					$iDescuentoGratuita = 0;
					$iDescuentoGlobalImpuesto = 0;
					
					$fTotalIcbper = 0.00;
					$Po_IGV = "";
					foreach ($arrData as $row) {
						$Ss_Precio_VU = round($row->Ss_Precio, 6);
						if ($row->Nu_Tipo_Impuesto == 1){//IGV
							$Po_IGV = "18.00";
							$Ss_Precio_VU = round($row->Ss_Precio / $row->Ss_Impuesto, 6);
							$Ss_IGV += $row->Ss_Impuesto_Producto;
						
							$Ss_Gravada += $row->Ss_SubTotal_Producto;
						} else if ($row->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
							$Ss_Inafecto += $row->Ss_SubTotal_Producto;
						} else if ($row->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
							$Ss_Exonerada += $row->Ss_SubTotal_Producto;
						} else if ($row->Nu_Tipo_Impuesto == 4){//Gratuita
							$Ss_Gratuita += $row->Ss_SubTotal_Producto;
						}

						if ( $row->ID_Impuesto_Icbper == 1 )
							$fTotalIcbper += $row->Ss_Total_Producto;

						$Ss_Descuento_Producto += $row->Ss_Descuento_Producto;
						$Ss_Total += $row->Ss_Total_Producto;
						
						$data_detalle["items"][$i]["unidad_de_medida"]			= $row->Nu_Sunat_Codigo_UM;
						$data_detalle["items"][$i]["codigo"]					= $row->Nu_Codigo_Barra;
						$data_detalle["items"][$i]["codigo_producto_sunat"]		= $row->Nu_Codigo_Producto_Sunat;//Nuevo
						$data_detalle["items"][$i]["descripcion"]				= $row->No_Producto;
						$data_detalle["items"][$i]["cantidad"]					= $row->Qt_Producto;
						$data_detalle["items"][$i]["valor_unitario"]			= $Ss_Precio_VU;//Precio sin IGV
						$data_detalle["items"][$i]["precio_unitario"]			= $row->Ss_Precio;//Precio con IGV
						$data_detalle["items"][$i]["descuento"] 				= $row->Ss_Descuento_Producto;
						$data_detalle["items"][$i]["subtotal"]					= $row->Ss_SubTotal_Producto;
						$data_detalle["items"][$i]["tipo_de_igv"]				= $row->Nu_Valor_Fe_Impuesto;
						$data_detalle["items"][$i]["igv"]						= $row->Ss_Impuesto_Producto;
						$data_detalle["items"][$i]["total"] 					= $row->Ss_Total_Producto;
						$data_detalle["items"][$i]["anticipo_regularizacion"]	= false;
						$data_detalle["items"][$i]["anticipo_documento_serie"]	= "";
						$data_detalle["items"][$i]["anticipo_documento_numero"] = "";
						$i++;
					}
					$iTipoComprobanteModifica = "";
					if (!empty($arrData[0]->ID_Tipo_Documento_Modificar)) {
						$iTipoComprobanteModifica = 2;//BOLETAS DE VENTA ELECTRÓNICAS
						if ($arrData[0]->ID_Tipo_Documento_Modificar == 3)//Factura
							$iTipoComprobanteModifica = 1;//FACTURAS ELECTRÓNICAS
					}
					
					$iIdClienteTipoDocumentoIdentidad = $arrData[0]->Nu_Sunat_Codigo_TDI;
					$sNombreCliente = $arrData[0]->No_Entidad;
					if (
						$arrData[0]->ID_Tipo_Documento == 4
						&& (empty($arrData[0]->Nu_Documento_Identidad) || empty($arrData[0]->No_Entidad))
						&& ($Ss_Total + $fTotalIcbper) < 700
					) {
						$iIdClienteTipoDocumentoIdentidad = '-';
						$sNombreCliente = 'vacio';
					}
/*
					$sDiasCredito = '';
					if ( $arrData[0]->No_Codigo_Medio_Pago_Sunat_PLE == '0' ) {
						$sDiasCredito = 'CRÉDITO ' . $arrData[0]->Nu_Dias_Credito . ' DÍAS';
						if (  $arrData[0]->Nu_Dias_Credito == 0 || $arrData[0]->Nu_Dias_Credito == '' ) {
							$date1 = new DateTime($arrData[0]->Fe_Emision);
							$date2 = new DateTime($arrData[0]->Fe_Vencimiento);
							$diff = $date1->diff($date2);
							$sDiasCredito = 'CRÉDITO ' . $diff->days . ' DÍAS';
						}
					}
*/

					$sDiasCredito = '';
					$arrVentasCreditoCuotas = array();
					if ( $arrData[0]->No_Codigo_Medio_Pago_Sunat_PLE == '0' ) {
						//$sDiasCredito = 'CRÉDITO ' . $arrData[0]->Nu_Dias_Credito . ' DÍAS';
						if (  $arrData[0]->Nu_Dias_Credito == 0 || $arrData[0]->Nu_Dias_Credito == '' ) {
							//$iDays = diferenciaFechasMultipleFormato($arrData[0]->Fe_Emision, $arrData[0]->Fe_Vencimiento, 'dias');
							//$sDiasCredito = 'CRÉDITO ' . $iDays . ' DÍAS';
						}

						$arrVentasCreditoCuotas = array(
							'venta_al_credito' => array(
								0 => array(
									'cuota' => 1,
									'fecha_de_pago' => $arrData[0]->Fe_Vencimiento,
									'importe' => $arrData[0]->Ss_Total_Saldo,
								)
							)
						);
					}

					//nubefact
					$sConcatenarMultiplesMedioPago = $arrData[0]->Txt_Medio_Pago;
					if ( $arrData[0]->No_Codigo_Medio_Pago_Sunat_PLE == '006' )
						$sConcatenarMultiplesMedioPago = 'PAGO CON TARJETA';//Tarjeta de crédito
					
					
					$iTipoNC = "";
					$iTipoND = "";
					
					$iTipoComprobante = 2;//Boleta
					if ($arrData[0]->ID_Tipo_Documento == 3) {//Factura
						$iTipoComprobante = 1;
					} else if ($arrData[0]->ID_Tipo_Documento == 5) {//N/Crédito
						$iTipoComprobante = 3;
						$iTipoNC = $arrData[0]->Nu_Codigo_Motivo_Referencia;
						
						if ( $arrData[0]->No_Codigo_Medio_Pago_Sunat_PLE == '0' ) {
							$sConcatenarMultiplesMedioPago='<b>[CREDITO POR PAGAR] CUOTA 1</b> ' . $arrData[0]->Fe_Vencimiento . ' (' . $arrData[0]->No_Signo . ' ' . $arrData[0]->Ss_Total_Saldo . ')';
						}
						//[CREDITO POR PAGAR] CUOTA 1 2021-12-01(S/50.0)
					} else if ($arrData[0]->ID_Tipo_Documento == 6) {//N/Débito
						$iTipoComprobante = 4;
						$iTipoND = $arrData[0]->Nu_Codigo_Motivo_Referencia;
						$sConcatenarMultiplesMedioPago='';
					}
					

					$Txt_Glosa = $arrData[0]->Txt_Glosa;
					$fTotal = ($Ss_Total + $fTotalIcbper);
					//if($arrData[0]->Ss_Saldo_Detraccion > 0.00 && $arrData[0]->No_Codigo_Medio_Pago_Sunat_PLE == '0'){
					if($arrData[0]->Ss_Saldo_Detraccion > 0.00){
						$Txt_Glosa .= (!empty($Txt_Glosa) ? ' <br> ' : '');
						$Txt_Glosa .= 'Monto neto de pago: ' . round($fTotal - $arrData[0]->Ss_Saldo_Detraccion, 2) . ' <br>';
					}
					
					//if($arrData[0]->Ss_Retencion > 0.00 && $arrData[0]->No_Codigo_Medio_Pago_Sunat_PLE == '0'){
					if($arrData[0]->Ss_Retencion > 0.00){
						$Txt_Glosa .= (!empty($Txt_Glosa) ? ' <br> ' : '');
						$Txt_Glosa .= 'Monto neto de pago: ' . round($fTotal - $arrData[0]->Ss_Retencion, 2) . ' <br>';
						$Txt_Glosa .= '<br> Base imponible de la retención: ' . ($Ss_Total) . ' <br>';
						$Txt_Glosa .= '<br> Monto de la retención: ' . round($arrData[0]->Ss_Retencion, 2) . ' <br>';
					}

					$data_cabecera = array(
						"operacion"							=> "generar_comprobante",
						"tipo_de_comprobante"               => $iTipoComprobante,
						"serie"                             => $arrData[0]->ID_Serie_Documento,
						"numero"							=> $arrData[0]->ID_Numero_Documento,
						"sunat_transaction"					=> "1",
						"cliente_tipo_de_documento"			=> $iIdClienteTipoDocumentoIdentidad,
						"cliente_numero_de_documento"		=> $arrData[0]->Nu_Documento_Identidad,
						"cliente_denominacion"              => $sNombreCliente,
						"cliente_direccion"                 => $arrData[0]->Txt_Direccion_Entidad,
						"cliente_email"                     => "",
						"cliente_email_1"                   => "",
						"cliente_email_2"                   => "",
						"fecha_de_emision"                  => $arrData[0]->Fe_Emision,
						"fecha_de_vencimiento"              => $arrData[0]->Fe_Vencimiento,
						"moneda"                            => $arrData[0]->Nu_Valor_Fe_Moneda,
						"tipo_de_cambio"                    => ($arrData[0]->Nu_Valor_Fe_Moneda == 1 ? "" : $arrData[0]->Ss_Tipo_Cambio),
						"porcentaje_de_igv"                 => $Po_IGV,
						"descuento_global"                  => "",
						"total_descuento"                   => $arrData[0]->Ss_Descuento,
						"total_anticipo"                    => "",
						"total_gravada"                     => $Ss_Gravada,
						"total_inafecta"                    => $Ss_Inafecto,
						"total_exonerada"                   => $Ss_Exonerada,
						"total_igv"                         => $Ss_IGV,
						"total_gratuita"                    => $Ss_Gratuita,
						"total_otros_cargos"                => "",
						"total"                             => $fTotal,
						"retencion_tipo"=> ($arrData[0]->Nu_Retencion == 0 ? "" : "1"),
						"retencion_base_imponible"=> ($arrData[0]->Nu_Retencion == 0 ? "" : $Ss_Total),
						"total_retencion"=> ($arrData[0]->Nu_Retencion == 0 ? "" : $arrData[0]->Ss_Retencion),
						"percepcion_tipo"                   => "",
						"percepcion_base_imponible"         => "",
						"total_percepcion"                  => "",
						"total_incluido_percepcion"         => "",
						"total_impuestos_bolsas" => $fTotalIcbper,
						"detraccion"                        => ($arrData[0]->Nu_Detraccion == 0 ? false : true),
						"observaciones"                     => $Txt_Glosa,
						"documento_que_se_modifica_tipo"    => $iTipoComprobanteModifica,
						"documento_que_se_modifica_serie"   => $arrData[0]->ID_Serie_Documento_Modificar,
						"documento_que_se_modifica_numero"  => $arrData[0]->ID_Numero_Documento_Modificar,
						"tipo_de_nota_de_credito"           => $iTipoNC,
						"tipo_de_nota_de_debito"            => $iTipoND,
						"enviar_automaticamente_a_la_sunat" => true,
						"enviar_automaticamente_al_cliente" => false,
						"codigo_unico"                      => $iTipoComprobante . $arrData[0]->ID_Serie_Documento . $arrData[0]->ID_Numero_Documento,
						"condiciones_de_pago"               => $sDiasCredito,
						"medio_de_pago"                     => $sConcatenarMultiplesMedioPago,
						"placa_vehiculo"                    => "",
						"orden_compra_servicio"             => "",
						"tabla_personalizada_codigo"        => "",
						"formato_de_pdf"                    => $arrData[0]->No_Formato_PDF,
					);
					
					//$data = array_merge($data_cabecera, $data_detalle);
					$data = array_merge($data_cabecera, $data_detalle, $arrVentasCreditoCuotas);

					$cadena_de_texto = $arrData[0]->Txt_Garantia;
					$cadena_buscada = '-';
					$posicion_coincidencia = strpos($cadena_de_texto, $cadena_buscada);
					if ( strlen($arrData[0]->Txt_Garantia) > 5 && $posicion_coincidencia !== false) {
						$arrCadena = explode(',',$arrData[0]->Txt_Garantia);
						foreach ($arrCadena as $row) {
							$arrGuias['guias'][] = array(
								"guia_tipo" => '1',
								"guia_serie_numero" => $row,
							);
						}
						$data = array_merge($data, $arrGuias);
					}
				} else {
					if ($sTypeResponse=='json') {
						echo json_encode($arrData);
						exit();
					} else {
						return $arrData;
					}
				}// if - else arrdata modal get documento
			}// if - else estado de documento
			
			$data_json = json_encode($data);
			//array_debug($data_json);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $ruta);
			curl_setopt(
				$ch, CURLOPT_HTTPHEADER, array(
				'Authorization: Token token="'.$token.'"',
				'Content-Type: application/json',
				)
			);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$respuesta = curl_exec($ch);
			curl_close($ch);
			
			$leer_respuesta = json_decode($respuesta, true);
			if ( (!isset($leer_respuesta['errors']) && !empty($leer_respuesta)) || isset($leer_respuesta['codigo']) ) {
				if ($Nu_Estado == 6 || $Nu_Estado == 8 || $Nu_Estado == 9)// 6 = Completado y 8 = Completado enviado
					$iTipoStatus = 8;
				else if ($Nu_Estado == 7 || $Nu_Estado == 10 || $Nu_Estado == 11)
					$iTipoStatus = 10;//10 = Anulado enviado
				
				if ( !isset($leer_respuesta['codigo']) ) {
					$arrParametrosNubefact = array (
						'url_pdf' => $leer_respuesta['enlace_del_pdf'],
						'url_xml' => $leer_respuesta['enlace_del_xml'],
						'url_cdr' => $leer_respuesta['enlace_del_cdr'],
						'url_nubefact' => $leer_respuesta['enlace'],
						'Txt_QR' => isset($leer_respuesta['cadena_para_codigo_qr']) ? $leer_respuesta['cadena_para_codigo_qr'] : '',
						'Txt_Hash' => isset($leer_respuesta['codigo_hash']) ? $leer_respuesta['codigo_hash'] : '',
					);
				} else {
					$arrParametrosNubefact = array ();
				}
				$arrResponseCambiarEstadoDocumento = $this->VentaModel->changeStatusSunat($ID, $iTipoStatus, $arrParametrosNubefact);
				if ( $arrResponseCambiarEstadoDocumento['sStatus'] == 'success' ) {	
					if ( $iTipoStatus == 8 && !empty($arrData[0]->Txt_Email_Entidad) )
						$response_email = $this->sendCorreoFacturaVentaSUNAT($ID, $arrData[0]->Txt_Email_Entidad);

					if ($sTypeResponse=='json') {
						$arrResponseFE = array(
							'sStatus' => !isset($leer_respuesta['codigo']) ? 'success' : 'warning',
							'status' => !isset($leer_respuesta['codigo']) ? 'success' : 'warning',
							'style_modal' => !isset($leer_respuesta['codigo']) ? 'modal-success' : 'modal-warning',
							'message' => !isset($leer_respuesta['codigo']) ? 'Comprobante enviado' : 'Venta guardada pero ' . $leer_respuesta['errors'] . ' (JSON)',
							'arrMessagePSE' => !isset($leer_respuesta['errors']) ? 'Comprobante aceptado' : $leer_respuesta['errors'] . ' (JSON)',
							'sCodigo' => !isset($leer_respuesta['codigo']) ? '0' : $leer_respuesta['codigo'],
							'arrData' => $data
						);
						if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 ) {//Nubefact
							$arrParams['iIdDocumentoCabecera'] = $this->security->xss_clean($ID);
							$this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE, $arrParams );
						} else {
							$arrParams['iIdDocumentoCabecera'] = $this->security->xss_clean($ID);
							$this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE='', $arrParams );
						}
						echo json_encode($arrResponseFE);
						exit();
					} else {
						return array(
							'sStatus' => !isset($leer_respuesta['codigo']) ? 'success' : 'warning',
							'status' => !isset($leer_respuesta['codigo']) ? 'success' : 'warning',
							'style_modal' => !isset($leer_respuesta['codigo']) ? 'modal-success' : 'modal-warning',
							'message' => !isset($leer_respuesta['codigo']) ? 'Comprobante enviado' : 'Venta guardada pero ' . $leer_respuesta['errors'],
							'sMessage' => !isset($leer_respuesta['codigo']) ? 'Comprobante enviado' : 'Venta guardada pero ' . $leer_respuesta['errors'],
							'arrMessagePSE' => !isset($leer_respuesta['errors']) ? 'Comprobante aceptado' : $leer_respuesta['errors'],
							'sCodigo' => !isset($leer_respuesta['codigo']) ? '0' : $leer_respuesta['codigo'],
							'arrData' => $data
						);
					}
				} else {
					if ($sTypeResponse=='json') {
						echo json_encode($arrResponseCambiarEstadoDocumento);
						exit();
					} else {
						return $arrResponseCambiarEstadoDocumento;
					}
				}// /. if - else cambiar estado documento despues de enviarlo nubefact
			} else {
				if ($sTypeResponse=='json') {
					$arrResponseFE = array(
						'status' => 'error',
						'style_modal' => 'modal-danger',
						'message' => (!empty($leer_respuesta) ? 'Problemas al generar documento electrónico' : 'Venta guardada pero no se envio a SUNAT (JSON)'),
						'message_nubefact' => $leer_respuesta['errors'],
						'sStatus' => 'danger',
						'sMessage' => (!empty($leer_respuesta) ? 'Problemas al generar documento electrónico' : 'Venta guardada pero no se envio a SUNAT (JSON)'),
						'arrMessagePSE' => !empty($leer_respuesta['errors']) ? $leer_respuesta['errors'] : 'Venta guardada pero no se envio a SUNAT (JSON)',
						'sCodigo' => !isset($leer_respuesta['codigo']) ? '0' : $leer_respuesta['codigo'],
						'arrData' => $data
					);
					if ( $this->empresa->Nu_Tipo_Proveedor_FE == 1 ) {//Nubefact
						$arrParams['iIdDocumentoCabecera'] = $this->security->xss_clean($ID);
						$this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE, $arrParams );
					} else {
						$arrParams['iIdDocumentoCabecera'] = $this->security->xss_clean($ID);
						$this->DocumentoElectronicoModel->agregarMensajeRespuestaProveedorFE( $arrResponseFE='', $arrParams );
					}
					echo json_encode($arrResponseFE);
					exit();
				} else {
					return array(
						'status' => 'error',
						'style_modal' => 'modal-danger',
						'message' => (!empty($leer_respuesta) ? 'Problemas al generar documento electrónico' : 'Venta guardada pero no se envio a SUNAT'),
						'message_nubefact' => $leer_respuesta['errors'],
						'sStatus' => 'danger',
						'sMessage' => (!empty($leer_respuesta) ? 'Problemas al generar documento electrónico' : 'Venta guardada pero no se envio a SUNAT'),
						'arrMessagePSE' => !empty($leer_respuesta['errors']) ? $leer_respuesta['errors'] : 'Venta guardada pero no se envio a SUNAT',
						'sCodigo' => !isset($leer_respuesta['codigo']) ? '0' : $leer_respuesta['codigo'],
						'arrData' => $data
					);
				}
			}
		} else if ( $this->empresa->Nu_Estado_Pago_Sistema == 0 ) {
			if ($sTypeResponse=='json') {
				$arrResponseFE = array(
					'status' => 'error',
					'style_modal' => 'modal-danger',
					'message' => 'Corte de servicio por falta de pago, tienen hasta 7 días calendarios para enviar el documento pasada la fecha no se podrá enviar el comprobante',
					'message_nubefact' => 'Corte de servicio por falta de pago, tienen hasta 7 días calendarios para enviar el documento pasada la fecha no se podrá enviar el comprobante',
					'sStatus' => 'danger',
					'sMessage' => 'Corte de servicio por falta de pago, tienen hasta 7 días calendarios para enviar el documento pasada la fecha no se podrá enviar el comprobante',
					'arrMessagePSE' => '',
					'sCodigo' => '',
				);
				echo json_encode($arrResponseFE);
				exit();
			} else {
				return array(
					'status' => 'error',
					'style_modal' => 'modal-danger',
					'message' => 'Corte de servicio por falta de pago, tienen hasta 7 días calendarios para enviar el documento pasada la fecha no se podrá enviar el comprobante',
					'message_nubefact' => 'Corte de servicio por falta de pago, tienen hasta 7 días calendarios para enviar el documento pasada la fecha no se podrá enviar el comprobante',
					'sStatus' => 'danger',
					'sMessage' => 'Corte de servicio por falta de pago, tienen hasta 7 días calendarios para enviar el documento pasada la fecha no se podrá enviar el comprobante',
					'arrMessagePSE' => '',
					'sCodigo' => '',
				);
			}
		} // ./ if - else tipo de envio proveedor fe sunat
	}
    
	public function sendCorreoFacturaVentaSUNAT($id=0, $Txt_Email_Entidad=''){
		// Parametros de entrada
		$iIdDocumentoCabecera = !isset($_POST['ID']) ? $id : $this->input->post('ID');
		$arrData = $this->VentaModel->get_by_id($iIdDocumentoCabecera);
		if ( $arrData['sStatus'] == 'success' ) {
			$arrData = $arrData['arrData'];

			$this->load->library('email');

			$data = array();

			$data["No_Documento"]	= strtoupper($arrData[0]->No_Tipo_Documento) . ' ELECTRÓNICA '  . ' ' . $arrData[0]->ID_Serie_Documento . '-' . $arrData[0]->ID_Numero_Documento;
			$data["Fe_Emision"] 	= ToDateBD($arrData[0]->Fe_Emision);
			$data["No_Signo"]		= $arrData[0]->No_Signo;
			$data["Ss_Total"]		= $arrData[0]->Ss_Total;
			
			$data["No_Entidad"] = $arrData[0]->No_Entidad;
			
			$data["No_Empresa"] 					= $this->empresa->No_Empresa;
			$data["Nu_Documento_Identidad_Empresa"] = $this->empresa->Nu_Documento_Identidad;
			
			$data["url_comprobante"] = (!empty($arrData[0]->Txt_Url_Comprobante) ? $arrData[0]->Txt_Url_Comprobante : '');
			
			$asunto = 'COPIA DE ' . $data["No_Documento"] . ' ' . $this->empresa->No_Empresa . ' | ' . $this->empresa->Nu_Documento_Identidad;
			
			$message = $this->load->view('correos/documentos_electronicos', $data, true);
			
			$this->email->from('noreply@laesystems.com', $this->empresa->No_Empresa);//de
			
			if ( !isset($_POST['ID']) )
				$this->email->to($Txt_Email_Entidad);//para
			else
				$this->email->to($this->input->post('Txt_Email'));//para
				
			$this->email->subject($asunto);
			$this->email->message($message);
			if (!empty($arrData[0]->Txt_Url_PDF))
				$this->email->attach($arrData[0]->Txt_Url_PDF);
			if (!empty($arrData[0]->Txt_Url_XML))
				$this->email->attach($arrData[0]->Txt_Url_XML);
			if (!empty($arrData[0]->Txt_Url_CDR))
				$this->email->attach($arrData[0]->Txt_Url_CDR);
			$this->email->set_newline("\r\n");

			$isSend = $this->email->send();
			
			if($isSend) {
				$peticion = array(
					'status' => 'success',
					'style_modal' => 'modal-success',
					'message' => 'Correo enviado',
				);
			} else {
				$peticion = array(
					'status' => 'error',
					'style_modal' => 'modal-danger',
					'message' => 'No se pudo enviar el correo, inténtelo más tarde.',
					'sMessageErrorEmail' => $this->email->print_debugger(),
				);
			}// if - else envio email
			if (isset($_POST['ID']))
				echo json_encode($peticion);
		} else {
			echo json_encode($arrData);
		}// if - else arrdata modal get documento
	}

	public function generarRepresentacionInternaPDF($ID){
		$arrData = $this->VentaModel->get_by_id($ID);
		if ( $arrData['sStatus'] == 'success' ) {
			$arrData = $arrData['arrData'];
			
			$this->load->library('EnLetras', 'el');
			$EnLetras = new EnLetras();
			
			$this->load->library('Pdf');
			
			$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
			
			ob_start();
			$file = $this->load->view('Ventas/pdf/RepresentacionInternaView', array(
				'arrData' => $arrData,
				'totalEnLetras'	=> $EnLetras->ValorEnLetras($arrData[0]->Ss_Total, $arrData[0]->No_Moneda),
			));
			$html = ob_get_contents();
			ob_end_clean();
			
			$pdf->SetAuthor('LAE');
			$pdf->SetTitle('LAE - Representacion_Interna_' . $arrData[0]->ID_Tipo_Documento . '_' . $arrData[0]->ID_Serie_Documento . '_' . $arrData[0]->ID_Numero_Documento);
		
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			
			$pdf->AddPage();
			
			$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
			if ( !file_exists($this->file_path . $sNombreLogo) ) {
				$sNombreLogo='lae_logo_cotizacion.png';
			}
			$sCssFontFamily='Arial';
			$format_header = '<table>';
				$format_header .= '<tr>';
					$format_header .= '<td rowspan="3" style="width: 60%; text-align: left;">';
						$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
						$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->No_Empresa . '</label><br>';
						$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->Txt_Direccion_Empresa . '</label><br>';
					$format_header .= '</td>';
					$format_header .= '<td style="width: 40%; text-align: center;">';
						$format_header .= '<p style="border: 0.5px solid #ffffff; border-radius: 50%; background-color:#F2F5F5;"><br>';
							$format_header .= '<label style="color: #2a2a2a ; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">RUC ' . $this->empresa->Nu_Documento_Identidad . '</label><br>';
							$format_header .= '<label style="color: #2a2a2a; font-weight:bold; font-size: 18px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . strtoupper($arrData[0]->No_Tipo_Documento) . ' ELECTRÓNICA </label><br>';
							$format_header .= '<label style="color: #2a2a2a; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $arrData[0]->ID_Serie_Documento . '-' . autocompletarConCeros('', $arrData[0]->ID_Numero_Documento, $arrData[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT) . '</label><br>';
						$format_header .= '</p>';
					$format_header .= '</td>';
				$format_header .= '</tr>';
				$format_header .= '<tr><td rowspan="4" style="width: 60%; text-align: left;">&nbsp;</td></tr>';
			$format_header .= '</table>';
			
			$pdf->writeHTML($format_header, true, 0, true, 0);
			
			$pdf->setFont('helvetica', '', 7);
			$pdf->writeHTML($html, true, false, true, false, '');
			
			$file_name = 'LAE - Representacion_Interna_' . $arrData[0]->ID_Tipo_Documento . '_' . $arrData[0]->ID_Serie_Documento . '_' . $arrData[0]->ID_Numero_Documento . '.pdf';
			$pdf->Output($file_name, 'I');
		} else {
			exit();
		}
	}
    
	public function obtenerEstadoPSE($ID_Documento_Cabecera){
		$arrDataResponse = $this->VentaModel->obtenerEstadoPSE($ID_Documento_Cabecera);
		if ( $arrDataResponse['sStatus'] == 'success' ) {
			$arrDataResponse = $arrDataResponse['arrData'];
			
			if ($arrDataResponse[0]->Nu_Estado==9)
				$this->consultaEstadoPse($arrDataResponse, $ID_Documento_Cabecera);
				
			if ($arrDataResponse[0]->Nu_Estado==11)
				$this->consultaEstadoAnuladoPse($arrDataResponse, $ID_Documento_Cabecera);
		} else {
			echo json_encode($arrDataResponse);
		}
  	}

	public function consultaEstadoPse($arrDataResponse, $ID_Documento_Cabecera){
		$ID_Tipo_Documento = '2';//Boleta Nubefact
		if ( $arrDataResponse[0]->ID_Tipo_Documento=='3' )//Factura
			$ID_Tipo_Documento = '1';//Factura
		else if ( $arrDataResponse[0]->ID_Tipo_Documento=='5' )//N/Crédito
			$ID_Tipo_Documento = '3';//N/Crédito
		else if ( $arrDataResponse[0]->ID_Tipo_Documento=='6' )//N/Débito
			$ID_Tipo_Documento = '4';//N/Débito

		$arrParams = array(
			'ruta' => $arrDataResponse[0]->Txt_FE_Ruta,
			'sToken' => $arrDataResponse[0]->Txt_FE_Token,
			'ID_Documento_Cabecera' => $ID_Documento_Cabecera,
			'sTipoFormatoArchivo' => 'CDR',
			'arrData' => array(
				'operacion' => 'consultar_comprobante',
				'tipo_de_comprobante' => $ID_Tipo_Documento,
				'serie' => $arrDataResponse[0]->ID_Serie_Documento,
				'numero' => $arrDataResponse[0]->ID_Numero_Documento,
			),
		);
				
		$arrData = $arrParams['arrData'];
		$data_json = json_encode($arrData);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $arrParams['ruta']);
		curl_setopt(
			$ch, CURLOPT_HTTPHEADER, array(
				'Authorization: Token token="'.$arrParams['sToken'].'"',
				'Content-Type: application/json',
			)
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$respuesta = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
	
		$accepted_response = array( 200, 301, 302 );
		if( in_array( $httpcode, $accepted_response ) ) {
			$leer_respuesta = json_decode($respuesta, true);
			if (isset($leer_respuesta['errors'])) {
				echo json_encode(
					array(
						'status' => 'warning',
						'style_modal' => 'modal-warning',
						'message' => $leer_respuesta['errors'],
					)
				);
			} else {
				if ($leer_respuesta['aceptada_por_sunat'] == true || $leer_respuesta['aceptada_por_sunat'] =='1') {
					if ( $arrParams['sTipoFormatoArchivo'] == 'CDR' ) {
						$arrParams = array(
							'ID_Documento_Cabecera' => $arrParams['ID_Documento_Cabecera'],
							'Nu_Estado' => 8,
							'enlace_del_cdr' => $leer_respuesta['enlace_del_cdr'],
							'codigo_sunat' => $leer_respuesta['sunat_responsecode'],
							'message_sunat' => $leer_respuesta['sunat_description'],
						);
						echo json_encode($this->VentaModel->updDocumentoVenta($arrParams));
					} else {
						$arrParams = array(
							'ID_Documento_Cabecera' => $arrParams['ID_Documento_Cabecera'],
							'enlace_del_xml' => $leer_respuesta['enlace_del_xml'],
						);
						echo json_encode($this->VentaModel->updDocumentoVentaNubefactXML($arrParams));
					}
				} else {
					if ( $arrParams['sTipoFormatoArchivo'] == 'CDR' ) {
						$arrParams = array(
							'ID_Documento_Cabecera' => $arrParams['ID_Documento_Cabecera'],
							'Nu_Estado' => 9,
							'enlace_del_cdr' => '',
							'codigo_sunat' => $leer_respuesta['sunat_responsecode'],
							'message_sunat' => $leer_respuesta['sunat_description'],
						);
						echo json_encode($this->VentaModel->updDocumentoVenta($arrParams));
					}
				}
			}
		} else {
			$leer_respuesta = json_decode($respuesta, true);
			echo json_encode(
				array(
					'status' => 'warning',
					'style_modal' => 'modal-warning',
					'message' => 'Error de httpcode NubeFact Reseller ' . $leer_respuesta['errors'] . ' ' . $httpcode,
				)
			);
		}
	}

	public function consultaEstadoAnuladoPse($arrDataResponse, $ID_Documento_Cabecera){
		$ID_Tipo_Documento = '2';//Boleta Nubefact
		if ( $arrDataResponse[0]->ID_Tipo_Documento=='3' )//Factura
			$ID_Tipo_Documento = '1';//Factura
		else if ( $arrDataResponse[0]->ID_Tipo_Documento=='5' )//N/Crédito
			$ID_Tipo_Documento = '3';//N/Crédito
		else if ( $arrDataResponse[0]->ID_Tipo_Documento=='6' )//N/Débito
			$ID_Tipo_Documento = '4';//N/Débito

		$arrParams = array(
			'ruta' => $arrDataResponse[0]->Txt_FE_Ruta,
			'sToken' => $arrDataResponse[0]->Txt_FE_Token,
			'ID_Documento_Cabecera' => $ID_Documento_Cabecera,
			'arrData' => array(
				'operacion' => 'consultar_comprobante',
				'tipo_de_comprobante' => $ID_Tipo_Documento,
				'serie' => $arrDataResponse[0]->ID_Serie_Documento,
				'numero' => $arrDataResponse[0]->ID_Numero_Documento,
			),
		);
				
		$arrData = $arrParams['arrData'];
		$data_json = json_encode($arrData);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $arrParams['ruta']);
		curl_setopt(
			$ch, CURLOPT_HTTPHEADER, array(
				'Authorization: Token token="'.$arrParams['sToken'].'"',
				'Content-Type: application/json',
			)
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$respuesta  = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
	
		$accepted_response = array( 200, 301, 302 );
		if( in_array( $httpcode, $accepted_response ) ) {
			$leer_respuesta = json_decode($respuesta, true);
			if ( $leer_respuesta['aceptada_por_sunat'] == true ) {
				//echo "PASO 2: Verificando que documento fue anulado en Nubefact PSE" . "<br>";				
				$data = array(
					"operacion"	=> "consultar_anulacion",
					'tipo_de_comprobante' => $ID_Tipo_Documento,
					'serie' => $arrDataResponse[0]->ID_Serie_Documento,
					'numero' => $arrDataResponse[0]->ID_Numero_Documento,
				);
				$data_json = json_encode($data);

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $arrParams['ruta']);
				curl_setopt(
					$ch, CURLOPT_HTTPHEADER, array(
						'Authorization: Token token="'.$arrParams['sToken'].'"',
						'Content-Type: application/json',
					)
				);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$respuesta = curl_exec($ch);
				$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE );
				curl_close($ch);
				$accepted_response = array( 200, 301, 302 );
				if( in_array( $httpcode, $accepted_response ) ) {
					$leer_respuesta = json_decode($respuesta, true);
					if( !empty($leer_respuesta['sunat_ticket_numero'])){
						//echo "PASO 3: El documento fue anulado en nubefact PSE, cambiar estado en laesystems" . "<br>";						
						$arrJsonResponseSunat = json_encode(array(
							'Proveedor' => 'Nubefact PSE Api',
							'Enviada_SUNAT' => 'Si',
							'Aceptada_SUNAT' => 'Si',
							'Codigo_SUNAT' => $leer_respuesta['sunat_responsecode'],
							'Mensaje_SUNAT' => utf8_decode(quitarCaracteresEspeciales($leer_respuesta['sunat_description'])) . ' (consultando anulacion cron)',
							'Fecha_Registro' => dateNow('fecha_hora'),
							'Fecha_Envio' => dateNow('fecha_hora'),
						));
						
						$arrDataUPD = array(
							'ID_Documento_Cabecera' => $arrParams->ID_Documento_Cabecera,
							'enlace_del_cdr' => $leer_respuesta['enlace_del_cdr'],
							'Txt_Respuesta_Sunat_FE' => $arrJsonResponseSunat,
							'Nu_Estado' => 10,
						);						
						echo json_encode($this->VentaModel->updDocumentoVentaCDR($arrDataUPD));
						exit();
					} else {
						echo json_encode(
							array(
								'status' => 'warning',
								'style_modal' => 'modal-warning',
								'message' => 'PASO 3: Respuesta ' . $leer_respuesta['sunat_description'] . ' ' . $leer_respuesta['sunat_soap_error'] . ' ' . $httpcode,
							)
						);
						exit();
					}
				} else {
					echo json_encode(
						array(
							'status' => 'warning',
							'style_modal' => 'modal-warning',
							'message' => 'PASO 2: Respuesta ' . $respuesta . $httpcode,
						)
					);
					exit();
				}
			} else {
				echo json_encode(
					array(
						'status' => 'warning',
						'style_modal' => 'modal-warning',
						'message' => 'PASO 3: Respuesta ' . $leer_respuesta['sunat_description'] . ' ' . $leer_respuesta['sunat_soap_error'] . ' ' . $httpcode,
					)
				);
				exit();
			}
		} else {
			echo json_encode(
				array(
					'status' => 'warning',
					'style_modal' => 'modal-warning',
					'message' => 'PASO 1: ' . $respuesta . $httpcode,
				)
			);
			exit();
		}
	}
}