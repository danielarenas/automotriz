<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class VehiculoController extends CI_Controller {
		
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Ventas/ReglasVenta/VehiculoModel');
	}
	
	public function importarExcelProductos(){
		if (isset($_FILES['excel-archivo_producto']['name']) && isset($_FILES['excel-archivo_producto']['type']) && isset($_FILES['excel-archivo_producto']['tmp_name'])) {
		    $archivo = $_FILES['excel-archivo_producto']['name'];
		    $tipo = $_FILES['excel-archivo_producto']['type'];
		    $destino = "bak_" . $archivo;
		    
		    if (copy($_FILES['excel-archivo_producto']['tmp_name'], $destino)) {
		        if (file_exists($destino)) {
					$this->load->library('Excel');
		    		$objReader = new PHPExcel_Reader_Excel2007();
		    		$objPHPExcel = $objReader->load($destino);
		            $objPHPExcel->setActiveSheetIndex(0);
		            
		            $iCantidadRegistros = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
		            
		            $column = array(
		                'GRUPO_PRODUCTO' 		=> 'A',
		                'TIPO_PRODUCTO'			=> 'B',
		                'UBICACION_INVENTARIO'	=> 'C',
		                'CODIGO_BARRA'			=> 'D',
		                'CODIGO_PRODUCTO'		=> 'E',
		                'NOMBRE'				=> 'F',
		                'GRUPO_IMPUESTO'		=> 'G',
		                'UNIDAD_MEDIDA'			=> 'H',
		                'MARCA'					=> 'I',
		                'CATEGORIA'				=> 'J',
		                'SUB_CATEGORIA'			=> 'K',
		                'CODIGO_PRODUCTO_SUNAT'	=> 'L',
						'CANTIDAD_CO2_PRODUCTO'	=> 'M',
						'PRECIO' => 'N',
						'COSTO'	=> 'O',
						'STOCK_MINIMO' => 'P',
						'TIPO_LAVADO' => 'Q',
						'ESTADO' => 'R',
						'PRECIO_ECOMMERCE_ONLINE_REGULAR' => 'S',
						'PRECIO_ECOMMERCE_ONLINE' => 'T',
						'CATEGORIA_MARKETPLACE' => 'U',
						'SUB_CATEGORIA_MARKETPLACE' => 'V',
						'MARCA_MARKETPLACE' => 'W',
		            );
		            
	                $arrProducto = array();
	                $iCantidadNoProcesados = 0;
                	for ($i = 2; $i <= $iCantidadRegistros; $i++) {
	                	$iID_Grupo_Producto = $objPHPExcel->getActiveSheet()->getCell($column['GRUPO_PRODUCTO'] . $i)->getCalculatedValue();
	                	$iID_Grupo_Producto = filter_var(trim($iID_Grupo_Producto));
	                	
	                	$ID_Tipo_Producto = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['TIPO_PRODUCTO'] . $i)->getCalculatedValue()));
	                	
	                	$No_Ubicacion_Inventario = $objPHPExcel->getActiveSheet()->getCell($column['UBICACION_INVENTARIO'] . $i)->getCalculatedValue();
	                	$No_Ubicacion_Inventario = ucfirst(filter_var(trim($No_Ubicacion_Inventario)));
	                	
	                	$Nu_Codigo_Barra = $objPHPExcel->getActiveSheet()->getCell($column['CODIGO_BARRA'] . $i)->getCalculatedValue();
	                	$Nu_Codigo_Barra = strtoupper(filter_var(trim($Nu_Codigo_Barra)));
	                	
	                	$Nu_Codigo_Producto = $objPHPExcel->getActiveSheet()->getCell($column['CODIGO_PRODUCTO'] . $i)->getCalculatedValue();
	                	$Nu_Codigo_Producto = strtoupper(filter_var(trim($Nu_Codigo_Producto)));
						
	                	$No_Producto = nl2br(trim($objPHPExcel->getActiveSheet()->getCell($column['NOMBRE'] . $i)->getCalculatedValue()));
                        $No_Producto = quitarCaracteresEspeciales($No_Producto);
                        
	                	$No_Impuesto = $objPHPExcel->getActiveSheet()->getCell($column['GRUPO_IMPUESTO'] . $i)->getCalculatedValue();
	                	$No_Impuesto = filter_var(trim($No_Impuesto));
	                	
	                	$No_Unidad_Medida = $objPHPExcel->getActiveSheet()->getCell($column['UNIDAD_MEDIDA'] . $i)->getCalculatedValue();
	                	$No_Unidad_Medida = filter_var(trim($No_Unidad_Medida));
	                	
	                	$No_Marca = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['MARCA'] . $i)->getCalculatedValue()));
	                	$No_Marca = quitarCaracteresEspeciales($No_Marca);
	                	
	                	$No_Familia = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['CATEGORIA'] . $i)->getCalculatedValue()));
	                	$No_Familia = quitarCaracteresEspeciales($No_Familia);
	                	
	                	$No_Sub_Familia = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['SUB_CATEGORIA'] . $i)->getCalculatedValue()));
	                	$No_Sub_Familia = quitarCaracteresEspeciales($No_Sub_Familia);
	                	
	                	$Nu_Codigo_Producto_Sunat = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['CODIGO_PRODUCTO_SUNAT'] . $i)->getCalculatedValue()));
	                	$Nu_Codigo_Producto_Sunat = quitarCaracteresEspeciales($Nu_Codigo_Producto_Sunat);
	                	
	                	$Qt_CO2_Producto = $objPHPExcel->getActiveSheet()->getCell($column['CANTIDAD_CO2_PRODUCTO'] . $i)->getCalculatedValue();
						$Qt_CO2_Producto = strtoupper(filter_var(trim($Qt_CO2_Producto)));
						
	                	$fPrecio = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['PRECIO'] . $i)->getCalculatedValue()));
						$fPrecio = quitarCaracteresEspeciales($fPrecio);
						
	                	$fCosto = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['COSTO'] . $i)->getCalculatedValue()));
						$fCosto = quitarCaracteresEspeciales($fCosto);
						
	                	$iStockMinimo = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['STOCK_MINIMO'] . $i)->getCalculatedValue()));
						$iStockMinimo = quitarCaracteresEspeciales($iStockMinimo);
						
	                	$iTipoLavado = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['TIPO_LAVADO'] . $i)->getCalculatedValue()));
						$iTipoLavado = quitarCaracteresEspeciales($iTipoLavado);
						
	                	$iEstado = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['ESTADO'] . $i)->getCalculatedValue()));
						$iEstado = quitarCaracteresEspeciales($iEstado);
						
	                	$Ss_Precio_Ecommerce_Online_Regular = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['PRECIO_ECOMMERCE_ONLINE_REGULAR'] . $i)->getCalculatedValue()));
	                	$Ss_Precio_Ecommerce_Online_Regular = quitarCaracteresEspeciales($Ss_Precio_Ecommerce_Online_Regular);
	                	
	                	$Ss_Precio_Ecommerce_Online = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['PRECIO_ECOMMERCE_ONLINE'] . $i)->getCalculatedValue()));
	                	$Ss_Precio_Ecommerce_Online = quitarCaracteresEspeciales($Ss_Precio_Ecommerce_Online);
	                	
	                	$No_Familia_Marketplace = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['CATEGORIA_MARKETPLACE'] . $i)->getCalculatedValue()));
	                	$No_Familia_Marketplace = quitarCaracteresEspeciales($No_Familia_Marketplace);
	                	
	                	$No_Sub_Familia_Marketplace = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['SUB_CATEGORIA_MARKETPLACE'] . $i)->getCalculatedValue()));
	                	$No_Sub_Familia_Marketplace = quitarCaracteresEspeciales($No_Sub_Familia_Marketplace);
	                	
	                	$No_Marca_Marketplace = filter_var(trim($objPHPExcel->getActiveSheet()->getCell($column['MARCA_MARKETPLACE'] . $i)->getCalculatedValue()));
	                	$No_Marca_Marketplace = quitarCaracteresEspeciales($No_Marca_Marketplace);
	                	
	                	if ( 
							($iID_Grupo_Producto == 0 || $iID_Grupo_Producto == 1 || $iID_Grupo_Producto == 2)
							&& !empty($Nu_Codigo_Barra)
							&& !empty($No_Producto)
							&& !empty($No_Impuesto)
							&& !empty($No_Unidad_Medida)
							&& !empty($fPrecio)
						) {
		                	$arrProducto[] = array(
								'Nu_Tipo_Producto' => $iID_Grupo_Producto,
								'ID_Tipo_Producto' => $ID_Tipo_Producto,
								'No_Ubicacion_Inventario' => $No_Ubicacion_Inventario,
								'Nu_Codigo_Barra' => $Nu_Codigo_Barra,
								'Nu_Codigo_Producto' => $Nu_Codigo_Producto,
								'No_Producto' => $No_Producto,
								'No_Impuesto' => $No_Impuesto,
								'No_Marca' => $No_Marca,
								'No_Unidad_Medida' => $No_Unidad_Medida,
								'No_Familia' => $No_Familia,
								'No_Sub_Familia' => $No_Sub_Familia,
								'Nu_Codigo_Producto_Sunat' => $Nu_Codigo_Producto_Sunat,
								'Qt_CO2_Producto' => $Qt_CO2_Producto,
								'fPrecio' => $fPrecio,
								'fCosto' => $fCosto,
								'iStockMinimo' => $iStockMinimo,
								'iTipoLavado' => $iTipoLavado,
								'iEstado' => $iEstado,
								'Ss_Precio_Ecommerce_Online_Regular' => $Ss_Precio_Ecommerce_Online_Regular,
								'Ss_Precio_Ecommerce_Online' => $Ss_Precio_Ecommerce_Online,
								'No_Familia_Marketplace' => $No_Familia_Marketplace,
								'No_Sub_Familia_Marketplace' => $No_Sub_Familia_Marketplace,
								'No_Marca_Marketplace' => $No_Marca_Marketplace,
		                	);
	                	} else {
                        	$iCantidadNoProcesados++;
                        }
                	}
                	
                	$bResponse=false;
                	if ( count($arrProducto) > 0 ) {
		                $this->VehiculoModel->setBatchImport($arrProducto);
		                $bResponse = $this->VehiculoModel->importData();
                	} else {
	            		unlink($destino);
	                	unset($arrProducto);
                	
                		$sStatus = 'error-sindatos';
						redirect('Ventas/ReglasVenta/VehiculoController/listar/' . $sStatus);
                		exit();
                	}
                	
            		unlink($destino);
                	unset($arrProducto);
                	
                	if ($bResponse){
                		$sStatus = 'success';
						redirect('Ventas/ReglasVenta/VehiculoController/listar/' . $sStatus . '/' . $iCantidadNoProcesados);
                	} else {
                		$sStatus = 'error-bd';
						redirect('Ventas/ReglasVenta/VehiculoController/listar/' . $sStatus);
                	}
		        } else {
        	        $sStatus = 'error-archivo_no_existe';
					redirect('Ventas/ReglasVenta/VehiculoController/listar/' . $sStatus);
		        }
		    } else {
		        $sStatus = 'error-copiar_archivo';
				redirect('Ventas/ReglasVenta/VehiculoController/listar/' . $sStatus);
		    }
		}
	}

	public function listar($sStatus='', $iCantidadNoProcesados=''){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/ReglasVenta/VehiculoView', array('sStatus' => $sStatus, 'iCantidadNoProcesados' => $iCantidadNoProcesados));
			$this->load->view('footer', array("js_vehiculo" => true));
		}
	}
	
	public function ajax_list(){
		$arrData = $this->VehiculoModel->get_datatables();
        $data = array();
		// Datatables Variables
		$draw = intval($this->input->get("draw"));
		$no = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$action = 'delete';
		
        foreach ($arrData as $row) {
        	settype($row->Qt_Producto, "double");
            $no++;
            $rows = array();
            $rows[] = $row->No_Entidad;
            $rows[] = $row->No_Placa_Vehiculo;
            $rows[] = $row->No_Year_Vehiculo;
            $rows[] = $row->No_Marca_Vehiculo;
            $rows[] = $row->No_Modelo_Vehiculo;
            $rows[] = $row->No_Tipo_Combustible_Vehiculo;
            $rows[] = $row->No_Vin_Vehiculo;
            $rows[] = $row->No_Motor;
            $rows[] = $row->Nu_Kilometraje;
            $rows[] = $row->No_Color_Vehiculo;
            $rows[] = $row->Qt_Producto;
            $rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verProducto(\'' . $row->ID_Producto . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			//$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarProducto(\'' . $row->ID_Producto . '\', \'' . $action . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->VehiculoModel->count_all(),
	        'recordsFiltered' => $this->VehiculoModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
	
	public function ajax_edit($ID){
        echo json_encode($this->VehiculoModel->get_by_id($this->security->xss_clean($ID)));
    }
    
	public function crudProducto(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');
		$data = array(
			'ID_Empresa' => $this->user->ID_Empresa,
			'Nu_Tipo_Producto' => 3,
			'ID_Tipo_Producto' => 2,
			'ID_Entidad' => $this->security->xss_clean($_POST['AID']),
			'No_Producto' => $this->security->xss_clean($_POST['No_Placa_Vehiculo']),
			'No_Placa_Vehiculo' => $this->security->xss_clean($_POST['No_Placa_Vehiculo']),
			'No_Year_Vehiculo' => $this->security->xss_clean($_POST['No_Year_Vehiculo']),
			'No_Marca_Vehiculo' => $this->security->xss_clean($_POST['No_Marca_Vehiculo']),
			'No_Modelo_Vehiculo' => $this->security->xss_clean($_POST['No_Modelo_Vehiculo']),
			'No_Tipo_Combustible_Vehiculo' => $this->security->xss_clean($_POST['No_Tipo_Combustible_Vehiculo']),
			'No_Vin_Vehiculo' => $this->security->xss_clean($_POST['No_Vin_Vehiculo']),
			'No_Motor' => $this->security->xss_clean($_POST['No_Motor']),
			'Nu_Kilometraje' => $this->security->xss_clean($_POST['Nu_Kilometraje']),
			'No_Color_Vehiculo' => $this->security->xss_clean($_POST['No_Color_Vehiculo']),
			'Nu_Estado' => 1,
		);
		echo json_encode(
		( $this->security->xss_clean($_POST['EID_Producto']) != '') ?
			$this->VehiculoModel->actualizarProducto(array('ID_Entidad' => $this->input->post('EID_Entidad'), 'ID_Producto' => $this->input->post('EID_Producto')), $data)
		:
			$this->VehiculoModel->agregarProducto($data)
		);
	}
    
	public function eliminarProducto($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->VehiculoModel->eliminarProducto($this->security->xss_clean($ID)));
	}
}
