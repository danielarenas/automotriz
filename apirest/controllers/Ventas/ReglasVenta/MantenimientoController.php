<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class MantenimientoController extends CI_Controller {

    function __construct(){
    	parent::__construct();
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Ventas/ReglasVenta/MantenimientoModel');
	}

    public function listar($sStatus='', $iCantidadNoProcesados=''){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/ReglasVenta/MantenimientoView', array('sStatus' => $sStatus, 'iCantidadNoProcesados' => $iCantidadNoProcesados));
			$this->load->view('footer', array("js_mantenimiento" => true));
		}
	}

    public function ajax_list(){
		$arrData = $this->MantenimientoModel->get_datatables();
        $data = array();
		// Datatables Variables
		$draw = intval($this->input->get("draw"));
		$no = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
		$action = 'delete';
		
        foreach ($arrData as $row) {
            $no++;
            $rows = array();
            $rows[] = $row->No_Descripcion;
			$rows[] = '<button class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verProducto(\'' . $row->ID_Tabla_Dato . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
			$rows[] = '<button class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarProducto(\'' . $row->Nu_Valor . '\', \'' . $action . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->MantenimientoModel->count_all(),
	        'recordsFiltered' => $this->MantenimientoModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }

    public function ajax_edit($ID){
        echo json_encode($this->MantenimientoModel->get_by_id($this->security->xss_clean($ID)));
    }

	public function crudProducto(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');
		$data = array(
			'ID_Tabla_Dato' => $this->security->xss_clean($_POST['ID_Tabla_Dato']),
			'No_Descripcion' =>  $this->security->xss_clean($_POST['No_Descripcion']),
			'No_Relacion' => 'Tipos_Mantenimiento',
			'No_Class' => 'warning'
		);
		echo json_encode(
		( $this->security->xss_clean($_POST['ID_Tabla_Dato']) != '') ?
			$this->MantenimientoModel->actualizarProducto(array('ID_Tabla_Dato' => $this->input->post('ID_Tabla_Dato')), $data)
		:
			$this->MantenimientoModel->agregarProducto($data)
		);
	}

	public function eliminarProducto($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->MantenimientoModel->eliminarProducto($this->security->xss_clean($ID)));
	}
}