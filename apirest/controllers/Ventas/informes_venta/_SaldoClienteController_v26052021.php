<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class SaldoClienteController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('Ventas/informes_venta/SaldoClienteModel');
		$this->load->model('HelperModel');
	}

	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/informes_venta/SaldoClienteView');
			$this->load->view('footer', array("js_saldo_cliente" => true));
		}
	}	
	
    private function getReporte($arrParams){
        $arrResponseModal = $this->SaldoClienteModel->getReporte($arrParams);
        if ( $arrResponseModal['sStatus']=='success' ) {
            $data = array();
            $sEstadoPago = '';
            $sEstadoPagoClass = '';
            $sAccionVer='ver';
            $sAccionImprimir='imprimir';
            $sVacio='';
            foreach ($arrResponseModal['arrData'] as $row) {
                $rows = array();
                //$rows['Fe_Emision_Hora'] = allTypeDate($row->Fe_Emision_Hora, '-', 0);
                $rows['Fe_Emision'] = ToDateBD($row->Fe_Emision);
                $rows['Fe_Vencimiento'] = ToDateBD($row->Fe_Vencimiento);
                $rows['Fe_Entrega_Factura'] = ToDateBD($row->Fe_Entrega_Factura);
                $dActual=date_create($row->Fe_Emision);
                $dEmision=date_create($row->Fe_Vencimiento);
                $iDiferenciaDias=date_diff($dActual,$dEmision);
                $sMensajeVencimiento = $iDiferenciaDias->format("%a") . ' día(s)';
                if ( dateNow('fecha') > $row->Fe_Vencimiento ) {
                    $dActual=date_create($row->Fe_Emision);
                    $dEmision=date_create(dateNow('fecha'));
                    $iDiferenciaDias=date_diff($dActual,$dEmision);
                    $sMensajeVencimiento = $iDiferenciaDias->format("%a") . ' días vcto.';
                }
                
                $rows['Dias_Vencimiento'] = '<span class="label label-danger">' . $sMensajeVencimiento . '</span>';

                $rows['Dias_Vencimiento_Excel'] = $sMensajeVencimiento;
                $rows['No_Tipo_Documento_Breve'] = $row->No_Tipo_Documento_Breve;
                $rows['ID_Serie_Documento'] = $row->ID_Serie_Documento;
                $rows['ID_Numero_Documento'] = $row->ID_Numero_Documento;
                $rows['No_Entidad'] = $row->No_Entidad;
                $rows['No_Signo'] = $row->No_Signo;
                $rows['Ss_Total'] = ($row->ID_Tipo_Documento != 5 ? $row->Ss_Total : -$row->Ss_Total);
                $rows['Ss_Total_Saldo'] = ($row->ID_Tipo_Documento != 5 ? $row->Ss_Total_Saldo : -$row->Ss_Total_Saldo);
                $sEstadoPago = 'pendiente';
                $sEstadoPagoClass = 'warning';
                if ($row->Ss_Total_Saldo == 0.00) {
                    $sEstadoPago = 'cancelado';
                    $sEstadoPagoClass = 'success';
                }
                $rows['No_Estado_Pago'] = $sEstadoPago;
                $rows['No_Class_Estado_Pago'] = $sEstadoPagoClass;
                $rows['No_Estado'] = $row->No_Estado;
                $rows['No_Class_Estado'] = $row->No_Class_Estado;

                
                $rows['Ss_Saldo_Detraccion'] = $row->Ss_Saldo_Detraccion;

                $btn_pagar_detraccion = '';
                $sDetraccion = '';
                if ($row->Nu_Detraccion == 1) {
                    $btn_pagar_detraccion = '<button type="button" id="btn-pagar_detraccion-' . $row->ID_Documento_Cabecera . '" class="btn btn-xs btn-link" alt="Pagar detracción" title="Pagar detracción" href="javascript:void(0)" onclick="pagarDetraccion(\'' . $row->ID_Documento_Cabecera . '\')">Pagar</button>';
                    $sDetraccion = '<span class="label label-warning">Pendiente Pago</span>' . $btn_pagar_detraccion;
                }

                if ($row->Nu_Estado_Detraccion == 1)
			        $sDetraccion = '<span class="label label-success">Cancelada</span>';

                $rows['sTieneDetraccion'] = ($row->Nu_Detraccion == 0 ? 'No' : 'Si');
                $rows['sDetraccion'] = $sDetraccion;
                
                $rows['Ss_Retencion'] = $row->Ss_Retencion;

                $btn_pagar_retencion = '';
                $sRetencion = '';
                if ($row->Ss_Retencion > 0.00) {
                    $btn_pagar_retencion = '<button type="button" id="btn-pagar_retencion-' . $row->ID_Documento_Cabecera . '" class="btn btn-xs btn-link" alt="Pagar retención" title="Pagar retención" href="javascript:void(0)" onclick="pagarRetencion(\'' . $row->ID_Documento_Cabecera . '\')">Pagar</button>';
                    $sRetencion = '<span class="label label-warning">Pendiente Pago</span>' . $btn_pagar_retencion;
                } else {
			        $sRetencion = '<span class="label label-success">Cancelada</span>';
                }

                $rows['sTieneRetencion'] = ($row->Nu_Retencion == 0 ? 'No' : 'Si');
                $rows['sRetencion'] = $sRetencion;

                $sEstadoVencimiento = '<span class="label label-warning">Pendiente</span>';
                if ( $row->Ss_Total_Saldo > 0.00 && dateNow('fecha') > $row->Fe_Vencimiento )
                    $sEstadoVencimiento = '<span class="label label-danger">Vencida</span>';

                $rows['sEstadoVencimiento'] = $sEstadoVencimiento;
                
                $data[] = (object)$rows;
            }
            return array(
                'sStatus' => 'success',
                'arrData' => $data,
            );
        } else {
            return $arrResponseModal;
        }
    }
    
	public function sendReporte(){
        $arrParams = array(
            'Fe_Inicio' => $this->input->post('Fe_Inicio'),
            'Fe_Fin' => $this->input->post('Fe_Fin'),
            'iIdTipoDocumento' => $this->input->post('iIdTipoDocumento'),
            'iIdSerieDocumento' => $this->input->post('iIdSerieDocumento'),
            'iNumeroDocumento' => $this->input->post('iNumeroDocumento'),
            'iEstadoPago' => $this->input->post('iEstadoPago'),
            'iIdCliente' => $this->input->post('iIdCliente'),
            'sNombreCliente' => $this->input->post('sNombreCliente')
        );
        echo json_encode($this->getReporte($arrParams));
    }
    
	public function sendReportePDF($Fe_Inicio, $Fe_Fin, $iIdTipoDocumento, $iIdSerieDocumento, $iNumeroDocumento, $iEstadoPago, $iIdCliente, $sNombreCliente){
        $this->load->library('FormatoLibroSunatPDF');
		
        $Fe_Inicio = $this->security->xss_clean($Fe_Inicio);
        $Fe_Fin = $this->security->xss_clean($Fe_Fin);
        $iIdTipoDocumento = $this->security->xss_clean($iIdTipoDocumento);
        $iIdSerieDocumento = $this->security->xss_clean($iIdSerieDocumento);
        $iNumeroDocumento = $this->security->xss_clean($iNumeroDocumento);
        $iEstadoPago = $this->security->xss_clean($iEstadoPago);
        $iIdCliente = $this->security->xss_clean($iIdCliente);
        $sNombreCliente = $this->security->xss_clean($sNombreCliente);
        
		$fileNamePDF = "reporte_saldo_cliente_" . $Fe_Inicio . "_" . $Fe_Fin . ".pdf";
        
		$pdf = new FormatoLibroSunatPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $arrCabecera = array (
            "Fe_Inicio" => ToDateBD($Fe_Inicio),
            "Fe_Fin" => ToDateBD($Fe_Fin),
        );
        
        $arrParams = array(
            'Fe_Inicio' => $Fe_Inicio,
            'Fe_Fin' => $Fe_Fin,
            'iIdTipoDocumento' => $iIdTipoDocumento,
            'iIdSerieDocumento' => $iIdSerieDocumento,
            'iNumeroDocumento' => $iNumeroDocumento,
            'iEstadoPago' => $iEstadoPago,
            'iIdCliente' => $iIdCliente,
            'sNombreCliente' => $sNombreCliente,
        );

		ob_start();
		$file = $this->load->view('Ventas/informes_venta/pdf/SaldoClienteViewPDF', array(
			'arrCabecera' => $arrCabecera,
			'arrDetalle' => $this->getReporte($arrParams),
		));
		$html = ob_get_contents();
		ob_end_clean();
        		
		$pdf->SetAuthor('LAE');
		$pdf->SetTitle('LAE - Informes de Saldo de Cliente');
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
        $pdf->setFont('helvetica', '', 5);
        
		$pdf->AddPage('P', 'A4');
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Output($fileNamePDF, 'I');
	}
    
	public function sendReporteEXCEL($Fe_Inicio, $Fe_Fin, $iIdTipoDocumento, $iIdSerieDocumento, $iNumeroDocumento, $iEstadoPago, $iIdCliente, $sNombreCliente){
        $this->load->library('Excel');
		
        $Fe_Inicio = $this->security->xss_clean($Fe_Inicio);
        $Fe_Fin = $this->security->xss_clean($Fe_Fin);
        $iIdTipoDocumento = $this->security->xss_clean($iIdTipoDocumento);
        $iIdSerieDocumento = $this->security->xss_clean($iIdSerieDocumento);
        $iNumeroDocumento = $this->security->xss_clean($iNumeroDocumento);
        $iEstadoPago = $this->security->xss_clean($iEstadoPago);
        $iIdCliente = $this->security->xss_clean($iIdCliente);
        $sNombreCliente = $this->security->xss_clean($sNombreCliente);
        
		$fileNameExcel = "reporte_saldo_cliente_" . $Fe_Inicio . "_" . $Fe_Fin . ".xls";
		
	    $objPHPExcel = new PHPExcel();
	    
	    $objPHPExcel->getActiveSheet()->setTitle('Informe de Saldo de Cliente');
        
	    $hoja_activa = 0;
	    
        $BStyle_top = array(
          'borders' => array(
            'top' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_left = array(
          'borders' => array(
            'left' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_right = array(
          'borders' => array(
            'right' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_bottom = array(
          'borders' => array(
            'bottom' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $style_align_center = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        
        $style_align_right = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );
        
        $style_align_left = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        
	    //Title
	    $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex($hoja_activa)
        ->setCellValue('A1', $this->empresa->No_Empresa)
        ->setCellValue('C2', 'Informe de Saldo de Cliente')
        ->setCellValue('C3', 'Desde: ' . ToDateBD($Fe_Inicio) . ' Hasta: ' . ToDateBD($Fe_Fin));
        
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($style_align_center);
        $objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($style_align_center);
        $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('C2:H2');
        $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('C3:H3');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("15");
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth("10");
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth("12");
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth("12");
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth("40");
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth("15");
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth("20");

        $objPHPExcel->getActiveSheet()->getStyle('A5:M5')->applyFromArray($BStyle_top);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray($BStyle_right);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5:M5')->applyFromArray($BStyle_bottom);

        $objPHPExcel->getActiveSheet()->getStyle('A5:M5')->getFont()->setBold(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5:M5')->applyFromArray($style_align_center);
        
        $objPHPExcel->setActiveSheetIndex($hoja_activa)
        ->setCellValue('A5', 'F. Emisión')
        ->setCellValue('B5', 'F. Vencimiento')
        ->setCellValue('C5', 'Vence')
        ->setCellValue('D5', 'Tipo')
        ->setCellValue('E5', 'Serie')
        ->setCellValue('F5', 'Número')
        ->setCellValue('G5', 'Cliente')
        ->setCellValue('H5', 'M')
        ->setCellValue('I5', 'Total')
        ->setCellValue('J5', 'Total Saldo')
        ->setCellValue('K5', 'Estado Pago')
        ->setCellValue('L5', 'Estado')
        ->setCellValue('M5', 'Estado Vencimiento')
        ;
        
        $objPHPExcel->getActiveSheet()->freezePane('A6');//LINEA HORIZONTAL PARA SEPARAR CABECERA Y DETALLE
        
        $fila = 6;

        $arrParams = array(
            'Fe_Inicio' => $Fe_Inicio,
            'Fe_Fin' => $Fe_Fin,
            'iIdTipoDocumento' => $iIdTipoDocumento,
            'iIdSerieDocumento' => $iIdSerieDocumento,
            'iNumeroDocumento' => $iNumeroDocumento,
            'iEstadoPago' => $iEstadoPago,
            'iIdCliente' => $iIdCliente,
            'sNombreCliente' => $sNombreCliente,
        );
        $arrData = $this->getReporte($arrParams);
        if ( $arrData['sStatus'] == 'success' ) {
            $total_s = 0.00; $total_s_saldo = 0.00; $sum_total_s = 0.00; $sum_total_s_saldo = 0.00;
            foreach($arrData['arrData'] as $row) {                
                $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':' . 'D' . $fila)->applyFromArray($style_align_center);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($style_align_center);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $fila)->applyFromArray($style_align_left);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $fila)->applyFromArray($style_align_center);
                $objPHPExcel->getActiveSheet()->getStyle('I' . $fila . ':' . 'J' . $fila)->applyFromArray($style_align_right);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $fila . ':' . 'L' . $fila)->applyFromArray($style_align_center);

                $objPHPExcel->setActiveSheetIndex($hoja_activa)
                ->setCellValue('A' . $fila, $row->Fe_Emision)
                ->setCellValue('B' . $fila, $row->Fe_Vencimiento)
                ->setCellValue('C' . $fila, $row->Dias_Vencimiento_Excel)
                ->setCellValue('D' . $fila, $row->No_Tipo_Documento_Breve)
                ->setCellValue('E' . $fila, $row->ID_Serie_Documento)
                ->setCellValue('F' . $fila, $row->ID_Numero_Documento)
                ->setCellValue('G' . $fila, $row->No_Entidad)
                ->setCellValue('H' . $fila, $row->No_Signo)
                ->setCellValue('I' . $fila, numberFormat($row->Ss_Total, 2, '.', ','))
                ->setCellValue('J' . $fila, numberFormat($row->Ss_Total_Saldo, 2, '.', ','))
                ->setCellValue('K' . $fila, $row->No_Estado_Pago)
                ->setCellValue('L' . $fila, $row->No_Estado)
                ->setCellValue('M' . $fila, strip_tags($row->sEstadoVencimiento))
                ;
                $fila++;

                $sum_total_s += $row->Ss_Total;
                $sum_total_s_saldo += $row->Ss_Total_Saldo;
            }
            
            $objPHPExcel->setActiveSheetIndex($hoja_activa)
            ->setCellValue('H' . $fila, 'Total')
            ->setCellValue('I' . $fila, numberFormat($sum_total_s, 2, '.', ','))
            ->setCellValue('J' . $fila, numberFormat($sum_total_s_saldo, 2, '.', ','));
            
            $objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'J' . $fila)->applyFromArray($style_align_right);
                        
            $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $fila . ':' . 'J' . $fila)
            ->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'E7E7E7')
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('H' . $fila . ':' . 'J' . $fila)->getFont()->setBold(true);
        } else {
            $objPHPExcel->setActiveSheetIndex($hoja_activa)
            ->setCellValue('E' . $fila, $arrData['sMessage']);

            $objPHPExcel->getActiveSheet()->getStyle('E' . $fila)->applyFromArray($style_align_center);
        }// /. if - else arrData
        
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $fileNameExcel . '"');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
	}
	
	public function pagarDetraccion(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->SaldoClienteModel->pagarDetraccion($this->input->post()));
	}
	
	public function pagarRetencion(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->SaldoClienteModel->pagarRetencion($this->input->post()));
	}
}
