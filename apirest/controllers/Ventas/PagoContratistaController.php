<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PagoContratistaController extends CI_Controller {
	private $file_path = '../assets/images/logos/';
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Ventas/PagoContratistaModel');
	}

	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/PagoContratistaView');
			$this->load->view('footer', array("js_pago_contratista" => true));
		}
	}

	public function ajax_list(){
		$arrData = $this->PagoContratistaModel->get_datatables();
        $data = array();
        $no = $this->input->post('start');
        $action = 'delete';
        foreach ($arrData as $row) {
            $no++;
			$rows = array();
			$rows[] = $row->No_Almacen;
			
			$iIdPresupuesto = '';
			$arrParams = array(
				'iRelacionDatos' => 7,
				'ID_Relacion_Enlace_Tabla' => $row->ID_Documento_Cabecera
			);
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaPagoContratistaToPresupuesto($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$iIdPresupuesto = $row_relacion->ID_Documento_Cabecera;
					
					$orden_ingreso = '';
					$arrParams = array(
						'iRelacionDatos' => 6,
						'iIdOrigenTabla' => $iIdPresupuesto
					);
					$arrResponseRelacion = $this->HelperModel->getRelacionTablaMultiplePresupuestoAndOI($arrParams);
					if ($arrResponseRelacion['sStatus'] == 'success') {
						foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
							$orden_ingreso .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
						}
					}
				}
			}

            $rows[] = ToDateBD($row->Fe_Emision);
			$rows[] = $orden_ingreso;
			$rows[] = $row->No_Tipo_Documento_Breve;
			$rows[] = $row->ID_Serie_Documento;
			$rows[] = $row->ID_Numero_Documento;
			$rows[] = $row->No_Entidad;
			$rows[] = $row->No_Familia;
			$rows[] = $row->Qt_Producto;			
			$rows[] = $row->Ss_Total;
			$rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="PDF" title="PDF" href="javascript:void(0)" onclick="pdfPagoContratista(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>';
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verPagoContratista(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
            $data[] = $rows;
        }
        $output = array(
	        'draw' => $this->input->post('draw'),
	        'recordsTotal' => $this->PagoContratistaModel->count_all(),
	        'recordsFiltered' => $this->PagoContratistaModel->count_filtered(),
	        'data' => $data,
        );
        echo json_encode($output);
    }
    
	public function ajax_edit($ID){
        $data = $this->PagoContratistaModel->get_by_id($this->security->xss_clean($ID));
        $arrImpuesto = $this->HelperModel->getImpuestos($arrPost = '');
        $output = array(
        	'arrEdit' => $data,
        	'arrImpuesto' => $arrImpuesto,
        );
        echo json_encode($output);
    }
    
	public function ajax_edit_modificar($ID){
        $data = $this->PagoContratistaModel->get_by_id_modificar($this->security->xss_clean($ID));
        $arrImpuesto = $this->HelperModel->getImpuestos($arrPost = '');
        $output = array(
        	'arrEdit' => $data,
        	'arrImpuesto' => $arrImpuesto,
        );
        echo json_encode($output);
    }

	public function crudPagoContratista(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');

		$arrCabecera = array(
			'Fe_Emision' => ToDate($this->security->xss_clean($_POST['arrCabecera']['Fe_Emision'])),
			'Txt_Glosa' => $this->security->xss_clean($_POST['arrCabecera']['Txt_Glosa']),
			'Ss_Total' => $this->security->xss_clean($_POST['arrCabecera']['Ss_Total']),
		);
		echo json_encode($this->PagoContratistaModel->actualizarPagoContratista(
				array(
					'ID_Documento_Cabecera' => $this->security->xss_clean($_POST['arrCabecera']['EID_Documento_Cabecera'])
				),
				$arrCabecera,
				$_POST['arrDetalle']
			)
		);
	}

	public function generarLiquidacion(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->PagoContratistaModel->generarLiquidacion($this->input->post()));
	}
 
	// M040 - 2 - INICIO

	public function generarPDF_No2($ID){
        $data = $this->PagoContratistaModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Ventas/pdf/PagoContrastitaPDF_No2', array(
			'arrData' => $data,
			'HelperModel' => $this->HelperModel,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - Pago Contratista Nro. 2 - ' . $data[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-25, PDF_MARGIN_RIGHT-10);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->AddPage();
	
		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.png';
		}
		$sCssFontFamily='Arial';
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td style="width: 60%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
					$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->No_Empresa . '</label><br>';
					$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->Txt_Direccion_Empresa . '</label><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 40%; text-align: center;">';
					$format_header .= '<p style="border: 0.5px solid #ffffff; border-radius: 50%; background-color:#F2F5F5;"><br>';
						$format_header .= '<label style="color: #2a2a2a ; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">RUC ' . $this->empresa->Nu_Documento_Identidad . '</label><br>';
						$format_header .= '<label style="color: #2a2a2a; font-weight:bold; font-size: 18px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . strtoupper($data[0]->No_Tipo_Documento_Breve) . '</label><br>';
						$format_header .= '<label style="color: #2a2a2a; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $data[0]->ID_Serie_Documento . '-' . autocompletarConCeros('', $data[0]->ID_Numero_Documento, $data[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT) . '</label><br>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_pago_contratista_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".pdf";
		$pdf->Output($file_name, 'I');
	}

	// M040 - 2 - FIN



	public function generarPDF($ID){
        $data = $this->PagoContratistaModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Ventas/pdf/PagoContrastitaPDF', array(
			'arrData' => $data,
			'HelperModel' => $this->HelperModel,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - Pago Contratista Nro. ' . $data[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->SetMargins(PDF_MARGIN_LEFT-10, PDF_MARGIN_TOP-25, PDF_MARGIN_RIGHT-10);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->AddPage();
	
		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.png';
		}
		$sCssFontFamily='Arial';
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td style="width: 60%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
					$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->No_Empresa . '</label><br>';
					$format_header .= '<label style="font-size: 10px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $this->empresa->Txt_Direccion_Empresa . '</label><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 40%; text-align: center;">';
					$format_header .= '<p style="border: 0.5px solid #ffffff; border-radius: 50%; background-color:#F2F5F5;"><br>';
						$format_header .= '<label style="color: #2a2a2a ; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">RUC ' . $this->empresa->Nu_Documento_Identidad . '</label><br>';
						$format_header .= '<label style="color: #2a2a2a; font-weight:bold; font-size: 18px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . strtoupper($data[0]->No_Tipo_Documento_Breve) . '</label><br>';
						$format_header .= '<label style="color: #2a2a2a; font-size: 16px; font-family: "' . $sCssFontFamily . '", Times, serif;">' . $data[0]->ID_Serie_Documento . '-' . autocompletarConCeros('', $data[0]->ID_Numero_Documento, $data[0]->Nu_Cantidad_Caracteres, '0', STR_PAD_LEFT) . '</label><br>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_pago_contratista_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".pdf";
		$pdf->Output($file_name, 'I');
	}
}
