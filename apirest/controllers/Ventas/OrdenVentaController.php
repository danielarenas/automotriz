<?php
defined('BASEPATH') || exit('No direct script access allowed');
date_default_timezone_set('America/Lima'); 

class OrdenVentaController extends CI_Controller {  
	private $file_path = './assets/images/logos/';
 
	function __construct(){  
		parent::__construct();
		$this->load->library('session'); 
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('Ventas/OrdenVentaModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
	} 
 
	public function listarOrdenesVenta(){
		if(!$this->MenuModel->verificarAccesoMenu()) {
			redirect('Inicio/InicioView');
		}
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('Ventas/OrdenVentaView');
			$this->load->view('footer', array("js_orden_venta" => true));
			$this->load->view('Ventas/GenerarOCModal.php');
		}
	}

	// M041-2 - I 

	public function getCustomReport($ID_Usuario,  $Fecha_Efectiva  ) {
		
		$ID_Usuario=$this->security->xss_clean($ID_Usuario);
		$Fecha_Efectiva=$this->security->xss_clean($Fecha_Efectiva);
		$data = $this->OrdenVentaModel->get_Reporte_Servicio_A_Cliente( $ID_Usuario,  $Fecha_Efectiva );

		$this->load->library('Excel');

		$style_Alignment = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);

		$Style_Border = array(
			'borders' => array(
			  'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			  )
			)
		  );
		  
		
	$this->excel->setActiveSheetIndex(0);         
    $this->excel->getActiveSheet()->setTitle('Presupuesto');         
    $this->excel->getActiveSheet()->setCellValue('A1', 'Reporte Presupuesto Servicio A Cliente');         
    $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13);     
    $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($style_Alignment);
    //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);         
    $this->excel->getActiveSheet()->mergeCells('A1:Q1');           
	$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
	$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
	$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
	$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(70);
	$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
	$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
	$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
	$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
	$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
	$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
	$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(70);
	$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
	$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
	$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
	$this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
	$this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
	$this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(10);


    $this->excel->getActiveSheet()->setCellValue('A2', 'Tipo'); 
    $this->excel->getActiveSheet()->setCellValue('B2', 'Serie'); 
    $this->excel->getActiveSheet()->setCellValue('C2', 'Número'); 
    $this->excel->getActiveSheet()->setCellValue('D2', 'Fecha Presupuesto'); 
    $this->excel->getActiveSheet()->setCellValue('E2', 'Cliente'); 
    $this->excel->getActiveSheet()->setCellValue('F2', 'Placa'); 
    $this->excel->getActiveSheet()->setCellValue('G2', 'Marca'); 
    $this->excel->getActiveSheet()->setCellValue('H2', 'Modelo'); 
    $this->excel->getActiveSheet()->setCellValue('I2', 'Año Vehiculo'); 
    $this->excel->getActiveSheet()->setCellValue('J2', 'Codigo Barra'); 
    $this->excel->getActiveSheet()->setCellValue('K2', 'Categoria'); 
    $this->excel->getActiveSheet()->setCellValue('L2', 'Productos'); 
    $this->excel->getActiveSheet()->setCellValue('M2', 'Cant'); 
    $this->excel->getActiveSheet()->setCellValue('N2', 'Unid'); 
    $this->excel->getActiveSheet()->setCellValue('O2', 'Precio Unit (sin IGV)'); 
    $this->excel->getActiveSheet()->setCellValue('P2', 'Precio (sin IGV)'); 
    $this->excel->getActiveSheet()->setCellValue('Q2', 'Costo Unit  (sin IGV)'); 
    $this->excel->getActiveSheet()->setCellValue('R2', 'Costo (sin IGV)'); 
    $this->excel->getActiveSheet()->setCellValue('S2', 'Margen'); 
    $this->excel->getActiveSheet()->setCellValue('T2', 'Margen %'); 


	$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($Style_Border);
	$this->excel->getActiveSheet()->getStyle('A2:T2')->applyFromArray($Style_Border);
	unset($styleArray);

	$c=2;


	foreach($data as $item) {
		{
			
			$c++;




			$this->excel->getActiveSheet()->setCellValue('A'.$c, $item->No_Tipo_Documento); 
			$this->excel->getActiveSheet()->setCellValue('B'.$c, $item->ID_Serie_Documento); 
			$this->excel->getActiveSheet()->setCellValue('C'.$c, $item->ID_Numero_Documento); 
			$this->excel->getActiveSheet()->setCellValue('D'.$c, $item->Fe_Emision); 
			$this->excel->getActiveSheet()->setCellValue('E'.$c, $item->No_Entidad); 
			$this->excel->getActiveSheet()->setCellValue('F'.$c, $item->No_Placa_Vehiculo); 
			$this->excel->getActiveSheet()->setCellValue('G'.$c, $item->No_Marca_Vehiculo); 
			$this->excel->getActiveSheet()->setCellValue('H'.$c, $item->No_Modelo_Vehiculo); 
			$this->excel->getActiveSheet()->setCellValue('I'.$c, $item->No_Year_Vehiculo); 
			$this->excel->getActiveSheet()->setCellValue('J'.$c, $item->nu_codigo_barra); 
			$this->excel->getActiveSheet()->setCellValue('K'.$c, $item->no_familia); 
			$this->excel->getActiveSheet()->setCellValue('L'.$c, $item->No_Producto); 
			$this->excel->getActiveSheet()->setCellValue('M'.$c, $item->Qt_Producto); 
			$this->excel->getActiveSheet()->setCellValue('N'.$c, $item->No_Unidad_Medida); 
			$this->excel->getActiveSheet()->setCellValue('O'.$c,  $item->Ss_Precio  ); 
			$this->excel->getActiveSheet()->setCellValue('P'.$c,  $item->Ss_Total  ); 
			$this->excel->getActiveSheet()->setCellValue('Q'.$c,  $item->costo_prod  ); 
			$this->excel->getActiveSheet()->setCellValue('R'.$c,  ($item->costo_prod *  $item->Qt_Producto  ) ); 
			$margen = $item->Ss_Total - $item->costo_real_prod;
			$this->excel->getActiveSheet()->setCellValue('S'.$c,  $margen ); 
			$margen_porc = ($margen/$item->Ss_Total)*100   ;
			$this->excel->getActiveSheet()->setCellValue('T'.$c, round( $margen_porc,2)   ); 
		}
	}
    header('Content-Type: application/vnd.ms-excel');         
    header('Content-Disposition: attachment;filename="Reporte_Presupuesto_Servicio_A_Cliente.xls"');
    header('Cache-Control: max-age=0'); //no cache         
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');         
    
    // Forzamos a la descarga         
    $objWriter->save('php://output');
	
	
	$ID_Usuario=$this->security->xss_clean($ID_Usuario);
	$Fecha_Efectiva=$this->security->xss_clean($Fecha_Efectiva);
	$this->OrdenVentaModel->delete_tbl_Reporte_Servicio_A_Cliente( $ID_Usuario,  $Fecha_Efectiva );

	}

	// M041-2 - F 
	

	public function ajax_list(){
		$arrData = $this->OrdenVentaModel->get_datatables();
		$data = [];
		$no = $this->input->post('start');
		$action = 'delete';
		$btn_generar_ot = '';
		foreach ($arrData as $row) {
			$span_enlace_documentos = '';
			$no++;
			$rows = [];

			$ID_Liquidacion = 0;
			$liquidacion = '';
			$arrParams = [
				'iRelacionDatos' => 4,
				'iIdOrigenTabla' => $row->ID_Documento_Cabecera
			];
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoALiquidacion($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$ID_Liquidacion = $row_relacion->ID_Documento_Cabecera;
					$liquidacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento;
				}
			}

			$facturacion = '';
			$facturacion_fecha_presupuesto = '';
			$facturacion_mensaje_dias_transcurrido = '';
			$no_fecha_facturación = false;
			$arrParams = [
				'iRelacionDatos' => 5,
				'iIdOrigenTabla' => $row->ID_Documento_Cabecera
			];
			// C-02 - Inicio 
			$valor_facturacion='';
			$arrResponseRelacion = $this->HelperModel->CustPrep_getRelacionTablaPresupuestoAFactura($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion){
					if($row_relacion->No_Tipo_Documento == 'Nota de Crédito') {
						$facturacion .= '<span class="label label-danger">' . $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . "</span><br>";
					} else {
						if ($valor_facturacion != $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento ) { 
							$valor_facturacion = $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento;
					
							$facturacion = $facturacion . $valor_facturacion . '<br>' ;
						}
												
					}
					$facturacion_fecha_presupuesto = $row_relacion->Fe_Emision;
					$no_fecha_facturación = ($row_relacion->No_Tipo_Documento == 'Nota de Crédito');
				}
			}
			/* $arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion){
					if($row_relacion->No_Tipo_Documento == 'Nota de Crédito') {
						$facturacion .= '<span class="label label-danger">' . $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . "</span><br>";
					} else {
						$facturacion .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
					}
					$facturacion_fecha_presupuesto = $row_relacion->Fe_Emision;
					$no_fecha_facturación = ($row_relacion->No_Tipo_Documento == 'Nota de Crédito');
				}
			} */
			// C-02 - Fin 
			if (empty($facturacion) && !empty($liquidacion)) {
				$facturacion = '';
				$arrParams = array(
					'iRelacionDatos' => 5,
					'iIdOrigenTabla' => $ID_Liquidacion
				);
				$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoAFactura($arrParams);
				if ($arrResponseRelacion['sStatus'] == 'success') {
					foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
						if($row_relacion->No_Tipo_Documento == 'Nota de Crédito') {
							$facturacion .= '<span class="label label-danger">' . $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . "</span><br>";
						} else {
							$facturacion = $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
						}
						$facturacion_fecha_presupuesto = $row_relacion->Fe_Emision;
						$no_fecha_facturación = ($row_relacion->No_Tipo_Documento == 'Nota de Crédito');
					}
				}
			}
				$arrParams = array(
					'iRelacionDatos' => 5,
					'ID_OI_Detalle' => $row->ID_Documento_Cabecera
				);
				$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoAFacturaDetallev2($arrParams);
				if ($arrResponseRelacion['sStatus'] == 'success') {
					foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
						// RELACION DE DOCUMENTOS
						$arrParams = array('ID_Documento_Cabecera' => $row_relacion->ID_Documento_Cabecera);
						$arrResponseDocument = $this->HelperModel->getDocumentoEnlaceDestino($arrParams);
						if ($arrResponseDocument['sStatus'] == 'success') {
							foreach ($arrResponseDocument['arrData'] as $rowEnlace) {
								$span_enlace_documentos = '<span class="label label-danger">' . $rowEnlace->No_Tipo_Documento_Breve . ' - ' . $rowEnlace->_ID_Serie_Documento . ' - '. $rowEnlace->ID_Numero_Documento . "</span><br>";
								// $fTotalNC += $rowEnlace->Ss_Total;
							}
						}
					}
				}

			$checkbox = '';
			$orden_ingreso = '';
			$orden_ingreso_estado = '';
			$orden_ingreso_etapa = '';
			$orden_ingreso_fecha_salida = '';
			$iIdOrdenIngreso = '';
			$arrParams = [
				'iRelacionDatos' => 6,
				'iIdOrigenTabla' => $row->ID_Documento_Cabecera
			];
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaMultiplePresupuestoAndOI($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$orden_ingreso .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
					$iIdOrdenIngreso = $row_relacion->ID_Orden_Ingreso;
					$orden_ingreso_estado .= $row_relacion->No_Descripcion_Estado;
					$orden_ingreso_etapa .= '<span class="label label-' . $row_relacion->No_Class_Etapa . '">' . $row_relacion->No_Etapa . '</span>';
					$orden_ingreso_fecha_salida = $row_relacion->Fe_Entrega_Tentativa;
				}
			}

			$existsVale = false;
			$arrParams = ['iRelacionDatos' => 0, 'iIdOrigenTabla' => $row->ID_Documento_Cabecera];
			$arrResponseRelacion = $this->HelperModel->getRelacionTabla($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				$existsVale = true;
			}

			$checkbox = '<button type="button" class="btn btn-xs btn-link" alt="Desvíncular OI" title="Desvíncular OI" href="javascript:void(0)" onclick="desvincularOrdenIngreso('. $row->ID_Documento_Cabecera . ')"><i class="fa fa-lock" aria-hidden="true"> Desvíncular OI</i></button>';
			$existsOI = true;
			if(empty($orden_ingreso)) {
				$checkbox = "<input type='checkbox' id='" . $row->ID_Documento_Cabecera . "' class='check-iIdDocumentoCabecera' name='arrIdDocumentoCabecera[" . $row->ID_Documento_Cabecera . "]'>";
				$existsOI = false;
			}
			$rows[] = ($row->Nu_Estado == 21) ? '' : (($existsVale && $existsOI) ? '' : $checkbox);

			$checkbox = '';
			if ($row->Nu_Estado == 22 && $row->Ss_Total > 0.00) {
				$checkbox = "<input type='checkbox' id='" . $row->ID_Documento_Cabecera . "' class='check-iIdDocumentoCabecera' name='arrIdDocumentoCabecera[" . $row->ID_Documento_Cabecera . "]'>";
			}

			if (!empty($liquidacion) || !empty($facturacion)) {
				$rows[] = '';
			} else {
				$rows[] = $checkbox;//Liquidacion
			}

			//Pago de contratista
			$checkbox = '';
			//if($row->Nu_Estado == 22 && $row->Nu_Pago_Contratista_Generado == 0) {//Falta validacion para que no muestre si ya tiene liquidacion de pago contratista
			if( $row->Nu_Pago_Contratista_Generado == 0) {//Falta validacion para que no muestre si ya tiene liquidacion de pago contratista
				$checkbox = "<input type='checkbox' id='" . $row->ID_Documento_Cabecera . "' class='check-iIdDocumentoCabecera' name='arrIdDocumentoCabecera[" . $row->ID_Documento_Cabecera . "]'>";
			}
			$rows[] = $checkbox;

			$rows[] = $row->ID_Documento_Cabecera;
			$rows[] = ToDateBD($row->Fe_Emision);
			$rows[] = $row->No_Tipo_Documento;
			$rows[] = $row->ID_Serie_Documento;
			$rows[] = $row->ID_Numero_Documento;
			$rows[] = $row->No_Entidad;
			$rows[] = $row->No_Placa_Vehiculo;
			$rows[] = $row->No_Marca_Vehiculo;
			$rows[] = $row->No_Modelo_Vehiculo;
			$rows[] = $row->No_Signo;
			$rows[] = numberFormat($row->Ss_Total, 2, '.', ',');

			$dropdown = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';
			if ( $row->Nu_Estado != 21 ) {
				$dropdown = '
				<div class="dropdown">
					<button class="btn btn-' . $row->No_Class_Estado . ' dropdown-toggle" type="button" data-toggle="dropdown">' . $row->No_Descripcion_Estado . '
					<span class="caret"></span></button>
					<ul class="dropdown-menu">
						<li><a alt="Revisado" title="Revisado" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 22);">Aprobado</a></li>
						<li><a alt="Aceptado" title="Aceptado" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 23);">No Aprobado</a></li>
						<li><a alt="Aceptado" title="Aceptado" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 26);">Anulado</a></li>
						<li><a alt="Cotización" title="Cotización" href="javascript:void(0)" onclick="estadoOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', 27);">Cotización</a></li>
					</ul>
				</div>';
			}

			$rows[] = $dropdown;
			$rows[] = '<span class="label label-' . $row->No_Class_Estado . '">' . $row->No_Descripcion_Estado . '</span>';//estado presupuesto
			$rows[] = $row->No_Descripcion_Tipo_Mantenimiento;
			$rows[] = $orden_ingreso;
			$rows[] = $orden_ingreso_estado;

			$orden_trabajo = '';
			$arrParams = array(
				'iRelacionDatos' => 2,//Presupuesto a orden de trabajo
				'iIdOrigenTabla' => $row->ID_Documento_Cabecera
			);
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoToOT($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
					$orden_trabajo .= 'OT - ' . $row_relacion->ID_Documento_Cabecera . '<br>';
				}
			}

			$btn_generar_ot = 'Falta agregar item';
			if ($row->Ss_Total>0.00 && empty($orden_trabajo)) {
				$btn_generar_ot = '<button type="button" class="btn btn-xs btn-link" alt="Generar OT" title="Generar OT" href="javascript:void(0)" onclick="generarOT(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-book" aria-hidden="true"> Generar OT</i></button>';
			}
			$rows[] = (!empty($orden_trabajo) ? $orden_trabajo : $btn_generar_ot);
			$rows[] = $liquidacion;//Número de Liquidación

			$pago_contratista = '';
			$arrParams = array(
				'iRelacionDatos' => 7,//Relación Presupuesto a Pago de Contratista
				'iIdOrigenTabla' => $row->ID_Documento_Cabecera
			);
			$arrResponseRelacion = $this->HelperModel->getRelacionTablaPresupuestoToPagoContratista($arrParams);
			if ($arrResponseRelacion['sStatus'] == 'success') {
				foreach ($arrResponseRelacion['arrData'] as $row_relacion){
					$pago_contratista .= $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento . '<br>';
				}
			}

			$rows[] = $pago_contratista;//Pago contratistas
			$rows[] = $facturacion. ' ' . $span_enlace_documentos;//Número de Factura
			$rows[] = $orden_ingreso_etapa ;
			$rows[] = (!empty($orden_ingreso_fecha_salida) && $orden_ingreso_fecha_salida != '0000-00-00' ? ToDateBD($orden_ingreso_fecha_salida) : '');
			$rows[] = (!empty($facturacion_fecha_presupuesto) && !$no_fecha_facturación ? ToDateBD($facturacion_fecha_presupuesto) : '');//fecha emision de Factura

			if ($orden_ingreso_fecha_salida != '0000-00-00' && !empty($facturacion_fecha_presupuesto) && !$no_fecha_facturación ) {
				$dActual=date_create($facturacion_fecha_presupuesto);
				$dEmision=date_create($orden_ingreso_fecha_salida);
				$iDiferenciaDias=date_diff($dActual,$dEmision);

				// M0028 - Inicio
				// $facturacion_mensaje_dias_transcurrido = $iDiferenciaDias->format("%a") . ' día(s) transcurrido';
				$facturacion_mensaje_dias_transcurrido = $iDiferenciaDias->format("%a");
				// M0028 - Fin

				$sSpanClass='success';
				if ($iDiferenciaDias->format("%a")>5 && $iDiferenciaDias->format("%a")<10) {
					$sSpanClass='warning';
				} else if ($iDiferenciaDias->format("%a")>=10) {
					$sSpanClass='danger';
				}

				$facturacion_mensaje_dias_transcurrido = '<span class="label label-' . $sSpanClass . '">' . $facturacion_mensaje_dias_transcurrido . '</span>';	
			} else {
				if ($orden_ingreso_fecha_salida != '0000-00-00') {
					$dActual=date_create(dateNow('fecha'));
					$dEmision=date_create($orden_ingreso_fecha_salida);
					$iDiferenciaDias=date_diff($dActual,$dEmision);
					// M0028 - Inicio
				    // $facturacion_mensaje_dias_transcurrido = $iDiferenciaDias->format("%a") . ' día(s) transcurrido';
					$facturacion_mensaje_dias_transcurrido = $iDiferenciaDias->format("%a");
					// M0028 - Fin
					$sSpanClass='success';
					if ($iDiferenciaDias->format("%a")>5 && $iDiferenciaDias->format("%a")<10) {
						$sSpanClass='warning';
					} else if ($iDiferenciaDias->format("%a")>=10) {
						$sSpanClass='danger';
					}
					$facturacion_mensaje_dias_transcurrido = '<span class="label label-' . $sSpanClass . '">' . $facturacion_mensaje_dias_transcurrido . '</span>';
				}
			}

			$rows[] = $facturacion_mensaje_dias_transcurrido;
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Generar Carta de Compromiso" title="Generar Carta de Compromiso" href="javascript:void(0)" onclick="pdfCartaCompromiso(\'' . $row->ID_Documento_Cabecera . '\', \''.$row->No_Descripcion_Tipo_Mantenimiento.'\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>';
			// Agregando Total OC
			// OC_MODAL_3 - INICIO 

			$arrParams_1 = array(
				'ID_Cabe_Presupuesto' => $row->ID_Documento_Cabecera
			);
			$arrResponseSumaOc = $this->HelperModel->Cust_getSumaTotalOc_En_Presupuesto($arrParams_1);
			
			if ($arrResponseSumaOc['sStatus'] == 'success') {
				 foreach ($arrResponseSumaOc['arrData'] as $row_relacion){
					$SumaOc =  $row_relacion->Ss_Total;
				}
				if ($SumaOc == ''){
					$SumaOc='0.00';
				}
				
			}
			
			// OC_MODAL_7 - INICIO				
			$rows[] = $SumaOc;			
			// $rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Total OC" title="Total OC">' . $SumaOc .  '</button>';
			// OC_MODAL_7 - FIN 
			
			// OC_MODAL_3 - FIN
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Facturar" title="Facturar" href="javascript:void(0)" onclick="addFactura()">Facturar</button>';
			$excelButton = '<button type="button" class="btn btn-xs btn-link" alt="Excel" title="Excel" href="javascript:void(0)" onclick="excelOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-file-excel-o color_icon_excel"></i> Excel</button>';
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="PDF" title="PDF" href="javascript:void(0)" onclick="pdfOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF</button>'.$excelButton;

			$btn_generar_vale = 'Falta agregar item';
			if ($row->Nu_Estado != 3 && $row->Ss_Total>0.00) {
				$btn_generar_vale = '<button type="button" class="btn btn-xs btn-link" alt="Facturar" title="Facturar" href="javascript:void(0)" onclick="facturarOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-book" aria-hidden="true"> Generar Vale</i></button>';
				$arrParams = array(
					'iRelacionDatos' => 0,
					'iIdOrigenTabla' => $row->ID_Documento_Cabecera
				);
				$arrResponseRelacion = $this->HelperModel->getRelacionTabla($arrParams);
				if ($arrResponseRelacion['sStatus'] == 'success') {
					foreach ($arrResponseRelacion['arrData'] as $row_relacion) {
						$btn_generar_vale .= '<br>' . $row_relacion->No_Tipo_Documento . ' - ' . $row_relacion->ID_Serie_Documento . ' - ' . $row_relacion->ID_Numero_Documento;
					}
				}
			}

			if ($row->Nu_Estado == 5 || $row->Nu_Estado == 0 || $row->Nu_Estado == 1 || $row->Nu_Estado == 22 || $row->Nu_Estado == 23) {
				$rows[] = $btn_generar_vale;
				// Agregando Generar OC
				// // OC_EN_PRESUPUESTO - I 

				// OC_MODAL_5-2 - INICIO 
				$arrParams_2 = array(
					'ID_Cabe_Presupuesto' => $row->ID_Documento_Cabecera
				);
				$arrResponsegetIDsOc = $this->HelperModel->Cust_getIDsOc_En_Presupuesto($arrParams_2);
				$Acumulador_Oc='</br>';
				if ($arrResponsegetIDsOc['sStatus'] == 'success') {
					 foreach ($arrResponsegetIDsOc['arrData'] as $row_relacion){
						if($row_relacion->Estado_Documento == '0' ){ 
							$Acumulador_Oc = $Acumulador_Oc. '<span class="label label-danger" style="font-size:1.0em;" >' . $row_relacion->ID_Cabe_OC . '</span>'.'</br>' ;
						} else{
							$Acumulador_Oc = $Acumulador_Oc. $row_relacion->ID_Cabe_OC.'</br>' ;
						}
					}					
				}
				$rows[] =  '<button type="button" id="btn-modal_light" class="btn btn-xs btn-link" data-toggle="modal" data-target="#GenerarOCModal" data-id-cabecera="'.$row->ID_Documento_Cabecera.'"> Generar OC</button>' . $Acumulador_Oc  ;				
				// OC_MODAL_5-2 - FIN 

				// $rows[] =  '<button type="button" class="btn btn-xs btn-link" alt="Generar OC" title="Generar OC" href="javascript:void(0)" onclick="GenOC(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-book" aria-hidden="true"> Generar OC</i></button>';
				// // OC_EN_PRESUPUESTO - F  
				$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Modificar" title="Modificar" href="javascript:void(0)" onclick="verOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $iIdOrdenIngreso . '\')"><i class="fa fa-pencil" aria-hidden="true"> Modificar</i></button>';
				$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Eliminar" title="Eliminar" href="javascript:void(0)" onclick="eliminarOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\', \'' . $row->Nu_Descargar_Inventario . '\', \'' . $action . '\')"><i class="fa fa-trash-o" aria-hidden="true"> Eliminar</i></button>';
			} else {
				$rows[] = $btn_generar_vale;
				$rows[] = '';
				$rows[] = '';
				// 10_08 INICIO 
				$rows[] = '';
				// 10_08 FIN 
			}
			$rows[] = '<button type="button" class="btn btn-xs btn-link" alt="Duplicar" title="Duplicar" href="javascript:void(0)" onclick="duplicarOrdenVenta(\'' . $row->ID_Documento_Cabecera . '\')"><i class="fa fa-clone" aria-hidden="true"> Duplicar</i></button>';
			$data[] = $rows;
		}
		$output = array(
			'draw' => $this->input->post('draw'),
			'recordsTotal' => $this->OrdenVentaModel->count_all(),
			'recordsFiltered' => $this->OrdenVentaModel->count_filtered(),
			'data' => $data,
		);
		echo json_encode($output);
	}

	public function ajax_edit($ID){ 
		$data = $this->OrdenVentaModel->get_by_id($this->security->xss_clean($ID));
		$arrImpuesto = $this->HelperModel->getImpuestos($arrPost = '');
		$output = array(
			'arrEdit' => $data,
			'arrImpuesto' => $arrImpuesto,
		);
		echo json_encode($output);
	}

	public function ajax_actividades($ID){
		$data = $this->OrdenVentaModel->get_by_id($this->security->xss_clean($ID));
		$output = array(
			'arrData' => $data,
		);
		echo json_encode($output);
	}

	public function crudOrdenVenta(){
		if (!$this->input->is_ajax_request()) exit('No se puede Agregar/Editar y acceder');

		$arrClienteNuevo = '';
		if (isset($_POST['arrClienteNuevo'])){
			$arrClienteNuevo = array(
				'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrClienteNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrClienteNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad'					=> $this->security->xss_clean($_POST['arrClienteNuevo']['No_Entidad']),
				'Txt_Direccion_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Txt_Direccion_Entidad']),
				'Nu_Telefono_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Telefono_Entidad']),
				'Nu_Celular_Entidad'			=> $this->security->xss_clean($_POST['arrClienteNuevo']['Nu_Celular_Entidad']),
			);
		}
		
		$arrContactoNuevo = '';
		if (isset($_POST['arrContactoNuevo'])){
			$Nu_Telefono_Contacto = '';
			if ( $_POST['arrContactoNuevo']['Nu_Telefono_Entidad'] && strlen($_POST['arrContactoNuevo']['Nu_Telefono_Entidad']) === 8){
		        $Nu_Telefono_Contacto = explode(' ', $_POST['arrContactoNuevo']['Nu_Telefono_Entidad']);
		        $Nu_Telefono_Contacto = $Nu_Telefono_Contacto[0].$Nu_Telefono_Contacto[1];
			}
			
			$Nu_Celular_Contacto = '';
			if ( $_POST['arrContactoNuevo']['Nu_Celular_Entidad'] && strlen($_POST['arrContactoNuevo']['Nu_Celular_Entidad']) === 11){
		        $Nu_Celular_Contacto = explode(' ', $_POST['arrContactoNuevo']['Nu_Celular_Entidad']);
		        $Nu_Celular_Contacto = $Nu_Celular_Contacto[0].$Nu_Celular_Contacto[1].$Nu_Celular_Contacto[2];
			}
			$arrContactoNuevo = array(
				'ID_Tipo_Documento_Identidad'	=> $this->security->xss_clean($_POST['arrContactoNuevo']['ID_Tipo_Documento_Identidad']),
				'Nu_Documento_Identidad'		=> $this->security->xss_clean(strtoupper($_POST['arrContactoNuevo']['Nu_Documento_Identidad'])),
				'No_Entidad'					=> $this->security->xss_clean($_POST['arrContactoNuevo']['No_Entidad']),
				'Nu_Telefono_Entidad'			=> $Nu_Telefono_Contacto,
				'Nu_Celular_Entidad'			=> $Nu_Celular_Contacto,
				'Txt_Email_Entidad'				=> $this->security->xss_clean($_POST['arrContactoNuevo']['Txt_Email_Entidad']),
			);
		}
		
		$arrVehiculoNuevo = '';
		if (isset($_POST['arrVehiculoNuevo'])){
			$arrVehiculoNuevo = array(
				'No_Placa_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Placa_Vehiculo']),
				'No_Year_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Year_Vehiculo']),
				'No_Marca_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Marca_Vehiculo']),
				'No_Modelo_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Modelo_Vehiculo']),
				'No_Tipo_Combustible_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Tipo_Combustible_Vehiculo']),
				'No_Vin_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Vin_Vehiculo']),
				'No_Motor' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Motor']),
				'No_Color_Vehiculo' => $this->security->xss_clean($_POST['arrVehiculoNuevo']['No_Color_Vehiculo']),
			);
		}
		
		$iDescargarStock = $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Nu_Descargar_Inventario']);
		$arrOrdenVentaCabecera = array(
			'ID_Empresa'					=> $this->empresa->ID_Empresa,
			'ID_Organizacion'				=> $this->empresa->ID_Organizacion,
			'ID_Tipo_Asiento'				=> 1,//Venta
			'ID_Tipo_Documento'	=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Tipo_Documento']),//Proforma
			'ID_Serie_Documento' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Serie_Documento']),//Proforma
			'ID_Serie_Documento_PK'	=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Serie_Documento_PK']),//Proforma
			'ID_Numero_Documento' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Numero_Documento']),//Proforma
			'Nu_Correlativo'				=> 0,
			'Fe_Emision'					=> ToDate($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Fe_Emision'])),
			'Fe_Vencimiento'				=> ToDate($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Fe_Vencimiento'])),
			'Fe_Periodo'					=> ToDate($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Fe_Entrega'])),
			'ID_Moneda'						=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Moneda']),
			'ID_Medio_Pago'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Medio_Pago']),
			'Nu_Descargar_Inventario'		=> $iDescargarStock,
			'ID_Entidad'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Entidad']),
			'ID_Contacto'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Contacto']),
			'Txt_Garantia'					=> nl2br($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Txt_Garantia'])),
			'Txt_Glosa'						=> nl2br($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Txt_Glosa'])),
			'Txt_Resumen_Servicio_Presupuesto' => nl2br($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Txt_Resumen_Servicio_Presupuesto'])),
			'Po_Descuento'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Po_Descuento']),
			'Ss_Descuento'					=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Ss_Descuento']),
			'Ss_Total'						=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Ss_Total']),
			'Nu_Estado'						=> ($this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ENu_Estado']) != '' ? $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ENu_Estado']) : 5),
			'ID_Mesero'	=> $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Mesero']),
			'ID_Comision' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Comision']),
			'No_Formato_PDF' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['No_Formato_PDF']),
			'ID_Producto' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Producto']),
			'Nu_Tipo_Mantenimiento' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['Nu_Tipo_Mantenimiento']),
			'ID_Origen_Tabla' => (isset($_POST['arrOrdenVentaCabecera']['ID_Origen_Tabla']) ? $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Origen_Tabla']) : ''),
		);

		if ( $_POST['arrOrdenVentaCabecera']['ID_Lista_Precio_Cabecera'] != 0 )
			$arrOrdenVentaCabecera = array_merge($arrOrdenVentaCabecera, array("ID_Lista_Precio_Cabecera" => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Lista_Precio_Cabecera'])));

		//if ( $iDescargarStock == 1 ) {
			$arrOrdenVentaCabecera = array_merge($arrOrdenVentaCabecera, array("ID_Almacen" => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['ID_Almacen'])));
		//}

		echo json_encode(
		( $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Empresa']) != '' && $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Documento_Cabecera']) != '') ?
			$this->actualizarVenta_Inventario(array('ID_Empresa' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Empresa']), 'ID_Documento_Cabecera' => $this->security->xss_clean($_POST['arrOrdenVentaCabecera']['EID_Documento_Cabecera'])), $arrOrdenVentaCabecera, $_POST['arrDetalleOrdenVenta'], $arrOrdenVentaCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo)
		:
			$this->agregarVenta_Inventario($arrOrdenVentaCabecera, (isset($_POST['arrDetalleOrdenVenta']) ? $_POST['arrDetalleOrdenVenta'] : ''), $arrOrdenVentaCabecera['Nu_Descargar_Inventario'], $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo)
		);
	}

	public function agregarVenta_Inventario($arrOrdenVentaCabecera = '', $arrDetalleOrdenVenta = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = '', $arrContactoNuevo = '', $arrVehiculoNuevo = ''){
		$responseVenta = $this->OrdenVentaModel->agregarVenta($arrOrdenVentaCabecera, $arrDetalleOrdenVenta, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo);
		if ($responseVenta['status'] == 'success') {
			/*
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrOrdenVentaCabecera['ID_Tipo_Movimiento'] = 1;//Venta
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrOrdenVentaCabecera['ID_Almacen'], $responseVenta['Last_ID_Documento_Cabecera'], 0, $arrDetalleOrdenVenta, $arrOrdenVentaCabecera['ID_Tipo_Movimiento'], 0, '', 1, 1);
			}
			*/
			return $responseVenta;
		} else {
			return $responseVenta;
		}
	}

	public function actualizarVenta_Inventario($arrWhereVenta = '', $arrOrdenVentaCabecera = '', $arrDetalleOrdenVenta = '', $Nu_Descargar_Inventario = '', $arrClienteNuevo = '', $arrContactoNuevo = '', $arrVehiculoNuevo = ''){
		$responseVenta = $this->OrdenVentaModel->actualizarVenta($arrWhereVenta, $arrOrdenVentaCabecera, $arrDetalleOrdenVenta, $arrClienteNuevo, $arrContactoNuevo, $arrVehiculoNuevo);
		if ($responseVenta['status'] == 'success') {
			/*
			if ($Nu_Descargar_Inventario == '1'){//1 = Si
				$arrOrdenVentaCabecera['ID_Tipo_Movimiento'] = 1;//Venta
				return $this->MovimientoInventarioModel->crudMovimientoInventario($arrOrdenVentaCabecera['ID_Almacen'], $responseVenta['Last_ID_Documento_Cabecera'], 0, $arrDetalleOrdenVenta, $arrOrdenVentaCabecera['ID_Tipo_Movimiento'], 1, $arrWhereVenta, 1, 1);
			}
			*/
			return $responseVenta;
		} else {
			return $responseVenta;
		}
	}
	
	public function eliminarOrdenVenta($ID, $Nu_Descargar_Inventario){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenVentaModel->eliminarOrdenVenta($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario)));
	}
	
	public function estadoOrdenVenta($ID, $Nu_Descargar_Inventario, $Nu_Estado){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenVentaModel->estadoOrdenVenta($this->security->xss_clean($ID), $this->security->xss_clean($Nu_Descargar_Inventario), $this->security->xss_clean($Nu_Estado)));
	}
	
	public function duplicarOrdenVenta($ID){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->OrdenVentaModel->duplicarOrdenVenta($this->security->xss_clean($ID)));
	}

	public function getOrdenVentaPDF($ID){
		$data = $this->OrdenVentaModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Pdf');
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		ob_start();
		$file = $this->load->view('Ventas/pdf/orden_venta_view', array(
			'arrData' => $data,
		));
		$html = ob_get_contents();
		ob_end_clean();
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - cotizacion Nro. ' . $data[0]->ID_Documento_Cabecera);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(true);
		$pdf->AddPage();

		$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.png';
		}
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 20%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 80%; text-align: left;">';
					$format_header .= '<p>';
						$format_header .= '<label style="color: #090909; text-align: center; font-size: 7px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Slogan_Empresa . '</label><br>';
						$format_header .= '<label style="color: #090909; text-align: center; font-size: 7px; font-family: "Times New Roman", Times, serif;">Dirección: ' . $this->empresa->Txt_Direccion_Empresa . '</label><br>';
						$format_header .= '<label style="color: #090909; text-align: center; font-size: 7px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Nu_Telefono_Empresa . ' - ' . $this->empresa->Nu_Celular_Empresa . '</label><br>';
						$format_header .= '<label style="color: #090909; text-align: center; font-size: 7px; font-family: "Times New Roman", Times, serif;">Correo: ' . $this->empresa->Txt_Email_Empresa . ' / RUC ' . $this->empresa->Nu_Documento_Identidad . '</label><br>';
						$format_header .= '<label style="color: #090909; text-align: center; font-size: 7px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->No_Dominio_Empresa . '</label>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';

		$pdf->writeHTML($format_header, true, 0, true, 0);
		$pdf->setFont('helvetica', '', 5);
		$pdf->writeHTML($html, true, false, true, false, '');
		if (!empty($this->empresa->Txt_Cuentas_Bancarias)) {
			$tableFooter = '
			<table class="table_pdf" cellpadding="2">
				<thead>
					<tr class="tr-theadFormat tr-header tr-header-detalle ">
						<th class="text-left" style="width: 100%; background-color: #C1C1BE;">Cuentas Bancarias</th>
					</tr>
					<tr class="tr-sub_thead">
						<th class="text-left content">'.$this->empresa->Txt_Cuentas_Bancarias.'</th>
					</tr>
				</thead>
			</table>';

			$pdf->SetY(-48);
			$pdf->setFont('helvetica', '', 7);
			$pdf->writeHTML($tableFooter, true, false, true, false, '');
		}

		$file_name = "laesystems_cotizacion_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".pdf";
		$pdf->Output($file_name, 'I');
	}

	public function getCartaCompromisoPDF($ID){
		$data = $this->OrdenVentaModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Pdf');
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', false, 'UTF-8', false);
		ob_start();
		$this->load->view('Ventas/pdf/carta_compromiso_view', array(
			'arrData' => $data
		));
		$html = ob_get_contents();
		ob_end_clean();
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems - cotizacion Nro. ' . $data[0]->ID_Documento_Cabecera);
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(false, 0);
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetHeaderMargin(0);
		$pdf->SetFooterMargin(0);
		$pdf->writeHTML($html);
		$pdf->SetY(-48);
		$pdf->Image("./assets/images/carta_compromiso/header_agua-fpng.jpg", 0, 0, 210, 27, '', '', '', false, 300, '', false, false, 0);
		$pdf->Image("./assets/images/carta_compromiso/footer_agua-fpng.jpg", 0, 280, 210, 17, '', '', '', false, 300, '', false, false, 0);
		$file_name = "laesystems_carta_compromiso_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".pdf";
		$pdf->Output($file_name, 'I');
	}

	public function enviarCorreo(){
		$arrData = $this->OrdenVentaModel->get_by_id($this->input->post('iIdOrden'));
		$this->load->library('email');
		$data = array();
		$data["No_Documento"] = 'Cotización Nro. ' . $this->input->post('iIdOrden');
		$data["Fe_Emision"] = ToDateBD($arrData[0]->Fe_Emision);
		$data["Fe_Vencimiento"] = ToDateBD($arrData[0]->Fe_Vencimiento);
		$data["No_Signo"] = $arrData[0]->No_Signo;
		$data["Ss_Total"] = $arrData[0]->Ss_Total;
		$data["No_Entidad"] = $arrData[0]->No_Entidad;
		$data["No_Empresa"]	= $this->empresa->No_Empresa;
		$data["Nu_Documento_Identidad_Empresa"] = $this->empresa->Nu_Documento_Identidad;
		$message = $this->load->view('correos/orden_venta', $data, true);
		$asunto = $this->input->post('sAsunto');

		$this->email->from($this->input->post('sCorreoDe'), $this->empresa->No_Empresa);//de
		$this->email->to($this->input->post('sCorreoPara'));//para
		$this->email->subject($asunto);
		$this->email->message($message);

		$this->load->library('Pdf');
		
		$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		ob_start();
		$file = $this->load->view('Ventas/pdf/orden_venta_view', array(
			'arrData' => $arrData,
		));
		$html = ob_get_contents();
		ob_end_clean();
		
		$pdf->SetAuthor('laesystems');
		$pdf->SetTitle('laesystems_cotizacion_' . $arrData[0]->ID_Documento_Cabecera);
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
		$pdf->AddPage();
		
    	$sNombreLogo=str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if ( !file_exists($this->file_path . $sNombreLogo) ) {
			$sNombreLogo='lae_logo_cotizacion.jpg';
		}
		$format_header = '<table border="0">';
			$format_header .= '<tr>';
				$format_header .= '<td rowspan="3" style="width: 20%; text-align: left;">';
					$format_header .= '<img style="height: 80px; width: ' . $this->empresa->Nu_Width_Logo_Ticket . 'px;" src="' . $this->file_path . $sNombreLogo . '"><br>';
				$format_header .= '</td>';
				$format_header .= '<td style="width: 80%; text-align: left;">';
					$format_header .= '<p>';
						$format_header .= '<br>';
						$format_header .= '<a style="text-decoration: none;" href="https://' . $this->empresa->No_Dominio_Empresa . '" target="_blank"><label style="color: #000000; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->No_Dominio_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #868686; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Nu_Celular_Empresa . '</label><br>';
						$format_header .= '<label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label><br>';
						//$format_header .= '<a style="text-decoration: none;" href="mailto:' . $this->empresa->Txt_Email_Empresa . '" target="_top" lt="Correo LAE System" title="Correo LAE System" data-attr="email"><label style="color: #34bdad; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Email_Empresa . '</label></a><br>';
						$format_header .= '<label style="color: #979797; font-size: 12px; font-family: "Times New Roman", Times, serif;">' . $this->empresa->Txt_Slogan_Empresa . '</label>';
					$format_header .= '</p>';
				$format_header .= '</td>';
			$format_header .= '</tr>';
		$format_header .= '</table>';
		
		$pdf->writeHTML($format_header, true, 0, true, 0);
		
        $pdf->setFont('helvetica', '', 7);
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$file_name = "laesystems_cotizacion_" . $arrData[0]->ID_Documento_Cabecera . "_" . $arrData[0]->Nu_Documento_Identidad . ".pdf";
		$pdfdoc = $pdf->Output(__DIR__ . $file_name, "F");
		
		$this->email->attach(__DIR__ . $file_name);

		$this->email->set_newline("\r\n");

		$isSend = $this->email->send();
		
		if($isSend) {
			unlink(__DIR__ . $file_name);
			$peticion = array(
				'status' => 'success',
				'style_modal' => 'modal-success',
				'message' => 'Correo enviado',
			);
			echo json_encode($peticion);
			exit();
		} else {
			unlink(__DIR__ . $file_name);
			$peticion = array(
				'status' => 'error',
				'style_modal' => 'modal-danger',
				'message' => 'No se pudo enviar el correo, inténtelo más tarde.',
				'sMessageErrorEmail' => $this->email->print_debugger(),
			);
			echo json_encode($peticion);
			exit();
		}// if - else envio email
	}
	
	public function generarLiquidacion(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		if (isset($_POST['arrIdDocumentoCabecera']))
			echo json_encode($this->OrdenVentaModel->generarLiquidacion($this->input->post()));
		else
			echo json_encode(array('sStatus' => 'danger', 'sMessage' => 'Debe seleccionar al menos 1 fila'));
	}

	public function enlazarOI(){
		if (!$this->input->is_ajax_request()) {
			exit('No se puede eliminar y acceder');
		}
		if (isset($_POST['arrIdDocumentoCabecera'])) {
			echo json_encode($this->OrdenVentaModel->enlazarOI($this->input->post()));
		}	else {
			echo json_encode(array('sStatus' => 'danger', 'sMessage' => 'Debe seleccionar al menos 1 fila'));
		}
	}

	public function desvincularOI(){
		if (!$this->input->is_ajax_request()) {
			exit('No se puede eliminar y acceder');
		}
		if (isset($_POST['ID'])) {
			echo json_encode($this->OrdenVentaModel->desvincularOI($_POST['ID']));
		}	else {
			echo json_encode(array('sStatus' => 'danger', 'sMessage' => 'No se encontro OI enlazada'));
		}
	}

	public function pagoContratista(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		if (isset($_POST['arrIdDocumentoCabecera']))
			echo json_encode($this->OrdenVentaModel->pagoContratista($this->input->post()));
		else
			echo json_encode(array('sStatus' => 'danger', 'sMessage' => 'Debe seleccionar al menos 1 fila'));
	}

	public function getOrdenVentaExcel($ID) {
		$data = $this->OrdenVentaModel->get_by_id($this->security->xss_clean($ID));
		$this->load->library('Excel');

		$styleCellHeader = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
		];
		$styleCellTitleTableHeader = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
                ]
			],
			'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'F5C72D'],
            ],
			'font' => [
                'bold' => true,
            ],
		];
		$styleCellTitleDataRightTableHeader = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'borders' => [
                'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleCellTitleDataLeftTableHeader = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'borders' => [
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleRowSubtitleTableHeader = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'C1C1BE'],
            ],
			'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
				],
				'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
				'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
			],
			'font' => [
                'bold' => true,
            ],
		];
		$styleLeftBorderThinTableHeader = [
			'borders' => [
				'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleAllBorderThinTableHeader = [
			'borders' => [
				'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleLeftBorderTableHeader = [
			'borders' => [
				'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleRightBorderTableHeader = [
			'borders' => [
				'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleBottomBorderTableHeader = [
			'borders' => [
				'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleColumnsTable = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'font' => [
                'bold' => true,
            ],
		];
		$styleGroupTable = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'font' => [
                'bold' => true,
            ],
			'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'C1C1BE'],
            ],
		];
		$styleItemDescriptionTable = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
		];
		$styleItemTable = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
		];
		$styleSubtotalTable = [
			'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'C1C1BE'],
            ],
		];
		$styleTotalsTable = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'borders' => [
                'outline' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ]
			],
			'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'C1C1BE'],
            ],
			'font' => [
                'bold' => true,
            ],
		];
		$styleItemTotalsTable = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'indent' => 2,
            ],
			'borders' => [
                'outline' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ]
			],
		];
		$styleHeaderObservationTable = [
			'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
			'borders' => [
                'outline' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
				'bottom' => [
					'style' => PHPExcel_Style_Border::BORDER_NONE,
				]
			],
			'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'C1C1BE'],
            ],
			'font' => [
                'bold' => true,
            ],
		];
		$styleItemObservationTable = [
			'borders' => [
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
				'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
			],
		];
		$styleLastItemObservationTable = [
			'borders' => [
                'outline' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
				],
				'top' => [
					'style' => PHPExcel_Style_Border::BORDER_NONE,
				]
			],
		];

		$formatCodeCurrency = '_("S/ "#,##0.00_);_("S/ "\(#,##0.00\);_("S/ ""0.00"??_);_(@_)';
		$formatCodeAccounting = '_("S/"* #,##0.00_);_("S/"* \(#,##0.00\);_("S/"* "0.00"??_);_(@_)';

		$fileNameExcel = "laesystems_cotizacion_" . $data[0]->ID_Documento_Cabecera . "_" . $data[0]->Nu_Documento_Identidad . ".xls";
		$sNombreLogo = str_replace(' ', '_', $this->empresa->No_Logo_Empresa);
		if (!file_exists($this->file_path . $sNombreLogo)) {
			$sNombreLogo = 'lae_logo_cotizacion.png';
		}
		$pathLogo = $this->file_path . $sNombreLogo;

		$headerTable = [
			$this->empresa->Txt_Slogan_Empresa,
			'Dirección: ' . $this->empresa->Txt_Direccion_Empresa,
			$this->empresa->Nu_Telefono_Empresa . ' - ' . $this->empresa->Nu_Celular_Empresa,
			'Correo: ' . $this->empresa->Txt_Email_Empresa . ' / RUC ' . $this->empresa->Nu_Documento_Identidad,
			$this->empresa->No_Dominio_Empresa,
        ];

		$spreadsheet = new PHPExcel();
		$worksheet = new PHPExcel_Worksheet($spreadsheet, "Hoja 1");
		$spreadsheet->addSheet($worksheet, 0);
		$worksheet->setShowGridlines(false);

		// Format width columns
		$firstColumn = "A";
		$lastColumn = "K";
		$defaultColumnWidth = 10;
		while ($firstColumn <= $lastColumn) {
			switch ($firstColumn) {
				case "A": $width = 2; break;
				case "I":
				case "K": $width = 22; break;
				default: $width = $defaultColumnWidth;
			}
 			$worksheet->getColumnDimension($firstColumn)->setWidth($width);
			$firstColumn++;
		}

		$row = 2;
		/**
		 * HEADER
		 */
		foreach ($headerTable as $item) {
			$worksheet->setCellValue("D$row", $item);
			$worksheet->mergeCells("D$row:K$row");
			$worksheet->getStyle("D$row:K$row")->applyFromArray($styleCellHeader);
			$row++;
		}
		$row++;

		/**
		 * LOGO
		 */
		$drawing = new PHPExcel_Worksheet_Drawing();
		$drawing->setName('test_img');
		$drawing->setDescription('test_img');
		$drawing->setPath($pathLogo);
		$drawing->setCoordinates('B2');
		$drawing->setOffsetX(5);
		$drawing->setOffsetY(5);
		$drawing->setWidth(100);
		$drawing->setHeight(100);
		$drawing->setWorksheet($worksheet);

		/**
		 * HEADER TABLE
		 */
		$worksheet->getRowDimension($row)->setRowHeight(22);
		$worksheet->setCellValue("B$row", mb_strtoupper($data[0]->No_Tipo_Documento_Breve, 'UTF-8'));
		$worksheet->mergeCells("B$row:I$row");
		$worksheet->getStyle("B$row:I$row")->applyFromArray($styleCellTitleTableHeader);

		$worksheet->setCellValue("J$row", $data[0]->ID_Serie_Documento . '-' . $data[0]->ID_Numero_Documento);
		$worksheet->mergeCells("J$row:K$row");
		$worksheet->getStyle("J$row:K$row")->applyFromArray($styleCellTitleTableHeader);
		$row++;

		$worksheet->getRowDimension($row)->setRowHeight(18);
		$worksheet->setCellValue("B$row", "Fecha emisión: ".ToDateBD($data[0]->Fe_Emision));
		$worksheet->mergeCells("B$row:G$row");
		$worksheet->getStyle("B$row:G$row")->applyFromArray($styleCellTitleDataLeftTableHeader);
		$worksheet->setCellValue("H$row", "Celular Asesor: ".$data[0]->Nu_Celular_Entidad_Empleado);
		$worksheet->mergeCells("H$row:K$row");
		$worksheet->getStyle("H$row:K$row")->applyFromArray($styleCellTitleDataRightTableHeader);
		$row++;

		$worksheet->getRowDimension($row)->setRowHeight(18);
		$worksheet->setCellValue("B$row", "Asesor: ".$data[0]->No_Entidad_Empleado);
		$worksheet->mergeCells("B$row:G$row");
		$worksheet->getStyle("B$row:G$row")->applyFromArray($styleCellTitleDataLeftTableHeader);
		$worksheet->setCellValue("H$row", "Correo: ".$data[0]->Txt_Email_Entidad_Empleado);
		$worksheet->mergeCells("H$row:K$row");
		$worksheet->getStyle("H$row:K$row")->applyFromArray($styleCellTitleDataRightTableHeader);
		$row++;

		$worksheet->setCellValue("B$row", "DATOS CLIENTE");
		$worksheet->mergeCells("B$row:F$row");
		$worksheet->setCellValue("G$row", "DATOS VEHÍCULO");
		$worksheet->mergeCells("G$row:K$row");
		$worksheet->getStyle("B$row:K$row")->applyFromArray($styleRowSubtitleTableHeader);
		$worksheet->getRowDimension($row)->setRowHeight(19);
		$row++;

		$worksheet->setCellValue("B$row", "Cliente:");
		$worksheet->getStyle("B$row")->applyFromArray($styleLeftBorderTableHeader);
		$worksheet->setCellValue("C$row", $data[0]->No_Entidad);
		$worksheet->mergeCells("C$row:F$row");
		$worksheet->setCellValue("G$row", "Placa: ".$data[0]->No_Placa_Vehiculo);
		$worksheet->mergeCells("G$row:I$row");
		$worksheet->getStyle("G$row")->applyFromArray($styleLeftBorderThinTableHeader);
		$worksheet->setCellValue("J$row", "Color: ".$data[0]->No_Color_Vehiculo);
		$worksheet->mergeCells("J$row:K$row");
		$worksheet->getStyle("K$row")->applyFromArray($styleRightBorderTableHeader);
		$worksheet->getRowDimension($row)->setRowHeight(18);
		$worksheet->getStyle("B$row:K$row")->applyFromArray(array_merge($styleLeftBorderTableHeader, $styleItemDescriptionTable));
		$row++;

		$km = '';
		$arrResponseRelacion = $this->HelperModel->getRelacionTablaMultiplePresupuestoAndOI([
			'iRelacionDatos' => 6,
			'iIdOrigenTabla' => $data[0]->ID_Documento_Cabecera
		]);
		if ($arrResponseRelacion['sStatus'] == 'success') {
			$km = $arrResponseRelacion['arrData'][0]->No_Kilometraje;
		}
		$worksheet->setCellValue("B$row", $data[0]->No_Tipo_Documento_Identidad_Breve.': ');
		$worksheet->getStyle("B$row")->applyFromArray($styleLeftBorderTableHeader);
		$worksheet->setCellValue("C$row", $data[0]->Nu_Documento_Identidad);
		$worksheet->mergeCells("C$row:F$row");
		$worksheet->getStyle("C$row:F$row")->applyFromArray($styleItemDescriptionTable);
		$worksheet->getStyle("C$row:F$row")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
		$worksheet->setCellValue("G$row", "Marca: ".$data[0]->No_Marca_Vehiculo);
		$worksheet->getStyle("G$row")->applyFromArray($styleLeftBorderThinTableHeader);
		$worksheet->mergeCells("G$row:I$row");
		$worksheet->setCellValue("J$row", "KM: ".$km);
		$worksheet->mergeCells("J$row:K$row");
		$worksheet->getStyle("K$row")->applyFromArray($styleRightBorderTableHeader);
		$worksheet->getRowDimension($row)->setRowHeight(18);
		$worksheet->getStyle("B$row:K$row")->applyFromArray(array_merge($styleLeftBorderTableHeader, $styleItemDescriptionTable));
		$row++;

		$worksheet->getStyle("B$row")->applyFromArray($styleLeftBorderTableHeader);
		$worksheet->setCellValue("G$row", "Modelo: ".$data[0]->No_Modelo_Vehiculo);
		$worksheet->getStyle("G$row")->applyFromArray($styleLeftBorderThinTableHeader);
		$worksheet->mergeCells("G$row:K$row");
		$worksheet->getStyle("K$row")->applyFromArray($styleRightBorderTableHeader);
		$worksheet->getStyle("B$row:K$row")->applyFromArray($styleBottomBorderTableHeader);
		$worksheet->getRowDimension($row)->setRowHeight(18);
		$worksheet->getStyle("B$row:K$row")->applyFromArray(array_merge($styleLeftBorderTableHeader, $styleItemDescriptionTable));
		$row+=2;

		/**
		 * COLUMNS TABLE
		 */
		$worksheet->setCellValue("B$row", "DESCRIPCIÓN");
		$worksheet->mergeCells("B$row:F$row");
		$worksheet->setCellValue("G$row", "Cant");
		$worksheet->setCellValue("I$row", "P. Unit");
		$worksheet->setCellValue("K$row", "Sub Total");
		$worksheet->getStyle("B$row:K$row")->applyFromArray($styleColumnsTable);
		$row++;

		$Ss_Gravada = 0.00;
		$Ss_Exonerada = 0.00;
		$Ss_Inafecto = 0.00;
		$Ss_Gratuita = 0.00;
		$Ss_IGV = 0.00;
		$Ss_Total = 0.00;
		$iCounter = 0;
		$iIdFamilia = 0;
		$arrDataLinea=array();
		$Ss_Total_Familia = 0.00;
		$Ss_SubTotal_Familia = 0.00;
		foreach($data as $item) {
			if ( $iIdFamilia != $item->ID_Familia ) {
				if ($iCounter != 0) {
					$row++;
					$worksheet->setCellValue("I$row", "Sub Total");
					$worksheet->mergeCells("I$row:J$row");
					$worksheet->setCellValue("K$row", $Ss_SubTotal_Familia);
					$worksheet->getStyle("K$row")->getNumberFormat()->setFormatCode($formatCodeAccounting);
					$worksheet->getStyle("I$row:K$row")->applyFromArray($styleSubtotalTable);
					$worksheet->getRowDimension($row)->setRowHeight(19);
					$row+=2;

					$row_linea['Ss_SubTotal_Familia'] = $Ss_SubTotal_Familia;
					$row_linea['Ss_Total_Familia'] = $Ss_Total_Familia;
					$arrDataLinea[] = $row_linea;
					$Ss_Total_Familia = 0.00;
					$Ss_SubTotal_Familia = 0.00;
				}

				$worksheet->setCellValue("B$row", $item->No_Familia);
				$worksheet->mergeCells("B$row:K$row");
				$worksheet->getStyle("B$row:K$row")->applyFromArray($styleGroupTable);
				$worksheet->getRowDimension($row)->setRowHeight(19);
				$row++;

				$iIdFamilia = $item->ID_Familia;
                $row_linea = array();
                $row_linea['No_Familia'] = $item->No_Familia;
			}
			if ($item->Nu_Tipo_Impuesto == 1){//IGV
				$Ss_IGV += $item->Ss_Impuesto_Producto;
				$Ss_Gravada += $item->Ss_SubTotal_Producto;
			} else if ($item->Nu_Tipo_Impuesto == 2){//Inafecto - Operación Onerosa
				$Ss_Inafecto += $item->Ss_SubTotal_Producto;
			} else if ($item->Nu_Tipo_Impuesto == 3){//Exonerado - Operación Onerosa
				$Ss_Exonerada += $item->Ss_SubTotal_Producto;
			} else if ($item->Nu_Tipo_Impuesto == 4){//Gratuita
				$Ss_Gratuita += $item->Ss_SubTotal_Producto;
			}
			$Ss_Total += $item->Ss_Total_Producto;
			$Ss_Total_Familia += $item->Ss_Total_Producto;
			$Ss_SubTotal_Familia += $item->Ss_SubTotal_Producto;

			$subdescription = !empty($item->Txt_Nota_Detalle) ? ": ".PHP_EOL. $item->Txt_Nota_Detalle : '';
			$description = $item->No_Producto . $subdescription;
			$worksheet->setCellValue("C$row", $description);
			$worksheet->setCellValue("Z$row", $description);
			$worksheet->mergeCells("C$row:F$row");
			$worksheet->getRowDimension($row)->setRowHeight(-1);
			$worksheet->getStyle("C$row:F$row")->getAlignment()->setWrapText(true);
			$worksheet->getStyle("Z$row")->getAlignment()->setWrapText(true);
			$worksheet->getColumnDimension("Z")->setWidth($defaultColumnWidth*4);
			$worksheet->getColumnDimension("Z")->setVisible(false);
			$worksheet->setCellValue("G$row", $item->Qt_Producto);
			$worksheet->getStyle("G$row")->applyFromArray($styleItemTable);
			$worksheet->setCellValue("I$row", $item->Ss_Precio / $item->Ss_Impuesto);
			$worksheet->getStyle("I$row")->applyFromArray($styleItemTable);
			$worksheet->getStyle("I$row")->getNumberFormat()->setFormatCode($formatCodeCurrency);
			$worksheet->setCellValue("K$row", $item->Ss_SubTotal_Producto);
			$worksheet->getStyle("K$row")->applyFromArray($styleItemTable);
			$worksheet->getStyle("K$row")->getNumberFormat()->setFormatCode($formatCodeCurrency);
			$row++;
			$iCounter++;
		}
		$row++;
		$worksheet->setCellValue("I$row", "Sub Total");
		$worksheet->mergeCells("I$row:J$row");
		$worksheet->setCellValue("K$row", $Ss_SubTotal_Familia);
		$worksheet->getStyle("K$row")->getNumberFormat()->setFormatCode($formatCodeAccounting);
		$worksheet->getStyle("I$row:K$row")->applyFromArray($styleSubtotalTable);
		$worksheet->getRowDimension($row)->setRowHeight(19);
		$row+=2;

		$row_linea['Ss_SubTotal_Familia'] = $Ss_SubTotal_Familia;
		$row_linea['Ss_Total_Familia'] = $Ss_Total_Familia;
		$arrDataLinea[] = $row_linea;
		$Ss_Total_Familia = 0.00;
		$Ss_SubTotal_Familia = 0.00;

		$row++;
		$worksheet->setCellValue("F$row", "Totales");
		$worksheet->mergeCells("F$row:K$row");
		$worksheet->getStyle("F$row:K$row")->applyFromArray($styleTotalsTable);
		$worksheet->getRowDimension($row)->setRowHeight(19);
		$row++;

		foreach($arrDataLinea as $item) {
			$worksheet->setCellValue("F$row", "Sub total ".$item['No_Familia']);
			$worksheet->mergeCells("F$row:J$row");
			$worksheet->getStyle("F$row:J$row")->applyFromArray($styleItemTotalsTable);
			$worksheet->setCellValue("K$row", $item['Ss_SubTotal_Familia']);
			$worksheet->getStyle("K$row")->applyFromArray($styleAllBorderThinTableHeader);
			$worksheet->getStyle("K$row")->getNumberFormat()->setFormatCode($formatCodeAccounting);
			$worksheet->getRowDimension($row)->setRowHeight(19);
			$row++;
		}

		$worksheet->setCellValue("F$row", "Suma Total Sin Igv");
		$worksheet->mergeCells("F$row:J$row");
		$worksheet->getStyle("F$row:J$row")->applyFromArray($styleItemTotalsTable);
		$worksheet->setCellValue("K$row", $Ss_Gravada);
		$worksheet->getStyle("K$row")->applyFromArray($styleAllBorderThinTableHeader);
		$worksheet->getStyle("K$row")->getNumberFormat()->setFormatCode($formatCodeAccounting);
		$worksheet->getRowDimension($row)->setRowHeight(19);
		$row++;
		$worksheet->setCellValue("F$row", "Igv");
		$worksheet->mergeCells("F$row:J$row");
		$worksheet->getStyle("F$row:J$row")->applyFromArray($styleItemTotalsTable);
		$worksheet->setCellValue("K$row", $Ss_IGV);
		$worksheet->getStyle("K$row")->applyFromArray($styleAllBorderThinTableHeader);
		$worksheet->getStyle("K$row")->getNumberFormat()->setFormatCode($formatCodeAccounting);
		$worksheet->getRowDimension($row)->setRowHeight(19);
		$row++;
		$worksheet->setCellValue("F$row", "Suma Total con Igv");
		$worksheet->mergeCells("F$row:J$row");
		$worksheet->getStyle("F$row:J$row")->applyFromArray($styleItemTotalsTable);
		$worksheet->setCellValue("K$row", $data[0]->Ss_Total);
		$worksheet->getStyle("K$row")->applyFromArray($styleAllBorderThinTableHeader);
		$worksheet->getStyle("K$row")->getNumberFormat()->setFormatCode($formatCodeAccounting);
		$worksheet->getRowDimension($row)->setRowHeight(19);
		$row++;

		$worksheet->getRowDimension($row)->setRowHeight(31);
		$row++;

		/**
		 * OBSERVACIONES
		 */
		if (!empty($data[0]->Txt_Glosa)) {
			$worksheet->setCellValue("B$row", "Observaciones:");
			$worksheet->mergeCells("B$row:K$row");
			$worksheet->getStyle("B$row:K$row")->applyFromArray($styleHeaderObservationTable);
			$worksheet->getRowDimension($row)->setRowHeight(19);
			$row++;

			$worksheet->setCellValue("B$row", "");
			$worksheet->mergeCells("B$row:K$row");
			$worksheet->getStyle("B$row:K$row")->applyFromArray($styleItemObservationTable);
			$row++;

			$worksheet->getRowDimension($row)->setRowHeight(-1);
			$worksheet->getStyle("B$row:K$row")->getAlignment()->setWrapText(true);
			$worksheet->setCellValue("B$row", $data[0]->Txt_Glosa);
			$worksheet->mergeCells("B$row:K$row");
			$worksheet->getStyle("B$row:K$row")->applyFromArray($styleItemObservationTable);
			$row++;

			$worksheet->setCellValue("B$row", "");
			$worksheet->mergeCells("B$row:K$row");
			$worksheet->getStyle("B$row:K$row")->applyFromArray($styleLastItemObservationTable);
			$row++;
		}

		/**
		 * FOOTER
		 */
		$row++;
		$worksheet->setCellValue("B$row", "Cuentas Bancarias");
		$worksheet->mergeCells("B$row:K$row");
		$worksheet->getStyle("B$row:K$row")->applyFromArray($styleGroupTable);
		$worksheet->getRowDimension($row)->setRowHeight(19);
		$row++;

		$worksheet->setCellValue("B$row", "CUENTA CORRIENTE SOLES");
		$worksheet->mergeCells("B$row:E$row");
		$worksheet->getStyle("B$row:E$row")->applyFromArray(['font' => ['bold' => true,],]);
		$worksheet->setCellValue("F$row", "CUENTA CORRIENTE SOLES");
		$worksheet->mergeCells("F$row:K$row");
		$worksheet->getStyle("F$row:K$row")->applyFromArray(['font' => ['bold' => true,],]);
		$worksheet->getRowDimension($row)->setRowHeight(17);
		$row++;

		$rowsFooter = [
			['Banco: BCP', 'Banco: BBVA'],
			['Titular: MAFISA MOTORS SAC', 'Titular: MAFISA MOTORS SAC'],
			['Número de Cuenta: 191-2324271-0-84', 'Número de Cuenta: 0011-0109-0100066710-63'],
			['CCI: 002-191-002324271084-58', 'CCI: 011-109-000100066710-63'],
		];
		foreach ($rowsFooter as $rowFooter) {
			foreach ($rowFooter as $index => $itemFooter) {
				$sentences = explode(': ', $itemFooter);
				$richText = new PHPExcel_RichText();
				$richText->createTextRun($sentences[0].": ");
				$value = $richText->createTextRun($sentences[1]);
				$value->getFont()->setBold(true);
				if ($index == 0) {
					$worksheet->setCellValue("B$row", $richText);
					$worksheet->mergeCells("B$row:E$row");
				} else {
					$worksheet->setCellValue("F$row", $richText);
					$worksheet->mergeCells("F$row:K$row");
				}
			}
			$worksheet->getRowDimension($row)->setRowHeight(17);
			$row++;
		}

		$spreadsheet->removeSheetByIndex(1);
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $fileNameExcel . '"');

        $objWriter = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
        $objWriter->save('php://output');
	}
}