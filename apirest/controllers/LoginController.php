<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class LoginController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->library('encryption');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('LoginModel');
	}

	public function index(){
		$this->load->view('Login/LoginView');
	}
	
	public function recuperar_cuenta($Txt_Token_Activacion){
		$data = array();
		$data["token"] = $Txt_Token_Activacion;
		$this->load->view('Login/recuperar_cuenta', $data);
	}
	
	public function post(){
		if($this->input->is_ajax_request()){
			$data = array(
				'No_Usuario' => $this->input->post('No_Usuario'),
				'No_Password' => $this->input->post('No_Password'),
				'ID_Empresa' => $this->input->post('ID_Empresa'),
				'ID_Organizacion' => $this->input->post('ID_Organizacion'),
			);
			
			$arrModelLogin = $this->LoginModel->verificarAccesoLogin($data);
			echo json_encode($arrModelLogin);
			exit();
		}else{
			echo "error 404";
			show_404();
		}
	}
	
	private function token(){
        return sha1(uniqid(rand(),true));
    }
    
	public function verificar_email(){
		if($this->input->is_ajax_request()){
			$sendPost = array(
				'Txt_Email' => $this->input->post('Txt_Email_Recovery'),
				'Txt_Token_Activacion' => $this->token(),
			);
			
			$peticion = $this->LoginModel->verificar_email($sendPost);

			if($peticion['status'] == 'success' ) {
				$this->load->library('email');

				$data = array();
				$data["token"] = $sendPost['Txt_Token_Activacion'];
                $message = $this->load->view('correos/recuperar_cuenta', $data, true);
                
				$this->email->from('noreply@laesystems.com', 'laesystems');//de
				$this->email->to($this->input->post('Txt_Email_Recovery'));//para
				$this->email->subject('Recuperar cuenta');
				$this->email->message($message);
				$this->email->set_newline("\r\n");

				$isSend = $this->email->send();
				//log_message('error', $this->email->print_debugger());
				//$isSend = TRUE;
				
				if($isSend) {
					$peticion = array(
						'status' => 'success',
						'type' => 'user',
						'message' => 'Correo enviado',
					);
				} else {
					$peticion = array(
						'status' => 'error',
						'type' => 'user',
						'message' => 'No se pudo enviar el correo, inténtelo más tarde.',
					);
				}
			}
			echo json_encode($peticion);
		}else{
			show_404();
		}
	}
    
	public function cambiar_clave(){
		if($this->input->is_ajax_request()){
			$data = array(
				'No_Password' => $this->encryption->encrypt($this->input->post('No_Password')),
				'Txt_Token_Activacion' => $this->input->post('Txt_Token_Activacion'),
			);
			echo json_encode($this->LoginModel->cambiar_clave($data));
		} else{
			show_404();
		}
	}
	
	public function logout(){
		$this->session->unset_userdata('usuario');
		unset(
			$_SESSION['usuario']->ID_Empresa,
			$_SESSION['usuario']->ID_Organizacion,
			$_SESSION['usuario']->ID_Usuario,
			$_SESSION['usuario']->No_Usuario,
			$_SESSION['usuario']->No_Nombres_Apellidos,
			$_SESSION['usuario']->Txt_Email,
			$_SESSION['usuario']->Txt_Token_Activacion,
			$_SESSION['usuario']->ID_Grupo,
			$_SESSION['usuario']->No_IP,
			$_SESSION['usuario']->Fe_Creacion,
			$_SESSION['usuario']->Nu_Estado,
			$_SESSION['usuario']->No_Grupo,
			$_SESSION['usuario']->ID_Grupo_Usuario
		);
		$this->session->sess_destroy();
		redirect('');
	}
}
