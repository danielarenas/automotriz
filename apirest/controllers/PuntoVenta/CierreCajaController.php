<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class CierreCajaController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('HelperModel');
		$this->load->model('PuntoVenta/CierreCajaModel');
	}

	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$arrModalVentasMultiples = array();
			if ( isset($this->session->userdata['arrDataPersonal']) && $this->session->userdata['arrDataPersonal']['sStatus']=='success' ) {
				$arrParams = array(
					'iIdMatriculaPersonal' => $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado,
					'dMatricula' => $this->session->userdata['arrDataPersonal']['arrData'][0]->Fe_Matricula,
				);
				$arrModalVentasMultiples = $this->CierreCajaModel->obtenerVentasMultiples($arrParams);
			}
			$this->load->view('header');
			$this->load->view('PuntoVenta/CierreCajaView', array(
				'arrModalVentasMultiples' => $arrModalVentasMultiples,
			));
			$this->load->view('footer', array("js_cierre_caja" => true));
		}
	}

	public function addCierreCaja(){
		if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
	
		$arrData = array(
			'ID_Empresa' => $this->empresa->ID_Empresa,
			'ID_Organizacion' => $this->empresa->ID_Organizacion,
			'ID_Almacen' => $this->empresa->ID_Almacen,
			'ID_Matricula_Empleado' => $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_Matricula_Empleado,
			'ID_POS' => $this->session->userdata['arrDataPersonal']['arrData'][0]->ID_POS,
			'Fe_Movimiento' => dateNow('fecha_hora'),
			'ID_Tipo_Operacion_Caja_Apertura' => $this->input->post('iIdTipoOperacionCajaApertura'),//3=Apertura de Caja
			'ID_Tipo_Operacion_Caja' => $this->input->post('iIdTipoOperacionCaja'),//4=Cierre de Caja
			'ID_Moneda' => $this->input->post('iIdMoneda'),
			'Ss_Expectativa' => $this->input->post('fTotalLiquidar'),
			'Ss_Total' => $this->input->post('fTotalDepositado'),
			'Txt_Nota' => $this->input->post('sNotaCaja'),
			'Nu_Estado' => 1,
		);
		
		$arrResponseModal = $this->CierreCajaModel->addCierreCaja($arrData);
		if ( $arrResponseModal['sStatus']=='success' ) {
			$keys = array(
				'arrDataPersonal',
			);
			$this->session->unset_userdata($keys);
		}
		echo json_encode($arrResponseModal);
	}
}
