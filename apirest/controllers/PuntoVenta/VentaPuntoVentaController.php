<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Lima');

class VentaPuntoVentaController extends CI_Controller {
	
	function __construct(){
    	parent::__construct();	
		$this->load->library('session');
		$this->load->database('LAE_SYSTEMS');
		$this->load->model('PuntoVenta/VentaPuntoVentaModel');
		$this->load->model('HelperModel');
		$this->load->model('Logistica/MovimientoInventarioModel');
		$this->load->model('DocumentoElectronicoModel');
	}

	public function listar(){
		if(!$this->MenuModel->verificarAccesoMenu()) redirect('Inicio/InicioView');
		if(isset($this->session->userdata['usuario'])) {
			$this->load->view('header');
			$this->load->view('PuntoVenta/VentaPuntoVentaView');
			$this->load->view('footer', array("js_venta_punto_venta" => true));
		}
	}	
	
    private function getReporte($arrParams){
        $arrResponseModal = $this->VentaPuntoVentaModel->getReporte($arrParams);
        if ( $arrResponseModal['sStatus']=='success' ) {
            $data = array();
            
            $fSubTotal = 0.00;
            $fIGV = 0.00;
            $fDescuento = 0.00;
            $fTotal = 0.00;
            $sAccionVer='ver';
            $sAccionImprimir='imprimir';
            $sVacio='mostrar-img-logo_punto_venta';
            
            foreach ($arrResponseModal['arrData'] as $row) {
                $rows = array();
                $rows['ID_Documento_Cabecera'] = $row->ID_Documento_Cabecera;
                $rows['Fe_Emision_Hora'] = allTypeDate($row->Fe_Emision_Hora, '-', 0);
                $rows['No_Empleado'] = $row->No_Empleado;
                $rows['ID_Tipo_Documento'] = $row->ID_Tipo_Documento;
                $rows['No_Tipo_Documento_Breve'] = $row->No_Tipo_Documento_Breve;
                $rows['ID_Serie_Documento'] = $row->ID_Serie_Documento;
                $rows['ID_Numero_Documento'] = $row->ID_Numero_Documento;
                $rows['No_Entidad'] = $row->No_Entidad;
                $rows['Ss_Tipo_Cambio'] = ($row->ID_Tipo_Documento != 5 ? $row->Ss_Tipo_Cambio : $row->Ss_Tipo_Cambio_Modificar);
                
                $fTotal = $row->Ss_Total;
                $fTotalExtranjera = 0.00;
                if ( $row->Nu_Sunat_Codigo_Moneda != 'PEN' ) {//1=Soles
                    $fTotalExtranjera = $row->Ss_Total;
                }
                $rows['Ss_Total'] = ($row->ID_Tipo_Documento != 5 ? $fTotal : -$fTotal);
                $rows['Ss_Total_Extranjero'] = ($row->ID_Tipo_Documento != 5 ? $fTotalExtranjera : -$fTotalExtranjera);
                $rows['Ss_Total_Saldo'] = ($row->ID_Tipo_Documento != 5 ? $row->Ss_Total_Saldo : -$row->Ss_Total_Saldo);

                $rows['No_Estado'] = $row->No_Estado;
                $rows['No_Class_Estado'] = $row->No_Class_Estado;
                $rows['Nu_Estado'] = $row->Nu_Estado;
                $rows['Nu_Estado_Lavado'] = $row->Nu_Estado_Lavado;
                $rows['Nu_Estado_Lavado_Recepcion_Cliente'] = $row->Nu_Estado_Lavado_Recepcion_Cliente;
                $rows['No_Signo'] = $row->No_Signo;
                $rows['ID_Documento_Medio_Pago'] = $row->ID_Documento_Medio_Pago;
                $rows['sAccionVer'] = '<button class="btn btn-xs btn-link" alt="Ver comprobante" title="Ver comprobante" href="javascript:void(0)" onclick="formatoImpresionTicket(\'' . $sAccionVer . '\', \'' . $row->ID_Documento_Cabecera . '\', \'' . $sVacio . '\')"><i class="fa fa-list-alt" aria-hidden="true"> Ver</i></button>';
                $rows['sAccionImprimir'] = '<button class="btn btn-xs btn-link" alt="Imprimir comprobante" title="Imprimir comprobante" href="javascript:void(0)" onclick="formatoImpresionTicket(\'' . $sAccionImprimir . '\', \'' . $row->ID_Documento_Cabecera . '\', \'' . $sVacio . '\')"><i class="fa fa-print" aria-hidden="true"> Imprimir</i></button>';
                $data[] = (object)$rows;
            }
            return array(
                'sStatus' => 'success',
                'arrData' => $data,
            );
        } else {
            return $arrResponseModal;
        }
    }
    
	public function sendReporte(){
        $arrParams = array(
            'iTipoConsultaFecha'  => $this->input->post('iTipoConsultaFecha'),
            'Fe_Inicio'  => $this->input->post('Fe_Inicio'),
            'Fe_Fin'  => $this->input->post('Fe_Fin'),
            'ID_Tipo_Documento'  => $this->input->post('ID_Tipo_Documento'),
            'ID_Serie_Documento'  => $this->input->post('ID_Serie_Documento'),
            'ID_Numero_Documento'  => $this->input->post('ID_Numero_Documento'),
            'Nu_Estado_Documento'  => $this->input->post('Nu_Estado_Documento'),
            'iIdCliente' => $this->input->post('iIdCliente'),
            'sNombreCliente' => $this->input->post('sNombreCliente'),
            'iTipoRecepcionCliente' => $this->input->post('iTipoRecepcionCliente'),
        );
        echo json_encode($this->getReporte($arrParams));
    }
    
	public function sendReportePDF($iTipoConsultaFecha, $Fe_Inicio, $Fe_Fin, $ID_Tipo_Documento, $ID_Serie_Documento, $ID_Numero_Documento, $Nu_Estado_Documento, $iIdCliente, $sNombreCliente, $iTipoRecepcionCliente){
        $this->load->library('FormatoLibroSunatPDF');
        
        $iTipoConsultaFecha = $this->security->xss_clean($iTipoConsultaFecha);
        $Fe_Inicio = $this->security->xss_clean($Fe_Inicio);
        $Fe_Fin = $this->security->xss_clean($Fe_Fin);
        $ID_Tipo_Documento = $this->security->xss_clean($ID_Tipo_Documento);
        $ID_Serie_Documento = $this->security->xss_clean($ID_Serie_Documento);
        $ID_Numero_Documento = $this->security->xss_clean($ID_Numero_Documento);
        $Nu_Estado_Documento = $this->security->xss_clean($Nu_Estado_Documento);
        $iIdCliente = $this->security->xss_clean($iIdCliente);
        $sNombreCliente = $this->security->xss_clean($sNombreCliente);
        $iTipoRecepcionCliente = $this->security->xss_clean($iTipoRecepcionCliente);
        
		$fileNamePDF = "ventas_x_punto_venta" . $Fe_Inicio . "_" . $Fe_Fin . ".pdf";
        
        $pdf = new FormatoLibroSunatPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $arrCabecera = array (
            "Fe_Inicio" => ToDateBD($Fe_Inicio),
            "Fe_Fin" => ToDateBD($Fe_Fin),
        );
        
        $arrParams = array(
            'iTipoConsultaFecha' => $iTipoConsultaFecha,
            'Fe_Inicio' => $Fe_Inicio,
            'Fe_Fin' => $Fe_Fin,
            'ID_Tipo_Documento' => $ID_Tipo_Documento,
            'ID_Serie_Documento' => $ID_Serie_Documento,
            'ID_Numero_Documento' => $ID_Numero_Documento,
            'Nu_Estado_Documento' => $Nu_Estado_Documento,
            'iIdCliente' => $iIdCliente,
            'sNombreCliente' => $sNombreCliente,
            'iTipoRecepcionCliente' => $iTipoRecepcionCliente,
        );

		ob_start();
		$file = $this->load->view('PuntoVenta/pdf/VentaPuntoVentaViewPDF', array(
			'arrCabecera' => $arrCabecera,
			'arrDetalle' => $this->getReporte($arrParams),
		));
		$html = ob_get_contents();
		ob_end_clean();
        		
		$pdf->SetAuthor('LAE');
		$pdf->SetTitle('LAE - Ventas por Punto de Venta');
	
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        
        $pdf->setFont('helvetica', '', 5);
        
		$pdf->AddPage('P', 'A4');
		$pdf->writeHTML($html, true, false, true, false, '');
		
		$pdf->Output($fileNamePDF, 'I');
	}
    
	public function sendReporteEXCEL($iTipoConsultaFecha, $Fe_Inicio, $Fe_Fin, $ID_Tipo_Documento, $ID_Serie_Documento, $ID_Numero_Documento, $Nu_Estado_Documento, $iIdCliente, $sNombreCliente, $iTipoRecepcionCliente){
        $this->load->library('Excel');
	    
        $iTipoConsultaFecha = $this->security->xss_clean($iTipoConsultaFecha);
        $Fe_Inicio = $this->security->xss_clean($Fe_Inicio);
        $Fe_Fin = $this->security->xss_clean($Fe_Fin);
        $ID_Tipo_Documento = $this->security->xss_clean($ID_Tipo_Documento);
        $ID_Serie_Documento = $this->security->xss_clean($ID_Serie_Documento);
        $ID_Numero_Documento = $this->security->xss_clean($ID_Numero_Documento);
        $Nu_Estado_Documento = $this->security->xss_clean($Nu_Estado_Documento);
        $iIdCliente = $this->security->xss_clean($iIdCliente);
        $sNombreCliente = $this->security->xss_clean($sNombreCliente);
        $iTipoRecepcionCliente = $this->security->xss_clean($iTipoRecepcionCliente);
        
		$fileNameExcel = "ventas_x_punto_venta" . $Fe_Inicio . "_" . $Fe_Fin . ".xls";
		
	    $objPHPExcel = new PHPExcel();
	    
	    $objPHPExcel->getActiveSheet()->setTitle('Ventas por Punto de Venta');
        
	    $hoja_activa = 0;
	    
        $BStyle_top = array(
          'borders' => array(
            'top' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_left = array(
          'borders' => array(
            'left' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_right = array(
          'borders' => array(
            'right' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $BStyle_bottom = array(
          'borders' => array(
            'bottom' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        );
        
        $style_align_center = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        
        $style_align_right = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );
        
        $style_align_left = array(
        'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        
	    //Title
	    $objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex($hoja_activa)
        ->setCellValue('A1', $this->empresa->No_Empresa)
        ->setCellValue('C2', 'Informe de Ventas por Punto de Venta')
        ->setCellValue('C3', 'Desde: ' . ToDateBD($Fe_Inicio) . ' Hasta: ' . ToDateBD($Fe_Fin));
        
        $objPHPExcel->getActiveSheet()->getStyle('C2')->applyFromArray($style_align_center);
        $objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray($style_align_center);
        $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('C2:H2');
        $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('C3:H3');
        $objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("20");
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("15");
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth("10");
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth("10");
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth("40");
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth("8");
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth("12");
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth("12");
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth("12");
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth("25");

        $objPHPExcel->getActiveSheet()->getStyle('A5:K5')->applyFromArray($BStyle_top);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($BStyle_right);
        $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray($BStyle_right);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5:K5')->applyFromArray($BStyle_bottom);

        $objPHPExcel->getActiveSheet()->getStyle('A5:K5')->getFont()->setBold(true);
        
        $objPHPExcel->getActiveSheet()->getStyle('A5:K5')->applyFromArray($style_align_center);

        $objPHPExcel->setActiveSheetIndex($hoja_activa)
        ->setCellValue('A5', 'F. Emisión')
        ->setCellValue('B5', 'Cajero')
        ->setCellValue('C5', 'Tipo')
        ->setCellValue('D5', 'Serie')
        ->setCellValue('E5', 'Número')
        ->setCellValue('F5', 'Cliente')
        ->setCellValue('G5', 'T.C.')
        ->setCellValue('H5', 'Total S/')
        ->setCellValue('I5', 'Total M. Ex.')
        ->setCellValue('J5', 'Saldo')
        ->setCellValue('K5', 'Estado')
        ;
        
        $objPHPExcel->getActiveSheet()->freezePane('A6');//LINEA HORIZONTAL PARA SEPARAR CABECERA Y DETALLE
        
        $fila = 6;

        $arrParams = array(
            'iTipoConsultaFecha'  => $iTipoConsultaFecha,
            'Fe_Inicio'  => $Fe_Inicio,
            'Fe_Fin'  => $Fe_Fin,
            'ID_Tipo_Documento'  => $ID_Tipo_Documento,
            'ID_Serie_Documento'  => $ID_Serie_Documento,
            'ID_Numero_Documento'  => $ID_Numero_Documento,
            'Nu_Estado_Documento'  => $Nu_Estado_Documento,
            'iIdCliente' => $iIdCliente,
            'sNombreCliente' => $sNombreCliente,
            'iTipoRecepcionCliente' => $iTipoRecepcionCliente,
        );
        $arrData = $this->getReporte($arrParams);
        
        if ( $arrData['sStatus'] == 'success' ) {
            $subtotal_s = 0.00; $descuento_s = 0.00; $igv_s = 0.00; $total_s = 0.00; $total_d = 0.00;
            $sum_general_subtotal_s=0.00; $sum_general_igv_s=0.00; $sum_general_descuento_s=0.00; $sum_general_total_s=0.00; $sum_general_total_d=0.00;
            foreach($arrData['arrData'] as $row) {
                $objPHPExcel->getActiveSheet()->getStyle('A' . $fila . ':' . 'E' . $fila)->applyFromArray($style_align_center);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $fila)->applyFromArray($style_align_left);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $fila . ':' . 'J' . $fila)->applyFromArray($style_align_right);
                $objPHPExcel->getActiveSheet()->getStyle('K' . $fila)->applyFromArray($style_align_center);

                $objPHPExcel->setActiveSheetIndex($hoja_activa)
                ->setCellValue('A' . $fila, $row->Fe_Emision_Hora)
                ->setCellValue('B' . $fila, $row->No_Empleado)
                ->setCellValue('C' . $fila, $row->No_Tipo_Documento_Breve)
                ->setCellValue('D' . $fila, $row->ID_Serie_Documento)
                ->setCellValue('E' . $fila, $row->ID_Numero_Documento)
                ->setCellValue('F' . $fila, $row->No_Entidad)
                ->setCellValue('G' . $fila, numberFormat($row->Ss_Tipo_Cambio, 3, '.', ','))
                ->setCellValue('H' . $fila, numberFormat($row->Ss_Total, 3, '.', ','))
                ->setCellValue('I' . $fila, numberFormat($row->Ss_Total_Extranjero, 3, '.', ','))
                ->setCellValue('J' . $fila, numberFormat($row->Ss_Total_Saldo, 3, '.', ','))
                ->setCellValue('K' . $fila, $row->No_Estado)
                ;
                $sum_general_total_s += $row->Ss_Total;
                $sum_general_total_d += $row->Ss_Total_Extranjero;

                $fila++;
            }// /. for each arrData
            
            $objPHPExcel->setActiveSheetIndex($hoja_activa)
            ->setCellValue('G' . $fila, 'Total')
            ->setCellValue('H' . $fila, numberFormat($sum_general_total_s, 2, '.', ','))
            ->setCellValue('I' . $fila, numberFormat($sum_general_total_d, 2, '.', ','));
            
            $objPHPExcel->getActiveSheet()->getStyle('G' . $fila . ':' . 'I' . $fila)->applyFromArray($style_align_right);
                        
            $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $fila . ':' . 'I' . $fila)
            ->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'E7E7E7')
                    )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle('G' . $fila . ':' . 'I' . $fila)->getFont()->setBold(true);
        } else {
            $objPHPExcel->setActiveSheetIndex($hoja_activa)
            ->setCellValue('A' . $fila, $arrData['sMessage']);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $fila)->applyFromArray($style_align_center);
            $objPHPExcel->setActiveSheetIndex($hoja_activa)->mergeCells('A' . $fila . ':' . 'J' . $fila);
        }// if - else arrData
        
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $fileNameExcel . '"');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
	public function entregarPedidoLavado(){
        if (!$this->input->is_ajax_request()) exit('No se puede eliminar y acceder');
		echo json_encode($this->VentaPuntoVentaModel->entregarPedidoLavado($this->input->post()));
	}
    
	public function facturarOrdenLavanderia(){
        echo json_encode($this->VentaPuntoVentaModel->facturarOrdenLavanderia($this->input->post()));
	}
}
