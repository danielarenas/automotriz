var url;
var table_compra;
var considerar_igv;
var nu_enlace;
var value_importes_cero = 0.00;
var texto_importes_cero = '0.00';
var arrImpuestosProducto = '{ "arrImpuesto" : [';
var arrImpuestosProductoDetalle;
var arrAreaIngreso = '';
var arrAreaIngresoDetalle;
var accion = '';
var bEstadoValidacion;
var iTipoImpuesto;

$('.date-picker-invoice').val(fDay + '/' + fMonth + '/' + fYear);

function agregarCompra() {
  accion = 'add_factura_compra';
  $('#modal-loader').modal('show');

  $('.div-Listar').hide();
  $('.div-AgregarEditar').show();

  $('[name="ENu_Estado"]').val('');
  $('.div-almacen').show();
  $('.div-stock').show();

  $('#form-Compra')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('.title_Compra').text('Nuevo Compra');

  $('[name="EID_Empresa"]').val('');
  $('[name="EID_Guia_Cabecera"]').val('');

  $('.date-picker-invoice').val(fDay + '/' + fMonth + '/' + fYear);

  $('#radio-ActiveFlete').prop('checked', true);
  $('#radio-InActiveFlete').prop('checked', false);

  $('#cbo-OrganizacionesVenta').prop('disabled', false);

  $('#radio-cliente_existente').prop('checked', true).iCheck('update');
  $('#radio-cliente_nuevo').prop('checked', false).iCheck('update');
  $('.div-cliente_existente').show();
  $('.div-cliente_nuevo').hide();

  $('#div-addFlete').show();
  $('#radio-flete_si').prop('checked', true).iCheck('update');
  $('#radio-flete_no').prop('checked', false).iCheck('update');

  $('#table-DetalleProductos tbody').empty();

  $('#panel-DetalleProductos').removeClass('panel-danger');
  $('#panel-DetalleProductos').addClass('panel-default');

  $('#txt-subTotal').val(value_importes_cero);
  $('#span-subTotal').text(texto_importes_cero);

  $('#txt-exonerada').val(value_importes_cero);
  $('#span-exonerada').text(texto_importes_cero);

  $('#txt-impuesto').val(value_importes_cero);
  $('#span-impuesto').text(texto_importes_cero);

  $('#txt-inafecto').val(value_importes_cero);
  $('#span-inafecto').text(texto_importes_cero);

  $('#txt-gratuita').val(value_importes_cero);
  $('#span-gratuita').text(texto_importes_cero);

  $('#txt-descuento').val(value_importes_cero);
  $('#span-descuento').text(texto_importes_cero);

  $('#txt-total').val(value_importes_cero);
  $('#span-total').text(texto_importes_cero);

  $('.span-signo').text('S/');

  $('#btn-save').attr('disabled', false);

  considerar_igv = 0;

  url = base_url + 'HelperController/getTiposDocumentos';
  $.post(url, { Nu_Tipo_Filtro: 7 }, function (response) {//2 = Compra
    $('#cbo-TiposDocumento').html('<option value="" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-TiposDocumento').append('<option value="' + response[i]['ID_Tipo_Documento'] + '" data-nu_impuesto="' + response[i]['Nu_Impuesto'] + '" data-nu_enlace="' + response[i]['Nu_Enlace'] + '">' + response[i]['No_Tipo_Documento_Breve'] + '</option>');
  }, 'JSON');

  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post(url, function (response) {
    $('#cbo-TiposDocumentoIdentidadCliente').html('');
    for (var i = 0; i < response.length; i++)
      $('#cbo-TiposDocumentoIdentidadCliente').append('<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>');
  }, 'JSON');

  $('#cbo-TiposDocumento').change(function () {
    if ($(this).val() > 0) {
      considerar_igv = $(this).find(':selected').data('nu_impuesto');
      nu_enlace = $(this).find(':selected').data('nu_enlace');

      url = base_url + 'HelperController/getSeriesDocumentoxAlmacen';
      $.post(url, { ID_Organizacion: $('#header-a-id_organizacion').val(), ID_Almacen: $('#cbo-almacen').val(), ID_Tipo_Documento: $(this).val() }, function (response) {
        if (response.length === 1) {
          $('#cbo-SeriesDocumento').html('<option value="' + response[0].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>');
          //Get número cuando solo haya una serie por un tipo de documento
          $('#txt-ID_Numero_Documento').val('');
          url = base_url + 'HelperController/getNumeroDocumento';
          $.post(url, { ID_Organizacion: $('#header-a-id_organizacion').val(), ID_Almacen: $('#cbo-almacen').val(), ID_Tipo_Documento: $('#cbo-TiposDocumento').val(), ID_Serie_Documento: response[0].ID_Serie_Documento }, function (responseNumeros) {
            if (responseNumeros.length === 0)
              $('#txt-ID_Numero_Documento').val('');
            else
              $('#txt-ID_Numero_Documento').val(responseNumeros.ID_Numero_Documento);
          }, 'JSON');
          //Fin número
          $('#cbo-SeriesDocumento').html('<option value="' + response[0].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>');
        } else if (response.length > 1) {
          $('#cbo-SeriesDocumento').html('<option value="0" selected="selected">- Seleccionar -</option>');
          for (var i = 0; i < response.length; i++)
            $('#cbo-SeriesDocumento').append('<option value="' + response[i].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[i].ID_Serie_Documento_PK + '>' + response[i].ID_Serie_Documento + '</option>');
        } else
          $('#cbo-SeriesDocumento').html('<option value="0" selected="selected">Sin serie</option>');
      }, 'JSON');

      var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
      var $Ss_SubTotal = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var iCantDescuento = 0;
      var globalImpuesto = 0;
      var $Ss_Descuento_p = 0;
      $("#table-DetalleProductos > tbody > tr").each(function () {
        var rows = $(this);
        var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
        var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
        var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
        var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
        var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

        $Ss_Total += $Ss_Total_Producto;

        if (iGrupoImpuesto == 1) {
          $Ss_SubTotal += $Ss_SubTotal_Producto;
          $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          globalImpuesto = fImpuesto;
        } else if (iGrupoImpuesto == 2) {
          $Ss_Inafecto += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else if (iGrupoImpuesto == 3) {
          $Ss_Exonerada += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else {
          $Ss_Gratuita += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        }

        if (isNaN($Ss_Descuento_Producto))
          $Ss_Descuento_Producto = 0;

        $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto))) / 100);
      });

      $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
      if ($Ss_Descuento > 0.00) {
        var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
        if ($Ss_SubTotal > 0.00) {
          $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
          $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
          $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
          $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
        }

        if ($Ss_Inafecto > 0.00) {
          $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
          $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
          $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
        }

        if ($Ss_Exonerada > 0.00) {
          $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
          $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
          $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
        }

        if ($Ss_Gratuita > 0.00) {
          $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
          $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Exonerada;
          $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
        }

        $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
        $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
      } else
        $Ss_Descuento = $Ss_Descuento_p;

      if (isNaN($Ss_Descuento))
        $Ss_Descuento = 0.00;

      $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
      $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

      $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
      $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

      $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
      $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

      $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
      $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

      $('#txt-impuesto').val($Ss_IGV.toFixed(2));
      $('#span-impuesto').text($Ss_IGV.toFixed(2));

      $('#txt-descuento').val($Ss_Descuento.toFixed(2));
      $('#span-descuento').text($Ss_Descuento.toFixed(2));

      $('#txt-total').val($Ss_Total.toFixed(2));
      $('#span-total').text($Ss_Total.toFixed(2));
    }
  })

  $('#panel-DetalleProductos').show();

  url = base_url + 'HelperController/getMonedas';
  $.post(url, function (response) {
    $('#cbo-Monedas').html('');
    $('.span-signo').text(response[0]['No_Signo']);
    for (var i = 0; i < response.length; i++)
      $('#cbo-Monedas').append('<option value="' + response[i]['ID_Moneda'] + '" data-no_signo="' + response[i]['No_Signo'] + '">' + response[i]['No_Moneda'] + '</option>');
  }, 'JSON');

  $('.div-stock').hide();
  /*
  $('#cbo-descargar_stock').html('<option value="1">Si</option>');
  $('#cbo-descargar_stock').append('<option value="0">No</option>');
  */
  
  $('#cbo-descargar_stock').html('<option value="0">No</option>');
  $('#cbo-descargar_stock').append('<option value="1">Si</option>');

  $('#cbo-almacen').show();

  var arrParams = {};
  getAlmacenes(arrParams);

  url = base_url + 'HelperController/getTipoMovimiento';
  $.post(url, { Nu_Tipo_Movimiento: 1 }, function (response) {
    $('#cbo-tipo_movimiento').html('<option value="" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-tipo_movimiento').append('<option value="' + response[i]['ID_Tipo_Movimiento'] + '">' + response[i]['No_Tipo_Movimiento'] + '</option>');
  }, 'JSON');

  url = base_url + 'HelperController/getValoresTablaDato';
  $.post(url, { sTipoData: 'Motivo_Traslado' }, function (response) {
    $('#cbo-motivo_traslado').html('<option value="0" selected="selected">- Sin datos -</option>');
    if (response.sStatus == 'success') {
      $('#cbo-motivo_traslado').html('<option value="" selected="selected">- Seleccionar -</option>');
      var response = response.arrData;
      for (var i = 0; i < response.length; i++)
        $('#cbo-motivo_traslado').append('<option value="' + response[i].ID_Tabla_Dato + '">' + response[i].No_Descripcion + '</option>');
    }
  }, 'JSON');

  $('#table-DetalleProductos').hide();

  url = base_url + 'HelperController/getImpuestos';
  $.post(url, function (response) {
    arrImpuestosProducto = '';
    arrImpuestosProductoDetalle = '';
    for (var i = 0; i < response.length; i++)
      arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
    arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';
    $('#modal-loader').modal('hide');
  }, 'JSON');

  var _ID_Producto = '';
  var option_impuesto_producto = '';

  url = base_url + 'HelperController/getTiposDocumentos';
  $.post(url, { Nu_Tipo_Filtro: 13 }, function (response) {
    arrAreaIngreso = '';
    arrAreaIngresoDetalle = '';
    for (var i = 0; i < response.length; i++)
      arrAreaIngresoDetalle += '{"ID_Tipo_Documento" : "' + response[i].ID_Tipo_Documento + '", "No_Tipo_Documento_Breve":"' + response[i].No_Tipo_Documento_Breve + '"},';
    arrAreaIngreso = '{ "arrAreaIngreso" : [' + arrAreaIngresoDetalle.slice(0, -1) + ']}';
  }, 'JSON');

  $('#cbo-tipo_mantenimiento').html('<option value="1">Correctivo</option>');
  $('#cbo-tipo_mantenimiento').append('<option value="2">Preventivo</option>');
}

function verCompra(ID, Nu_Documento_Identidad) {
  accion = 'upd_factura_compra';
  $('#modal-loader').modal('show');

  $('.div-Listar').hide();

  $('#form-Compra')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('#radio-ActiveFlete').prop('checked', true);
  $('#radio-InActiveFlete').prop('checked', false);

  $('#cbo-OrganizacionesVenta').prop('disabled', true);

  $('#radio-cliente_existente').prop('checked', true).iCheck('update');
  $('#radio-cliente_nuevo').prop('checked', false).iCheck('update');
  $('.div-cliente_existente').show();
  $('.div-cliente_nuevo').hide();

  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post(url, function (response) {
    $('#cbo-TiposDocumentoIdentidadCliente').html('');
    for (var i = 0; i < response.length; i++)
      $('#cbo-TiposDocumentoIdentidadCliente').append('<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>');
  }, 'JSON');

  $('#panel-DetalleProductos').removeClass('panel-danger');
  $('#panel-DetalleProductos').addClass('panel-default');

  $('#txt-subTotal').val(value_importes_cero);
  $('#span-subTotal').text(texto_importes_cero);

  $('#txt-inafecto').val(value_importes_cero);
  $('#span-inafecto').text(texto_importes_cero);

  $('#txt-exonerada').val(value_importes_cero);
  $('#span-exonerada').text(texto_importes_cero);

  $('#txt-gratuita').val(value_importes_cero);
  $('#span-gratuita').text(texto_importes_cero);

  $('#txt-impuesto').val(value_importes_cero);
  $('#span-impuesto').text(texto_importes_cero);

  $('#txt-descuento').val(value_importes_cero);
  $('#span-descuento').text(texto_importes_cero);

  $('#txt-total').val(value_importes_cero);
  $('#span-total').text(texto_importes_cero);

  $('#btn-save').attr('disabled', false);

  considerar_igv = 0;

  $('#cbo-TiposDocumento').change(function () {
    if ($(this).val() > 0) {
      considerar_igv = $(this).find(':selected').data('nu_impuesto');
      nu_enlace = $(this).find(':selected').data('nu_enlace');
      var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
      var $Ss_SubTotal = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var iCantDescuento = 0;
      var globalImpuesto = 0;
      var $Ss_Descuento_p = 0;
      $("#table-DetalleProductos > tbody > tr").each(function () {
        var rows = $(this);
        var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
        var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
        var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / fImpuesto);
        var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
        var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

        $Ss_Total += $Ss_Total_Producto;

        if (iGrupoImpuesto == 1) {
          $Ss_SubTotal += $Ss_SubTotal_Producto;
          $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          globalImpuesto = fImpuesto;
        } else if (iGrupoImpuesto == 2) {
          $Ss_Inafecto += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else if (iGrupoImpuesto == 3) {
          $Ss_Exonerada += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else {
          $Ss_Gratuita += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        }

        if (isNaN($Ss_Descuento_Producto))
          $Ss_Descuento_Producto = 0;

        $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto))) / 100);
      });

      if ($Ss_Descuento > 0.00) {
        var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita;
        if ($Ss_SubTotal > 0.00) {
          $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
          $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
          $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
          $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
        }

        if ($Ss_Inafecto > 0.00) {
          $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
          $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
          $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
        }

        if ($Ss_Exonerada > 0.00) {
          $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
          $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
          $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
        }

        if ($Ss_Gratuita > 0.00) {
          $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
          $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
          $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
        }

        $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
        $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
      } else
        $Ss_Descuento = $Ss_Descuento_p;

      if (isNaN($Ss_Descuento))
        $Ss_Descuento = 0.00;

      $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
      $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

      $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
      $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

      $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
      $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

      $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
      $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

      $('#txt-impuesto').val($Ss_IGV.toFixed(2));
      $('#span-impuesto').text($Ss_IGV.toFixed(2));

      $('#txt-descuento').val($Ss_Descuento.toFixed(2));
      $('#span-descuento').text($Ss_Descuento.toFixed(2));

      $('#txt-total').val($Ss_Total.toFixed(2));
      $('#span-total').text($Ss_Total.toFixed(2));
    }
  })

  url = base_url + 'Logistica/SalidaInventarioController/ajax_edit/' + ID;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      $('.div-AgregarEditar').show();

      $('.title_Compra').text('Modifcar Compra');

      $('[name="EID_Empresa"]').val(response.arrEdit[0].ID_Empresa);
      $('[name="EID_Guia_Cabecera"]').val(response.arrEdit[0].ID_Guia_Cabecera);

      $('#cbo-tipo_mantenimiento').html('');
      var selected = '';
      for (var i = 1; i < 3; i++) {
        selected = '';
        if (response.arrEdit[0].Nu_Tipo_Mantenimiento == i)
          selected = 'selected="selected"';
        $('#cbo-tipo_mantenimiento').append('<option value="' + i + '" ' + selected + '>' + (i == 1 ? 'Correctivo' : 'Preventivo') + '</option>');
      }

      //Datos Documento
      considerar_igv = response.arrEdit[0].Nu_Impuesto;

      var arrParams = { ID_Almacen: response.arrEdit[0].ID_Almacen }
      getAlmacenes(arrParams);

      nu_enlace = response.arrEdit[0].Nu_Enlace;
      url = base_url + 'HelperController/getTiposDocumentos';
      $.post(url, { Nu_Tipo_Filtro: 7 }, function (responseTiposDocumento) {//2 = Compra
        $('#cbo-TiposDocumento').html('');
        for (var i = 0; i < responseTiposDocumento.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Tipo_Documento == responseTiposDocumento[i]['ID_Tipo_Documento'])
            selected = 'selected="selected"';
          $('#cbo-TiposDocumento').append('<option value="' + responseTiposDocumento[i]['ID_Tipo_Documento'] + '" data-nu_impuesto="' + responseTiposDocumento[i]['Nu_Impuesto'] + '" data-nu_enlace="' + responseTiposDocumento[i]['Nu_Enlace'] + '" ' + selected + '>' + responseTiposDocumento[i]['No_Tipo_Documento_Breve'] + '</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getSeriesDocumento';
      $.post(url, { ID_Organizacion: response.arrEdit[0].ID_Organizacion, ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento }, function (responseSeriesDocumento) {
        $('#cbo-SeriesDocumento').html('');
        for (var i = 0; i < responseSeriesDocumento.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Serie_Documento == responseSeriesDocumento[i]['ID_Serie_Documento'])
            selected = 'selected="selected"';
          $('#cbo-SeriesDocumento').append('<option value="' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '" ' + selected + ' data-id_serie_documento_pk=' + responseSeriesDocumento[i].ID_Serie_Documento_PK + '>' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '</option>');
        }
      }, 'JSON');

      $('[name="ID_Numero_Documento"]').val(response.arrEdit[0].ID_Numero_Documento);

      $('[name="Fe_Emision"]').val(ParseDateString(response.arrEdit[0].Fe_Emision, 6, '-'));

      url = base_url + 'HelperController/getMonedas';
      $.post(url, function (responseMonedas) {
        $('#cbo-Monedas').html('');
        for (var i = 0; i < responseMonedas.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Moneda == responseMonedas[i]['ID_Moneda']) {
            selected = 'selected="selected"';
            $('.span-signo').text(responseMonedas[i]['No_Signo']);
          }
          $('#cbo-Monedas').append('<option value="' + responseMonedas[i]['ID_Moneda'] + '" data-no_signo="' + responseMonedas[i]['No_Signo'] + '" ' + selected + '>' + responseMonedas[i]['No_Moneda'] + '</option>');
        }
      }, 'JSON');

      $('[name="ENu_Estado"]').val(response.arrEdit[0].Nu_Estado);
      $('.div-almacen').show();
      $('.div-stock').show();
      if (response.arrEdit[0].Nu_Estado == 12) {//pendiente porque viene de presuepuedo el vale generado para salida de inventario
        $('.div-almacen').hide();
        $('.div-stock').hide();
      }

      if (response.arrEdit[0].Nu_Descargar_Inventario == 1) {
        $('#cbo-descargar_stock').html('<option value="1" selected>Si</option>');
        $('#cbo-descargar_stock').append('<option value="0">No</option>');
      } else {
        $('#cbo-descargar_stock').html('<option value="1">Si</option>');
        $('#cbo-descargar_stock').append('<option value="0" selected>No</option>');
      }

      url = base_url + 'HelperController/getTipoMovimiento';
      $.post(url, { Nu_Tipo_Movimiento: 1 }, function (responseTiposMovimiento) {
        for (var i = 0; i < responseTiposMovimiento.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Tipo_Movimiento == responseTiposMovimiento[i]['ID_Tipo_Movimiento'])
            selected = 'selected="selected"';
          $('#cbo-tipo_movimiento').append('<option value="' + responseTiposMovimiento[i]['ID_Tipo_Movimiento'] + '" ' + selected + '>' + responseTiposMovimiento[i]['No_Tipo_Movimiento'] + '</option>');
        }
      }, 'JSON');

      //Datos Cliente
      $('[name="AID"]').val(response.arrEdit[0].ID_Entidad);
      $('[name="ANombre"]').val(response.arrEdit[0].No_Entidad);
      $('[name="ACodigo"]').val(response.arrEdit[0].Nu_Documento_Identidad);
      $('[name="Txt_Direccion_Entidad"]').val(response.arrEdit[0].Txt_Direccion_Entidad);

      // Flete
      $('#div-addFlete').hide();

      $('#radio-flete_si').prop('checked', false).iCheck('update');
      $('#radio-flete_no').prop('checked', true).iCheck('update');
      if (response.arrEdit[0].ID_Entidad_Transportista != '' && response.arrEdit[0].ID_Entidad_Transportista != null) {
        $('#div-addFlete').show();

        $('#radio-flete_si').prop('checked', true).iCheck('update');
        $('#radio-flete_no').prop('checked', false).iCheck('update');

        $('[name="AID_Transportista"]').val(response.arrEdit[0].ID_Entidad_Transportista);
        $('[name="ANombre_Transportista"]').val(response.arrEdit[0].No_Entidad_Transportista);
        $('[name="ACodigo_Transportista"]').val(response.arrEdit[0].Nu_Documento_Identidad_Transportista);

        $('[name="No_Placa"]').val(response.arrEdit[0].No_Placa);
        $('[name="Fe_Traslado"]').val(ParseDateString(response.arrEdit[0].Fe_Traslado, 6, '-'));

        url = base_url + 'HelperController/getMotivosTraslado';
        $.post(url, function (responseMT) {
          $('#cbo-motivo_traslado').html('');
          for (var i = 0; i < responseMT.length; i++) {
            selected = '';
            if (response.arrEdit[0].ID_Motivo_Traslado == responseMT[i].Nu_Valor)
              selected = 'selected="selected"';
            $('#cbo-motivo_traslado').append('<option value="' + responseMT[i].Nu_Valor + '" ' + selected + '>' + responseMT[i].No_Descripcion + '</option>');
          }
        }, 'JSON');

        $('[name="No_Licencia"]').val(response.arrEdit[0].No_Licencia);
        $('[name="No_Certificado_Inscripcion"]').val(response.arrEdit[0].No_Certificado_Inscripcion);
      }

      //Detalle
      $('#cbo-lista_precios').html('');
      url = base_url + 'HelperController/getListaPrecio';
      $.post(url, { Nu_Tipo_Lista_Precio: $('[name="Nu_Tipo_Lista_Precio"]').val(), ID_Organizacion: response.arrEdit[0].ID_Organizacion, ID_Almacen: response.arrEdit[0].ID_Almacen }, function (responseLista) {
        for (var i = 0; i < responseLista.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Lista_Precio_Cabecera == responseLista[i].ID_Lista_Precio_Cabecera)
            selected = 'selected="selected"';
          $('#cbo-lista_precios').append('<option value="' + responseLista[i].ID_Lista_Precio_Cabecera + '" ' + selected + '>' + responseLista[i].No_Lista_Precio + '</option>');
        }
      }, 'JSON');

      $('[name="Txt_Glosa"]').val(response.arrEdit[0].Txt_Glosa);

      $('#table-DetalleProductos').show();
      $('#table-DetalleProductos tbody').empty();

      var table_detalle_producto = '';
      var _ID_Producto = '';
      var $Ss_SubTotal_Producto = 0.00;
      var $Ss_IGV_Producto = 0.00;
      var $Ss_Descuento_Producto = 0.00;
      var $Ss_Total_Producto = 0.00;
      var $Ss_Gravada = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var option_impuesto_producto = '';

      var $fDescuento_Producto = 0;
      var fDescuento_Total_Producto = 0;
      var globalImpuesto = 0;
      var $iDescuentoGravada = 0;
      var $iDescuentoExonerada = 0;
      var $iDescuentoInafecto = 0;
      var $iDescuentoGratuita = 0;
      var $iDescuentoGlobalImpuesto = 0;
      var selected;

      var iTotalRegistros = response.arrEdit.length;
      var iTotalRegistrosImpuestos = response.arrImpuesto.length;
      var iTotalRegistrosAreasIngreso = response.arrAreaIngreso.length;
      
      for (var i = 0; i < response.arrEdit.length; i++) {
        if (_ID_Producto != response.arrEdit[i].ID_Producto) {
          _ID_Producto = response.arrEdit[i].ID_Producto;
          option_impuesto_producto = '';
          option_impuesto_area_ingreso = '';
        }

        $Ss_SubTotal_Producto = parseFloat(response.arrEdit[i].Ss_SubTotal_Producto);
        if (response.arrEdit[i].Nu_Tipo_Impuesto == 1) {
          $Ss_IGV += parseFloat(response.arrEdit[i].Ss_Impuesto_Producto);
          $Ss_Gravada += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 2) {
          $Ss_Inafecto += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 3) {
          $Ss_Exonerada += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 4) {
          $Ss_Gratuita += $Ss_SubTotal_Producto;
        }

        $Ss_Descuento_Producto += parseFloat(response.arrEdit[i].Ss_Descuento_Producto);
        $Ss_Total += parseFloat(response.arrEdit[i].Ss_Total_Producto);

        var sClassDisabled = '';
        if (response.arrEdit[0].Nu_Estado == 13) {//pendiente porque viene de presuepuedo el vale generado para salida de inventario
          sClassDisabled = "disabled";
        }

        for (var x = 0; x < iTotalRegistrosImpuestos; x++) {
          selected = '';
          if (response.arrImpuesto[x].ID_Impuesto_Cruce_Documento == response.arrEdit[i].ID_Impuesto_Cruce_Documento)
            selected = 'selected="selected"';
          option_impuesto_producto += "<option value='" + response.arrImpuesto[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + response.arrImpuesto[x].Nu_Tipo_Impuesto + "' data-impuesto_producto='" + response.arrImpuesto[x].Ss_Impuesto + "' " + selected + ">" + response.arrImpuesto[x].No_Impuesto + "</option>";
        }

        for (var x = 0; x < iTotalRegistrosAreasIngreso; x++) {
          selected = '';
          if (response.arrAreaIngreso[x].ID_Tipo_Documento == response.arrEdit[i].ID_Area_Ingreso)
            selected = 'selected="selected"';
          option_impuesto_area_ingreso += "<option value='" + response.arrAreaIngreso[x].ID_Tipo_Documento + "' " + selected + ">" + response.arrAreaIngreso[x].No_Tipo_Documento_Breve + "</option>";
        }

        table_detalle_producto +=
          "<tr id='tr_detalle_producto" + response.arrEdit[i].ID_Producto + "'>"
          + "<td style='display:none;' class='text-left td-iIdItem'>" + response.arrEdit[i].ID_Producto + "</td>"
          + "<td class='text-right'><input type='text' class='pos-input txt-Qt_Producto form-control input-decimal' " + (response.arrEdit[i].Nu_Tipo_Producto == 1 ? 'onkeyup=validateStockNow(event);' : '') + " data-id_item='" + response.arrEdit[i].ID_Producto + "' data-id_producto='" + response.arrEdit[i].ID_Producto + "' value='" + response.arrEdit[i].Qt_Producto + "' " + sClassDisabled + " autocomplete='off'></td>"
          + "<td class='text-left'>" + response.arrEdit[i].No_Producto + "</td>"
          + "<td class='text-right'><input type='tel' class='pos-input txt-Ss_Precio form-control input-decimal' value='" + response.arrEdit[i].Ss_Precio + "' autocomplete='off'></td>"
          + "<td class='text-right'>"
          + "<select class='cbo-ImpuestosProducto form-control required' style='width: 100%;'>"
          + option_impuesto_producto
          + "</select>"
          + "</td>"
          + "<td style='display:none;' class='text-right'><input type='tel' class='pos-input txt-Ss_SubTotal_Producto form-control' value='" + response.arrEdit[i].Ss_SubTotal_Producto + "' autocomplete='off' disabled></td>"
          + "<td class='text-right'><input type='tel' class='pos-input txt-Ss_Descuento form-control input-decimal' value='" + (response.arrEdit[i].Po_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Po_Descuento_Impuesto_Producto) + "' autocomplete='off'></td>"
          + "<td class='text-right'><input type='tel' class='pos-input txt-Ss_Total_Producto form-control input-decimal' value='" + response.arrEdit[i].Ss_Total_Producto + "' autocomplete='off'></td>"
          + "<td class='text-right'><input type='text' class='pos-input txt-Nu_Lote_Vencimiento form-control input-codigo_barra' placeholder='Opcional' value='" + (response.arrEdit[i].Nu_Lote_Vencimiento != null ? response.arrEdit[i].Nu_Lote_Vencimiento : '') + "' autocomplete='off'></td>"
          + "<td class='text-right'><input type='text' class='pos-input txt-Fe_Lote_Vencimiento form-control date-picker-invoice' placeholder='Opcional' value='" + (response.arrEdit[i].Fe_Lote_Vencimiento != null ? ParseDateString(response.arrEdit[i].Fe_Lote_Vencimiento, 6, '-') : '') + "' autocomplete='off'></td>"
          + "<td class='text-right'>"
          + "<select class='cbo-AreaIngreso form-control required' style='width: 100%;'>"
          + option_impuesto_area_ingreso
          + "</select>"
          + "</td>"
          + "<td class='text-right'>"
          + "<select class='cbo-Activo form-control required' style='width: 100%;'>"
          + "<option value='1' " + ((response.arrEdit[i].Nu_Estado_Depreciacion == 1) ? 'selected="selected"' : '') + ">Activo</option>"
          + "<option value='0' " + ((response.arrEdit[i].Nu_Estado_Depreciacion == 0) ? 'selected="selected"' : '') + ">Baja</option>"
          + "</select>"
          + "</td>"
          + "<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Producto) + "</td>"
          + "<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Impuesto_Producto) + "</td>"
          + "<td class='text-center'><button type='button' id='btn-deleteProducto' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-2x' aria-hidden='true'> </i></button></td>"
          + "</tr>";
      }

      $('#table-DetalleProductos > tbody').append(table_detalle_producto);

      $('.txt-Fe_Lote_Vencimiento').datepicker({
        autoclose: true,
        startDate: new Date(fYear, fToday.getMonth(), fDay),
        todayHighlight: true
      })

      $('#txt-subTotal').val($Ss_Gravada.toFixed(2));
      $('#span-subTotal').text($Ss_Gravada.toFixed(2));

      $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
      $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

      $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
      $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

      $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
      $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

      if (parseFloat(response.arrEdit[0].Ss_Descuento) > 0 && $Ss_Descuento_Producto == 0)
        $('#txt-Ss_Descuento').val(response.arrEdit[0].Po_Descuento);
      else
        $('#txt-Ss_Descuento').val('');

      $('#txt-descuento').val(response.arrEdit[0].Ss_Descuento);
      $('#span-descuento').text(response.arrEdit[0].Ss_Descuento);

      $('#txt-impuesto').val($Ss_IGV.toFixed(2));
      $('#span-impuesto').text($Ss_IGV.toFixed(2));

      $('#txt-total').val($Ss_Total.toFixed(2));
      $('#span-total').text($Ss_Total.toFixed(2));

      validateDecimal();
      validateNumber();
      validateNumberOperation();
      validateCodigoBarra();

      url = base_url + 'HelperController/getImpuestos';
      $.post(url, function (response) {
        arrImpuestosProducto = '';
        arrImpuestosProductoDetalle = '';
        for (var i = 0; i < response.length; i++)
          arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
        arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';

        $('#modal-loader').modal('hide');
      }, 'JSON');

      url = base_url + 'HelperController/getTiposDocumentos';
      $.post(url, { Nu_Tipo_Filtro: 13 }, function (response) {
        arrAreaIngreso = '';
        arrAreaIngresoDetalle = '';
        for (var i = 0; i < response.length; i++)
          arrAreaIngresoDetalle += '{"ID_Tipo_Documento" : "' + response[i].ID_Tipo_Documento + '", "No_Tipo_Documento_Breve":"' + response[i].No_Tipo_Documento_Breve + '"},';
        arrAreaIngreso = '{ "arrAreaIngreso" : [' + arrAreaIngresoDetalle.slice(0, -1) + ']}';
      }, 'JSON');

      var _ID_Producto = '';
      var option_impuesto_producto = '';
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $('#modal-loader').modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');

      $('#modal-message').modal('show');
      $('.modal-message').addClass('modal-danger');
      $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
      setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

      //Message for developer
      console.log(jqXHR.responseText);
    }
  })
}

function anularCompra(ID, Nu_Enlace, Nu_Descargar_Inventario, accion) {
  var $modal_delete = $('.modal-message-delete');
  $modal_delete.modal('show');

  $('.modal-message-delete').removeClass('modal-danger modal-warning modal-success');
  $('.modal-message-delete').addClass('modal-warning');

  $('.modal-title-message-delete').text('¿Deseas anular el documento?');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $(document).bind('keydown', 'alt+k', function () {
    if (accion == 'anular') {
      _anularCompra($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario);
      accion = '';
    }
  });

  $('#btn-save-delete').off('click').click(function () {
    _anularCompra($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario);
  });
}

function eliminarCompra(ID, Nu_Enlace, Nu_Descargar_Inventario, accion) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $(document).bind('keydown', 'alt+l', function () {
    if (accion == 'delete') {
      _eliminarCompra($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario);
      accion = '';
    }
  });

  $('#btn-save-delete').off('click').click(function () {
    _eliminarCompra($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario);
  });
}

$(function () {
  $('[data-mask]').inputmask();

  $('.select2').select2();
  $('#radio-cliente_existente').on('ifChecked', function () {
    $('.div-cliente_existente').show();
    $('.div-cliente_nuevo').hide();
  })

  $('#radio-cliente_nuevo').on('ifChecked', function () {
    $('.div-cliente_existente').hide();
    $('.div-cliente_nuevo').show();
  })

  $(".div-flete").click(function () {
    $('#div-addFlete').show();

    $('#radio-flete_si').prop('checked', true).iCheck('update');
    $('#radio-flete_no').prop('checked', false).iCheck('update');
    if ($(this).data('estado') == 0) {
      $('#div-addFlete').hide();

      $('#radio-flete_si').prop('checked', false).iCheck('update');
      $('#radio-flete_no').prop('checked', true).iCheck('update');
    }
  });

  $('#radio-flete_si').on('ifChecked', function () {
    $('#div-addFlete').show();
  })

  $('#radio-flete_no').on('ifChecked', function () {
    $('#div-addFlete').hide();
  })

  //Global Autocomplete
  $('.autocompletar_transportista').autoComplete({
    minChars: 0,
    source: function (term, response) {
      var term = term.toLowerCase();
      var global_class_method = $('.autocompletar').data('global-class_method');
      var global_table = $('.autocompletar').data('global-table');

      var filter_id_codigo = '';
      if ($('#txt-EID_Producto').val() !== undefined)
        filter_id_codigo = $('#txt-EID_Producto').val();

      $.post(base_url + global_class_method, { global_table: global_table, global_search: term, filter_id_codigo: filter_id_codigo }, function (arrData) {
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search) {
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      var data_direccion = '';
      if ($('#txt-Txt_Direccion_Entidad').val() != undefined)
        data_direccion = 'data-direccion_cliente="' + item.Txt_Direccion_Entidad + '"';
      var data_telefono = '';
      if ($('#txt-Nu_Telefono_Entidad_Cliente').val() != undefined)
        data_telefono = 'data-telefono="' + item.Nu_Telefono_Entidad + '"';
      var data_celular = '';
      if ($('#txt-Nu_Celular_Entidad_Cliente').val() != undefined)
        data_celular = 'data-celular="' + item.Nu_Celular_Entidad + '"';
      var data_email = '';
      if ($('#txt-Txt_Email_Entidad_Cliente').val() != undefined)
        data_email = 'data-email="' + item.Txt_Email_Entidad + '"';
      var data_dias_credito = '';
      if ($('#txt-Fe_Vencimiento').val() != undefined && ($('#cbo-MediosPago').val() != undefined && $('#cbo-MediosPago').find(':selected').data('nu_tipo') == 1))
        data_dias_credito = 'data-dias_credito="' + item.Nu_Dias_Credito + '"';
      return '<div class="autocomplete-suggestion" data-id="' + item.ID + '" data-codigo="' + item.Codigo + '" data-nombre="' + item.Nombre + '" data-estado="' + item.Nu_Estado + '" data-val="' + search + '" ' + data_direccion + ' ' + data_telefono + ' ' + data_celular + ' ' + data_email + ' ' + data_dias_credito + '>' + item.Nombre.replace(re, "<b>$1</b>") + '</div>';
    },
    onSelect: function (e, term, item) {
      $('#txt-AID_Transportista').val(item.data('id'));
      $('#txt-ACodigo_Transportista').val(item.data('codigo'));
      $('#txt-ANombre_Transportista').val(item.data('nombre'));
      $('#txt-ACodigo_Transportista').closest('.form-group').find('.help-block').html('');
      $('#txt-ACodigo_Transportista').closest('.form-group').removeClass('has-error');
    }
  });

  //LAE API SUNAT / RENIEC - CLIENTE
  $('#btn-cloud-api_compra_cliente').click(function () {
    if ($('#cbo-TiposDocumentoIdentidadCliente').val().length === 0) {
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').find('.help-block').html('Seleccionar tipo doc. identidad');
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') != $('#txt-Nu_Documento_Identidad_Cliente').val().length) {
      $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').find('.help-block').html('Debe ingresar ' + $('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') + ' dígitos');
      $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if (
      (
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 1 ||
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 3 ||
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 5 ||
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 6
      )
    ) {
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').find('.help-block').html('Disponible DNI / RUC');
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $('#btn-cloud-api_compra_cliente').text('');
      $('#btn-cloud-api_compra_cliente').attr('disabled', true);
      $('#btn-cloud-api_compra_cliente').append('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      // Obtener datos de SUNAT y RENIEC
      var url_api = 'https://www.laesystems.com/librerias/sunat/partner/format/json/x-api-key/';
      if ($('#cbo-TiposDocumentoIdentidadCliente').val() == 2)//2=RENIEC
        url_api = 'https://www.laesystems.com/librerias/reniec/partner/format/json/x-api-key/';
      url_api = url_api + sTokenGlobal;

      var data = {
        ID_Tipo_Documento_Identidad: $('#cbo-TiposDocumentoIdentidadCliente').val(),
        Nu_Documento_Identidad: $('#txt-Nu_Documento_Identidad_Cliente').val(),
      };

      $.ajax({
        url: url_api,
        type: 'POST',
        data: data,
        success: function (response) {
          $('#btn-cloud-api_compra_cliente').closest('.form-group').find('.help-block').html('');
          $('#btn-cloud-api_compra_cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

          if (response.success == true) {
            $('#txt-No_Entidad_Cliente').val(response.data.No_Names);
            if ($('#cbo-TiposDocumentoIdentidadCliente').val() == 4) {//RUC
              $('#txt-Txt_Direccion_Entidad_Cliente').val(response.data.Txt_Address);
              $('#txt-Nu_Telefono_Entidad_Cliente').val(response.data.Nu_Phone);
              $('#txt-Nu_Celular_Entidad_Cliente').val(response.data.Nu_Cellphone);
              if (response.data.Nu_Status == 1)
                $("div.estado select").val("1");
              else
                $("div.estado select").val("0");
            }
          } else {
            $('#txt-No_Entidad_Cliente').val('');
            if ($('#cbo-TiposDocumentoIdentidadCliente').val() == 4) {//RUC
              $('#txt-Txt_Direccion_Entidad_Cliente').val('');
              $('#txt-Nu_Telefono_Entidad_Cliente').val('');
              $('#txt-Nu_Celular_Entidad_Cliente').val('');
            }
            $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').find('.help-block').html(response.msg);
            $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

            $('#txt-Nu_Documento_Identidad_Cliente').focus();
            $('#txt-Nu_Documento_Identidad_Cliente').select();
          }

          $('#btn-cloud-api_compra_cliente').text('');
          $('#btn-cloud-api_compra_cliente').attr('disabled', false);
          $('#btn-cloud-api_compra_cliente').append('<i class="fa fa-cloud-download fa-lg"></i>');
        },
        error: function (response) {
          $('#btn-cloud-api_compra_cliente').closest('.form-group').find('.help-block').html('Sin acceso');
          $('#btn-cloud-api_compra_cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

          $('#txt-No_Entidad_Cliente').val('');
          $('#txt-Txt_Direccion_Entidad_Cliente').val('');
          $('#txt-Nu_Telefono_Entidad_Cliente').val('');
          $('#txt-Nu_Celular_Entidad_Cliente').val('');

          $('#btn-cloud-api_compra_cliente').text('');
          $('#btn-cloud-api_compra_cliente').attr('disabled', false);
          $('#btn-cloud-api_compra_cliente').append('<i class="fa fa-cloud-download fa-lg"></i>');
        }
      });
    }
  })

  /* Tipo Documento Identidad Cliente */
  $('#cbo-TiposDocumentoIdentidadCliente').change(function () {
    if ($(this).val() == 2) {//DNI
      $('#label-Nombre_Documento_Identidad_Cliente').text('DNI');
      $('#label-No_Entidad_Proveeedor').text('Nombre(s) y Apellidos');
      $('#txt-Nu_Documento_Identidad_Cliente').attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
    } else if ($(this).val() == 4) {//RUC
      $('#label-Nombre_Documento_Identidad_Cliente').text('RUC');
      $('#label-No_Entidad_Proveeedor').text('Razón Social');
      $('#txt-Nu_Documento_Identidad_Cliente').attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
    } else {
      $('#label-Nombre_Documento_Identidad_Cliente').text('# Documento Identidad');
      $('#label-No_Entidad_Proveeedor').text('Nombre(s) y Apellidos');
      $('#txt-Nu_Documento_Identidad_Cliente').attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
    }
  })

  url = base_url + 'HelperController/getTiposDocumentos';
  $.post(url, { Nu_Tipo_Filtro: 7 }, function (response) {//2 = Compra
    $('#cbo-Filtro_TiposDocumento').html('<option value="0" selected="selected">Todos</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-Filtro_TiposDocumento').append('<option value="' + response[i]['ID_Tipo_Documento'] + '">' + response[i]['No_Tipo_Documento_Breve'] + '</option>');
  }, 'JSON');

  $('#cbo-Filtro_SeriesDocumento').html('<option value="0" selected="selected">Todos</option>');
  $('#cbo-Filtro_TiposDocumento').change(function () {
    $('#cbo-Filtro_SeriesDocumento').html('<option value="0" selected="selected">Todos</option>');
    if ($(this).val() > 0) {
      url = base_url + 'HelperController/getSeriesDocumento';
      $.post(url, { ID_Organizacion: $('#cbo-filtro-organizacion').val(), ID_Tipo_Documento: $(this).val() }, function (response) {
        $('#cbo-Filtro_SeriesDocumento').html('<option value="0" selected="selected">- Todos -</option>');
        if (response.length > 0) {
          for (var i = 0; i < response.length; i++)
            $('#cbo-Filtro_SeriesDocumento').append('<option value="' + response[i].ID_Serie_Documento + '">' + response[i].ID_Serie_Documento + '</option>');
        }
      }, 'JSON');
    }
  })

  url = base_url + 'Logistica/SalidaInventarioController/ajax_list';
  table_compra = $('#table-Compra').DataTable({
    'dom': 'B<"top">frt<"bottom"lp><"clear">',
    buttons: [{
      extend: 'excel',
      text: '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr: 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend: 'pdf',
      text: '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr: 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend: 'colvis',
      text: '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr: 'Columnas'
    }],
    'searching': false,
    'bStateSave': true,
    'processing': true,
    'serverSide': true,
    'info': true,
    'autoWidth': false,
    'pagingType': 'full_numbers',
    'oLanguage': {
      'sInfo': 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu': '_MENU_',
      'sSearch': 'Buscar por: ',
      'sSearchPlaceholder': 'UPC / Nombre',
      'sZeroRecords': 'No se encontraron registros',
      'sInfoEmpty': 'No hay registros',
      'sLoadingRecords': 'Cargando...',
      'sProcessing': 'Procesando...',
      'oPaginate': {
        'sFirst': '<<',
        'sLast': '>>',
        'sPrevious': '<',
        'sNext': '>',
      },
    },
    'order': [],
    'ajax': {
      'url': url,
      'type': 'POST',
      'dataType': 'json',
      'data': function (data) {
        data.Filtro_Fe_Inicio = ParseDateString($('#txt-Filtro_Fe_Inicio').val(), 1, '/'),
          data.Filtro_Fe_Fin = ParseDateString($('#txt-Filtro_Fe_Fin').val(), 1, '/'),
          data.Filtro_TiposDocumento = $('#cbo-Filtro_TiposDocumento').val(),
          data.Filtro_SerieDocumento = $('#txt-Filtro_SerieDocumento').val(),
          data.Filtro_NumeroDocumento = $('#txt-Filtro_NumeroDocumento').val(),
          data.Filtro_Estado = $('#cbo-Filtro_Estado').val(),
          data.Filtro_Entidad = $('#txt-Filtro_Entidad').val();
      },
    },
    'columnDefs': [{
      'className': 'text-center',
      'targets': 'no-sort',
      'orderable': false,
    },
    {
      'className': 'text-left',
      'targets': 'no-sort_left',
      'orderable': false,
    },
    {
      'className': 'text-right',
      'targets': 'no-sort_right',
      'orderable': false,
    },
    {
      'className': 'text-center',
      'targets': 'sort_center',
      'orderable': true,
    },
    {
      'className': 'text-right',
      'targets': 'sort_right',
      'orderable': true,
    },],
    'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
  });

  $('.dataTables_length').addClass('col-md-3');
  $('.dataTables_paginate').addClass('col-md-9');

  $('#btn-filter').click(function () {
    table_compra.ajax.reload();
  });

  $('#form-Compra').validate({
    rules: {
      ID_Tipo_Documento: {
        required: true,
      },
      ID_Serie_Documento: {
        required: true,
      },
      ID_Numero_Documento: {
        required: true,
      },
      Fe_Emision: {
        required: true,
      },
      ID_Tipo_Movimiento: {
        required: true,
      },
      ANombre: {
        required: true,
      },
      No_Entidad_Cliente: {
        required: true,
      },
      ANombre_Transportista: {
        required: true,
      },
      No_Placa: {
        required: true,
      },
      ID_Motivo_Traslado: {
        required: true,
      },
    },
    messages: {
      ID_Tipo_Documento: {
        required: "Seleccionar tipo",
      },
      ID_Serie_Documento: {
        required: "Ingresar serie",
        minlength: "Debe ingresar 3 dígitos",
        maxlength: "Debe ingresar 3 dígitos",
      },
      ID_Numero_Documento: {
        required: "Ingresar número",
        minlength: "Debe ingresar 4 dígitos",
        maxlength: "Debe ingresar 4 dígitos",
      },
      Fe_Emision: {
        required: "Ingresar F. Emisión",
      },
      ID_Tipo_Movimiento: {
        required: "Seleccionar movimiento",
      },
      ANombre: {
        required: "Ingresar cliente",
      },
      No_Entidad_Cliente: {
        required: "Ingresar cliente",
      },
      ANombre_Transportista: {
        required: "Ingresar transportista",
      },
      No_Placa: {
        required: "Ingresar Placa",
      },
      ID_Motivo_Traslado: {
        required: "Seleccionar motivo de traslado",
      },
    },
    errorPlacement: function (error, element) {
      $(element).closest('.form-group').find('.help-block').html(error.html());
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.form-group').find('.help-block').html('');
    },
    submitHandler: form_Compra
  });

	$( '#cbo-SeriesDocumento' ).change(function(){
	  $( '#txt-ID_Numero_Documento' ).val('');
	  if ( $(this).val() != '') {
		  url = base_url + 'HelperController/getNumeroDocumento';
      $.post( url, { ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Tipo_Documento: $( '#cbo-TiposDocumento' ).val(), ID_Serie_Documento: $(this).val() }, function( response ){
        if (response.length == 0)
          $( '#txt-ID_Numero_Documento' ).val('');
        else
          $( '#txt-ID_Numero_Documento' ).val(response.ID_Numero_Documento);
      }, 'JSON');
    }
	})
  
  $('#cbo-Monedas').change(function () {
    if ($(this).val() > 0)
      $('.span-signo').text($(this).find(':selected').data('no_signo'));
  })

  $('#cbo-almacen').change(function () {
    if ($(this).val() > 0) {
      var arrParams = {
        ID_Almacen: 0,
      };
      getListaPrecios(arrParams);
    }
  })

  $('#div-addFlete').hide();

  var _ID_Producto = '';
  var option_impuesto_producto = '';
  $('#btn-addProductoCompra').click(function () {
    var $ID_Producto = $('#txt-ID_Producto').val();
    var $Ss_Precio = parseFloat($('#txt-Ss_Precio').val());
    var $No_Producto = $('#txt-No_Producto').val();
    var $ID_Impuesto_Cruce_Documento = $('#txt-ID_Impuesto_Cruce_Documento').val();
    var $Nu_Tipo_Impuesto = $('#txt-Nu_Tipo_Impuesto').val();
    var $Ss_Impuesto = $('#txt-Ss_Impuesto').val();
    var $Ss_SubTotal_Producto = 0.00;
    var $Ss_Total_Producto = 0.00;
    var $iTipoItem = $( '#txt-nu_tipo_item' ).val()
    var $Qt_Producto = parseFloat($('#txt-Qt_Producto').val());
    
    bEstadoValidacion = validatePreviousDocumentToSavePurchase();

    if (bEstadoValidacion == true && $ID_Producto.length === 0 || $No_Producto.length === 0) {
      $('#txt-No_Producto').closest('.form-group').find('.help-block').html('Ingresar producto');
      $('#txt-No_Producto').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if (bEstadoValidacion) {
      
      if (iValidarStockGlobal == 1 && $iTipoItem == 1 && (parseFloat($Qt_Producto) <= 0.000000 || isNaN($Qt_Producto) == true)) {
        $modal_msg_stock = $( '.modal-message' );
        $modal_msg_stock.modal('show');
    
        $modal_msg_stock.removeClass('modal-danger modal-warning modal-success');
        $modal_msg_stock.addClass('modal-warning');
    
        $( '.modal-title-message' ).text('Sin stock disponible');
    
        setTimeout(function() {$modal_msg_stock.modal('hide');}, 1300);
      } else {
        if ($('[name="addCliente"]:checked').attr('value') == 1) {//Agregar cliente
          if ($('#cbo-Estado').val() == 1) {//1 = Activo
            generarTablaTemporalItems($ID_Producto, $No_Producto, $Ss_Precio, $ID_Impuesto_Cruce_Documento, $Nu_Tipo_Impuesto, $Ss_Impuesto, $iTipoItem);
          } else {
            $('#modal-message').modal('show');
            $('.modal-message').addClass('modal-danger');
            $('.modal-title-message').text('El cliente se encuentra con BAJA DE OFICIO / NO HABIDO');
            setTimeout(function () { $('#modal-message').modal('hide'); }, 2500);
          }
        } else {
          var arrPOST = {
            sTipoData: 'get_entidad',
            iIDEntidad: $('#txt-AID').val(),
            iTipoEntidad: 0,
          };
          url = base_url + 'HelperController/getDataGeneral';
          $.post(url, arrPOST, function (response) {
            $('.modal-message').removeClass('modal-danger modal-warning modal-success');
            if (response.sStatus == 'success') {// Si el RUC es válido
              if (response.arrData[0].Nu_Estado == '1') {
                generarTablaTemporalItems($ID_Producto, $No_Producto, $Ss_Precio, $ID_Impuesto_Cruce_Documento, $Nu_Tipo_Impuesto, $Ss_Impuesto, $iTipoItem);
              } else if (response.arrData[0].Nu_Estado != '1') {
                $('#modal-message').modal('show');
                $('.modal-message').addClass('modal-danger');
                $('.modal-title-message').text('El cliente se encuentra con BAJA DE OFICIO / NO HABIDO');
                setTimeout(function () { $('#modal-message').modal('hide'); }, 2500);
              }
            } else {
              if (response.sMessageSQL !== undefined) {
                console.log(response.sMessageSQL);
              }
              $('#modal-message').modal('show');
              $('.modal-message').addClass(response.sClassModal);
              $('.modal-title-message').text(response.sMessage);
              setTimeout(function () { $('#modal-message').modal('hide'); }, 2500);
            }
          }, 'json');// Obtener informacion de una entidad, para saber si esta HABIDO y SIN BAJA DE OFICIO
        }// /. Verificar si es un cliente existente o nuevo
      } 
    }// /. Validaciones previas
  })

  $('#table-DetalleProductos tbody').on('input', '.txt-Ss_Precio', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0) {
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $('#table-DetalleProductos tfoot').empty();
      if (nu_tipo_impuesto == 1) {//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
        });
        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-impuesto').val($Ss_IGV.toFixed(2));
        $('#span-impuesto').text($Ss_IGV.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 2) {//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
        $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 3) {//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
        $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 4) {//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
        $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else {//Sin ningun tipo de impuesto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

        var $Ss_Subtotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (considerar_igv == 0)
            $Ss_Subtotal += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-subTotal').val($Ss_Subtotal.toFixed(2));
        $('#span-subTotal').text($Ss_Subtotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      }
    }
  })

  $('#table-DetalleProductos tbody').on('input', '.txt-Qt_Producto', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0) {
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $('#table-DetalleProductos tfoot').empty();
      if (nu_tipo_impuesto == 1) {//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(Math.round10(fDescuento_SubTotal_Producto, -2)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
        });
        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-impuesto').val($Ss_IGV.toFixed(2));
        $('#span-impuesto').text($Ss_IGV.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 2) {//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
        $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 3) {//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
        $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 4) {//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
        $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else {//Sin ningun tipo de impuesto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

        var $Ss_Subtotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (considerar_igv == 0)
            $Ss_Subtotal += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-subTotal').val($Ss_Subtotal.toFixed(2));
        $('#span-subTotal').text($Ss_Subtotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      }
    }
  })

  $('#table-DetalleProductos tbody').on('change', '.cbo-ImpuestosProducto', function () {
    var fila = $(this).parents("tr");
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (considerar_igv == 1) {//SI IGV
      if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(total_producto) > 0) {
        if (nu_tipo_impuesto == 1) {//CON IGV
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
          $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 2) {//Inafecto
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += (($Ss_SubTotal_Producto * Ss_Impuesto) - $Ss_SubTotal_Producto);
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
          $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 3) {//Exonerada
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += (($Ss_SubTotal_Producto * Ss_Impuesto) - $Ss_SubTotal_Producto);
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
          $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 4) {//Gratuita
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += (($Ss_SubTotal_Producto * Ss_Impuesto) - $Ss_SubTotal_Producto);
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
          $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        }
      }
    }
  })

  $('#table-DetalleProductos tbody').on('input', '.txt-Ss_Descuento', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(descuento) >= 0 && parseFloat(total_producto) > 0 && (parseFloat($('#txt-Ss_Descuento').val()) == 0 || $('#txt-Ss_Descuento').val() == '')) {
      if (parseFloat(subtotal_producto) >= parseFloat(((subtotal_producto * descuento) / 100))) {
        if (nu_tipo_impuesto == 1) {//CON IGV
          fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
          fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 2) {//Inafecto
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_Inafecto = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 2)
              $Ss_Inafecto += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 3) {//Exonerada
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_Exonerada = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;

          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 3)
              $Ss_Exonerada += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 4) {//Gratuita
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_Gratuita = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 4)
              $Ss_Gratuita += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });

          $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
          $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (considerar_igv == 0) {//Sin ningun tipo de impuesto
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;

          $("#table-DetalleProductos > tbody > tr").each(function () {
            var rows = $(this);
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (considerar_igv == 0)
              $Ss_SubTotal += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        }
      }
    }
  })

  $('#table-DetalleProductos tbody').on('input', '.txt-Ss_Total_Producto', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();

    if (parseFloat(cantidad) > 0 && parseFloat(total_producto) > 0) {
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $('#table-DetalleProductos tfoot').empty();
      if (nu_tipo_impuesto == 1) {//CON IGV
        fila.find(".txt-Ss_Precio").val((parseFloat(total_producto / cantidad).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
        });

        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-impuesto').val($Ss_IGV.toFixed(2));
        $('#span-impuesto').text($Ss_IGV.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 2) {//Inafecto
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
        $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 3) {//Exonerada
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
        $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 4) {//Gratuita
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
        $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else {//Sin ningun tipo de impuesto
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto - ((descuento * total_producto) / 100)) / cantidad).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto - ((descuento * total_producto) / 100)).toFixed(6)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductos > tbody > tr").each(function () {
          var rows = $(this);
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (considerar_igv == 0)
            $Ss_SubTotal += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto))) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });

        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      }
    }
  })

  $('#table-DetalleProductos tbody').on('click', '#btn-deleteProducto', function () {
    $(this).closest('tr').remove();

    var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
    var $Ss_SubTotal = 0.00;
    var $Ss_Inafecto = 0.00;
    var $Ss_Exonerada = 0.00;
    var $Ss_Gratuita = 0.00;
    var $Ss_IGV = 0.00;
    var $Ss_Total = 0.00;
    var iCantDescuento = 0;
    var globalImpuesto = 0;
    var $Ss_Descuento_p = 0;
    $("#table-DetalleProductos > tbody > tr").each(function () {
      var rows = $(this);
      var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
      var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
      var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
      var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
      var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

      $Ss_Total += $Ss_Total_Producto;

      if (iGrupoImpuesto == 1) {
        $Ss_SubTotal += $Ss_SubTotal_Producto;
        $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
        globalImpuesto = fImpuesto;
      } else if (iGrupoImpuesto == 2) {
        $Ss_Inafecto += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else if (iGrupoImpuesto == 3) {
        $Ss_Exonerada += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else {
        $Ss_Gratuita += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      }

      if (isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;

      $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto))) / 100);
    });

    if ($Ss_Descuento > 0.00) {
      var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
      if ($Ss_SubTotal > 0.00) {
        $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
        $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
        $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
        $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
      }

      if ($Ss_Inafecto > 0.00) {
        $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
        $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
        $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
      }

      if ($Ss_Exonerada > 0.00) {
        $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
        $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
        $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
      }

      if ($Ss_Gratuita > 0.00) {
        $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
        $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
        $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
      }

      $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
      $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
    } else
      $Ss_Descuento = $Ss_Descuento_p;

    if (isNaN($Ss_Descuento))
      $Ss_Descuento = 0.00;

    $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
    $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

    $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
    $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

    $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
    $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

    $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
    $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

    $('#txt-impuesto').val($Ss_IGV.toFixed(2));
    $('#span-impuesto').text($Ss_IGV.toFixed(2));

    $('#txt-descuento').val($Ss_Descuento.toFixed(2));
    $('#span-descuento').text($Ss_Descuento.toFixed(2));

    $('#txt-total').val($Ss_Total.toFixed(2));
    $('#span-total').text($Ss_Total.toFixed(2));

    if ($('#table-DetalleProductos >tbody >tr').length == 0)
      $('#table-DetalleProductos').hide();
  })

  $('#table-CompraTotal').on('input', '#txt-Ss_Descuento', function () {
    var $Ss_Descuento_Producto = 0.00;
    $("#table-DetalleProductos > tbody > tr").each(function () {
      var rows = $(this);
      var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

      if (isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;

      $Ss_Descuento_Producto += $Ss_Descuento_Producto;
    })

    if ($Ss_Descuento_Producto == 0) {
      var $Ss_Descuento = parseFloat($(this).val());
      var $Ss_SubTotal = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var globalImpuesto = 0;
      $("#table-DetalleProductos > tbody > tr").each(function () {
        var rows = $(this);
        var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
        var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
        var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
        var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

        $Ss_Total += $Ss_Total_Producto;

        if (iGrupoImpuesto == 1) {
          $Ss_SubTotal += $Ss_SubTotal_Producto;
          $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          globalImpuesto = fImpuesto;
        } else if (iGrupoImpuesto == 2) {
          $Ss_Inafecto += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else if (iGrupoImpuesto == 3) {
          $Ss_Exonerada += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else {
          $Ss_Gratuita += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        }
      });

      if ($Ss_Descuento > 0.00) {
        var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
        if ($Ss_SubTotal > 0.00) {
          $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
          $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
          $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
          $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
        }

        if ($Ss_Inafecto > 0.00) {
          $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
          $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
          $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
        }

        if ($Ss_Exonerada > 0.00) {
          $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
          $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
          $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
        }

        if ($Ss_Gratuita > 0.00) {
          $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
          $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
          $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
        }

        $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
        $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
      }

      $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
      $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

      $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
      $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

      $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
      $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

      $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
      $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

      $('#txt-impuesto').val($Ss_IGV.toFixed(2));
      $('#span-impuesto').text($Ss_IGV.toFixed(2));

      $('#txt-descuento').val($Ss_Descuento.toFixed(2));
      $('#span-descuento').text($Ss_Descuento.toFixed(2));

      $('#txt-total').val($Ss_Total.toFixed(2));
      $('#span-total').text($Ss_Total.toFixed(2));
    }
  })

  $('#btn-modal-add_activo').click(function () {
    var arrDetalleCompraModal = [];

    $("#table-detalleActivo > tbody > tr").each(function () {
      var rows = $(this);

      var $ID_Empresa = $('#EID_Empresa_ModalActivo').val();
      var $ID_Organizacion = $('#EID_Organizacion_ModalActivo').val();
      var $ID_Almacen = $('#EID_Almacen_ModalActivo').val();
      var $ID_Guia_Cabecera = $('#EID_Guia_Cabecera_ModalActivo').val();

      var $ID_Guia_Detalle = rows.find(".iIdGuiaDetalle").text();
      var $ID_Area_Ingreso = rows.find(".iIdAreaIngreso").text();
      
        // M022 - INICIO
        var $ID_Tipo_Activo = rows.find(".iIdTipoActivo").text();
        // M022 - FIN

      var $Nu_Estado_Depreciacion = rows.find(".iEstadoDepreciacion").text();

        // M021 - INICIO 
        var $ID_Cliente_1 = rows.find(".iIdCliente_1").text();
        var $ID_Cliente_2 = rows.find(".iIdCliente_2").text();
        var $ID_Cliente_3 = rows.find(".iIdCliente_3").text();
        // M021 - FIN

      var $Fe_Salida_Item = rows.find(".dFechaProducto").text();

      var obj = {};
      obj.ID_Empresa = $ID_Empresa;
      obj.ID_Organizacion = $ID_Organizacion;
      obj.ID_Almacen = $ID_Almacen;
      obj.ID_Guia_Cabecera = $ID_Guia_Cabecera;
      obj.ID_Guia_Detalle = $ID_Guia_Detalle;
      obj.ID_Area_Ingreso = $ID_Area_Ingreso;
      
        // M022 - INICIO
        obj.ID_Tipo_Activo = $ID_Tipo_Activo;
        // M022 - FIN

      obj.Nu_Estado_Depreciacion = $Nu_Estado_Depreciacion;

      // M021 - INICIO
      obj.ID_Cliente_1 = $ID_Cliente_1;
      obj.ID_Cliente_2 = $ID_Cliente_2;
      obj.ID_Cliente_3 = $ID_Cliente_3;
      // M021 - FIN

      obj.Fe_Salida_Item = $Fe_Salida_Item;
      arrDetalleCompraModal.push(obj);
    });

    $('.help-block').empty();
    $('.form-group').removeClass('has-error');

    if (arrDetalleCompraModal.length == 0) {
      $('#cbo-ProductoDetalle').closest('.form-group').find('.help-block').html('Activo <b>sin detalle</b>');
      $('#cbo-ProductoDetalleo').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($("html, body"), $('#cbo-ProductoDetalle'));
    } else {
      $('#btn-modal-add_activo').text('');
      $('#btn-modal-add_activo').attr('disabled', true);
      $('#btn-modal-add_activo').append('Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');
      $('#btn-modal-salir').attr('disabled', true);

      url = base_url + 'Logistica/SalidaInventarioController/saveActivo';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: {arrDetalleCompraModal : arrDetalleCompraModal},
        success: function (response) {
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.sStatus == 'success') {
            $('.modal-modal_activo').modal('hide');

            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);

            reload_table_compra();
          } else {
            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 3100);
          }

          $('#btn-modal-add_activo').text('');
          $('#btn-modal-add_activo').append('Guardar');
          $('#btn-modal-add_activo').attr('disabled', false);
          $('#btn-modal-salir').attr('disabled', false);
        }
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');

        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 3100);

        //Message for developer
        console.log(jqXHR.responseText);

        $('#btn-modal-add_activo').text('');
        $('#btn-modal-add_activo').attr('disabled', false);
        $('#btn-modal-add_activo').append('Cobrar');
        $('#btn-modal-salir').attr('disabled', false);
      })
    }
  })
})// $function()

function isExistTableTemporalProducto($ID_Producto) {
  return Array.from($('tr[id*=tr_detalle_producto]'))
    .some(element => ($('td:nth(0)', $(element)).html() === $ID_Producto));
}

function form_Compra() {
  if (accion == 'add_factura_compra' || accion == 'upd_factura_compra') {//Accion para validar tecla ENTER
    var arrDetalleCompra = [];
    var arrValidarNumerosEnCero = [];
    var $counterNumerosEnCero = 0;
    var tr_foot = '';

    $("#table-DetalleProductos > tbody > tr").each(function () {
      var rows = $(this);

      var $ID_Producto = rows.find(".td-iIdItem").text();
      var $Qt_Producto = $('.txt-Qt_Producto', this).val();
      var $Ss_Precio = $('.txt-Ss_Precio', this).val();
      var $ID_Impuesto_Cruce_Documento = $('.cbo-ImpuestosProducto option:selected', this).val();
      var $Ss_SubTotal = $('.txt-Ss_SubTotal_Producto', this).val();
      var $Ss_Descuento = $('.txt-Ss_Descuento', this).val();
      var $Ss_Total = $('.txt-Ss_Total_Producto', this).val();
      var $Nu_Lote_Vencimiento = $('.txt-Nu_Lote_Vencimiento', this).val();
      var $Fe_Lote_Vencimiento = $('.txt-Fe_Lote_Vencimiento', this).val();
      var $ID_Area_Ingreso = $('.cbo-AreaIngreso option:selected', this).val();
      var $Nu_Estado_Depreciacion = $('.cbo-Activo option:selected', this).val();
      var $fDescuentoSinImpuestosItem = rows.find(".td-fDescuentoSinImpuestosItem").text();
      var $fDescuentoImpuestosItem = rows.find(".td-fDescuentoImpuestosItem").text();

      if (parseFloat($Ss_Precio) == 0 || parseFloat($Qt_Producto) == 0 || parseFloat($Ss_Total) == 0) {
        arrValidarNumerosEnCero[$counterNumerosEnCero] = $ID_Producto;
        //$('#tr_detalle_producto' + $ID_Producto).addClass('danger');
      }

      var obj = {};

      obj.ID_Producto = $ID_Producto;
      obj.Ss_Precio = $Ss_Precio;
      obj.Qt_Producto = $Qt_Producto;
      obj.ID_Impuesto_Cruce_Documento = $ID_Impuesto_Cruce_Documento;
      obj.Ss_SubTotal = $Ss_SubTotal;
      obj.Ss_Descuento = $Ss_Descuento;
      obj.Ss_Impuesto = $Ss_Total - $Ss_SubTotal;
      obj.Ss_Total = $Ss_Total;
      obj.Nu_Lote_Vencimiento = $Nu_Lote_Vencimiento;
      obj.Fe_Lote_Vencimiento = $Fe_Lote_Vencimiento;
      obj.ID_Area_Ingreso = $ID_Area_Ingreso;
      obj.Nu_Estado_Depreciacion = $Nu_Estado_Depreciacion;
      obj.fDescuentoSinImpuestosItem = $fDescuentoSinImpuestosItem;
      obj.fDescuentoImpuestosItem = $fDescuentoImpuestosItem;
      arrDetalleCompra.push(obj);
      $counterNumerosEnCero++;
    });

    bEstadoValidacion = validatePreviousDocumentToSavePurchase();

    if (arrDetalleCompra.length == 0) {
      $('#panel-DetalleProductos').removeClass('panel-default');
      $('#panel-DetalleProductos').addClass('panel-danger');

      $('#txt-No_Producto').closest('.form-group').find('.help-block').html('Documento <b>sin detalle</b>');
      $('#txt-No_Producto').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($("html, body"), $('#txt-No_Producto'));
    } else if (bEstadoValidacion) {
      $('#panel-DetalleProductos').removeClass('panel-danger');
      $('#panel-DetalleProductos').addClass('panel-default');

      //var iEstadoRegistro = 5;//12/02/2021
      //var iEstadoRegistro = 13;//Estado Completado para que luego entren a descargar stock no mas //20/04/2021
      var iEstadoRegistro = 12;
      if ($('#txt-ENu_Estado').val() == 12)
        iEstadoRegistro = 13;
      if ($('#txt-ENu_Estado').val() == 13)
        iEstadoRegistro = 14;

      var arrCompraCabecera = Array();
      arrCompraCabecera = {
        'EID_Guia_Cabecera': $('#txt-EID_Guia_Cabecera').val(),
        'ID_Almacen': $('#cbo-almacen').val(),
        'ID_Tipo_Documento': $('#cbo-TiposDocumento').val(),
        'ID_Serie_Documento': $('#cbo-SeriesDocumento').val(),
        'ID_Serie_Documento_PK': $('#cbo-SeriesDocumento').find(':selected').data('id_serie_documento_pk'),
        'ID_Numero_Documento': $('#txt-ID_Numero_Documento').val(),
        'Fe_Emision': $('#txt-Fe_Emision').val(),
        'ID_Moneda': $('#cbo-Monedas').val(),
        'Nu_Descargar_Inventario': $('#cbo-descargar_stock').val(),
        'ID_Tipo_Movimiento': $('#cbo-tipo_movimiento').val(),
        'ID_Entidad': $('#txt-AID').val(),
        'iFlete': $('[name="radio-addFlete"]:checked').attr('value'),//Flete
        'ID_Entidad_Transportista': $('#txt-AID_Transportista').val(),
        'No_Placa': $('[name="No_Placa"]').val(),
        'Fe_Traslado': $('[name="Fe_Traslado"]').val(),
        'ID_Motivo_Traslado': $('#cbo-motivo_traslado').val(),
        'No_Licencia': $('[name="No_Licencia"]').val(),
        'No_Certificado_Inscripcion': $('[name="No_Certificado_Inscripcion"]').val(),
        'ID_Lista_Precio_Cabecera': $('#cbo-lista_precios').val(),
        'Txt_Glosa': $('[name="Txt_Glosa"]').val(),
        'Po_Descuento': $('#txt-Ss_Descuento').val(),
        'Ss_Descuento': $('#txt-descuento').val(),
        'Ss_Total': $('#txt-total').val(),
        'Nu_Tipo_Mantenimiento': $('#cbo-tipo_mantenimiento').val(),
        'ID_Origen_Tabla':'',
        'Nu_Estado': iEstadoRegistro,
        'ENu_Estado': $('#txt-ENu_Estado').val(),
      };

      var No_Cliente_Filter = $('#txt-ANombre').val(), arrClienteNuevo = {};
      if ($('[name="addCliente"]:checked').attr('value') == 1) {//Agregar cliente
        arrClienteNuevo = {
          'ID_Tipo_Documento_Identidad': $('#cbo-TiposDocumentoIdentidadCliente').val(),
          'Nu_Documento_Identidad': $('#txt-Nu_Documento_Identidad_Cliente').val(),
          'No_Entidad': $('#txt-No_Entidad_Cliente').val(),
          'Txt_Direccion_Entidad': $('#txt-Txt_Direccion_Entidad_Cliente').val(),
          'Nu_Telefono_Entidad': $('#txt-Nu_Telefono_Entidad_Cliente').val(),
          'Nu_Celular_Entidad': $('#txt-Nu_Celular_Entidad_Cliente').val(),
        };
        No_Cliente_Filter = $('#txt-No_Entidad_Cliente').val();
      }  

      
      console.log(arrCompraCabecera);
      
      $('#btn-save').text('');
      $('#btn-save').attr('disabled', true);
      $('#btn-save').append('Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      $('#modal-loader').modal('show');

      // STOCK_PROD_UPDT - INICIO
      // var Actualiza_Stock_en_Vale='salida_inventario';
      // STOCK_PROD_UPDT - FIN 

      url = base_url + 'Logistica/SalidaInventarioController/crudCompra';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: {
          arrCompraCabecera: arrCompraCabecera,
          arrClienteNuevo: arrClienteNuevo,
          arrDetalleCompra: arrDetalleCompra,
          // STOCK_PROD_UPDT - INICIO
          // Actualiza_Stock_en_Vale : Actualiza_Stock_en_Vale,
          // STOCK_PROD_UPDT - FIN 
        },
        success: function (response) {
          $('#modal-loader').modal('hide');

          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.status == 'success') {
            accion = '';

            $('#txt-Filtro_Entidad').val(No_Cliente_Filter);

            $('.div-AgregarEditar').hide();
            $('.div-Listar').show();
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);

            $('#form-Compra')[0].reset();
            reload_table_compra();
          } else {
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
          }

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar (ENTER)');
          $('#btn-save').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          $('#modal-loader').modal('hide');
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');

          $('#modal-message').modal('show');
          $('.modal-message').addClass('modal-danger');
          $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

          //Message for developer
          console.log(jqXHR.responseText);

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar');
          $('#btn-save').attr('disabled', false);
        }
      });
    }
  }
}

function reload_table_compra() {
  table_compra.ajax.reload(null, false);
}

function _anularCompra($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario) {
  $('#modal-loader').modal('show');

  url = base_url + 'Logistica/SalidaInventarioController/anularCompra/' + ID + '/' + Nu_Enlace + '/' + Nu_Descargar_Inventario;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      $('#modal-loader').modal('hide');

      $modal_delete.modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');
      $('#modal-message').modal('show');

      if (response.status == 'success') {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text(response.message);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
        reload_table_compra();
      } else {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text(response.message);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
      }
      accion = '';
    },
    error: function (jqXHR, textStatus, errorThrown) {
      accion = '';
      $('#modal-loader').modal('hide');
      $modal_delete.modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');

      $('#modal-message').modal('show');
      $('.modal-message').addClass('modal-danger');
      $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
      setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

      //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function _eliminarCompra($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario) {
  $('#modal-loader').modal('show');

  url = base_url + 'Logistica/SalidaInventarioController/eliminarCompra/' + ID + '/' + Nu_Enlace + '/' + Nu_Descargar_Inventario;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      $('#modal-loader').modal('hide');

      $modal_delete.modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');
      $('#modal-message').modal('show');

      if (response.status == 'success') {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text(response.message);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
        reload_table_compra();
      } else {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text(response.message);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
      }
      accion = '';
    },
    error: function (jqXHR, textStatus, errorThrown) {
      accion = '';
      $('#modal-loader').modal('hide');
      $modal_delete.modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');

      $('#modal-message').modal('show');
      $('.modal-message').addClass('modal-danger');
      $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
      setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

      //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function validatePreviousDocumentToSavePurchase() {
  var bEstadoValidacion = true;

  if ($('#cbo-TiposDocumento').val() == 0) {
    $('#cbo-TiposDocumento').closest('.form-group').find('.help-block').html('Seleccionar documento');
    $('#cbo-TiposDocumento').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#cbo-TiposDocumento'));
  } else if ($('[name="addCliente"]:checked').attr('value') == 0 && ($('#txt-AID').val().length === 0 || $('#txt-ANombre').val().length === 0)) {
    $('#txt-ANombre').closest('.form-group').find('.help-block').html('Seleccionar cliente');
    $('#txt-ANombre').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#txt-ANombre'));
  } else if ($('#cbo-tipo_movimiento').val() == 0) {
    $('#cbo-tipo_movimiento').closest('.form-group').find('.help-block').html('Seleccionar tipo de movimiento');
    $('#cbo-tipo_movimiento').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#cbo-tipo_movimiento'));
  } else if ($('[name="addCliente"]:checked').attr('value') == 0 && ($('#txt-AID').val().length === 0 || $('#txt-ANombre').val().length === 0 || $('#txt-ACodigo').val().length === 0)) {
    $('#txt-ANombre').closest('.form-group').find('.help-block').html('Seleccionar cliente');
    $('#txt-ANombre').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#txt-ANombre'));
  } else if ($('#cbo-TiposDocumentoIdentidadCliente').val() != 1 && $('#cbo-TiposDocumentoIdentidadCliente').val() != 2 && ($('[name="addCliente"]:checked').attr('value') == 1 && $('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') != $('#txt-Nu_Documento_Identidad_Cliente').val().length)) {
    $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').find('.help-block').html('Debe ingresar ' + $('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') + ' dígitos');
    $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#txt-Nu_Documento_Identidad_Cliente'));
  } else if ($('[name="addCliente"]:checked').attr('value') == 1 && $('#cbo-Estado').val() == 0) {
    $('#modal-message').modal('show');
    $('.modal-message').addClass('modal-danger');
    $('.modal-title-message').text('El cliente se encuentra con BAJA DE OFICIO / NO HABIDO');
    setTimeout(function () { $('#modal-message').modal('hide'); }, 2500);

    bEstadoValidacion = false;
  }
  return bEstadoValidacion;
}

function generarTablaTemporalItems($ID_Producto, $No_Producto, $Ss_Precio, $ID_Impuesto_Cruce_Documento, $Nu_Tipo_Impuesto, $Ss_Impuesto, $iTipoItem) {
  var _ID_Producto = '', option_impuesto_producto = '', option_area_impuesto = '', $Ss_SubTotal_Producto = 0.00, $Ss_Total_Producto = 0.00;

  var obj = JSON.parse(arrImpuestosProducto);
  for (var x = 0; x < obj.arrImpuesto.length; x++) {
    var selected = '';
    if ($ID_Impuesto_Cruce_Documento == obj.arrImpuesto[x].ID_Impuesto_Cruce_Documento)
      selected = 'selected="selected"';
    option_impuesto_producto += "<option value='" + obj.arrImpuesto[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + obj.arrImpuesto[x].Nu_Tipo_Impuesto + "' data-impuesto_producto='" + obj.arrImpuesto[x].Ss_Impuesto + "' " + selected + ">" + obj.arrImpuesto[x].No_Impuesto + "</option>";
  }

  var obj = JSON.parse(arrAreaIngreso);
  for (var x = 0; x < obj.arrAreaIngreso.length; x++) {
    option_area_impuesto += "<option value='" + obj.arrAreaIngreso[x].ID_Tipo_Documento + "'>" + obj.arrAreaIngreso[x].No_Tipo_Documento_Breve + "</option>";
  }

  $Ss_Precio = isNaN($Ss_Precio) ? 0 : $Ss_Precio;

  $Ss_Total_Producto = $Ss_Precio;
  $Ss_SubTotal_Producto = parseFloat($Ss_Precio / $Ss_Impuesto);

  var table_detalle_producto =
    "<tr id='tr_detalle_producto" + $ID_Producto + "'>"
    + "<td style='display:none;' class='text-left td-iIdItem'>" + $ID_Producto + "</td>"
    + "<td class='text-right'><input type='tel' id=" + $ID_Producto + " class='pos-input txt-Qt_Producto form-control input-decimal' " + ($iTipoItem == 1 ? 'onkeyup=validateStockNow(event);' : '') + " data-id_item='" + $ID_Producto + "' data-id_producto='" + $ID_Producto + "' value='1' autocomplete='off'></td>"
    + "<td class='text-left'>" + $No_Producto + "</td>"
    + "<td class='text-right'><input type='text' class='pos-input txt-Ss_Precio form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='" + $Ss_Precio + "' autocomplete='off'></td>"
    + "<td class='text-right'>"
      + "<select class='cbo-ImpuestosProducto form-control required' style='width: 100%;'>"
        + option_impuesto_producto
      + "</select>"
    + "</td>"
    + "<td style='display:none;' class='text-right'><input type='tel' class='pos-input txt-Ss_SubTotal_Producto form-control' value='" + $Ss_SubTotal_Producto.toFixed(2) + "' autocomplete='off' disabled></td>"
    + "<td class='text-right'><input type='tel' class='pos-input txt-Ss_Descuento form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='' autocomplete='off'></td>"
    + "<td class='text-right'><input type='tel' class='pos-input txt-Ss_Total_Producto form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='" + $Ss_Total_Producto.toFixed(2) + "' autocomplete='off'></td>"
    + "<td class='text-right'><input type='text' class='pos-input txt-Nu_Lote_Vencimiento form-control input-codigo_barra' placeholder='Opcional' data-id_producto='" + $ID_Producto + "' value='' autocomplete='off'></td>"
    + "<td class='text-right'><input type='text' class='pos-input txt-Fe_Lote_Vencimiento form-control date-picker-invoice' placeholder='Opcional' data-id_producto='" + $ID_Producto + "' value='' autocomplete='off'></td>"
    + "<td class='text-right'>"
      + "<select class='cbo-AreaIngreso form-control required' style='width: 100%;'>"
        + option_area_impuesto
      + "</select>"
    + "</td>"
    + "<td class='text-right'>"
      + "<select class='cbo-Activo form-control required' style='width: 100%;'>"
        +"<option value='1'>Activo</option>"
        +"<option value='0'>Baja</option>"
      + "</select>"
    + "</td>"
    + "<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>0.00</td>"
    + "<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>0.00</td>"
    + "<td class='text-center'><button type='button' id='btn-deleteProducto' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-2x' aria-hidden='true'> </i></button></td>"
    + "</tr>";

  if (isExistTableTemporalProducto($ID_Producto)) {
    $('#txt-No_Producto').closest('.form-group').find('.help-block').html('Ya existe producto <b>' + $No_Producto + '</b>');
    $('#txt-No_Producto').closest('.form-group').removeClass('has-success').addClass('has-error');
    $('#txt-No_Producto').focus();

    $('#txt-ID_Producto').val('');
    $('#txt-No_Producto').val('');
    $('#txt-Ss_Precio').val('');
  } else {
    $('#txt-ID_Producto').val('');
    $('#txt-No_Producto').val('');
    $('#txt-Ss_Precio').val('');

    $('#table-DetalleProductos').show();
    $('#table-DetalleProductos >tbody').append(table_detalle_producto);

    $('.txt-Fe_Lote_Vencimiento').datepicker({
      autoclose: true,
      startDate: new Date(fYear, fToday.getMonth(), fDay),
      todayHighlight: true
    })

    $('#' + $ID_Producto).focus();
    $('#' + $ID_Producto).select();

    var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
    var $Ss_SubTotal = 0.00;
    var $Ss_Inafecto = 0.00;
    var $Ss_Exonerada = 0.00;
    var $Ss_Gratuita = 0.00;
    var $Ss_IGV = 0.00;
    var $Ss_Total = 0.00;
    var iCantDescuento = 0;
    var globalImpuesto = 0;
    var $Ss_Descuento_p = 0;
    $("#table-DetalleProductos > tbody > tr").each(function () {
      var rows = $(this);
      var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
      var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
      var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / fImpuesto);
      var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
      var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

      $Ss_Total += $Ss_Total_Producto;

      if (iGrupoImpuesto == 1) {
        $Ss_SubTotal += $Ss_SubTotal_Producto;
        $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
        globalImpuesto = fImpuesto;
      } else if (iGrupoImpuesto == 2) {
        $Ss_Inafecto += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else if (iGrupoImpuesto == 3) {
        $Ss_Exonerada += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else {
        $Ss_Gratuita += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      }

      if (isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;

      $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto))) / 100);
    });

    if ($Ss_SubTotal > 0.00 || $Ss_Inafecto > 0.00 || $Ss_Exonerada > 0.00) {
      if ($Ss_Descuento > 0.00) {
        var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
        if ($Ss_SubTotal > 0.00) {
          $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
          $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
          $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
          $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
        }

        if ($Ss_Inafecto > 0.00) {
          $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
          $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
          $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
        }

        if ($Ss_Exonerada > 0.00) {
          $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
          $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
          $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
        }

        if ($Ss_Gratuita > 0.00) {
          $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
          $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
          $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
        }

        $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
        $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
      } else
        $Ss_Descuento = $Ss_Descuento_p;

      if (isNaN($Ss_Descuento))
        $Ss_Descuento = 0.00;

      $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
      $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

      $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
      $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

      $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
      $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

      $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
      $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

      $('#txt-impuesto').val($Ss_IGV.toFixed(2));
      $('#span-impuesto').text($Ss_IGV.toFixed(2));

      $('#txt-descuento').val($Ss_Descuento.toFixed(2));
      $('#span-descuento').text($Ss_Descuento.toFixed(2));

      $('#txt-total').val($Ss_Total.toFixed(2));
      $('#span-total').text($Ss_Total.toFixed(2));
    }

    validateDecimal();
    validateNumber();
    validateNumberOperation();
    validateCodigoBarra();
  }
}

// Ayudas - combobox
function getAlmacenes(arrParams) {
  url = base_url + 'HelperController/getAlmacenes';
  $.post(url, {}, function (responseAlmacen) {
    var iCantidadRegistros = responseAlmacen.length;
    var selected = '';
    var iIdAlmacen = 0;
    if (iCantidadRegistros == 1) {
      if (arrParams !== undefined) {
        iIdAlmacen = arrParams.ID_Almacen;
      }
      if (iIdAlmacen == responseAlmacen[0]['ID_Almacen']) {
        selected = 'selected="selected"';
      }
      $('#cbo-almacen').html('<option value="' + responseAlmacen[0]['ID_Almacen'] + '" ' + selected + '>' + responseAlmacen[0]['No_Almacen'] + '</option>');
      var arrParams = {
        ID_Almacen: responseAlmacen[0]['ID_Almacen'],
      }
      getListaPrecios(arrParams);
    } else {
      $('#cbo-almacen').html('<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < iCantidadRegistros; i++) {
        selected = '';
        if (arrParams !== undefined) {
          iIdAlmacen = arrParams.ID_Almacen;
        }
        if (iIdAlmacen == responseAlmacen[i]['ID_Almacen']) {
          selected = 'selected="selected"';
        }
        $('#cbo-almacen').append('<option value="' + responseAlmacen[i]['ID_Almacen'] + '" ' + selected + '>' + responseAlmacen[i]['No_Almacen'] + '</option>');
      }
    }
  }, 'JSON');
}

function getListaPrecios(arrParams) {
  url = base_url + 'HelperController/getListaPrecio';
  var arrPost = {
    Nu_Tipo_Lista_Precio: $('[name="Nu_Tipo_Lista_Precio"]').val(),
    ID_Organizacion: $('#header-a-id_organizacion').val(),
    ID_Almacen: arrParams.ID_Almacen,
  }
  $.post(url, arrPost, function (responseListaPrecio) {
    var iCantidadRegistrosListaPrecios = responseListaPrecio.length;
    if (iCantidadRegistrosListaPrecios == 1) {
      $('#cbo-lista_precios').html('<option value="' + responseListaPrecio[0].ID_Lista_Precio_Cabecera + '">' + responseListaPrecio[0].No_Lista_Precio + '</option>');
    } else if (iCantidadRegistrosListaPrecios > 1) {
      $('#cbo-lista_precios').html('<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < iCantidadRegistrosListaPrecios; i++)
        $('#cbo-lista_precios').append('<option value="' + responseListaPrecio[i].ID_Lista_Precio_Cabecera + '">' + responseListaPrecio[i].No_Lista_Precio + '</option>');
    } else {
      $('#cbo-lista_precios').html('<option value="0">- Sin lista precio -</option>');
    }
  }, 'JSON');
}

function pdfSalidaInventario(ID) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('.modal-message-delete').removeClass('modal-danger modal-warning modal-success');
  $('.modal-message-delete').addClass('modal-success');

  $('.modal-title-message-delete').text('¿Deseas generar PDF?');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $('#btn-save-delete').off('click').click(function () {
    $('#modal-loader').modal('show');
    url = base_url + 'Logistica/SalidaInventarioController/getSalidaInventarioPDF/' + ID;
    window.open(url, '_blank');
    $('#modal-loader').modal('hide');
    $modal_delete.modal('hide');
  });
}

function crearActivo(ID_Guia_Cabecera) {
  $('#table-detalleActivo > tbody').empty();

  $('#form-addActivo')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('[name="EID_Empresa_ModalActivo"]').val('');
  $('[name="EID_Organizacion_ModalActivo"]').val('');
  $('[name="EID_Almacen_ModalActivo"]').val('');
  $('[name="EID_Guia_Cabecera_ModalActivo"]').val('');

  $('#labelDocumentoModalAcitvo').text('');

  $('#cbo-ProductoDetalle').html('');
  $('#cbo-AreaIngresoDetalle').html('');

  // M021 - INICIO

  $('#cbo-ClienteDetalle_1').html('');
  $('#cbo-ClienteDetalle_2').html('');
  $('#cbo-ClienteDetalle_3').html('');

  // M021 - FIN

  $('.txt-Fe_Salida_Item').val(fDay + '/' + fMonth + '/' + fYear);
  $('.txt-Fe_Salida_Item').datepicker({
    autoclose: true,
    startDate: new Date(fYear, fToday.getMonth(), fDay),
    todayHighlight: true
  })

  url = base_url + 'Logistica/SalidaInventarioController/getDetalle';
  var arrPost = {
    ID_Guia_Cabecera: ID_Guia_Cabecera,
  }
  $.post(url, arrPost, function (response) {
    if (response.sStatus == 'success') {
      $('.modal-modal_activo').modal('show');

      $('#EID_Empresa_ModalActivo').val(response.arrData[0].ID_Empresa);
      $('#EID_Organizacion_ModalActivo').val(response.arrData[0].ID_Organizacion);
      $('#EID_Almacen_ModalActivo').val(response.arrData[0].ID_Almacen);
      $('#EID_Guia_Cabecera_ModalActivo').val(ID_Guia_Cabecera);

      $('#labelDocumentoModalAcitvo').text(response.arrData[0].No_Tipo_Documento_Breve + ' ' + response.arrData[0].ID_Serie_Documento + ' ' + response.arrData[0].ID_Numero_Documento);

      url = base_url + 'HelperController/getTiposDocumentos';
      var arrPost = {
        Nu_Tipo_Filtro: 21,
      }
      $.post(url, arrPost, function (responseArea) {
        $('#cbo-AreaIngresoDetalle').html('<option value="0">- Seleccionar -</option>');
        for (var x = 0; x < responseArea.length; x++) {
          $('#cbo-AreaIngresoDetalle').append('<option value="' + responseArea[x].ID_Tipo_Documento + '">' + responseArea[x].No_Tipo_Documento_Breve + '</option>');
        }
      }, 'JSON');

      var response = response.arrData;
      $('#cbo-ProductoDetalle').html('<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < response.length; i++) {
        $('#cbo-ProductoDetalle').append('<option value="' + response[i].ID_Producto + '" data-id_guia_detalle="' + response[i].ID_Guia_Detalle +'">' + response[i].Nu_Codigo_Barra + ' ' + response[i].No_Producto + ' Precio: ' + response[i].Ss_Precio + '</option>');
      }

// M021 - INICIO 

url = base_url + 'HelperController/getClientesCbo';

var arrPost = {
  //Nu_Tipo_Filtro: 21,
}

$.post(url, arrPost, function (responseCliente){
  $('#cbo-ClienteDetalle_1').html('<option value="0">- Seleccionar -</option>');
  for (var x = 0; x < responseCliente.length; x++) {
    $('#cbo-ClienteDetalle_1').append('<option value="' + responseCliente[x].ID_entidad + '">' + responseCliente[x].nu_documento_identidad + ' - '+ responseCliente[x].no_entidad + '</option>');
  }

  $('#cbo-ClienteDetalle_2').html('<option value="0">- Seleccionar -</option>');
  for (var x = 0; x < responseCliente.length; x++) {
    $('#cbo-ClienteDetalle_2').append('<option value="' + responseCliente[x].ID_entidad + '">' + responseCliente[x].nu_documento_identidad + ' - '+ responseCliente[x].no_entidad + '</option>');
  }

  $('#cbo-ClienteDetalle_3').html('<option value="0">- Seleccionar -</option>');
  for (var x = 0; x < responseCliente.length; x++) {
    $('#cbo-ClienteDetalle_3').append('<option value="' + responseCliente[x].ID_entidad + '">' + responseCliente[x].nu_documento_identidad + ' - '+ responseCliente[x].no_entidad + '</option>');
  }

}, 'JSON');

// M021 - FIN


    } else {
      alert(response.sMessage);
    }
  }, 'JSON');

  $('#btn-addProductoDetalle').click(function () {
    var $ID_Producto = $('#cbo-ProductoDetalle').val();
    var $ID_Guia_Detalle = $('#cbo-ProductoDetalle').find(':selected').data('id_guia_detalle');
    var $No_Producto = $('#cbo-ProductoDetalle :selected').text();
    var $Fe_Salida_Producto = $('.txt-Fe_Salida_Item').val();
    var $ID_Area_Ingreso = $('#cbo-AreaIngresoDetalle').val();
    var $No_Area_Ingreso = $('#cbo-AreaIngresoDetalle :selected').text();
    var $Nu_Estado_Depreciacion = $('#cbo-ActivoDetalle').val();
    
      //M022 - INICIO
      var $ID_Tipo_Activo = $('#cbo-TipoActivoDetalle').val();
      var $No_Tipo_Activo = $('#cbo-TipoActivoDetalle :selected').text();
      //M022 - FIN 

    var $No_Estado_Depreciacion = $('#cbo-ActivoDetalle :selected').text();
    
    //M021 - INICIO
    var $ID_Cliente_1 = $('#cbo-ClienteDetalle_1').val();
    var $No_Cliente_1 = $('#cbo-ClienteDetalle_1 :selected').text();
    var $ID_Cliente_2 = $('#cbo-ClienteDetalle_2').val();
    var $No_Cliente_2 = $('#cbo-ClienteDetalle_2 :selected').text();
    var $ID_Cliente_3 = $('#cbo-ClienteDetalle_3').val();
    var $No_Cliente_3 = $('#cbo-ClienteDetalle_3 :selected').text();

    //alert( $ID_Cliente_1+" - "+$No_Cliente_1+" 2: "+    $ID_Cliente_2+" - "+$No_Cliente_2+" 3: "+    $ID_Cliente_3+" - "+$No_Cliente_3);

    //M021 - FIN 

    $('.help-block').empty();
    $('.form-group').removeClass('has-error');
    if ($ID_Producto == 0) {
      $('#cbo-ProductoDetalle').closest('.form-group').find('.help-block').html('Seleccionar producto');
      $('#cbo-ProductoDetalle').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($ID_Area_Ingreso == 0) {
      $('#cbo-AreaIngresoDetalle').closest('.form-group').find('.help-block').html('Seleccionar Area');
      $('#cbo-AreaIngresoDetalle').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {

      // M021 FIX - I 
      if ($ID_Cliente_1 == 0) {
        $('#cbo-ClienteDetalle_1').closest('.form-group').find('.help-block').html('Seleccionar Cliente');
        $('#cbo-ClienteDetalle_1').closest('.form-group').removeClass('has-success').addClass('has-error');
      } else if ($ID_Cliente_2 == 0) {
        $('#cbo-ClienteDetalle_2').closest('.form-group').find('.help-block').html('Seleccionar Receptor');
        $('#cbo-ClienteDetalle_2').closest('.form-group').removeClass('has-success').addClass('has-error');
      } else if ($ID_Cliente_3 == 0) {
        $('#cbo-ClienteDetalle_3').closest('.form-group').find('.help-block').html('Seleccionar Solicitante Cambio');
        $('#cbo-ClienteDetalle_3').closest('.form-group').removeClass('has-success').addClass('has-error');
      } else {
        // M021 FIX - F

      var table_enlace_producto =
        "<tr id='tr_enlace_producto" + $ID_Producto + "'>"
        + "<td class='text-left iIdProducto' style='display:none;'>" + $ID_Producto + "</td>"
        + "<td class='text-left iIdGuiaDetalle' style='display:none;'>" + $ID_Guia_Detalle + "</td>"
        + "<td class='text-left iIdAreaIngreso' style='display:none;'>" + $ID_Area_Ingreso + "</td>"
        
          //M022 - INICIO
          + "<td class='text-left iIdTipoActivo' style='display:none;'>" + $ID_Tipo_Activo + "</td>"
          //M022 - FIN

          //M021 - INICIO
          + "<td class='text-left iIdCliente_1' style='display:none;'>" + $ID_Cliente_1 + "</td>"
          + "<td class='text-left iIdCliente_2' style='display:none;'>" + $ID_Cliente_2 + "</td>"
          + "<td class='text-left iIdCliente_3' style='display:none;'>" + $ID_Cliente_3 + "</td>"
         //M021 - FIN

        + "<td class='text-left iEstadoDepreciacion' style='display:none;'>" + $Nu_Estado_Depreciacion + "</td>"
        
        
        
        + "<td class='text-left'>" + $No_Producto + "</td>"
        + "<td class='text-right dFechaProducto'>" + $Fe_Salida_Producto + "</td>"
        + "<td class='text-right'>" + $No_Area_Ingreso + "</td>"
        
          //M022 - INICIO
          + "<td class='text-right'>" + $No_Tipo_Activo + "</td>"
          //M022 - FIN

        + "<td class='text-right'>" + $No_Estado_Depreciacion + "</td>"

        //M021 - INICIO
        + "<td class='text-right'>" + $No_Cliente_1 + "</td>"
        + "<td class='text-right'>" + $No_Cliente_2 + "</td>"
        + "<td class='text-right'>" + $No_Cliente_3 + "</td>"
        //M021 - FIN

        + "<td class='text-center'><button type='button' id='btn-deleteProductoEnlace' class='btn btn-xs btn-link' alt='Eliminar' title='Eliminar'>Eliminar</button></td>"
        + "</tr>";

      if (isExistTableTemporalEnlacesProducto($ID_Producto)) {
        $('#cbo-ProductoDetalle').closest('.form-group').find('.help-block').html('Ya existe producto <b>' + $No_Producto + '</b>');
        $('#cbo-ProductoDetalle').closest('.form-group').removeClass('has-success').addClass('has-error');
      } else {
        $('#table-detalleActivo').show();
        $('#table-detalleActivo').append(table_enlace_producto);
        $('#cbo-ProductoDetalle').val(0).trigger("change");
        $('#cbo-AreaIngresoDetalle').val(0).trigger("change");
      }
      // M021 FIX - I 
      }
      // M021 FIX - F
    }
  })

  $('#table-detalleActivo tbody').on('click', '#btn-deleteProductoEnlace', function () {
    $(this).closest('tr').remove();
    if ($('#table-detalleActivo >tbody >tr').length == 0)
      $('#table-detalleActivo').hide();
  })
}

function isExistTableTemporalEnlacesProducto($ID_Producto) {
  return Array.from($('tr[id*=tr_enlace_producto]'))
    .some(element => ($('td:nth(0)', $(element)).html() === $ID_Producto));
}