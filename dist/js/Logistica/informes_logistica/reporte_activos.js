var url;

$(function () {
  $( '#modal-loader' ).modal('show');
  $( '#div-compras_x_proveedor' ).hide();
  
  url = base_url + 'HelperController/getTiposDocumentos';
  $.post( url, {Nu_Tipo_Filtro : 2}, function( response ){
    $( '#cbo-filtros_tipos_documento' ).html('<option value="0" selected="selected">Todos</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-filtros_tipos_documento' ).append( '<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>' );
    $( '#modal-loader' ).modal('hide');
  }, 'JSON');
   
  $( '.btn-generar_compras_x_proveedor' ).click(function(){
    var Fe_Inicio, Fe_Fin, ID_Tipo_Documento, ID_Serie_Documento, ID_Numero_Documento, Nu_Estado_Documento, sNombreProveedor, iIdItem, sNombreItem, iTipoReporte, ID_OI_Detalle;
    
    Fe_Inicio = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
    Fe_Fin = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
    ID_Tipo_Documento = $( '#cbo-filtros_tipos_documento' ).val();
    ID_Serie_Documento = ($( '#txt-Filtro_SerieDocumento' ).val().length == 0 ? '-' : $( '#txt-Filtro_SerieDocumento' ).val());
    ID_Numero_Documento = ($( '#txt-Filtro_NumeroDocumento' ).val().length == 0 ? '-' : $( '#txt-Filtro_NumeroDocumento' ).val());
    Nu_Estado_Documento = $( '#cbo-estado_documento option:selected' ).val();
   
    // M044 - I 
    // iIdProveedor = ($( '#txt-AID' ).val().length === 0 ? '-' : $( '#txt-AID' ).val());
    //sNombreProveedor = ($( '#txt-Filtro_Entidad' ).val().length === 0 ? '-' : $( '#txt-Filtro_Entidad' ).val());
    iIdCliente= ($( '#txt-AID' ).val().length === 0 ? '-' : $( '#txt-AID' ).val());
    sNombreCliente  = ($( '#txt-Filtro_Entidad' ).val().length === 0 ? '-' : $( '#txt-Filtro_Entidad' ).val());

    // M044 - F 

    iIdItem = ($( '#txt-ID_Producto' ).val().length === 0 ? '-' : $( '#txt-ID_Producto' ).val());
    sNombreItem = ($( '#txt-No_Producto' ).val().length === 0 ? '-' : $( '#txt-No_Producto' ).val());
    iTipoReporte = $('[name="radio-tipo-reporte-compras_x_proveedor"]:checked').attr('value');
    ID_OI_Detalle = ($('#txt-Filtro_OI').val().length == 0 ? '-' : $('#txt-Filtro_OI').val());
      
    if ($(this).data('type') == 'html') {
      $( '#btn-html_compras_x_proveedor' ).text('');
      $( '#btn-html_compras_x_proveedor' ).attr('disabled', true);
      $( '#btn-html_compras_x_proveedor' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
      $( '#table-compras_x_proveedor > tbody' ).empty();
      $( '#table-compras_x_proveedor > tfoot' ).empty();
        
      var arrPost = {
        Fe_Inicio : Fe_Inicio,
        Fe_Fin : Fe_Fin,
        ID_Tipo_Documento : ID_Tipo_Documento,
        ID_Serie_Documento : ID_Serie_Documento,
        ID_Numero_Documento : ID_Numero_Documento,
        Nu_Estado_Documento : Nu_Estado_Documento, 

        // M044 - INICIO 
        // iIdProveedor : iIdProveedor,
        // sNombreProveedor : sNombreProveedor, 
        
        iIdCliente : iIdCliente,
        sNombreCliente : sNombreCliente, 

        // M044 - FIN

        iIdItem : iIdItem,
        sNombreItem: sNombreItem,
        ID_OI_Detalle: ID_OI_Detalle,
      };

      url = base_url + 'Logistica/informes_logistica/ReporteActivosController/sendReporte';
      $.post( url, arrPost, function( response ){
        if ( response.sStatus == 'success' ) {
          var iTotalRegistros = response.arrData.length, tr_body = '', tr_foot = '';
          var ID_Entidad = '', counter = 0, sum_compras_cantidad = 0.000000, sum_compras_subtotal_s = 0.00, sum_compras_descuento_s = 0.00, sum_compras_igv_s = 0.00, sum_compras_total_s = 0.00, sum_compras_total_d = 0.00;
          var subtotal_s = 0.00, descuento_s = 0.00, igv_s = 0.00, total_s = 0.00;
          var sum_general_compras_cantidad = 0.000000, sum_general_compras_subtotal_s = 0.00, sum_general_compras_descuento_s = 0.00, sum_general_compras_igv_s = 0.00, sum_general_compras_total_s = 0.00, sum_general_compras_total_d = 0.00;
          var response=response.arrData;
          for (var i = 0; i < iTotalRegistros; i++) {
            if (ID_Entidad != response[i].ID_Entidad) {
              if (counter != 0) {
                tr_body +=
                +"<tr>"
                  +"<th class='text-right' colspan='7'>Total </th>"
                  +"<th class='text-right'>" + number_format(sum_compras_cantidad, 6) + "</th>"
                  +"<th class='text-right'></th>"
                  +"<th class='text-right'>" + number_format(sum_compras_subtotal_s, 2) + "</th>"
                  +"<th class='text-right'>" + number_format(sum_compras_igv_s, 2) + "</th>"
                  +"<th class='text-right'>" + number_format(sum_compras_descuento_s, 2) + "</th>"
                  +"<th class='text-right'>" + number_format(sum_compras_total_s, 2) + "</th>"
                  +"<th class='text-right'>" + number_format(sum_compras_total_d, 2) + "</th>"
                  +"<th class='text-right'></th>"
                +"</tr>";
                sum_compras_cantidad = 0.000000;
                sum_compras_subtotal_s = 0.00;
                sum_compras_igv_s = 0.00;
                sum_compras_descuento_s = 0.00;
                sum_compras_total_s = 0.00;
                sum_compras_total_d = 0.00;
              }
              
              tr_body +=
              "<tr>"
                +"<th class='text-right'>Proveedor </th>"
                +"<th class='text-left'>" + response[i].Nu_Documento_Identidad + "</th>"
                +"<th class='text-left' colspan='13'>" + response[i].No_Entidad + "</th>"
              +"</tr>";
              ID_Entidad = response[i].ID_Entidad;
            }
            
            subtotal_s = (!isNaN(parseFloat(response[i].Ss_SubTotal)) ? parseFloat(response[i].Ss_SubTotal) : 0);
            igv_s = (!isNaN(parseFloat(response[i].Ss_IGV)) ? parseFloat(response[i].Ss_IGV) : 0);
            descuento_s = (!isNaN(parseFloat(response[i].Ss_Descuento)) ? parseFloat(response[i].Ss_Descuento) : 0);
            total_s = (!isNaN(parseFloat(response[i].Ss_Total)) ? parseFloat(response[i].Ss_Total) : 0);
            
            if (iTipoReporte==0) {//0=Detallado
              tr_body +=
              "<tr>"
                +"<td class='text-center'>" + response[i].Fe_Emision + "</td>"
                +"<td class='text-center'>" + response[i].No_Tipo_Documento_Breve + "</td>"
                +"<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
                +"<td class='text-right'>" + response[i].ID_Numero_Documento + "</td>"
                +"<td class='text-center'>" + response[i].No_Signo + "</td>"
                +"<td class='text-left'>" + (response[i].Nu_Codigo_Barra != '' ? (response[i].Nu_Codigo_Barra + " - ") : "") + response[i].No_Producto + "</td>"

                // M023 - FIX - I  
                +"<td class='text-center'>" + response[i].No_Sub_Familia + "</td>"
                +"<td class='text-center'>" + response[i].No_Marca + "</td>"
                +"<td class='text-center'>" + response[i].Modelo_Tipo + "</td>"
                +"<td class='text-center'>" + response[i].Contenido + "</td>"
                // M023 - FIX - F 

                +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Qt_Producto, 6) + "</td>"
                +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Precio, 6) + "</td>"
                +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</span></td>"
                +"<td class='text-center'>" + response[i].ID_Activo_Empresa + "</td>"
                +"<td class='text-center'>" + response[i].Fe_Salida_Item + "</td>"

                // M023 - I
                +"<td class='text-center'>" + response[i].receptor + "</td>"
                +"<td class='text-center'>" + response[i].solic_loc + "</td>"
                +"<td class='text-center'>" + response[i].Tipo_Activo + "</td>"
                // M023 - F 

                +"<td class='text-center'>" + response[i].No_Area_Ingreso + "</td>"
                +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado_Activo + "'>" + response[i].No_Estado_Activo + "</span></td>"
              +"</tr>";
            }
            counter++;
          }// /. for
        } else {
          tr_body +=
          "<tr>"
            + "<td colspan='15' class='text-center'>No hay registros</td>"
          + "</tr>";
        }
        
        $( '#div-compras_x_proveedor' ).show();
        $( '#table-compras_x_proveedor > tbody' ).append(tr_body);
        $( '#table-compras_x_proveedor > tbody' ).after(tr_foot);
        
        $( '#btn-html_compras_x_proveedor' ).text('');
        $( '#btn-html_compras_x_proveedor' ).append( '<i class="fa fa-search"></i> Buscar' );
        $( '#btn-html_compras_x_proveedor' ).attr('disabled', false);
      }, 'JSON')
      .fail(function(jqXHR, textStatus, errorThrown) {
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
        
        //Message for developer
        console.log(jqXHR.responseText);
        
        $( '#btn-html_compras_x_proveedor' ).text('');
        $( '#btn-html_compras_x_proveedor' ).append( '<i class="fa fa-search"></i> Buscar' );
        $( '#btn-html_compras_x_proveedor' ).attr('disabled', false);
      });
    } else if ($(this).data('type') == 'pdf') {
      $( '#btn-pdf_compras_x_proveedor' ).text('');
      $( '#btn-pdf_compras_x_proveedor' ).attr('disabled', true);
      $( '#btn-pdf_compras_x_proveedor' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
          
      url = base_url + 'Logistica/informes_logistica/ReporteActivosController/sendReportePDF/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Tipo_Documento + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Estado_Documento + '/' + iIdProveedor + '/' + sNombreProveedor + '/' + iIdItem + '/' + sNombreItem + '/' + iTipoReporte + '/' + ID_OI_Detalle;
      window.open(url,'_blank');
      
      $( '#btn-pdf_compras_x_proveedor' ).text('');
      $( '#btn-pdf_compras_x_proveedor' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
      $( '#btn-pdf_compras_x_proveedor' ).attr('disabled', false);
    } else if ($(this).data('type') == 'excel') {
      $( '#btn-excel_compras_x_proveedor' ).text('');
      $( '#btn-excel_compras_x_proveedor' ).attr('disabled', true);
      $( '#btn-excel_compras_x_proveedor' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      
      url = base_url + 'Logistica/informes_logistica/ReporteActivosController/sendReporteEXCEL/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Tipo_Documento + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Estado_Documento + '/' + iIdProveedor + '/' + sNombreProveedor + '/' + iIdItem + '/' + sNombreItem + '/' + iTipoReporte + '/' + ID_OI_Detalle;
      window.open(url,'_blank');
      
      $( '#btn-excel_compras_x_proveedor' ).text('');
      $( '#btn-excel_compras_x_proveedor' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
      $( '#btn-excel_compras_x_proveedor' ).attr('disabled', false);
    }
  })//./ btn
})