var url;

$(function () {
  $('.select2').select2();
  
  $( '.div-fecha_stock_valorizado' ).hide();
  $( '.div-productos' ).hide();
  
  url = base_url + 'HelperController/getAlmacenes';
  $.post( url, function( response ){
    $( '#cbo-Almacenes_Stock_Valorizado' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-Almacenes_Stock_Valorizado' ).append( '<option value="' + response[i].ID_Almacen + '">' + response[i].No_Almacen + '</option>' );
  }, 'JSON');
  
  url = base_url + 'HelperController/getLineas';
  $.post( url, function( response ){
    $( '#cbo-Lineas_Stock_Valorizado' ).html('<option value="0" selected="selected">- Todas -</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-Lineas_Stock_Valorizado' ).append( '<option value="' + response[i].ID_Linea + '">' + response[i].No_Linea + '</option>' );
  }, 'JSON');
  
  $( '#txt-ID_Producto' ).val(0);
  $( '#cbo-FiltrosProducto' ).change(function() {
    $( '.div-productos' ).hide();
    $( '#txt-ID_Producto' ).val(0);
    $( '#txt-No_Producto' ).val('');
    if ( $(this).val() > 0 )
      $( '.div-productos' ).show();
  })
  
  $( '#div-stock_valorizado' ).hide();
  
  $( '.btn-generar_stock_valorizado' ).click(function(){
    if ( $( '#cbo-Almacenes_Stock_Valorizado' ).val() == 0 ) {
      $( '#cbo-Almacenes_Stock_Valorizado' ).closest('.form-group').find('.help-block').html('Seleccionar almacén');
		  $( '#cbo-Almacenes_Stock_Valorizado' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
		  $( '.help-block' ).empty();
		  
      var ID_Almacen, No_Almacen, iTipoFecha, Fe_Inicio = "", Fe_Fin = "", iTipoStock, ID_Linea, ID_Producto, tr_body = "", tr_foot = "";
  
      ID_Almacen = $( '#cbo-Almacenes_Stock_Valorizado option:selected' ).val();
      No_Almacen = $( '#cbo-Almacenes_Stock_Valorizado option:selected' ).text();
      iTipoFecha = $('[name="radio-fecha"]:checked').attr('value');
  
      if ($('[name="radio-fecha"]:checked').attr('value') == 1) {
        Fe_Inicio = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
        Fe_Fin    = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
      } else {
        Fe_Inicio = 0;
        Fe_Fin = 0;
      }
    
      iTipoStock  = $('[name="radio-stock"]:checked').attr('value');
      ID_Linea    = $( '#cbo-Lineas_Stock_Valorizado option:selected' ).val();
      ID_Producto = $( '#txt-ID_Producto' ).val();
  
      var arrPost = {
        ID_Almacen  : ID_Almacen,
        iTipoFecha  : iTipoFecha,
        Fe_Inicio   : Fe_Inicio,
        Fe_Fin      : Fe_Fin,
        iTipoStock  : iTipoStock,
        ID_Linea    : ID_Linea,
        ID_Producto : ID_Producto,
      };
        
      if ($(this).data('type') == 'html') {
        $( '#btn-html_stock_valorizado' ).text('');
        $( '#btn-html_stock_valorizado' ).attr('disabled', true);
        $( '#btn-html_stock_valorizado' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
    
        $( '#table-stock_valorizado > tbody' ).empty();
        $( '#table-stock_valorizado > tfoot' ).empty();
        
        url = base_url + 'Logistica/informes_logistica/StockValorizadoController/sendReporte';
        $.post( url, arrPost, function( response ){
          if (response.length > 0){
            var ID_Linea = '', counter = 0, sum_linea_cantidad = 0.000000, sum_cantidad = 0.000000;
            for (var i = 0, len = response.length; i < len; i++) {
              if (ID_Linea != response[i].ID_Linea) {
                if (counter != 0) {
                  tr_body +=
                  +"<tr>"
                    +"<th class='text-right' colspan='3'>Total Linea</th>"
                    +"<th class='text-right'>" + number_format(sum_linea_cantidad, 6) + "</th>"
                  +"</tr>";
                  
                  sum_linea_cantidad = 0.000000;
                }
                
                tr_body +=
                "<tr>"
                  +"<th class='text-right'>Linea</th>"
                  +"<th class='text-left' colspan='3'>" + response[i].No_Linea + "</th>"
                +"</tr>";
                
                ID_Linea = response[i].ID_Linea;
              }
              
              tr_body +=
              "<tr>"
                +"<td class='text-center'>" + response[i].Nu_Codigo_Barra + "</td>"
                +"<td class='text-left'>" + response[i].No_Producto + "</td>"
                +"<td class='text-left'>" + response[i].No_Unidad_Medida + "</td>"
                +"<td class='text-right'>" + number_format(response[i].Qt_Producto, 6) + "</td>"
              +"</tr>";
              
              sum_linea_cantidad += parseFloat(response[i].Qt_Producto);
              sum_cantidad += parseFloat(response[i].Qt_Producto);
              counter++;
            }
            
            tr_foot =
            "<tfoot>"
              +"<tr>"
                +"<th class='text-right' colspan='3'>Total Linea</th>"
                +"<th class='text-right'>" + number_format(sum_linea_cantidad, 6) + "</th>"
              +"</tr>"
              +"<tr>"
                +"<th class='text-right' colspan='3'>Total General</th>"
                +"<th class='text-right'>" + number_format(sum_cantidad, 6) + "</th>"
              +"</tr>"
            +"</tfoot>";
          } else {
            tr_body +=
            "<tr>"
              + "<td colspan='4' class='text-center'>No hay registros</td>"
            + "</tr>";
          }
          
          $( '#div-stock_valorizado' ).show();
          $( '#table-stock_valorizado > tbody' ).append(tr_body);
          $( '#table-stock_valorizado > tbody' ).after(tr_foot);
          
          $( '#btn-html_stock_valorizado' ).text('');
          $( '#btn-html_stock_valorizado' ).append( '<i class="fa fa-search"></i> Buscar' );
          $( '#btn-html_stock_valorizado' ).attr('disabled', false);
        }, 'JSON');
      } else if ($(this).data('type') == 'pdf') {
        $( '#btn-pdf_stock_valorizado' ).text('');
        $( '#btn-pdf_stock_valorizado' ).attr('disabled', true);
        $( '#btn-pdf_stock_valorizado' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
            
        url = base_url + 'Logistica/informes_logistica/StockValorizadoController/sendReportePDF/' + ID_Almacen + '/' + iTipoFecha + '/' + Fe_Inicio + '/' + Fe_Fin + '/' + iTipoStock + '/' + ID_Linea + '/' + ID_Producto + '/' + No_Almacen;
        window.open(url,'_blank');
        
        $( '#btn-pdf_stock_valorizado' ).text('');
        $( '#btn-pdf_stock_valorizado' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
        $( '#btn-pdf_stock_valorizado' ).attr('disabled', false);
      } else if ($(this).data('type') == 'excel') {
		    $( '#btn-excel_stock_valorizado' ).text('');
        $( '#btn-excel_stock_valorizado' ).attr('disabled', true);
        $( '#btn-excel_stock_valorizado' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
        
        url = base_url + 'Logistica/informes_logistica/StockValorizadoController/sendReporteEXCEL/' + ID_Almacen + '/' + iTipoFecha + '/' + Fe_Inicio + '/' + Fe_Fin + '/' + iTipoStock + '/' + ID_Linea + '/' + ID_Producto + '/' + No_Almacen;
        window.open(url,'_blank');
        
        $( '#btn-excel_stock_valorizado' ).text('');
        $( '#btn-excel_stock_valorizado' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
        $( '#btn-excel_stock_valorizado' ).attr('disabled', false);
		  }
    }//./ Validacion
  })//./ btn
})

function verFecha(tipo){
  $( '.div-fecha_stock_valorizado' ).hide();
  if (tipo === '1')
    $( '.div-fecha_stock_valorizado' ).show();
}