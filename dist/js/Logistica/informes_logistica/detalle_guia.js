var url;

$(function () {
  $('.select2').select2();
  $('.div-proveedores').hide();
  $('.div-productos').hide();

  $('#cbo-Almacenes_Detalle_Guia').html('<option value="0" selected="selected">- Vacío -</option>');
  url = base_url + 'HelperController/getAlmacenes';
  $.post(url, function (response) {
    if (response.length == 1) {
      $('#cbo-Almacenes_Detalle_Guia').html('<option value="' + response[0].ID_Almacen + '">' + response[0].No_Almacen + '</option>');
    } else {
      $('#cbo-Almacenes_Detalle_Guia').html('<option value="0" selected="selected">- Seleccionar -</option>');
      for (var i = 0; i < response.length; i++)
        $('#cbo-Almacenes_Detalle_Guia').append('<option value="' + response[i].ID_Almacen + '">' + response[i].No_Almacen + '</option>');
    }
  }, 'JSON');

  url = base_url + 'HelperController/getTiposDocumentos';
  $.post(url, { Nu_Tipo_Filtro: 7 }, function (response) {//2 = Compra
    $('#cbo-filtros_tipos_documento').html('<option value="0" selected="selected">- Todos -</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-filtros_tipos_documento').append('<option value="' + response[i]['ID_Tipo_Documento'] + '" data-nu_impuesto="' + response[i]['Nu_Impuesto'] + '" data-nu_enlace="' + response[i]['Nu_Enlace'] + '">' + response[i]['No_Tipo_Documento_Breve'] + '</option>');
  }, 'JSON');

  $('#txt-AID').val(0);
  $('#cbo-FiltrosProveedorGuias').change(function () {
    $('.div-proveedores').hide();
    $('#txt-AID').val(0);
    $('#txt-Filtro_Entidad').val('');
    if ($(this).val() > 0)
      $('.div-proveedores').show();
  })


  $('#txt-ID_Producto').val(0);
  $('#cbo-FiltrosProductoGuias').change(function () {
    $('.div-productos').hide();
    $('#txt-ID_Producto').val(0);
    $('#txt-No_Producto').val('');
    if ($(this).val() > 0)
      $('.div-productos').show();
  })

  $('#div-detalle_guia').hide();

  $('.btn-generar_detalle_guia').click(function () {
    if ($('#cbo-Almacenes_Detalle_Guia').val() == 0) {
      $('#cbo-Almacenes_Detalle_Guia').closest('.form-group').find('.help-block').html('Seleccionar almacén');
      $('#cbo-Almacenes_Detalle_Guia').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $('.help-block').empty();

      var ID_Almacen, No_Almacen, Fe_Inicio, Fe_Fin, ID_Serie_Documento, ID_Numero_Documento, Nu_Tipo_Movimiento, Nu_Estado_Documento, ID_Proveedor, ID_Producto, tr_body = "", tr_foot = "", ID_OI_Detalle, ID_OI_Detalle_Numero;

      ID_Almacen = $('#cbo-Almacenes_Detalle_Guia option:selected').val();
      No_Almacen = $('#cbo-Almacenes_Detalle_Guia option:selected').text();
      Fe_Inicio = ParseDateString($('#txt-Filtro_Fe_Inicio').val(), 1, '/');
      Fe_Fin = ParseDateString($('#txt-Filtro_Fe_Fin').val(), 1, '/');
      ID_Tipo_Documento = $('#cbo-filtros_tipos_documento option:selected').val();
      ID_Serie_Documento = ($('#txt-Filtro_SerieDocumento').val().length === 0 ? '-' : $('#txt-Filtro_SerieDocumento').val());
      ID_Numero_Documento = ($('#txt-Filtro_NumeroDocumento').val().length === 0 ? '-' : $('#txt-Filtro_NumeroDocumento').val());
      Nu_Tipo_Movimiento = $('#cbo-tipo_movimiento option:selected').val();
      Nu_Estado_Documento = $('#cbo-estado_documento option:selected').val();
      ID_Proveedor = $('#txt-AID').val();
      ID_Producto = $('#txt-ID_Producto').val();
      ID_OI_Detalle = ($('#txt-Filtro_OI').val().length == 0 ? '-' : $('#txt-Filtro_OI').val());
      ID_OI_Detalle_Numero = ($('#txt-Filtro_Numero_OI').val().length == 0 ? '-' : $('#txt-Filtro_Numero_OI').val());

      var arrPost = {
        ID_Almacen: ID_Almacen,
        Fe_Inicio: Fe_Inicio,
        Fe_Fin: Fe_Fin,
        ID_Tipo_Documento: ID_Tipo_Documento,
        ID_Serie_Documento: ID_Serie_Documento,
        ID_Numero_Documento: ID_Numero_Documento,
        Nu_Tipo_Movimiento: Nu_Tipo_Movimiento,
        Nu_Estado_Documento: Nu_Estado_Documento,
        ID_Proveedor: ID_Proveedor,
        ID_Producto: ID_Producto,
        ID_OI_Detalle: ID_OI_Detalle,
        ID_OI_Detalle_Numero: ID_OI_Detalle_Numero,
      };

      if ($(this).data('type') == 'html') {
        $('#btn-html_detalle_guia').text('');
        $('#btn-html_detalle_guia').attr('disabled', true);
        $('#btn-html_detalle_guia').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

        $('#table-detalle_guia > tbody').empty();
        $('#table-detalle_guia > tfoot').empty();

        url = base_url + 'Logistica/informes_logistica/DetalleGuiaController/sendReporte';
        $.post(url, arrPost, function (response) {
          if (response.length > 0) {
            var ID_Guia_Cabecera = '', counter = 0, sum_guia_cantidad = 0.000000, sum_guia_total_s = 0.00, sum_guia_total_d = 0.00;
            for (var i = 0, len = response.length; i < len; i++) {
              if (ID_Guia_Cabecera != response[i].ID_Guia_Cabecera) {
                if (counter != 0) {
                  tr_body +=
                    +"<tr>"
                    + "<th class='text-right' colspan='11'>Total Guía</th>"
                    + "<th class='text-right'>" + number_format(sum_guia_cantidad, 6) + "</th>"
                    + "<th class='text-right'></th>"
                    + "<th class='text-right'>" + number_format(sum_guia_total_s, 2) + "</th>"
                    + "<th class='text-right'>" + number_format(sum_guia_total_d, 2) + "</th>"
                    + "<th class='text-right'></th>"
                    + "</tr>";
                  sum_guia_cantidad = 0.000000;
                  sum_guia_total_s = 0.00;
                  sum_guia_total_d = 0.00;
                }

                tr_body +=
                  "<tr>"
                  + "<th class='text-left'>" + response[i].ID_Serie_Documento + "</th>"
                  + "<th class='text-left'>" + response[i].ID_Numero_Documento + "</th>"
                  + "<th class='text-center'>" + response[i].Fe_Emision + "</th>"
                  + "<th class='text-center'>" + response[i].Nu_Documento_Identidad + "</th>"
                  + "<th class='text-left'>" + response[i].No_Entidad + "</th>"
                  + "<th class='text-left'>" + (response[i].ID_Serie_Documento_Factura !== null ? response[i].ID_Serie_Documento_Factura : '') + "</th>"
                  + "<th class='text-left'>" + (response[i].ID_Numero_Documento_Factura !== null ? response[i].ID_Numero_Documento_Factura : '') + "</th>"
                  + "<th class='text-right' colspan='8'></th>"
                  + "<th class='text-center'>" + response[i].No_Estado + "</th>"
                  + "<th class='text-center'>" + response[i].No_Tipo_Movimiento + "</th>"
                + "<th class='text-center'>" + response[i].iIdOrdenIngreso + "</th>"
                + "<th class='text-center'>" + response[i].sDocumentoOI + "</th>"
                  + "</tr>";
                ID_Guia_Cabecera = response[i].ID_Guia_Cabecera;
              }

              if (response[i].Qt_Producto !== null && response[i].Ss_Precio !== null) {
                tr_body +=
                  "<tr>"
                  + "<td class='text-right' colspan='7'></td>"
                  + "<td class='text-right'>" + response[i].No_Signo + "</td>"
                  + "<td class='text-right'>" + number_format(response[i].Ss_Tipo_Cambio, 3) + "</td>"
                  + "<td class='text-left'>" + response[i].Nu_Codigo_Barra + "</td>"
                  + "<td class='text-left'>" + response[i].No_Producto + "</td>"
                  + "<td class='text-right'>" + response[i].Qt_Producto + "</td>"
                  + "<td class='text-right'>" + response[i].Ss_Precio + "</td>"
                  + "<td class='text-right'>" + number_format(response[i].Ss_SubTotal, 2) + "</td>"
                  + "<td class='text-right'>" + (response[i].ID_Moneda === '1' ? '0.00' : number_format(parseFloat(response[i].Ss_SubTotal) * parseFloat(response[i].Ss_Tipo_Cambio), 2)) + "</td>"
                  + "<td class='text-right'></td>"
                  + "</tr>";
                sum_guia_cantidad += parseFloat(response[i].Qt_Producto);
                sum_guia_total_s += parseFloat(response[i].Ss_SubTotal);
                sum_guia_total_d += (response[i].ID_Moneda === '2') ? parseFloat(response[i].Ss_SubTotal) : 0;
              }

              counter++;
            }

            tr_foot =
              "<tfoot>"
              + "<tr>"
              + "<th class='text-right' colspan='11'>Total Guía</th>"
              + "<th class='text-right'>" + number_format(sum_guia_cantidad, 6) + "</th>"
              + "<th class='text-right'></th>"
              + "<th class='text-right'>" + number_format(sum_guia_total_s, 2) + "</th>"
              + "<th class='text-right'>" + number_format(sum_guia_total_d, 2) + "</th>"
              + "<th class='text-right'></th>"
              + "</tr>"
              + "</tfoot>";
          } else {
            tr_body +=
              "<tr>"
              + "<td colspan='16' class='text-center'>No hay registros</td>"
              + "</tr>";
          }

          $('#div-detalle_guia').show();
          $('#table-detalle_guia > tbody').append(tr_body);
          $('#table-detalle_guia > tbody').after(tr_foot);

          $('#btn-html_detalle_guia').text('');
          $('#btn-html_detalle_guia').append('<i class="fa fa-search"></i> Buscar');
          $('#btn-html_detalle_guia').attr('disabled', false);
        }, 'JSON')
          .fail(function (jqXHR, textStatus, errorThrown) {
            $('.modal-message').removeClass('modal-danger modal-warning modal-success');

            $('#modal-message').modal('show');
            $('.modal-message').addClass('modal-danger');
            $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

            //Message for developer
            console.log(jqXHR.responseText);

            $('#btn-html_detalle_guia').text('');
            $('#btn-html_detalle_guia').append('<i class="fa fa-search"></i> Buscar');
            $('#btn-html_detalle_guia').attr('disabled', false);
          });
      } else if ($(this).data('type') == 'pdf') {
        $('#btn-pdf_detalle_guia').text('');
        $('#btn-pdf_detalle_guia').attr('disabled', true);
        $('#btn-pdf_detalle_guia').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

        url = base_url + 'Logistica/informes_logistica/DetalleGuiaController/sendReportePDF/' + ID_Almacen + '/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Tipo_Movimiento + '/' + Nu_Estado_Documento + '/' + ID_Proveedor + '/' + ID_Producto + '/' + No_Almacen + '/' + ID_Tipo_Documento + '/' + ID_OI_Detalle + '/' + ID_OI_Detalle_Numero;
        window.open(url, '_blank');

        $('#btn-pdf_detalle_guia').text('');
        $('#btn-pdf_detalle_guia').append('<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF');
        $('#btn-pdf_detalle_guia').attr('disabled', false);
      } else if ($(this).data('type') == 'excel') {
        $('#btn-excel_detalle_guia').text('');
        $('#btn-excel_detalle_guia').attr('disabled', true);
        $('#btn-excel_detalle_guia').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

        url = base_url + 'Logistica/informes_logistica/DetalleGuiaController/sendReporteEXCEL/' + ID_Almacen + '/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Tipo_Movimiento + '/' + Nu_Estado_Documento + '/' + ID_Proveedor + '/' + ID_Producto + '/' + No_Almacen + '/' + ID_Tipo_Documento + '/' + ID_OI_Detalle + '/' + ID_OI_Detalle_Numero;
        window.open(url, '_blank');

        $('#btn-excel_detalle_guia').text('');
        $('#btn-excel_detalle_guia').append('<i class="fa fa-file-excel-o color_icon_excel"></i> Excel');
        $('#btn-excel_detalle_guia').attr('disabled', false);
      }
    }//./ Validacion
  })//./ btn
})