var save_method;
var url;
var table_marca;
var accion_marca;

$(function () {
	$(document).keyup(function(event){
    if(event.which == 27){//ESC
      $( "#modal-ConfiguracionContableProducto" ).modal('hide');
    }
	});
	
  $('.select2').select2();
  $('[data-mask]').inputmask()
  
  url = base_url + 'Logistica/ReglasLogistica/ConfiguracionContableProductoController/ajax_list';
  table_marca = $( '#table-ConfiguracionContableProducto' ).DataTable({
    'dom'       : 'B<"top">frt<"bottom"lp><"clear">',
    buttons     : [{
      extend    : 'excel',
      text      : '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr : 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'pdf',
      text      : '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr : 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'colvis',
      text      : '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr : 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching'   : false,
    'bStateSave'  : true,
    'processing'  : true,
    'serverSide'  : true,
    'info'        : true,
    'autoWidth'   : false,
    'pagingType'  : 'full_numbers',
    'oLanguage' : {
      'sInfo'              : 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu'        : '_MENU_',
      'sSearch'            : 'Buscar por: ',
      'sSearchPlaceholder' : 'UPC / Nombre',
      'sZeroRecords'       : 'No se encontraron registros',
      'sInfoEmpty'         : 'No hay registros',
      'sLoadingRecords'    : 'Cargando...',
      'sProcessing'        : 'Procesando...',
      'oPaginate'          : {
        'sFirst'    : '<<',
        'sLast'     : '>>',
        'sPrevious' : '<',
        'sNext'     : '>',
      },
    },
    'order': [],
    'ajax': {
      'url'       : url,
      'type'      : 'POST',
      'dataType'  : 'json',
      'data'      : function ( data ) {
        data.Filtros_ConfiguracionContableProductos = $( '#cbo-Filtros_ConfiguracionContableProductos' ).val(),
        data.Global_Filter = $( '#txt-Global_Filter' ).val();
      },
    },
    'columnDefs': [{
      'className' : 'text-center',
      'targets'   : 'no-sort',
      'orderable' : false,
    },],
  });
  
  $( '.dataTables_length' ).addClass('col-md-3');
  $( '.dataTables_paginate' ).addClass('col-md-9');
  
  $( '#txt-Global_Filter' ).keyup(function() {
    table_marca.search($(this).val()).draw();
  });
  
  $( '#form-ConfiguracionContableProducto' ).validate({
		rules:{
      ID_Configuracion_Cuenta_Contable: {
				required: true,
      },
      ID_Producto: {
        required: true,
      },
		},
		messages:{
      ID_Configuracion_Cuenta_Contable:{
        required: "Seleccionar cuenta"
      },
      ID_Producto: {
        required: "Seleccionar item"
      },
		},
		errorPlacement : function(error, element) {
		  $(element).closest('.form-group').find('.help-block').html(error.html());
	  },
		highlight : function(element) {
	    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			$(element).closest('.form-group').find('.help-block').html('');
    },
		submitHandler: form_ConfiguracionContableProducto
	});
	
  $(document).bind('keydown', 'f2', function(){
    agregarConfiguracionContableProducto();
  });
})

function agregarConfiguracionContableProducto(){
  accion_marca='add_marca';
  
  $( '#modal-loader' ).modal('show');
  
  $( '#form-ConfiguracionContableProducto' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
  $( '#modal-ConfiguracionContableProducto' ).modal('show');
  $( '.modal-title' ).text('Nueva ConfiguracionContableProducto');
  
	$( '#modal-ConfiguracionContableProducto' ).on('shown.bs.modal', function() {
		$( '#txt-Nu_Cuenta_Contable' ).focus();
	})
  
  $( '[name="EID_Empresa"]' ).val('');
  $( '[name="EID_Configuracion_Cuenta_Contable_Producto"]' ).val('');
  $('[name="EID_Configuracion_Cuenta_Contable"]').val('');
  $( '[name="EID_Producto"]' ).val('');

  $('#cbo-Empresas').html('<option value="0" selected="selected">- Seleccionar -</option>');
  $('#cbo-ID_Configuracion_Cuenta_Contable').html('<option value="0" selected="selected">- Seleccionar -</option>');
  $('#cbo-ID_Producto').html('<option value="0" selected="selected">- Seleccionar -</option>');


  $( '#modal-loader' ).modal('show');
  url = base_url + 'HelperController/getEmpresas';
  $.post( url , function( response ){
    $( '#cbo-Empresas' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-Empresas' ).append( '<option value="' + response[i].ID_Empresa + '">' + response[i].No_Empresa + '</option>' );
    $( '#modal-loader' ).modal('hide');
  }, 'JSON');

  url = base_url + 'HelperController/getCuentasContables';
  $.post(url, function (response) {
    if (response.sStatus == 'success') {
      var l = response.arrData.length;
      if (l == 1) {
        $('#cbo-ID_Configuracion_Cuenta_Contable').html('<option value="' + response.arrData[0].ID_Configuracion_Cuenta_Contable + '">' + (response.arrData[0].Nu_Tipo_Cuenta == 0 ? 'Debe' : 'Haber') + ': ' + response.arrData[0].Nu_Cuenta_Contable + '</option>');
      } else {
        $('#cbo-ID_Configuracion_Cuenta_Contable').html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var x = 0; x < l; x++) {
          $('#cbo-ID_Configuracion_Cuenta_Contable').append('<option value="' + response.arrData[x].ID_Configuracion_Cuenta_Contable + '">' + (response.arrData[x].Nu_Tipo_Cuenta == 0 ? 'Debe' : 'Haber') + ': ' + response.arrData[x].Nu_Cuenta_Contable + '</option>');
        }
      }
    } else {
      if (response.sMessageSQL !== undefined) {
        console.log(response.sMessageSQL);
      }
      if (response.sStatus != 'warning') {
        $('#modal-message').modal('show');
        $('.modal-message').addClass(response.sClassModal);
        $('.modal-title-message').text(response.sMessage);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
      }
    }
  }, 'JSON');

  url = base_url + 'HelperController/getProductos';
  $.post(url, function (response) {
    if (response.sStatus == 'success') {
      var l = response.arrData.length;
      if (l == 1) {
        $('#cbo-ID_Producto').html('<option value="' + response.arrData[0].ID_Producto + '">' + response.arrData[0].No_Producto + '</option>');
      } else {
        $('#cbo-ID_Producto').html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var x = 0; x < l; x++) {
          $('#cbo-ID_Producto').append('<option value="' + response.arrData[x].ID_Producto + '">' + response.arrData[x].No_Producto + '</option>');
        }
      }
    } else {
      if (response.sMessageSQL !== undefined) {
        console.log(response.sMessageSQL);
      }
      if (response.sStatus != 'warning') {
        $('#modal-message').modal('show');
        $('.modal-message').addClass(response.sClassModal);
        $('.modal-title-message').text(response.sMessage);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
      }
    }
  }, 'JSON');

  $( '.div-Estado' ).hide();
  $( '#cbo-Estado' ).html( '<option value="1">Activo</option>' );
 
  save_method = 'add';
}

function verConfiguracionContableProducto(ID){
  accion_marca='upd_marca';
  
  $( '#form-ConfiguracionContableProducto' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
  $( '#modal-loader' ).modal('show');
  
	save_method = 'update';
	
  url = base_url + 'Logistica/ReglasLogistica/ConfiguracionContableProductoController/ajax_edit/' + ID;
  $.ajax({
    url : url,
    type: "GET",
    dataType: "JSON",
    success: function(response){
      $( '#modal-ConfiguracionContableProducto' ).modal('show');
      $( '.modal-title' ).text('Modifcar ConfiguracionContableProducto');
  
    	$( '#modal-ConfiguracionContableProducto' ).on('shown.bs.modal', function() {
    		$( '#txt-Nu_Cuenta_Contable' ).focus();
    	})
      
      $( '[name="EID_Empresa"]' ).val(response.ID_Empresa);
      $( '[name="EID_Configuracion_Cuenta_Contable_Producto"]' ).val(response.ID_Configuracion_Cuenta_Contable_Producto);
      $('[name="EID_Configuracion_Cuenta_Contable"]').val(response.ID_Configuracion_Cuenta_Contable);
      $('[name="EID_Producto"]').val(response.ID_Producto);
      
      var selected;
      url = base_url + 'HelperController/getEmpresas';
      $.post( url , function( responseEmpresa ){
        $( '#cbo-Empresas' ).html('');
        for (var i = 0; i < responseEmpresa.length; i++){
          selected = '';
          if(response.ID_Empresa == responseEmpresa[i].ID_Empresa)
            selected = 'selected="selected"';
          $( '#cbo-Empresas' ).append( '<option value="' + responseEmpresa[i].ID_Empresa + '" ' + selected + '>' + responseEmpresa[i].No_Empresa + '</option>' );
        }
      }, 'JSON');

      url = base_url + 'HelperController/getCuentasContables';
      $.post(url, function (responseCuenta) {
        $('#cbo-ID_Configuracion_Cuenta_Contable').html('<option value="">- Seleccionar -</option>');
        if (responseCuenta.sStatus == 'success') {
          var l = responseCuenta.arrData.length;
          for (var x = 0; x < l; x++) {
            selected = '';
            if (response.ID_Configuracion_Cuenta_Contable == responseCuenta.arrData[x].ID_Configuracion_Cuenta_Contable)
              selected = 'selected="selected"';
            $('#cbo-ID_Configuracion_Cuenta_Contable').append('<option value="' + responseCuenta.arrData[x].ID_Configuracion_Cuenta_Contable + '" ' + selected + '>' + (responseCuenta.arrData[x].Nu_Tipo_Cuenta == 0 ? 'Debe' : 'Haber') + ': ' + responseCuenta.arrData[x].Nu_Cuenta_Contable + '</option>');
          }
        } else {
          if (responseCuenta.sMessageSQL !== undefined) {
            console.log(responseCuenta.sMessageSQL);
          }
          if (responseCuenta.sStatus == 'warning')
            $('#cbo-ID_Configuracion_Cuenta_Contable').html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getProductos';
      $.post(url, function (responseItem) {
        $('#cbo-ID_Producto').html('<option value="">- Seleccionar -</option>');
        if (responseItem.sStatus == 'success') {
          var l = responseItem.arrData.length;
          for (var x = 0; x < l; x++) {
            selected = '';
            if (response.ID_Producto == responseItem.arrData[x].ID_Producto)
              selected = 'selected="selected"';
            $('#cbo-ID_Producto').append('<option value="' + responseItem.arrData[x].ID_Producto + '" ' + selected + '>' + responseItem.arrData[x].No_Producto + '</option>');
          }
        } else {
          if (responseItem.sMessageSQL !== undefined) {
            console.log(responseItem.sMessageSQL);
          }
          if (responseItem.sStatus == 'warning')
            $('#cbo-ID_Producto').html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');

      $( '.div-Estado' ).show();
      $( '#cbo-Estado' ).html( '' );      
      selected='';
      for (var i = 0; i < 2; i++){
        selected = '';
        if(response.Nu_Estado == i)
          selected = 'selected="selected"';
        $( '#cbo-Estado' ).append( '<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'Inactivo' : 'Activo') + '</option>' );
      }
      
      $( '#modal-loader' ).modal('hide');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $( '#modal-loader' ).modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    }
  });
}

function form_ConfiguracionContableProducto(){
  if ( accion_marca=='add_marca' || accion_marca=='upd_marca' ) {
    $( '#btn-save' ).text('');
    $( '#btn-save' ).attr('disabled', true);
    $( '#btn-save' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
    
    $( '#modal-loader' ).modal('show');
    
    url = base_url + 'Logistica/ReglasLogistica/ConfiguracionContableProductoController/crudConfiguracionContableProducto';
    
    $.ajax({
      dataType  : 'JSON',
      type      : 'POST',
      url       : url,
		  data		  : $( '#form-ConfiguracionContableProducto' ).serialize(),
      success: function(response) {
  		  $( '#modal-loader' ).modal('hide');
  		  
  	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
    	  $( '#modal-message' ).modal('show');
  		  
  		  if (response.status == 'success'){
  		    accion_marca='';
  		    $('#modal-ConfiguracionContableProducto').modal('hide');
    	    $( '.modal-message' ).addClass(response.style_modal);
    	    $( '.modal-title-message' ).text(response.message);
    	    setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
    	    reload_table_marca();
  		  } else {
    	    $( '.modal-message' ).addClass(response.style_modal);
    	    $( '.modal-title-message' ).text(response.message);
    	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
  	    }
  	    
        $( '#btn-save' ).text('');
        $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar' );
        $( '#btn-save' ).attr('disabled', false);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        $( '#modal-loader' ).modal('hide');
  	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	    
    	  $( '#modal-message' ).modal('show');
  	    $( '.modal-message' ).addClass( 'modal-danger' );
  	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
  	    
  	    //Message for developer
        console.log(jqXHR.responseText);
  	    
        $( '#btn-save' ).text('');
        $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar' );
        $( '#btn-save' ).attr('disabled', false);
      }
    });
  }
}

function eliminarConfiguracionContableProducto(ID, accion_marca){
  var $modal_delete = $( '#modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });
  
  $(document).bind('keydown', 'alt+l', function(){
    if ( accion_marca=='delete' ) {
      _eliminarConfiguracionContableProducto($modal_delete, ID);
      accion_marca='';
    }
  });

  $( '#btn-save-delete' ).off('click').click(function () {
    _eliminarConfiguracionContableProducto($modal_delete, ID);
  });
}

function _eliminarConfiguracionContableProducto($modal_delete, ID){
  $( '#modal-loader' ).modal('show');
  
  url = base_url + 'Logistica/ReglasLogistica/ConfiguracionContableProductoController/eliminarConfiguracionContableProducto/' + ID;
  $.ajax({
    url       : url,
    type      : "GET",
    dataType  : "JSON",
    success: function( response ){
      $( '#modal-loader' ).modal('hide');
      
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
		    accion_marca='';
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
  	    reload_table_marca();
		  } else {
		    accion_marca='';
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1500);
		  }
    },
    error: function (jqXHR, textStatus, errorThrown) {
		  accion_marca='';
      $( '#modal-loader' ).modal('hide');
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function reload_table_marca(){
  table_marca.ajax.reload(null,false);
}