var url;
var table_producto;
var accion_producto = '';

function importarExcelProductos(){
  $( ".modal_importar_producto" ).modal( "show" );
}

$(function () {
  $("#myInput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("#table-Producto tr").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  // Validate exist file excel product
	$( document ).on('click', '#btn-excel-importar_producto', function(event) {
	  if ( $( "#my-file-selector" ).val().length === 0 ) {
      $( '#my-file-selector' ).closest('.form-group').find('.help-block').html('Seleccionar archivo');
		  $( '#my-file-selector' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	  } else {
      $( '#btn-cancel-product' ).attr('disabled', true);
      $( '#a-download-product' ).attr('disabled', true);
	    
      $( '#btn-excel-importar_producto' ).text('');
      $( '#btn-excel-importar_producto' ).attr('disabled', true);
      $( '#btn-excel-importar_producto' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
      $( '#modal-loader' ).modal('show');
	  }
  })
  
  $( '.div-AgregarEditar' ).hide();
  
  $('.select2').select2();
  
  url = base_url + 'Logistica/ReglasLogistica/ProductoController/ajax_list';
  table_producto = $('#table-Producto').DataTable({
    'dom': 'B<"top">frt<"bottom"lip><"clear">',
    buttons     : [{
      extend    : 'excel',
      text      : '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr : 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'pdf',
      text      : '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr : 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'colvis',
      text      : '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr : 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching'   : false,
    'bStateSave'  : true,
    'processing'  : true,
    'serverSide'  : true,
    'info'        : true,
    'autoWidth'   : false,
    'pagingType'  : 'full_numbers',
    'oLanguage' : {
      'sInfo'               : 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu'         : '_MENU_',
      'sSearch'             : 'Buscar por: ',
      'sSearchPlaceholder'  : 'UPC / Nombre',
      'sZeroRecords'        : 'No se encontraron registros',
      'sInfoEmpty'          : 'No hay registros',
      'sLoadingRecords'     : 'Cargando...',
      'sProcessing'         : 'Procesando...',
      'oPaginate'           : {
        'sFirst'    : '<<',
        'sLast'     : '>>',
        'sPrevious' : '<',
        'sNext'     : '>',
      },
    },
    'order': [],
    'ajax': {
      'url'       : url,
      'type'      : 'POST',
      'dataType'  : 'json',
      'data'      : function ( data ) {
        data.Filtros_Productos = $( '#cbo-Filtros_Productos' ).val(),
        data.Global_Filter = $( '#txt-Global_Filter' ).val();
      },
    },
    'columnDefs': [{
      'className' : 'text-center',
      'targets'   : 'no-sort',
      'orderable' : false,
    },{
      'className' : 'text-right',
      'targets'   : 'sort_right',
      'orderable' : true,
    },],
    'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
  });

  $('.dataTables_length').addClass('col-xs-4 col-sm-5 col-md-1');
  $('.dataTables_info').addClass('col-xs-8 col-sm-7 col-md-4');
  $('.dataTables_paginate').addClass('col-xs-12 col-sm-12 col-md-7');

  $( '#txt-Global_Filter' ).keyup(function() {
    table_producto.search($(this).val()).draw();
  });

  $('#cbo-Filtros_Productos').on('change', function() {
    if (this.value == "Stock") {
      $('#txt-Global_Filter').prop({ type:"number" });
    } else {
      $('#txt-Global_Filter').prop({ type:"text" });
    }
  });

  $( '#form-Producto' ).validate({
		rules:{
			Nu_Tipo_Producto: {
				required: true,
			},

      // M011 - I 
      Nu_Area_Almacen: {
        required: true,
      },
      // M011 - F 

			Nu_Codigo_Barra: {
				required: true,
			},
			No_Producto: {
				required: true,
      },
      ID_Impuesto: {
				required: true,
      },
      ID_Unidad_Medida: {
				required: true,
      },
      ID_Familia: {
				required: true,
      },
      Ss_Precio_Ecommerce_Online: {
				required: true,
      },
      ID_Familia_Marketplace: {
				required: true,
      },
		},
		messages:{
			Nu_Tipo_Producto:{
				required: "Seleccionar grupo",
			},

       // M011 - I 
       Nu_Area_Almacen:{ 
        required: "Seleccionar area",
      },
      // M011 - F 

			Nu_Codigo_Barra:{
				required: "ingresar UPC",
			},
			No_Producto:{
				required: "Ingresar nombre",
			},
			ID_Impuesto:{
				required: "Seleccionar impuesto",
			},
			ID_Unidad_Medida:{
				required: "Seleccionar unidad medida",
			},
			ID_Familia:{
				required: "Seleccionar categoría",
			},
			Ss_Precio_Ecommerce_Online:{
				required: "Ingresar precio",
			},
			ID_Familia_Marketplace:{
				required: "Seleccionar categoría",
			},
		},
		errorPlacement : function(error, element) {
			$(element).closest('.form-group').find('.help-block').html(error.html());
	  },
		highlight : function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			$(element).closest('.form-group').find('.help-block').html('');
	  },
		submitHandler: form_Producto
	});
	
	$( '.div-Producto' ).show();
	$( '#cbo-TiposItem' ).change(function(){
  	$( '.div-Producto' ).show();
	  if ( $(this).val() == 0 || $(this).val() == 2 ){//Servicio o (Interno) No para la venta
    	$( '.div-Producto' ).hide();
	  }
	})
	
  $( '.div-Compuesto' ).hide();
	$( '#table-Producto_Enlace' ).hide();
	$( '#cbo-Compuesto' ).change(function(){
	  $( '.div-Compuesto' ).hide();  
	  if( $(this).val() == 1 ){
	    $( '.div-Compuesto' ).show();
	    $( '#txt-ANombre' ).focus();
    }
	})
	
	$( '#btn-addProductosEnlaces' ).click(function(){
	  var $ID_Producto        = $( '#txt-AID' ).val();
    var $ID_Producto_Enlace = $( '#txt-ACodigo' ).val();
    var $No_Producto_Enlace = $( '#txt-ANombre' ).val();
    var $Qt_Producto_Enlace = $( '#txt-Qt_Producto_Descargar' ).val();
  
    if ( $ID_Producto.length === 0 || $No_Producto_Enlace.length === 0) {
	    $( '#txt-ANombre' ).closest('.form-group').find('.help-block').html('Ingresar producto');
			$( '#txt-ANombre' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ( $Qt_Producto_Enlace.length === 0) {
      $( '#txt-Qt_Producto_Descargar' ).closest('.form-group').find('.help-block').html('Ingresar cantidad');
		  $( '#txt-Qt_Producto_Descargar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ( $Qt_Producto_Enlace == 0) {
      $( '#txt-Qt_Producto_Descargar' ).closest('.form-group').find('.help-block').html('La cantidad debe de ser mayor 0');
		  $( '#txt-Qt_Producto_Descargar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      var table_enlace_producto =
      "<tr id='tr_enlace_producto" + $ID_Producto + "'>"
        + "<td class='text-left' style='display:none;'>" + $ID_Producto + "</td>"
        + "<td class='text-left'>" + $ID_Producto_Enlace + "</td>"
        + "<td class='text-left'>" + $No_Producto_Enlace + "</td>"
        + "<td class='text-right'>" + $Qt_Producto_Enlace + "</td>"
        + "<td class='text-center'><button type='button' id='btn-deleteProductoEnlace' class='btn btn-xs btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o' aria-hidden='true'> Eliminar</i></button></td>"
      + "</tr>";
      
	    if( isExistTableTemporalEnlacesProducto($ID_Producto) ){
  	    $( '#txt-ANombre' ).closest('.form-group').find('.help-block').html('Ya existe producto <b>' + $No_Producto_Enlace + '</b>');
  			$( '#txt-ANombre' ).closest('.form-group').removeClass('has-success').addClass('has-error');
  			$( '#txt-AID' ).val('');
  			$( '#txt-ACodigo' ).val('');
  			$( '#txt-ANombre' ).val('');
  			
  			$( '#txt-ANombre' ).focus();
	    } else {
	      $( '#table-Producto_Enlace' ).show();
			  $( '#table-Producto_Enlace' ).append(table_enlace_producto);
			  $( '#txt-AID' ).val('');
			  $( '#txt-ACodigo' ).val('');
  			$( '#txt-ANombre' ).val('');
  			
  			$( '#txt-ANombre' ).focus();
	    }
    }
	})
	
	$( '#table-Producto_Enlace tbody' ).on('click', '#btn-deleteProductoEnlace', function(){
    $(this).closest ('tr').remove ();
    if ($( '#table-Producto_Enlace >tbody >tr' ).length == 0)
	      $( '#table-Producto_Enlace' ).hide();
	})
	
  $(document).bind('keydown', 'f2', function(){
    agregarProducto();
  });
  
	/* Categorías */
	$( '#cbo-categoria' ).change(function(){
    url = base_url + 'HelperController/getDataGeneral';
    var arrParams = {
      sTipoData : 'subcategoria',
      sWhereIdCategoria : $( this ).val(),
    }
    $.post( url, arrParams, function( response ){
      if ( response.sStatus == 'success' ) {
        var l = response.arrData.length;
        if (l==1) {
          $( '#cbo-sub_categoria' ).html( '<option value="' + response.arrData[0].ID + '">' + response.arrData[0].Nombre + '</option>' );
        } else {
          $( '#cbo-sub_categoria' ).html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            $( '#cbo-sub_categoria' ).append( '<option value="' + response.arrData[x].ID + '">' + response.arrData[x].Nombre + '</option>' );
          }
        }
      } else {
        if( response.sMessageSQL !== undefined ) {
          console.log(response.sMessageSQL);
        }
        if ( response.sStatus != 'warning' ) {
          $( '#modal-message' ).modal('show');
          $( '.modal-message' ).addClass(response.sClassModal);
          $( '.modal-title-message' ).text(response.sMessage);
          setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
        }
      }
    }, 'JSON');
  });
  
	$( '#cbo-categoria_marketplace' ).change(function(){
    url = base_url + 'HelperController/getSubCategoriasMarketplace';
    var arrParams = {'iIdFamilia' : $(this).val()}
    $.post( url, arrParams, function( response ){
      if ( response.sStatus == 'success' ) {
        var l = response.arrData.length;
        if (l==1) {
          $( '#cbo-sub_categoria_marketplace' ).html( '<option value="' + response.arrData[0].ID_Sub_Familia + '">' + response.arrData[0].No_Sub_Familia + '</option>' );
        } else {
          $( '#cbo-sub_categoria_marketplace' ).html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            $( '#cbo-sub_categoria_marketplace' ).append( '<option value="' + response.arrData[x].ID_Sub_Familia + '">' + response.arrData[x].No_Sub_Familia + '</option>' );
          }
        }
      } else {
        if( response.sMessageSQL !== undefined ) {
          console.log(response.sMessageSQL);
        }
        console.log(response.sMessage);
      }
    }, 'JSON');
	});
})

function isExistTableTemporalEnlacesProducto($ID_Producto_Enlace){
  return Array.from($('tr[id*=tr_enlace_producto]'))
    .some(element => ($('td:nth(0)',$(element)).html()===$ID_Producto_Enlace));
}

function agregarProducto(){
  accion_producto = 'add_producto';
  $( '[name="EID_Empresa"]' ).focus();
  
  $( '#form-Producto' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();
  $( '.div-AgregarEditar' ).show();
  
  $( '.div-Compuesto' ).hide();
	$( '#table-Producto_Enlace tbody' ).empty();
		
  $( '.title_Producto' ).text('Nuevo Producto');

  $('[name="EID_Empresa"]').val('');
  $('[name="EID_Producto"]').val('');
  $('[name="ENu_Codigo_Barra"]').val('');
  
	$( '.div-Producto' ).show();
	
  $( '#cbo-TiposItem' ).prop('disabled', false);
  
  // M011 - I 
  url = base_url + 'HelperController/getAreaAlmacenProducto';
  $.post( url , function( responseAreaAlmacen ){
    $( '#cbo-AreaAlmacen' ).html( '<option value="">- Seleccionar -</option>' );
    for (var i = 0; i < responseAreaAlmacen.length; i++)
      $( '#cbo-AreaAlmacen' ).append( '<option value="' + responseAreaAlmacen[i].Nu_Valor + '">' + responseAreaAlmacen[i].No_Descripcion + '</option>' );
  }, 'JSON');
  // M011 - F 

  url = base_url + 'HelperController/getTiposProducto';
  $.post( url , function( responseTiposProducto ){
    $( '#cbo-TiposItem' ).html( '<option value="">- Seleccionar -</option>' );
    for (var i = 0; i < responseTiposProducto.length; i++)
      $( '#cbo-TiposItem' ).append( '<option value="' + responseTiposProducto[i].Nu_Valor + '">' + responseTiposProducto[i].No_Descripcion + '</option>' );
  }, 'JSON');
  
  url = base_url + 'HelperController/getTiposExistenciaProducto';
  $.post( url , function( responseTiposExistenciaProducto ){
    $( '#cbo-TiposExistenciaProducto' ).html( '<option value="0">- Seleccionar -</option>' );
    for (var i = 0; i < responseTiposExistenciaProducto.length; i++)
      $( '#cbo-TiposExistenciaProducto' ).append( '<option value="' + responseTiposExistenciaProducto[i].ID_Tipo_Producto + '">' + responseTiposExistenciaProducto[i].No_Tipo_Producto + '</option>' );
  }, 'JSON');
  
  url = base_url + 'HelperController/getUbicacionesInventario';
  $.post( url , function( responseUbicacionesInventario ){
    $( '#cbo-UbicacionesInventario' ).html( '' );
    for (var i = 0; i < responseUbicacionesInventario.length; i++)
      $( '#cbo-UbicacionesInventario' ).append( '<option value="' + responseUbicacionesInventario[i].ID_Ubicacion_Inventario + '">' + responseUbicacionesInventario[i].No_Ubicacion_Inventario + '</option>' );
  }, 'JSON');
  
  url = base_url + 'HelperController/getImpuestos';
  $.post( url , function( response ){
    $( '#cbo-Impuestos' ).html( '<option value="">- Vacío -</option>' );
    if ( response.length == 1 ) {
      $( '#cbo-Impuestos' ).html( '<option value="' + response[0].ID_Impuesto + '" data-ss_impuesto="' + response[0].Ss_Impuesto + '" data-nu_tipo_impuesto="' + response[0].Nu_Tipo_Impuesto + '">' + response[0].No_Impuesto + '</option>' );
    } else if ( response.length > 1 ) {
      $( '#cbo-Impuestos' ).html( '<option value="">- Seleccionar -</option>' );
      for (var i = 0; i < response.length; i++)
        $( '#cbo-Impuestos' ).append( '<option value="' + response[i].ID_Impuesto + '" data-ss_impuesto="' + response[i].Ss_Impuesto + '" data-nu_tipo_impuesto="' + response[i].Nu_Tipo_Impuesto + '">' + response[i].No_Impuesto + '</option>' );
    }
  }, 'JSON');
  
  $( '#cbo-lote_vencimiento' ).html( '<option value="0">No</option>' );
  $( '#cbo-lote_vencimiento' ).append( '<option value="1">Si</option>' );

  url = base_url + 'HelperController/getUnidadesMedida';
  $.post( url , function( responseUnidadMedidas ){
    if ( responseUnidadMedidas.length == 1 ) {
      $( '#cbo-UnidadesMedida' ).html( '<option value="' + responseUnidadMedidas[0].ID_Unidad_Medida + '" selected>' + responseUnidadMedidas[0].No_Unidad_Medida + '</option>' );
    } else {
      $( '#cbo-UnidadesMedida' ).html( '<option value="">- Seleccionar -</option>' );
      for (var i = 0; i < responseUnidadMedidas.length; i++) {
        $( '#cbo-UnidadesMedida' ).append( '<option value="' + responseUnidadMedidas[i].ID_Unidad_Medida + '">' + responseUnidadMedidas[i].No_Unidad_Medida + '</option>' );
      }
    }
  }, 'JSON');
  
  url = base_url + 'HelperController/getMarcas';
  $.post( url , function( responseMarcas ){
    $( '#cbo-Marcas' ).html( '<option value="">- Seleccionar -</option>' );
    for (var i = 0; i < responseMarcas.length; i++)
      $( '#cbo-Marcas' ).append( '<option value="' + responseMarcas[i].ID_Marca + '">' + responseMarcas[i].No_Marca + '</option>' );
  }, 'JSON');
  
  url = base_url + 'HelperController/getDataGeneral';
  $.post( url, {sTipoData : 'categoria'}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      if ( l == 1 ) {
        $( '#cbo-categoria' ).html( '<option value="' + response.arrData[0].ID + '">' + response.arrData[0].Nombre + '</option>' );
        
        url = base_url + 'HelperController/getDataGeneral';
        var arrParams = {
          sTipoData : 'subcategoria',
          sWhereIdCategoria : response.arrData[0].ID,
        }
        $.post( url, arrParams, function( responseSubCategoria ){
          if ( responseSubCategoria.sStatus == 'success' ) {
            var l = responseSubCategoria.arrData.length;
            if (l==1) {
              $( '#cbo-sub_categoria' ).html( '<option value="' + responseSubCategoria.arrData[0].ID + '">' + responseSubCategoria.arrData[0].Nombre + '</option>' );
            } else {
              $( '#cbo-sub_categoria' ).html('<option value="" selected="selected">- Seleccionar -</option>');
              for (var x = 0; x < l; x++) {
                $( '#cbo-sub_categoria' ).append( '<option value="' + responseSubCategoria.arrData[x].ID + '">' + responseSubCategoria.arrData[x].Nombre + '</option>' );
              }
            }
          } else {
            $( '#cbo-sub_categoria' ).html('<option value="" selected="selected">- vacío -</option>');
          }
        }, 'JSON');
      } else {
        $( '#cbo-categoria' ).html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var x = 0; x < l; x++) {
          $( '#cbo-categoria' ).append( '<option value="' + response.arrData[x].ID + '">' + response.arrData[x].Nombre + '</option>' );
        }
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
  	  $( '#modal-message' ).modal('show');
      $( '.modal-message' ).addClass(response.sClassModal);
      $( '.modal-title-message' ).text(response.sMessage);
      setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
    }
    $( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
  /* Ecommerce Marketplace */
  url = base_url + 'HelperController/getCategoriasMarketplace';
  $.post( url, {}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      $( '#cbo-categoria_marketplace' ).html('<option value="" selected="selected">- Seleccionar -</option>');
      for (var x = 0; x < l; x++) {
        $( '#cbo-categoria_marketplace' ).append( '<option value="' + response.arrData[x].ID_Familia + '">' + response.arrData[x].No_Familia + '</option>' );
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      console.log(response.sMessage);
    }
  }, 'JSON');

  $( '#cbo-sub_categoria_marketplace' ).html('<option value="" selected="selected">- Vacío -</option>');
  
  url = base_url + 'HelperController/getMarcasMarketplace';
  $.post( url, {}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      $( '#cbo-marca_marketplace' ).html('<option value="" selected="selected">- Seleccionar -</option>');
      for (var x = 0; x < l; x++) {
        $( '#cbo-marca_marketplace' ).append( '<option value="' + response.arrData[x].ID_Marca + '">' + response.arrData[x].No_Marca + '</option>' );
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      console.log(response.sMessage);
      $( '#cbo-marca_marketplace' ).html('<option value="" selected="selected">- Vacío -</option>');
    }
  }, 'JSON');
  /* End Ecommerce Marketplace */

  $( '#cbo-receta_medica' ).html( '<option value="0">No</option>' );
  $( '#cbo-receta_medica' ).append( '<option value="1">Si</option>' );
  
  if (iIdTipoRubroEmpresaGlobal == '1') {
    url = base_url + 'HelperController/getDataGeneral';
    $.post( url, {sTipoData : 'laboratorio'}, function( response ){
      if ( response.sStatus == 'success' ) {
        var l = response.arrData.length;
        $( '#cbo-laboratorio' ).html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var x = 0; x < l; x++) {
          $( '#cbo-laboratorio' ).append( '<option value="' + response.arrData[x].ID + '">' + response.arrData[x].Nombre + '</option>' );
        }
      } else {
        if( response.sMessageSQL !== undefined ) {
          console.log(response.sMessageSQL);
        }
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass(response.sClassModal);
        $( '.modal-title-message' ).text(response.sMessage);
        setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
      }
      $( '#modal-loader' ).modal('hide');
    }, 'JSON');

    $("#cbo-composicion").select2({
      placeholder: "- Seleccionar -",
      allowClear: true
    });

    url = base_url + 'HelperController/getDataGeneral';
    $.post( url, {sTipoData : 'composicion'}, function( response ){
      if ( response.sStatus == 'success' ) {
        var l = response.arrData.length;
        $( '#cbo-composicion' ).html('');
        for (var x = 0; x < l; x++) {
          $( '#cbo-composicion' ).append( '<option value="' + response.arrData[x].ID + '">' + response.arrData[x].Nombre + '</option>' );
        }
      } else {
        if( response.sMessageSQL !== undefined ) {
          console.log(response.sMessageSQL);
        }
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass(response.sClassModal);
        $( '.modal-title-message' ).text(response.sMessage);
        setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
      }
      $( '#modal-loader' ).modal('hide');
    }, 'JSON');
  }

  url = base_url + 'HelperController/getDataGeneral';
  $.post( url, {sTipoData : 'categoria'}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      $( '#cbo-categoria' ).html('<option value="" selected="selected">- Seleccionar -</option>');
      for (var x = 0; x < l; x++) {
        $( '#cbo-categoria' ).append( '<option value="' + response.arrData[x].ID + '">' + response.arrData[x].Nombre + '</option>' );
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
  	  $( '#modal-message' ).modal('show');
      $( '.modal-message' ).addClass(response.sClassModal);
      $( '.modal-title-message' ).text(response.sMessage);
      setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
    }
    $( '#modal-loader' ).modal('hide');
  }, 'JSON');

  url = base_url + 'HelperController/getValoresTablaDato';
  $.post( url, {sTipoData : 'Tipos_PedidoLavado'}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      $( '#cbo-tipo_pedido_lavado' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
      for (var x = 0; x < l; x++) {
        $( '#cbo-tipo_pedido_lavado' ).append( '<option value="' + response.arrData[x].Nu_Valor + '">' + response.arrData[x].No_Descripcion + '</option>' );
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      if ( response.sStatus == 'warning')
        $( '#cbo-tipo_pedido_lavado' ).html('<option value="0" selected="selected">- Vacío -</option>');
    }
  }, 'JSON');
  
  $( '#cbo-impuesto_icbper' ).html( '<option value="0">No</option>' );
  $( '#cbo-impuesto_icbper' ).append( '<option value="1">Si</option>' );
  
  $( '#cbo-Compuesto' ).html( '<option value="0">No</option>' );
  $( '#cbo-Compuesto' ).append( '<option value="1">Si</option>' );

  $( '#cbo-Estado' ).html( '<option value="1">Activo</option>' );
  $( '#cbo-Estado' ).append( '<option value="0">Inactivo</option>' );

  /* obtener imagen guardada(s) */
  $( '.divDropzone' ).html(
  '<div id="id-divDropzone" class="dropzone div-dropzone">'
    +'<div class="dz-message">'
      +'Arrastrar o presionar click para subir imágen(es)'
    +'</div>'
  +'</div>'
  );

  Dropzone.autoDiscover = false;
  Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastrar o presionar click para subir imágen";
  Dropzone.prototype.defaultOptions.dictFallbackMessage = "Tu navegador no soporta la función arrastrar la imágen";
  Dropzone.prototype.defaultOptions.dictFileTooBig = "La imágen pesa ({{filesize}}MiB). El tamaño máximo es: {{maxFilesize}}MiB.";
  Dropzone.prototype.defaultOptions.dictInvalidFileType = "Solo se permite imágenes PNG / JPG";
  Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
  Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estás seguro de cancelar la subida?";
  Dropzone.prototype.defaultOptions.dictRemoveFile = "Eliminar";
  Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo se puede subir 2 imágenes";
  
  url = base_url + 'Logistica/ReglasLogistica/ProductoController/uploadMultiple';
  var myDropzone = new Dropzone("#id-divDropzone", {
    url: url,
    params: {
      iVersionImage: 1,
      iIdProducto: 1,
    },
    acceptedFiles: ".jpeg,.jpg,.png,.webp",
    addRemoveLinks: true,
    uploadMultiple: false,
    maxFilesize: 1,//Peso en MB
    maxFiles: 2,
    thumbnailHeight: 200,
    thumbnailWidth: 200,
    parallelUploads: 2,
    thumbnail: function(file, dataUrl) {
      if (file.previewElement) {
        file.previewElement.classList.remove("dz-file-preview");
        var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
        for (var i = 0; i < images.length; i++) {
          var thumbnailElement = images[i];
          thumbnailElement.alt = file.name;
          thumbnailElement.src = dataUrl;
        }
        setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
      }
    },
    removedfile: function(file){
      var nameFileImage = file.name;
      url = base_url + 'Logistica/ReglasLogistica/ProductoController/removeFileImage';
      $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        data: {nameFileImage : nameFileImage},
      })
      var previewElement;
      return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
    },
    init : function() {
      //Verificar respuesta del servidor al subir archivo
      this.on("success", function(file, response) {
        var response = jQuery.parseJSON(response);
        
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        $( '#modal-message' ).modal('show');
        
        if (response.sStatus != 'error'){
          $( '.modal-message' ).addClass(response.sClassModal);
          $( '.modal-title-message' ).text(response.sMessage);

          $( '#hidden-nombre_imagen' ).val( response.sNombreImagenItem );

          setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
        } else {
          $( '.modal-message' ).addClass(response.sClassModal);
          $( '.modal-title-message' ).text(response.sMessage);
          setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
        }
      })
    },
  })
	
  $( '[name="No_Producto"]' ).focus();
}

function verProducto(ID, Nu_Codigo_Barra, No_Imagen_Item, Nu_Version_Imagen){
  accion_producto = 'upd_producto';
  $( '[name="EID_Empresa"]' ).focus();
  
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();
  
  $( '.div-Compuesto' ).hide();
	$( '#table-Producto_Enlace tbody' ).empty();

  $( '#form-Producto' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
 
	$( '.div-Producto' ).show();
  
  $( '#cbo-TiposItem' ).prop('disabled', true);
  
  url = base_url + 'Logistica/ReglasLogistica/ProductoController/ajax_edit/' + ID;
  $.ajax({
    url : url,
    type: "GET",
    dataType: "JSON",
    success: function(response){
      $( '.div-AgregarEditar' ).show();
      
      $( '.title_Producto' ).text('Modifcar Producto');
      
      $( '[name="EID_Empresa"]' ).val(response.ID_Empresa);
      $( '[name="EID_Producto"]' ).val(response.ID_Producto);
      $( '[name="ENu_Codigo_Barra"]' ).val(response.Nu_Codigo_Barra);
      $( '#hidden-nombre_imagen' ).val(response.No_Imagen_Item);
      
      if ( response.Nu_Tipo_Producto == 0 || response.Nu_Tipo_Producto == 2){//Servicio
      	$( '.div-Producto' ).hide();
      }
      
	    var selected='';
      url = base_url + 'HelperController/getTiposProducto';
      $.post( url , function( responseTiposProducto ){
        $( '#cbo-TiposItem' ).html( '<option value="">- Seleccionar -</option>' );
        for (var i = 0; i < responseTiposProducto.length; i++){
          selected = '';
          if(response.Nu_Tipo_Producto == responseTiposProducto[i].Nu_Valor)
            selected = 'selected="selected"';
          $( '#cbo-TiposItem' ).append( '<option value="' + responseTiposProducto[i].Nu_Valor + '" ' + selected + '>' + responseTiposProducto[i].No_Descripcion + '</option>' );
        }
      }, 'JSON');
      
       // M011 - I 
       url = base_url + 'HelperController/getAreaAlmacenProducto';
       $.post( url , function( responseAreaAlmacen ){
         $( '#cbo-AreaAlmacen' ).html( '<option value="">- Seleccionar -</option>' );
         for (var i = 0; i < responseAreaAlmacen.length; i++){
           /* 
           window.alert(esponse.Nu_Area_Almacen +' - '+responseAreaAlmacen[i].Nu_Valor);*/
           selected = '';
           if(response.Nu_Area_Almacen == responseAreaAlmacen[i].Nu_Valor)
             selected = 'selected="selected"'; 
             //window.alert(responseAreaAlmacen[i].Nu_Valor);
             //window.alert(response.Nu_Area_Almacen + " - " + responseAreaAlmacen[i].Nu_Valor + " - "  + selected);
           $( '#cbo-AreaAlmacen' ).append( '<option value="' + responseAreaAlmacen[i].Nu_Valor + '"   ' + selected + '    >' + responseAreaAlmacen[i].No_Descripcion + '</option>' );
    
         /* if(response.Nu_Area_Almacen == responseAreaAlmacen[i].Nu_Valor)
           selected = 'selected="selected"';
           $( '#cbo-AreaAlmacen' ).append( '<option value="' + responseAreaAlmacen[i].Nu_Valor + '" ' + selected + '>' + responseAreaAlmacen[i].No_Descripcion + '</option>' );
         */
         }
 
       }, 'JSON');
 
 
       // M011 - F 
 

      url = base_url + 'HelperController/getTiposExistenciaProducto';
      $.post( url , function( responseTiposExistenciaProducto ){
        $( '#cbo-TiposExistenciaProducto' ).html( '<option value="">- Seleccionar -</option>' );
        for (var i = 0; i < responseTiposExistenciaProducto.length; i++){
          selected = '';
          if(response.ID_Tipo_Producto == responseTiposExistenciaProducto[i].ID_Tipo_Producto)
            selected = 'selected="selected"';
          $( '#cbo-TiposExistenciaProducto' ).append( '<option value="' + responseTiposExistenciaProducto[i].ID_Tipo_Producto + '" ' + selected + '>' + responseTiposExistenciaProducto[i].No_Tipo_Producto + '</option>' );
        }
      }, 'JSON');
      
      url = base_url + 'HelperController/getUbicacionesInventario';
      $.post( url , function( responseUbicacionesInventario ){
        $( '#cbo-UbicacionesInventario' ).html( '<option value="">- Seleccionar -</option>' );
        for (var i = 0; i < responseUbicacionesInventario.length; i++){
          selected = '';
          if(response.ID_Ubicacion_Inventario == responseUbicacionesInventario[i].ID_Ubicacion_Inventario)
            selected = 'selected="selected"';
          $( '#cbo-UbicacionesInventario' ).append( '<option value="' + responseUbicacionesInventario[i].ID_Ubicacion_Inventario + '" ' + selected + '>' + responseUbicacionesInventario[i].No_Ubicacion_Inventario + '</option>' );
        }
      }, 'JSON');
      
      $('[name="No_Codigo_Interno"]').val(response.No_Codigo_Interno);
      $('[name="Nu_Codigo_Barra"]').val(response.Nu_Codigo_Barra);
      
      $('[name="ID_Tabla_Dato"]').val(response.ID_Producto_Sunat);
      $('[name="No_Descripcion"]').val(response.No_Producto_Sunat);
      $('#txt-Txt_Ubicacion_Producto_Tienda').val(response.Txt_Ubicacion_Producto_Tienda);
      $('[name="No_Producto"]').val( clearHTMLTextArea(response.No_Producto) );

      $( '[name="Ss_Precio"]' ).val( response.Ss_Precio );
      $( '[name="Ss_Costo"]' ).val( response.Ss_Costo );

      url = base_url + 'HelperController/getMarcas';
      $.post( url , function( responseMarcas ){
        $( '#cbo-Marcas' ).html( '<option value="">- Seleccionar -</option>' );
        for (var i = 0; i < responseMarcas.length; i++){
          selected = '';
          if(response.ID_Marca == responseMarcas[i].ID_Marca)
            selected = 'selected="selected"';
          $( '#cbo-Marcas' ).append( '<option value="' + responseMarcas[i].ID_Marca + '" ' + selected + '>' + responseMarcas[i].No_Marca + '</option>' );
        }
      }, 'JSON');
      
      url = base_url + 'HelperController/getImpuestos';
      $.post( url , function( responseImpuestos ){
        $( '#cbo-Impuestos' ).html( '<option value="">- Seleccionar -</option>' );
        for (var i = 0; i < responseImpuestos.length; i++){
          selected = '';
          if(response.ID_Impuesto == responseImpuestos[i].ID_Impuesto)
            selected = 'selected="selected"';
          $( '#cbo-Impuestos' ).append( '<option value="' + responseImpuestos[i].ID_Impuesto + '" data-ss_impuesto="' + responseImpuestos[i].Ss_Impuesto + '" data-nu_tipo_impuesto="' + responseImpuestos[i]['Nu_Tipo_Impuesto'] + '" ' + selected + '>' + responseImpuestos[i]['No_Impuesto'] + '</option>' );
        }
      }, 'JSON');
      
      $( '#cbo-lote_vencimiento' ).html('');
      for (var i = 0; i < 2; i++){
        selected = '';
        if(response.Nu_Lote_Vencimiento == i)
          selected = 'selected="selected"';
        $( '#cbo-lote_vencimiento' ).append( '<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'No' : 'Si') + '</option>' );
      }

      url = base_url + 'HelperController/getUnidadesMedida';
      $.post( url , function( responseUnidadMedidas ){
        $( '#cbo-UnidadesMedida' ).html( '<option value="">- Seleccionar -</option>' );
        for (var i = 0; i < responseUnidadMedidas.length; i++){
          selected = '';
          if(response.ID_Unidad_Medida == responseUnidadMedidas[i].ID_Unidad_Medida)
            selected = 'selected="selected"';
          $( '#cbo-UnidadesMedida' ).append( '<option value="' + responseUnidadMedidas[i].ID_Unidad_Medida + '" ' + selected + '>' + responseUnidadMedidas[i].No_Unidad_Medida + '</option>' );
        }
      }, 'JSON');
      
      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData : 'categoria'}, function( responseCategoria ){
        $( '#cbo-categoria' ).html( '<option value="">- Seleccionar -</option>' );
        if ( responseCategoria.sStatus == 'success' ) {
          var l = responseCategoria.arrData.length;
          for (var x = 0; x < l; x++) {
            selected = '';
            if(response.ID_Familia == responseCategoria.arrData[x].ID)
              selected = 'selected="selected"';
            $( '#cbo-categoria' ).append( '<option value="' + responseCategoria.arrData[x].ID + '" ' + selected + '>' + responseCategoria.arrData[x].Nombre + '</option>' );
          }
        } else {
          if( responseCategoria.sMessageSQL !== undefined ) {
            console.log(responseCategoria.sMessageSQL);
          }
          if ( responseCategoria.sStatus == 'warning')
            $( '#cbo-categoria' ).html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');
      
      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData : 'subcategoria', sWhereIdCategoria : response.ID_Familia}, function( responseSubCategoria ){
        if ( responseSubCategoria.sStatus == 'success' ) {
          $( '#cbo-sub_categoria' ).html( '<option value="">- Seleccionar -</option>' );
          var l = responseSubCategoria.arrData.length;
          for (var x = 0; x < l; x++) {
            selected = '';
            if(response.ID_Sub_Familia == responseSubCategoria.arrData[x].ID)
              selected = 'selected="selected"';
            $( '#cbo-sub_categoria' ).append( '<option value="' + responseSubCategoria.arrData[x].ID + '" ' + selected + '>' + responseSubCategoria.arrData[x].Nombre + '</option>' );
          }
        } else {
          if( responseSubCategoria.sMessageSQL !== undefined ) {
            console.log(responseSubCategoria.sMessageSQL);
          }
          if ( responseSubCategoria.sStatus == 'warning')
            $( '#cbo-sub_categoria' ).html('<option value="0" selected="selected">- Vacío -</option>');
        }
        $( '#modal-loader' ).modal('hide');
      }, 'JSON');
      
      /* Ecommerce Marketplace */
      $( '[name="Ss_Precio_Ecommerce_Online_Regular"]' ).val( response.Ss_Precio_Ecommerce_Online_Regular );
      $( '[name="Ss_Precio_Ecommerce_Online"]' ).val( response.Ss_Precio_Ecommerce_Online );

      url = base_url + 'HelperController/getCategoriasMarketplace';
      $.post( url, {}, function( responseCategoria ){
        if ( responseCategoria.sStatus == 'success' ) {
          var l = responseCategoria.arrData.length;
          $( '#cbo-categoria_marketplace' ).html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            selected = '';
            if(response.ID_Familia_Marketplace == responseCategoria.arrData[x].ID_Familia)
              selected = 'selected="selected"';
            $( '#cbo-categoria_marketplace' ).append( '<option value="' + responseCategoria.arrData[x].ID_Familia + '" ' + selected + '>' + responseCategoria.arrData[x].No_Familia + '</option>' );
          }
        } else {
          if( responseCategoria.sMessageSQL !== undefined ) {
            console.log(responseCategoria.sMessageSQL);
          }
          console.log(responseCategoria.sMessage);
        }
      }, 'JSON');

      url = base_url + 'HelperController/getSubCategoriasMarketplace';
      var arrParams = {'iIdFamilia' : response.ID_Familia_Marketplace}
      $.post( url, arrParams, function( responseSubCategoria ){
        if ( responseSubCategoria.sStatus == 'success' ) {
          var l = responseSubCategoria.arrData.length;
          if (l==1) {
            $( '#cbo-sub_categoria_marketplace' ).html( '<option value="' + responseSubCategoria.arrData[0].ID_Sub_Familia + '">' + responseSubCategoria.arrData[0].No_Sub_Familia + '</option>' );
          } else {
            $( '#cbo-sub_categoria_marketplace' ).html('<option value="" selected="selected">- Seleccionar -</option>');
            for (var x = 0; x < l; x++) {
              selected = '';
              if(response.ID_Sub_Familia_Marketplace == responseSubCategoria.arrData[x].ID_Sub_Familia)
                selected = 'selected="selected"';
              $( '#cbo-sub_categoria_marketplace' ).append( '<option value="' + responseSubCategoria.arrData[x].ID_Sub_Familia + '" ' + selected + '>' + responseSubCategoria.arrData[x].No_Sub_Familia + '</option>' );
            }
          }
        } else {
          if( responseSubCategoria.sMessageSQL !== undefined ) {
            console.log(responseSubCategoria.sMessageSQL);
          }
          console.log(responseSubCategoria.sMessage);
        }
      }, 'JSON');

      url = base_url + 'HelperController/getMarcasMarketplace';
      $.post( url, {}, function( responseMarca ){
        if ( responseMarca.sStatus == 'success' ) {
          var l = responseMarca.arrData.length;
          $( '#cbo-marca_marketplace' ).html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            selected = '';
            if(response.ID_Marca_Marketplace == responseMarca.arrData[x].ID_Marca)
              selected = 'selected="selected"';
            $( '#cbo-marca_marketplace' ).append( '<option value="' + responseMarca.arrData[x].ID_Marca + '" ' + selected + '>' + responseMarca.arrData[x].No_Marca + '</option>' );
          }
        } else {
          if( responseMarca.sMessageSQL !== undefined ) {
            console.log(responseMarca.sMessageSQL);
          }
          console.log(responseMarca.sMessage);
        }
      }, 'JSON');
      /* End Ecommerce Marketplace */

      url = base_url + 'HelperController/getValoresTablaDato';
      $.post( url, {sTipoData : 'Tipos_PedidoLavado'}, function( responseTipoPedidoLavado ){
        $( '#cbo-tipo_pedido_lavado' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
        if ( responseTipoPedidoLavado.sStatus == 'success' ) {
          var l = responseTipoPedidoLavado.arrData.length;
          $( '#cbo-tipo_pedido_lavado' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            selected = '';
            if(response.ID_Tipo_Pedido_Lavado == responseTipoPedidoLavado.arrData[x].Nu_Valor)
              selected = 'selected="selected"';
            $( '#cbo-tipo_pedido_lavado' ).append( '<option value="' + responseTipoPedidoLavado.arrData[x].Nu_Valor + '" ' + selected + '>' + responseTipoPedidoLavado.arrData[x].No_Descripcion + '</option>' );
          }
        } else {
          if( responseTipoPedidoLavado.sMessageSQL !== undefined ) {
            console.log(responseTipoPedidoLavado.sMessageSQL);
          }
          if ( responseTipoPedidoLavado.sStatus == 'warning')
            $( '#cbo-tipo_pedido_lavado' ).html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');

      $('#tel-Nu_Stock_Minimo').val(response.Nu_Stock_Minimo);
      $('#tel-Qt_CO2_Producto').val(response.Qt_CO2_Producto);

      $( '#cbo-receta_medica' ).html('');
      for (var i = 0; i < 2; i++){
        selected = '';
        if(response.Nu_Receta_Medica == i)
          selected = 'selected="selected"';
        $( '#cbo-receta_medica' ).append( '<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'No' : 'Si') + '</option>' );
      }

      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData : 'laboratorio'}, function( responseLaboratorio ){
        $( '#cbo-laboratorio' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
        if ( responseLaboratorio.sStatus == 'success' ) {
          var l = responseLaboratorio.arrData.length;
          for (var x = 0; x < l; x++) {
            selected = '';
            if(response.ID_Laboratorio == responseLaboratorio.arrData[x].ID)
              selected = 'selected="selected"';
            $( '#cbo-laboratorio' ).append( '<option value="' + responseLaboratorio.arrData[x].ID + '" ' + selected + '>' + responseLaboratorio.arrData[x].Nombre + '</option>' );
          }
        } else {
          if( responseLaboratorio.sMessageSQL !== undefined ) {
            console.log(responseLaboratorio.sMessageSQL);
          }
          if ( responseLaboratorio.sStatus == 'warning')
            $( '#cbo-laboratorio' ).html('<option value="0" selected="selected">- Vacío -</option>');          
        }
      }, 'JSON');
  
      $( '#cbo-composicion' ).html('');
      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData : 'composicion'}, function( responseComposicion ){
        if ( responseComposicion.sStatus == 'success' ) {
          var l = responseComposicion.arrData.length;
          if ( response.Txt_Composicion != '' && response.Txt_Composicion != null ) {
            var arrComposicion=response.Txt_Composicion.split(',');
            var iCountComposicion=arrComposicion.length;     
            for (var x = 0; x < l; x++) {
              selected = '';
              for (var y=0; y < iCountComposicion; y++) {
                if(arrComposicion[y] == responseComposicion.arrData[x].ID)
                  selected = 'selected="selected"';
              }
              $( '#cbo-composicion' ).append( '<option value="' + responseComposicion.arrData[x].ID + '" ' + selected + '>' + responseComposicion.arrData[x].Nombre + '</option>' );
            }
          } else {
            for (var x = 0; x < l; x++) {
              $( '#cbo-composicion' ).append( '<option value="' + responseComposicion.arrData[x].ID + '">' + responseComposicion.arrData[x].Nombre + '</option>' );
            }
          }
        } else {
          console.log(response.sMessageSQL);
          $( '.modal-message' ).addClass(response.sClassModal);
          $( '.modal-title-message' ).text(response.sMessage);
          setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
        }
      }, 'JSON');
      
      $( '#cbo-impuesto_icbper' ).html('');
      for (var i = 0; i < 2; i++){
        selected = '';
        if(response.ID_Impuesto_Icbper == i)
          selected = 'selected="selected"';
        $( '#cbo-impuesto_icbper' ).append( '<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'No' : 'Si') + '</option>' );
      }

      $( '#cbo-Compuesto' ).html('');
      for (var i = 0; i < 2; i++){
        selected = '';
        if(response.Nu_Compuesto == i)
          selected = 'selected="selected"';
        $( '#cbo-Compuesto' ).append( '<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'No' : 'Si') + '</option>' );
      }

      $('[name="No_Modelo_Vehiculo"]').val(response.No_Modelo_Vehiculo);
      $('[name="No_Color_Vehiculo"]').val(response.No_Color_Vehiculo);
      $('[name="No_Motor"]').val(response.No_Motor);

      /* Enlaces de Productos */
      if (response.Nu_Compuesto == 1) {
        url = base_url + 'Logistica/ReglasLogistica/ProductoController/ajax_edit_enlace/' + ID;
        $.getJSON( url, function( data ){
          if ( data.length > 0 ){
	          $( '.div-Compuesto' ).show();
	          $( '#table-Producto_Enlace' ).show();
            var table_enlace_producto = "";
            for (i = 0; i < data.length; i++) {
              table_enlace_producto += 
              "<tr id='tr_enlace_producto" + data[i]['ID_Producto'] + "'>"
                + "<td class='text-left' style='display:none;'>" + data[i]['ID_Producto'] + "</td>"
                + "<td class='text-left'>" + data[i]['Nu_Codigo_Barra'] + "</td>"
                + "<td class='text-left'>" + data[i]['No_Producto'] + "</td>"
                + "<td class='text-right'>" + data[i]['Qt_Producto_Descargar'] + "</td>"
                + "<td class='text-center'><button type='button' id='btn-deleteProductoEnlace' class='btn btn-xs btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o' aria-hidden='true'> Eliminar</i></button></td>"
              + "</tr>";
            }
			      $( '#table-Producto_Enlace' ).append(table_enlace_producto);
          }
        }, 'JSON')
      }

      $( '#cbo-Estado' ).html('');
      for (var i = 0; i < 2; i++){
        selected = '';
        if(response.Nu_Estado == i)
          selected = 'selected="selected"';
        $( '#cbo-Estado' ).append( '<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'Inactivo' : 'Activo') + '</option>' );
      }

      $( '[name="Txt_Producto"]' ).val( clearHTMLTextArea(response.Txt_Producto) );
      
      $( '[name="No_Producto"]' ).focus();
      
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $( '#modal-loader' ).modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    }
  })
  
  /* obtener imagen guardada(s) */
  $( '.divDropzone' ).html(
  '<div id="id-divDropzone" class="dropzone div-dropzone">'
    +'<div class="dz-message">'
      +'Arrastrar o presionar click para subir imágen(es)'
    +'</div>'
  +'</div>'
  );
  
  Dropzone.autoDiscover = false;
  Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastrar o presionar click para subir imágen";
  Dropzone.prototype.defaultOptions.dictFallbackMessage = "Tu navegador no soporta la función arrastrar la imágen";
  Dropzone.prototype.defaultOptions.dictFileTooBig = "La imágen pesa ({{filesize}}MiB). El tamaño máximo es: {{maxFilesize}}MiB.";
  Dropzone.prototype.defaultOptions.dictInvalidFileType = "Solo se permite imágenes PNG / JPG";
  Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
  Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estás seguro de cancelar la subida?";
  Dropzone.prototype.defaultOptions.dictRemoveFile = "Eliminar";
  Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo se puede subir 1 imágen";
  
  url = base_url + 'Logistica/ReglasLogistica/ProductoController/uploadMultiple';
  var myDropzone = new Dropzone("#id-divDropzone", {
    url: url,
    params: {
      iVersionImage: (parseInt(Nu_Version_Imagen) + 1),
      iIdProducto: ID,
    },
    acceptedFiles: ".jpeg,.jpg,.png,.webp",
    addRemoveLinks: true,
    uploadMultiple: false,
    maxFilesize: 1,//Peso en MB
    maxFiles: 2,
    thumbnailHeight: 200,
    thumbnailWidth: 200,
    parallelUploads: 2,
    thumbnail: function(file, dataUrl) {
      if (file.previewElement) {
        file.previewElement.classList.remove("dz-file-preview");
        var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
        for (var i = 0; i < images.length; i++) {
          var thumbnailElement = images[i];
          thumbnailElement.alt = file.name;
          thumbnailElement.src = dataUrl;
        }
        setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
      }
    },
    removedfile: function(file){
      var arrName = file.name.split('/');
      var nameFileImage;
      if (arrName.length === 5)//Si la imagen ya está en el server
        nameFileImage = arrName[4];
      else//Si la imagén recién la vamos a subir y no existe en el server
        nameFileImage = arrName[0];
      url = base_url + 'Logistica/ReglasLogistica/ProductoController/removeFileImage';
      $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        data: {nameFileImage : nameFileImage},
      })
      var previewElement;
      return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
    },
    init : function() {
      //Verificar respuesta del servidor al subir archivo
      this.on("success", function(file, response) {
        var response = jQuery.parseJSON(response);
        
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        $( '#modal-message' ).modal('show');
        
        if (response.sStatus != 'error'){
          $( '.modal-message' ).addClass(response.sClassModal);
          $( '.modal-title-message' ).text(response.sMessage);
          
          $( '#hidden-nombre_imagen' ).val( response.sNombreImagenItem );
          
          setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
        } else {
          $( '.modal-message' ).addClass(response.sClassModal);
          $( '.modal-title-message' ).text(response.sMessage);
          setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
        }
      })
      
      var me = this;
      url = base_url + 'Logistica/ReglasLogistica/ProductoController/get_image';
      var arrPost={
        'sUrlImage': No_Imagen_Item,
      }
      $.post(url, arrPost, function(response){
        $.each(response, function(key, value){
          //console.log(key);
          console.log(value.name);
          var mockfile = value;
          me.emit("addedfile", mockfile);
          me.emit("thumbnail", mockfile, value.name);
          me.emit("complete", mockfile);
        })
      }, 'json');
    }
  })
}

function form_Producto(){
  if (accion_producto == 'add_producto' || accion_producto == 'upd_producto') {
    var arrProductoEnlace = [];
  
    $( "#table-Producto_Enlace tbody tr" ).each(function(){
      var rows                = $(this);
      var $ID_Producto_Enlace = rows.find("td:eq(0)").text();
      var $Qt_Producto_Enlace = rows.find("td:eq(3)").text();
      var obj                 = {};
      
      obj.ID_Producto_Enlace	= $ID_Producto_Enlace;
      obj.Qt_Producto_Descargar	= $Qt_Producto_Enlace;
      arrProductoEnlace.push(obj);
    });

    $( '.help-block' ).empty();
    if ( $( '#cbo-TiposItem' ).val() == '1' && $( '#cbo-TiposExistenciaProducto' ).val() == '0'){
      $( '#cbo-TiposExistenciaProducto' ).closest('.form-group').find('.help-block').html('Seleccionar tipo producto');
  	  $( '#cbo-TiposExistenciaProducto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ( $( '#cbo-TiposItem' ).val() == '1' && $( '#cbo-Impuestos' ).val() == '0'){
      $( '#cbo-Impuestos' ).closest('.form-group').find('.help-block').html('Seleccionar impuesto');
  	  $( '#cbo-Impuestos' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ( $( '#cbo-TiposItem' ).val() == '1' && $( '#cbo-UnidadesMedida' ).val() == '0'){
      $( '#cbo-UnidadesMedida' ).closest('.form-group').find('.help-block').html('Seleccionar unidad medida');
  	  $( '#cbo-UnidadesMedida' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ( $( '#cbo-TiposItem' ).val() == '1' && $( '#cbo-Compuesto' ).val() == '1' && arrProductoEnlace.length === 0){
      $( '#cbo-Compuesto' ).closest('.form-group').find('.help-block').html('Seleccionar productos para enlazar');
  	  $( '#cbo-Compuesto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      if ( $( '#cbo-Compuesto' ).val()=='0')//No
  			arrProductoEnlace = null;
  		var iIDTipoExistenciaProducto = $( '#cbo-TiposExistenciaProducto' ).val();
      if ( $( '#cbo-TiposItem' ).val() == '0' || $( '#cbo-TiposItem' ).val() == '2' )//Servicio o interno
        iIDTipoExistenciaProducto = 4;//Otros
      
  		var arrProducto = Array();
  		arrProducto = {
  		  'EID_Empresa'               : $( '#txt-EID_Empresa' ).val(),
  		  'EID_Producto'              : $( '#txt-EID_Producto' ).val(),
  		  'ENu_Codigo_Barra'          : $( '#txt-ENu_Codigo_Barra' ).val(),
  		  'Nu_Tipo_Producto'          : $( '#cbo-TiposItem' ).val(),

        // M011 - I 

        'Nu_Area_Almacen'          : $( '#cbo-AreaAlmacen' ).val(),
                
        // M011 - F

  		  'ID_Tipo_Producto'          : iIDTipoExistenciaProducto,
  		  'ID_Ubicacion_Inventario'   : $( '#cbo-UbicacionesInventario' ).val(),
  			'Nu_Codigo_Barra'           : $( '#txt-Nu_Codigo_Barra' ).val(),
        'No_Producto'               : $( '[name="No_Producto"]' ).val(),
        'Ss_Precio' : $( '[name="Ss_Precio"]' ).val(),
        'Ss_Costo' : $( '[name="Ss_Costo"]' ).val(),
  			'No_Codigo_Interno'         : $( '#txt-No_Codigo_Interno' ).val(),
  			'ID_Impuesto'               : $( '#cbo-Impuestos' ).val(),
  			'Nu_Lote_Vencimiento'       : $( '#cbo-lote_vencimiento' ).val(),
  			'ID_Unidad_Medida'          : $( '#cbo-UnidadesMedida' ).val(),
  			'ID_Marca'                  : $( '#cbo-Marcas' ).val(),
  			'ID_Familia'                : $( '#cbo-categoria' ).val(),
  			'ID_Sub_Familia'            : $( '#cbo-sub_categoria' ).val(),
  			'Nu_Stock_Minimo'           : $( '#tel-Nu_Stock_Minimo' ).val(),
  			'Qt_CO2_Producto'           : $( '#tel-Qt_CO2_Producto' ).val(),
  			'Nu_Receta_Medica'          : $( '#cbo-receta_medica' ).val(),
  			'ID_Laboratorio'            : $( '#cbo-laboratorio' ).val(),
  			'Txt_Composicion'           : $( '#cbo-composicion' ).val(),
  			'ID_Impuesto_Icbper'        : $( '#cbo-impuesto_icbper' ).val(),
  			'Nu_Compuesto'              : $( '#cbo-Compuesto' ).val(),
  			'Nu_Estado'                 : $( '#cbo-Estado' ).val(),
  			'Txt_Ubicacion_Producto_Tienda' : $( '#txt-Txt_Ubicacion_Producto_Tienda' ).val(),
  			'Txt_Producto'              : $( '[name="Txt_Producto"]' ).val(),
        'ID_Producto_Sunat'         : $( '#hidden-ID_Tabla_Dato' ).val(),
        'ID_Tipo_Pedido_Lavado' : $( '#cbo-tipo_pedido_lavado' ).val(),
        'No_Imagen_Item' : $( '#hidden-nombre_imagen' ).val(),
        'Ss_Precio_Ecommerce_Online' : $( '[name="Ss_Precio_Ecommerce_Online"]' ).val(),
        'Ss_Precio_Ecommerce_Online_Regular' : $( '[name="Ss_Precio_Ecommerce_Online_Regular"]' ).val(),
  			'ID_Familia_Marketplace' : $( '#cbo-categoria_marketplace' ).val(),
  			'ID_Sub_Familia_Marketplace' : $( '#cbo-sub_categoria_marketplace' ).val(),
        'ID_Marca_Marketplace': $('#cbo-marca_marketplace').val(),
        'No_Modelo_Vehiculo': $('[name="No_Modelo_Vehiculo"]').val(),
        'No_Color_Vehiculo': $('[name="No_Color_Vehiculo"]').val(),
        'No_Motor': $('[name="No_Motor"]').val(),
      };

      $( '#btn-save' ).text('');
      $( '#btn-save' ).attr('disabled', true);
      $( '#btn-save' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
    
      $( '#modal-loader' ).modal('show');
      
      url = base_url + 'Logistica/ReglasLogistica/ProductoController/crudProducto';
    	$.ajax({
        type		  : 'POST',
        dataType	: 'JSON',
    		url		    : url,
    		data		  : {
    		  arrProducto : arrProducto,
    		  arrProductoEnlace : arrProductoEnlace,
    		},
    		success : function( response ){
    		  $( '#modal-loader' ).modal('hide');
    		  
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
      	  $( '#modal-message' ).modal('show');
      	  
    		  if (response.status == 'success'){
    		    accion_producto = '';
    		    
    		    $( '#form-Producto' )[0].reset();
            $( '.div-AgregarEditar' ).hide();
            $( '.div-Listar' ).show();
      	    $( '.modal-message' ).addClass(response.style_modal);
      	    $( '.modal-title-message' ).text(response.message);
      	    setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
      	    reload_table_producto();
    		  } else {
      	    $( '.modal-message' ).addClass(response.style_modal);
      	    $( '.modal-title-message' ).text(response.message);
      	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
    		  }
    		  
          $( '#btn-save' ).text('');
          $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar (ENTER)' );
          $( '#btn-save' ).attr('disabled', false);
    		},
        error: function (jqXHR, textStatus, errorThrown) {
          $( '#modal-loader' ).modal('hide');
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
    	    
      	  $( '#modal-message' ).modal('show');
    	    $( '.modal-message' ).addClass( 'modal-danger' );
    	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
    	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
    	    
    	    //Message for developer
          console.log(jqXHR.responseText);
    	    
          $( '#btn-save' ).text('');
          $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar' );
          $( '#btn-save' ).attr('disabled', false);
        }
    	});
    }
  }// /. if de accion_producto
}

function eliminarProducto(ID_Empresa, ID, Nu_Codigo_Barra, Nu_Compuesto, accion_producto, sNombreImagenItem){
  var $modal_delete = $( '#modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });
  
  $(document).bind('keydown', 'alt+l', function(){
    if ( accion_producto=='delete' ) {
      _eliminarProducto($modal_delete, ID_Empresa, ID, Nu_Codigo_Barra, Nu_Compuesto, sNombreImagenItem);
      accion_producto='';
    }
  });

  $( '#btn-save-delete' ).off('click').click(function () {
    _eliminarProducto($modal_delete, ID_Empresa, ID, Nu_Codigo_Barra, Nu_Compuesto, sNombreImagenItem);
  });
}

function reload_table_producto(){
  table_producto.ajax.reload(null,false);
}

function _eliminarProducto($modal_delete, ID_Empresa, ID, Nu_Codigo_Barra, Nu_Compuesto, sNombreImagenItem){
  $( '#modal-loader' ).modal('show');
  
  if (Nu_Codigo_Barra == '')
    Nu_Codigo_Barra = '-';
  
  url = base_url + 'Logistica/ReglasLogistica/ProductoController/eliminarProducto/' + ID_Empresa + '/' + ID + '/' + Nu_Codigo_Barra + '/' + Nu_Compuesto + '/' + sNombreImagenItem;
  $.ajax({
    url       : url,
    type      : "GET",
    dataType  : "JSON",
    success: function( response ){
      $( '#modal-loader' ).modal('hide');
      
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
    	  accion_producto = '';
    		    
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
  	    reload_table_producto();
		  } else {
    	  accion_producto = '';
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1500);
		  }
    },
    error: function (jqXHR, textStatus, errorThrown) {
    	accion_producto = '';
      $( '#modal-loader' ).modal('hide');
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}