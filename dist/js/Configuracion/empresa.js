var url;
var table_empresa;

$(function () {
  $('.select2').select2();
  $('[data-mask]').inputmask()

  //LAE API SUNAT
  $('#btn-cloud-api_empresa').click(function () {
    if ($('#txt-Nu_Documento_Identidad').val().length < 11) {
      $('#txt-Nu_Documento_Identidad').closest('.form-group').find('.help-block').html('RUC inválido');
      $('#txt-Nu_Documento_Identidad').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $('#btn-cloud-api_empresa').text('');
      $('#btn-cloud-api_empresa').attr('disabled', true);
      $('#btn-cloud-api_empresa').append('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      //PRUEBAS LOCAL
      //var url_api = 'https://lae-systems-cloudglle.c9users.io/laesystems.com/librerias/sunat/partner/format/json/x-api-key/' + sTokenGlobal;
      var url_api = 'https://www.laesystems.com/librerias/sunat/partner/format/json/x-api-key/' + sTokenGlobal;

      var data = {
        ID_Tipo_Documento_Identidad: 4,
        Nu_Documento_Identidad: $('#txt-Nu_Documento_Identidad').val(),
      };

      $.ajax({
        url: url_api,
        type: 'POST',
        data: data,
        success: function (response) {
          if (response.success === true) {
            $('[name="No_Empresa"]').val(response.data.No_Names);
            $('[name="Txt_Direccion_Empresa"]').val(response.data.Txt_Address);
            if (response.data.Nu_Status === '1')
              $("div.estado select").val("1");
            else
              $("div.estado select").val("0");
          } else {
            $('[name="No_Empresa"]').val('');
            $('[name="Txt_Direccion_Empresa"]').val('');

            $('#txt-Nu_Documento_Identidad').closest('.form-group').find('.help-block').html(response.msg);
            $('#txt-Nu_Documento_Identidad').closest('.form-group').removeClass('has-success').addClass('has-error');
          }

          $('#txt-Nu_Documento_Identidad').focus();

          $('#btn-cloud-api_empresa').text('');
          $('#btn-cloud-api_empresa').attr('disabled', false);
          $('#btn-cloud-api_empresa').append('<i class="fa fa-cloud-download fa-lg"></i>');
        },
        error: function (response) {
          console.log(response);
        }
      });
    }
  })

  url = base_url + 'Configuracion/EmpresaController/ajax_list';
  table_empresa = $('#table-Empresa').DataTable({
    'dom': 'B<"top">frt<"bottom"lp><"clear">',
    buttons: [{
      extend: 'excel',
      text: '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr: 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend: 'pdf',
      text: '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr: 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend: 'colvis',
      text: '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr: 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching': false,
    'bStateSave': true,
    'processing': true,
    'serverSide': true,
    'info': true,
    'autoWidth': false,
    'order': [],
    'pagingType': 'full_numbers',
    'oLanguage': {
      'sInfo': 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu': '_MENU_',
      'sSearch': 'Buscar por: ',
      'sSearchPlaceholder': 'UPC / Nombre',
      'sZeroRecords': 'No se encontraron registros',
      'sInfoEmpty': 'No hay registros',
      'sLoadingRecords': 'Cargando...',
      'sProcessing': 'Procesando...',
      'oPaginate': {
        'sFirst': '<<',
        'sLast': '>>',
        'sPrevious': '<',
        'sNext': '>',
      },
    },
    'ajax': {
      'url': url,
      'type': 'POST',
      'dataType': 'JSON',
      'data': function (data) {
        data.Filtros_Empresas = $('#cbo-Filtros_Empresas').val(),
          data.Global_Filter = $('#txt-Global_Filter').val();
      },
    },
    'columnDefs': [{
      'className': 'text-center',
      'targets': 'no-sort',
      'orderable': false,
    }, {
      'className': 'text-left',
      'targets': 'no-sort_left',
      'orderable': false,
    },],
  });

  $('.dataTables_length').addClass('col-md-3');
  $('.dataTables_paginate').addClass('col-md-9');

  $('#txt-Global_Filter').keyup(function () {
    table_empresa.search($(this).val()).draw();
  });

  $('#form-Empresa').validate({
    rules: {
      Nu_Documento_Identidad: {
        required: true,
        minlength: 11,
        maxlength: 11
      },
      No_Empresa: {
        required: true,
      },
      Txt_Direccion_Empresa: {
        required: true,
      },
      Txt_Usuario_Sunat_Sol: {
        required: true,
      },
      Txt_Password_Sunat_Sol: {
        required: true,
      },
      Txt_Password_Firma_Digital: {
        required: true,
      },
    },
    messages: {
      Nu_Documento_Identidad: {
        required: "Ingresar número",
        minlength: "Debe ingresar 11 dígitos",
        maxlength: "Debe ingresar 11 dígitos"
      },
      No_Empresa: {
        required: "Ingresar razón social"
      },
      Txt_Direccion_Empresa: {
        required: "Ingresar dirección"
      },
      Txt_Usuario_Sunat_Sol: {
        required: "Ingresar usuario SOL"
      },
      Txt_Password_Sunat_Sol: {
        required: "Ingresar contraseña SOL"
      },
      Txt_Password_Firma_Digital: {
        required: "Ingresar contraseña firma"
      },
    },
    errorPlacement: function (error, element) {
      $(element).closest('.form-group').find('.help-block').html(error.html());
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.form-group').find('.help-block').html('');
    },
    submitHandler: form_Empresa
  });

  $('#cbo-tipo_proveedor_fe').change(function () {
    $('.div-row-nubefact').show();
    if ($(this).val() == 1 || $(this).val() == 3) {
      $('.div-row-nubefact').hide();
    }
  });

  $('#form-Empresa').submit(function (e) {
    $('.help-block').empty();
    if ($('#txt-Nu_Documento_Identidad').val().length == 0) {
      $('#txt-Nu_Documento_Identidad').closest('.form-group').find('.help-block').html('Ingresar RUC');
      $('#txt-Nu_Documento_Identidad').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('[name="No_Empresa"]').val().length === 0) {
      $('[name="No_Empresa"]').closest('.form-group').find('.help-block').html('Ingresar razón social');
      $('[name="No_Empresa"]').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('[name="Txt_Direccion_Empresa"]').val().length === 0) {
      $('[name="Txt_Direccion_Empresa"]').closest('.form-group').find('.help-block').html('Ingresar dirección');
      $('[name="Txt_Direccion_Empresa"]').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('#cbo-ubigeo_inei').val() == '') {
      $('#cbo-ubigeo_inei').closest('.form-group').find('.help-block').html('Seleccionar ubigeo inei');
      $('#cbo-ubigeo_inei').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('#cbo-Paises').val() == '') {
      $('#cbo-Paises').closest('.form-group').find('.help-block').html('Seleccionar país');
      $('#cbo-Paises').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('#cbo-Departamentos').val() == '') {
      $('#cbo-Departamentos').closest('.form-group').find('.help-block').html('Seleccionar departamento');
      $('#cbo-Departamentos').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('#cbo-Provincias').val() == '') {
      $('#cbo-Provincias').closest('.form-group').find('.help-block').html('Seleccionar provincia');
      $('#cbo-Provincias').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('#cbo-Distritos').val() == '') {
      $('#cbo-Distritos').closest('.form-group').find('.help-block').html('Seleccionar distrito');
      $('#cbo-Distritos').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('[name="Txt_Usuario_Sunat_Sol"]').val().length === 0) {
      $('[name="Txt_Usuario_Sunat_Sol"]').closest('.form-group').find('.help-block').html('Ingresar usuario SOL');
      $('[name="Txt_Usuario_Sunat_Sol"]').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('[name="Txt_Password_Sunat_Sol"]').val().length === 0) {
      $('[name="Txt_Password_Sunat_Sol"]').closest('.form-group').find('.help-block').html('Ingresar contraseña SOL');
      $('[name="Txt_Password_Sunat_Sol"]').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-tipo_proveedor_fe').val() == 2 && $('[name="Txt_Password_Firma_Digital"]').val().length === 0) {
      $('[name="Txt_Password_Firma_Digital"]').closest('.form-group').find('.help-block').html('Ingresar contraseña certificado');
      $('[name="Txt_Password_Firma_Digital"]').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      e.preventDefault();

      $('#btn-save').text('');
      $('#btn-save').attr('disabled', true);
      $('#btn-save').append('Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      $('#modal-loader').modal('show');

      url = base_url + 'Configuracion/EmpresaController/crudEmpresa';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (response) {
          $('#modal-loader').modal('hide');

          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('.modal-title-message').text('');
          $('#modal-message').modal('show');

          if (response.status == 'success') {
            $('#form-Empresa')[0].reset();
            $('#upload-file-certificado_digital').text('');

            $('#modal-Empresa').modal('hide');
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
            reload_table_empresa();
          } else {
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
          }

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar');
          $('#btn-save').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          $('#modal-loader').modal('hide');
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');

          $('#modal-message').modal('show');
          $('.modal-message').addClass('modal-danger');
          $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

          //Message for developer
          console.log(jqXHR.responseText);

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar');
          $('#btn-save').attr('disabled', false);
        }
      });
    } // if y else - validaciones
  });

  $('#cbo-Paises').change(function () {
    $('#cbo-Departamentos').html('');
    if ($(this).val() > 0) {
      url = base_url + 'HelperController/getDepartamentos';
      $.post(url, { ID_Pais: $(this).val() }, function (response) {
        $('#cbo-Departamentos').html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var i = 0; i < response.length; i++)
          $('#cbo-Departamentos').append('<option value="' + response[i].ID_Departamento + '">' + response[i].No_Departamento + '</option>');
      }, 'JSON');
    }
  })

  $('#cbo-Departamentos').change(function () {
    $('#cbo-Provincias').html('');
    if ($(this).val() > 0) {
      url = base_url + 'HelperController/getProvincias';
      $.post(url, { ID_Departamento: $(this).val() }, function (response) {
        $('#cbo-Provincias').html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var i = 0; i < response.length; i++)
          $('#cbo-Provincias').append('<option value="' + response[i].ID_Provincia + '">' + response[i].No_Provincia + '</option>');
      }, 'JSON');
    }
  })

  $('#cbo-Provincias').change(function () {
    $('#cbo-Distritos').html('');
    if ($(this).val() > 0) {
      url = base_url + 'HelperController/getDistritos';
      $.post(url, { ID_Provincia: $(this).val() }, function (response) {
        $('#cbo-Distritos').html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var i = 0; i < response.length; i++)
          $('#cbo-Distritos').append('<option value="' + response[i].ID_Distrito + '">' + response[i].No_Distrito + '</option>');
      }, 'JSON');
    }
  })

  $('#cbo-tipo_ecommerce_empresa').change(function () {
    $('.div-row-empresas-marketplace').hide();
    $('#cbo-empresa-marketplace').html('<option value="0" selected="selected">- Vacío -</option>');
    if ($('#cbo-tipo_ecommerce_empresa').find(':selected').data('tipo') == 2) {
      $('.div-row-empresas-marketplace').show();
      $('#cbo-empresa-marketplace').html('<option value="0" selected="selected">- Vacío -</option>');
      url = base_url + 'HelperController/getEmpresasMarketplace';
      $.post(url, {}, function (response) {
        if (response.sStatus == 'success') {
          var l = response.arrData.length;
          $('#cbo-empresa-marketplace').html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            $('#cbo-empresa-marketplace').append('<option value="' + response.arrData[x].ID_Empresa + '">' + response.arrData[x].No_Empresa + '</option>');
          }
        } else {
          if (response.sMessageSQL !== undefined) {
            console.log(response.sMessageSQL);
          }
          if (response.sStatus == 'warning')
            $('#cbo-empresa-marketplace').html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');
    }
  })
})

function agregarEmpresa() {
  $('#modal-loader').modal('show');

  $('#form-Empresa')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('#modal-Empresa').modal('show');
  $('.modal-title').text('Nueva Empresa');

  $('[name="EID_Empresa"]').val('');
  $('[name="ENu_Documento_Identidad"]').val('');

  $('.div-row-empresas-marketplace').hide();

  $('.div-Estado').hide();
  $('#cbo-Estado').html('<option value="1">Activo</option>');

  url = base_url + 'HelperController/getValoresTablaDato';
  $.post(url, { sTipoData: 'Tipos_Proveedor_FE' }, function (response) {
    if (response.sStatus == 'success') {
      var l = response.arrData.length;
      $('#cbo-tipo_proveedor_fe').html('<option value="" selected="selected">- Seleccionar -</option>');
      for (var x = 0; x < l; x++) {
        $('#cbo-tipo_proveedor_fe').append('<option value="' + response.arrData[x].Nu_Valor + '">' + response.arrData[x].No_Descripcion + '</option>');
      }
    } else {
      if (response.sMessageSQL !== undefined) {
        console.log(response.sMessageSQL);
      }
      if (response.sStatus == 'warning')
        $('#cbo-tipo_proveedor_fe').html('<option value="0" selected="selected">- Vacío -</option>');
    }
  }, 'JSON');

  url = base_url + 'HelperController/getValoresTablaDato';
  $.post(url, { sTipoData: 'Tipos_Ecommerce_Empresa' }, function (response) {
    if (response.sStatus == 'success') {
      var l = response.arrData.length;
      $('#cbo-tipo_ecommerce_empresa').html('<option value="" selected="selected">- Seleccionar -</option>');
      for (var x = 0; x < l; x++) {
        $('#cbo-tipo_ecommerce_empresa').append('<option value="' + response.arrData[x].ID_Tabla_Dato + '" data-tipo="' + response.arrData[x].Nu_Valor + '">' + response.arrData[x].No_Descripcion + '</option>');
      }
    } else {
      if (response.sMessageSQL !== undefined) {
        console.log(response.sMessageSQL);
      }
      if (response.sStatus == 'warning')
        $('#cbo-tipo_ecommerce_empresa').html('<option value="0" selected="selected">- Vacío -</option>');
    }
  }, 'JSON');

  $('#cbo-empresa-marketplace').html('<option value="0" selected="selected">- Vacío -</option>');

  url = base_url + 'HelperController/getPaises';
  $.post(url, function (response) {
    $('#modal-loader').modal('hide');
    $('#cbo-Paises').html('<option value="" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-Paises').append('<option value="' + response[i].ID_Pais + '">' + response[i].No_Pais + '</option>');
  }, 'JSON');

  url = base_url + 'HelperController/getValoresTablaDato';
  var arrParams = {
    sTipoData: 'Ubigeo_INEI',
  }
  $.post(url, arrParams, function (response) {
    if (response.sStatus == 'success') {
      var iTotalRegistros = response.arrData.length, response = response.arrData;
      $('#cbo-ubigeo_inei').html('<option value="" selected="selected">- Seleccionar -</option>');
      for (var i = 0; i < iTotalRegistros; i++)
        $('#cbo-ubigeo_inei').append('<option value="' + response[i].ID_Tabla_Dato + '">' + response[i].Nu_Valor + ': ' + response[i].No_Descripcion + '</option>');
    } else {
      $('#cbo-ubigeo_inei').html('<option value="" selected="selected">- Vacío -</option>');
      console.log(response);
    }
  }, 'JSON');
}

function verEmpresa(ID) {
  $('#form-Empresa')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('#modal-loader').modal('show');

  url = base_url + 'Configuracion/EmpresaController/ajax_edit/' + ID;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      $('#modal-Empresa').modal('show');
      $('.modal-title').text('Modifcar Empresa');

      $('[name="EID_Empresa"]').val(response.ID_Empresa);
      $('[name="ENu_Documento_Identidad"]').val(response.Nu_Documento_Identidad);

      $('.div-row-nubefact').show();
      if (response.Nu_Tipo_Proveedor_FE == 1 || response.Nu_Tipo_Proveedor_FE == 3) {
        $('.div-row-nubefact').hide();
      }

      url = base_url + 'HelperController/getValoresTablaDato';
      $.post(url, { sTipoData: 'Tipos_Proveedor_FE' }, function (responseProveedorFE) {
        $('#cbo-tipo_proveedor_fe').html('<option value="" selected="selected">- Seleccionar -</option>');
        if (responseProveedorFE.sStatus == 'success') {
          var l = responseProveedorFE.arrData.length;
          $('#cbo-tipo_proveedor_fe').html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            selected = '';
            if (response.Nu_Tipo_Proveedor_FE == responseProveedorFE.arrData[x].Nu_Valor)
              selected = 'selected="selected"';
            $('#cbo-tipo_proveedor_fe').append('<option value="' + responseProveedorFE.arrData[x].Nu_Valor + '" ' + selected + '>' + responseProveedorFE.arrData[x].No_Descripcion + '</option>');
          }
        } else {
          if (responseProveedorFE.sMessageSQL !== undefined) {
            console.log(responseProveedorFE.sMessageSQL);
          }
          if (responseProveedorFE.sStatus == 'warning')
            $('#cbo-tipo_proveedor_fe').html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getValoresTablaDato';
      $.post(url, { sTipoData: 'Tipos_Ecommerce_Empresa' }, function (responseEcommerce) {
        $('#cbo-tipo_ecommerce_empresa').html('<option value="0" selected="selected">- Seleccionar -</option>');
        if (responseEcommerce.sStatus == 'success') {
          var l = responseEcommerce.arrData.length;
          $('#cbo-tipo_ecommerce_empresa').html('<option value="0" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            selected = '';
            if (response.Nu_Tipo_Ecommerce_Empresa == responseEcommerce.arrData[x].ID_Tabla_Dato)
              selected = 'selected="selected"';
            $('#cbo-tipo_ecommerce_empresa').append('<option value="' + responseEcommerce.arrData[x].ID_Tabla_Dato + '" data-tipo="' + responseEcommerce.arrData[x].Nu_Valor + '" ' + selected + '>' + responseEcommerce.arrData[x].No_Descripcion + '</option>');
          }
        } else {
          if (responseEcommerce.sMessageSQL !== undefined) {
            console.log(responseEcommerce.sMessageSQL);
          }
          if (responseEcommerce.sStatus == 'warning')
            $('#cbo-tipo_ecommerce_empresa').html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');

      $('.div-row-empresas-marketplace').hide();
      if (response.ID_Empresa_Marketplace > 0)
        $('.div-row-empresas-marketplace').show();

      url = base_url + 'HelperController/getEmpresasMarketplace';
      $.post(url, {}, function (responseEmpresasMarketplace) {
        if (responseEmpresasMarketplace.sStatus == 'success') {
          var l = responseEmpresasMarketplace.arrData.length;
          $('#cbo-empresa-marketplace').html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            selected = '';
            if (response.ID_Empresa_Marketplace == responseEmpresasMarketplace.arrData[x].ID_Empresa)
              selected = 'selected="selected"';
            $('#cbo-empresa-marketplace').append('<option value="' + responseEmpresasMarketplace.arrData[x].ID_Empresa + '" ' + selected + '>' + responseEmpresasMarketplace.arrData[x].No_Empresa + '</option>');
          }
        } else {
          if (responseEmpresasMarketplace.sMessageSQL !== undefined) {
            console.log(responseEmpresasMarketplace.sMessageSQL);
          }
          if (responseEmpresasMarketplace.sStatus == 'warning')
            $('#cbo-empresa-marketplace').html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');

      $('[name="Nu_Documento_Identidad"]').val(response.Nu_Documento_Identidad);
      $('[name="No_Empresa"]').val(response.No_Empresa);
      $('[name="No_Empresa_Comercial"]').val(response.No_Empresa_Comercial);
      $('[name="Txt_Direccion_Empresa"]').val(response.Txt_Direccion_Empresa);

      $('[name="Txt_Usuario_Sunat_Sol"]').val(response.Txt_Usuario_Sunat_Sol);
      $('[name="Txt_Password_Sunat_Sol"]').val(response.Txt_Password_Sunat_Sol);
      $('[name="Txt_Password_Firma_Digital"]').val(response.Txt_Password_Firma_Digital);
      $('#upload-file-certificado_digital').text(response.sNombreArchivoCertificadoDigital);

      $('.div-Estado').show();
      $('#cbo-Estado').html('');
      var selected;
      for (var i = 0; i < 2; i++) {
        selected = '';
        if (response.Nu_Estado == i)
          selected = 'selected="selected"';
        $('#cbo-Estado').append('<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'Inactivo' : 'Activo') + '</option>');
      }

      url = base_url + 'HelperController/getValoresTablaDato';
      var arrParams = {
        sTipoData: 'Ubigeo_INEI',
      }
      $.post(url, arrParams, function (responseUbigeoInei) {
        if (responseUbigeoInei.sStatus == 'success') {
          var iTotalRegistros = responseUbigeoInei.arrData.length, responseUbigeoInei = responseUbigeoInei.arrData;
          $('#cbo-ubigeo_inei').html('<option value="" selected="selected">- Seleccionar -</option>');
          for (var i = 0; i < iTotalRegistros; i++) {
            selected = '';
            if (response.ID_Ubigeo_Inei == responseUbigeoInei[i].ID_Tabla_Dato)
              selected = 'selected="selected"';
            $('#cbo-ubigeo_inei').append('<option value="' + responseUbigeoInei[i].ID_Tabla_Dato + '" ' + selected + '>' + responseUbigeoInei[i].Nu_Valor + ': ' + responseUbigeoInei[i].No_Descripcion + '</option>');
          }
        } else {
          $('#cbo-ubigeo_inei').html('<option value="" selected="selected">- Vacío -</option>');
          console.log(response);
        }
      }, 'JSON');

      url = base_url + 'HelperController/getPaises';
      $.post(url, function (responsePais) {
        $('#cbo-Paises').html('');
        for (var i = 0; i < responsePais.length; i++) {
          selected = '';
          if (response.ID_Pais == responsePais[i].ID_Pais)
            selected = 'selected="selected"';
          $('#cbo-Paises').append('<option value="' + responsePais[i].ID_Pais + '" ' + selected + '>' + responsePais[i].No_Pais + '</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getDepartamentos';
      $.post(url, { ID_Pais: response.ID_Pais }, function (responseDepartamentos) {
        $('#cbo-Departamentos').html('');
        for (var i = 0; i < responseDepartamentos.length; i++) {
          selected = '';
          if (response.ID_Departamento == responseDepartamentos[i].ID_Departamento)
            selected = 'selected="selected"';
          $('#cbo-Departamentos').append('<option value="' + responseDepartamentos[i].ID_Departamento + '" ' + selected + '>' + responseDepartamentos[i].No_Departamento + '</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getProvincias';
      $.post(url, { ID_Departamento: response.ID_Departamento }, function (responseProvincia) {
        $('#cbo-Provincias').html('');
        for (var i = 0; i < responseProvincia.length; i++) {
          selected = '';
          if (response.ID_Provincia == responseProvincia[i].ID_Provincia)
            selected = 'selected="selected"';
          $('#cbo-Provincias').append('<option value="' + responseProvincia[i].ID_Provincia + '" ' + selected + '>' + responseProvincia[i].No_Provincia + '</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getDistritos';
      $.post(url, { ID_Provincia: response.ID_Provincia }, function (responseDistrito) {
        $('#cbo-Distritos').html('');
        for (var i = 0; i < responseDistrito.length; i++) {
          selected = '';
          if (response.ID_Distrito == responseDistrito[i].ID_Distrito)
            selected = 'selected="selected"';
          $('#cbo-Distritos').append('<option value="' + responseDistrito[i].ID_Distrito + '" ' + selected + '>' + responseDistrito[i].No_Distrito + '</option>');
        }
      }, 'JSON');

      $('#modal-loader').modal('hide');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $('#modal-loader').modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');

      $('#modal-message').modal('show');
      $('.modal-message').addClass('modal-danger');
      $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
      setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

      //Message for developer
      console.log(jqXHR.responseText);
    }
  });
}

function form_Empresa() {
}

function eliminarEmpresa(ID) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $('#btn-save-delete').off('click').click(function () {
    $('#modal-loader').modal('show');

    url = base_url + 'Configuracion/EmpresaController/eliminarEmpresa/' + ID;
    $.ajax({
      url: url,
      type: "GET",
      dataType: "JSON",
      success: function (response) {
        $('#modal-loader').modal('hide');

        $modal_delete.modal('hide');
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.status == 'success') {
          $('.modal-message').addClass(response.style_modal);
          $('.modal-title-message').text(response.message);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
          reload_table_empresa();
        } else {
          $('.modal-message').addClass(response.style_modal);
          $('.modal-title-message').text(response.message);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        $('#modal-loader').modal('hide');
        $modal_delete.modal('hide');
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');

        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

        //Message for developer
        console.log(jqXHR.responseText);
      }
    });
  });
}

function configuracionAutomaticaOpciones(ID, Nu_Estado_Empresa, Nu_Tipo_Proveedor_FE, Txt_Direccion_Empresa) {
  var $modal_id = $('#modal-configuracion_automatica');
  $modal_id.modal('show');

  $('.modal-header-title-configuracion_automatica').text('¿Deseas configurar la empresa automáticamente?');

  url = base_url + 'HelperController/getValoresTablaDato';
  $.post(url, { sTipoData: 'Tipo_Rubro_Empresa' }, function (response) {
    if (response.sStatus == 'success') {
      var iTotalRegistros = response.arrData.length, response = response.arrData;
      $('#cbo-tipo_rubro_empresa_automatico').html('<option value="0" selected="selected">- Seleccionar -</option>');
      for (var i = 0; i < iTotalRegistros; i++)
        $('#cbo-tipo_rubro_empresa_automatico').append('<option value="' + response[i].Nu_Valor + '">' + response[i].No_Descripcion + '</option>');
    } else {
      $('#cbo-tipo_rubro_empresa_automatico').html('<option value="0" selected="selected">- Vacío -</option>');
      console.log(response);
    }
  }, 'JSON');

  $('#modal-configuracion_automatica').on('shown.bs.modal', function () {
    $('#txt-nombres_apellidos_automatico').focus();
  })

  $('#btn-modal-configuracion_automatica-cancel').off('click').click(function () {
    $modal_id.modal('hide');
  });

  $("#txt-email_automatico").blur(function () {
    caracteresCorreoValido($(this).val(), '#span-email');
  })

  $('#btn-modal-configuracion_automatica-send').off('click').click(function () {
    if ($("#txt-nombres_apellidos_automatico").val().length === 0) {
      $('#txt-nombres_apellidos_automatico').closest('.form-group').find('.help-block').html('Ingresar nombres y apellidos');
      $('#txt-nombres_apellidos_automatico').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('#modal-configuracion_automatica .modal-body'), $('#txt-nombres_apellidos_automatico'));
    } else if ($('#cbo-tipo_rubro_empresa_automatico').val() == 0) {
      $('#cbo-tipo_rubro_empresa_automatico').closest('.form-group').find('.help-block').html('Seleccionar tipo impuesto');
      $('#cbo-tipo_rubro_empresa_automatico').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('#modal-configuracion_automatica .modal-body'), $('#cbo-tipo_rubro_empresa_automatico'));
    } else if (!caracteresCorreoValido($('#txt-email_automatico').val(), '#div-email')) {
      scrollToError($('#modal-configuracion_automatica .modal-body'), $('#txt-email_automatico'));
    } else if ($("#txt-password_automatico").val().length === 0) {
      $('#txt-password_automatico').closest('.form-group').find('.help-block').html('Ingresar contraseña');
      $('#txt-password_automatico').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('#modal-configuracion_automatica .modal-body'), $('#txt-password_automatico'));
    } else {
      $('#modal-loader').modal('show');

      var arrParams = {
        'iIdEmpresa': ID,
        'iEstadoEmpresa': Nu_Estado_Empresa,
        'iTipoProveedorFE': Nu_Tipo_Proveedor_FE,
        'sDireccionEmpresa': Txt_Direccion_Empresa,
        'iTipoRubroEmpresa': $('#cbo-tipo_rubro_empresa_automatico').val(),
        'sNombresApellidos': $('#txt-nombres_apellidos_automatico').val(),
        'sNumeroCelular': $('#txt-celular_automatico').val(),
        'sEmailUsuario': $('#txt-email_automatico').val(),
        'sPasswordUsuario': $('#txt-password_automatico').val(),
      };

      url = base_url + 'Configuracion/EmpresaController/configuracionAutomaticaOpciones';
      $.ajax({
        url: url,
        type: "POST",
        dataType: "JSON",
        data: arrParams,
        success: function (response) {
          $('#modal-loader').modal('hide');

          $modal_id.modal('hide');
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.sStatus == 'success') {
            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
            reload_table_empresa();
          } else {
            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          $('#modal-loader').modal('hide');
          $modal_id.modal('hide');
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');

          $('#modal-message').modal('show');
          $('.modal-message').addClass('modal-danger');
          $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

          //Message for developer
          console.log(jqXHR.responseText);
        }
      });
    } // if - else validacion
  });
}

function reload_table_empresa() {
  table_empresa.ajax.reload(null, false);
}