//Clear HTML textarea
function clearHTMLTextArea(str){
  str=str.replace(/<br>/gi, "");
  str=str.replace(/<br\s\/>/gi, "");
  str=str.replace(/<br\/>/gi, "");
  str=str.replace(/<\/button>/gi, "");
  str=str.replace(/<br >/gi, "");
  return str;
}

// valid email pattern
var eregex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
$.validator.addMethod("validemail", function( value, element ) {
	return this.optional( element ) || eregex.test( value );
});

$(document).keyup(function(event){
  if(event.which == 27) {//ESC
    $( '.div-AgregarEditar' ).hide();
    $( '.div-Ver' ).hide();
    $( '.div-AgregarEditarPrecio' ).hide();
    $( '.div-Listar' ).show();
  }
});

//Variables globales
var url,
src_root_sitio_web_js = '../../../../',
src_root_sitio_subweb_js = '../../../',
sTokenGlobal = '',
iIdTipoRubroEmpresaGlobal = 0,
iValidarStockGlobal = 0,
iMostrarLogoTicketGlobal = 0,
iFormatoTicketLiquidacionCajaGlobal = 0,
iHeightLogoTicketGlobal = 0,
iWidthLogoTicketGlobal = 0,
iVerificarAutorizacionVentaGlobal = 0,
sTerminosCondicionesTicket = '',
iActivarDescuentoPuntoVenta = 0,
nu_enlace = '',
fYearIni,
fYearFin,
fInicioSistema,
fToday = new Date(), // Date today
fYear = fToday.getFullYear(),
fMonth = fToday.getMonth() + 1,//hoy es 0!
fDay = fToday.getDate();
// /. Variables Globales

$(function () {
  // Div formulario para agregar / editar
  $( '.div-AgregarEditar' ).hide();
  $( '.div-Ver' ).hide();
  
  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
  })
  
	$.fn.datepicker.dates['en'] = {
    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    daysShort: ["Dom", "Lun", "Mar", "Mié", "Juv", "Vie", "Sáb"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "dd/mm/yyyy",
    titleFormat: "MM yyyy",
    weekStart: 0
  };
  
  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  
  $( '.input-datepicker' ).inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  $( '.input-datepicker' ).val(fDay + '/' + fMonth + '/' + fYear);
	$( '.input-datepicker' ).datepicker({
		autoclose : true,
		startDate : new Date(fYear + '-' + fMonth + '-' + (parseInt(fDay) + 1)),
		todayHighlight : true
  });
  
  $( '.date-picker-invoice' ).inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  $( '.date-picker-invoice' ).val(fDay + '/' + fMonth + '/' + fYear);
	
  $( '.input-datepicker-today-to-more' ).inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  $( '.input-datepicker-today-to-more' ).val(fDay + '/' + fMonth + '/' + fYear);
  
  //Date picker employee
	fYearIni = fYear - 65;//Edad para empleo 35 significa años
	fYearFin = fYear - 17;//Edad para empleo 17 significa años
  $( '.date-picker-employee' ).datepicker({
    autoclose : true,
    startDate : new Date(fYearIni + '-' + fMonth + '-' + fDay),
    endDate   : new Date(fYearFin + '-' + fMonth + '-' + fDay),
  });
  
  //Validation input's
  $( '.input-codigo_barra' ).on('input', function () {
    this.value = this.value.replace(/[^a-zA-Z0-9]/g,'');
  });
  
  $( '.input-number' ).on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
  });
  
  $( '.input-porcentaje' ).on('input', function () {
    var numero = parseFloat(this.value);
    if(!isNaN(numero)){
      this.value = this.value.replace(/[^0-9\.]/g,'');
      if (numero < 0)
        this.value = '';
    } else
      this.value = this.value.replace(/[^0-9\.]/g,'');
  });
  
  $( '.input-decimal' ).on('input', function () {
    var numero = parseFloat(this.value);
    if(!isNaN(numero)){
      this.value = this.value.replace(/[^0-9\.]/g,'');
      if (numero < 0)
        this.value = '';
    } else
      this.value = this.value.replace(/[^0-9\.]/g,'');
  });
  
  $( '.input-number_operacion' ).on('input', function () {
    var numero = parseFloat(this.value);
    if(!isNaN(numero)){
      this.value = this.value.replace(/[^0-9]/g,'');
      if (numero < 0)
        this.value = '';
    } else
      this.value = this.value.replace(/[^0-9]/g,'');
  });
  
  //Div ocultar / mostrar
  $( '#btn-cancelar' ).click(function() {
    $( '.div-AgregarEditar' ).hide();
    $( '.div-Ver' ).hide();
    $( '.div-Listar' ).show();
  })
  
  $( '#btn-cancelar_precio' ).click(function() {
    $( '.div-AgregarEditarPrecio' ).hide();
    $( '.div-Listar' ).show();
  })
  
  $( '#btn-cerrar_modal_excel' ).click(function() {
    $('#modal-message_excel').modal('toggle');
  })
  
  //Global Autocomplete
  $( '.autocompletar' ).autoComplete({
    minChars: 0,
    source: function(term, response){
      var term                = term.toLowerCase();
      var global_class_method = $( '.autocompletar' ).data('global-class_method');
      var global_table        = $( '.autocompletar' ).data('global-table');
      
      // AQUI 
      //alert(global_class_method  +'/' +global_table );

      var filter_id_codigo = '';
      if ($( '#txt-EID_Producto' ).val() !== undefined)
        filter_id_codigo = $( '#txt-EID_Producto' ).val();
      
      $.post( base_url + global_class_method, { global_table: global_table, global_search : term, filter_id_codigo : filter_id_codigo }, function( arrData ){
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      var data_direccion = '';
      if ($( '#txt-Txt_Direccion_Entidad' ).val() != undefined)
        data_direccion = 'data-direccion_cliente="' + item.Txt_Direccion_Entidad + '"';
      var data_telefono = '';
      if ($( '#txt-Nu_Telefono_Entidad_Cliente' ).val() != undefined)
        data_telefono = 'data-telefono="' + item.Nu_Telefono_Entidad + '"';
      var data_celular = '';
      if ($( '#txt-Nu_Celular_Entidad_Cliente' ).val() != undefined)
        data_celular = 'data-celular="' + item.Nu_Celular_Entidad + '"';
      var data_email = '';
      if ($( '#txt-Txt_Email_Entidad_Cliente' ).val() != undefined)
        data_email = 'data-email="' + item.Txt_Email_Entidad + '"';
      var data_dias_credito = '';
      if ( $( '#txt-Fe_Vencimiento' ).val() != undefined && ($( '#cbo-MediosPago' ).val() != undefined && $( '#cbo-MediosPago' ).find(':selected').data('nu_tipo') == 1) )
        data_dias_credito = 'data-dias_credito="' + item.Nu_Dias_Credito + '"';
      return '<div class="autocomplete-suggestion" data-id="'+item.ID+'" data-codigo="'+item.Codigo+'" data-nombre="'+item.Nombre+'" data-estado="'+item.Nu_Estado+'" data-val="'+search+'" ' + data_direccion + ' ' + data_telefono + ' ' + data_celular + ' ' + data_email + ' ' + data_dias_credito + '>'+item.Nombre.replace(re, "<b>$1</b>")+'</div>';
    },
    onSelect: function(e, term, item){
      $( '#txt-AID' ).val(item.data('id'));
      $( '#txt-ACodigo' ).val(item.data('codigo'));
      $('#txt-ANombre').val(item.data('nombre'));
      if ($('#txt-Filtro_Entidad').val() !== undefined)
        $('#txt-Filtro_Entidad').val(item.data('nombre'));
      if ( $( '#txt-Filtro_Entidad' ).val() !== undefined )
        $( '#txt-Filtro_Entidad' ).val(item.data('nombre'));
      if ( $( '#txt-Txt_Direccion_Entidad' ).val() !== undefined )
        $( '#txt-Txt_Direccion_Entidad' ).val(item.data('direccion_cliente'));
      if ( $( '#txt-Nu_Telefono_Entidad_Cliente' ).val() !== undefined )
        $( '#txt-Nu_Telefono_Entidad_Cliente' ).val(item.data('telefono'));
      if ( $( '#txt-Nu_Celular_Entidad_Cliente' ).val() !== undefined )
        $( '#txt-Nu_Celular_Entidad_Cliente' ).val(item.data('celular'));
      if ( $( '#txt-Txt_Email_Entidad_Cliente' ).val() !== undefined )
        $( '#txt-Txt_Email_Entidad_Cliente' ).val(item.data('email'));
      if ( $( '#label-no_nombres' ).val() !== undefined )
        $('#label-no_nombres').text(item.data('nombre'));
      if ( $( '#hidden-nu_numero_documento_identidad' ).val() !== undefined )
        $('#hidden-nu_numero_documento_identidad').val(item.data('codigo'));
      if ( $( '#hidden-estado_entidad' ).val() !== undefined )
        $('#hidden-estado_entidad').val(item.data('estado'));
      if ( $( '#txt-Fe_Vencimiento' ).val() != undefined && ($( '#cbo-MediosPago' ).val() != undefined && $( '#cbo-MediosPago' ).find(':selected').data('nu_tipo') == 1) ) {
        var dNuevaFechaVencimiento = sumaFecha(item.data('dias_credito'), $( '#txt-Fe_Emision' ).val());
        $( '#txt-Fe_Vencimiento' ).val( dNuevaFechaVencimiento );
      }
      $( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
      $( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
    }
  });
  
// PREP_OC-2 - 1 

  $( '.autocompletar_ProveeEnPresup' ).autoComplete({
    minChars: 0,
    source: function(term, response){
      var term                = term.toLowerCase();
      var global_class_method = $( '.autocompletar_ProveeEnPresup' ).data('global-class_method');
      var global_table        = $( '.autocompletar_ProveeEnPresup' ).data('global-table');
      
      /* var filter_id_codigo = '';
      if ($( '#txt-EID_Producto' ).val() !== undefined)
        filter_id_codigo = $( '#txt-EID_Producto' ).val();
      */

      $.post( base_url + global_class_method, { global_table: global_table, global_search : term, filter_id_codigo : '' /* filter_id_codigo */ }, function( arrData ){
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
  
      return '<div class="autocomplete-suggestion" data-id="' + item.ID + '" data-codigo="' + item.Codigo + '" data-nombre="' + item.Nombre +  '" data-val="'+search+'" ' +   '> '+item.Nombre.replace(re, "<b>$1</b>")+'</div>';
    },
    onSelect: function(e, term, item){

      $( '#txt-AID_oc' ).val(item.data('id'));
      $( '#txt-ACodigo_oc' ).val(item.data('codigo'));
      $('#txt-ANombre_oc').val(item.data('nombre'));
      $( '#txt-DireccEntid_oc' ).val(item.data('Txt_Direccion_Entidad'));
      
    }
  });

  $( '.autocompletar_detalle_ProveeEnPresup' ).autoComplete({
    minChars: 0,
    source: function(term, response){
      var term                = term.toLowerCase();
      var global_class_method = $( '.autocompletar_detalle_ProveeEnPresup' ).data('global-class_method');
      var global_table        = $( '.autocompletar_detalle_ProveeEnPresup' ).data('global-table');

      
      var filter_id_almacen = '';
      if ( $( '#cbo-DescargarInventario' ).val() == '1' && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
        filter_id_almacen = $( '#cbo-Almacenes' ).val();

      var filter_nu_compuesto = '';
      if ($( '#txt-Nu_Compuesto' ).val() !== undefined)
        filter_nu_compuesto = $( '#txt-Nu_Compuesto' ).val();

      var filter_nu_tipo_producto = '';
      if ($( '#txt-Nu_Tipo_Producto' ).val() !== undefined)
        filter_nu_tipo_producto = $( '#txt-Nu_Tipo_Producto' ).val();
      
      var filter_lista = 0;
      if ($( '#cbo-lista_precios' ).val() !== undefined)
        filter_lista = $( '#cbo-lista_precios' ).val();
      
      var send_post = {
        global_table : global_table,
        global_search : term,
        filter_id_almacen : filter_id_almacen,
        filter_nu_compuesto : filter_nu_compuesto,
        filter_nu_tipo_producto : filter_nu_tipo_producto,
        filter_lista : filter_lista,
      }
      
      $.post( base_url + global_class_method, send_post, function( arrData ){
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      
      return '<div class="autocomplete-suggestion" data-id="' + item.ID + '" data-nu_tipo_item="' + item.Nu_Tipo_Producto + '" data-codigo="' + item.Codigo + '" data-nombre="' + item.Nombre + '" data-precio="' + Ss_Precio + '" data-nu_tipo_impuesto="' + item.Nu_Tipo_Impuesto + '" data-id_impuesto_cruce_documento="' + item.ID_Impuesto_Cruce_Documento + '" data-ss_impuesto="' + item.Ss_Impuesto + '" data-qt_producto="' + item.Qt_Producto + '" data-unidad_medida="' + item.No_Unidad_Medida + '" data-val="' + search + '">' + '[' + (item.No_Marca !== null ? item.No_Marca : 'Sin marca') + '] ' + item.Nombre.replace(re, "<b>$1</b>") + ' / UM: ' + item.No_Unidad_Medida + ' / Precio: ' + Ss_Precio + ' / stock: ' + (isNaN(parseFloat(item.Qt_Producto)) ? 0 : parseFloat(item.Qt_Producto) ) + '</div>';
    },
    onSelect: function(e, term, item){
      $( '#txt-ID_Producto_oc' ).val(item.data('id'));
      $( '#txt-Nu_Codigo_Barra_oc' ).val(item.data('codigo'));
      $( '#txt-No_Producto_oc' ).val(item.data('nombre'));
      $( '#txt-Qt_Producto_oc' ).val(item.data('qt_producto'));

      url = base_url + 'HelperController/getDataProducto';
     $.post( url, {ID_Producto : item.data('id') }, function( responseprod ){
       for (var i = 0; i < responseprod.length; i++) {
             
       $( '#txt-Ss_Costo' ).val(responseprod[i].Ss_Costo);
       }
     }, 'JSON'); 

    }
  });

// PREP_OC-2 - 2 

// OC-modal-lunes - Inicio   
// OC_MODAL_7 - INICIO
$( '.autocompletar_detalle_de_producto_oc' ).autoComplete({
  minChars: 0,
  source: function(term, response){

    var global_class_method = $( '.autocompletar_detalle_de_producto_oc' ).data('global-class_method');
    var global_table        = $( '.autocompletar_detalle_de_producto_oc' ).data('global-table'); 

    var filter_id_almacen = $( '#cbo-almacen_oc' ).val();
    var filter_nu_compuesto = '0';
    var filter_nu_tipo_producto = '';
        var filter_lista = 0;
    
    var send_post = {
      global_table : global_table,
      global_search : term,
      filter_id_almacen : filter_id_almacen,
      filter_nu_compuesto : filter_nu_compuesto,
      filter_nu_tipo_producto : filter_nu_tipo_producto,
      filter_lista : filter_lista,
    }
    
    $.post( base_url + global_class_method, send_post, function( arrData ){
      response(arrData);
    }, 'JSON');
  },
  renderItem: function (item, search){
    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
    var Ss_Precio = 0;
    if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Precio_Item !== null || item.Ss_Precio_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
      Ss_Precio = item.Ss_Precio_Item;
    if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Costo_Item !== null || item.Ss_Costo_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '0')
      Ss_Precio = item.Ss_Costo_Item;
    if ( item.Ss_Precio !== null )
      Ss_Precio = item.Ss_Precio;
    // return '<div class="autocomplete-suggestion" data-id="' + item.ID + '" data-nu_tipo_item="' + item.Nu_Tipo_Producto + '" data-codigo="' + item.Codigo + '" data-nombre="' + item.Nombre + '" data-precio="' + Ss_Precio + '" data-nu_tipo_impuesto="' + item.Nu_Tipo_Impuesto + '" data-id_impuesto_cruce_documento="' + item.ID_Impuesto_Cruce_Documento + '" data-ss_impuesto="' + item.Ss_Impuesto + '" data-qt_producto="' + item.Qt_Producto + '" data-unidad_medida="' + item.No_Unidad_Medida + '" data-val="' + search + '">' + '[' + (item.No_Marca !== null ? item.No_Marca : 'Sin marca') + '] ' + item.Nombre.replace(re, "<b>$1</b>") + ' / UM: ' + item.No_Unidad_Medida + ' / Precio: ' + Ss_Precio + ' / stock: ' + (isNaN(parseFloat(item.Qt_Producto)) ? 0 : parseFloat(item.Qt_Producto) ) + '</div>';
    return '<div class="autocomplete-suggestion" data-id="' + item.ID + '" data-nu_tipo_item="' + item.Nu_Tipo_Producto + '" data-codigo="' + item.Codigo + '" data-nombre="' + item.Nombre + '" data-costo="' + item.Ss_Costo_Item + '" data-nu_tipo_impuesto="' + item.Nu_Tipo_Impuesto + '" data-id_impuesto_cruce_documento="' + item.ID_Impuesto_Cruce_Documento + '" data-ss_impuesto="' + item.Ss_Impuesto + '" data-qt_producto="' + item.Qt_Producto + '" data-unidad_medida="' + item.No_Unidad_Medida + '" data-val="' + search + '">' + '[' + (item.No_Marca !== null ? item.No_Marca : 'Sin marca') + '] ' + item.Nombre.replace(re, "<b>$1</b>") + ' / UM: ' + item.No_Unidad_Medida + ' / Costo: ' + item.Ss_Costo_Item + ' / stock: ' + (isNaN(parseFloat(item.Qt_Producto)) ? 0 : parseFloat(item.Qt_Producto) ) + '</div>';
  },
  onSelect: function(e, term, item){
    $( '#txt-ID_Producto_oc' ).val(item.data('id'));
    $( '#txt-Nu_Codigo_Barra_oc' ).val(item.data('codigo'));
    $( '#txt-No_Producto_oc' ).val(item.data('nombre'));
    //$( '#txt-Qt_Producto' ).val(item.data('qt_producto'));

    $( '#txt-Ss_Precio_oc' ).val(0);
    url = base_url + 'HelperController/getDataProducto';
   $.post( url, {ID_Producto : item.data('id') }, function( responseprod ){
     for (var i = 0; i < responseprod.length; i++) {
           
     $( '#txt-Ss_Precio_oc' ).val(responseprod[i].Ss_Costo);
     }
   }, 'JSON'); 

    if ( $( '#txt-ID_Impuesto_Cruce_Documento_oc' ).val() !== undefined )
      $( '#txt-ID_Impuesto_Cruce_Documento_oc' ).val(item.data('id_impuesto_cruce_documento'));
    if ( $( '#txt-Nu_Tipo_Impuesto_oc' ).val() !== undefined )
      $( '#txt-Nu_Tipo_Impuesto_oc' ).val(item.data('nu_tipo_impuesto'));
    if ( $( '#txt-Ss_Impuesto_oc' ).val() !== undefined )
      $( '#txt-Ss_Impuesto_oc' ).val(item.data('ss_impuesto'));
    if ( $( '#txt-nu_tipo_item_oc' ).val() !== undefined )
      $('#txt-nu_tipo_item_oc').val(item.data('nu_tipo_item'));
    if ($('#txt-No_Unidad_Medida_oc').val() !== undefined)
      $('#txt-No_Unidad_Medida_oc').val(item.data('unidad_medida'));
  }
});
// OC_MODAL_7 - FIN
// OC-modal-lunes - Fin  

    // OC_MODAL_6 - INICIO
  $( '.autocompletar_detalle_oc_v2' ).autoComplete({ 
    minChars: 0,
    source: function(term, response){
      var global_class_method = $( '.autocompletar_detalle_oc_v2' ).data('global-class_method');
      var global_table        = $( '.autocompletar_detalle_oc_v2' ).data('global-table');

      var filter_id_almacen = '';
      if ( $( '#cbo-DescargarInventario' ).val() == '1' && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
        filter_id_almacen = $( '#cbo-Almacenes' ).val();

      var filter_nu_compuesto = '';
      if ($( '#txt-Nu_Compuesto' ).val() !== undefined)
        filter_nu_compuesto = $( '#txt-Nu_Compuesto' ).val();

      var filter_nu_tipo_producto = '';
      if ($( '#txt-Nu_Tipo_Producto' ).val() !== undefined)
        filter_nu_tipo_producto = $( '#txt-Nu_Tipo_Producto' ).val();
      
      var filter_lista = 0;
      if ($( '#cbo-lista_precios' ).val() !== undefined)
        filter_lista = $( '#cbo-lista_precios' ).val();
      
      var send_post = {
        global_table : global_table,
        global_search : term,
        filter_id_almacen : filter_id_almacen,
        filter_nu_compuesto : filter_nu_compuesto,
        filter_nu_tipo_producto : filter_nu_tipo_producto,
        filter_lista : filter_lista,
      }
      
      $.post( base_url + global_class_method, send_post, function( arrData ){
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      var Ss_Precio = 0;
      if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Precio_Item !== null || item.Ss_Precio_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
        Ss_Precio = item.Ss_Precio_Item;
      if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Costo_Item !== null || item.Ss_Costo_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '0')
        Ss_Precio = item.Ss_Costo_Item;
      if ( item.Ss_Precio !== null )
        Ss_Precio = item.Ss_Precio;
      return '<div class="autocomplete-suggestion" data-id="' + item.ID + '" data-nu_tipo_item="' + item.Nu_Tipo_Producto + '" data-codigo="' + item.Codigo + '" data-nombre="' + item.Nombre + '" data-precio="' + Ss_Precio + '" data-nu_tipo_impuesto="' + item.Nu_Tipo_Impuesto + '" data-id_impuesto_cruce_documento="' + item.ID_Impuesto_Cruce_Documento + '" data-ss_impuesto="' + item.Ss_Impuesto + '" data-qt_producto="' + item.Qt_Producto + '" data-unidad_medida="' + item.No_Unidad_Medida + '" data-val="' + search + '">' + '[' + (item.No_Marca !== null ? item.No_Marca : 'Sin marca') + '] ' + item.Nombre.replace(re, "<b>$1</b>") + ' / UM: ' + item.No_Unidad_Medida + ' / Precio: ' + Ss_Precio + ' / stock: ' + (isNaN(parseFloat(item.Qt_Producto)) ? 0 : parseFloat(item.Qt_Producto) ) + '</div>';
    },
    onSelect: function(e, term, item){
      $( '#txt-ID_Producto' ).val(item.data('id'));
      $( '#txt-Nu_Codigo_Barra' ).val(item.data('codigo'));
      $( '#txt-No_Producto' ).val(item.data('nombre'));
      $( '#txt-Qt_Producto' ).val(item.data('qt_producto'));

      url = base_url + 'HelperController/getDataProducto';
     $.post( url, {ID_Producto : item.data('id') }, function( responseprod ){
       for (var i = 0; i < responseprod.length; i++) {
             
       $( '#txt-Ss_Precio' ).val(responseprod[i].Ss_Costo);
       }
     }, 'JSON'); 

      if ( $( '#txt-ID_Impuesto_Cruce_Documento' ).val() !== undefined )
        $( '#txt-ID_Impuesto_Cruce_Documento' ).val(item.data('id_impuesto_cruce_documento'));
      if ( $( '#txt-Nu_Tipo_Impuesto' ).val() !== undefined )
        $( '#txt-Nu_Tipo_Impuesto' ).val(item.data('nu_tipo_impuesto'));
      if ( $( '#txt-Ss_Impuesto' ).val() !== undefined )
        $( '#txt-Ss_Impuesto' ).val(item.data('ss_impuesto'));
      if ( $( '#txt-nu_tipo_item' ).val() !== undefined )
        $('#txt-nu_tipo_item').val(item.data('nu_tipo_item'));
      if ($('#txt-No_Unidad_Medida').val() !== undefined)
        $('#txt-No_Unidad_Medida').val(item.data('unidad_medida'));
    }
  });
  
  // OC_MODAL_6 - FIN



  //Global autocomplete Producto
  $( '.autocompletar_detalle' ).autoComplete({
    minChars: 0,
    source: function(term, response){
      var global_class_method = $( '.autocompletar_detalle' ).data('global-class_method');
      var global_table        = $( '.autocompletar_detalle' ).data('global-table');

      var filter_id_almacen = '';
      if ( $( '#cbo-DescargarInventario' ).val() == '1' && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
        filter_id_almacen = $( '#cbo-Almacenes' ).val();

      var filter_nu_compuesto = '';
      if ($( '#txt-Nu_Compuesto' ).val() !== undefined)
        filter_nu_compuesto = $( '#txt-Nu_Compuesto' ).val();

      var filter_nu_tipo_producto = '';
      if ($( '#txt-Nu_Tipo_Producto' ).val() !== undefined)
        filter_nu_tipo_producto = $( '#txt-Nu_Tipo_Producto' ).val();
      
      var filter_lista = 0;
      if ($( '#cbo-lista_precios' ).val() !== undefined)
        filter_lista = $( '#cbo-lista_precios' ).val();
      
      var send_post = {
        global_table : global_table,
        global_search : term,
        filter_id_almacen : filter_id_almacen,
        filter_nu_compuesto : filter_nu_compuesto,
        filter_nu_tipo_producto : filter_nu_tipo_producto,
        filter_lista : filter_lista,
      }
      
      $.post( base_url + global_class_method, send_post, function( arrData ){
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      var Ss_Precio = 0;
      if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Precio_Item !== null || item.Ss_Precio_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
        Ss_Precio = item.Ss_Precio_Item;
      if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Costo_Item !== null || item.Ss_Costo_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '0')
        Ss_Precio = item.Ss_Costo_Item;
      if ( item.Ss_Precio !== null )
        Ss_Precio = item.Ss_Precio;
      return '<div class="autocomplete-suggestion" data-id="' + item.ID + '" data-nu_tipo_item="' + item.Nu_Tipo_Producto + '" data-codigo="' + item.Codigo + '" data-nombre="' + item.Nombre + '" data-precio="' + Ss_Precio + '" data-nu_tipo_impuesto="' + item.Nu_Tipo_Impuesto + '" data-id_impuesto_cruce_documento="' + item.ID_Impuesto_Cruce_Documento + '" data-ss_impuesto="' + item.Ss_Impuesto + '" data-qt_producto="' + item.Qt_Producto + '" data-unidad_medida="' + item.No_Unidad_Medida + '" data-val="' + search + '">' + '[' + (item.No_Marca !== null ? item.No_Marca : 'Sin marca') + '] ' + item.Nombre.replace(re, "<b>$1</b>") + ' / UM: ' + item.No_Unidad_Medida + ' / Precio: ' + Ss_Precio + ' / stock: ' + (isNaN(parseFloat(item.Qt_Producto)) ? 0 : parseFloat(item.Qt_Producto) ) + '</div>';
    },
    onSelect: function(e, term, item){
      $( '#txt-ID_Producto' ).val(item.data('id'));
      $( '#txt-Nu_Codigo_Barra' ).val(item.data('codigo'));
      $( '#txt-No_Producto' ).val(item.data('nombre'));
      $( '#txt-Qt_Producto' ).val(item.data('qt_producto'));

      // M041 - I 

      url = base_url + 'HelperController/getDataProducto';
     $.post( url, {ID_Producto : item.data('id') }, function( responseprod ){
       for (var i = 0; i < responseprod.length; i++) {
             
       $( '#txt-Ss_Costo' ).val(responseprod[i].Ss_Costo);
       }
     }, 'JSON'); 

     // M041 - F 

      if ( $( '#txt-Ss_Precio' ).val() !== undefined )
        $( '#txt-Ss_Precio' ).val(item.data('precio'));
      if ( $( '#txt-ID_Impuesto_Cruce_Documento' ).val() !== undefined )
        $( '#txt-ID_Impuesto_Cruce_Documento' ).val(item.data('id_impuesto_cruce_documento'));
      if ( $( '#txt-Nu_Tipo_Impuesto' ).val() !== undefined )
        $( '#txt-Nu_Tipo_Impuesto' ).val(item.data('nu_tipo_impuesto'));
      if ( $( '#txt-Ss_Impuesto' ).val() !== undefined )
        $( '#txt-Ss_Impuesto' ).val(item.data('ss_impuesto'));
      if ( $( '#txt-nu_tipo_item' ).val() !== undefined )
        $('#txt-nu_tipo_item').val(item.data('nu_tipo_item'));
      if ($('#txt-No_Unidad_Medida').val() !== undefined)
        $('#txt-No_Unidad_Medida').val(item.data('unidad_medida'));
    }
  });
  
  $( '.autocompletar_detalle_version2' ).autoComplete({
    minChars: 0,
    source: function(term, response){
      var global_class_method = $( '.autocompletar_detalle' ).data('global-class_method');
      var global_table        = $( '.autocompletar_detalle' ).data('global-table');

      var filter_id_almacen = '';
      if ( $( '#cbo-DescargarInventario' ).val() == '1' && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
        filter_id_almacen = $( '#cbo-Almacenes' ).val();

      var filter_nu_compuesto = '';
      if ($( '#txt-Nu_Compuesto' ).val() !== undefined)
        filter_nu_compuesto = $( '#txt-Nu_Compuesto' ).val();

      var filter_nu_tipo_producto = '';
      if ($( '#txt-Nu_Tipo_Producto' ).val() !== undefined)
        filter_nu_tipo_producto = $( '#txt-Nu_Tipo_Producto' ).val();
      
      var filter_lista = 0;
      if ($( '#cbo-lista_precios' ).val() !== undefined)
        filter_lista = $( '#cbo-lista_precios' ).val();
      
      var send_post = {
        global_table : global_table,
        global_search : term,
        filter_id_almacen : filter_id_almacen,
        filter_nu_compuesto : filter_nu_compuesto,
        filter_nu_tipo_producto : filter_nu_tipo_producto,
        filter_lista : filter_lista,
      }
      
      $.post( base_url + global_class_method, send_post, function( arrData ){
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      var Ss_Precio = 0;
      if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Precio_Item !== null || item.Ss_Precio_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '1')
        Ss_Precio = item.Ss_Precio_Item;
      if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Costo_Item !== null || item.Ss_Costo_Item != 0.000000) && $( '#txt-Nu_Tipo_Registro' ).val() == '0')
        Ss_Precio = item.Ss_Costo_Item;
      if ( item.Ss_Precio !== null )
        Ss_Precio = item.Ss_Precio;
      return '<div class="autocomplete-suggestion" data-id="'+item.ID+'" data-nu_tipo_item="'+item.Nu_Tipo_Producto+'" data-codigo="'+item.Codigo+'" data-nombre="'+item.Nombre+'" data-precio="'+Ss_Precio+'" data-nu_tipo_impuesto="'+item.Nu_Tipo_Impuesto+'" data-id_impuesto_cruce_documento="'+item.ID_Impuesto_Cruce_Documento+'" data-ss_impuesto="'+item.Ss_Impuesto+'" data-qt_producto="'+item.Qt_Producto+'" data-val="'+search+'">' + '[' + (item.No_Marca !== null ? item.No_Marca : 'Sin marca') + '] ' + item.Nombre.replace(re, "<b>$1</b>") + ' / Precio: ' + Ss_Precio + ' / stock: ' + (isNaN(parseFloat(item.Qt_Producto)) ? 0 : parseFloat(item.Qt_Producto) ) + '</div>';
    },
    onSelect: function(e, term, item){
      $( '#txt-Nu_Codigo_Barra' ).val(item.data('codigo'));
      $( '#txt-Qt_Producto' ).val(item.data('qt_producto'));
      if ( $( '#txt-Ss_Precio' ).val() !== undefined )
        $( '#txt-Ss_Precio' ).val(item.data('precio'));
      if ( $( '#txt-ID_Impuesto_Cruce_Documento' ).val() !== undefined )
        $( '#txt-ID_Impuesto_Cruce_Documento' ).val(item.data('id_impuesto_cruce_documento'));
      if ( $( '#txt-Nu_Tipo_Impuesto' ).val() !== undefined )
        $( '#txt-Nu_Tipo_Impuesto' ).val(item.data('nu_tipo_impuesto'));
      if ( $( '#txt-Ss_Impuesto' ).val() !== undefined )
        $( '#txt-Ss_Impuesto' ).val(item.data('ss_impuesto'));
      if ( $( '#txt-nu_tipo_item' ).val() !== undefined )
        $( '#txt-nu_tipo_item' ).val(item.data('nu_tipo_item'));
      if ( $( '#txt-id_item' ).val() !== undefined )
        $( '#txt-id_item' ).val(item.data('id'));
      if ( $( '#txt-item' ).val() !== undefined )
        $( '#txt-item' ).val(item.data('nombre'));
    }
  });

  //Global Autocomplete Contacto
  $( '.autocompletar_contacto' ).autoComplete({
    minChars: 0,
    source: function(term, response){
      var filter_tipo_asiento = '';
      if ($( '#txt-ID_Tipo_Asiento' ).val() !== undefined)
        filter_tipo_asiento = $( '#txt-ID_Tipo_Asiento' ).val();

      $.post( base_url + 'AutocompleteController/getAllContact', { global_search : term.toLowerCase(), filter_tipo_asiento : filter_tipo_asiento }, function( arrData ){
        if (arrData.length === 0){
          if ( $( '#txt-Nu_Documento_Identidad_existe' ).val() !== undefined )
            $( '#txt-Nu_Documento_Identidad_existe' ).val( '' );
          if ( $( '#txt-No_Contacto_existe' ).val() !== undefined )
            $( '#txt-No_Contacto_existe' ).val( '' );
          if ( $( '#txt-Txt_Email_Contacto_existe' ).val() !== undefined )
            $( '#txt-Txt_Email_Contacto_existe' ).val( '' );
          if ( $( '#txt-Nu_Telefono_Contacto_existe' ).val() !== undefined )
            $( '#txt-Nu_Telefono_Contacto_existe' ).val( '' );
          if ( $( '#txt-Nu_Celular_Contacto_existe' ).val() !== undefined )
            $( '#txt-Nu_Celular_Contacto_existe' ).val( '' );
        }
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      return '<div class="autocomplete-suggestion" data-id="'+item.ID+'" data-tipo="'+item.ID_Tipo_Documento_Identidad+'" data-codigo="'+item.Nu_Documento_Identidad+'" data-nombre="'+item.No_Contacto+'" data-correo="'+item.Txt_Email_Contacto+'" data-telefono="'+item.Nu_Telefono_Contacto+'" data-celular="'+item.Nu_Celular_Contacto+'" data-val="'+search+'">'+item.No_Contacto.replace(re, "<b>$1</b>")+'</div>';
    },
    onSelect: function(e, term, item){
      $("div.id_tipo_documento_identidad select").val(item.data('tipo'));
      $( '#txt-Nu_Documento_Identidad_existe' ).val(item.data('codigo'));
      $( '#txt-AID_Contacto' ).val(item.data('id'));
      $( '#txt-No_Contacto_existe' ).val(item.data('nombre'));
      $( '#txt-Txt_Email_Contacto_existe' ).val(item.data('correo'));
      $( '#txt-Nu_Telefono_Contacto_existe' ).val(item.data('telefono'));
      $( '#txt-Nu_Celular_Contacto_existe' ).val(item.data('celular'));
      if ( $( '#txt-Filtro_Contacto' ).val() !== undefined )
        $( '#txt-Filtro_Contacto' ).val(item.data('nombre'));
    }
  });
  
  //Global Autocomplete Orden
  $( '.autocompletar_orden' ).autoComplete({
    minChars: 0,
    source: function(term, response){
      $.post( base_url + 'AutocompleteController/getAllOrden', { global_search : term.toLowerCase() }, function( arrData ){
        if (arrData.length === 0){
          if ( $( '#txt-No_Contacto_existe' ).val() !== undefined )
            $( '#txt-No_Contacto_existe' ).val( '' );
        }
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search){
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      return '<div class="autocomplete-suggestion" data-id_orden="'+item.ID_Documento_Cabecera+'" data-tipo="'+item.ID_Tipo_Documento_Identidad+'" data-codigo="'+item.Nu_Documento_Identidad+'" data-nombre="'+item.No_Contacto+'" data-correo="'+item.Txt_Email_Contacto+'" data-telefono="'+item.Nu_Telefono_Contacto+'" data-celular="'+item.Nu_Celular_Contacto+'" data-val="'+search+'">'+item.ID_Documento_Cabecera.replace(re, "<b>$1</b>")+'</div>';
    },
    onSelect: function(e, term, item){
      $( '#txt-ID_Documento_Cabecera' ).val(item.data('id_orden'));
      $( '#txt-No_Contacto_existe' ).val(item.data('nombre'));
    }
  });

  //Global Autocomplete Placa x Cliente
  $('.autocompletar_placa_x_cliente').autoComplete({
    minChars: 0,
    source: function (term, response) {
      var sNumeroDocumentoIdentidad = '';
      if ($('#txt-AID').val() !== undefined)
        sNumeroDocumentoIdentidad = $('#txt-AID').val();

      var send_post = {
        global_search: term.toLowerCase(),
        sNumeroDocumentoIdentidad: sNumeroDocumentoIdentidad,
      }

      $.post(base_url + 'AutocompleteController/getPlacaxCliente', send_post, function (arrData) {
        if (arrData.length === 0) {
          if ($('#txt-No_Placa_Vehiculo_existe').val() !== undefined)
            $('#txt-No_Placa_Vehiculo_existe').val('');
        }
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search) {
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      return '<div class="autocomplete-suggestion" data-id_producto="' + item.ID_Producto + '" data-placa="' + item.No_Placa_Vehiculo + '" data-marca="' + item.No_Marca_Vehiculo + '" data-modelo="' + item.No_Modelo_Vehiculo + '" data-color="' + item.No_Color_Vehiculo + '" data-val="' + search + '">' + item.No_Placa_Vehiculo.replace(re, "<b>$1</b>") + '</div>';
    },
    onSelect: function (e, term, item) {
      $('#txt-ID_Placa').val(item.data('id_producto'));
      $('#txt-No_Placa_Vehiculo_existe').val(item.data('placa'));
      $('#txt-marca_existe').val(item.data('marca'));
      $('#txt-modelo_existe').val(item.data('modelo'));
      $('#txt-color_existe').val(item.data('color'));
    }
  });

  //Global Autocomplete Orden de Ingreso
  $('.autocompletar_oi').autoComplete({
    minChars: 0,
    source: function (term, response) {
      $.post(base_url + 'AutocompleteController/getAllOI', { global_search: term.toLowerCase() }, function (arrData) {
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search) {
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      return '<div class="autocomplete-suggestion" data-placa="'+item.No_Placa_Vehiculo+'" data-modelo="'+item.No_Modelo_Vehiculo+'" data-marca="'+item.No_Marca_Vehiculo+'" data-id="' + item.ID_Orden_Ingreso + '" data-val="' + search + '">' + item.ID_Orden_Ingreso.replace(re, "<b>$1</b>") + '</div>';
    },
    onSelect: function (e, term, item) {
      $('#txt-ID_Orden_Ingreso_Existe').val(item.data('id'));
      $('#CONTENT_Orden_Ingreso_Existe').html(`
        <p><strong>Marca</strong>: ${item.data('marca')}<p>
        <p><strong>Modelo</strong>: ${item.data('modelo')}<p>
        <p><strong>Placa</strong>: ${item.data('placa')}<p>
      `);
    }
  });


  //Global Autocomplete presupuesto
  $('.autocompletar_presupuesto').autoComplete({
    minChars: 3,
    source: function (term, response) {
      $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val('');
      $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val('');
      $.post(base_url + 'AutocompleteController/getAllPrespuesto', { global_search: term.toLowerCase() }, function (arrData) {
        console.log(arrData);
        if (arrData.length === 0) {
          if ($('#txt-ID_Presupesto_Existe').val() !== undefined)
            $('#txt-ID_Presupesto_Existe').val('');
          if ($('#txt-ID_Presupesto_Existe-modal').val() !== undefined)
            $('#txt-ID_Presupesto_Existe-modal').val('');
          if ($('#txt-AID').val() !== undefined)
            $('#txt-AID').val('');
          if ($('#txt-ANombre').val() !== undefined)
            $('#txt-ANombre').val('');
          if ($('#txt-ACodigo').val() !== undefined)
            $('#txt-ACodigo').val('');
          if ($('#txt-Txt_Direccion_Entidad').val() !== undefined)
            $('#txt-Txt_Direccion_Entidad').val('');

          if ($('#txt-ID_Placa').val() !== undefined)
            $('#txt-ID_Placa').val('');
          if ($('#txt-No_Placa_Vehiculo_existe').val() !== undefined)
            $('#txt-No_Placa_Vehiculo_existe').val('');
          if ($('#txt-marca_existe').val() !== undefined)
            $('#txt-marca_existe').val('');
          if ($('#txt-modelo_existe').val() !== undefined)
            $('#txt-modelo_existe').val('');
          if ($('#txt-color_existe').val() !== undefined)
            $('#txt-color_existe').val('');
          if ($('#txt-modal-precioItem').val() !== undefined)
            $('#txt-modal-precioItem').val('');
          if ($('[name="textarea-modal-nombreItem"]').val() !== undefined)
            $('[name="textarea-modal-nombreItem"]').val('');
          if ($('[name="Txt_Glosa"]').val() !== undefined)
            $('[name="Txt_Glosa"]').val('');
          if ($('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val() !== undefined)
            $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val('');
          if ($('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val() !== undefined)
            $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val('');
        }
        response(arrData);
      }, 'JSON');
    },
    renderItem: function (item, search) {
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      return '<div class="autocomplete-suggestion" id="autocompletar_presupuesto" data-id_producto="'+item.ID_Producto+'" data-id_empresa="'+item.ID_Empresa+'" data-id_orden="' + item.ID_Documento_Cabecera + '" data-tipo="' + item.No_Tipo_Documento + '" data-serie="' + item.ID_Serie_Documento + '" data-numero="' + item.ID_Numero_Documento + '" data-ss_total="' + item.Ss_Total + '" data-id_cliente="' + item.ID_Entidad + '" data-no_cliente="' + item.No_Entidad + '" data-no_numero_documento_identidad_cliente="' + item.Nu_Documento_Identidad + '" data-txt_direccion_cliente="' + item.Txt_Direccion_Entidad + '" data-id_vehiculo="' + item.ID_Vehiculo + '" data-no_placa_vehiculo="' + item.No_Placa_Vehiculo + '" data-no_marca_vehiculo="' + item.No_Marca_Vehiculo + '" data-no_modelo_vehiculo="' + item.No_Modelo_Vehiculo + '" data-no_color_vehiculo="' + item.No_Color_Vehiculo + '" data-nu_kilometraje_vehiculo="' + item.Nu_Kilometraje + '" data-no_kilometraje_vehiculo="' + item.No_Kilometraje + '" data-val="' + search + '">' + item.ID_Documento_Cabecera.replace(re, "<b>$1</b>") + '</div>';
    },
    onSelect: function (e, term, item) {
      console.log(item);
      $('#txt-ID_Presupesto_Existe').val(item.data('id_orden'));
      //cliente
      $('#txt-AID').val(item.data('id_cliente'));
      $('#txt-ANombre').val(item.data('no_cliente'));
      $('#txt-ACodigo').val(item.data('no_numero_documento_identidad_cliente'));
      $('#txt-Txt_Direccion_Entidad').val(item.data('txt_direccion_cliente'));
      //placa
      $('#txt-ID_Placa').val(item.data('id_vehiculo'));
      $('#txt-No_Placa_Vehiculo_existe').val(item.data('no_placa_vehiculo'));
      $('#txt-marca_existe').val(item.data('no_marca_vehiculo'));
      $('#txt-modelo_existe').val(item.data('no_modelo_vehiculo'));
      $('#txt-color_existe').val(item.data('no_color_vehiculo'));
      //datos para facturar
      if ($('#txt-ID_Presupesto_Existe-modal').val() !== undefined)
        $('#txt-ID_Presupesto_Existe-modal').val(item.data('id_orden'));
      if ($('#txt-modal-precioItem').val() !== undefined)
        $('#txt-modal-precioItem').val(item.data('ss_total'));
      if ($('[name="textarea-modal-nombreItem"]').val() !== undefined)
        $('[name="textarea-modal-nombreItem"]').val('SERVICIO DE MANTENIMIENTO CORRECTIVO MARCA: ' + item.data('no_marca_vehiculo') + ' MODELO: ' + item.data('no_modelo_vehiculo') + ' PLACA: ' + item.data('no_placa_vehiculo') + ' KM: ' + item.data('no_kilometraje_vehiculo'));

      $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val('');
      $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val('');

      if ($('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val() !== undefined) {
          var sGlosa = $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val();
          sGlosa += item.data('id_orden') + ',';
          $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val(sGlosa);
      }

      if ($('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val() !== undefined) {
        var sGlosa = $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val();
        sGlosa += item.data('tipo') + ' - ' + item.data('serie') + ' - ' + item.data('numero') + ', \n';
        $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val(sGlosa);
      }


      if ($('#txt-modal-upcItem').val() !== undefined) {
       // C-01 - INICIO 
       var random_num = Math.floor(Math.random() * (1000-2+1)+2 );
       //$('#txt-modal-upcItem').val(item.data('id_orden')); 
       $('#txt-modal-upcItem').val(item.data('id_orden')+'A'+random_num  ); 
       // C-01- FIN 
      }
    }
  });

  if (fMonth < 10) {
    fMonth = '0' + fMonth;
  }
  $( '.date-picker-report' ).val('01/' + fMonth + '/' + fYear);

	//Cargar token dni y ruc, fecha de inicio de sistema
	$.post( base_url + 'HelperController/getToken', function( response ){
	  sTokenGlobal = response.Txt_Token;
		fInicioSistema = response.Fe_Inicio_Sistema;
    iIdTipoRubroEmpresaGlobal = response.Nu_Tipo_Rubro_Empresa;
    iValidarStockGlobal = response.Nu_Validar_Stock;
    iMostrarLogoTicketGlobal = response.Nu_Logo_Empresa_Ticket;
	  iFormatoTicketLiquidacionCajaGlobal = response.Nu_Imprimir_Liquidacion_Caja;
	  iHeightLogoTicketGlobal = response.Nu_Height_Logo_Ticket;
    iWidthLogoTicketGlobal = response.Nu_Width_Logo_Ticket;
    iVerificarAutorizacionVentaGlobal = response.Nu_Verificar_Autorizacion_Venta;
    sTerminosCondicionesTicket = response.Txt_Terminos_Condiciones_Ticket;
    iActivarDescuentoPuntoVenta = response.Nu_Activar_Descuento_Punto_Venta;
    
    //Date picker report
    $( '.date-picker-report' ).inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    $( '.date-picker-report' ).datepicker({
      autoclose       : true,
      startDate : new Date(fInicioSistema),
      endDate         : new Date(fYear, fMonth, fDay),
      todayHighlight  : true
    });

    $( '.date-picker-report' ).datepicker({}).on('changeDate', function (selected) {
      var minDate = new Date(selected.date.valueOf());
      $( '.txt-Filtro_Fe_Fin' ).datepicker('setStartDate', minDate);
    });

    $( '.date-picker-invoice' ).datepicker({
  		autoclose : true,
      startDate : new Date(fInicioSistema),
      endDate   : new Date(fYear, fToday.getMonth(), fDay),
  		todayHighlight  : true
    });
    
    //Date picker invoice
    $( '.input-datepicker-today-to-more' ).datepicker({
  		autoclose : true,
      startDate : new Date(fYear, fToday.getMonth(), fDay),
  		todayHighlight  : true
    });
	}, 'JSON');
  // /. Cargar token dni y ruc, fecha de inicio de sistema
})// /. document ready

function scrollToError( $sMetodo, $IdElemento ){
  $sMetodo.animate({
    scrollTop: $IdElemento.offset().top
  }, 'slow');
}


function validateNumber(){
  $( '.input-number' ).unbind();
  $( '.input-number' ).on('input', function () {
    this.value = this.value.replace(/[^0-9]/g,'');
  });
}

function validateDecimal(){
  $( '.input-decimal' ).unbind();
  $( '.input-decimal' ).on('input', function () {
    numero = parseFloat(this.value);
    if(!isNaN(numero)){
      this.value = this.value.replace(/[^0-9\.]/g,'');
      if (numero < 0)
        this.value = '';
    } else
      this.value = this.value.replace(/[^0-9\.]/g,'');
  });
}

function validateNumberOperation(){
  $( '.input-number_operacion' ).unbind();
  $( '.input-number_operacion' ).on('input', function () {
    numero = parseFloat(this.value);
    if(!isNaN(numero)){
      this.value = this.value.replace(/[^0-9]/g,'');
      if (numero < 0)
        this.value = '';
    } else
      this.value = this.value.replace(/[^0-9]/g,'');
  });
}

function validateCodigoBarra(){
  $( '.input-codigo_barra' ).on('input', function () {
    this.value = this.value.replace(/[^a-zA-Z0-9]/g,'');
  });
}

function ParseDate(fecha){
  var _FE = fecha.split('-');
  return _FE[2] + '/' + _FE[1] + '/' + _FE[0];
}

function ParseDateHour(fecha){
  var _FE = fecha.split('-');
  var _FEH = _FE[2].split(' ');
  return _FEH[0] + '/' + _FE[1] + '/' + _FE[0] + ' ' + _FEH[1];
}

function ParseDateString(fecha, tipo, caracter){
  if (tipo == 1) {// Caracter -> /
    var _FE = fecha.split(caracter);
    return _FE[2] + '-' + _FE[1] + '-' + _FE[0];
  } else if (tipo == 2) {// Caracter -> -
    var _FE = fecha.split(caracter);
    return _FE[2] + '/' + _FE[1] + '/' + _FE[0];
  } else if (tipo == 3) {// Caracter -> - y fecha y hora BD pero solo obtiene fecha
    var _FE = fecha.split(caracter);
    var _FEH = _FE[2].split(' ');
    return _FEH[0] + '/' + _FE[1] + '/' + _FE[0];
  } else if (tipo == 4) {// Caracter -> - y fecha y hora BD pero solo obtiene hora
    var _FE = fecha.split(caracter);
    var _FEH = _FE[2].split(' ');
    var _H = _FEH[1].split(':');
    return _H[0];
  } else if (tipo == 5) {// Caracter -> - y fecha y hora BD pero solo obtiene minuto
    var _FE = fecha.split(caracter);
    var _FEH = _FE[2].split(' ');
    var _M = _FEH[1].split(':');
    return _M[1];
  } else if (tipo == 6) {// Caracter -> (-) y formato de fecha BD (YYY-MM-DD)
    var _FE = fecha.split('-');
    return _FE[2] + '/' + _FE[1] + '/' + _FE[0];
  }
}

function number_format(amount, decimals) {
  amount += ''; // por si pasan un numero en vez de un string
  amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

  decimals = decimals || 0; // por si la variable no fue fue pasada

  // si no es un numero o es igual a cero retorno el mismo cero
  if (isNaN(amount) || amount === 0) 
    return parseFloat(0).toFixed(decimals);

  // si es mayor o menor que cero retorno el valor formateado como numero
  amount = '' + amount.toFixed(decimals);

  var amount_parts = amount.split('.'), regexp = /(\d+)(\d{3})/;

  while (regexp.test(amount_parts[0]))
    amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

  return amount_parts.join('.');
}

function existeUrl(url) {
  var http = new XMLHttpRequest();
  http.open('HEAD', url, false);
  http.send();
  return http.status!=404;
}

function formatoImpresionTicket(Accion, ID_Documento_Cabecera, url_print){
  if ( Accion != 'imprimir' ) {
    $( '.modal_ticket' ).modal('show');
    $( '#modal-loader' ).modal('show');
  }
  
  url = base_url + 'ImprimirTicketController/formatoImpresionTicket';
  $.post( url, {ID_Documento_Cabecera : ID_Documento_Cabecera}, function( response ) {    
    $( '#table-modal_ticket thead' ).empty();
	  $( '#table-modal_ticket tbody' ).empty();
    $( '#table-modal_ticket tfoot' ).empty();
	  
    var iCantidadRegistros = response['arrTicket'].length;
	  var sPorecentajeDescuento="0.00";
	  var fImpuesto=0.00;
    for (var i = 0; i < iCantidadRegistros; i++) {
      if (response['arrTicket'][i].Nu_Tipo_Impuesto == "1") {
        sPorecentajeDescuento = response['arrTicket'][i].Po_Impuesto;
        fImpuesto = response['arrTicket'][i].Ss_Impuesto_Producto;
      }
    }
    
    var sDocumentoElectronico = response['arrTicket'][0].No_Tipo_Documento.toUpperCase() + ( ( response['arrTicket'][0].ID_Tipo_Documento!="2" || response['arrTicket'][0].Nu_Tipo_Proveedor_FE == 3 ) ? " ELECTRÓNICA" : "");

    var sTdOriginal3 = '4';
    var sTdOriginal2 = '3';
    if (iIdTipoRubroEmpresaGlobal == 2 ) {//2=Tienda a Granel
      var sTdOriginal3 = '5';
      var sTdOriginal2 = '4';
    }

    $ ( '#img-logo_punto_venta' ).hide();
    $ ( '#img-logo_punto_venta_click' ).hide();
    
    if ( iMostrarLogoTicketGlobal == 1 ) {
      $ ( '#img-logo_punto_venta' ).show();
    }

    if ( response['arrTicket'][0].ID_Tipo_Documento == '2' && iIdTipoRubroEmpresaGlobal == 3 ){//3 = Lavanderia
      sDocumentoElectronico = 'ORDEN LAVADO';
    }

    var p_title = '';
    p_title += 
    '<strong>' + response['arrTicket'][0].No_Empresa + '</strong><br>'
    + response['arrTicket'][0].Txt_Direccion_Empresa + '<br>';

    if ( response['arrTicket'][0].Txt_Direccion_Empresa != response['arrTicket'][0].Txt_Direccion_Almacen ) {
      p_title += response['arrTicket'][0].Txt_Direccion_Almacen + '<br>';
    }

    if ( response['arrTicket'][0].Nu_Celular_Empresa != '' && response['arrTicket'][0].Nu_Telefono_Empresa != '' ) {
      p_title += response['arrTicket'][0].Nu_Celular_Empresa + ' - ' + response['arrTicket'][0].Nu_Telefono_Empresa + '<br>';
    } else if ( response['arrTicket'][0].Nu_Celular_Empresa != '' && response['arrTicket'][0].Nu_Telefono_Empresa.length == 0 ) {
      p_title += response['arrTicket'][0].Nu_Celular_Empresa + '<br>';
    } else {
      p_title += response['arrTicket'][0].Nu_Telefono_Empresa + '<br>';
    }

    if ( response['arrTicket'][0].Txt_Email_Empresa != '' ) {
      p_title += response['arrTicket'][0].Txt_Email_Empresa + '<br>';
    }

    p_title += 
    '<br><strong> RUC: ' + response['arrTicket'][0].Nu_Documento_Identidad_Empresa + '</strong><br>'
    + '<strong>' + sDocumentoElectronico + '</strong><br>'
    + '<strong>' + response['arrTicket'][0].ID_Serie_Documento + '-' + ('000000' + response['arrTicket'][0].ID_Numero_Documento).slice(-6) + '</strong><br>'
    + '<br>';
    $( '#modal-body-p-title' ).html(p_title);

    var sCssFontSize = 'font-size: 14px; font-family: Courier New;';
    var iFontSize = '14px;';
    if ( iIdTipoRubroEmpresaGlobal != 2 ) {
      sCssFontSize = 'font-size: 11.5px; font-family: Courier New;';
      iFontSize = '11.5px;';
    }

    var table_ticket = '';
    table_ticket += 
    "<thead>"
      +"<tr>"
        +"<td class='text-left' style='" + sCssFontSize + "' colspan='" + sTdOriginal2 + "'></td>"
      +"</tr>"
      +"<tr>"
        +"<td class='text-left' style='" + sCssFontSize + "'>" + response['arrTicket'][0].No_Tipo_Documento_Identidad_Breve + "</td>"
        +"<td class='text-left' style='" + sCssFontSize + "' colspan='" + sTdOriginal2 + "'>" + response['arrTicket'][0].Nu_Documento_Identidad + "</td>"
      +"</tr>"
      +"<tr>"
        +"<td class='text-left' style='" + sCssFontSize + "'>CLIENTE: </td>"
        +"<td class='text-left' style='" + sCssFontSize + "' colspan='" + sTdOriginal2 + "'>" + response['arrTicket'][0].No_Entidad + "</td>"
      +"</tr>"
      +"<tr>"
        +"<td class='text-left' style='" + sCssFontSize + "'>F. EMISIÓN: </td>"
        +"<td class='text-left' style='" + sCssFontSize + "' colspan='" + sTdOriginal2 + "'>" + ParseDateHour(response['arrTicket'][0].Fe_Emision_Hora) + "</td>"
      +"</tr>";

    if (iIdTipoRubroEmpresaGlobal == 3) {//3=Lavandería
      table_ticket += 
      "<tr>"
        +"<td class='text-left' style='" + sCssFontSize + "'>F. ENTREGA: </td>"
        +"<td class='text-left' style='" + sCssFontSize + "' colspan='" + sTdOriginal2 + "'>" + ParseDate(response['arrTicket'][0].Fe_Entrega) + "</td>"
      +"</tr>";
    }

      table_ticket +=
      +"<tr>"
        +"<td class='text-left' style='" + sCssFontSize + "'>CAJERO: </td>"
        +"<td class='text-left' style='" + sCssFontSize + "' colspan='" + sTdOriginal2 + "'>" + response['arrTicket'][0].No_Empleado + "</td>"
      +"</tr>"
    "</thead>";
    
    table_ticket +=
    "<tbody>"
      +"<tr>"
        +"<td colspan='" + sTdOriginal3 + "' class='text-left' style='font-size:" + iFontSize + "; font-family: Courier New; width: 20%; padding: 2px;'>DESCRIPCION</td>"
      +"</tr>"
      +"<tr>"
        +"<td class='text-left' style='font-size:" + iFontSize + "; font-family: Courier New; width: 3%; padding: 2px; border-top: 1px solid transparent !important; border-bottom: 1px solid black !important;'>CANT.</td>";
        if (iIdTipoRubroEmpresaGlobal == 2) {//2=Tienda a granel
          table_ticket += "<td class='text-left' style='font-size:" + iFontSize + "; font-family: Courier New; width: 3%; padding: 2px; border-top: 1px solid transparent !important; border-bottom: 1px solid black !important;'>CO2</td>";
        }
      table_ticket +=
        "<td class='text-left' style='font-size:" + iFontSize + "; font-family: Courier New; width: 3%; padding: 2px; border-top: 1px solid transparent !important; border-bottom: 1px solid black !important;'>DSCTO.</td>"
        +"<td class='text-center' style='font-size:" + iFontSize + "; font-family: Courier New; width: 5%; padding: 2px; border-top: 1px solid transparent !important; border-bottom: 1px solid black !important;'>PRECIO</td>"
        +"<td class='text-right' style='font-size:" + iFontSize + "; font-family: Courier New; width: 13%; padding: 2px; border-top: 1px solid transparent !important; border-bottom: 1px solid black !important;'>TOTAL</td>"
      +"</tr>";
    
    var sNombreBreveLaboratorio = '';
    var fCantidadItem = 0.00;
    var Ss_Total_Producto = 0.00;
    var fOperacionesGravadas = 0.00;
    var fOperacionesExoneradas = 0.00;
    var fOperacionesInafectas = 0.00;

    var fTotalCO2 = 0.00;
    var fTotalGeneralCO2=0.00;
    var fTotalIcbper = 0.00;

    for (var i = 0; i < iCantidadRegistros; i++) {
      sNombreBreveLaboratorio='';
      if (iIdTipoRubroEmpresaGlobal == 1 && response['arrTicket'][i].No_Laboratorio_Breve !== '' && response['arrTicket'][i].No_Laboratorio_Breve !== null)
        sNombreBreveLaboratorio= ' ' + response['arrTicket'][i].No_Laboratorio_Breve;

      Ss_Total_Producto = parseFloat(response['arrTicket'][i].Ss_Total_Producto);

      if (response['arrTicket'][i].Nu_Tipo_Impuesto == "1")//IGV
        fOperacionesGravadas += Ss_Total_Producto;
        
      if (response['arrTicket'][i].Nu_Tipo_Impuesto == "2")//INA
        fOperacionesInafectas += Ss_Total_Producto;
        
      if (response['arrTicket'][i].Nu_Tipo_Impuesto == "3")//EXO
        fOperacionesExoneradas += Ss_Total_Producto;
        
      if (response['arrTicket'][i].ID_Impuesto_Icbper == "1")
        fTotalIcbper += parseFloat(response['arrTicket'][i].Ss_Total_Producto);
        
      fCantidadItem = parseFloat(response['arrTicket'][i].Qt_Producto);

      table_ticket +=
      "<tr>"
        +"<td style='" + sCssFontSize + "' colspan='" + sTdOriginal3 + "' class='text-left' style='padding: 0px;'>" + response['arrTicket'][i].No_Producto + sNombreBreveLaboratorio + ( response['arrTicket'][i].Txt_Nota_Item != null ? ' ' + response['arrTicket'][i].Txt_Nota_Item : '' ) + "</td>"
      +"</tr>"
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' style='padding: 0px; border-top: 1px solid transparent;'>" + fCantidadItem + " " + response['arrTicket'][i].nu_codigo_unidad_medida_sunat + " </td>";
        if (iIdTipoRubroEmpresaGlobal == 2) {
          fTotalCO2 = fCantidadItem * parseFloat(response['arrTicket'][i].Qt_CO2_Producto);
          table_ticket += "<td class='text-left' style='padding: 0px; border-top: 1px solid transparent;'>" + fTotalCO2 + "</td>";
          fTotalGeneralCO2 += fTotalCO2;
        }
      table_ticket +=
        "<td style='" + sCssFontSize + "' class='text-right' style='padding: 0px; border-top: 1px solid transparent;'>" + (parseFloat(response['arrTicket'][i].Ss_Descuento_Producto) > 0.00 ? response['arrTicket'][i].Ss_Descuento_Producto : '') + "</td>"
        +"<td style='" + sCssFontSize + "' class='text-right' style='padding: 0px; border-top: 1px solid transparent;'>" + number_format(response['arrTicket'][i].ss_precio_unitario, 3, '.') + "</td>"
        +"<td style='" + sCssFontSize + "' class='text-right' style='padding: 0px; border-top: 1px solid transparent;'>" + number_format(Ss_Total_Producto, 2, '.', ',') + "</td>"
      +"</tr>";
    }
    
    var fIGV = 0.00;
    if ( fImpuesto > 0.00) {
      fOperacionesGravadas = Math.round10(parseFloat(fOperacionesGravadas) / parseFloat(fImpuesto), -2);
      fIGV = parseFloat(response['arrTicket'][0].Ss_Total) - parseFloat(fOperacionesGravadas);
    }
    
    table_ticket +=
    "</tbody>"
    +"<tfoot>";
    if ( response['arrTicket'][0].ID_Tipo_Documento!='2' && fOperacionesGravadas > 0.00 ) {
    table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>OP. GRAVADAS</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format(fOperacionesGravadas, 2, '.', ',') + "</td>"
      +"</tr>";
    }

    if ( response['arrTicket'][0].ID_Tipo_Documento!='2' && fOperacionesInafectas > 0.00 ) {
      table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>OP. INAFECTAS</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format(fOperacionesInafectas, 2, '.', ',') + "</td>"
      +"</tr>";
    }

    if ( response['arrTicket'][0].ID_Tipo_Documento!='2' && fOperacionesExoneradas > 0.00 ) {
      table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>OP. EXONERADAS</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format(fOperacionesExoneradas, 2, '.', ',') + "</td>"
      +"</tr>";
    }

    if ( response['arrTicket'][0].ID_Tipo_Documento!='2' && fTotalIcbper > 0.00 ) {
    table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>ICBPER</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format(fTotalIcbper, 2, '.', ',') + "</td>"
      +"</tr>";
    }

    if ( response['arrTicket'][0].ID_Tipo_Documento!='2' && fOperacionesGravadas > 0.00 ) {
    table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>IGV " + (fImpuesto > 0.00 ? sPorecentajeDescuento + '%' : '') + "</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format(fIGV, 2, '.', ',') + "</td>"
      +"</tr>";
    }

    table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>TOTAL</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format( parseFloat(response['arrTicket'][0].Ss_Total) + fTotalIcbper, 2, '.', ',') + "</td>"
      +"</tr>";
    
    if ( parseFloat(response['arrTicket'][0].Ss_Total_Saldo) > 0.00 ) {  
      table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>A CUENTA</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format( (parseFloat(response['arrTicket'][0].Ss_Total) + fTotalIcbper) - parseFloat(response['arrTicket'][0].Ss_Total_Saldo), 2, '.', ',') + "</td>"
      +"</tr>"
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>SALDO</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + response['arrTicket'][0].No_Signo + " " + number_format( parseFloat(response['arrTicket'][0].Ss_Total_Saldo), 2, '.', ',') + "</td>"
      +"</tr>";
    }

    if (iIdTipoRubroEmpresaGlobal == 2) {//2=Tienda a granel
      table_ticket += 
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal2 + "'>TOTAL CO2</td>"
        +"<td style='" + sCssFontSize + "' class='text-right'>" + number_format(fTotalGeneralCO2, 2, '.', ',') + " <i class='fa fa-tree' aria-hidden='true'></i></td>"
      +"</tr>";
    }

    table_ticket +=
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal3 + "'>F. PAGO: " + response['arrTicket'][0].No_Medio_Pago + "</td>"
      +"</tr>"
      +"<tr>"
        +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal3 + "'>SON: " + response['totalEnLetras'] + "</td>"
      +"</tr>";
     
      if ( response['arrTicket'][0].Txt_Glosa_Global != '' ) {
        table_ticket +=
          +"<tr>"
            +"<td style='" + sCssFontSize + "' class='text-left' colspan='" + sTdOriginal3 + "'>NOTA: " + response['arrTicket'][0].Txt_Glosa_Global + "</td>"
          +"</tr>";
      }

      if ( response['arrTicket'][0].Nu_Tipo_Proveedor_FE != 3 ) {
        if ( response['arrTicket'][0].ID_Tipo_Documento!='2' && response['arrTicket'][0].Nu_Tipo_Proveedor_FE == 1 ) {
          table_ticket +=
          "<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Representación impresa de la " + sDocumentoElectronico + ", para ver el documento visita <strong>https://laesystems.pse.pe/" + response['arrTicket'][0].Nu_Documento_Identidad_Empresa + "</strong></td>"
          +"</tr>"
          +"<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Emitido mediante un <strong>PROVEEDOR Autorizado por la SUNAT</strong> mediante Resolución de Intendencia No.<strong>034-005-0005315</strong></td>"
          +"</tr>";
          +"<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Resumen: " + response['arrTicket'][0].Txt_Hash + "</td>"
          +"</tr>"
          "<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>&nbsp;</td>"
          +"</tr>"
          +"<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Emitido desde laesystems.com</td>"
          +"</tr>"
        +"</tfoot>";
        } else if ( response['arrTicket'][0].ID_Tipo_Documento!='2' && response['arrTicket'][0].Nu_Tipo_Proveedor_FE == 2 ) {
          table_ticket +=
          "<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Representación impresa de la " + sDocumentoElectronico + ", para ver el documento visita <strong>https://laesystems.pse.pe/" + response['arrTicket'][0].Nu_Documento_Identidad_Empresa + "</strong></td>"
          +"</tr>"
          +"<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Resumen: " + response['arrTicket'][0].Txt_Hash + "</td>"
          +"</tr>"
          +"<tr>"
            +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Emitido desde laesystems.com</td>"
          +"</tr>";
          if ( response['arrTicket'][0].Nu_Estado_Sistema == 0 ) {
            table_ticket +=
            "<tr>"
              +"<td style='" + sCssFontSize + "' class='text-center' colspan='" + sTdOriginal3 + "'>Representación Impresa de Documento Electrónico Generado En Una Versión de Pruebas. No tiene Validez!</td>"
            +"</tr>";
          }
          table_ticket +=
        +"</tfoot>";
        } // if - else formato ticket
      }

      $( '#table-modal_ticket' ).html(table_ticket);

      if ( response['arrTicket'][0].ID_Tipo_Documento=='2' && sTerminosCondicionesTicket != '' ) {
        var pTerminosCondicionesTicket = sTerminosCondicionesTicket;
        $( '#modal-body-p-terminos_condiciones_ticket' ).html(pTerminosCondicionesTicket);
      }

    $( '#div-codigo_qr' ).hide();
    $( '#div-codigo_qr' ).html('');
    if ( response['arrTicket'][0].ID_Tipo_Documento!="2" && response['arrTicket'][0].Txt_QR != null ) {
      var miCodigoQR = new QRCode("div-codigo_qr");
      miCodigoQR.makeCode(response['arrTicket'][0].Txt_QR);

      $( '#div-codigo_qr' ).show();
    }
   
    if ( Accion != 'imprimir' ) {
      $( '#modal-loader' ).modal('hide');
    }

    if (Accion == 'imprimir') {
      generarFormatoImpresion('div-ticket', '');
    }
  }, 'JSON')
  .fail(function() {
    $( '#modal-loader' ).modal('hide');
  });
}

function formatoImpresionLiquidacionCaja(arrPost){
  if ( arrPost.sAccion == undefined ) {
    arrPost = JSON.parse(arrPost);
  }

  if ( arrPost.sAccion != 'imprimir' ) {
    $( '.modal-liquidacion_caja' ).modal('show');

    $( '#modal-loader' ).modal('show');
  }

  url = base_url + 'ImprimirLiquidacionCajaController/formatoImpresionLiquidacionCaja';
  $.post( url, arrPost, function( response ) {
    $( '#modal-table-ventas_x_familia' ).empty();
    $( '#modal-table-movimientos_caja' ).empty();
    $( '#modal-table-ventas_generales' ).empty();
    $( '#modal-table-ventas_totales' ).empty();

    if ( response.sStatus == 'success' ) {
      var fTotal=0.00;

      var iCantidadRegistrosVentasxFamilia = response.arrData.VentasxFamilia.length;
      var arrDataVentasxFamilia = response.arrData.VentasxFamilia;
      var sHtmlTableVentasxFamilia = '';

      sHtmlTableVentasxFamilia +=
      '<thead>'+
        '<tr>'+
          '<td class="text-left">&nbsp;</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="text-left">F. Apertura: </td>'+
          '<td class="text-left" colspan="2">' + ParseDateHour(response.arrData.TotalesLiquidacionCaja[0].Fe_Apertura) + '</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="text-left">F. Cierre: </td>'+
          '<td class="text-left" colspan="2">' + ParseDateHour(response.arrData.TotalesLiquidacionCaja[0].Fe_Cierre) + '</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="text-left">Caja: </td>'+
          '<td class="text-left" colspan="2">' + response.arrData.TotalesLiquidacionCaja[0].ID_POS + '</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="text-left">Cajero: </td>'+
          '<td class="text-left" colspan="2">' + response.arrData.TotalesLiquidacionCaja[0].No_Entidad + '</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="text-left">&nbsp;</td>'+
        '</tr>'+
        '</thead>'+
        '<thead>'+
          '<tr>'+
            '<th class="text-center" colspan="4">Ventas por ' + (iFormatoTicketLiquidacionCajaGlobal == 1 ? 'Familia' : 'Producto') + '</th>'+
          '</tr>'+
          '<tr>'+
            '<th class="text-center">' + (iFormatoTicketLiquidacionCajaGlobal == 1 ? 'Categoría' : 'Producto' ) + '</th>'+
            '<th class="text-center">Cantidad</th>'+
            '<th class="text-center">M</th>'+
            '<th class="text-right">Total</th>'+
          '</tr>'+
        '</thead>';
      if ( iCantidadRegistrosVentasxFamilia>0 ) {
        fTotal=0.00;
        sHtmlTableVentasxFamilia +=
        '<tbody>';
        for (var i = 0; i < iCantidadRegistrosVentasxFamilia; i++) {//Ventas x Familia
          sHtmlTableVentasxFamilia +=
          '<tr>'+
            '<td class="text-left">' + arrDataVentasxFamilia[i].No_Familia_Item + '</td>'+
            '<td class="text-center">' + arrDataVentasxFamilia[i].Qt_Producto + '</td>'+
            '<td class="text-center">' + arrDataVentasxFamilia[i].No_Signo + '</td>'+
            '<td class="text-right">' + arrDataVentasxFamilia[i].Ss_Total + '</td>'+
          '</tr>';
          fTotal+=parseFloat(arrDataVentasxFamilia[i].Ss_Total);
        }// ./ for
        sHtmlTableVentasxFamilia +=
        '</tbody>'+
        '<tfoot>'+
          '<tr>'+
            '<th class="text-right" colspan="3">Total</th>'+
            '<th class="text-right">S/ ' + fTotal + '</th>'+
          '</tr>'+
        '</tfoot>';
      } else {
        sHtmlTableVentasxFamilia += 
        '<tr>'+
          '<td class="text-center" colspan="4">No hay registros</td>'+
        '</tr>';
      }// ./ if - else table ventas por familia

      $( '#modal-table-ventas_x_familia' ).append( sHtmlTableVentasxFamilia );

      var iCantidadRegistrosMovimientosCaja = response.arrData.MovimientosCaja.length;
      var arrDataMovimientosCaja = response.arrData.MovimientosCaja;
      var sHtmlTableMovimientosCaja = '';

      sHtmlTableMovimientosCaja +=
      '<table id="modal-table-movimientos_caja" class="table table-hover">'+
        '<thead>'+
          '<tr>'+
            '<th class="text-center" colspan="4">Movimientos de Caja</th>'+
          '</tr>'+
          '<tr>'+
            '<th class="text-center" colspan="2">Movimiento</th>'+
            '<th class="text-center">M</th>'+
            '<th class="text-right">Total 1</th>'+
          '</tr>'+
        '</thead>';        
      if ( iCantidadRegistrosMovimientosCaja>0 ) {
        fTotal=0.00;
        sHtmlTableMovimientosCaja +=
        '<tbody>';
        for (var i = 0; i < iCantidadRegistrosMovimientosCaja; i++) {//Movimientos de Caja
          fTotal+= (arrDataMovimientosCaja[i].Nu_Tipo != '6' ? parseFloat(arrDataMovimientosCaja[i].Ss_Total) : -parseFloat(arrDataMovimientosCaja[i].Ss_Total));
          sHtmlTableMovimientosCaja +=
          '<tr>'+
            '<td class="text-left" colspan="2">' + arrDataMovimientosCaja[i].No_Tipo_Operacion_Caja + '</td>'+
            '<td class="text-center">' + arrDataMovimientosCaja[i].No_Signo + '</td>'+
            '<td class="text-right">' + arrDataMovimientosCaja[i].Ss_Total + '(' + (arrDataMovimientosCaja[i].Nu_Tipo != '6' ? '+' : '-') + ')</td>'+
          '</tr>';
        }// ./ for
        sHtmlTableMovimientosCaja +=
        '</tbody>'+
        '<tfoot>'+
          '<tr>'+
            '<th class="text-right" colspan="3">Total</th>'+
            '<th class="text-right">S/ ' + fTotal + '</td>'+
          '</tr>'+
        '</tfoot>';
      } else {
        sHtmlTableMovimientosCaja += 
        '<tr>'+
          '<td class="text-center" colspan="4">No hay registros</td>'+
        '</tr>';
      }// ./ if - else table Movimientos de Caja

      sHtmlTableMovimientosCaja += '</table>';
      $( '#modal-table-movimientos_caja' ).append( sHtmlTableMovimientosCaja );

      var iCantidadRegistrosVentasGenerales = response.arrData.VentasGenerales.length;
      var arrDataVentasGenerales = response.arrData.VentasGenerales;
      var sHtmlTableVentasGenerales = '';

      sHtmlTableVentasGenerales +=
      '<table id="modal-table-ventas_generales" class="table table-hover">'+
        '<thead>'+
          '<tr>'+
            '<th class="text-center" colspan="4">Ventas Generales</th>'+
            '</tr>'+
            '<tr>'+
            '<th class="text-center" colspan="2">Tipo</th>'+
            '<th class="text-center">M</th>'+
            '<th class="text-right">Total</th>'+
          '</tr>'+
        '</thead>';
      if ( iCantidadRegistrosVentasGenerales>0 ) {
        fTotal=0.00;
        sHtmlTableVentasGenerales +=
        '<tbody>';
        for (var i = 0; i < iCantidadRegistrosVentasGenerales; i++) {//Ventas Generales
          sHtmlTableVentasGenerales +=
          '<tr>'+
            '<td class="text-left" colspan="2">' + arrDataVentasGenerales[i].No_Medio_Pago + '</td>'+
            '<td class="text-center">' + arrDataVentasGenerales[i].No_Signo + '</td>'+
            '<td class="text-right">' + arrDataVentasGenerales[i].Ss_Total + ' ' + (arrDataVentasGenerales[i].Nu_Tipo_Caja == '0' ? '(+)' : '') + '</td>'+
          '</tr>';
          fTotal+=parseFloat(arrDataVentasGenerales[i].Ss_Total);
        }// ./ for
        sHtmlTableVentasGenerales +=
        '</tbody>'+
        '<tfoot>'+
          '<tr>'+
            '<th class="text-right" colspan="3">Total</th>'+
            '<th class="text-right">S/ ' + fTotal + '</td>'+
          '</tr>'+
        '</tfoot>';
      } else {
        sHtmlTableVentasGenerales += 
        '<tr>'+
          '<td class="text-center" colspan="4">No hay registros</td>'+
        '</tr>';
      }// ./ if - else table Ventas Generales

      sHtmlTableVentasGenerales += '</table>';
      $( '#modal-table-ventas_generales' ).append( sHtmlTableVentasGenerales );

      var sHtmlTotales = '';
      // Totales a liquidar, depositar y diferencia
      var fDiferencia = (  parseFloat(response.arrData.TotalesLiquidacionCaja[0].Ss_Total) - parseFloat(response.arrData.TotalesLiquidacionCaja[0].Ss_Expectativa) );
      sHtmlTotales +=
      '<tbody>'+
        '<tr>'+
          '<td class="text-left">&nbsp;</td>'+
        '</tr>'+
        '<tr>'+
          '<th class="text-right" colspan="2">Total a Liquidar</th>'+
          '<th class="text-right" colspan="2">S/ ' + response.arrData.TotalesLiquidacionCaja[0].Ss_Expectativa + '</td>'+
        '</tr>'+
        '<tr>'+
          '<th class="text-right" colspan="2">Total a Depositado</th>'+
          '<th class="text-right" colspan="2">S/ ' + response.arrData.TotalesLiquidacionCaja[0].Ss_Total + '</td>'+
        '</tr>'+
        '<tr>'+
          '<th class="text-right" colspan="2">Diferencia</th>'+
          '<th class="text-right" colspan="2">S/ ' + fDiferencia + '</td>'+
        '</tr>'+
      '</tbody>';

      $( '#modal-table-ventas_totales' ).append( sHtmlTotales );

      // Totales a liquidar, depositar y diferencia
      var fDiferencia = (  parseFloat(response.arrData.TotalesLiquidacionCaja[0].Ss_Total) - parseFloat(response.arrData.TotalesLiquidacionCaja[0].Ss_Expectativa) );
      $( '#label-ss_liquidado' ).text( 'S/ ' + response.arrData.TotalesLiquidacionCaja[0].Ss_Expectativa );
      $( '#label-ss_depositado' ).text( 'S/ ' + response.arrData.TotalesLiquidacionCaja[0].Ss_Total );
      $( '#label-ss_diferencia' ).text( 'S/ ' + fDiferencia );
    } else {
      $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
      $( '#modal-message' ).modal('show');
      $( '.modal-message' ).addClass( 'modal-' + response.sStatus );
      $( '.modal-title-message' ).text( response.sMessage );
      setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
    }
    $( '#modal-loader' ).modal('hide');
  
    if (arrPost.sAccion == 'imprimir') {
      generarFormatoImpresion('modal-body-div-liquidacion_caja', arrPost);
    }
  }, 'JSON')
  .fail(function() {
    $( '#modal-loader' ).modal('hide');
  });
}

function generarFormatoImpresion(sIdFormatoImpresion, arrPost){
  winPrintSunat = window.open("", "MsgWindow", "top=80,left=800,width=550,height=550");
  winPrintSunat.document.open();
	printContentsSunat = document.getElementById(sIdFormatoImpresion).innerHTML;
  winPrintSunat.document.write(printContentsSunat);
	winPrintSunat.document.close();
	winPrintSunat.focus();
	winPrintSunat.print();
  winPrintSunat.close();
  
  if (arrPost.sUrlAperturaCaja !== undefined ){
    window.location = arrPost.sUrlAperturaCaja
  }
}

function AjaxPopupModal(id, title, url, params){
	$("#" + id).remove();
  $("body").append(
  '<div id="' + id + '" class="modal fade" role="dialog">'
    + '<div class="modal-dialog">' 
      + '<div class="modal-content">'
        + '<div class="modal-header">'
          + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close_new" aria-hidden="true">&times;</span></button>'
          + '<h4 class="panel-heading text-center"><b>' + title + '</b></h4>'
        + '</div>'
        + '<div class="modal-body">'
        + '</div>'
      + '</div>'
    + '</div>'
  + '</div>'
  );
  
  $("#" + id).modal();
  // Cargando
  $( "#modal-loader" ).modal( "show" );
  $.post(base_url + url, params, function(r){
    $("#" + id).find('.modal-body').html(r);
    $( "#modal-loader" ).modal( "hide" );
  });
}

function validateStockNow(event){
  //console.log(iValidarStockGlobal);
  //console.log(event);
  if ( iValidarStockGlobal == 1 ) {// 1 = Validar stock
    var iIdItem = event.target.dataset.id_item, fCantidadActualCliente = event.target.value, fCantidadActualEmpresa = 0.00;
    var arrParams = {
      iIdItem : iIdItem,
    };
    $.post( base_url + 'HelperController/validateStockNow', arrParams, function( response ){
      fCantidadActualCliente = parseFloat(fCantidadActualCliente);
      fCantidadActualEmpresa = parseFloat(response.Qt_Producto);
      if ( fCantidadActualCliente > fCantidadActualEmpresa ) {
        alert( 'La cantidad ingresada: ' + fCantidadActualCliente + ' es mayor al stock actual: ' + fCantidadActualEmpresa );
      }
    }, 'JSON')
  }
}

function validarNumeroCelular(celular, div){
  var caract = new RegExp(/^([0-9\s]{11})+$/);
  /*
  if (caract.test(celular) == false){
    $(div).hide().removeClass('hide').slideDown('fast');
    return false;
  } else{
    $(div).hide().addClass('hide').slideDown('slow');
    return true;
  }
  */
}

function caracteresCorreoValido(email, div){
  var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
  if (caract.test(email) == false){
    $(div).hide().removeClass('hide').slideDown('fast');
    return false;
  }else{
    $(div).hide().addClass('hide').slideDown('slow');
    return true;
  }
}

function sumaFecha(d, fecha){
  var Fecha = new Date();
  var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
  var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
  var aFecha = sFecha.split(sep);
  var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];
  fecha= new Date(fecha);
  fecha.setDate(fecha.getDate()+parseInt(d));
  var anno=fecha.getFullYear();
  var mes= fecha.getMonth()+1;
  var dia= fecha.getDate();
  mes = (mes < 10) ? ("0" + mes) : mes;
  dia = (dia < 10) ? ("0" + dia) : dia;
  var fechaFinal = dia+sep+mes+sep+anno;
  return (fechaFinal);
}

(function() {
  /**
   * Ajuste decimal de un número.
   *
   * @param {String}  tipo  El tipo de ajuste.
   * @param {Number}  valor El numero.
   * @param {Integer} exp   El exponente (el logaritmo 10 del ajuste base).
   * @returns {Number} El valor ajustado.
   */
  function decimalAdjust(type, value, exp) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }
})();
