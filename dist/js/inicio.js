jQuery(function($) {
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Hoy'           : [moment(), moment()],
        'Ayer'          : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Último 7 Días' : [moment().subtract(6, 'days'), moment()],
        'Último 30 Días': [moment().subtract(29, 'days'), moment()],
        'Mes Actual'    : [moment().startOf('month'), moment().endOf('month')],
        'Último Mes'    : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      //startDate: new Date(),//Hoy
      startDate : moment().startOf('month'),
      endDate  : moment().endOf('month'),
      "locale": {
        "format": "DD/MM/YYYY",
        "separator": " - ",
        "applyLabel": "Buscar",
        "cancelLabel": "Salir",
        "fromLabel": "Desde",
        "toLabel": "Hasta",
        "customRangeLabel": "Seleccionar rango fecha",
        "daysOfWeek": [
          "Do",
          "Lu",
          "Ma",
          "Mié",
          "Ju",
          "Vi",
          "Sá"
        ],
        "monthNames": [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Setiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
        ],
        "firstDay": 1
      }
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
      
      var arrReporteGrafico = {
        dInicial:start.format('YYYY-MM-DD'),
        dFinal:end.format('YYYY-MM-DD'),
        iIDMoneda: 1,//1=Soles
      };
      
      reporteGraficoInicio(arrReporteGrafico);  
    }
  );
  
  // Inicio / Escritorio para cargar modal con el cambio de version
	$( ".aVersionSistema" ).click(function(){
    $( '#modal-header-actualizacion_sistema' ).text( 'Nueva versión del sistema ' + $(this).data('numero_version_sistema') );
    $( '.modal-actualizacion_sistema' ).modal('show');
	})
	
	// Modal de change log de versiones
  $( '#btn-modal-actualizar_version_sistema' ).off('click').click(function () {
    enviarCorreoSoporteMigracionSistema();
  });
})

function reporteGraficoInicio(arrReporteGrafico){
  $( '#modal-loader' ).modal('show');
  $( "#div-inicio-reporte-grafico" ).load(base_url + 'InicioController/Ajax/Reporte',
    {
      arrReporteGrafico
    },
    function(response, status, xhr) {
      $( '#modal-loader' ).modal('hide');
      
	    $( '.modal-message' ).removeClass( "modal-danger modal-warning modal-success" );
	    
  	  if (status == "error"){
  	    $( '#modal-message' ).modal('show');
  	    $( '.modal-message' ).addClass( "modal-danger" );
  	    $( '.modal-title-message' ).text( xhr.status + " " + xhr.statusText );
		  }
    }
  );
}

function enviarCorreoSoporteMigracionSistema(){
  $( '#btn-modal-actualizar_version_sistema' ).text('');
  $( '#btn-modal-actualizar_version_sistema' ).attr('disabled', true);
  $( '#btn-modal-actualizar_version_sistema' ).append( 'Actualizando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
  url = base_url + 'InicioController/enviarCorreoSoporteMigracionSistema/';
  $.ajax({
    url       : url,
    type      : "POST",
    dataType  : "JSON",
    data      : {},
    success: function( response ){
      $( '.modal-actualizacion_sistema' ).modal('hide');
        
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
  	    
  	    window.location = base_url + 'InicioController/index';
		  } else {
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1800);
      }
    }
  });
}


    
            