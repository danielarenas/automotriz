var url;

$(function () {
  $( '#modal-loader' ).modal('show');
  
  url = base_url + 'LibrosPLE/RegistroCompraController/getTiposLibroSunat';
  $.post( url, {ID_Tipo_Asiento : 2}, function( response ){
    $( '#cbo-TiposLibroSunatCompra' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
    for (var i = 0, len = response.length; i < len; i++) {
      $( '#cbo-TiposLibroSunatCompra' ).append( '<option value="' + response[i].ID_Tipo_Asiento_Detalle + '" data-id_tipo_asiento="' + response[i].ID_Tipo_Asiento + '" data-nu_codigo_libro_sunat="' + response[i].Nu_Codigo_Libro_Sunat + '" data-no_tipo_asiento_apertura="' + response[i].No_Tipo_Asiento_Apertura + '">' + response[i].No_Sub_Libro_Sunat + '</option>' );
    }
  }, 'JSON');
  
  $( '#cbo-organizaciones' ).html( '<option value="0" selected="selected">- Todas -</option>');
  url = base_url + 'HelperController/getOrganizaciones';
  var arrParams = {
    iIdEmpresa : $( '#header-a-id_empresa' ).val(),
  }
  $.post( url, arrParams, function( response ){
    if ( response.length == 1 ) //única organización
      $( '#cbo-organizaciones' ).append( '<option value="' + response[0].ID_Organizacion + '">' + response[0].No_Organizacion + '</option>' );
    else if (response.length > 1 ) {
      for (var i = 0; i < response.length; i++)
        $( '#cbo-organizaciones' ).append( '<option value="' + response[i].ID_Organizacion + '">' + response[i].No_Organizacion + '</option>' );
    }
    $( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
  $( '#btn-modificar' ).click(function(){
    var fYear, fMonth, iOrdenar;

    fYear = $( '#cbo-year' ).val();
    fMonth = $( '#cbo-mes' ).val();
    iOrdenar = $( '#cbo-ordenar' ).val();

    url = base_url + 'LibrosPLE/RegistroCompraController/modificarCorrelativo';
    $.post( url, {
      fYear : fYear,
      fMonth : fMonth,
      iOrdenar : iOrdenar,
    }, function( response ){
      if ( response.sStatus=='success' ){
        alert( response.sMessage );
      } else {
        alert( response.sMessage );
      }
    }, 'json');
  })

  $( '#table-RegistroCompra' ).hide();
  
  $( '.btn-generarCompra' ).click(function(){
    if ( $( '#cbo-TiposLibroSunatCompra' ).val() == 0 ) {
      $( '#cbo-TiposLibroSunatCompra' ).closest('.form-group').find('.help-block').html('Seleccionar libro');
		  $( '#cbo-TiposLibroSunatCompra' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $( '#cbo-TiposLibroSunatCompra' ).closest('.form-group').find('.help-block').html('');
		  $( '#cbo-TiposLibroSunatCompra' ).closest('.form-group').removeClass('has-error');
		  
      var ID_Tipo_Asiento, ID_Tipo_Asiento_Detalle, sNombreLibroSunat, ID_Organizacion, ID_Tipo_Vista, Nu_Codigo_Libro_Sunat, No_Tipo_Asiento_Apertura, fYear, fMonth, fMonthText;

      ID_Tipo_Asiento = $( '#cbo-TiposLibroSunatCompra' ).find(':selected').data('id_tipo_asiento');
      ID_Tipo_Asiento_Detalle = $( '#cbo-TiposLibroSunatCompra' ).val();
      sNombreLibroSunat = $( '#cbo-TiposLibroSunatCompra :selected' ).text();
      ID_Organizacion = $( '#cbo-organizaciones' ).val();
      ID_Tipo_Vista = $( '#cbo-tipo_vista_venta' ).val();
      Nu_Codigo_Libro_Sunat = $( '#cbo-TiposLibroSunatCompra' ).find(':selected').data('nu_codigo_libro_sunat');
      No_Tipo_Asiento_Apertura = $( '#cbo-TiposLibroSunatCompra' ).find(':selected').data('no_tipo_asiento_apertura');
      fYear = $( '#cbo-year' ).val();
      fMonth = $( '#cbo-mes :selected' ).val();
      fMonthText = $( '#cbo-mes :selected' ).text();
        
		  if ($(this).data('type') == 'htmlCompra') {
        $( '#btn-htmlCompra' ).text('');
        $( '#btn-htmlCompra' ).attr('disabled', true);
        $( '#btn-htmlCompra' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
  	    $( '#table-RegistroCompra >tbody' ).empty();
  	    $( '#table-RegistroCompra >tfoot' ).empty();
        
        var arrPost = {
          ID_Tipo_Asiento : ID_Tipo_Asiento,
          ID_Tipo_Asiento_Detalle : ID_Tipo_Asiento_Detalle,
          ID_Organizacion : ID_Organizacion,
          ID_Tipo_Vista : ID_Tipo_Vista,
          Nu_Codigo_Libro_Sunat : Nu_Codigo_Libro_Sunat,
          No_Tipo_Asiento_Apertura : No_Tipo_Asiento_Apertura,
          fYear : fYear,
          fMonth : fMonth
        };
        if (ID_Tipo_Asiento == 2) {
          url = base_url + 'LibrosPLE/RegistroCompraController/registroCompras';
          $.post( url, arrPost, function( response ){
            if ( response.sStatus == 'success' ) {
              var $sum_Ss_SubTotal_Gravadas = 0.00, $sum_Ss_IGV = 0.00, $sum_Ss_Inafecta = 0.00, $sum_Ss_Exonerada = 0.00, $sum_Ss_Percepcion = 0.00, $sum_Ss_Gratuita = 0.00, $sum_Ss_Exportacion = 0.00, $sum_Ss_Total = 0.00;                
              var $sumGeneral_Ss_SubTotal_Gravadas = 0.00, $sumGeneral_Ss_IGV = 0.00, $sumGeneral_Ss_Inafecta = 0.00, $sumGeneral_Ss_Exonerada = 0.00, $sumGeneral_Ss_Percepcion = 0.00, $sumGeneral_Ss_Gratuita = 0.00, $sumGeneral_Ss_Exportacion = 0.00, $sumGeneral_Ss_Total = 0.00;              
              var $DOCU_Nu_Sunat_Codigo = '', $ID_Tipo_Documento = 0, $No_Tipo_Documento = '', $counter = 0;
              var iTotalRegistros = response.arrData.length, tr_body = '', tr_foot = '';
              var response=response.arrData;
              for (var i = 0; i < iTotalRegistros; i++) {
                if ($DOCU_Nu_Sunat_Codigo != response[i].DOCU_Nu_Sunat_Codigo) {
                  if ($counter != 0) {
                    tr_body +=
                    +"<tr>"
                      +"<th colspan='6' class='text-right'>Total " + $No_Tipo_Documento + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_SubTotal_Gravadas, 2) + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_IGV, 2) + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Inafecta, 2) + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Exonerada, 2) + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Gratuita, 2) + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Percepcion, 2) + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Exportacion, 2) + "</th>"
                      +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Total, 2) + "</th>"
                    +"</tr>";
                  }
                  $sum_Ss_SubTotal_Gravadas = 0.00;
                  $sum_Ss_IGV = 0.00;
                  $sum_Ss_Inafecta = 0.00;
                  $sum_Ss_Exonerada = 0.00;
                  $sum_Ss_Percepcion = 0.00;
                  $sum_Ss_Gratuita = 0.00;
                  $sum_Ss_Exportacion = 0.00;
                  $sum_Ss_Total = 0.00;
                  $DOCU_Nu_Sunat_Codigo = response[i].DOCU_Nu_Sunat_Codigo;
                }
                
                tr_body +=
                "<tr>"
                  +"<td class='text-center'>" + response[i].Fe_Emision + "</td>"
                  +"<td class='text-center'>" + response[i].DOCU_Nu_Sunat_Codigo + "</td>"
                  +"<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
                  +"<td class='text-left'>" + response[i].ID_Numero_Documento_Inicial + (response[i].ID_Numero_Documento_Final != '' ? response[i].ID_Numero_Documento_Final : '' ) + "</td>"
                  +"<td class='text-left'>" + response[i].Nu_Documento_Identidad + "</td>"
                  +"<td class='text-left'>" + response[i].No_Entidad + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_SubTotal_Gravadas, 2) + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_IGV, 2) + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Inafecta, 2) + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Exonerada, 2) + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Gratuita, 2) + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Percepcion, 2) + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Exportacion, 2) + "</td>"
                  +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Total, 2) + "</td>"
                  +"<td class='text-left'>" + response[i].MONE_Nu_Sunat_Codigo + "</td>"
                  +"<td class='text-right'>" + response[i].Ss_Tipo_Cambio + "</td>"
                  +"<td class='text-left'>" + (response[i].Fe_Emision_Modificar == '01/01/0001' ? '' : response[i].Fe_Emision_Modificar) + "</td>"
                  +"<td class='text-left'>" + (response[i].ID_Tipo_Documento_Modificar == null ? '' : response[i].ID_Tipo_Documento_Modificar) + "</td>"
                  +"<td class='text-left'>" + (response[i].ID_Serie_Documento_Modificar == null ? '' : response[i].ID_Serie_Documento_Modificar) + "</td>"
                  +"<td class='text-left'>" + (response[i].ID_Numero_Documento_Modificar == null ? '' : response[i].ID_Numero_Documento_Modificar) + "</td>"
                  +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</span></td>"
                +"</tr>";
                
                $counter++;
                $sum_Ss_SubTotal_Gravadas += parseFloat(response[i].Ss_SubTotal_Gravadas);
                $sum_Ss_IGV += parseFloat(response[i].Ss_IGV);
                $sum_Ss_Inafecta += parseFloat(response[i].Ss_Inafecta);
                $sum_Ss_Exonerada += parseFloat(response[i].Ss_Exonerada);
                $sum_Ss_Gratuita += parseFloat(response[i].Ss_Gratuita);
                $sum_Ss_Percepcion += parseFloat(response[i].Ss_Percepcion);
                $sum_Ss_Exportacion += parseFloat(response[i].Ss_Exportacion);
                $sum_Ss_Total += parseFloat(response[i].Ss_Total);

                $sumGeneral_Ss_SubTotal_Gravadas += parseFloat(response[i].Ss_SubTotal_Gravadas);
                $sumGeneral_Ss_IGV += parseFloat(response[i].Ss_IGV);
                $sumGeneral_Ss_Inafecta += parseFloat(response[i].Ss_Inafecta);
                $sumGeneral_Ss_Exonerada += parseFloat(response[i].Ss_Exonerada);
                $sumGeneral_Ss_Gratuita += parseFloat(response[i].Ss_Gratuita);
                $sumGeneral_Ss_Percepcion += parseFloat(response[i].Ss_Percepcion);
                $sumGeneral_Ss_Exportacion += parseFloat(response[i].Ss_Exportacion);
                $sumGeneral_Ss_Total += parseFloat(response[i].Ss_Total);
                
                if ($ID_Tipo_Documento != response[i].ID_Tipo_Documento) {
                  $ID_Tipo_Documento = response[i].ID_Tipo_Documento;
                  $No_Tipo_Documento = response[i].No_Tipo_Documento;
                }
              }
              
              tr_foot =
              "<tfoot>"
                +"<tr>"
                  +"<th colspan='6' class='text-right'>Total " + $No_Tipo_Documento + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_SubTotal_Gravadas, 2) + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_IGV, 2) + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Inafecta, 2) + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Exonerada, 2) + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Gratuita, 2) + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Percepcion, 2) + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Exportacion, 2) + "</th>"
                  +"<th class='text-right'>" + ($ID_Tipo_Documento != 5 ? '' : '-') + number_format($sum_Ss_Percepcion + $sum_Ss_Total, 2) + "</th>"
                +"</tr>"
                +"<tr>"
                  +"<th colspan='6' class='text-right'>Total General</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_SubTotal_Gravadas, 2) + "</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_IGV, 2) + "</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_Inafecta, 2) + "</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_Exonerada, 2) + "</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_Gratuita, 2) + "</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_Percepcion, 2) + "</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_Exportacion, 2) + "</th>"
                  +"<th class='text-right'>" + number_format($sumGeneral_Ss_Percepcion + $sumGeneral_Ss_Total, 2) + "</th>"
                +"</tr>"
              +"</tfoot>";
            } else {
              if( response.sMessageSQL !== undefined ) {
                console.log(response.sMessageSQL);
              }
              tr_body +=
              "<tr>"
                + "<td colspan='21' class='text-center'>" + response.sMessage + "</td>"
              + "</tr>";
            }
            $( '#table-RegistroCompra' ).show();
            $( '#table-RegistroCompra >tbody' ).append(tr_body);
            $( '#table-RegistroCompra >tbody' ).after(tr_foot);
            
            $( '#btn-htmlCompra' ).text('');
            $( '#btn-htmlCompra' ).append( '<i class="fa fa-table"></i> HTML' );
            $( '#btn-htmlCompra' ).attr('disabled', false);
          }, 'JSON')
          .fail(function(jqXHR, textStatus, errorThrown) {
            $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
            
            $( '#modal-message' ).modal('show');
            $( '.modal-message' ).addClass( 'modal-danger' );
            $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
            setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
            
            //Message for developer
            console.log(jqXHR.responseText);
            
            $( '#btn-htmlCompra' ).text('');
            $( '#btn-htmlCompra' ).append( '<i class="fa fa-table"></i> HTML' );
            $( '#btn-htmlCompra' ).attr('disabled', false);
          });
        }
		  } else if ($(this).data('type') == 'pdfCompra') {
        $( '#btn-pdfCompra' ).text('');
        $( '#btn-pdfCompra' ).attr('disabled', true);
        $( '#btn-pdfCompra' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
            
        url = base_url + 'LibrosPLE/RegistroCompraController/registroCompraPDF/' + ID_Organizacion + '/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle + '/' + ID_Tipo_Vista + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura + '/' + fYear + '/' + fMonth + '/' + fMonthText + '/' + sNombreLibroSunat;
        window.open(url,'_blank');
        
        $( '#btn-pdfCompra' ).text('');
        $( '#btn-pdfCompra' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
        $( '#btn-pdfCompra' ).attr('disabled', false);
		  } else if ($(this).data('type') == 'excelCompra') {
		    $( '#btn-excelCompra' ).text('');
        $( '#btn-excelCompra' ).attr('disabled', true);
        $( '#btn-excelCompra' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
        
        url = base_url + 'LibrosPLE/RegistroCompraController/registroCompraEXCEL/' + ID_Organizacion + '/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle + '/' + ID_Tipo_Vista + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura + '/' + fYear + '/' + fMonth + '/' + fMonthText + '/' + sNombreLibroSunat;
        window.open(url,'_blank');
        
        $( '#btn-excelCompra' ).text('');
        $( '#btn-excelCompra' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
        $( '#btn-excelCompra' ).attr('disabled', false);
		  } else if ($(this).data('type') == 'txtCompra') {
		    $( '#btn-txtCompra' ).text('');
        $( '#btn-txtCompra' ).attr('disabled', true);
        $( '#btn-txtCompra' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
        
        if (ID_Tipo_Asiento_Detalle == 1) {
          url = base_url + 'LibrosPLE/RegistroCompraController/registroCompraTXT/' + ID_Organizacion + '/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle  + '/' + ID_Tipo_Vista + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura + '/' + fYear + '/' + fMonth + '/' + fMonthText;
          window.open(url,'_blank');
        } else if (ID_Tipo_Asiento_Detalle == 2) {
          url = base_url + 'LibrosPLE/RegistroCompraController/registroCompraNODomiciliadoTXT/' + ID_Organizacion + '/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle + '/' + ID_Tipo_Vista + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura + '/' + fYear + '/' + fMonth + '/' + fMonthText;
          window.open(url,'_blank');
        } else if (ID_Tipo_Asiento_Detalle == 3) {
          url = base_url + 'LibrosPLE/RegistroCompraController/registroCompraSimplificadoTXT/' + ID_Organizacion + '/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle + '/' + ID_Tipo_Vista + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura + '/' + fYear + '/' + fMonth + '/' + fMonthText;
          window.open(url,'_blank');
        }

        $( '#btn-txtCompra' ).text('');
        $( '#btn-txtCompra' ).append( '<i class="fa fa-files-o"></i> Libro Electrónico' );
        $( '#btn-txtCompra' ).attr('disabled', false);
		  }
    }
  })
})