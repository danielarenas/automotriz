var url;

$(function () {
  $('.select2').select2();
  $('#modal-loader').modal('show');

  url = base_url + 'LibrosPLE/KardexController/getTiposLibroSunat';
  $.post(url, { ID_Tipo_Asiento: 3 }, function (response) {
    $('#cbo-TiposLibroSunatKardex').html('<option value="0" selected="selected">- Seleccionar -</option>');
    for (var i = 0, len = response.length; i < len; i++)
      $('#cbo-TiposLibroSunatKardex').append('<option value="' + response[i].ID_Tipo_Asiento_Detalle + '" data-id_tipo_asiento="' + response[i].ID_Tipo_Asiento + '" data-nu_codigo_libro_sunat="' + response[i].Nu_Codigo_Libro_Sunat + '" data-no_tipo_asiento_apertura="' + response[i].No_Tipo_Asiento_Apertura + '">' + response[i].No_Sub_Libro_Sunat + '</option>');
  }, 'JSON');

  var arrParams = {};
  getAlmacenes(arrParams);

  $('.div-productos').hide();
  $('#txt-ID_Producto').val(0);
  $('#cbo-FiltrosProducto').change(function () {
    $('.div-productos').hide();
    $('#txt-ID_Producto').val(0);
    $('#txt-No_Producto').val('');
    if ($(this).val() > 0)
      $('.div-productos').show();
  })

  $('#table-Kardex').hide();

  $('.btn-generar_kardex').click(function () {
    if ($('#cbo-TiposLibroSunatKardex').val() == 0) {
      $('#cbo-TiposLibroSunatKardex').closest('.form-group').find('.help-block').html('Seleccionar libro');
      $('#cbo-TiposLibroSunatKardex').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-Almacenes_filtro_kardex').val() == 0) {
      $('#cbo-Almacenes_filtro_kardex').closest('.form-group').find('.help-block').html('Seleccionar almacén');
      $('#cbo-Almacenes_filtro_kardex').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $('.help-block').empty();

      var ID_Tipo_Asiento, ID_Tipo_Asiento_Detalle, ID_Almacen, dInicio, dFin, ID_Producto, Txt_Direccion_Almacen, Nu_Codigo_Libro_Sunat, No_Tipo_Asiento_Apertura;

      ID_Tipo_Asiento = $('#cbo-TiposLibroSunatKardex').find(':selected').data('id_tipo_asiento');
      ID_Tipo_Asiento_Detalle = $('#cbo-TiposLibroSunatKardex').val();
      ID_Almacen = $('#cbo-Almacenes_filtro_kardex').val();
      dInicio = ParseDateString($('#txt-Filtro_Fe_Inicio').val(), 1, '/');
      dFin = ParseDateString($('#txt-Filtro_Fe_Fin').val(), 1, '/');
      ID_Producto = $('#txt-ID_Producto').val();
      Txt_Direccion_Almacen = $('#cbo-Almacenes_filtro_kardex').find(':selected').data('direccion_almacen');
      Nu_Codigo_Libro_Sunat = $('#cbo-TiposLibroSunatKardex').find(':selected').data('nu_codigo_libro_sunat');
      No_Tipo_Asiento_Apertura = $('#cbo-TiposLibroSunatKardex').find(':selected').data('no_tipo_asiento_apertura');

      if ($(this).data('type') == 'html') {
        $('#btn-html_kardex').text('');
        $('#btn-html_kardex').attr('disabled', true);
        $('#btn-html_kardex').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

        $('#table-Kardex >tbody').empty();
        $('#table-Kardex >tfoot').empty();

        var arrPost = {
          ID_Tipo_Asiento: ID_Tipo_Asiento,
          ID_Tipo_Asiento_Detalle: ID_Tipo_Asiento_Detalle,
          ID_Almacen: ID_Almacen,
          dInicio: dInicio,
          dFin: dFin,
          ID_Producto: ID_Producto,
        };

        if (ID_Tipo_Asiento == 3) {
          url = base_url + 'LibrosPLE/KardexController/kardex';
          $.post(url, arrPost, function (response) {
            if (response.sStatus == 'success') {
              var iTotalRegistros = response.arrData.length, response = response.arrData, tr_body = '', tr_foot = '';
              var $ID_Producto = 0, $counter = 0, $sum_Producto_Qt_Entrada = 0.00, $sum_Producto_Qt_Salida = 0.00, $sum_General_Qt_Entrada = 0.00, $sum_General_Qt_Salida = 0.00, $Qt_Producto_Saldo_Movimiento = 0.00;
              for (var i = 0; i < iTotalRegistros; i++) {
                if ($ID_Producto != response[i].ID_Producto) {
                  if ($counter != 0) {
                    tr_body +=
                      +"<tr>"
                      + "<th class='text-right' colspan='9'>Total Producto</th>"
                      + "<th class='text-right'>" + $sum_Producto_Qt_Entrada + "</th>"
                      + "<th class='text-right'>" + $sum_Producto_Qt_Salida + "</th>"
                      + "</tr>";
                  }

                  $sum_Producto_Qt_Entrada = 0.00;
                  $sum_Producto_Qt_Salida = 0.00;

                  tr_body +=
                    "<tr>"
                    + "<th class='text-right'>UPC</th>"
                    + "<th class='text-left'>" + response[i].Nu_Codigo_Barra + "</th>"
                    + "<th class='text-right'>SKU</th>"
                    + "<th class='text-left'>" + response[i].No_Codigo_Interno + "</th>"
                    + "<th class='text-right'>Nombre</th>"
                    + "<th class='text-left' colspan='3'>" + response[i].No_Producto + "</th>"
                    + "<th class='text-right' colspan='2'>Saldo Inicial</th>"
                    + "<th class='text-right'>" + number_format(parseFloat(response[i].Qt_Producto_Inicial), 6) + "</th>"
                    + "</tr>";
                  $ID_Producto = response[i].ID_Producto;
                  $Qt_Producto_Saldo_Movimiento = parseFloat(response[i].Qt_Producto_Inicial);
                }

                tr_body +=
                  "<tr>"
                  + "<td class='text-center'>" + response[i].Fe_Emision + "</td>"
                  + "<td class='text-center'>" + response[i].Tipo_Documento_Sunat_Codigo + "</td>"
                  + "<td class='text-center'>" + response[i].No_Tipo_Documento_Breve + "</td>"
                  + "<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
                  + "<td class='text-center'>" + response[i].ID_Numero_Documento + "</td>"
                  + "<td class='text-center'>" + response[i].Tipo_Operacion_Sunat_Codigo + "</td>"
                  + "<td class='text-center'>" + response[i].No_Tipo_Movimiento + "</td>"
                  + "<td class='text-left'>" + response[i].Nu_Documento_Identidad + "</td>"
                  + "<td class='text-left'>" + response[i].No_Entidad + "</td>";

                if (response[i].Nu_Tipo_Movimiento == 0) {//Entrada
                  tr_body +=
                    "<td class='text-right'>" + number_format(parseFloat(response[i].Qt_Producto), 6) + "</td>"
                    + "<td class='text-right'>0.00</td>";

                  $Qt_Producto_Saldo_Movimiento += parseFloat(response[i].Qt_Producto);
                  $sum_Producto_Qt_Entrada += parseFloat(response[i].Qt_Producto);
                  $sum_General_Qt_Entrada += parseFloat(response[i].Qt_Producto);
                } else { //Salida
                  tr_body +=
                    "<td class='text-right'>0.00</td>"
                    + "<td class='text-right'>" + number_format(parseFloat(response[i].Qt_Producto), 6) + "</td>";

                  $Qt_Producto_Saldo_Movimiento -= parseFloat(response[i].Qt_Producto);
                  $sum_Producto_Qt_Salida += parseFloat(response[i].Qt_Producto);
                  $sum_General_Qt_Salida += parseFloat(response[i].Qt_Producto);
                }

                tr_body += "<td class='text-right'>" + ($Qt_Producto_Saldo_Movimiento < 0 ? '-' : '') + number_format($Qt_Producto_Saldo_Movimiento, 6) + "</td>"
                tr_body += "<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</td>"
                  + "</tr>";
                $counter++;
              }

              tr_foot =
                "<tfoot>"
                + "<tr>"
                + "<th class='text-right' colspan='9'>Total Producto</th>"
                + "<th class='text-right'>" + $sum_Producto_Qt_Entrada + "</th>"
                + "<th class='text-right'>" + $sum_Producto_Qt_Salida + "</th>"
                + "</tr>"
                + "<tr>"
                + "<th class='text-right' colspan='9'>Total General</th>"
                + "<th class='text-right'>" + $sum_General_Qt_Entrada + "</th>"
                + "<th class='text-right'>" + $sum_General_Qt_Salida + "</th>"
                + "</tr>"
                + "</tfoot>";
            } else {
              tr_body +=
                "<tr>"
                + "<td colspan='13' class='text-center'>No hay registros</td>"
                + "</tr>";
            }

            $('#table-Kardex').show();
            $('#table-Kardex >tbody').append(tr_body);
            $('#table-Kardex >tbody').after(tr_foot);

            $('#btn-html_kardex').text('');
            $('#btn-html_kardex').append('<i class="fa fa-table"></i> HTML');
            $('#btn-html_kardex').attr('disabled', false);
          }, 'JSON')
            .fail(function (jqXHR, textStatus, errorThrown) {
              $('.modal-message').removeClass('modal-danger modal-warning modal-success');

              $('#modal-message').modal('show');
              $('.modal-message').addClass('modal-danger');
              $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
              setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

              //Message for developer
              console.log(jqXHR.responseText);

              $('#btn-html_kardex').text('');
              $('#btn-html_kardex').append('<i class="fa fa-search"></i> Buscar');
              $('#btn-html_kardex').attr('disabled', false);
            });
        }
      } else if ($(this).data('type') == 'pdf') {
        $('#btn-pdf_kardex').text('');
        $('#btn-pdf_kardex').attr('disabled', true);
        $('#btn-pdf_kardex').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

        url = base_url + 'LibrosPLE/KardexController/kardexPDF/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle + '/' + ID_Almacen + '/' + dInicio + '/' + dFin + '/' + ID_Producto + '/' + Txt_Direccion_Almacen + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura;
        window.open(url, '_blank');

        $('#btn-pdf_kardex').text('');
        $('#btn-pdf_kardex').append('<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF');
        $('#btn-pdf_kardex').attr('disabled', false);
      } else if ($(this).data('type') == 'excel') {
        $('#btn-excel_kardex').text('');
        $('#btn-excel_kardex').attr('disabled', true);
        $('#btn-excel_kardex').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

        url = base_url + 'LibrosPLE/KardexController/kardexEXCEL/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle + '/' + ID_Almacen + '/' + dInicio + '/' + dFin + '/' + ID_Producto + '/' + Txt_Direccion_Almacen + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura;
        window.open(url, '_blank');

        $('#btn-excel_kardex').text('');
        $('#btn-excel_kardex').append('<i class="fa fa-file-excel-o color_icon_excel"></i> Excel');
        $('#btn-excel_kardex').attr('disabled', false);
      } else if ($(this).data('type') == 'txt') {
        $('#btn-txt_kardex').text('');
        $('#btn-txt_kardex').attr('disabled', true);
        $('#btn-txt_kardex').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

        url = base_url + 'LibrosPLE/KardexController/kardexTXT/' + ID_Tipo_Asiento + '/' + ID_Tipo_Asiento_Detalle + '/' + ID_Almacen + '/' + dInicio + '/' + dFin + '/' + ID_Producto + '/' + Txt_Direccion_Almacen + '/' + Nu_Codigo_Libro_Sunat + '/' + No_Tipo_Asiento_Apertura;
        window.open(url, '_blank');

        $('#btn-txt_kardex').text('');
        $('#btn-txt_kardex').append('<i class="fa fa-files-o"></i> Libro Electrónico');
        $('#btn-txt_kardex').attr('disabled', false);
      }
    }
  })
})

// Ayudas - combobox
function getAlmacenes(arrParams) {
  url = base_url + 'HelperController/getAlmacenes';
  $.post(url, {}, function (responseAlmacen) {
    var iCantidadRegistros = responseAlmacen.length;
    var selected = '';
    var iIdAlmacen = 0;
    if (iCantidadRegistros == 1) {
      if (arrParams !== undefined) {
        iIdAlmacen = arrParams.ID_Almacen;
      }
      if (iIdAlmacen == responseAlmacen[0]['ID_Almacen']) {
        selected = 'selected="selected"';
      }
      $('#cbo-Almacenes_filtro_kardex').html('<option value="' + responseAlmacen[0]['ID_Almacen'] + '" ' + selected + ' data-direccion_almacen="' + responseAlmacen[0]['Txt_Direccion_Almacen'] + '">' + responseAlmacen[0]['No_Almacen'] + '</option>');
    } else {
      $('#cbo-Almacenes_filtro_kardex').html('<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < iCantidadRegistros; i++) {
        if (arrParams !== undefined) {
          iIdAlmacen = arrParams.ID_Almacen;
        }
        if (iIdAlmacen == responseAlmacen[0]['ID_Almacen']) {
          selected = 'selected="selected"';
        }
        $('#cbo-Almacenes_filtro_kardex').append('<option value="' + responseAlmacen[i]['ID_Almacen'] + '" ' + selected + ' data-direccion_almacen="' + responseAlmacen[0]['Txt_Direccion_Almacen'] + '">' + responseAlmacen[i]['No_Almacen'] + '</option>');
      }
    }
    $('#modal-loader').modal('hide');
  }, 'JSON');
}