var url;
var table_serie;
var accion_serie;

$(function () {
  $('.select2').select2();

  $('#btn-generar_liquidacion').off('click').click(function () {
    $('#hidden-ID_Tipo_Documento').val(3);//Factura

    url = base_url + 'Ventas/LiquidacionPresupuestoController/generarLiquidacion';
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: url,
      data: $('#form-liquidacion_prespuesto').serialize(),
      success: function (response) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.sStatus == 'success') {
          $('.modal-message').addClass('modal-' + response.sStatus);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);

          reload_table_serie();
        } else {
          $('.modal-message').addClass('modal-' + response.sStatus);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
        }

        $('#btn-generar_liquidacion').text('');
        $('#btn-generar_liquidacion').append('Generar Liquidación Factura');
        $('#btn-generar_liquidacion').attr('disabled', false);
      }
    })
      .fail(function (jqXHR, textStatus, errorThrown) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');

        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

        $('#modal-loader').modal('hide');

        //Message for developer
        console.log(jqXHR.responseText);

        $('#btn-generar_liquidacion').text('');
        $('#btn-generar_liquidacion').append('Generar Liquidación');
        $('#btn-generar_liquidacion').attr('disabled', false);
      });
  });

  $('#btn-generar_liquidacion_nota').off('click').click(function () {
    $('#hidden-ID_Tipo_Documento').val(2);//Nota de venta

    url = base_url + 'Ventas/LiquidacionPresupuestoController/generarLiquidacion';
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: url,
      data: $('#form-liquidacion_prespuesto').serialize(),
      success: function (response) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.sStatus == 'success') {
          $('.modal-message').addClass('modal-' + response.sStatus);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);

          reload_table_serie();
        } else {
          $('.modal-message').addClass('modal-' + response.sStatus);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
        }

        $('#btn-generar_liquidacion_nota').text('');
        $('#btn-generar_liquidacion_nota').append('Generar Liquidación Factura');
        $('#btn-generar_liquidacion_nota').attr('disabled', false);
      }
    })
      .fail(function (jqXHR, textStatus, errorThrown) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');

        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

        $('#modal-loader').modal('hide');

        //Message for developer
        console.log(jqXHR.responseText);

        $('#btn-generar_liquidacion_nota').text('');
        $('#btn-generar_liquidacion_nota').append('Generar Liquidación');
        $('#btn-generar_liquidacion_nota').attr('disabled', false);
      });
  });

	$(document).keyup(function(event){
    if(event.which == 27){//ESC
      $( "#modal-Serie" ).modal('hide');
    }
	});
	
  url = base_url + 'Ventas/LiquidacionPresupuestoController/ajax_list';
  table_serie = $( '#table-Serie' ).DataTable({
    'dom'       : 'B<"top">frt<"bottom"lp><"clear">',
    buttons     : [{
      extend    : 'excel',
      text      : '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr : 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'pdf',
      text      : '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr : 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'colvis',
      text      : '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr : 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching'   : false,
    'bStateSave'  : true,
    'processing'  : true,
    'serverSide'  : true,
    'info'        : true,
    'autoWidth'   : false,
    'pagingType'  : 'full_numbers',
    'oLanguage' : {
      'sInfo'               : 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu'         : '_MENU_',
      'sSearch'             : 'Buscar por: ',
      'sSearchPlaceholder'  : 'UPC / Nombre',
      'sZeroRecords'        : 'No se encontraron registros',
      'sInfoEmpty'          : 'No hay registros',
      'sLoadingRecords'     : 'Cargando...',
      'sProcessing'         : 'Procesando...',
      'oPaginate'           : {
        'sFirst'    : '<<',
        'sLast'     : '>>',
        'sPrevious' : '<',
        'sNext'     : '>',
      },
    },
    'order': [],
    'ajax': {
      'url'       : url,
      'type'      : 'POST',
      'dataType'  : 'JSON',
      'data'      : function ( data ) {
        data.Filtro_Fe_Inicio = ParseDateString($('#txt-Filtro_Fe_Inicio').val(), 1, '/'),
          data.Filtro_Fe_Fin = ParseDateString($('#txt-Filtro_Fe_Fin').val(), 1, '/'),
          data.Filtro_fecha_liquidacion = $('#cbo-Filtro_fecha_liquidacion option:selected').val(),
          data.Filtro_envio_fecha_liquidacion = $('#cbo-Filtro_envio_fecha_liquidacion option:selected').val(),
          data.Filtro_SerieDocumento = $('#txt-Filtro_SerieDocumento').val(),
          data.Filtro_NumeroDocumento = $('#txt-Filtro_NumeroDocumento').val(),
          data.Filtro_Entidad = $('#txt-Filtro_Entidad').val(),
          data.Filtro_Servicio = $('#txt-Filtro_Servicio').val(),
          data.Filtro_Periodo = $('#txt-Filtro_Periodo').val(),
          data.Filtro_OC = $('#txt-Filtro_OC').val();
      },
    },
    'columnDefs': [{
      'className' : 'text-center',
      'targets'   : 'no-sort',
      'orderable' : false,
    },],
    'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
  });
  
  $( '.dataTables_length' ).addClass('col-md-3');
  $('.dataTables_paginate').addClass('col-md-9');
  
  $('#btn-filter').click(function () {
    table_serie.ajax.reload();
  });

  // MODIFICAR DATOS
  $('#btn-modificar').click(function () {
    $('#btn-modificar').text('');
    $('#btn-modificar').attr('disabled', true);
    $('#btn-modificar').append('Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');
    $('#btn-salir').attr('disabled', true);

    url = base_url + 'Ventas/LiquidacionPresupuestoController/modificarVenta';
    $.ajax({
      type: 'POST',
      dataType: 'JSON',
      url: url,
      data: $('#form-modificar').serialize(),
      success: function (response) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.sStatus == 'success') {
          $('.modal-modificar').modal('hide');

          $('.modal-message').addClass('modal-' + response.sStatus);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);

          reload_table_serie();
        } else {
          $('.modal-message').addClass('modal-' + response.sStatus);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 3100);
        }

        $('#btn-modificar').text('');
        $('#btn-modificar').append('Modificar');
        $('#btn-modificar').attr('disabled', false);
        $('#btn-salir').attr('disabled', false);
      }
    })
      .fail(function (jqXHR, textStatus, errorThrown) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');

        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 3100);

        //Message for developer
        console.log(jqXHR.responseText);

        $('#btn-modificar').text('');
        $('#btn-modificar').attr('disabled', false);
        $('#btn-modificar').append('Modificar');
        $('#btn-salir').attr('disabled', false);
      })
  })
})

function reload_table_serie(){
  table_serie.ajax.reload(null,false);
}

function pdfLiquidacionPresupuesto(ID) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('.modal-message-delete').removeClass('modal-danger modal-warning modal-success');
  $('.modal-message-delete').addClass('modal-success');

  $('.modal-title-message-delete').text('¿Deseas generar PDF?');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $('#btn-save-delete').off('click').click(function () {
    sendPDF($modal_delete, ID);
  });
}

function sendPDF($modal_delete, ID) {
  $('#modal-loader').modal('show');
  $modal_delete.modal('hide');
  url = base_url + 'Ventas/LiquidacionPresupuestoController/generarPDF/' + ID;
  window.open(url, '_blank');
  $('#modal-loader').modal('hide');
}

function modificarVenta(arrParams) {
  arrParams = JSON.parse(arrParams);
  var No_Tipo_Documento_Breve = '', No_Entidad = '';
  No_Entidad = arrParams.No_Entidad;
  No_Entidad = No_Entidad.split('-');
  No_Entidad = No_Entidad[0] + ' ' + No_Entidad[1] + ' ' + (No_Entidad[2] !== undefined ? No_Entidad[2] : '');
  
  No_Tipo_Documento_Breve = arrParams.No_Tipo_Documento_Breve;
  No_Tipo_Documento_Breve = No_Tipo_Documento_Breve.split('-');
  No_Tipo_Documento_Breve = No_Tipo_Documento_Breve[0] + ' ' + No_Tipo_Documento_Breve[1];

  $('#form-modificar')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('.modal-modificar').modal('show');

  var sDocumento = No_Tipo_Documento_Breve + ' ' + arrParams.ID_Serie_Documento + ' ' + arrParams.ID_Numero_Documento;
  $('#modal-header-modificar-title').text(sDocumento);

  $('#modificar-modal-body-cliente').text(No_Entidad);

  $('[name="ID_Documento_Cabecera-Modificar"]').val(arrParams.ID_Documento_Cabecera);
  $('[name="Fe_Liquidacion_Envio_Presupuesto-Modificar"]').val(arrParams.Fe_Emision);
  var Fe_Emision = arrParams.Fe_Emision.split('-');
  $('#txt-Fe_Liquidacion_Envio_Presupuesto').datepicker({
    autoclose: true,
    startDate: new Date(parseInt(Fe_Emision[0]), parseInt(Fe_Emision[1]) - 1, parseInt(Fe_Emision[2])),
    todayHighlight: true
  })
  $('#txt-Fe_Liquidacion_Envio_Presupuesto').val(ParseDateString(arrParams.Fe_Emision, 6, '-'));
}