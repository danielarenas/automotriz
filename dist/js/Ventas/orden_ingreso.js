var url;
var table_orden_venta;
var considerar_igv;
var value_importes_cero = 0.00;
var texto_importes_cero = '0.00';
var arrImpuestosProducto = '{ "arrImpuesto" : [';
var arrImpuestosProductoDetalle;
var accion_orden_venta = '';
var bEstadoValidacion; 

function agregarOrdenIngreso() {
  accion_orden_venta = 'add_orden_venta';

  $('#modal-loader').modal('show');

  $('.div-Listar').hide();
  $('.div-AgregarEditar').show();

  $('#txt-EID_Empresa').focus();

  $('#form-OrdenIngreso')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('.title_OrdenIngreso').text('Nueva Orden de Ingreso');

  $('[name="EID_Empresa"]').val('');
  $('[name="EID_Documento_Cabecera"]').val('');
  $('[name="ENu_Estado"]').val('');
  $('[name="Nu_Estado_Facturacion"]').val('');
  $('[name="EID_Orden_Ingreso"]').val('');

  $('[name="sTypeAction"]').val('add');
  if ($('[name="sTypeAction"]').val() == 'add')
    $('.div-fecha_salida').hide();

  $('.date-picker-invoice').val(fDay + '/' + fMonth + '/' + fYear);

  $('#radio-cliente_existente').prop('checked', true).iCheck('update');
  $('#radio-cliente_nuevo').prop('checked', false).iCheck('update');
  $('.div-cliente_existente').show();
  $('.div-cliente_nuevo').hide();

  $('#radio-contacto_existente').prop('checked', true).iCheck('update');
  $('#radio-contacto_nuevo').prop('checked', false).iCheck('update');
  $('.div-contacto_existente').show();
  $('.div-contacto_nuevo').hide();

  $('#btn-save').attr('disabled', false);

  url = base_url + 'HelperController/getTiposDocumentos';
  $.post(url, { Nu_Tipo_Filtro: 13 }, function (response) {
    $('#cbo-area_ingreso').html('<option value="0" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-area_ingreso').append('<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>');
  }, 'JSON');

  var Fe_Emision = $('#txt-Fe_Emision').val().split('/');
  $('#txt-Fe_Vencimiento').datepicker({
    autoclose: true,
    startDate: new Date(Fe_Emision[2], Fe_Emision[1] - 1, Fe_Emision[0]),
    todayHighlight: true
  })

  $('#txt-Fe_Entrega').datepicker({
    autoclose: true,
    startDate: new Date(Fe_Emision[2], Fe_Emision[1] - 1, Fe_Emision[0]),
    todayHighlight: true
  })

  $('#txt-Fe_Entrega').datepicker({
    autoclose: true,
    startDate: new Date(Fe_Emision[2], Fe_Emision[1] - 1, Fe_Emision[0]),
    todayHighlight: true
  })

  $('#txt-Fe_Entrega').val($('#txt-Fe_Emision').val());
  $('#txt-Fe_Entrega_Tentativa').val($('#txt-Fe_Emision').val());

  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post(url, function (response) {
    $('#cbo-TiposDocumentoIdentidadCliente').html('');
    for (var i = 0; i < response.length; i++)
      $('#cbo-TiposDocumentoIdentidadCliente').append('<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>');

    $('#modal-loader').modal('hide');
  }, 'JSON');

  $('#cbo-descargar_stock').html('<option value="1">Si</option>');
  $('#cbo-descargar_stock').append('<option value="0">No</option>');

  var arrParams = {
    iIdComboxAlmacen: 'cbo-almacen',
  };
  getAlmacenes(arrParams);
  
  url = base_url + 'HelperController/getValoresTablaDato';
  $.post(url, { sTipoData: 'Orden_Ingreso_Niveles_Combustible'}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      $( '#cbo-nivel_combustible' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
      for (var x = 0; x < l; x++) {
        $( '#cbo-nivel_combustible' ).append( '<option value="' + response.arrData[x].ID_Tabla_Dato + '">' + response.arrData[x].No_Descripcion + '</option>' );
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      if ( response.sStatus == 'warning')
        $( '#cbo-nivel_combustible' ).html('<option value="0" selected="selected">- Vacío -</option>');
    }
  }, 'JSON');

  $('#txt-Fe_Entrega_Tentativa').val('00/00/0000');

  /* obtener imagen guardada(s) */
  $('.divDropzone').html(
    '<div id="id-divDropzone" class="dropzone div-dropzone">'
    + '<div class="dz-message">'
    + 'Arrastrar o presionar click para subir imágen'
    + '</div>'
    + '</div>'
  );


  Dropzone.autoDiscover = false;
  Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastrar o presionar click para subir imágen";
  Dropzone.prototype.defaultOptions.dictFallbackMessage = "Tu navegador no soporta la función arrastrar la imágen";
  Dropzone.prototype.defaultOptions.dictFileTooBig = "La imágen pesa ({{filesize}}MiB). El tamaño máximo es: {{maxFilesize}}MiB.";
  Dropzone.prototype.defaultOptions.dictInvalidFileType = "Solo se permite imágenes / pdf";
  Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
  Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estás seguro de cancelar la subida?";
  Dropzone.prototype.defaultOptions.dictRemoveFile = "Eliminar";
  Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo se puede subir 1 imágen / pdf";

  url = base_url + 'Ventas/OrdenIngresoController/uploadOnly';
  var myDropzone = new Dropzone("#id-divDropzone", {
    url: url,
    params: {
      iVersionImage: 1,
      iIdFamilia: 1,
    },
    acceptedFiles: ".jpeg,.jpg,.png,.svg,.pdf",
    addRemoveLinks: true,
    uploadMultiple: false,
    maxFilesize: 1,//Peso en MB
    maxFiles: 1,
    thumbnailHeight: 200,
    thumbnailWidth: 200,
    parallelUploads: 1,
    thumbnail: function (file, dataUrl) {
      if (file.previewElement) {
        file.previewElement.classList.remove("dz-file-preview");
        var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
        for (var i = 0; i < images.length; i++) {
          var thumbnailElement = images[i];
          thumbnailElement.alt = file.name;
          thumbnailElement.src = dataUrl;
        }
        setTimeout(function () { file.previewElement.classList.add("dz-image-preview"); }, 1);
      }
    },
    removedfile: function (file) {
      var nameFileImage = file.name;
      url = base_url + 'Ventas/OrdenIngresoController/removeFileImage';
      $.ajax({
        url: url,
        type: "POST",
        dataType: "JSON",
        data: { nameFileImage: nameFileImage },
      })
      var previewElement;
      return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
    },
    init: function () {
      //Verificar respuesta del servidor al subir archivo
      this.on("success", function (file, response) {
        var response = jQuery.parseJSON(response);

        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.sStatus != 'error') {
          $('.modal-message').addClass(response.sClassModal);
          $('.modal-title-message').text(response.sMessage);

          $('[name="No_Imagen"]').val(response.sNombreImagen);
          $('[name="No_Imagen_Url"]').val(response.sNombreImagenUrl);

          setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
        } else {
          $('.modal-message').addClass(response.sClassModal);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
        }
      })
    },
  });


// REQ-3 - INICIO 
let yourDate = new Date();

$( "#txt-Fe_Emision" ).prop( "disabled", true );
$( "#txt-Fe_Entrega" ).prop( "disabled", true );
$('txt-Fe_Emision').val(yourDate.toISOString().split('T')[0]);
$('Fe_Entrega').val(yourDate.toISOString().split('T')[0]);

// REQ-3 - FIN

  $('.div-fecha_salida').hide();
  $('#cbo-generar_salida').html('<option value="0">NO</option>');
}// add orden de ingreso

function verOrdenIngreso(ID, No_Imagen, No_Imagen_Url) {

// REQ-3 - INICIO 

$( "#txt-Fe_Emision" ).prop( "disabled", true );
$( "#txt-Fe_Entrega" ).prop( "disabled", true );

// REQ-3 - FIN




  accion_orden_venta = 'upd_orden_venta';
  $('#modal-loader').modal('show');

  $('.div-Listar').hide();

  $('#txt-EID_Empresa').focus();

  $('#form-OrdenIngreso')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('#radio-cliente_existente').prop('checked', true).iCheck('update');
  $('#radio-cliente_nuevo').prop('checked', false).iCheck('update');
  $('.div-cliente_existente').show();
  $('.div-cliente_nuevo').hide();

  $('#radio-contacto_existente').prop('checked', true).iCheck('update');
  $('#radio-contacto_nuevo').prop('checked', false).iCheck('update');
  $('.div-contacto_existente').show();
  $('.div-contacto_nuevo').hide();

  $('[name="ENu_Estado"]').val('');

  $('#btn-save').attr('disabled', false);

  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post(url, function (response) {
    $('#cbo-TiposDocumentoIdentidadCliente').html('');
    for (var i = 0; i < response.length; i++)
      $('#cbo-TiposDocumentoIdentidadCliente').append('<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>');
  }, 'JSON');


  url = base_url + 'Ventas/OrdenIngresoController/ajax_edit/' + ID;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      console.log(response);
      response = response[0];
      $('.div-AgregarEditar').show();

      $('.title_OrdenIngreso').text('Modifcar OrdenIngreso');

      $('[name="Nu_Estado_Facturacion"]').val(response.Nu_Estado_Facturacion);
      $('[name="EID_Orden_Ingreso"]').val(response.ID_Orden_Ingreso);
      $('[name="sTypeAction"]').val('upd');
      if ($('[name="sTypeAction"]').val() == 'upd')
        $('.div-fecha_salida').show();

      //Datos Documento
      $('[name="Fe_Emision"]').val(ParseDateString(response.Fe_Emision, 6, '-'));

      $('#txt-Fe_Emision').datepicker({}).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#txt-Fe_Entrega').datepicker('setStartDate', minDate);
        $('#txt-Fe_Entrega_Tentativa').datepicker('setStartDate', minDate);
      });

      var Fe_Emision = response.Fe_Emision.split('-');
      $('#txt-Fe_Entrega').datepicker({
        autoclose: true,
        startDate: new Date(parseInt(Fe_Emision[0]), parseInt(Fe_Emision[1]) - 1, parseInt(Fe_Emision[2])),
        todayHighlight: true
      })

      $('#txt-Fe_Entrega').datepicker('setStartDate', new Date(Fe_Emision[0] + "/" + Fe_Emision[1] + "/" + Fe_Emision[2]));
      $('#txt-Fe_Entrega').val(ParseDateString(response.Fe_Entrega, 6, '-'));

   
// 25_08 - INICIO 

//console.log(Fe_Emision[0] +'-'+Fe_Emision[1]+'-'+Fe_Emision[2]);
//console.log(response.Fe_Entrega_Tentativa+'... ');

let date = new Date()
let day = date.getDate()
let month = date.getMonth() + 1
let year = date.getFullYear()
$('#txt-Fe_Entrega_Tentativa').datepicker({
  autoclose: true,
  startDate: new Date(parseInt(year), parseInt(month), parseInt(day)),
  todayHighlight: true
})
$('#txt-Fe_Entrega_Tentativa').datepicker('setStartDate', new Date(year + "/" + month + "/" + day));
$('#txt-Fe_Entrega_Tentativa').val(ParseDateString(year +'-'+month+'-'+day, 6, '-'));

$( "#txt-Fe_Entrega_Tentativa" ).prop( "disabled", true );

// 25_08 - FIN 

      $('.div-fecha_salida').hide();
      $('#cbo-generar_salida').html('');
      var selected = '';
      for (var i = 0; i < 2; i++) {
        selected = '';
        if (response.Fe_Entrega_Tentativa != '0000-00-00') {
          selected = 'selected="selected"';
          $('.div-fecha_salida').show();
        }
        $('#cbo-generar_salida').append('<option value="' + i + '" ' + selected + '>' + (i == 0 ? 'NO' : 'SI') + '</option>');
      }

      var arrParams = {
        iIdComboxAlmacen: 'cbo-almacen',
        ID_Almacen: response.ID_Almacen,
      }
      getAlmacenes(arrParams);

      url = base_url + 'HelperController/getTiposDocumentos';
      $.post(url, { Nu_Tipo_Filtro: 13 }, function (responseAreasIngreso) {
        $('#cbo-area_ingreso').html('<option value="0" selected="selected">- Seleccionar -</option>');
        for (var i = 0; i < responseAreasIngreso.length; i++) {
          selected = '';
          if (response.ID_Area_Ingreso == responseAreasIngreso[i].ID_Tipo_Documento)
            selected = 'selected="selected"';
          $('#cbo-area_ingreso').append('<option value="' + responseAreasIngreso[i].ID_Tipo_Documento + '" ' + selected + '>' + responseAreasIngreso[i].No_Tipo_Documento_Breve + '</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getSeriesDocumentoOficinaPuntoVenta';
      $.post(url, { ID_Tipo_Documento: response.ID_Area_Ingreso }, function (responseSerieDocumentos) {
        var l = responseSerieDocumentos.length;
        for (var i = 0; i < l; i++) {
          selected = '';
          if (response.ID_Serie_Documento == responseSerieDocumentos[i].ID_Serie_Documento_PK)
            selected = 'selected="selected"';
          $('#cbo-serie_documento').append('<option value="' + responseSerieDocumentos[i].ID_Serie_Documento_PK + '" data-id_serie_documento="' + responseSerieDocumentos[i]['ID_Serie_Documento'] + '" ' + selected + '>' + responseSerieDocumentos[i].ID_Serie_Documento + '</option>');
        }
      }, 'JSON');

      $('#txt-ID_Numero_Documento').val(response.ID_Numero_Documento);
      $('[name="ID_Presupesto_Existe"]').val(response.ID_Presupuesto);
      $('[name="Nu_Estado"]').val(response.Nu_Estado);

      //CLIENTE
      $('#txt-AID').val(response.ID_Entidad);
      $('#txt-ANombre').val(response.No_Entidad);
      $('#txt-ACodigo').val(response.Nu_Documento_Identidad);
      $('#txt-Txt_Direccion_Entidad').val(response.Txt_Direccion_Entidad);

      //VEHICULO
      $('#txt-ID_Placa').val(response.ID_Producto);
      $('#txt-No_Placa_Vehiculo_existe').val(response.No_Placa_Vehiculo);
      $('#txt-marca_existe').val(response.No_Marca_Vehiculo);
      $('#txt-modelo_existe').val(response.No_Modelo_Vehiculo);
      $('#txt-color_existe').val(response.No_Color_Vehiculo);

      url = base_url + 'HelperController/getValoresTablaDato';
      $.post(url, { sTipoData: 'Orden_Ingreso_Niveles_Combustible' }, function (responseNivelCombustible) {
        $('#cbo-nivel_combustible').html('<option value="0" selected="selected">- Seleccionar -</option>');
        if (responseNivelCombustible.sStatus == 'success') {
          var l = responseNivelCombustible.arrData.length;
          $('#cbo-nivel_combustible').html('<option value="0" selected="selected">- Seleccionar -</option>');
          for (var x = 0; x < l; x++) {
            selected = '';
            if (response.No_Nivel_Combustible == responseNivelCombustible.arrData[x].ID_Tabla_Dato)
              selected = 'selected="selected"';
            $('#cbo-nivel_combustible').append('<option value="' + responseNivelCombustible.arrData[x].ID_Tabla_Dato + '" ' + selected + '>' + responseNivelCombustible.arrData[x].No_Descripcion + '</option>');
          }
        } else {
          if (responseNivelCombustible.sMessageSQL !== undefined) {
            console.log(responseNivelCombustible.sMessageSQL);
          }
          if (responseNivelCombustible.sStatus == 'warning')
            $('#cbo-nivel_combustible').html('<option value="0" selected="selected">- Vacío -</option>');
        }
      }, 'JSON');

      $('[name="No_Kilometraje"]').val(response.No_Kilometraje);
      $('[name="Txt_Reparacion"]').val(response.Txt_Reparacion);

      $('#modal-loader').modal('hide');
    }
  })// ./AJAX edit

  /* obtener imagen guardada(s) */
  $('.divDropzone').html(
    '<div id="id-divDropzone" class="dropzone div-dropzone">'
    + '<div class="dz-message">'
    + 'Arrastrar o presionar click para subir imágen'
    + '</div>'
    + '</div>'
  );

  Dropzone.autoDiscover = false;
  Dropzone.prototype.defaultOptions.dictDefaultMessage = "Arrastrar o presionar click para subir imágen";
  Dropzone.prototype.defaultOptions.dictFallbackMessage = "Tu navegador no soporta la función arrastrar la imágen";
  Dropzone.prototype.defaultOptions.dictFileTooBig = "La imágen pesa ({{filesize}}MiB). El tamaño máximo es: {{maxFilesize}}MiB.";
  Dropzone.prototype.defaultOptions.dictInvalidFileType = "Solo se permite imágenes / pdf";
  Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancelar";
  Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "¿Estás seguro de cancelar la subida?";
  Dropzone.prototype.defaultOptions.dictRemoveFile = "Eliminar";
  Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Solo se puede subir 1 imágen / pdf";

  url = base_url + 'Ventas/OrdenIngresoController/uploadOnly/' + ID;
  var myDropzone = new Dropzone("#id-divDropzone", {
    url: url,
    params: {
      iVersionImage: 0,
      iId: ID,
    },
    acceptedFiles: ".jpeg,.jpg,.png,.svg,.pdf",
    addRemoveLinks: true,
    uploadMultiple: false,
    maxFilesize: 1,//Peso en MB
    maxFiles: 1,
    thumbnailHeight: 200,
    thumbnailWidth: 200,
    parallelUploads: 1,
    thumbnail: function (file, dataUrl) {
      if (file.previewElement) {
        file.previewElement.classList.remove("dz-file-preview");
        var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
        for (var i = 0; i < images.length; i++) {
          var thumbnailElement = images[i];
          thumbnailElement.alt = file.name;
          thumbnailElement.src = dataUrl;
        }
        setTimeout(function () { file.previewElement.classList.add("dz-image-preview"); }, 1);
      }
    },
    removedfile: function (file) {
      var arrName = file.name.split('/');
      var nameFileImage;
      if (arrName.length === 4)//Si la imagen ya está en el server
        nameFileImage = arrName[3];
      else//Si la imagén recién la vamos a subir y no existe en el server
        nameFileImage = arrName[0];
      url = base_url + 'Ventas/OrdenIngresoController/removeFileImage';
      $.ajax({
        url: url,
        type: "POST",
        dataType: "JSON",
        data: { nameFileImage: nameFileImage },
      })
      var previewElement;
      return (previewElement = file.previewElement) != null ? (previewElement.parentNode.removeChild(file.previewElement)) : (void 0);
    },
    init: function () {
      //Verificar respuesta del servidor al subir archivo
      this.on("success", function (file, response) {
        var response = jQuery.parseJSON(response);

        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.sStatus != 'error') {
          $('.modal-message').addClass(response.sClassModal);
          $('.modal-title-message').text(response.sMessage);

          $('[name="No_Imagen"]').val(response.sNombreImagen);
          $('[name="No_Imagen_Url"]').val(response.sNombreImagenUrl);

          setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
        } else {
          $('.modal-message').addClass(response.sClassModal);
          $('.modal-title-message').text(response.sMessage);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
        }
      })

      var me = this;
      url = base_url + 'Ventas/OrdenIngresoController/get_image';
      var arrPost = {
        'sNombreImage': No_Imagen,
        'sUrlImage': No_Imagen_Url,
      }
      $.post(url, arrPost, function (response) {
        $.each(response, function (key, value) {
          var mockfile = value;
          me.emit("addedfile", mockfile);
          me.emit("thumbnail", mockfile, No_Imagen_Url);
          me.emit("complete", mockfile);
        })
      }, 'json');
    }
  })
}

function eliminarOrdenIngreso(ID, Nu_Descargar_Inventario, accion_orden_venta) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('.modal-message-delete').removeClass('modal-danger modal-warning modal-success');
  $('.modal-message-delete').addClass('modal-danger');

  $('.modal-title-message-delete').text('¿Deseas eliminar la orden de venta?');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  accion_orden_venta = 'delete';

  $(document).bind('keydown', 'return', function () {
    if (accion_orden_venta == 'delete') {
      _eliminarOrdenIngreso($modal_delete, ID);
      accion_orden_venta = '';
    }
  });

  $('#btn-save-delete').off('click').click(function () {
    _eliminarOrdenIngreso($modal_delete, ID, Nu_Descargar_Inventario);
    accion_orden_venta = '';
  });
}

$(function () {
  $('[data-mask]').inputmask();

  $("#myInput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("#table-OrdenIngreso tr").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  $('#cbo-filtro_nivel_combustible').html('<option value="0" selected="selected">- Todos -</option>');
  url = base_url + 'HelperController/getValoresTablaDato';
  $.post(url, { sTipoData: 'Orden_Ingreso_Niveles_Combustible' }, function (response) {
    if (response.sStatus == 'success') {
      var l = response.arrData.length;
      $('#cbo-filtro_nivel_combustible').html('<option value="0" selected="selected">- Todos -</option>');
      for (var x = 0; x < l; x++) {
        $('#cbo-filtro_nivel_combustible').append('<option value="' + response.arrData[x].ID_Tabla_Dato + '">' + response.arrData[x].No_Descripcion + '</option>');
      }
    } else {
      if (response.sMessageSQL !== undefined) {
        console.log(response.sMessageSQL);
      }
      if (response.sStatus == 'warning')
        $('#cbo-filtro_nivel_combustible').html('<option value="0" selected="selected">- Vacío -</option>');
    }
  }, 'JSON');

  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $('#radio-cliente_existente').on('ifChecked', function () {
    $('.div-cliente_existente').show();
    $('.div-cliente_nuevo').hide();
  })

  $('#radio-cliente_nuevo').on('ifChecked', function () {
    $('.div-cliente_existente').hide();
    $('.div-cliente_nuevo').show();
  })

  $('#radio-contacto_existente').on('ifChecked', function () {
    $('.div-contacto_existente').show();
    $('.div-contacto_nuevo').hide();
  })

  $('#radio-contacto_nuevo').on('ifChecked', function () {
    $('.div-contacto_existente').hide();
    $('.div-contacto_nuevo').show();
  })

  //LAE API SUNAT / RENIEC - CLIENTE
  $('#btn-cloud-api_orden_venta_cliente').click(function () {
    if ($('#cbo-TiposDocumentoIdentidadCliente').val().length === 0) {
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').find('.help-block').html('Seleccionar tipo doc. identidad');
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') != $('#txt-Nu_Documento_Identidad_Cliente').val().length) {
      $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').find('.help-block').html('Debe ingresar ' + $('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') + ' dígitos');
      $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if (
      (
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 1 ||
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 3 ||
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 5 ||
        $('#cbo-TiposDocumentoIdentidadCliente').val() == 6
      )
    ) {
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').find('.help-block').html('Disponible DNI / RUC');
      $('#cbo-TiposDocumentoIdentidadCliente').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $('#btn-cloud-api_orden_venta_cliente').text('');
      $('#btn-cloud-api_orden_venta_cliente').attr('disabled', true);
      $('#btn-cloud-api_orden_venta_cliente').append('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      // Obtener datos de SUNAT y RENIEC
      var url_api = 'https://www.laesystems.com/librerias/sunat/partner/format/json/x-api-key/';
      if ($('#cbo-TiposDocumentoIdentidadCliente').val() == 2)//2=RENIEC
        url_api = 'https://www.laesystems.com/librerias/reniec/partner/format/json/x-api-key/';
      url_api = url_api + sTokenGlobal;

      var data = {
        ID_Tipo_Documento_Identidad: $('#cbo-TiposDocumentoIdentidadCliente').val(),
        Nu_Documento_Identidad: $('#txt-Nu_Documento_Identidad_Cliente').val(),
      };

      $.ajax({
        url: url_api,
        type: 'POST',
        data: data,
        success: function (response) {
          $('#btn-cloud-api_orden_venta_cliente').closest('.form-group').find('.help-block').html('');
          $('#btn-cloud-api_orden_venta_cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

          if (response.success == true) {
            $('#txt-No_Entidad_Cliente').val(response.data.No_Names);
            if ($('#cbo-TiposDocumentoIdentidadCliente').val() == 4) {//RUC
              $('#txt-Txt_Direccion_Entidad_Cliente').val(response.data.Txt_Address);
              $('#txt-Nu_Telefono_Entidad_Cliente').val(response.data.Nu_Phone);
              $('#txt-Nu_Celular_Entidad_Cliente').val(response.data.Nu_Cellphone);
            }
          } else {
            $('#txt-No_Entidad_Cliente').val('');
            if ($('#cbo-TiposDocumentoIdentidadCliente').val() == 4) {//RUC
              $('#txt-Txt_Direccion_Entidad_Cliente').val('');
              $('#txt-Nu_Telefono_Entidad_Cliente').val('');
              $('#txt-Nu_Celular_Entidad_Cliente').val('');
            }
            $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').find('.help-block').html(response.msg);
            $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

            $('#txt-Nu_Documento_Identidad_Cliente').focus();
            $('#txt-Nu_Documento_Identidad_Cliente').select();
          }

          $('#btn-cloud-api_orden_venta_cliente').text('');
          $('#btn-cloud-api_orden_venta_cliente').attr('disabled', false);
          $('#btn-cloud-api_orden_venta_cliente').append('<i class="fa fa-cloud-download fa-lg"></i>');
        },
        error: function (response) {
          $('#btn-cloud-api_orden_venta_cliente').closest('.form-group').find('.help-block').html('Sin acceso');
          $('#btn-cloud-api_orden_venta_cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

          $('#txt-No_Entidad_Cliente').val('');
          $('#txt-Txt_Direccion_Entidad_Cliente').val('');
          $('#txt-Nu_Telefono_Entidad_Cliente').val('');
          $('#txt-Nu_Celular_Entidad_Cliente').val('');

          $('#btn-cloud-api_orden_venta_cliente').text('');
          $('#btn-cloud-api_orden_venta_cliente').attr('disabled', false);
          $('#btn-cloud-api_orden_venta_cliente').append('<i class="fa fa-cloud-download fa-lg"></i>');
        }
      });
    }
  })

  /* Tipo Documento Identidad Cliente */
  $('#cbo-TiposDocumentoIdentidadCliente').change(function () {
    if ($(this).val() == 2) {//DNI
      $('#label-Nombre_Documento_Identidad_Cliente').text('DNI');
      $('#label-No_Entidad_Cliente').text('Nombre(s) y Apellidos');
      $('#txt-Nu_Documento_Identidad_Cliente').attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
    } else if ($(this).val() == 4) {//RUC
      $('#label-Nombre_Documento_Identidad_Cliente').text('RUC');
      $('#label-No_Entidad_Cliente').text('Razón Social');
      $('#txt-Nu_Documento_Identidad_Cliente').attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
    } else {
      $('#label-Nombre_Documento_Identidad_Cliente').text('# Documento Identidad');
      $('#label-No_Entidad_Cliente').text('Nombre(s) y Apellidos');
      $('#txt-Nu_Documento_Identidad_Cliente').attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
    }
  })

  $(document).on('click', '#btn-cloud-api_sunarp', function (event) {
    if ($('#txt-No_Placa_Vehiculo').val().length === 0) {
      $('#txt-No_Placa_Vehiculo').closest('.form-group').find('.help-block').html('Ingresar placa');
      $('#txt-No_Placa_Vehiculo').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $('#btn-cloud-api_sunarp').text('');
      $('#btn-cloud-api_sunarp').attr('disabled', true);
      $('#btn-cloud-api_sunarp').append('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      var url_api = 'https://laesystems.com/librerias_v3/v1/';
      var token = 'f778632f1016e627c3ec25b5a2ce9b34';
      var url = url_api + token + '/sat/sunarp/busca_placa';
      var arrParams = {
        'placa': $('#txt-No_Placa_Vehiculo').val(),
      }
      $.post(url, arrParams, function (response) {
        if (response.success == true) {
          $('[name="No_Year_Vehiculo"]').val(response.result[0].titulo_Anio);
          $('[name="No_Marca_Vehiculo"]').val(response.result[0].marca);
          $('[name="No_Modelo_Vehiculo"]').val(response.result[0].modelo);
          $('[name="No_Tipo_Combustible_Vehiculo"]').val(response.result[0].combustible);
          $('[name="No_Vin_Vehiculo"]').val(response.result[0].serie);
          $('[name="No_Motor"]').val(response.result[0].motor);
          $('[name="No_Color_Vehiculo"]').val(response.result[0].color);
        } else {
          alert(response.message);
        }
        console.log(response);
        $('#btn-cloud-api_sunarp').text('');
        $('#btn-cloud-api_sunarp').attr('disabled', false);
        $('#btn-cloud-api_sunarp').append('<i class="fa fa-cloud-download fa-lg"></i>');
      }, 'json')
        .fail(function (xhr, status, error) {
          $('#btn-cloud-api_sunarp').text('');
          $('#btn-cloud-api_sunarp').attr('disabled', false);
          $('#btn-cloud-api_sunarp').append('<i class="fa fa-cloud-download fa-lg"></i>');
        });
    }
  });

  url = base_url + 'HelperController/getOrganizaciones';
  $.post(url, function (response) {
    if (response.length == 1) {
      $('#cbo-filtro-organizacion').html('<option value="' + response[0].ID_Organizacion + '">' + response[0].No_Organizacion + '</option>');
    } else {
      $('#cbo-filtro-organizacion').html('<option value="" selected="selected">- Todos -</option>');
      for (var i = 0; i < response.length; i++)
        $('#cbo-filtro-organizacion').append('<option value="' + response[i].ID_Organizacion + '">' + response[i].No_Organizacion + '</option>');
    }
  }, 'JSON');

  url = base_url + 'Ventas/OrdenIngresoController/ajax_list';
  table_orden_venta = $('#table-OrdenIngreso').DataTable({
    'dom': 'B<"top">frt<"bottom"lp><"clear">',
    buttons: [{
      extend: 'excel',
      text: '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr: 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend: 'pdf',
      text: '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr: 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend: 'colvis',
      text: '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr: 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching': false,
    'bStateSave': true,
    'processing': true,
    'serverSide': true,
    'info': true,
    'autoWidth': false,
    'pagingType': 'full_numbers',
    'oLanguage': {
      'sInfo': 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu': '_MENU_',
      'sSearch': 'Buscar por: ',
      'sSearchPlaceholder': 'UPC / Nombre',
      'sZeroRecords': 'No se encontraron registros',
      'sInfoEmpty': 'No hay registros',
      'sLoadingRecords': 'Cargando...',
      'sProcessing': 'Procesando...',
      'oPaginate': {
        'sFirst': '<<',
        'sLast': '>>',
        'sPrevious': '<',
        'sNext': '>',
      },
    },
    'order': [],
    'ajax': {
      'url': url,
      'type': 'POST',
      'dataType': 'json',
      'data': function (data) {
        data.Filtro_Fe_Inicio = ParseDateString($('#txt-Filtro_Fe_Inicio').val(), 1, '/'),
          data.Filtro_Fe_Fin = ParseDateString($('#txt-Filtro_Fe_Fin').val(), 1, '/'),
          data.Filtro_area_ingreso = $('#cbo-Filtro_area_ingreso option:selected').val(),
          data.Filtro_serie_documento = $('#cbo-Filtro_serie_documento option:selected').val(),
          data.Filtro_NumeroDocumento = $('#txt-Filtro_NumeroDocumento').val(),
          data.Filtro_Estado = $('#cbo-Filtro_Estado option:selected').val(),
          data.Filtro_Estado_Facturacion = $('#cbo-Filtro_Estado_Facturacion option:selected').val(),
          data.Filtro_Entidad = $('#txt-Filtro_Entidad').val();
          data.Filtro_Placa = $('#txt-Filtro_Placa').val();
          data.Filtro_Marca = $('#txt-Filtro_Marca').val();
          data.Filtro_Modelo = $('#txt-Filtro_Modelo').val();
          data.Filtro_Kilometraje = $('#txt-Filtro_Kilometraje').val(),
          data.filtro_nivel_combustible = $('#cbo-filtro_nivel_combustible option:selected').val();
      },
    },
    'columnDefs': [{
      'className': 'text-center',
      'targets': 'no-sort',
      'orderable': false,
    },
    {
      'className': 'text-right',
      'targets': 'no-sort_right',
      'orderable': false,
    },
    {
      'className': 'text-left',
      'targets': 'no-sort_left',
      'orderable': false,
    },
    {
      'className': 'text-center',
      'targets': 'sort_center',
      'orderable': true,
    },
    {
      'className': 'text-right',
      'targets': 'sort_right',
      'orderable': true,
    },],
    'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
  });

  $('.dataTables_length').addClass('col-md-3');
  $('.dataTables_paginate').addClass('col-md-9');

  $('#btn-filter').click(function () {
    table_orden_venta.ajax.reload();
  });

  $('#form-OrdenIngreso').validate({
    rules: {
      Fe_Emision: {
        required: true,
      },
      Fe_Vencimiento: {
        required: true,
      },
      Fe_Entrega: {
        required: true,
      },
      No_Contacto: {
        required: true,
      },
      Txt_Email_Contacto: {
        required: true,
        validemail: true,
      },
      Nu_Telefono_Contacto: {
        minlength: 8,
        maxlength: 8
      },
      Nu_Celular_Contacto: {
        minlength: 11,
        maxlength: 11
      },
    },
    messages: {
      Fe_Emision: {
        required: "Ingresar F. Emisión",
      },
      Fe_Vencimiento: {
        required: "Ingresar F. Vencimiento",
      },
      Fe_Entrega: {
        required: "Ingresar F. Entrega",
      },
      No_Contacto: {
        required: "Ingresar nombre",
      },
      Txt_Email_Contacto: {
        required: "Ingresar correo",
        validemail: "Ingresar correo válido",
      },
      Nu_Telefono_Contacto: {
        minlength: "Debe ingresar 7 dígitos",
        maxlength: "Debe ingresar 7 dígitos"
      },
      Nu_Celular_Contacto: {
        minlength: "Debe ingresar 9 dígitos",
        maxlength: "Debe ingresar 9 dígitos"
      },
    },
    errorPlacement: function (error, element) {
      $(element).closest('.form-group').find('.help-block').html(error.html());
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.form-group').find('.help-block').html('');
    },
    submitHandler: form_OrdenIngreso
  });

  $('#cbo-descargar_stock').change(function () {
    $('.div-almacen').hide();
    if ($(this).val() > 0) {
      $('.div-almacen').show();
      var arrParams = {
        iIdComboxAlmacen: 'cbo-almacen',
      };
      getAlmacenes(arrParams);
    }
  })

  $('#cbo-generar_salida').change(function () {
    $('.div-fecha_salida').hide();
    if ($(this).val() == 1) {
      $('.div-fecha_salida').show();
    }
  })

  $('#cbo-Filtro_area_ingreso').html('<option value="0" selected="selected">- Todos -</option>');
  $('#cbo-Filtro_serie_documento').html('<option value="0" selected="selected">- Todos -</option>');

  url = base_url + 'HelperController/getTiposDocumentos';
  $.post(url, { Nu_Tipo_Filtro: 13 }, function (response) {
    $('#cbo-Filtro_area_ingreso').html('<option value="0" selected="selected">- Todos -</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-Filtro_area_ingreso').append('<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>');
  }, 'JSON');

  $('#cbo-Filtro_area_ingreso').change(function () {
    $('#cbo-Filtro_serie_documento').html('<option value="0" selected="selected">- Seleccionar -</option>');
    if ($(this).val() > 0) {
      url = base_url + 'HelperController/getSeriesDocumentoOficinaPuntoVenta';
      $.post(url, { ID_Tipo_Documento: $(this).val() }, function (response) {
        var l = response.length;
        for (var i = 0; i < l; i++) {
          $('#cbo-Filtro_serie_documento').append('<option value="' + response[i].ID_Serie_Documento_PK + '">' + response[i].ID_Serie_Documento + '</option>');
        }
      }, 'JSON');
    }
  })

  $('#cbo-area_ingreso').change(function () {
    $('#cbo-serie_documento').html('<option value="0" selected="selected">- Seleccionar -</option>');
    if ($(this).val() > 0) {
      url = base_url + 'HelperController/getSeriesDocumentoOficinaPuntoVenta';
      $.post(url, { ID_Tipo_Documento: $(this).val() }, function (response) {
        var l = response.length;
        for (var i = 0; i < l; i++) {
          $('#cbo-serie_documento').append('<option value="' + response[i].ID_Serie_Documento_PK + '" data-id_serie_documento="' + response[i]['ID_Serie_Documento'] + '">' + response[i].ID_Serie_Documento + '</option>');
        }
      }, 'JSON');
    }
  })

  $('#cbo-serie_documento').change(function () {
    $('#txt-ID_Numero_Documento').val('');
    if ($(this).val() != '') {
      url = base_url + 'HelperController/getNumeroDocumento';
      $.post(url, { ID_Organizacion: $('#header-a-id_organizacion').val(), ID_Tipo_Documento: $('#cbo-area_ingreso').val(), ID_Serie_Documento: $('#cbo-serie_documento').find(':selected').data('id_serie_documento') }, function (response) {
        if (response.length == 0)
          $('#txt-ID_Numero_Documento').val('');
        else
          $('#txt-ID_Numero_Documento').val(response.ID_Numero_Documento);
      }, 'JSON');
    }
  })
})

function form_OrdenIngreso() {
  if (accion_orden_venta == 'add_orden_venta' || accion_orden_venta == 'upd_orden_venta') {
    bEstadoValidacion = validatePreviousDocumentToSaveOrderSale();
    if (bEstadoValidacion) {
      var arrOrdenIngresoCabecera = Array();
      var iEstado = 1;
      if ($('#cbo-generar_salida').val() == 1) {
        iEstado = 2;
      }
      if ($('[name="Nu_Estado"]').val() == '3') {
        iEstado = 3;
      }

      arrOrdenIngresoCabecera = {
        'EID_Orden_Ingreso': $('[name="EID_Orden_Ingreso"]').val(),
        'Fe_Emision': $('#txt-Fe_Emision').val(),
        'Fe_Entrega': $('#txt-Fe_Entrega').val(),
        'Fe_Entrega_Tentativa': $('#txt-Fe_Entrega_Tentativa').val(),
        'ID_Area_Ingreso': $('#cbo-area_ingreso').val(),
        'ID_Serie_Documento': $('#cbo-serie_documento').val(),
        'ID_Numero_Documento': $('#txt-ID_Numero_Documento').val(),
        'Nu_Descargar_Inventario': $('#cbo-descargar_stock').val(),
        'ID_Almacen': $('#cbo-almacen').val(),
        'ID_Entidad': $('#txt-AID').val(),
        'ID_Producto': $('#txt-ID_Placa').val(),
        'No_Nivel_Combustible': $('#cbo-nivel_combustible').val(),
        'No_Kilometraje': $('[name="No_Kilometraje"]').val(),
        'Txt_Reparacion': $('[name="Txt_Reparacion"]').val(),
        //'ID_Origen_Tabla': $('[name="ID_Presupesto_Existe"]').val(),//cambiuado el 04/02/2021
        'ID_Origen_Tabla': '',
        'No_Imagen': $('[name="No_Imagen"]').val(),
        'No_Imagen_Url': $('[name="No_Imagen_Url"]').val(),
        'Nu_Estado': iEstado,//Agregado el 10/12/2020
        'Nu_Estado_Facturacion': $('[name="Nu_Estado_Facturacion"]').val(),
      };

      var arrClienteNuevo = {};
      if ($('[name="addCliente"]:checked').attr('value') == 1) {//Agregar cliente
        arrClienteNuevo = {
          'ID_Tipo_Documento_Identidad': $('#cbo-TiposDocumentoIdentidadCliente').val(),
          'Nu_Documento_Identidad': $('#txt-Nu_Documento_Identidad_Cliente').val(),
          'No_Entidad': $('#txt-No_Entidad_Cliente').val(),
          'Txt_Direccion_Entidad': $('#txt-Txt_Direccion_Entidad_Cliente').val(),
          'Nu_Telefono_Entidad': $('#txt-Nu_Telefono_Entidad_Cliente').val(),
          'Nu_Celular_Entidad': $('#txt-Nu_Celular_Entidad_Cliente').val(),
        };
      }

      var arrVehiculoNuevo = {};
      if ($('[name="addVehiculo"]:checked').attr('value') == 1) {//Agregar contacto
        arrVehiculoNuevo = {
          'No_Placa_Vehiculo': $('#txt-No_Placa_Vehiculo').val(),
          'No_Year_Vehiculo': $('[name="No_Year_Vehiculo"]').val(),
          'No_Marca_Vehiculo': $('[name="No_Marca_Vehiculo"]').val(),
          'No_Modelo_Vehiculo': $('[name="No_Modelo_Vehiculo"]').val(),
          'No_Tipo_Combustible_Vehiculo': $('[name="No_Tipo_Combustible_Vehiculo"]').val(),
          'No_Vin_Vehiculo': $('[name="No_Vin_Vehiculo"]').val(),
          'No_Motor': $('[name="No_Motor"]').val(),
          'No_Color_Vehiculo': $('[name="No_Color_Vehiculo"]').val(),
        };
      }

      $('#btn-save').text('');
      $('#btn-save').attr('disabled', true);
      $('#btn-save').append('Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      $('#modal-loader').modal('show');

      url = base_url + 'Ventas/OrdenIngresoController/crudOrdenIngreso';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: {
          arrOrdenIngresoCabecera: arrOrdenIngresoCabecera,
          arrClienteNuevo: arrClienteNuevo,
          arrVehiculoNuevo: arrVehiculoNuevo,
        },
        success: function (response) {
          $('#modal-loader').modal('hide');

          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.status == 'success') {
            accion_orden_venta = '';

            $('#form-OrdenIngreso')[0].reset();
            $('.div-AgregarEditar').hide();
            $('.div-Listar').show();
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
            reload_table_orden_venta();
          } else {
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
          }

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar (ENTER)');
          $('#btn-save').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          $('#modal-loader').modal('hide');
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');

          $('#modal-message').modal('show');
          $('.modal-message').addClass('modal-danger');
          $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

          //Message for developer
          console.log(jqXHR.responseText);

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar (ENTER)');
          $('#btn-save').attr('disabled', false);
        }
      });
    }
  }
}

function reload_table_orden_venta() {
  table_orden_venta.ajax.reload(null, false);
}

function _eliminarOrdenIngreso($modal_delete, ID, Nu_Descargar_Inventario) {
  $('#modal-loader').modal('show');

  url = base_url + 'Ventas/OrdenIngresoController/eliminarOrdenIngreso/' + ID + '/' + Nu_Descargar_Inventario;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      $('#modal-loader').modal('hide');

      $modal_delete.modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');
      $('#modal-message').modal('show');

      if (response.status == 'success') {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text(response.message);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
        reload_table_orden_venta();
      } else {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text(response.message);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
      }
      accion_orden_venta = '';
    },
    error: function (jqXHR, textStatus, errorThrown) {
      accion_orden_venta = '';
      $('#modal-loader').modal('hide');
      $modal_delete.modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');

      $('#modal-message').modal('show');
      $('.modal-message').addClass('modal-danger');
      $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
      setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

      //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function validatePreviousDocumentToSaveOrderSale() {
  bEstadoValidacion = true;

  if ($('[name="addCliente"]:checked').attr('value') == 0 && ($('#txt-AID').val().length === 0 || $('#txt-ANombre').val().length === 0 || $('#txt-ACodigo').val().length === 0)) {
    $('#txt-ANombre').closest('.form-group').find('.help-block').html('Seleccionar cliente');
    $('#txt-ANombre').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#txt-ANombre'));
  } else if ($('#cbo-TiposDocumentoIdentidadCliente').val() != 1 && ($('[name="addCliente"]:checked').attr('value') == 1 && $('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') != $('#txt-Nu_Documento_Identidad_Cliente').val().length)) {
    $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').find('.help-block').html('Debe ingresar ' + $('#cbo-TiposDocumentoIdentidadCliente').find(':selected').data('nu_cantidad_caracteres') + ' dígitos');
    $('#txt-Nu_Documento_Identidad_Cliente').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#txt-Nu_Documento_Identidad_Cliente'));
  }
  return bEstadoValidacion;
}

// Ayudas - combobox
function getAlmacenes(arrParams) {
  url = base_url + 'HelperController/getAlmacenes';
  $.post(url, {}, function (responseAlmacen) {
    var iCantidadRegistros = responseAlmacen.length;
    var selected = '';
    var iIdAlmacen = 0;
    if (iCantidadRegistros == 1) {
      if (arrParams !== undefined) {
        iIdAlmacen = arrParams.ID_Almacen;
      }
      if (iIdAlmacen == responseAlmacen[0]['ID_Almacen']) {
        selected = 'selected="selected"';
      }
      $('#' + arrParams.iIdComboxAlmacen).html('<option value="' + responseAlmacen[0]['ID_Almacen'] + '" ' + selected + '>' + responseAlmacen[0]['No_Almacen'] + '</option>');
      var arrParamsListaPrecio = {
        ID_Almacen: responseAlmacen[0]['ID_Almacen'],
      }
    } else {
      $('#' + arrParams.iIdComboxAlmacen).html('<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < iCantidadRegistros; i++) {
        if (arrParams !== undefined) {
          iIdAlmacen = arrParams.ID_Almacen;
        }
        if (iIdAlmacen == responseAlmacen[0]['ID_Almacen']) {
          selected = 'selected="selected"';
        }
        $('#' + arrParams.iIdComboxAlmacen).append('<option value="' + responseAlmacen[i]['ID_Almacen'] + '" ' + selected + '>' + responseAlmacen[i]['No_Almacen'] + '</option>');
      }
    }
  }, 'JSON');
}

function enviarCorreo(ID, sCorreoUsuario, sCorreoContacto, sAsunto) {
  var $modal_id = $('#modal-orden_correo_sunat');
  $modal_id.modal('show');

  $modal_id.removeClass('modal-default modal-danger modal-warning modal-success');
  $modal_id.addClass('modal-default');

  $('#modal-header-orden-title').text('¿Enviar correo?');

  $('#txt-orden-email_correo_sunat_de').val(sCorreoUsuario);
  $('#txt-orden-email_correo_sunat_para').val(sCorreoContacto);
  $('#txt-orden-email_correo_sunat_asunto').val(sAsunto);

  $modal_id.on('shown.bs.modal', function () {
    $('#txt-orden-email_correo_sunat_para').focus();
  })

  $('#btn-modal-footer-orden_correo_sunat-cancel').off('click').click(function () {
    $modal_id.modal('hide');
  });

  $("#txt-orden-email_correo_sunat_de").blur(function () {
    caracteresCorreoValido($(this).val(), '#span-email');
  })

  $("#txt-orden-email_correo_sunat_para").blur(function () {
    caracteresCorreoValido($(this).val(), '#span-email');
  })

  $('#btn-modal-footer-orden_correo_sunat-send').off('click').click(function () {
    if (!caracteresCorreoValido($('#txt-orden-email_correo_sunat_de').val(), '#div-email')) {
      scrollToError($('#modal-correo_sunat .modal-body'), $('#txt-orden-email_correo_sunat_de'));
    } else if (!caracteresCorreoValido($('#txt-orden-email_correo_sunat_para').val(), '#div-email')) {
      scrollToError($('#modal-correo_sunat .modal-body'), $('#txt-orden-email_correo_sunat_para'));
    } else if ($('#txt-orden-email_correo_sunat_asunto').val().length === 0) {
      $('#txt-orden-email_correo_sunat_asunto').closest('.form-group').find('.help-block').html('Ingresar asunto');
      $('#txt-orden-email_correo_sunat_asunto').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('#modal-correo_sunat .modal-body'), $('#txt-orden-email_correo_sunat_asunto'));
    } else {
      $('#btn-modal-footer-orden_correo_sunat-cancel').attr('disabled', true);
      $('#btn-modal-footer-orden_correo_sunat-send').text('');
      $('#btn-modal-footer-orden_correo_sunat-send').attr('disabled', true);
      $('#btn-modal-footer-orden_correo_sunat-send').append('Enviando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      var sendPost = {
        iIdOrden: ID,
        sCorreoDe: $('#txt-orden-email_correo_sunat_de').val(),
        sCorreoPara: $('#txt-orden-email_correo_sunat_para').val(),
        sAsunto: $('#txt-orden-email_correo_sunat_asunto').val(),
      };

      url = base_url + 'Ventas/OrdenIngresoController/enviarCorreo';
      $.ajax({
        url: url,
        type: "POST",
        dataType: "JSON",
        data: sendPost,
        success: function (response) {
          $modal_id.modal('hide');

          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.status == 'success') {
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
          } else {
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1800);
          }

          $('#btn-modal-footer-orden_correo_sunat-cancel').attr('disabled', false);
          $('#btn-modal-footer-orden_correo_sunat-send').text('');
          $('#btn-modal-footer-orden_correo_sunat-send').attr('disabled', false);
          $('#btn-modal-footer-orden_correo_sunat-send').append('Enviar');
        }
      });
    }
  });
}


function generarPresupuesto(ID, iIdEntidad, iIdItem) {
  var $modal = $('.modal-orden');
  $modal.modal('show');

  $modal.on('shown.bs.modal', function () {
    $('.hidden-modal_orden').focus();
  })

  //Salir modal
  $('#btn-modal-salir-orden').off('click').click(function () {
    $modal.modal('hide');
  });
  //Fin Salir modal

  //Limpiar modal
  $('#div-modal-body-orden').empty();

  $(document).ready(function () {
    $('#cbo-modal-descargar_stock').html('<option value="0">No</option>');

    $('.div-almacen').hide();
    var arrParams = {
      'iIdComboxAlmacen': 'cbo-modal-almacen'
    };
    getAlmacenes(arrParams);

    url = base_url + 'HelperController/getTiposDocumentos';
    $.post(url, { Nu_Tipo_Filtro: 10 }, function (responseTiposDocumentoModal) {//8 = Cotizacion Venta
      $('#cbo-tipo_documento_modal').html('<option value="0" selected="selected">- Seleccionar -</option>');
      for (var i = 0; i < responseTiposDocumentoModal.length; i++)
        $('#cbo-tipo_documento_modal').append('<option value="' + responseTiposDocumentoModal[i].ID_Tipo_Documento + '" data-nu_impuesto="' + responseTiposDocumentoModal[i].Nu_Impuesto + '">' + responseTiposDocumentoModal[i].No_Tipo_Documento_Breve + '</option>');
    }, 'JSON');

    $('#cbo-tipo_documento_modal').change(function () {
      $('#cbo-serie_documento_modal').html('');
      $('#txt-ID_Numero_Documento_modal').val('');
      if ($(this).val() > 0) {
        url = base_url + 'HelperController/getSeriesDocumento';
        $.post(url, { ID_Organizacion: $('#header-a-id_organizacion').val(), ID_Tipo_Documento: $(this).val() }, function (responseSeriesDocumentoModal) {
          if (responseSeriesDocumentoModal.length == 0)
            $('#cbo-serie_documento_modal').html('<option value="0" selected="selected">Sin serie</option>');
          else {
            $('#cbo-serie_documento_modal').html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < responseSeriesDocumentoModal.length; i++)
              $('#cbo-serie_documento_modal').append('<option value="' + responseSeriesDocumentoModal[i].ID_Serie_Documento + '" data-id_serie_documento_pk=' + responseSeriesDocumentoModal[i].ID_Serie_Documento_PK + '>' + responseSeriesDocumentoModal[i].ID_Serie_Documento + '</option>');
          }
        }, 'JSON');
      }
    })

    $('#cbo-serie_documento_modal').change(function () {
      $('#txt-ID_Numero_Documento_modal').val('');
      if ($(this).val() != '') {
        url = base_url + 'HelperController/getNumeroDocumento';
        $.post(url, { ID_Organizacion: $('#header-a-id_organizacion').val(), ID_Tipo_Documento: $('#cbo-tipo_documento_modal').val(), ID_Serie_Documento: $(this).val() }, function (responseSeries2DocumentoModal) {
          if (responseSeries2DocumentoModal.length == 0)
            $('#txt-ID_Numero_Documento_modal').val('');
          else
            $('#txt-ID_Numero_Documento_modal').val(responseSeries2DocumentoModal.ID_Numero_Documento);
        }, 'JSON');
      }
    })

    url = base_url + 'HelperController/getMonedas';
    $.post(url, function (responseMonedasModal) {
      $('#cbo-Monedas_modal').html('');
      $('.span-signo').text(responseMonedasModal[0]['No_Signo']);
      for (var i = 0; i < responseMonedasModal.length; i++)
        $('#cbo-Monedas_modal').append('<option value="' + responseMonedasModal[i]['ID_Moneda'] + '" data-no_signo="' + responseMonedasModal[i]['No_Signo'] + '">' + responseMonedasModal[i]['No_Moneda'] + '</option>');
    }, 'JSON');

    url = base_url + 'HelperController/getMediosPago';
    $.post(url, function (responseMediosPagoModal) {
      $('#cbo-MediosPago_modal').html('');
      for (var i = 0; i < responseMediosPagoModal.length; i++)
        $('#cbo-MediosPago_modal').append('<option value="' + responseMediosPagoModal[i]['ID_Medio_Pago'] + '" data-nu_tipo="' + responseMediosPagoModal[i]['Nu_Tipo'] + '">' + responseMediosPagoModal[i]['No_Medio_Pago'] + '</option>');
    }, 'JSON');
  });// document-ready

  var html_orden_cabecera = '';
  html_orden_cabecera +=
    '<div class="row">'
    + '<input type="hidden" id="txt-modal-ID_Orden_Ingreso" value="' + ID + '">'
    + '<input type="hidden" id="txt-modal-ID_Entidad" value="' + iIdEntidad + '">'
    + '<input type="hidden" id="txt-modal-ID_Producto" value="' + iIdItem + '">'
    + '<div class="col-sm-12 col-md-12">'
    + '<div class="panel panel-default">'
    + '<div class="panel-heading"><i class="fa fa-book"></i> <b>Generar Presupuesto</b></div>'
    + '<div class="panel-body">'
    + '<div class="row">'
    + '<div class="col-xs-7 col-sm-3 col-md-2">'
    + '<div class="form-group">'
    + '<label>Tipo <span class="label-advertencia">*</span></label>'
    + '<select id="cbo-tipo_documento_modal" class="form-control required" style="width: 100%;"></select>'
    + '<span class="help-block" id="error"></span>'
    + '</div>'
    + '</div>'
    + '<div class="col-xs-5 col-sm-3 col-md-2">'
    + '<div class="form-group">'
    + '<label>Series <span class="label-advertencia">*</span></label>'
    + '<select id="cbo-serie_documento_modal" class="form-control required" style="width: 100%;"></select>'
    + '<span class="help-block" id="error"></span>'
    + '</div>'
    + '</div>'
    + '<div class="col-xs-5 col-sm-3 col-md-2">'
    + '<div class="form-group">'
    + '<label>Número <span class="label-advertencia">*</span></label>'
    + '<input type="tel" id="txt-ID_Numero_Documento_modal" name="ID_Numero_Documento_modal" class="form-control required input-number" disabled>'
    + '<span class="help-block" id="error"></span>'
    + '</div>'
    + '</div>'
    + '<div class="col-xs-6 col-sm-3 col-md-2">'
      + '<div class="form-group">'
        + '<label>F. Emisión <span class="label-advertencia">*</span></label>'
        + '<div class="input-group date">'
          + '<input type="text" id="txt-Fe_Emision_modal" name="Fe_Emision" class="form-control date-picker-invoice required">'
        + '</div>'
        + '<span class="help-block" id="error"></span>'
      + '</div>'
    + '</div>'
    + '<div class="col-xs-6 col-sm-3 col-md-2">'
      + '<div class="form-group">'
      + '<label>F. Vencimiento <span class="label-advertencia">*</span></label>'
      + '<div class="input-group date">'
      + '<input type="text" id="txt-Fe_Vencimiento_modal" name="Fe_Emision" class="form-control required">'
      + '</div>'
      + '<span class="help-block" id="error"></span>'
      + '</div>'
    + '</div>'
    + '<div class="col-xs-6 col-sm-3 col-md-2">'
      + '<div class="form-group">'
        + '<label>F. Entrega <span class="label-advertencia">*</span></label>'
        + '<div class="input-group date">'
          + '<input type="text" id="txt-Fe_Entrega_modal" name="Fe_Emision" class="form-control required">'
        + '</div>'
        + '<span class="help-block" id="error"></span>'
      + '</div>'
    + '</div>'
    + '<div class="col-xs-12 col-sm-4 col-md-3" style="display: none;">'
    + '<div class="form-group">'
    + '<label>PDF</label>'
    + '<select id="cbo-modal-formato_pdf" class="form-control required" style="width: 100%;">'
    + '<option value="A4" selected="selected">A4</option>'
    + '<option value="A5">A5</option>'
    + '<option value="TICKET">Ticket</option>'
    + '</select>'
    + '<span class="help-block" id="error"></span>'
    + '</div>'
    + '</div>'
    + '<div class="col-xs-12 col-sm-4 col-md-4" style="display: none;">'
    + '<div class="form-group">'
    + '<label>¿Descargar Stock?</label>'
    + '<select id="cbo-modal-descargar_stock" class="form-control required" style="width: 100%;"></select>'
    + '<span class="help-block" id="error"></span>'
    + '</div>'
    + '</div>'
    + '<div class="col-xs-12 col-sm-8 col-md-6">'
      + '<div class="form-group">'
        + '<label>Almacen</label>'
        + '<select id="cbo-modal-almacen" class="form-control required" style="width: 100%;"></select>'
        + '<span class="help-block" id="error"></span>'
      + '</div>'
    + '</div>'
    + '<div class="col-xs-12 col-sm-8 col-md-3">'
    + '<div class="form-group">'
    + '<label>Moneda</label>'
    + '<select id="cbo-Monedas_modal" class="form-control required" style="width: 100%;"></select>'
    + '<span class="help-block" id="error"></span>'
    + '</div>'
    + '</div>'
    + '<div class="col-xs-12 col-sm-8 col-md-3">'
    + '<div class="form-group">'
    + '<label>Medio de Pago</label>'
    + '<select id="cbo-MediosPago_modal" class="form-control required" style="width: 100%;"></select>'
    + '<span class="help-block" id="error"></span>'
    + '</div>'
    + '</div>'
    + '</div>'
    + '</div>'
    + '</div>'
    + '</div>'
    + '</div>'
    ;

  $('#div-modal-body-orden').append(html_orden_cabecera);
  $('.date-picker-invoice').datepicker({
    autoclose: true,
    endDate: new Date(fYear, fToday.getMonth(), fDay),
    todayHighlight: true
  })
  $('.date-picker-invoice').val(fDay + '/' + fMonth + '/' + fYear);

  $('#txt-Fe_Emision_modal').datepicker({}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#txt-Fe_Vencimiento_modal').datepicker('setStartDate', minDate);
    $('#txt-Fe_Entrega_modal').datepicker('setStartDate', minDate);
  });

  var Fe_Emision = $('#txt-Fe_Emision_modal').val().split('/');
  $('#txt-Fe_Vencimiento_modal').datepicker({
    autoclose: true,
    startDate: new Date(Fe_Emision[2], Fe_Emision[1] - 1, Fe_Emision[0]),
    todayHighlight: true
  })

  $('#txt-Fe_Entrega_modal').datepicker({
    autoclose: true,
    startDate: new Date(Fe_Emision[2], Fe_Emision[1] - 1, Fe_Emision[0]),
    todayHighlight: true
  })

  $('#txt-Fe_Vencimiento_modal').val($('#txt-Fe_Emision_modal').val());
  $('#txt-Fe_Entrega_modal').val($('#txt-Fe_Emision_modal').val());

  $('#btn-modal-facturar-orden').off('click').click(function () {
    accion_orden_venta = 'add_orden_ingreso_presupuesto_modal';
    addVentaPresupuesto();
  });
}

function addVentaPresupuesto() {
  if (accion_orden_venta == 'add_orden_ingreso_presupuesto_modal') {
    var arrDetalleCompra = [];

    if ($('#cbo-tipo_documento_modal').val() == 0) {
      $('#cbo-tipo_documento_modal').closest('.form-group').find('.help-block').html('Seleccionar tipo');
      $('#cbo-tipo_documento_modal').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('.modal-orden .modal-body'), $('#cbo-tipo_documento_modal'));
    } else if ($('#cbo-serie_documento_modal').val() == 0) {
      $('#cbo-serie_documento_modal').closest('.form-group').find('.help-block').html('Seleccionar serie');
      $('#cbo-serie_documento_modal').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('.modal-orden .modal-body'), $('#cbo-serie_documento_modal'));
    } else {
      var arrOrdenVentaCabecera = Array(), arrDetalleOrdenVenta = Array();
      arrOrdenVentaCabecera = {
        'ID_Origen_Tabla': $('#txt-modal-ID_Orden_Ingreso').val(),
        'EID_Empresa': '',
        'EID_Documento_Cabecera': '',
        'ENu_Estado': '',
        'ID_Tipo_Documento': $('#cbo-tipo_documento_modal').val(),
        'ID_Serie_Documento': $('#cbo-serie_documento_modal').val(),
        'ID_Serie_Documento_PK': $('#cbo-serie_documento_modal').find(':selected').data('id_serie_documento_pk'),
        'ID_Numero_Documento': $('#txt-ID_Numero_Documento_modal').val(),
        'Fe_Emision': $('#txt-Fe_Emision_modal').val(),
        'Fe_Vencimiento': $('#txt-Fe_Vencimiento_modal').val(),
        'Fe_Entrega': $('#txt-Fe_Entrega_modal').val(),
        'ID_Moneda': $('#cbo-Monedas_modal').val(),
        'ID_Medio_Pago': $('#cbo-MediosPago_modal').val(),
        'Nu_Descargar_Inventario': 0,
        'ID_Entidad': $('#txt-modal-ID_Entidad').val(),
        'ID_Contacto': '',
        'Txt_Garantia': '',
        'Txt_Glosa': '',
        'Po_Descuento': '',
        'Ss_Descuento': '',
        'Ss_Total': '',
        'ID_Lista_Precio_Cabecera': 0,
        'ID_Mesero': '',
        'ID_Comision': '',
        'No_Formato_PDF': 'A4',
        'Nu_Descargar_Inventario': $('#cbo-modal-descargar_stock').val(),
        'ID_Almacen': $('#cbo-modal-almacen').val(),
        'ID_Producto': $('#txt-modal-ID_Producto').val(),
        'Nu_Tipo_Mantenimiento' : 1,
        'Txt_Resumen_Servicio_Presupuesto' : '',
      };

      var arrClienteNuevo = {}, arrContactoNuevo = {}, arrVehiculoNuevo = {}

      $('#btn-modal-facturar-orden').text('');
      $('#btn-modal-facturar-orden').attr('disabled', true);
      $('#btn-modal-facturar-orden').append('Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      $('#modal-loader').modal('show');
      $('#modal-loader').css("z-index", "3000");

      url = base_url + 'Ventas/OrdenVentaController/crudOrdenVenta';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: {
          arrOrdenVentaCabecera: arrOrdenVentaCabecera,
          arrDetalleOrdenVenta: arrDetalleOrdenVenta,
          arrClienteNuevo: arrClienteNuevo,
          arrContactoNuevo: arrContactoNuevo,
          arrVehiculoNuevo: arrVehiculoNuevo,
        },
        success: function (response) {
          $('#modal-loader').modal('hide');

          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');
          $('.modal-message').css("z-index", "4000");

          if (response.status == 'success') {
            accion_orden_venta = '';

            $('.modal-orden').modal('hide');
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
            reload_table_orden_venta();
          } else {
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            if (response.message_nubefact.length > 0)
              $('.modal-title-message').text(response.message_nubefact);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 4000);
          }

          $('#btn-modal-facturar-orden').text('');
          $('#btn-modal-facturar-orden').append('Facturar (ENTER)');
          $('#btn-modal-facturar-orden').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          $('#modal-loader').modal('hide');
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');

          $('#modal-message').modal('show');
          $('.modal-message').addClass('modal-danger');
          $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

          //Message for developer
          console.log(jqXHR.responseText);

          $('#btn-modal-facturar-orden').text('');
          $('#btn-modal-facturar-orden').append('<span class="fa fa-save"></span> Facturar (ENTER)');
          $('#btn-modal-facturar-orden').attr('disabled', false);
        }
      });
    }
  }
}

function cambiarEstadoFacturaOI(ID, Nu_Estado) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('.modal-message-delete').removeClass('modal-danger modal-warning modal-success');
  $('.modal-message-delete').addClass('modal-success');

  $('.modal-title-message-delete').text('¿Deseas cambiar el estado?');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $('#btn-save-delete').off('click').click(function () {
    $('#modal-loader').modal('show');

    url = base_url + 'Ventas/OrdenIngresoController/cambiarEstadoFacturaOI/' + ID + '/' + Nu_Estado;
    $.ajax({
      url: url,
      type: "GET",
      dataType: "JSON",
      success: function (response) {
        $('#modal-loader').modal('hide');

        $modal_delete.modal('hide');
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.status == 'success') {
          $('.modal-message').addClass(response.style_modal);
          $('.modal-title-message').text(response.message);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
          reload_table_orden_venta();
        } else {
          $('.modal-message').addClass(response.style_modal);
          $('.modal-title-message').text(response.message);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
        }
      }
    });
  });
}

function cambiarEstadoEtapaOI(ID, Nu_Estado) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('.modal-message-delete').removeClass('modal-danger modal-warning modal-success');
  $('.modal-message-delete').addClass('modal-success');

  $('.modal-title-message-delete').text('¿Deseas cambiar la etapa?');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $('#btn-save-delete').off('click').click(function () {
    $('#modal-loader').modal('show');

    url = base_url + 'Ventas/OrdenIngresoController/cambiarEstadoEtapaOI/' + ID + '/' + Nu_Estado;
    $.ajax({
      url: url,
      type: "GET",
      dataType: "JSON",
      success: function (response) {
        $('#modal-loader').modal('hide');

        $modal_delete.modal('hide');
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');

        if (response.status == 'success') {
          $('.modal-message').addClass(response.style_modal);
          $('.modal-title-message').text(response.message);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
          reload_table_orden_venta();
        } else {
          $('.modal-message').addClass(response.style_modal);
          $('.modal-title-message').text(response.message);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
        }
      }
    });
  });
}