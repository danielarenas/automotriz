var url;
var table_serie;
var accion_serie, value_importes_cero = 0.00, texto_importes_cero =0.00;

$(function () {
  $('.select2').select2();

	$(document).keyup(function(event){
    if(event.which == 27){//ESC
      $( "#modal-Serie" ).modal('hide');
    }
	});
	
  url = base_url + 'Ventas/PagoContratistaController/ajax_list';
  table_serie = $( '#table-Serie' ).DataTable({
    'dom'       : 'B<"top">frt<"bottom"lp><"clear">',
    buttons     : [{
      extend    : 'excel',
      text      : '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr : 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'pdf',
      text      : '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr : 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'colvis',
      text      : '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr : 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching'   : false,
    'bStateSave'  : true,
    'processing'  : true,
    'serverSide'  : true,
    'info'        : true,
    'autoWidth'   : false,
    'pagingType'  : 'full_numbers',
    'oLanguage' : {
      'sInfo'               : 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu'         : '_MENU_',
      'sSearch'             : 'Buscar por: ',
      'sSearchPlaceholder'  : 'UPC / Nombre',
      'sZeroRecords'        : 'No se encontraron registros',
      'sInfoEmpty'          : 'No hay registros',
      'sLoadingRecords'     : 'Cargando...',
      'sProcessing'         : 'Procesando...',
      'oPaginate'           : {
        'sFirst'    : '<<',
        'sLast'     : '>>',
        'sPrevious' : '<',
        'sNext'     : '>',
      },
    },
    'order': [],
    'ajax': {
      'url'       : url,
      'type'      : 'POST',
      'dataType'  : 'JSON',
      'data'      : function ( data ) {
        data.Filtro_Fe_Inicio = ParseDateString($('#txt-Filtro_Fe_Inicio').val(), 1, '/'),
        data.Filtro_Fe_Fin = ParseDateString($('#txt-Filtro_Fe_Fin').val(), 1, '/');
      },
    },
    'columnDefs': [{
      'className' : 'text-center',
      'targets'   : 'no-sort',
      'orderable' : false,
    },],
    'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
  });
  
  $( '.dataTables_length' ).addClass('col-md-3');
  $('.dataTables_paginate').addClass('col-md-9');
  
  $('#btn-filter').click(function () {
    table_serie.ajax.reload();
  });


  $('#form-PagoContratista').validate({
    rules: {
      Fe_Emision: {
        required: true,
      },
    },
    messages: {
      Fe_Emision: {
        required: "Ingresar F. Emisión",
      },
    },
    errorPlacement: function (error, element) {
      $(element).closest('.form-group').find('.help-block').html(error.html());
    },
    highlight: function (element) {
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
      $(element).closest('.form-group').find('.help-block').html('');
    },
    submitHandler: form_PagoContratista
  });

  $('#table-DetalleProductosPagoContratista tbody').on('input', '.txt-Ss_Precio', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0) {
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $('#table-DetalleProductosPagoContratista tfoot').empty();
      if (nu_tipo_impuesto == 1) {//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
        });
        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-impuesto').val($Ss_IGV.toFixed(2));
        $('#span-impuesto').text($Ss_IGV.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 2) {//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
        $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 3) {//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
        $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 4) {//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
        $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else {//Sin ningun tipo de impuesto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

        var $Ss_Subtotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (considerar_igv == 0)
            $Ss_Subtotal += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-subTotal').val($Ss_Subtotal.toFixed(2));
        $('#span-subTotal').text($Ss_Subtotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      }
    }
  })

  $('#table-DetalleProductosPagoContratista tbody').on('input', '.txt-Qt_Producto', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0) {
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $('#table-DetalleProductosPagoContratista tfoot').empty();
      if (nu_tipo_impuesto == 1 && considerar_igv == 1) {//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
        });
        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-impuesto').val($Ss_IGV.toFixed(2));
        $('#span-impuesto').text($Ss_IGV.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 2) {//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;

        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
        $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 3) {//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
        $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 4) {//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". "));

        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
        $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else {//Sin ningun tipo de impuesto
        fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
        fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

        var $Ss_Subtotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (considerar_igv == 0)
            $Ss_Subtotal += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-subTotal').val($Ss_Subtotal.toFixed(2));
        $('#span-subTotal').text($Ss_Subtotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      }
    }
  })

  $('#table-DetalleProductosPagoContratista tbody').on('change', '.cbo-ImpuestosProducto', function () {
    var fila = $(this).parents("tr");
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();

    if (considerar_igv == 1) {//SI IGV
      if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(total_producto) > 0) {
        if (nu_tipo_impuesto == 1) {//CON IGV
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 2) {//Inafecto
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 3) {//Exonerada
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 4) {//Gratuita
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Precio").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
          $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        }
      }
    }
  })

  $('#table-DetalleProductosPagoContratista tbody').on('input', '.txt-Ss_Descuento', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(descuento) >= 0 && parseFloat(total_producto) > 0 && (parseFloat($('#txt-Ss_Descuento').val()) == 0 || $('#txt-Ss_Descuento').val() == '')) {
      if (parseFloat(subtotal_producto) >= parseFloat(((subtotal_producto * descuento) / 100))) {
        if (nu_tipo_impuesto == 1) {//CON IGV
          fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
          fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 1) {
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            }

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-impuesto').val($Ss_IGV.toFixed(2));
          $('#span-impuesto').text($Ss_IGV.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 2) {//Inafecto
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_Inafecto = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 2)
              $Ss_Inafecto += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
            $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
          });

          $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
          $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 3) {//Exonerada
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_Exonerada = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 3)
              $Ss_Exonerada += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
            $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
          });

          $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
          $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (nu_tipo_impuesto == 4) {//Gratuita
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_Gratuita = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (Nu_Tipo_Impuesto == 4)
              $Ss_Gratuita += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
            $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
          });

          $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
          $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        } else if (considerar_igv == 0) {//Sin ningun tipo de impuesto
          fila.find(".td-fDescuentoSinImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".td-fDescuentoImpuestosItem").text((parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". "));
          fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". "));
          fila.find(".txt-Ss_Total_Producto").val((parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". "));

          var $Ss_SubTotal = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
            var rows = $(this);
            var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

            if (isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (considerar_igv == 0)
              $Ss_SubTotal += $Ss_SubTotal_Producto;

            $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
            $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
          });

          $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
          $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

          $('#txt-descuento').val($Ss_Descuento.toFixed(2));
          $('#span-descuento').text($Ss_Descuento.toFixed(2));

          $('#txt-total').val($Ss_Total.toFixed(2));
          $('#span-total').text($Ss_Total.toFixed(2));
        }
      }
    }
  })

  $('#table-DetalleProductosPagoContratista tbody').on('input', '.txt-Ss_Total_Producto', function () {
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();

    if (parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(total_producto) > 0) {
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $('#table-DetalleProductosPagoContratista tfoot').empty();
      if (nu_tipo_impuesto == 1) {//CON IGV
        fila.find(".txt-Ss_Precio").val((parseFloat(total_producto / cantidad).toFixed(2)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
        });

        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-impuesto').val($Ss_IGV.toFixed(2));
        $('#span-impuesto').text($Ss_IGV.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 2) {//Inafecto
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
        $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 3) {//Exonerada
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
        $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else if (nu_tipo_impuesto == 4) {//Gratuita
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". "));

        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
        $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      } else {//Sin ningun tipo de impuesto
        fila.find(".txt-Ss_Precio").val((parseFloat((total_producto - ((descuento * total_producto) / 100)) / cantidad).toFixed(3)).toString().split(". "));
        fila.find(".txt-Ss_SubTotal_Producto").val((parseFloat(total_producto - ((descuento * total_producto) / 100)).toFixed(6)).toString().split(". "));

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;

        $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
          var rows = $(this);
          var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());

          if (isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (considerar_igv == 0)
            $Ss_SubTotal += $Ss_SubTotal_Producto;

          $Ss_Descuento += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
          $Ss_Total += parseFloat(rows.find('td:eq(7) input', this).val());
        });

        $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
        $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

        $('#txt-descuento').val($Ss_Descuento.toFixed(2));
        $('#span-descuento').text($Ss_Descuento.toFixed(2));

        $('#txt-total').val($Ss_Total.toFixed(2));
        $('#span-total').text($Ss_Total.toFixed(2));
      }
    }
  })

  $('#table-DetalleProductosPagoContratista tbody').on('click', '#btn-deleteProducto', function () {
    $(this).closest('tr').remove();

    var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
    var $Ss_SubTotal = 0.00;
    var $Ss_Exonerada = 0.00;
    var $Ss_Inafecto = 0.00;
    var $Ss_Gratuita = 0.00;
    var $Ss_IGV = 0.00;
    var $Ss_Total = 0.00;
    var iCantDescuento = 0;
    var globalImpuesto = 0;
    var $Ss_Descuento_p = 0;
    $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
      var rows = $(this);
      var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
      var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
      var $Ss_SubTotal_Producto = parseFloat(rows.find('td:eq(5) input', this).val());
      var $Ss_Descuento_Producto = parseFloat(rows.find('td:eq(6) input', this).val());
      var $Ss_Total_Producto = parseFloat(rows.find('td:eq(7) input', this).val());

      $Ss_Total += $Ss_Total_Producto;

      if (iGrupoImpuesto == 1) {
        $Ss_SubTotal += $Ss_SubTotal_Producto;
        $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
        globalImpuesto = fImpuesto;
      } else if (iGrupoImpuesto == 2) {
        $Ss_Inafecto += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else if (iGrupoImpuesto == 3) {
        $Ss_Exonerada += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else {
        $Ss_Gratuita += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      }

      if (isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;

      $Ss_Descuento_p += (($Ss_Descuento_Producto * (parseFloat(rows.find('td:eq(1) input', this).val()) * parseFloat(rows.find('td:eq(3) input', this).val()))) / 100);
    });

    if ($Ss_Descuento > 0.00) {
      var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
      if ($Ss_SubTotal > 0.00) {
        $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
        $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
        $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
        $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
      }

      if ($Ss_Inafecto > 0.00) {
        $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
        $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
        $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
      }

      if ($Ss_Exonerada > 0.00) {
        $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
        $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
        $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
      }

      if ($Ss_Gratuita > 0.00) {
        $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
        $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
        $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
      }

      $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
      $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
    } else
      $Ss_Descuento = $Ss_Descuento_p;

    if (isNaN($Ss_Descuento))
      $Ss_Descuento = 0.00;

    $('#txt-subTotal').val($Ss_SubTotal.toFixed(2));
    $('#span-subTotal').text($Ss_SubTotal.toFixed(2));

    $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
    $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

    $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
    $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

    $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
    $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

    $('#txt-impuesto').val($Ss_IGV.toFixed(2));
    $('#span-impuesto').text($Ss_IGV.toFixed(2));

    $('#txt-descuento').val($Ss_Descuento.toFixed(2));
    $('#span-descuento').text($Ss_Descuento.toFixed(2));

    $('#txt-total').val($Ss_Total.toFixed(2));
    $('#span-total').text($Ss_Total.toFixed(2));

    if ($('#table-DetalleProductosPagoContratista >tbody >tr').length == 0)
      $('#table-DetalleProductosPagoContratista').hide();
  })
})//document ready

function reload_table_pago_contratista(){
  table_serie.ajax.reload(null,false);
}

function pdfPagoContratista(ID, ID_Familia) {
  var $modal_delete = $('#modal-message-delete');
  $modal_delete.modal('show');

  $('.modal-message-delete').removeClass('modal-danger modal-warning modal-success');
  $('.modal-message-delete').addClass('modal-success');

  $('.modal-title-message-delete').text('¿Deseas generar PDF?');

  $('#btn-cancel-delete').off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $('#btn-save-delete').off('click').click(function () {
    sendPDF($modal_delete, ID, ID_Familia);
  });
}

function sendPDF($modal_delete, ID, ID_Familia) {
  $('#modal-loader').modal('show');
  $modal_delete.modal('hide');
  url = base_url + 'Ventas/PagoContratistaController/generarPDF/' + ID + '/' + ID_Familia;
  window.open(url, '_blank');
  $('#modal-loader').modal('hide');
}

function verPagoContratista(ID, ID_Familia) {
  accion_orden_venta = 'upd_orden_venta';
  $('#modal-loader').modal('show');

  $('.div-Listar').hide();

  $('#txt-EID_Empresa').focus();

  $('#form-PagoContratista')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.form-group').removeClass('has-success');
  $('.help-block').empty();

  $('#panel-DetalleProductosPagoContratista').removeClass('panel-danger');
  $('#panel-DetalleProductosPagoContratista').addClass('panel-default');

  $('#radio-cliente_existente').prop('checked', true).iCheck('update');
  $('#radio-cliente_nuevo').prop('checked', false).iCheck('update');
  $('.div-cliente_existente').show();
  $('.div-cliente_nuevo').hide();

  $('#radio-contacto_existente').prop('checked', true).iCheck('update');
  $('#radio-contacto_nuevo').prop('checked', false).iCheck('update');
  $('.div-contacto_existente').show();
  $('.div-contacto_nuevo').hide();

  $('#radio-vehiculo_existente').prop('checked', true).iCheck('update');
  $('#radio-vehiculo_nuevo').prop('checked', false).iCheck('update');
  $('.div-vehiculo_existente').show();
  $('.div-vehiculo_nuevo').hide();

  $('#table-DetalleProductosPagoContratista tbody').empty();
  $('#table-DetalleProductosPagoContratistaModal thead').empty();
  $('#table-DetalleProductosPagoContratistaModal tbody').empty();

  $('#txt-subTotal').val(value_importes_cero);
  $('#span-subTotal').text(texto_importes_cero);

  $('#txt-inafecto').val(value_importes_cero);
  $('#span-inafecto').text(texto_importes_cero);

  $('#txt-exonerada').val(value_importes_cero);
  $('#span-exonerada').text(texto_importes_cero);

  $('#txt-gratuita').val(value_importes_cero);
  $('#span-gratuita').text(texto_importes_cero);

  $('#txt-impuesto').val(value_importes_cero);
  $('#span-impuesto').text(texto_importes_cero);

  $('#txt-descuento').val(value_importes_cero);
  $('#span-descuento').text(texto_importes_cero);

  $('#txt-total').val(value_importes_cero);
  $('#span-total').text(texto_importes_cero);

  $('[name="ENu_Estado"]').val('');

  $('#btn-save').attr('disabled', false);

  considerar_igv = 1;

  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post(url, function (response) {
    $('#cbo-TiposDocumentoIdentidadCliente').html('');
    for (var i = 0; i < response.length; i++)
      $('#cbo-TiposDocumentoIdentidadCliente').append('<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>');
  }, 'JSON');

  url = base_url + 'Ventas/PagoContratistaController/ajax_edit_modificar/' + ID;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      $('.div-AgregarEditar').show();

      $('.title_PagoContratista').text('Modifcar Pago de Contratista');

      $('[name="EID_Empresa"]').val(response.arrEdit[0].ID_Empresa);
      $('[name="EID_Documento_Cabecera"]').val(response.arrEdit[0].ID_Documento_Cabecera);

      url = base_url + 'HelperController/getAlmacenes';
      var arrParams = {
        iIdOrganizacion: response.arrEdit[0].ID_Organizacion,
      }
      $.post(url, arrParams, function (responseAlmacen) {
        $('#cbo-almacen').html('');
        for (var i = 0; i < responseAlmacen.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Almacen == responseAlmacen[i].ID_Almacen)
            selected = 'selected="selected"';
          $('#cbo-almacen').append('<option value="' + responseAlmacen[i].ID_Almacen + '" ' + selected + '>' + responseAlmacen[i].No_Almacen + '</option>');
        }
      }, 'JSON');

      //Datos Documento
      var selected = '';
      url = base_url + 'HelperController/getTiposDocumentos';
      $.post(url, { Nu_Tipo_Filtro: 12 }, function (responseTiposDocumento) {//5 = Cotizacion Venta
        $('#cbo-TiposDocumento').html('<option value="0" selected="selected">- Seleccionar -</option>');
        for (var i = 0; i < responseTiposDocumento.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Tipo_Documento == responseTiposDocumento[i].ID_Tipo_Documento)
            selected = 'selected="selected"';
          $('#cbo-TiposDocumento').append('<option value="' + responseTiposDocumento[i].ID_Tipo_Documento + '" data-nu_impuesto="' + responseTiposDocumento[i].Nu_Impuesto + '" data-nu_enlace="' + responseTiposDocumento[i].Nu_Enlace + '" ' + selected + '>' + responseTiposDocumento[i].No_Tipo_Documento_Breve + '</option>');
        }
      }, 'JSON');

      url = base_url + 'HelperController/getSeriesDocumento';
      $.post(url, { ID_Organizacion: response.arrEdit[0].ID_Organizacion, ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento }, function (responseSeriesDocumento) {
        $('#cbo-serie_documento').html('');
        for (var i = 0; i < responseSeriesDocumento.length; i++) {
          selected = '';
          if (response.arrEdit[0].ID_Serie_Documento == responseSeriesDocumento[i]['ID_Serie_Documento'])
            selected = 'selected="selected"';
          $('#cbo-serie_documento').append('<option value="' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '" ' + selected + ' data-id_serie_documento_pk=' + responseSeriesDocumento[i].ID_Serie_Documento_PK + '>' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '</option>');
        }
      }, 'JSON');

      $('[name="ID_Numero_Documento"]').val(response.arrEdit[0].ID_Numero_Documento);
      $('[name="Fe_Emision"]').val(ParseDateString(response.arrEdit[0].Fe_Emision, 6, '-'));

      //CLIENTE
      $('#txt-AID').val(response.arrEdit[0].ID_Entidad);
      $('#txt-ANombre').val(response.arrEdit[0].No_Entidad);
      $('#txt-ACodigo').val(response.arrEdit[0].Nu_Documento_Identidad);
      $('#txt-Txt_Direccion_Entidad').val(response.arrEdit[0].Txt_Direccion_Entidad);
      $('#txt-Nu_Documento_Identidad_existe').val(response.arrEdit[0].Nu_Documento_Identidad_Contacto);
      $('#txt-No_Contacto_existe').val(response.arrEdit[0].No_Contacto);
      $('#txt-Txt_Email_Contacto_existe').val(response.arrEdit[0].Txt_Email_Contacto);
      $('#txt-Nu_Telefono_Contacto_existe').val(response.arrEdit[0].Nu_Telefono_Contacto);
      $('#txt-Nu_Celular_Contacto_existe').val(response.arrEdit[0].Nu_Celular_Contacto);

      $('[name="Txt_Glosa"]').val(clearHTMLTextArea(response.arrEdit[0].Txt_Glosa));

      //Detalle
      $('#table-DetalleProductosPagoContratista').show();
      $('#table-DetalleProductosPagoContratista tbody').empty();

      var table_detalle_producto = '';
      var _ID_Producto = '';
      var $Ss_SubTotal_Producto = 0.00;
      var $Ss_IGV_Producto = 0.00;
      var $Ss_Descuento_Producto = 0.00;
      var $Ss_Total_Producto = 0.00;
      var $Ss_Gravada = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var option_impuesto_producto = '';

      var $fDescuento_Producto = 0;
      var fDescuento_Total_Producto = 0;
      var globalImpuesto = 0;
      var $iDescuentoGravada = 0;
      var $iDescuentoExonerada = 0;
      var $iDescuentoInafecto = 0;
      var $iDescuentoGratuita = 0;
      var $iDescuentoGlobalImpuesto = 0;
      var selected;

      var iTotalRegistros = response.arrEdit.length;
      var iTotalRegistrosImpuestos = response.arrImpuesto.length;
      console.log(response.arrEdit[0].ID_Producto);
      if (response.arrEdit[0].ID_Producto != null) {
        for (var i = 0; i < iTotalRegistros; i++) {
          if (_ID_Producto != response.arrEdit[i].ID_Producto) {
            _ID_Producto = response.arrEdit[i].ID_Producto;
            option_impuesto_producto = '';
          }

          $Ss_SubTotal_Producto = parseFloat(response.arrEdit[i].Ss_SubTotal_Producto)
          if (response.arrEdit[i].Nu_Tipo_Impuesto == 1) {
            $Ss_IGV += parseFloat(response.arrEdit[i].Ss_Impuesto_Producto);
            $Ss_Gravada += $Ss_SubTotal_Producto;
          } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 2) {
            $Ss_Inafecto += $Ss_SubTotal_Producto;
          } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 3) {
            $Ss_Exonerada += $Ss_SubTotal_Producto;
          } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 4) {
            $Ss_Gratuita += $Ss_SubTotal_Producto;
          }

          $Ss_Descuento_Producto += parseFloat(response.arrEdit[i].Ss_Descuento_Producto);
          $Ss_Total += parseFloat(response.arrEdit[i].Ss_Total_Producto);

          for (var x = 0; x < iTotalRegistrosImpuestos; x++) {
            selected = '';
            if (response.arrImpuesto[x].ID_Impuesto_Cruce_Documento == response.arrEdit[i].ID_Impuesto_Cruce_Documento)
              selected = 'selected="selected"';
            option_impuesto_producto += "<option value='" + response.arrImpuesto[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + response.arrImpuesto[x].Nu_Tipo_Impuesto + "' data-impuesto_producto='" + response.arrImpuesto[x].Ss_Impuesto + "' " + selected + ">" + response.arrImpuesto[x].No_Impuesto + "</option>";
          }

          table_detalle_producto +=
            "<tr id='tr_detalle_producto" + response.arrEdit[i].ID_Producto + "'>"
            + "<td style='display:none;' class='text-left'>" + response.arrEdit[i].ID_Producto + "</td>"
            + "<td class='text-right'><input type='tel' class='txt-Qt_Producto form-control input-decimal' value='" + response.arrEdit[i].Qt_Producto + "' autocomplete='off'></td>"
            + "<td class='text-left'>" + response.arrEdit[i].Nu_Codigo_Barra + " " + response.arrEdit[i].No_Producto + "</td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Precio form-control input-decimal' value='" + response.arrEdit[i].Ss_Precio + "' autocomplete='off'></td>"
            + "<td class='text-right'>"
            + "<select class='cbo-ImpuestosProducto form-control required' style='width: 100%;'>"
            + option_impuesto_producto
            + "</select>"
            + "</td>"
            + "<td style='display:none;' class='text-right'><input type='tel' class='txt-Ss_SubTotal_Producto form-control' value='" + response.arrEdit[i].Ss_SubTotal_Producto + "' autocomplete='off' disabled></td>"
            + "<td class='text-right'><input type='tel' class='txt-Ss_Descuento form-control input-decimal' value='" + (response.arrEdit[i].Po_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Po_Descuento_Impuesto_Producto) + "' autocomplete='off'></td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Total_Producto form-control input-decimal' value='" + response.arrEdit[i].Ss_Total_Producto + "' autocomplete='off'></td>"
            + "<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Producto) + "</td>"
            + "<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Impuesto_Producto) + "</td>"
            + "<td class='text-center'><button type='button' id='btn-deleteProducto' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-2x' aria-hidden='true'> </i></button></td>"
            + "</tr>";
        }
      }

      $('#table-DetalleProductosPagoContratista >tbody').append(table_detalle_producto);

      $('#txt-subTotal').val($Ss_Gravada.toFixed(2));
      $('#span-subTotal').text($Ss_Gravada.toFixed(2));

      $('#txt-exonerada').val($Ss_Exonerada.toFixed(2));
      $('#span-exonerada').text($Ss_Exonerada.toFixed(2));

      $('#txt-inafecto').val($Ss_Inafecto.toFixed(2));
      $('#span-inafecto').text($Ss_Inafecto.toFixed(2));

      $('#txt-gratuita').val($Ss_Gratuita.toFixed(2));
      $('#span-gratuita').text($Ss_Gratuita.toFixed(2));

      if (parseFloat(response.arrEdit[0].Ss_Descuento) > 0 && $Ss_Descuento_Producto == 0)
        $('#txt-Ss_Descuento').val(response.arrEdit[0].Po_Descuento);
      else
        $('#txt-Ss_Descuento').val('');

      $('#txt-descuento').val(response.arrEdit[0].Ss_Descuento);
      $('#span-descuento').text(response.arrEdit[0].Ss_Descuento);

      $('#txt-impuesto').val($Ss_IGV.toFixed(2));
      $('#span-impuesto').text($Ss_IGV.toFixed(2));

      $('#txt-total').val($Ss_Total.toFixed(2));
      $('#span-total').text($Ss_Total.toFixed(2));

      validateDecimal();
      validateNumber();
      validateNumberOperation();

      url = base_url + 'HelperController/getImpuestos';
      $.post(url, function (response) {
        arrImpuestosProducto = '';
        arrImpuestosProductoDetalle = '';
        for (var i = 0; i < response.length; i++)
          arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
        arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';

        $('#modal-loader').modal('hide');
      }, 'JSON');

      var _ID_Producto = '';
      var option_impuesto_producto = '';
    }
  })
}

function form_PagoContratista() {
  if (accion_orden_venta == 'add_orden_venta' || accion_orden_venta == 'upd_orden_venta') {
    var arrDetalle = [];
    var arrValidarNumerosEnCero = [];
    var $counterNumerosEnCero = 0;
    var tr_foot = '';

    $("#table-DetalleProductosPagoContratista > tbody > tr").each(function () {
      var rows = $(this);

      var $ID_Producto = rows.find("td:eq(0)").text();
      var $Qt_Producto = $('td:eq(1) input', this).val();
      var $Ss_Precio = $('td:eq(3) input', this).val();
      var $ID_Impuesto_Cruce_Documento = $('td:eq(4) select', this).val();
      var $Ss_SubTotal = $('td:eq(5) input', this).val();
      var $Ss_Descuento = $('td:eq(6) input', this).val();
      var $Ss_Total = $('td:eq(7) input', this).val();
      var $fDescuentoSinImpuestosItem = rows.find(".td-fDescuentoSinImpuestosItem").text();
      var $fDescuentoImpuestosItem = rows.find(".td-fDescuentoImpuestosItem").text();

      if (parseFloat($Ss_Precio) == 0 || parseFloat($Qt_Producto) == 0 || parseFloat($Ss_Total) == 0) {
        arrValidarNumerosEnCero[$counterNumerosEnCero] = $ID_Producto;
        $('#tr_detalle_producto' + $ID_Producto).addClass('danger');
      }

      var obj = {};

      obj.ID_Producto = $ID_Producto;
      obj.Ss_Precio = $Ss_Precio;
      obj.Qt_Producto = $Qt_Producto;
      obj.ID_Impuesto_Cruce_Documento = $ID_Impuesto_Cruce_Documento;
      obj.Ss_SubTotal = $Ss_SubTotal;
      obj.Ss_Descuento = $Ss_Descuento;
      obj.Ss_Impuesto = $Ss_Total - $Ss_SubTotal;
      obj.Ss_Total = $Ss_Total;
      obj.fDescuentoSinImpuestosItem = $fDescuentoSinImpuestosItem;
      obj.fDescuentoImpuestosItem = $fDescuentoImpuestosItem;
      arrDetalle.push(obj);
      $counterNumerosEnCero++;
    });

    if (arrDetalle.length == 0) {
      $('#panel-DetalleProductosPagoContratista').removeClass('panel-default');
      $('#panel-DetalleProductosPagoContratista').addClass('panel-danger');

      alert('No hay ítems en el detalle');
    } else if (arrValidarNumerosEnCero.length > 0) {
      tr_foot +=
        "<tfoot>"
        + "<tr class='danger'>"
        + "<td colspan='9' class='text-center'>Item(s) con <b>precio / cantidad / total en cero</b></td>"
        + "</tr>"
        + "<tfoot>";
      $('#table-DetalleProductosPagoContratista >tbody').after(tr_foot);
    } else {
      $('#panel-DetalleProductosPagoContratista').removeClass('panel-danger');
      $('#panel-DetalleProductosPagoContratista').addClass('panel-default');

      var arrCabecera = Array();
      arrCabecera = {
        'EID_Documento_Cabecera': $('[name="EID_Documento_Cabecera"]').val(),
        'Fe_Emision': $('#txt-Fe_Emision').val(),
        'Txt_Glosa': $('[name="Txt_Glosa"]').val(),
        'Ss_Total': $('#txt-total').val(),
      };

      var arrClienteNuevo = {};

      $('#btn-save').text('');
      $('#btn-save').attr('disabled', true);
      $('#btn-save').append('Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      $('#modal-loader').modal('show');

      url = base_url + 'Ventas/PagoContratistaController/crudPagoContratista';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: {
          arrCabecera: arrCabecera,
          arrDetalle: arrDetalle
        },
        success: function (response) {
          $('#modal-loader').modal('hide');

          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.status == 'success') {
            accion_orden_venta = '';

            $('#form-PagoContratista')[0].reset();
            $('.div-AgregarEditar').hide();
            $('.div-Listar').show();
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1100);
            reload_table_pago_contratista();
          } else {
            $('.modal-message').addClass(response.style_modal);
            $('.modal-title-message').text(response.message);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
          }

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar (ENTER)');
          $('#btn-save').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          $('#modal-loader').modal('hide');
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');

          $('#modal-message').modal('show');
          $('.modal-message').addClass('modal-danger');
          $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

          //Message for developer
          console.log(jqXHR.responseText);

          $('#btn-save').text('');
          $('#btn-save').append('<span class="fa fa-save"></span> Guardar (ENTER)');
          $('#btn-save').attr('disabled', false);
        }
      });
    }
  }
}