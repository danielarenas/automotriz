var url;

$(function () {
  $( '#modal-loader' ).modal('show');
  $( '#div-ventas_detalladas_generales' ).hide();
    
  url = base_url + 'HelperController/getTiposDocumentos';
  $.post( url, {Nu_Tipo_Filtro : 3}, function( response ){
    $( '#cbo-filtros_tipos_documento' ).html('<option value="0" selected="selected">Todos</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-filtros_tipos_documento' ).append( '<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>' );
	  $( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
	$( '#cbo-filtros_series_documento' ).html('<option value="0" selected="selected">Todos</option>');
	$( '#cbo-filtros_tipos_documento' ).change(function(){
	  $( '#cbo-filtros_series_documento' ).html('<option value="0" selected="selected">Todos</option>');
	  if ( $(this).val() > 0) {
		  url = base_url + 'HelperController/getSeriesDocumentoOficinaPuntoVenta';
      $.post( url, { ID_Tipo_Documento: $(this).val() }, function( response ){
        var l = response.length;
        var sTipoSerie = 'oficina';
        for (var i = 0; i < l; i++) {
          sTipoSerie = '(' + ( response[i].ID_POS > 0 ? 'pv' : 'oficina' ) + ')';
          $( '#cbo-filtros_series_documento' ).append( '<option value="' + response[i].ID_Serie_Documento + '">' + response[i].ID_Serie_Documento + ' ' + sTipoSerie + '</option>' );
        }
      }, 'JSON');
	  }
  })

  $( '.btn-generar_ventas_detalladas_generales' ).click(function(){    
    var Fe_Inicio, Fe_Fin, ID_Tipo_Documento, ID_Serie_Documento, ID_Numero_Documento, Nu_Estado_Documento, iIdCliente, sNombreCliente, iIdItem, sNombreItem, iTipoVenta;
    
    Fe_Inicio = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
    Fe_Fin = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
    ID_Tipo_Documento = $( '#cbo-filtros_tipos_documento' ).val();
    ID_Serie_Documento = $( '#cbo-filtros_series_documento' ).val();
    ID_Numero_Documento = ($( '#txt-Filtro_NumeroDocumento' ).val().length == 0 ? '-' : $( '#txt-Filtro_NumeroDocumento' ).val());
    Nu_Estado_Documento = $( '#cbo-estado_documento' ).val();
    iIdCliente = ($( '#txt-AID' ).val().length === 0 ? '-' : $( '#txt-AID' ).val());
    sNombreCliente = ($( '#txt-Filtro_Entidad' ).val().length === 0 ? '-' : $( '#txt-Filtro_Entidad' ).val());
    iIdItem = ($( '#txt-ID_Producto' ).val().length === 0 ? '-' : $( '#txt-ID_Producto' ).val());
    sNombreItem = ($( '#txt-No_Producto' ).val().length === 0 ? '-' : $( '#txt-No_Producto' ).val());
    iTipoVenta = $( '#cbo-tipo_venta' ).val();

    if ($(this).data('type') == 'html') {
      $( '#btn-html_ventas_detalladas_generales' ).text('');
      $( '#btn-html_ventas_detalladas_generales' ).attr('disabled', true);
      $( '#btn-html_ventas_detalladas_generales' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
      $( '#table-ventas_detalladas_generales > tbody' ).empty();
      $( '#table-ventas_detalladas_generales > tfoot' ).empty();

      var arrPost = {
        Fe_Inicio : Fe_Inicio,
        Fe_Fin : Fe_Fin,
        ID_Tipo_Documento : ID_Tipo_Documento,
        ID_Serie_Documento : ID_Serie_Documento,
        ID_Numero_Documento : ID_Numero_Documento,
        Nu_Estado_Documento : Nu_Estado_Documento,
        iIdCliente : iIdCliente,
        sNombreCliente : sNombreCliente,
        iIdItem : iIdItem,
        sNombreItem : sNombreItem,
        iTipoVenta : iTipoVenta,
      };      
      url = base_url + 'Ventas/informes_venta/VentasDetalladasGeneralesController/sendReporte';
      $.post( url, arrPost, function( response ){
        if ( response.sStatus == 'success' ) {
          var iTotalRegistros = response.arrData.length, tr_body = '', tr_foot = '';
          var fCantidadItem = 0.00, fPrecioItem = 0.00, fSubtotalItem = 0.00, fImpuestoItem = 0.00, fTotalItem = 0.00;
          var fCantidadTotalGeneral = 0.00, fSubtotalGeneral = 0.00, fImpuestoGeneral = 0.00, fTotalGeneral = 0.00;
          var response=response.arrData;
          for (var i = 0; i < iTotalRegistros; i++) {
            fCantidadItem = (!isNaN(parseFloat(response[i].Qt_Producto)) ? parseFloat(response[i].Qt_Producto) : 0);
            fPrecioItem = (!isNaN(parseFloat(response[i].Ss_Precio)) ? parseFloat(response[i].Ss_Precio) : 0);
            fSubtotalItem = (!isNaN(parseFloat(response[i].Ss_Subtotal)) ? parseFloat(response[i].Ss_Subtotal) : 0);
            fImpuestoItem = (!isNaN(parseFloat(response[i].Ss_Impuesto)) ? parseFloat(response[i].Ss_Impuesto) : 0);
            fTotalItem = (!isNaN(parseFloat(response[i].Ss_Total)) ? parseFloat(response[i].Ss_Total) : 0);

            tr_body +=
            "<tr>"
              +"<td class='text-center'>" + response[i].Fe_Emision_Hora + "</td>"
              +"<td class='text-left'>" + response[i].No_Empleado + "</td>"
              +"<td class='text-center'>" + response[i].No_Tipo_Documento_Breve + "</td>"
              +"<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
              +"<td class='text-center'>" + response[i].ID_Numero_Documento + "</td>"
              +"<td class='text-center'>" + response[i].No_Tipo_Documento_Identidad_Breve + "</td>"
              +"<td class='text-center'>" + response[i].Nu_Documento_Identidad + "</td>"
              +"<td class='text-left'>" + response[i].No_Entidad + "</td>"
              +"<td class='text-center'>" + response[i].No_Signo + "</td>"
              +"<td class='text-right'>" + number_format(response[i].Ss_Tipo_Cambio, 3) + "</td>"
              +"<td class='text-left'>" + response[i].Nu_Codigo_Barra + "</td>"
              +"<td class='text-left'>" + response[i].No_Producto + "</td>"
              +"<td class='text-left'>" + response[i].Txt_Nota_Item + "</td>"
              +"<td class='text-left'>" + response[i].No_Marca + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(fCantidadItem, 2) + "</td>"
              +"<td class='text-left'>" + response[i].Qt_CO2_Producto + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(fPrecioItem, 2) + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(fSubtotalItem, 2) + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(fImpuestoItem, 2) + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(fTotalItem, 2) + "</td>"
              +"<td class='text-left'>" + response[i].Txt_Nota + "</td>"
              +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</span></td>"
              +"<td class='text-center'>" + (response[i].Nu_Estado == 6 || response[i].Nu_Estado == 8 ? response[i].sAccionVer : '') + "</td>"
              +"<td class='text-center'>" + (response[i].Nu_Estado == 6 || response[i].Nu_Estado == 8 ? response[i].sAccionImprimir : '') + "</td>"
            +"</tr>";
            
            fCantidadTotalGeneral += fCantidadItem;
            fSubtotalGeneral += fSubtotalItem;
            fImpuestoGeneral += fImpuestoItem;
            fTotalGeneral += fTotalItem;
          }
          
          tr_foot =
          "<tfoot>"
            +"<tr>"
              +"<th class='text-right' colspan='14'>Total</th>"
              +"<th class='text-right'>" + number_format(fCantidadTotalGeneral, 2) + "</th>"
              +"<th class='text-right'>&nbsp;</th>"
              +"<th class='text-right'>&nbsp;</th>"
              +"<th class='text-right'>" + number_format(fSubtotalGeneral, 2) + "</th>"
              +"<th class='text-right'>" + number_format(fImpuestoGeneral, 2) + "</th>"
              +"<th class='text-right'>" + number_format(fTotalGeneral, 2) + "</th>"
              +"<th class='text-right'></th>"
            +"</tr>"
          +"</tfoot>";
        } else {
          if( response.sMessageSQL !== undefined ) {
            console.log(response.sMessageSQL);
          }
          tr_body +=
          "<tr>"
            +"<td colspan='23' class='text-center'>" + response.sMessage + "</td>"
          + "</tr>";
        } // ./ if arrData
        
        $( '#div-ventas_detalladas_generales' ).show();
        $( '#table-ventas_detalladas_generales > tbody' ).append(tr_body);
        $( '#table-ventas_detalladas_generales > tbody' ).after(tr_foot);
        
        $( '#btn-html_ventas_detalladas_generales' ).text('');
        $( '#btn-html_ventas_detalladas_generales' ).append( '<i class="fa fa-search"></i> Buscar' );
        $( '#btn-html_ventas_detalladas_generales' ).attr('disabled', false);
      }, 'JSON')
      .fail(function(jqXHR, textStatus, errorThrown) {
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
        
        //Message for developer
        console.log(jqXHR.responseText);
        
        $( '#btn-html_ventas_detalladas_generales' ).text('');
        $( '#btn-html_ventas_detalladas_generales' ).append( '<i class="fa fa-search"></i> Buscar' );
        $( '#btn-html_ventas_detalladas_generales' ).attr('disabled', false);
      });
    } else if ($(this).data('type') == 'pdf') {
      $( '#btn-pdf_ventas_detalladas_generales' ).text('');
      $( '#btn-pdf_ventas_detalladas_generales' ).attr('disabled', true);
      $( '#btn-pdf_ventas_detalladas_generales' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
          
      url = base_url + 'Ventas/informes_venta/VentasDetalladasGeneralesController/sendReportePDF/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Tipo_Documento + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Estado_Documento + '/' + iIdCliente + '/' + sNombreCliente + '/' + iIdItem + '/' + sNombreItem + '/' + iTipoVenta;
      window.open(url,'_blank');
      
      $( '#btn-pdf_ventas_detalladas_generales' ).text('');
      $( '#btn-pdf_ventas_detalladas_generales' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
      $( '#btn-pdf_ventas_detalladas_generales' ).attr('disabled', false);
    } else if ($(this).data('type') == 'excel') {
      $( '#btn-excel_ventas_detalladas_generales' ).text('');
      $( '#btn-excel_ventas_detalladas_generales' ).attr('disabled', true);
      $( '#btn-excel_ventas_detalladas_generales' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      
      url = base_url + 'Ventas/informes_venta/VentasDetalladasGeneralesController/sendReporteEXCEL/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Tipo_Documento + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Estado_Documento + '/' + iIdCliente + '/' + sNombreCliente + '/' + iIdItem + '/' + sNombreItem + '/' + iTipoVenta;
      window.open(url,'_blank');
      
      $( '#btn-excel_ventas_detalladas_generales' ).text('');
      $( '#btn-excel_ventas_detalladas_generales' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
      $( '#btn-excel_ventas_detalladas_generales' ).attr('disabled', false);
    }// /. if
  })// /. btn
})