var url;

$(function () {  
  $('.select2').select2();
  
  $( '#modal-loader' ).modal('show');

  $( '#div-ventas_x_familia' ).hide();
  
	url = base_url + 'HelperController/getDataGeneral';
	$.post( url, {sTipoData : 'categoria'}, function( response ){
    if ( response.sStatus == 'success' ) {
      var iTotalRegistros = response.arrData.length, response=response.arrData;
      $( '#cbo-familia' ).html( '<option value="0" selected="selected">- Todos -</option>');
      for (var i = 0; i < iTotalRegistros; i++)
        $( '#cbo-familia' ).append( '<option value="' + response[i].ID + '">' + response[i].Nombre + '</option>' );
    } else {
      $( '#cbo-familia' ).html( '<option value="0" selected="selected">- Vacío -</option>');
      console.log( response );
    }
		$( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
  $( '.btn-generar_ventas_x_familia' ).click(function(){
    $( '.help-block' ).empty();
  
    var Fe_Inicio, Fe_Fin, iIdFamilia;
    
    Fe_Inicio = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
    Fe_Fin = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
    iIdFamilia = $( '#cbo-familia' ).val();

    var arrPost = {
      Fe_Inicio : Fe_Inicio,
      Fe_Fin : Fe_Fin,
      iIdFamilia : iIdFamilia,
    };
      
    if ($(this).data('type') == 'html') {
      $( '#btn-html_ventas_x_familia' ).text('');
      $( '#btn-html_ventas_x_familia' ).attr('disabled', true);
      $( '#btn-html_ventas_x_familia' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
      $( '#table-ventas_x_familia > tbody' ).empty();
      $( '#table-ventas_x_familia > tfoot' ).empty();
      
      url = base_url + 'Ventas/informes_venta/VentasxFamiliaController/sendReporte';
      $.post( url, arrPost, function( response ){
        if ( response.sStatus == 'success' ) {
          var iTotalRegistros = response.arrData.length, tr_body = '', tr_foot = '', counter = 0, ID_Familia = '', cantidad = 0.00, total_s = 0.00, total_d = 0.00;
          var sum_cantidad = 0.00, sum_total_s = 0.00, sum_total_d = 0.00
          var sum_general_cantidad = 0.00, sum_general_total_s = 0.00, sum_general_total_d = 0.00;
          var response=response.arrData;
          for (var i = 0; i < iTotalRegistros; i++) {
            if (ID_Familia != response[i].ID_Familia) {
              if (counter != 0) {
                tr_body +=
                +"<tr>"
                  +"<th class='text-right' colspan='8'>Total </th>"
                  +"<th class='text-right'>" + number_format(sum_cantidad, 0) + "</th>"
                  +"<th class='text-right'></th>"
                  +"<th class='text-right'>" + number_format(sum_total_s, 2) + "</th>"
                  +"<th class='text-right'>" + number_format(sum_total_d, 2) + "</th>"
                +"</tr>";
                sum_cantidad = 0.00;
                sum_total_s = 0.00;
                sum_total_d = 0.00;
              }
              
              tr_body +=
              "<tr>"
                +"<th class='text-center'>Familia </th>"
                +"<th class='text-left' colspan='13'>" + response[i].No_Familia + "</th>"
              +"</tr>";
              ID_Familia = response[i].ID_Familia;
            }

            cantidad = (!isNaN(parseFloat(response[i].Qt_Producto)) ? parseFloat(response[i].Qt_Producto) : 0);
            total_s = (!isNaN(parseFloat(response[i].Ss_Total)) ? parseFloat(response[i].Ss_Total) : 0);
            total_d = (!isNaN(parseFloat(response[i].Ss_Total_Extranjero)) ? parseFloat(response[i].Ss_Total_Extranjero) : 0);

            tr_body +=
            "<tr>"
              +"<td class='text-center'>" + response[i].Fe_Emision_Hora + "</td>"
              +"<td class='text-center'>" + response[i].No_Tipo_Documento_Breve + "</td>"
              +"<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
              +"<td class='text-center'>" + response[i].ID_Numero_Documento + "</td>"
              +"<td class='text-left'>" + response[i].No_Entidad + "</td>"
              +"<td class='text-right'>" + number_format(response[i].Ss_Tipo_Cambio, 3) + "</td>"
              +"<td class='text-center'>" + response[i].No_Unidad_Medida + "</td>"
              +"<td class='text-left'>" + response[i].No_Producto + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(cantidad, 0) + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(response[i].Ss_Precio, 2) + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_s, 2) + "</td>"
              +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_d, 2) + "</td>"
              +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</span></td>"
              +"<td class='text-center'>" + (response[i].Nu_Estado == 6 || response[i].Nu_Estado == 8 ? response[i].sAccionVer : '') + "</td>"
              +"<td class='text-center'>" + (response[i].Nu_Estado == 6 || response[i].Nu_Estado == 8 ? response[i].sAccionImprimir : '') + "</td>"
            +"</tr>";
            
            sum_cantidad += cantidad;
            sum_total_s += total_s;
            sum_total_d += total_d;
            
            sum_general_cantidad += cantidad;
            sum_general_total_s += total_s;
            sum_general_total_d += total_d;

            counter++;
          }
          
          tr_foot =
          "<tfoot>"
            +"<tr>"
              +"<th class='text-right' colspan='8'>Total</th>"
              +"<th class='text-right'>" + number_format(sum_cantidad, 0) + "</th>"
              +"<th class='text-right'></th>"
              +"<th class='text-right'>" + number_format(sum_total_s, 2) + "</th>"
              +"<th class='text-right'>" + number_format(sum_total_d, 2) + "</th>"
              +"<th class='text-right'></th>"
            +"</tr>"
            +"<tr>"
              +"<th class='text-right' colspan='8'>Total General</th>"
              +"<th class='text-right'>" + number_format(sum_general_cantidad, 0) + "</th>"
              +"<th class='text-right'></th>"
              +"<th class='text-right'>" + number_format(sum_general_total_s, 2) + "</th>"
              +"<th class='text-right'>" + number_format(sum_general_total_d, 2) + "</th>"
              +"<th class='text-right'></th>"
            +"</tr>"
          +"</tfoot>";
        } else {
          if( response.sMessageSQL !== undefined ) {
            console.log(response.sMessageSQL);
          }
          tr_body +=
          "<tr>"
            +"<td colspan='13' class='text-center'>" + response.sMessage + "</td>"
          + "</tr>";
        } // ./ if arrData
        
        $( '#div-ventas_x_familia' ).show();
        $( '#table-ventas_x_familia > tbody' ).append(tr_body);
        $( '#table-ventas_x_familia > tbody' ).after(tr_foot);
        
        $( '#btn-html_ventas_x_familia' ).text('');
        $( '#btn-html_ventas_x_familia' ).append( '<i class="fa fa-search"></i> Buscar' );
        $( '#btn-html_ventas_x_familia' ).attr('disabled', false);
      }, 'JSON')
      .fail(function(jqXHR, textStatus, errorThrown) {
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
        
        //Message for developer
        console.log(jqXHR.responseText);
        
        $( '#btn-html_ventas_x_familia' ).text('');
        $( '#btn-html_ventas_x_familia' ).append( '<i class="fa fa-search"></i> Buscar' );
        $( '#btn-html_ventas_x_familia' ).attr('disabled', false);
      });
    } else if ($(this).data('type') == 'pdf') {
      $( '#btn-pdf_ventas_x_familia' ).text('');
      $( '#btn-pdf_ventas_x_familia' ).attr('disabled', true);
      $( '#btn-pdf_ventas_x_familia' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
          
      url = base_url + 'Ventas/informes_venta/VentasxFamiliaController/sendReportePDF/' + Fe_Inicio + '/' + Fe_Fin + '/' + iIdFamilia;
      window.open(url,'_blank');
      
      $( '#btn-pdf_ventas_x_familia' ).text('');
      $( '#btn-pdf_ventas_x_familia' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
      $( '#btn-pdf_ventas_x_familia' ).attr('disabled', false);
    } else if ($(this).data('type') == 'excel') {
      $( '#btn-excel_ventas_x_familia' ).text('');
      $( '#btn-excel_ventas_x_familia' ).attr('disabled', true);
      $( '#btn-excel_ventas_x_familia' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      
      url = base_url + 'Ventas/informes_venta/VentasxFamiliaController/sendReporteEXCEL/' + Fe_Inicio + '/' + Fe_Fin + '/' + iIdFamilia;
      window.open(url,'_blank');
      
      $( '#btn-excel_ventas_x_familia' ).text('');
      $( '#btn-excel_ventas_x_familia' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
      $( '#btn-excel_ventas_x_familia' ).attr('disabled', false);
    }// ./ if
  })//./ btn
})