var url;

$(document).ready(function(){
  Reporte(fYear, fMonth, '', '', 1);
  $( "#sltReporte" ).change(function(){
    Reporte(fYear, fMonth, '', '', 1);
  })
})

function Reporte(anio, mes, ID_Moneda, Nu_Tipo_Producto, iOrder){
  $( "#dvReporte" ).load(base_url + 'Ventas/informes_venta/GeneralVariosController/Ajax/Reporte', {
    tipo : $( "#sltReporte" ).val(),
    y : anio,
    m : mes,
    ID_Moneda : ID_Moneda,
    Nu_Tipo_Producto : Nu_Tipo_Producto,
    iOrder : iOrder,
  });
}