var url;

$(function () {
  $( '#div-venta_x_tipo_documento_sunat' ).hide();
  
  $( '.btn-generar_venta_sunat' ).click(function(){
    var Fe_Inicio, Fe_Fin, iDocumentStatus, tr_body = "", tr_foot = "";

    Fe_Inicio = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
    Fe_Fin    = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
    iDocumentStatus = $( '#cbo-Filtro_Estado' ).val();

    var arrPost = {
      Fe_Inicio : Fe_Inicio,
      Fe_Fin    : Fe_Fin,
      iDocumentStatus : iDocumentStatus,
    };
      
    if ($(this).data('type') == 'html') {
      $( '#btn-html_venta_sunat' ).text('');
      $( '#btn-html_venta_sunat' ).attr('disabled', true);
      $( '#btn-html_venta_sunat' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
      $( '#table-venta_x_tipo_documento_sunat > tbody' ).empty();
      $( '#table-venta_x_tipo_documento_sunat > tfoot' ).empty();
      
      url = base_url + 'Ventas/informes_venta/VentasTiposDocumentoSunatController/sendReporte';
      $.post( url, arrPost, function( response ){
        if (response.length > 0){
          var iCantTransBoleta = 0,
          fTotalBoleta = 0.00,
          iCantTransFactura = 0,
          fTotalFactura = 0.00,
          iCantTransNC = 0,
          fTotalNC = 0.00,
          iCantTransND = 0,
          fTotalND = 0.00,
          sum_cantidad_trans_b = 0,
          sum_total_b = 0.00,
          sum_cantidad_trans_f = 0,
          sum_total_f = 0.00,
          sum_cantidad_trans_nc = 0,
          sum_total_nc = 0.00,
          sum_cantidad_trans_nd = 0,
          sum_total_nd = 0.00;
          for (var i = 0, len = response.length; i < len; i++) {
            iCantTransBoleta = (!isNaN(parseInt(response[i].Nu_Cantidad_Trans_BOL)) ? parseInt(response[i].Nu_Cantidad_Trans_BOL) : 0 );
            fTotalBoleta = (!isNaN(parseFloat(response[i].Ss_Total_BOL)) ? parseFloat(response[i].Ss_Total_BOL) : 0 );
            iCantTransFactura = (!isNaN(parseInt(response[i].Nu_Cantidad_Trans_FACT)) ? parseInt(response[i].Nu_Cantidad_Trans_FACT) : 0 );
            fTotalFactura = (!isNaN(parseFloat(response[i].Ss_Total_FACT)) ? parseFloat(response[i].Ss_Total_FACT) : 0 );
            iCantTransNC = (!isNaN(parseInt(response[i].Nu_Cantidad_Trans_NC)) ? parseInt(response[i].Nu_Cantidad_Trans_NC) : 0 );
            fTotalNC = (!isNaN(parseFloat(response[i].Ss_Total_NC)) ? parseFloat(response[i].Ss_Total_NC) : 0 );
            iCantTransND = (!isNaN(parseInt(response[i].Nu_Cantidad_Trans_ND)) ? parseInt(response[i].Nu_Cantidad_Trans_ND) : 0 );
            fTotalND = (!isNaN(parseFloat(response[i].Ss_Total_ND)) ? parseFloat(response[i].Ss_Total_ND) : 0 );
             
            tr_body +=
            "<tr>"
              +"<td class='text-center'>" + response[i].Fe_Emision + "</td>"
              +"<td class='text-right'>" + iCantTransBoleta + "</td>"
              +"<td class='text-right'>" + number_format(fTotalBoleta, 2) + "</td>"
              +"<td class='text-right'>" + iCantTransFactura + "</td>"
              +"<td class='text-right'>" + number_format(fTotalFactura, 2) + "</td>"
              +"<td class='text-right'>" + iCantTransNC + "</td>"
              +"<td class='text-right'>" + number_format(fTotalNC, 2) + "</td>"
              +"<td class='text-right'>" + iCantTransND + "</td>"
              +"<td class='text-right'>" + number_format(fTotalND, 2) + "</td>"
              +"<td class='text-right'>" + (iCantTransBoleta + iCantTransFactura + iCantTransNC + iCantTransND) + "</td>"
              +"<td class='text-right'>" + number_format(fTotalBoleta + fTotalFactura - fTotalNC + fTotalND, 2) + "</td>"
            +"</tr>";
            
            sum_cantidad_trans_b += iCantTransBoleta;
            sum_total_b += fTotalBoleta;
            sum_cantidad_trans_f += iCantTransFactura;
            sum_total_f += fTotalFactura;
            sum_cantidad_trans_nc += iCantTransNC;
            sum_total_nc += fTotalNC;
            sum_cantidad_trans_nd += iCantTransND;
            sum_total_nd += fTotalND;
          }
          
          tr_foot =
          "<tfoot>"
            +"<tr>"
              +"<th class='text-right'>Total</th>"
              +"<th class='text-right'>" + sum_cantidad_trans_b + "</th>"
              +"<th class='text-right'>" + number_format(sum_total_b, 2) + "</th>"
              +"<th class='text-right'>" + sum_cantidad_trans_f + "</th>"
              +"<th class='text-right'>" + number_format(sum_total_f, 2) + "</th>"
              +"<th class='text-right'>" + sum_cantidad_trans_nc + "</th>"
              +"<th class='text-right'>" + number_format(sum_total_nc, 2) + "</th>"
              +"<th class='text-right'>" + sum_cantidad_trans_nd + "</th>"
              +"<th class='text-right'>" + number_format(sum_total_nd, 2) + "</th>"
              +"<th class='text-right'>" + (parseInt(sum_cantidad_trans_b) + parseInt(sum_cantidad_trans_f) + parseInt(sum_cantidad_trans_nc) + parseInt(sum_cantidad_trans_nd)) + "</th>"
              +"<th class='text-right'>" + number_format(parseFloat(sum_total_b) + parseFloat(sum_total_f) - parseFloat(sum_total_nc) + parseFloat(sum_total_nd), 2) + "</th>"
            +"</tr>"
          +"</tfoot>";
        } else {
          tr_body +=
          "<tr>"
            + "<td colspan='10' class='text-center'>No hay registros</td>"
          + "</tr>";
        }
        
        $( '#div-venta_x_tipo_documento_sunat' ).show();
        $( '#table-venta_x_tipo_documento_sunat > tbody' ).append(tr_body);
        $( '#table-venta_x_tipo_documento_sunat > tbody' ).after(tr_foot);
        
        $( '#btn-html_venta_sunat' ).text('');
        $( '#btn-html_venta_sunat' ).append( '<i class="fa fa-search"></i> Buscar' );
        $( '#btn-html_venta_sunat' ).attr('disabled', false);
      }, 'JSON');
    } else if ($(this).data('type') == 'pdf') {
      $( '#btn-pdf_venta_sunat' ).text('');
      $( '#btn-pdf_venta_sunat' ).attr('disabled', true);
      $( '#btn-pdf_venta_sunat' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
          
      url = base_url + 'Ventas/informes_venta/VentasTiposDocumentoSunatController/sendReportePDF/' + Fe_Inicio + '/' + Fe_Fin + '/' + iDocumentStatus;
      window.open(url,'_blank');
      
      $( '#btn-pdf_venta_sunat' ).text('');
      $( '#btn-pdf_venta_sunat' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
      $( '#btn-pdf_venta_sunat' ).attr('disabled', false);
    } else if ($(this).data('type') == 'excel') {
	    $( '#btn-excel_venta_sunat' ).text('');
      $( '#btn-excel_venta_sunat' ).attr('disabled', true);
      $( '#btn-excel_venta_sunat' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      
      url = base_url + 'Ventas/informes_venta/VentasTiposDocumentoSunatController/sendReporteEXCEL/' + Fe_Inicio + '/' + Fe_Fin + '/' + iDocumentStatus;
      window.open(url,'_blank');
      
      $( '#btn-excel_venta_sunat' ).text('');
      $( '#btn-excel_venta_sunat' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
      $( '#btn-excel_venta_sunat' ).attr('disabled', false);
	  }
  })
})