var url;

$('.date-picker-invoice').val(fDay + '/' + fMonth + '/' + fYear);
$(function () {  
  $('.select2').select2();

  $("#myInput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("#table-saldo_cliente tr").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  
  $( '#modal-loader' ).modal('show');

  $( '#div-saldo_cliente' ).hide();
  
  url = base_url + 'HelperController/getTiposDocumentos';
  $.post( url, {Nu_Tipo_Filtro : 3}, function( response ){
    $( '#cbo-filtros_tipos_documento' ).html('<option value="0" selected="selected">Todos</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-filtros_tipos_documento' ).append( '<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>' );
	  $( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
	$( '#cbo-filtros_series_documento' ).html('<option value="0" selected="selected">Todos</option>');
	$( '#cbo-filtros_tipos_documento' ).change(function(){
	  $( '#cbo-filtros_series_documento' ).html('<option value="0" selected="selected">Todos</option>');
	  if ( $(this).val() > 0) {
		  url = base_url + 'HelperController/getSeriesDocumentoOficinaPuntoVenta';
      $.post( url, { ID_Tipo_Documento: $(this).val() }, function( response ){
        var l = response.length;
        var sTipoSerie = 'oficina';
        for (var i = 0; i < l; i++) {
          sTipoSerie = '(' + ( response[i].ID_POS > 0 ? 'pv' : 'oficina' ) + ')';
          $( '#cbo-filtros_series_documento' ).append( '<option value="' + response[i].ID_Serie_Documento + '">' + response[i].ID_Serie_Documento + ' ' + sTipoSerie + '</option>' );
        }
      }, 'JSON');
	  }
  })
  
  $( '.btn-saldo_cliente' ).click(function(){
    if ( $( '#txt-Filtro_Entidad' ).val().length > 0 && $( '#txt-AID' ).val().length === 0 ) {
      $( '#txt-Filtro_Entidad' ).closest('.form-group').find('.help-block').html('Seleccionar cliente');
		  $( '#txt-Filtro_Entidad' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $( '.help-block' ).empty();
    
      var Fe_Inicio, Fe_Fin, iIdTipoDocumento, iIdSerieDocumento, iNumeroDocumento, iEstadoPago, iIdCliente, sNombreCliente;
      
      Fe_Inicio = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
      Fe_Fin = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
      iIdTipoDocumento = $( '#cbo-filtros_tipos_documento' ).val();
      iIdSerieDocumento = $( '#cbo-filtros_series_documento' ).val();
      iNumeroDocumento = ($( '#txt-Filtro_NumeroDocumento' ).val().length === 0 ? '-' : $( '#txt-Filtro_NumeroDocumento' ).val());
      iEstadoPago = $( '#cbo-estado_pago' ).val();
      iIdCliente = ($( '#txt-AID' ).val().length === 0 ? '-' : $( '#txt-AID' ).val());
      sNombreCliente = ($( '#txt-Filtro_Entidad' ).val().length === 0 ? '-' : $( '#txt-Filtro_Entidad' ).val());

      var arrPost = {
        Fe_Inicio : Fe_Inicio,
        Fe_Fin : Fe_Fin,
        iIdTipoDocumento : iIdTipoDocumento,
        iIdSerieDocumento : iIdSerieDocumento,
        iNumeroDocumento : iNumeroDocumento,
        iEstadoPago : iEstadoPago,
        iIdCliente : iIdCliente,
        sNombreCliente : sNombreCliente,
      };
        
      if ($(this).data('type') == 'html') {
        $( '#btn-html_saldo_cliente' ).text('');
        $( '#btn-html_saldo_cliente' ).attr('disabled', true);
        $( '#btn-html_saldo_cliente' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
    
        $( '#table-saldo_cliente > tbody' ).empty();
        $( '#table-saldo_cliente > tfoot' ).empty();
        
        url = base_url + 'Ventas/informes_venta/SaldoClienteController/sendReporte';
        $.post( url, arrPost, function( response ){
          if ( response.sStatus == 'success' ) {
            var iTotalRegistros = response.arrData.length, tr_body = '', tr_foot = '', total_s = 0.00, total_s_saldo = 0.00, sum_total_s = 0.00, sum_total_s_saldo = 0.00, saldo_detraccion = 0.00, saldo_retencion=0.00;
            var response=response.arrData;
            for (var i = 0; i < iTotalRegistros; i++) {
              total_s = (!isNaN(parseFloat(response[i].Ss_Total)) ? parseFloat(response[i].Ss_Total) : 0);
              total_s_saldo = (!isNaN(parseFloat(response[i].Ss_Total_Saldo)) ? parseFloat(response[i].Ss_Total_Saldo) : 0);
              saldo_detraccion = (!isNaN(parseFloat(response[i].Ss_Saldo_Detraccion)) ? parseFloat(response[i].Ss_Saldo_Detraccion) : 0);
              saldo_retencion = (!isNaN(parseFloat(response[i].Ss_Retencion)) ? parseFloat(response[i].Ss_Retencion) : 0);

              tr_body +=
              "<tr>"
                +"<td class='text-center'>" + response[i].Fe_Emision + "</td>"
                +"<td class='text-center'>" + response[i].Fe_Vencimiento + "</td>"
                +"<td class='text-center'>" + response[i].Fe_Entrega_Factura + "</td>"
                +"<td class='text-center'>" + response[i].Fe_Entrega_Vencimiento_Factura + "</td>"
                +"<td class='text-center'>" + response[i].Dias_Vencimiento + "</td>"
                +"<td class='text-center'>" + response[i].No_Tipo_Documento_Breve + "</td>"
                +"<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
                +"<td class='text-center'>" + response[i].ID_Numero_Documento + "</td>"
                +"<td class='text-left'>" + response[i].No_Entidad + "</td>"
                +"<td class='text-center'>" + response[i].No_Signo + "</td>"
                +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_s, 2) + "</td>"
                +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_s_saldo, 2) + "</td>"
                +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(saldo_detraccion, 2) + "</td>"
                +"<td class='text-center'>" + response[i].sTieneDetraccion + "</td>"
                + "<td class='text-center'>" + response[i].sDetraccion + "</td>"
                + "<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(saldo_retencion, 2) + "</td>"
                + "<td class='text-center'>" + response[i].sTieneRetencion + "</td>"
                + "<td class='text-center'>" + response[i].sRetencion + "</td>"
                +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado_Pago + "'>" + response[i].No_Estado_Pago + "</span></td>"
                +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</span></td>"
                +"<td class='text-center'>" + response[i].sEstadoVencimiento + "</td>"
              +"</tr>";
              
              sum_total_s += total_s;
              sum_total_s_saldo += total_s_saldo;
            }
            
            tr_foot =
            "<tfoot>"
              +"<tr>"
                +"<th class='text-right' colspan='6'>Total</th>"
                +"<th class='text-right'>" + number_format(sum_total_s, 2) + "</th>"
                +"<th class='text-right'>" + number_format(sum_total_s_saldo, 2) + "</th>"
              +"</tr>"
            +"</tfoot>";
          } else {
            if( response.sMessageSQL !== undefined ) {
              console.log(response.sMessageSQL);
            }
            tr_body +=
            "<tr>"
              +"<td colspan='9' class='text-center'>" + response.sMessage + "</td>"
            + "</tr>";
          } // ./ if arrData
          
          $( '#div-saldo_cliente' ).show();
          $( '#table-saldo_cliente > tbody' ).append(tr_body);
          $( '#table-saldo_cliente > tbody' ).after(tr_foot);
          
          $( '#btn-html_saldo_cliente' ).text('');
          $( '#btn-html_saldo_cliente' ).append( '<i class="fa fa-search"></i> Buscar' );
          $( '#btn-html_saldo_cliente' ).attr('disabled', false);
        }, 'JSON')
        .fail(function(jqXHR, textStatus, errorThrown) {
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          
          $( '#modal-message' ).modal('show');
          $( '.modal-message' ).addClass( 'modal-danger' );
          $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
          setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
          
          //Message for developer
          console.log(jqXHR.responseText);
          
          $( '#btn-html_saldo_cliente' ).text('');
          $( '#btn-html_saldo_cliente' ).append( '<i class="fa fa-search"></i> Buscar' );
          $( '#btn-html_saldo_cliente' ).attr('disabled', false);
        });
      } else if ($(this).data('type') == 'pdf') {
        $( '#btn-pdf_saldo_cliente' ).text('');
        $( '#btn-pdf_saldo_cliente' ).attr('disabled', true);
        $( '#btn-pdf_saldo_cliente' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );

        url = base_url + 'Ventas/informes_venta/SaldoClienteController/sendReportePDF/' + Fe_Inicio + '/' + Fe_Fin + '/' + iIdTipoDocumento + '/' + iIdSerieDocumento + '/' + iNumeroDocumento + '/' + iEstadoPago + '/' + iIdCliente + '/' + sNombreCliente;
        window.open(url,'_blank');
        
        $( '#btn-pdf_saldo_cliente' ).text('');
        $( '#btn-pdf_saldo_cliente' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
        $( '#btn-pdf_saldo_cliente' ).attr('disabled', false);
      } else if ($(this).data('type') == 'excel') {
        $( '#btn-excel_saldo_cliente' ).text('');
        $( '#btn-excel_saldo_cliente' ).attr('disabled', true);
        $( '#btn-excel_saldo_cliente' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
        
        url = base_url + 'Ventas/informes_venta/SaldoClienteController/sendReporteEXCEL/' + Fe_Inicio + '/' + Fe_Fin + '/' + iIdTipoDocumento + '/' + iIdSerieDocumento + '/' + iNumeroDocumento + '/' + iEstadoPago + '/' + iIdCliente + '/' + sNombreCliente;
        window.open(url,'_blank');
        
        $( '#btn-excel_saldo_cliente' ).text('');
        $( '#btn-excel_saldo_cliente' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
        $( '#btn-excel_saldo_cliente' ).attr('disabled', false);
      }// /. if all button 
    }// /. if - else validacion
  })// /. btn
})

function pagarDetraccion(ID_Documento_Cabecera) {
  $('#form-pagar_detraccion')[0].reset();
  $('[name="ID_Documento_Cabecera"]').val(ID_Documento_Cabecera);
  $('[name="Fe_Detraccion"]').val('');
  $('[name="Nu_Detraccion"]').val('');
  
  $('.modal-pagar_detraccion').on('shown.bs.modal', function () {
    $('[name="Nu_Detraccion"]').focus();
  })

  $('.date-picker-invoice').val(fDay + '/' + fMonth + '/' + fYear);
  $('.modal-pagar_detraccion').modal('show');
  $('#btn-salir').off('click').click(function () {
    $('.modal-pagar_detraccion').modal('hide');
  });

  $('#btn-pagar_detraccion').off('click').click(function () {
    if ($('[name="Fe_Detraccion"]').val().length == 0) {
      $('[name="Fe_Detraccion"]').closest('.form-group').find('.help-block').html('Ingresar Fecha');
      $('[name="Fe_Detraccion"]').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('.modal-pagar_detraccion .modal-body'), $('[name="Fe_Detraccion"]'));
    } else if ($('[name="Nu_Detraccion"]').val().length == 0) {
      $('[name="Nu_Detraccion"]').closest('.form-group').find('.help-block').html('Ingresar Número');
      $('[name="Nu_Detraccion"]').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('.modal-pagar_detraccion .modal-body'), $('[name="Nu_Detraccion"]'));
    } else {
      $('.help-block').empty();
      $('#txt-ID_Orden_Ingreso_Existe').closest('.form-group').removeClass('has-error');
      $('.modal-pagar_detraccion').modal('hide');

      $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).text('');
      $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).attr('disabled', true);
      $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).append('Enviando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      url = base_url + 'Ventas/informes_venta/SaldoClienteController/pagarDetraccion';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: $('#form-pagar_detraccion').serialize(),
        success: function (response) {
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.sStatus == 'success') {
            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);

            getReporteHTML();
          } else {
            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
          }

          $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).text('');
          $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).append('Pagar');
          $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).attr('disabled', false);
        }
      })
      .fail(function (jqXHR, textStatus, errorThrown) {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');

        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

        $('#modal-loader').modal('hide');

        //Message for developer
        console.log(jqXHR.responseText);

        $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).text('');
        $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).append('Pagar');
        $('#btn-pagar_detraccion-' + ID_Documento_Cabecera).attr('disabled', false);
      });
    }
  });
}

function pagarRetencion(ID_Documento_Cabecera) {
  $('#form-pagar_retencion')[0].reset();
  $('[name="ID_Documento_Cabecera-Retencion"]').val(ID_Documento_Cabecera);
  $('[name="Fe_Voucher_Retencion"]').val('');
  $('[name="Nu_Voucher_Retencion"]').val('');

  $('.modal-pagar_retencion').on('shown.bs.modal', function () {
    $('[name="Nu_Voucher_Retencion"]').focus();
  })

  $('.date-picker-invoice').val(fDay + '/' + fMonth + '/' + fYear);
  $('.modal-pagar_retencion').modal('show');
  $('#btn-salir').off('click').click(function () {
    $('.modal-pagar_retencion').modal('hide');
  });

  $('#btn-pagar_retencion').off('click').click(function () {
    if ($('[name="Fe_Voucher_Retencion"]').val().length == 0) {
      $('[name="Fe_Voucher_Retencion"]').closest('.form-group').find('.help-block').html('Ingresar Fecha');
      $('[name="Fe_Voucher_Retencion"]').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('.modal-pagar_retencion .modal-body'), $('[name="Fe_Voucher_Retencion"]'));
    } else if ($('[name="Nu_Voucher_Retencion"]').val().length == 0) {
      $('[name="Nu_Voucher_Retencion"]').closest('.form-group').find('.help-block').html('Ingresar Número');
      $('[name="Nu_Voucher_Retencion"]').closest('.form-group').removeClass('has-success').addClass('has-error');

      scrollToError($('.modal-pagar_retencion .modal-body'), $('[name="Nu_Voucher_Retencion"]'));
    } else {
      $('.help-block').empty();
      $('#txt-ID_Orden_Ingreso_Existe').closest('.form-group').removeClass('has-error');
      $('.modal-pagar_retencion').modal('hide');

      $('#btn-pagar_retencion-' + ID_Documento_Cabecera).text('');
      $('#btn-pagar_retencion-' + ID_Documento_Cabecera).attr('disabled', true);
      $('#btn-pagar_retencion-' + ID_Documento_Cabecera).append('Enviando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      url = base_url + 'Ventas/informes_venta/SaldoClienteController/pagarRetencion';
      $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: url,
        data: $('#form-pagar_retencion').serialize(),
        success: function (response) {
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');
          $('#modal-message').modal('show');

          if (response.sStatus == 'success') {
            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);

            getReporteHTML();
          } else {
            $('.modal-message').addClass('modal-' + response.sStatus);
            $('.modal-title-message').text(response.sMessage);
            setTimeout(function () { $('#modal-message').modal('hide'); }, 1500);
          }

          $('#btn-pagar_retencion-' + ID_Documento_Cabecera).text('');
          $('#btn-pagar_retencion-' + ID_Documento_Cabecera).append('Pagar');
          $('#btn-pagar_retencion-' + ID_Documento_Cabecera).attr('disabled', false);
        }
      })
        .fail(function (jqXHR, textStatus, errorThrown) {
          $('.modal-message').removeClass('modal-danger modal-warning modal-success');

          $('#modal-message').modal('show');
          $('.modal-message').addClass('modal-danger');
          $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
          setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

          $('#modal-loader').modal('hide');

          //Message for developer
          console.log(jqXHR.responseText);

          $('#btn-pagar_retencion-' + ID_Documento_Cabecera).text('');
          $('#btn-pagar_retencion-' + ID_Documento_Cabecera).append('Pagar');
          $('#btn-pagar_retencion-' + ID_Documento_Cabecera).attr('disabled', false);
        });
    }
  });
}

function getReporteHTML() {
  var Fe_Inicio, Fe_Fin, iIdTipoDocumento, iIdSerieDocumento, iNumeroDocumento, iEstadoPago, iIdCliente, sNombreCliente;

  Fe_Inicio = ParseDateString($('#txt-Filtro_Fe_Inicio').val(), 1, '/');
  Fe_Fin = ParseDateString($('#txt-Filtro_Fe_Fin').val(), 1, '/');
  iIdTipoDocumento = $('#cbo-filtros_tipos_documento').val();
  iIdSerieDocumento = $('#cbo-filtros_series_documento').val();
  iNumeroDocumento = ($('#txt-Filtro_NumeroDocumento').val().length === 0 ? '-' : $('#txt-Filtro_NumeroDocumento').val());
  iEstadoPago = $('#cbo-estado_pago').val();
  iIdCliente = ($('#txt-AID').val().length === 0 ? '-' : $('#txt-AID').val());
  sNombreCliente = ($('#txt-Filtro_Entidad').val().length === 0 ? '-' : $('#txt-Filtro_Entidad').val());

  var arrPost = {
    Fe_Inicio: Fe_Inicio,
    Fe_Fin: Fe_Fin,
    iIdTipoDocumento: iIdTipoDocumento,
    iIdSerieDocumento: iIdSerieDocumento,
    iNumeroDocumento: iNumeroDocumento,
    iEstadoPago: iEstadoPago,
    iIdCliente: iIdCliente,
    sNombreCliente: sNombreCliente,
  };

  $('#btn-html_saldo_cliente').text('');
  $('#btn-html_saldo_cliente').attr('disabled', true);
  $('#btn-html_saldo_cliente').append('Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

  $('#table-saldo_cliente > tbody').empty();
  $('#table-saldo_cliente > tfoot').empty();

  url = base_url + 'Ventas/informes_venta/SaldoClienteController/sendReporte';
  $.post(url, arrPost, function (response) {
    if (response.sStatus == 'success') {
      var iTotalRegistros = response.arrData.length, tr_body = '', tr_foot = '', total_s = 0.00, total_s_saldo = 0.00, sum_total_s = 0.00, sum_total_s_saldo = 0.00, saldo_detraccion = 0.00, saldo_retencion = 0.00;
      var response = response.arrData;
      for (var i = 0; i < iTotalRegistros; i++) {
        total_s = (!isNaN(parseFloat(response[i].Ss_Total)) ? parseFloat(response[i].Ss_Total) : 0);
        total_s_saldo = (!isNaN(parseFloat(response[i].Ss_Total_Saldo)) ? parseFloat(response[i].Ss_Total_Saldo) : 0);
        saldo_detraccion = (!isNaN(parseFloat(response[i].Ss_Saldo_Detraccion)) ? parseFloat(response[i].Ss_Saldo_Detraccion) : 0);
        saldo_retencion = (!isNaN(parseFloat(response[i].Ss_Retencion)) ? parseFloat(response[i].Ss_Retencion) : 0);

        tr_body +=
          "<tr>"
          + "<td class='text-center'>" + response[i].Fe_Emision + "</td>"
          +"<td class='text-center'>" + response[i].Fe_Vencimiento + "</td>"
          +"<td class='text-center'>" + response[i].Dias_Vencimiento + "</td>"
          + "<td class='text-center'>" + response[i].No_Tipo_Documento_Breve + "</td>"
          + "<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
          + "<td class='text-center'>" + response[i].ID_Numero_Documento + "</td>"
          + "<td class='text-left'>" + response[i].No_Entidad + "</td>"
          + "<td class='text-center'>" + response[i].No_Signo + "</td>"
          + "<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_s, 2) + "</td>"
          + "<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_s_saldo, 2) + "</td>"
          + "<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(saldo_detraccion, 2) + "</td>"
          + "<td class='text-center'>" + response[i].sTieneDetraccion + "</td>"
          + "<td class='text-center'>" + response[i].sDetraccion + "</td>"
          + "<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(saldo_retencion, 2) + "</td>"
          + "<td class='text-center'>" + response[i].sTieneRetencion + "</td>"
          + "<td class='text-center'>" + response[i].sRetencion + "</td>"
          + "<td class='text-center'><span class='label label-" + response[i].No_Class_Estado_Pago + "'>" + response[i].No_Estado_Pago + "</span></td>"
          + "<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</span></td>"
          + "</tr>";

        sum_total_s += total_s;
        sum_total_s_saldo += total_s_saldo;
      }

      tr_foot =
        "<tfoot>"
        + "<tr>"
        + "<th class='text-right' colspan='6'>Total</th>"
        + "<th class='text-right'>" + number_format(sum_total_s, 2) + "</th>"
        + "<th class='text-right'>" + number_format(sum_total_s_saldo, 2) + "</th>"
        + "</tr>"
        + "</tfoot>";
    } else {
      if (response.sMessageSQL !== undefined) {
        console.log(response.sMessageSQL);
      }
      tr_body +=
        "<tr>"
        + "<td colspan='9' class='text-center'>" + response.sMessage + "</td>"
        + "</tr>";
    } // ./ if arrData

    $('#div-saldo_cliente').show();
    $('#table-saldo_cliente > tbody').append(tr_body);
    $('#table-saldo_cliente > tbody').after(tr_foot);

    $('#btn-html_saldo_cliente').text('');
    $('#btn-html_saldo_cliente').append('<i class="fa fa-search"></i> Buscar');
    $('#btn-html_saldo_cliente').attr('disabled', false);
  }, 'JSON')
  .fail(function (jqXHR, textStatus, errorThrown) {
    $('.modal-message').removeClass('modal-danger modal-warning modal-success');

    $('#modal-message').modal('show');
    $('.modal-message').addClass('modal-danger');
    $('.modal-title-message').text(textStatus + ' [' + jqXHR.status + ']: ' + errorThrown);
    setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);

    //Message for developer
    console.log(jqXHR.responseText);

    $('#btn-html_saldo_cliente').text('');
    $('#btn-html_saldo_cliente').append('<i class="fa fa-search"></i> Buscar');
    $('#btn-html_saldo_cliente').attr('disabled', false);
  });
}