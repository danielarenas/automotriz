var url;
var table_venta;
var considerar_igv;
var nu_enlace;
var value_importes_cero = 0.00;
var texto_importes_cero = '0.00';
var arrImpuestosProducto = '{ "arrImpuesto" : [';
var arrImpuestosProductoDetalle;
var accion = '';
var bEstadoValidacion;
var detalleItemsFacturaDelete = [];

$('body').on('click', '#btn-cancelar', function() {
  detalleItemsFacturaDelete = [];
});

$( '.date-picker-invoice' ).val(fDay + '/' + fMonth + '/' + fYear);

function agregarVenta(){
  accion = 'add_factura_venta';
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();
  $( '.div-AgregarEditar' ).show();
  
	$( '#txt-EID_Empresa' ).focus();
  
  $( '#form-Venta' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
  $( '.title_Venta' ).text('Nuevo Venta');
  
  $('[name="EID_Empresa"]').val('');
  $('[name="EID_Documento_Cabecera"]').val('');
  $('[name="arrIdPresupuesto"]').val('');

  $( '.date-picker-invoice' ).val(fDay + '/' + fMonth + '/' + fYear);
  
	$( '#cbo-TiposDocumentoModificar' ).html('');
	$( '#cbo-SeriesDocumentoModificar' ).html('');
	$( '#cbo-MotivoReferenciaModificar' ).html('');
		
	$( '#txt-AID' ).val( '' );
	$( '#radio-cliente_varios' ).prop('checked', false).iCheck('update');
  $( '#radio-cliente_existente' ).prop('checked', true).iCheck('update');
  $( '#radio-cliente_nuevo' ).prop('checked', false).iCheck('update');
  
  $( '.div-cliente_existente' ).show();
  $( '.div-cliente_nuevo' ).hide();
  
  $( '#txt-ID_Documento_Guardado' ).val(0);
  $( '.div-DocumentoModificar' ).removeClass('panel-warning panel-danger panel-success');
  $( '.div-mensaje_verificarExisteDocumento' ).removeClass('text-danger text-success');
  $( '.div-mensaje_verificarExisteDocumento' ).text('');
  $( '.div-DocumentoModificar' ).addClass('panel-default');

	$( '#table-DetalleProductosVenta tbody' ).empty();
	
	$( '#panel-DetalleProductosVenta' ).removeClass('panel-danger');
	$( '#panel-DetalleProductosVenta' ).addClass('panel-default');
	
  $( '.div-MediosPago' ).hide();
  
  $('#radio-InactiveDetraccion').prop('checked', true).iCheck('update');
  $('#radio-ActiveDetraccion').prop('checked', false).iCheck('update');

  $('#radio-InactiveRetencion').prop('checked', true).iCheck('update');
  $('#radio-ActiveRetencion').prop('checked', false).iCheck('update');
  
	$( '#txt-subTotal' ).val( value_importes_cero );
	$( '#span-subTotal' ).text( texto_importes_cero );
	
	$( '#txt-exonerada' ).val( value_importes_cero );
	$( '#span-exonerada' ).text( texto_importes_cero );
	
	$( '#txt-inafecto' ).val( value_importes_cero );
	$( '#span-inafecto' ).text( texto_importes_cero );
	
	$( '#txt-gratuita' ).val( value_importes_cero );
	$( '#span-gratuita' ).text( texto_importes_cero );
	
	$( '#txt-impuesto' ).val( value_importes_cero );
	$( '#span-impuesto' ).text( texto_importes_cero );

	$( '#txt-descuento' ).val( value_importes_cero );
	$( '#span-descuento' ).text( texto_importes_cero );
	
	$( '#txt-total' ).val( value_importes_cero );
	$( '#span-total' ).text( texto_importes_cero );

  $( '.span-signo' ).text( 'S/' );

  $( '#btn-save' ).attr('disabled', false);
  
  considerar_igv=0;
  
  url = base_url + 'HelperController/getTiposDocumentos';
  $.post( url, {Nu_Tipo_Filtro : 3}, function( response ){//1 = Venta
    $( '#cbo-TiposDocumento' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-TiposDocumento' ).append( '<option value="' + response[i].ID_Tipo_Documento + '" data-nu_impuesto="' + response[i].Nu_Impuesto + '" data-nu_enlace="' + response[i].Nu_Enlace + '">' + response[i].No_Tipo_Documento_Breve + '</option>' );
  }, 'JSON');

  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post( url , function( response ){
    $( '#cbo-TiposDocumentoIdentidadCliente' ).html('');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-TiposDocumentoIdentidadCliente' ).append( '<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>' );
  }, 'JSON');
  
	// Tipos de comprobantes
	$( '.div-DocumentoModificar' ).hide();
	$( '#cbo-TiposDocumento' ).change(function(){
    if ( $( '#cbo-almacen' ).val() > 0 ) {
      $( '#cbo-SeriesDocumento' ).html('');
      $( '#txt-ID_Numero_Documento' ).val('');
      $( '.div-DocumentoModificar' ).hide();
      if ( $(this).val() > 0 ) {
        considerar_igv = $(this).find(':selected').data('nu_impuesto');
        nu_enlace = $(this).find(':selected').data('nu_enlace');
        if (nu_enlace == 1) {//Validar N/C y N/D
          $( '.div-DocumentoModificar' ).show();

          url = base_url + 'HelperController/getTiposDocumentosModificar';
          $.post( url, {Nu_Tipo_Filtro : 1}, function( response ){
            $( '#cbo-TiposDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-TiposDocumentoModificar' ).append( '<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>' );
          }, 'JSON');
          
          //Motivos de referencia  
          url = base_url + 'HelperController/getMotivosReferenciaModificar';
          $.post( url, {ID_Tipo_Documento: $(this).val()}, function( response ){
            $( '#cbo-MotivoReferenciaModificar' ).html('');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-MotivoReferenciaModificar' ).append( '<option value="' + response[i].Nu_Valor + '">' + response[i].No_Descripcion + '</option>' );
          }, 'JSON');
        }// /. Validación de N/C y N/D

        url = base_url + 'HelperController/getSeriesDocumentoxAlmacen';
        $.post( url, {ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Almacen : $( '#cbo-almacen' ).val(), ID_Tipo_Documento: $(this).val()}, function( response ){
          if (response.length === 1) {
            $( '#cbo-SeriesDocumento' ).html( '<option value="' + response[0].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>' );	    
            //Get número cuando solo haya una serie por un tipo de documento
            $( '#txt-ID_Numero_Documento' ).val('');
            url = base_url + 'HelperController/getNumeroDocumento';
            $.post( url, { ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Almacen : $( '#cbo-almacen' ).val(), ID_Tipo_Documento: $( '#cbo-TiposDocumento' ).val(), ID_Serie_Documento: response[0].ID_Serie_Documento }, function( responseNumeros ){
              if (responseNumeros.length === 0)
                $( '#txt-ID_Numero_Documento' ).val('');
              else
                $( '#txt-ID_Numero_Documento' ).val(responseNumeros.ID_Numero_Documento);
            }, 'JSON');
            //Fin número
            $( '#cbo-SeriesDocumento' ).html( '<option value="' + response[0].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>' );	    
          } else if (response.length > 1) {
            $( '#cbo-SeriesDocumento' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-SeriesDocumento' ).append( '<option value="' + response[i].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[i].ID_Serie_Documento_PK + '>' + response[i].ID_Serie_Documento + '</option>' );
          } else
            $( '#cbo-SeriesDocumento' ).html('<option value="0" selected="selected">Sin serie</option>');
        }, 'JSON');
        
        var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
        var $Ss_SubTotal = 0.00;
        var $Ss_Exonerada = 0.00;
        var $Ss_Inafecto = 0.00;
        var $Ss_Gratuita = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        var iCantDescuento = 0;
        var globalImpuesto = 0;
        var $Ss_Descuento_p = 0;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;
    
          if (iGrupoImpuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            globalImpuesto = fImpuesto;
          } else if (iGrupoImpuesto == 2) {
            $Ss_Inafecto += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          } else if (iGrupoImpuesto == 3) {
            $Ss_Exonerada += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          } else {
            $Ss_Gratuita += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          }
            
          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto) )) / 100);
        });
        
        if ($Ss_Descuento > 0.00) {
          var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
          if ($Ss_SubTotal > 0.00) {
            $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
            $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
            $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
            $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
          }
    
          if ($Ss_Inafecto > 0.00) {
            $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
            $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
            $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
          }
          
          if ($Ss_Exonerada > 0.00) {
            $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
            $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
            $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
          }
          
          if ($Ss_Gratuita > 0.00) {
            $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
            $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
            $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
          }
          
          $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
          $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
        } else
          $Ss_Descuento = $Ss_Descuento_p;
    
        if(isNaN($Ss_Descuento))
          $Ss_Descuento = 0.00;
        
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
        $( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
        
        $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
        $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
        
        $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
        $( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
        
        $( '#txt-gratuita' ).val( $Ss_Inafecto.toFixed(2) );
        $( '#span-gratuita' ).text( $Ss_Inafecto.toFixed(2) );
          
        $( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
        $( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
        
        $( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
        $( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    
        $( '#txt-total' ).val( $Ss_Total.toFixed(2) );
        $( '#span-total' ).text( $Ss_Total.toFixed(2) );
      }
    } else {
      $( '#cbo-almacen' ).closest('.form-group').find('.help-block').html('Seleccionar almacén');
  	  $( '#cbo-almacen' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    }// /. if - else validacion de seleccionar almacen
	})
	// /. Tipos de comprobantes
	
  url = base_url + 'HelperController/getMonedas';
  $.post( url , function( response ){
    $( '#cbo-Monedas' ).html('');
    $( '.span-signo' ).text(response[0]['No_Signo']);
    for (var i = 0; i < response.length; i++)
      $( '#cbo-Monedas' ).append( '<option value="' + response[i]['ID_Moneda'] + '" data-no_signo="' + response[i]['No_Signo'] + '">' + response[i]['No_Moneda'] + '</option>' );
  }, 'JSON');
	
  url = base_url + 'HelperController/getMediosPago';
  $.post( url , function( response ){
    $( '#cbo-MediosPago' ).html('');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-MediosPago' ).append( '<option value="' + response[i]['ID_Medio_Pago'] + '" data-nu_tipo="' + response[i]['Nu_Tipo'] + '">' + response[i]['No_Medio_Pago'] + '</option>' );
  }, 'JSON');
  
  $( '#txt-Fe_Emision' ).datepicker({}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $( '#txt-Fe_Vencimiento' ).datepicker('setStartDate', minDate);
  });

  var Fe_Emision = $( '#txt-Fe_Emision' ).val().split('/');
  $( '#txt-Fe_Vencimiento' ).datepicker({
    autoclose : true,
    startDate : new Date(Fe_Emision[2], Fe_Emision[1] - 1, Fe_Emision[0]),
    todayHighlight: true
  })
  
  $( '#txt-Fe_Vencimiento' ).val($( '#txt-Fe_Emision' ).val());

  $('#txt-Fe_Entrega_Factura').datepicker({}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#txt-Fe_Entrega_Vencimiento_Factura').datepicker('setStartDate', minDate);
  });

  $('#txt-Fe_Entrega_Factura').val($('#txt-Fe_Emision').val());

  $('#txt-Fe_Entrega_Vencimiento_Factura').datepicker({
    autoclose: true,
    startDate: new Date(Fe_Emision[2], Fe_Emision[1] - 1, Fe_Emision[0]),
    todayHighlight: true
  })

  $('#txt-Fe_Entrega_Vencimiento_Factura').val($('#txt-Fe_Emision').val());

	/* Personal de ventas */
  $( '#cbo-vendedor' ).html('<option value="" selected="selected">- No hay personal -</option>');
  url = base_url + 'HelperController/getDataGeneral';
  $.post( url, {sTipoData: 'entidad', iTipoEntidad: 4}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      if (l==1) {
        $( '#cbo-vendedor' ).html('<option value="" selected="selected">- Seleccionar -</option>');
        $( '#cbo-vendedor' ).append( '<option value="' + response.arrData[0].ID + '">' + response.arrData[0].Nombre + '</option>' );
      } else {
        $( '#cbo-vendedor' ).html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var x = 0; x < l; x++) {
          $( '#cbo-vendedor' ).append( '<option value="' + response.arrData[x].ID + '">' + response.arrData[x].Nombre + '</option>' );
        }
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      console.log(response.sMessage);
    }
  }, 'JSON');
  /* /. Personal de ventas */

	/* Porcentaje para ventas */
  $( '#cbo-porcentaje' ).html('<option value="" selected="selected">- No hay porcentaje -</option>');
  url = base_url + 'HelperController/getDataGeneral';
  $.post( url, {sTipoData: 'Porcentaje_Comision_Vendedores'}, function( response ){
    if ( response.sStatus == 'success' ) {
      var l = response.arrData.length;
      if (l==1) {
        $( '#cbo-porcentaje' ).html('<option value="" selected="selected">- Seleccionar -</option>');
        $( '#cbo-porcentaje' ).append( '<option value="' + response.arrData[0].ID_Tabla_Dato + '">' + response.arrData[0].No_Descripcion + '</option>' );
      } else {
        $( '#cbo-porcentaje' ).html('<option value="" selected="selected">- Seleccionar -</option>');
        for (var x = 0; x < l; x++) {
          $( '#cbo-porcentaje' ).append( '<option value="' + response.arrData[x].ID_Tabla_Dato + '">' + response.arrData[x].No_Descripcion + '</option>' );
        }
      }
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      console.log(response.sMessage);
    }
  }, 'JSON');
  /* /. Porcentaje para ventas */
	
  $( '#cbo-descargar_stock' ).html( '<option value="1">Si</option>' );
  $( '#cbo-descargar_stock' ).append( '<option value="0">No</option>' );

  $('.div-modal_datos_tarjeta_credito').hide();

  $('#cbo-tipos_servicios').html('<option value="" selected="selected">- No hay registros -</option>');
  url = base_url + 'HelperController/getTiposServicios';
  $.post(url, { }, function (response) {
    if (response.sStatus == 'success') {
      $('#cbo-tipos_servicios').html('<option value="" selected="selected">- Seleccionar -</option>');
      var l = response.arrData.length;
      if (l == 1) {
        $('#cbo-tipos_servicios').append('<option value="' + response.arrData[0].ID_Servicio_Presupuesto_Negocio + '">' + response.arrData[0].No_Servicio_Presupuesto_Negocio + '</option>');
      } else {
        for (var x = 0; x < l; x++) {
          $('#cbo-tipos_servicios').append('<option value="' + response.arrData[x].ID_Servicio_Presupuesto_Negocio + '">' + response.arrData[x].No_Servicio_Presupuesto_Negocio + '</option>');
        }
      }
    } else {
      if (response.sMessageSQL !== undefined) {
        console.log(response.sMessageSQL);
      }
      console.log(response.sMessage);
    }
  }, 'JSON');

  var arrParams = {};
  getAlmacenes(arrParams);
  $( '.div-almacen' ).show();

  $( '#table-DetalleProductosVenta' ).hide();
  
  url = base_url + 'HelperController/getImpuestos';
  $.post( url , function( response ){
    arrImpuestosProducto = '';
    arrImpuestosProductoDetalle = '';
    for (var i = 0; i < response.length; i++)
      arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
    arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';
    
    $( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
  var _ID_Producto = '';
  var option_impuesto_producto = '';
}

function verFacturaVenta(ID, Nu_Documento_Identidad){
  accion = 'upd_factura_venta';
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();

	$( '#txt-EID_Empresa' ).focus();
	
  $( '#form-Venta' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
  $( '#txt-AID' ).val( '' );
  $( '#radio-cliente_varios' ).prop('checked', false).iCheck('update');
  $( '#radio-cliente_existente' ).prop('checked', true).iCheck('update');
  $( '#radio-cliente_nuevo' ).prop('checked', false).iCheck('update');
  
  $( '.div-cliente_existente' ).show();
  $( '.div-cliente_nuevo' ).hide();
  
	$( '.div-DocumentoModificar' ).hide();
 
  $( '#txt-ID_Documento_Guardado' ).val(1);
  $( '.div-DocumentoModificar' ).removeClass('panel-warning panel-danger panel-success');
  $( '.div-mensaje_verificarExisteDocumento' ).removeClass('text-danger text-success');
  $( '.div-mensaje_verificarExisteDocumento' ).text('');
  $( '.div-DocumentoModificar' ).addClass('panel-default');
 
	$( '#panel-DetalleProductosVenta' ).removeClass('panel-danger');
	$( '#panel-DetalleProductosVenta' ).addClass('panel-default');
  
	$( '#txt-subTotal' ).val( value_importes_cero );
	$( '#span-subTotal' ).text( texto_importes_cero );
	
	$( '#txt-exonerada' ).val( value_importes_cero );
	$( '#span-exonerada' ).text( texto_importes_cero );
	
	$( '#txt-inafecto' ).val( value_importes_cero );
	$( '#span-inafecto' ).text( texto_importes_cero );
	
	$( '#txt-gratuita' ).val( value_importes_cero );
	$( '#span-gratuita' ).text( texto_importes_cero );
	
	$( '#txt-impuesto' ).val( value_importes_cero );
	$( '#span-impuesto' ).text( texto_importes_cero );
	
	$( '#txt-descuento' ).val( value_importes_cero );
	$( '#span-descuento' ).text( texto_importes_cero );
	
	$( '#txt-total' ).val( value_importes_cero );
	$( '#span-total' ).text( texto_importes_cero );

  $( '#btn-save' ).attr('disabled', false);

  considerar_igv=0;
  
  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post( url , function( response ){
    $( '#cbo-TiposDocumentoIdentidadCliente' ).html('');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-TiposDocumentoIdentidadCliente' ).append( '<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>' );
  }, 'JSON');
  
	$( '#cbo-TiposDocumento' ).change(function(){
    if ( $( '#cbo-almacen' ).val() > 0 ) {
      $( '#cbo-SeriesDocumento' ).html('');
      $( '#txt-ID_Numero_Documento' ).val('');
      $( '.div-DocumentoModificar' ).hide();
      if ( $(this).val() > 0 ) {
        considerar_igv = $(this).find(':selected').data('nu_impuesto');
        nu_enlace = $(this).find(':selected').data('nu_enlace');
        if (nu_enlace == 1) {//Validar N/C y N/D
          $( '.div-DocumentoModificar' ).show();

          url = base_url + 'HelperController/getTiposDocumentosModificar';
          $.post( url, {Nu_Tipo_Filtro : 1}, function( response ){
            $( '#cbo-TiposDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-TiposDocumentoModificar' ).append( '<option value="' + response[i]['ID_Tipo_Documento'] + '">' + response[i]['No_Tipo_Documento_Breve'] + '</option>' );
          }, 'JSON');
          
          //Motivos de referencia  
          url = base_url + 'HelperController/getMotivosReferenciaModificar';
          $.post( url, {ID_Tipo_Documento: $(this).val()}, function( response ){
            $( '#cbo-MotivoReferenciaModificar' ).html('');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-MotivoReferenciaModificar' ).append( '<option value="' + response[i].Nu_Valor + '">' + response[i].No_Descripcion + '</option>' );
          }, 'JSON');
        }// /. Validación de N/C y N/D

        url = base_url + 'HelperController/getSeriesDocumentoxAlmacen';
        $.post( url, {ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Almacen : $( '#cbo-almacen' ).val(), ID_Tipo_Documento: $(this).val()}, function( response ){
          if (response.length === 1) {
            $( '#cbo-SeriesDocumento' ).html( '<option value="' + response[0].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>' );	    
            //Get número cuando solo haya una serie por un tipo de documento
            $( '#txt-ID_Numero_Documento' ).val('');
            url = base_url + 'HelperController/getNumeroDocumentoxAlmacen';
            $.post( url, { ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Almacen : $( '#cbo-almacen' ).val(), ID_Tipo_Documento: $( '#cbo-TiposDocumento' ).val(), ID_Serie_Documento: response[0].ID_Serie_Documento }, function( responseNumeros ){
              if (responseNumeros.length === 0)
                $( '#txt-ID_Numero_Documento' ).val('');
              else
                $( '#txt-ID_Numero_Documento' ).val(responseNumeros.ID_Numero_Documento);
            }, 'JSON');
            //Fin número
            $( '#cbo-SeriesDocumento' ).html( '<option value="' + response[0].ID_Serie_Documento + '"data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>' );	    
          } else if (response.length > 1) {
            $( '#cbo-SeriesDocumento' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-SeriesDocumento' ).append( '<option value="' + response[i].ID_Serie_Documento + '"data-id_serie_documento_pk=' + response[i].ID_Serie_Documento_PK + '>' + response[i].ID_Serie_Documento + '</option>' );
          } else
            $( '#cbo-SeriesDocumento' ).html('<option value="0" selected="selected">Sin serie</option>');
        }, 'JSON');
        
        var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
        var $Ss_SubTotal = 0.00;
        var $Ss_Exonerada = 0.00;
        var $Ss_Inafecto = 0.00;
        var $Ss_Gratuita = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        var iCantDescuento = 0;
        var globalImpuesto = 0;
        var $Ss_Descuento_p = 0;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / fImpuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;
    
          if (iGrupoImpuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            globalImpuesto = fImpuesto;
          } else if (iGrupoImpuesto == 2) {
            $Ss_Inafecto += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          } else if (iGrupoImpuesto == 3) {
            $Ss_Exonerada += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          } else {
            $Ss_Gratuita += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          }
            
          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto) )) / 100);
        });
        
        if ($Ss_Descuento > 0.00) {
          var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
          if ($Ss_SubTotal > 0.00) {
            $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
            $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
            $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
            $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
          }
    
          if ($Ss_Inafecto > 0.00) {
            $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
            $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
            $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
          }
          
          if ($Ss_Exonerada > 0.00) {
            $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
            $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
            $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
          }
          
          if ($Ss_Gratuita > 0.00) {
            $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
            $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
            $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
          }
          
          $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
          $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
        } else
          $Ss_Descuento = $Ss_Descuento_p;
    
        if(isNaN($Ss_Descuento))
          $Ss_Descuento = 0.00;
        
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
        $( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
        
        $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
        $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
        
        $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
        $( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
        
        $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
        $( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
          
        $( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
        $( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
        
        $( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
        $( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    
        $( '#txt-total' ).val( $Ss_Total.toFixed(2) );
        $( '#span-total' ).text( $Ss_Total.toFixed(2) );
      }
    } else {
      $( '#cbo-almacen' ).closest('.form-group').find('.help-block').html('Seleccionar almacén');
      $( '#cbo-almacen' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    }// /. if - else validacion de seleccionar almacen
	})

  $('[name="arrIdPresupuesto"]').val('');

  url = base_url + 'Ventas/VentaController/ajax_edit/' + ID;
  $.ajax({
    url : url,
    type: "GET",
    dataType: "JSON",
    success: function(response){
      $( '.div-AgregarEditar' ).show();
      $( '.title_Venta' ).text('Modifcar Venta');
      $('[name="EID_Empresa"]').val(response.arrEdit[0].ID_Empresa);
      $('[name="EID_Documento_Cabecera"]').val(response.arrEdit[0].ID_Documento_Cabecera);
      $('[name="ID_Documento_Cabecera_Orden"]').val('');
      if ( response.arrEdit[0].ID_Tipo_Documento == 3 || response.arrEdit[0].ID_Tipo_Documento == 4 )
        $('[name="ID_Documento_Cabecera_Orden"]').val(response.arrEdit[0].ID_Documento_Cabecera_Enlace);

      //Datos Cliente
      $('[name="AID"]').val(response.arrEdit[0].ID_Entidad);
      $('[name="ANombre"]').val(response.arrEdit[0].No_Entidad);
      $('[name="ACodigo"]').val(response.arrEdit[0].Nu_Documento_Identidad);
      $('[name="Txt_Direccion_Entidad"]').val(response.arrEdit[0].Txt_Direccion_Entidad);

      //Datos Documento
      considerar_igv = response.arrEdit[0].Nu_Impuesto;

      url = base_url + 'HelperController/getOrganizaciones';
      $.post( url, function( responseOrganizaciones ){
        $( '#cbo-OrganizacionesVenta' ).html('');
        for (var i = 0; i < responseOrganizaciones.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Organizacion == responseOrganizaciones[i].ID_Organizacion)
            selected = 'selected="selected"';
          $( '#cbo-OrganizacionesVenta' ).append( '<option value="' + responseOrganizaciones[i].ID_Organizacion + '" ' + selected + '>' + responseOrganizaciones[i].No_Organizacion + '</option>' );
        }
      }, 'JSON');

	    nu_enlace = response.arrEdit[0].Nu_Enlace;
      url = base_url + 'HelperController/getTiposDocumentos';
      $.post( url, {Nu_Tipo_Filtro : 3}, function( responseTiposDocumento ){
        $( '#cbo-TiposDocumento' ).html('');
        for (var i = 0; i < responseTiposDocumento.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Tipo_Documento == responseTiposDocumento[i]['ID_Tipo_Documento'])
            selected = 'selected="selected"';
          $( '#cbo-TiposDocumento' ).append( '<option value="' + responseTiposDocumento[i]['ID_Tipo_Documento'] + '" data-nu_impuesto="' + responseTiposDocumento[i]['Nu_Impuesto'] + '" data-nu_enlace="' + responseTiposDocumento[i]['Nu_Enlace'] + '" ' + selected + '>' + responseTiposDocumento[i]['No_Tipo_Documento_Breve'] + '</option>' );
        }
      }, 'JSON');

		  url = base_url + 'HelperController/getSeriesDocumento';
      $.post( url, { ID_Organizacion : response.arrEdit[0].ID_Organizacion, ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento }, function( responseSeriesDocumento ){
        $( '#cbo-SeriesDocumento' ).html( '' );
        for (var i = 0; i < responseSeriesDocumento.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Serie_Documento == responseSeriesDocumento[i]['ID_Serie_Documento'])
            selected = 'selected="selected"';
          $( '#cbo-SeriesDocumento' ).append( '<option value="' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '" ' + selected + ' data-id_serie_documento_pk=' + responseSeriesDocumento[i].ID_Serie_Documento_PK + '>' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '</option>' );
        }
      }, 'JSON');

      $('[name="ID_Numero_Documento"]').val(response.arrEdit[0].ID_Numero_Documento);
      $( '[name="Fe_Emision"]' ).val(ParseDateString(response.arrEdit[0].Fe_Emision, 6, '-'));

      url = base_url + 'HelperController/getMonedas';
      $.post( url , function( responseMonedas ){
        $( '#cbo-Monedas' ).html('');
        for (var i = 0; i < responseMonedas.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Moneda == responseMonedas[i]['ID_Moneda']){
            selected = 'selected="selected"';
	          $( '.span-signo' ).text( responseMonedas[i]['No_Signo'] );
          }
          $( '#cbo-Monedas' ).append( '<option value="' + responseMonedas[i]['ID_Moneda'] + '" data-no_signo="' + responseMonedas[i]['No_Signo'] + '" ' + selected + '>' + responseMonedas[i]['No_Moneda'] + '</option>' );
        }
      }, 'JSON');

      url = base_url + 'HelperController/getMediosPago';
      $.post( url , function( responseMediosPago ){
        $( '#cbo-MediosPago' ).html('');
        for (var i = 0; i < responseMediosPago.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Medio_Pago == responseMediosPago[i]['ID_Medio_Pago'])
            selected = 'selected="selected"';
          $( '#cbo-MediosPago' ).append( '<option value="' + responseMediosPago[i]['ID_Medio_Pago'] + '" data-nu_tipo="' + responseMediosPago[i]['Nu_Tipo'] + '" ' + selected + '>' + responseMediosPago[i]['No_Medio_Pago'] + '</option>' );
        }
      }, 'JSON');

  	  if ( response.arrEdit[0].Nu_Tipo == 1)// Si es Crédito
  	    $( '.div-MediosPago' ).show();

      $('.div-modal_datos_tarjeta_credito').hide();
      if (response.arrEdit[0].ID_Tipo_Medio_Pago > 0) {
        $('.div-modal_datos_tarjeta_credito').show();
        url = base_url + 'HelperController/getTiposTarjetaCredito';
        $.post(url, { ID_Medio_Pago: response.arrEdit[0].ID_Medio_Pago }, function (responseTipoTarjetCredito) {
          $('#cbo-tarjeta_credito').html('');
          for (var i = 0; i < responseTipoTarjetCredito.length; i++) {
            selected = '';
            if (response.arrEdit[0].ID_Tipo_Medio_Pago == responseTipoTarjetCredito[i].ID_Tipo_Medio_Pago)
              selected = 'selected="selected"';
            $('#cbo-tarjeta_credito').append('<option value="' + responseTipoTarjetCredito[i].ID_Tipo_Medio_Pago + '" ' + selected + '>' + responseTipoTarjetCredito[i].No_Tipo_Medio_Pago + '</option>');
          }
        }, 'JSON');
      }

      $( '#txt-Fe_Emision' ).datepicker({}).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $( '#txt-Fe_Vencimiento' ).datepicker('setStartDate', minDate);
      });

      var Fe_Emision = response.arrEdit[0].Fe_Emision.split('-');
      $( '#txt-Fe_Vencimiento' ).datepicker({
        autoclose : true,
        startDate : new Date(parseInt(Fe_Emision[0]), parseInt(Fe_Emision[1]) - 1, parseInt(Fe_Emision[2])),
        todayHighlight: true
      })
      $( '#txt-Fe_Vencimiento' ).datepicker('setStartDate', new Date(Fe_Emision[0] + "/" + Fe_Emision[1] + "/" + Fe_Emision[2]) );
      $( '#txt-Fe_Vencimiento' ).val(ParseDateString(response.arrEdit[0].Fe_Vencimiento, 6, '-'));

      $('#txt-Fe_Entrega_Factura').datepicker({}).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#txt-Fe_Entrega_Vencimiento_Factura').datepicker('setStartDate', minDate);
      });
      $('#txt-Fe_Entrega_Factura').datepicker('setStartDate', new Date(Fe_Emision[0] + "/" + Fe_Emision[1] + "/" + Fe_Emision[2]));
      if (response.arrEdit[0].Fe_Entrega_Factura != '' && response.arrEdit[0].Fe_Entrega_Factura != null)
        $('#txt-Fe_Entrega_Factura').val(ParseDateString(response.arrEdit[0].Fe_Entrega_Factura, 6, '-'));

      $('#txt-Fe_Entrega_Vencimiento_Factura').datepicker({
        autoclose: true,
        todayHighlight: true
      })
      $('#txt-Fe_Entrega_Vencimiento_Factura').datepicker('setStartDate', new Date(Fe_Emision[0] + "/" + Fe_Emision[1] + "/" + Fe_Emision[2]));
      if (response.arrEdit[0].Fe_Entrega_Vencimiento_Factura != '' && response.arrEdit[0].Fe_Entrega_Vencimiento_Factura != null)
        $('#txt-Fe_Entrega_Vencimiento_Factura').val(ParseDateString(response.arrEdit[0].Fe_Entrega_Vencimiento_Factura, 6, '-'));

  	  //Validar N/C y N/D
	    if (response.arrEdit[0].Nu_Enlace == 1) {
	      $( '.div-DocumentoModificar' ).show();

        url = base_url + 'HelperController/getTiposDocumentosModificar';
        $.post( url, {Nu_Tipo_Filtro : 1}, function( responseTiposDocumentoModificar ){
          $( '#cbo-TiposDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
          for (var i = 0; i < responseTiposDocumentoModificar.length; i++){
            selected = '';
            if(response.arrEdit[0].ID_Tipo_Documento_Modificar == responseTiposDocumentoModificar[i]['ID_Tipo_Documento'])
              selected = 'selected="selected"';
            $( '#cbo-TiposDocumentoModificar' ).append( '<option value="' + responseTiposDocumentoModificar[i]['ID_Tipo_Documento'] + '" ' + selected + '>' + responseTiposDocumentoModificar[i]['No_Tipo_Documento_Breve'] + '</option>' );
          }
        }, 'JSON');

        url = base_url + 'HelperController/getMotivosReferenciaModificar';
        $.post( url, { ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento }, function( responseMotivosReferencia ){
          $( '#cbo-MotivoReferenciaModificar' ).html('');
          for (var i = 0; i < responseMotivosReferencia.length; i++){
            selected = '';
            if(response.arrEdit[0].Nu_Codigo_Motivo_Referencia == responseMotivosReferencia[i]['Nu_Valor'])
              selected = 'selected="selected"';
            $( '#cbo-MotivoReferenciaModificar' ).append( '<option value="' + responseMotivosReferencia[i]['Nu_Valor'] + '" ' + selected + '>' + responseMotivosReferencia[i]['No_Descripcion'] + '</option>' );
          }
        }, 'JSON');

  		  url = base_url + 'HelperController/getSeriesDocumentoModificar';
        $.post( url, { ID_Organizacion : response.arrEdit[0].ID_Organizacion, ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento_Modificar }, function( responseSeriesDocumentoModificar ){
          for (var i = 0; i < responseSeriesDocumentoModificar.length; i++){
            selected = '';
            if(response.arrEdit[0].ID_Serie_Documento_Modificar == responseSeriesDocumentoModificar[i]['ID_Serie_Documento'])
              selected = 'selected="selected"';
            $( '#cbo-SeriesDocumentoModificar' ).append( '<option value="' + responseSeriesDocumentoModificar[i]['ID_Serie_Documento'] + '" ' + selected + ' data-id_serie_documento_pk="' + responseSeriesDocumentoModificar[i]['ID_Serie_Documento_PK'] + '">' + responseSeriesDocumentoModificar[i]['ID_Serie_Documento'] + '</option>' );
          }
        }, 'JSON');

        $( '#txt-ID_Numero_Documento_Modificar' ).val(response.arrEdit[0].ID_Numero_Documento_Modificar);
	    }

      var arrParams = {
        ID_Almacen : response.arrEdit[0].ID_Almacen,
      }
      getAlmacenes( arrParams );

      if ( response.arrEdit[0].Nu_Descargar_Inventario == 1 ) {
        $( '#cbo-descargar_stock' ).html( '<option value="1" selected>Si</option>' );
        $( '#cbo-descargar_stock' ).append( '<option value="0">No</option>' );
      } else {
        $( '#cbo-descargar_stock' ).html( '<option value="1">Si</option>' );
        $( '#cbo-descargar_stock' ).append( '<option value="0" selected>No</option>' );
      }

      $('#txt-Nu_Presupuesto').val(response.arrEdit[0].Nu_Presupuesto);
      $('#txt-Nu_Orden_Compra').val(response.arrEdit[0].Nu_Orden_Compra);

      $('#cbo-tipos_servicios').html('<option value="">- No hay registros -</option>');
      url = base_url + 'HelperController/getTiposServicios';
      $.post(url, { }, function (responseTiposServicios) {
        if (responseTiposServicios.sStatus == 'success') {
          var l = responseTiposServicios.arrData.length;
          if (l == 1) {
            $('#cbo-tipos_servicios').html('<option value="">- Seleccionar -</option>');
            selected = '';
            if (response.arrEdit[0].ID_Servicio_Presupuesto_Negocio == responseTiposServicios.arrData[0].ID_Servicio_Presupuesto_Negocio)
              selected = 'selected="selected"';
            $('#cbo-tipos_servicios').append('<option value="' + responseTiposServicios.arrData[0].ID_Servicio_Presupuesto_Negocio + '" ' + selected + '>' + responseTiposServicios.arrData[0].No_Servicio_Presupuesto_Negocio + '</option>');
          } else {
            $('#cbo-tipos_servicios').html('<option value="" selected="selected">- Seleccionar -</option>');
            for (var x = 0; x < l; x++) {
              selected = '';
              if (response.arrEdit[0].ID_Servicio_Presupuesto_Negocio == responseTiposServicios.arrData[x].ID_Servicio_Presupuesto_Negocio)
                selected = 'selected="selected"';
              $('#cbo-tipos_servicios').append('<option value="' + responseTiposServicios.arrData[x].ID_Servicio_Presupuesto_Negocio + '" ' + selected + '>' + responseTiposServicios.arrData[x].No_Servicio_Presupuesto_Negocio + '</option>');
            }
          }
        } else {
          if (responseTiposServicios.sMessageSQL !== undefined) {
            console.log(responseTiposServicios.sMessageSQL);
          }
          console.log(responseTiposServicios.sMessage);
        }
      }, 'JSON');


    	/* Personal de ventas */
      $( '#cbo-vendedor' ).html('<option value="">- No hay personal -</option>');
      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData: 'entidad', iTipoEntidad: 4}, function( responsePersonal ){
        if ( responsePersonal.sStatus == 'success' ) {
          var l = responsePersonal.arrData.length;
          if (l==1) {
            $( '#cbo-vendedor' ).html('<option value="">- Seleccionar -</option>');
            selected = '';
            if(response.arrEdit[0].ID_Mesero == responsePersonal.arrData[0].ID)
              selected = 'selected="selected"';
            $( '#cbo-vendedor' ).append( '<option value="' + responsePersonal.arrData[0].ID + '" ' + selected + '>' + responsePersonal.arrData[0].Nombre + '</option>' );
          } else {
            $( '#cbo-vendedor' ).html('<option value="" selected="selected">- Seleccionar -</option>');
            for (var x = 0; x < l; x++) {
              selected = '';
              if(response.arrEdit[0].ID_Mesero == responsePersonal.arrData[x].ID)
                selected = 'selected="selected"';
              $( '#cbo-vendedor' ).append( '<option value="' + responsePersonal.arrData[x].ID + '" ' + selected + '>' + responsePersonal.arrData[x].Nombre + '</option>' );
            }
          }
        } else {
          if( responsePersonal.sMessageSQL !== undefined ) {
            console.log(responsePersonal.sMessageSQL);
          }
          console.log(responsePersonal.sMessage);
        }
      }, 'JSON');
      /* /. Personal de ventas */

    	/* Porcentaje para ventas */
      $( '#cbo-porcentaje' ).html('<option value="" selected="selected">- No hay porcentaje -</option>');
      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData: 'Porcentaje_Comision_Vendedores'}, function( responsePorcentaje ){
        if ( responsePorcentaje.sStatus == 'success' ) {
          var l = responsePorcentaje.arrData.length;
          if (l==1) {
            $( '#cbo-vendedor' ).html('<option value="">- Seleccionar -</option>');
            selected = '';
            if(response.arrEdit[0].ID_Comision == responsePorcentaje.arrData[0].ID_Tabla_Dato)
              selected = 'selected="selected"';
            $( '#cbo-porcentaje' ).append( '<option value="' + responsePorcentaje.arrData[0].ID_Tabla_Dato + '" ' + selected + '>' + responsePorcentaje.arrData[0].No_Descripcion + '</option>' );
          } else {
            $( '#cbo-porcentaje' ).html('<option value="" selected="selected">- Seleccionar -</option>');
            for (var x = 0; x < l; x++) {
              selected = '';
              if(response.arrEdit[0].ID_Comision == responsePorcentaje.arrData[x].ID_Tabla_Dato)
                selected = 'selected="selected"';
              $( '#cbo-porcentaje' ).append( '<option value="' + responsePorcentaje.arrData[x].ID_Tabla_Dato + '" ' + selected + '>' + responsePorcentaje.arrData[x].No_Descripcion + '</option>' );
            }
          }
        } else {
          if( responsePorcentaje.sMessageSQL !== undefined ) {
            console.log(responsePorcentaje.sMessageSQL);
          }
          console.log(responsePorcentaje.sMessage);
        }
      }, 'JSON');
      /* /. Porcentaje para ventas */

	    $( '#cbo-lista_precios' ).html('');
      url = base_url + 'HelperController/getListaPrecio';
      $.post( url, {Nu_Tipo_Lista_Precio : $( '[name="Nu_Tipo_Lista_Precio"]' ).val(), ID_Organizacion: response.arrEdit[0].ID_Organizacion, ID_Almacen : response.arrEdit[0].ID_Almacen}, function( responseLista ){
        for (var i = 0; i < responseLista.length; i++) {
          selected = '';
          if(response.arrEdit[0].ID_Lista_Precio_Cabecera == responseLista[i].ID_Lista_Precio_Cabecera)
            selected = 'selected="selected"';
          $( '#cbo-lista_precios' ).append( '<option value="' + responseLista[i].ID_Lista_Precio_Cabecera + '" ' + selected + '>' + responseLista[i].No_Lista_Precio + '</option>' );
        }
      }, 'JSON');

      $('[name="Txt_Glosa"]').val( '' );
      if (response.arrEdit[0].Txt_Glosa != '' && response.arrEdit[0].Txt_Glosa != null)
        $('[name="Txt_Glosa"]').val( clearHTMLTextArea(response.arrEdit[0].Txt_Glosa) );

      $('[name="Txt_Garantia"]').val( response.arrEdit[0].Txt_Garantia );

      // Detracción
      $('#radio-InactiveDetraccion').prop('checked', true).iCheck('update');
      $('#radio-ActiveDetraccion').prop('checked', false).iCheck('update');
      if (response.arrEdit[0].Nu_Detraccion != '0') {//0 = No y 1 = Si
        $('#radio-InactiveDetraccion').prop('checked', false).iCheck('update');
        $('#radio-ActiveDetraccion').prop('checked', true).iCheck('update');
      }

      // Retencion
      $('#radio-InactiveRetencion').prop('checked', true).iCheck('update');
      $('#radio-ActiveRetencion').prop('checked', false).iCheck('update');
      if (response.arrEdit[0].Nu_Retencion != '0') {//0 = No y 1 = Si
        $('#radio-InactiveRetencion').prop('checked', false).iCheck('update');
        $('#radio-ActiveRetencion').prop('checked', true).iCheck('update');
      }

      //Formato PDF
      var arrFormatoPDF = [
        {"No_Formato_PDF": "A4"},
        {"No_Formato_PDF": "A5"},
        {"No_Formato_PDF": "TICKET"},
      ];
      $( '#cbo-formato_pdf' ).html('');
      for (var i = 0; i < arrFormatoPDF.length; i++) {
        selected = '';
        if(response.arrEdit[0].No_Formato_PDF == arrFormatoPDF[i]['No_Formato_PDF'])
          selected = 'selected="selected"';
        $( '#cbo-formato_pdf' ).append( '<option value="' + arrFormatoPDF[i]['No_Formato_PDF'] + '" ' + selected + '>' + arrFormatoPDF[i]['No_Formato_PDF'] + '</option>' );
      }

      //Detalle
      $( '#table-DetalleProductosVenta' ).show();
      $( '#table-DetalleProductosVenta tbody' ).empty();

      var table_detalle_producto = '';
      var _ID_Producto = '';
      var $Ss_SubTotal_Producto = 0.00;
      var $Ss_IGV_Producto = 0.00;
      var $Ss_Descuento_Producto = 0.00;
      var $Ss_Total_Producto = 0.00;
      var $Ss_Gravada = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var option_impuesto_producto = '';

      var $fDescuento_Producto = 0;
      var fDescuento_Total_Producto = 0;
      var globalImpuesto = 0;
      var $iDescuentoGravada = 0;
      var $iDescuentoExonerada = 0;
      var $iDescuentoInafecto = 0;
      var $iDescuentoGratuita = 0;
      var $iDescuentoGlobalImpuesto = 0;
      var selected;

      var iTotalRegistros = response.arrEdit.length;
      var iTotalRegistrosImpuestos = response.arrImpuesto.length;

      console.log(response.arrEdit);
      console.log(iTotalRegistros);

      for (var i = 0; i < iTotalRegistros; i++) {
        if (_ID_Producto != response.arrEdit[i].ID_Producto) {
          _ID_Producto = response.arrEdit[i].ID_Producto;
          option_impuesto_producto = '';
        }

        $Ss_SubTotal_Producto = parseFloat(response.arrEdit[i].Ss_SubTotal_Producto)
        if (response.arrEdit[i].Nu_Tipo_Impuesto == 1){
					$Ss_IGV += parseFloat(response.arrEdit[i].Ss_Impuesto_Producto);
					$Ss_Gravada += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 2){
          $Ss_Inafecto += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 3){
          $Ss_Exonerada += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 4){
          $Ss_Gratuita += $Ss_SubTotal_Producto;
        }

        $Ss_Descuento_Producto += parseFloat(response.arrEdit[i].Ss_Descuento_Producto);
        $Ss_Total += parseFloat(response.arrEdit[i].Ss_Total_Producto);

	      for (var x = 0; x < iTotalRegistrosImpuestos; x++){
	        selected = '';
	        if (response.arrImpuesto[x].ID_Impuesto_Cruce_Documento == response.arrEdit[i].ID_Impuesto_Cruce_Documento)
	          selected = 'selected="selected"';
          option_impuesto_producto += "<option value='" + response.arrImpuesto[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + response.arrImpuesto[x].Nu_Tipo_Impuesto + "' data-impuesto_producto='" + response.arrImpuesto[x].Ss_Impuesto + "' " + selected + ">" + response.arrImpuesto[x].No_Impuesto + "</option>";
        }

        console.log(response);
        table_detalle_producto +=
          "<tr id='tr_detalle_producto" + response.arrEdit[i].ID_Producto + "'>"
            + "<td style='display:none;' class='text-left td-iIdItem'>"
              + "<input name='Edit_ID_Documento_Detalle[]' id='Edit_ID_Documento_Detalle' class='txt-Edit_ID_Documento_Detalle' value='" + response.arrEdit[i].ID_Documento_Detalle + "' />"
              + response.arrEdit[i].ID_Producto
            + "</td>"
            + "<td class='text-right'><input type='text' class='txt-Qt_Producto form-control input-decimal' " + (response.arrEdit[i].Nu_Tipo_Producto == 1 ? 'onkeyup=validateStockNow(event);' : '') + " data-id_item='" + response.arrEdit[i].ID_Producto + "' data-id_producto='" + response.arrEdit[i].ID_Producto + "' value='" + response.arrEdit[i].Qt_Producto + "' value='" + response.arrEdit[i].Qt_Producto + "' autocomplete='off'></td>"
            + "<td class='text-left'>" + response.arrEdit[i].No_Producto + "</td>"
            + "<td class='text-right'><input type='text' class='txt-fValorUnitario form-control input-decimal' value='" + parseFloat(response.arrEdit[i].Ss_Precio / response.arrEdit[i].Ss_Impuesto).toFixed(2) + "' autocomplete='off'></td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Precio form-control input-decimal' value='" + response.arrEdit[i].Ss_Precio + "' autocomplete='off'></td>"
            + "<td class='text-right'>"
            + "<select class='cbo-ImpuestosProducto form-control required' style='width: 100%;'>"
            + option_impuesto_producto
            + "</select>"
            + "</td>"
            + "<td style='display:none;' class='text-right'><input type='tel' class='txt-Ss_SubTotal_Producto form-control' value='" + response.arrEdit[i].Ss_SubTotal_Producto + "' autocomplete='off' disabled></td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Descuento form-control input-decimal' value='" + (response.arrEdit[i].Po_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Po_Descuento_Impuesto_Producto) + "' autocomplete='off'></td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Total_Producto form-control input-decimal' value='" + response.arrEdit[i].Ss_Total_Producto + "' autocomplete='off'></td>"
            + "<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Producto) + "</td>"
            + "<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Impuesto_Producto) + "</td>"
            + "<td style='display:none;' class='text-right td-iIdPresupuesto'>" + response.arrEdit[i].ID_OI_Detalle + "</td>"
            +"<td class='text-center'><button type='button' id='btn-deleteProducto' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-2x' aria-hidden='true'> </i></button></td>"
          + "</tr>";
      }

		  $( '#table-DetalleProductosVenta > tbody' ).append(table_detalle_producto);
			$( '#txt-subTotal' ).val( $Ss_Gravada.toFixed(2) );
			$( '#span-subTotal' ).text( $Ss_Gravada.toFixed(2) );
      $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
			$( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
			$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
			$( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
			$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );

      if (parseFloat(response.arrEdit[0].Ss_Descuento) > 0 && $Ss_Descuento_Producto == 0)
        $( '#txt-Ss_Descuento' ).val( response.arrEdit[0].Po_Descuento );
      else
        $( '#txt-Ss_Descuento' ).val( '' );

      $( '#txt-descuento' ).val( response.arrEdit[0].Ss_Descuento );
      $( '#span-descuento' ).text( response.arrEdit[0].Ss_Descuento );
			$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
			$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
			$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
			$( '#span-total' ).text( $Ss_Total.toFixed(2) );

      validateDecimal();
		  validateNumber();
  		validateNumberOperation();

      url = base_url + 'HelperController/getImpuestos';
      $.post( url , function( response ){
        arrImpuestosProducto = '';
        arrImpuestosProductoDetalle = '';
        for (var i = 0; i < response.length; i++)
          arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
        arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';

        $( '#modal-loader' ).modal('hide');
      }, 'JSON');

      var _ID_Producto = '';
      var option_impuesto_producto = '';
    }
  })
}

function imprimirVenta(ID){
  var $modal_imprimir = $( '.modal-message-delete' );
  $modal_imprimir.modal('show');
  
  $( '.modal-message-delete' ).removeClass('modal-danger modal-warning modal-success');
  $( '.modal-message-delete' ).addClass('modal-success');
  
  $( '.modal-title-message-delete' ).text('¿Deseas imprimir el documento?');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_imprimir.modal('hide');
  });
  
  $( '#btn-save-delete' ).off('click').click(function () {
    url = base_url + 'Ventas/VentaController/imprimirVenta/' + ID;
    //window.open(url,'_blank');
    window.location.href = url;
  });
}

function anularFacturaVenta(ID, Nu_Enlace, Nu_Descargar_Inventario, accion, iEstado, sTipoBajaSunat){
  var $modal_delete = $( '.modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '.modal-message-delete' ).removeClass('modal-danger modal-warning modal-success');
  $( '.modal-message-delete' ).addClass('modal-warning');
  
  $( '.modal-title-message-delete' ).text('¿Deseas anular el documento?');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });
  
  $(document).bind('keydown', 'alt+k', function(){
    if ( accion=='anular' ) {
      _anularFacturaVenta($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario, iEstado, sTipoBajaSunat);
      accion='';
    }
  });

  $( '#btn-save-delete' ).off('click').click(function () {
    _anularFacturaVenta($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario, iEstado, sTipoBajaSunat);
  });
}

function eliminarFacturaVenta(ID, Nu_Enlace, Nu_Descargar_Inventario, accion){
  var $modal_delete = $( '#modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '.modal-message-delete' ).removeClass('modal-danger modal-warning modal-success');
  $( '.modal-message-delete' ).addClass('modal-danger');
  
  $( '.modal-title-message-delete' ).text('¿Deseas eliminar el documento?');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });
  
  $(document).bind('keydown', 'alt+l', function(){
    if ( accion=='delete' ) {
      _eliminarFacturaVenta($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario);
      accion='';
    }
  });

  $( '#btn-save-delete' ).off('click').click(function () {
    _eliminarFacturaVenta($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario);
  });
}

$(function () {
  $('[data-mask]').inputmask();

  $("#myInput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("#table-Venta tr").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  //Medios de pago - tipos de medios de pago
  $('.div-modal_datos_tarjeta_credito').hide();
  $('#cbo-MediosPago').change(function () {
    ID_Medio_Pago = $(this).val();
    Nu_Tipo_Medio_Pago = $(this).find(':selected').data('nu_tipo');
    $('.div-modal_datos_tarjeta_credito').hide();
    $('#cbo-tarjeta_credito').html('');
    $('#tel-nu_referencia').val('');
    $('#tel-nu_ultimo_4_digitos_tarjeta').val('');
    if (Nu_Tipo_Medio_Pago == 2) {
      $('.div-modal_datos_tarjeta_credito').show();

      url = base_url + 'HelperController/getTiposTarjetaCredito';
      $.post(url, { ID_Medio_Pago: ID_Medio_Pago }, function (response) {
        $('#cbo-tarjeta_credito').html('');
        for (var i = 0; i < response.length; i++)
          $('#cbo-tarjeta_credito').append('<option value="' + response[i].ID_Tipo_Medio_Pago + '">' + response[i].No_Tipo_Medio_Pago + '</option>');
      }, 'JSON');
    }
  })

  // Button - Verificar si existe documento de enlace
  $( '#btn-verificarExisteDocumento' ).click(function(){
    var Input_Serie_Documento_Modificar;
    var ID_Serie_Documento_Modificar;
    if ($( '#cbo-SeriesDocumentoModificar' ).val() !== undefined){
      ID_Serie_Documento_Modificar = $( '#cbo-SeriesDocumentoModificar' ).val();
      Input_Serie_Documento_Modificar = 'cbo-SeriesDocumentoModificar';
    }
    if ($( '#txt-ID_Serie_Documento_Modificar' ).val() !== undefined){
      ID_Serie_Documento_Modificar = $( '#txt-ID_Serie_Documento_Modificar' ).val();
      Input_Serie_Documento_Modificar = 'txt-ID_Serie_Documento_Modificar';
    }
    if (nu_enlace == 1 && $( '#cbo-TiposDocumentoModificar' ).val() == '0'){
      $( '#cbo-TiposDocumentoModificar' ).closest('.form-group').find('.help-block').html('Seleccionar documento');
  	  $( '#cbo-TiposDocumentoModificar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if (nu_enlace == 1 && ID_Serie_Documento_Modificar == '0'){
      $( '#' + Input_Serie_Documento_Modificar ).closest('.form-group').find('.help-block').html('Seleccionar serie');
  	  $( '#' + Input_Serie_Documento_Modificar ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if (nu_enlace == 1 && $( '#txt-ID_Numero_Documento_Modificar' ).val().length === 0){
      $( '#txt-ID_Numero_Documento_Modificar' ).closest('.form-group').find('.help-block').html('Ingresar numero');
  	  $( '#txt-ID_Numero_Documento_Modificar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $( '#btn-verificarExisteDocumento' ).text('');
      $( '#btn-verificarExisteDocumento' ).attr('disabled', true);
      $( '#btn-verificarExisteDocumento' ).append( 'Verificando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );

      url = base_url + 'HelperController/documentExistVerify';
      $.post( url, {
        ID_Documento_Guardado : $( '#txt-ID_Documento_Guardado' ).val(),
        ID_Tipo_Documento_Modificar : $( '#cbo-TiposDocumentoModificar' ).val(),
        ID_Serie_Documento_Modificar : ID_Serie_Documento_Modificar,
        ID_Numero_Documento_Modificar : $( '#txt-ID_Numero_Documento_Modificar' ).val(),
        iIdEntidad : $( '#txt-AID' ).val(),
        iTipoCliente : $('[name="addCliente"]:checked').attr('value'),
      }, function( response ){
  	    $( '.div-DocumentoModificar' ).removeClass('panel-default panel-warning panel-danger panel-success');
  	    $( '.div-mensaje_verificarExisteDocumento' ).removeClass('text-warning text-danger text-success');
  		  if (response.status == 'error'){
  		    $( '#btn-save' ).attr('disabled', true);
    	    $( '.div-DocumentoModificar' ).addClass(response.style_panel);
    	    $( '.div-mensaje_verificarExisteDocumento' ).addClass(response.style_p);
  		    $( '.div-mensaje_verificarExisteDocumento' ).text(response.message);
  		  } else {
          $( '#btn-save' ).attr('disabled', false);
  		    $( '.div-DocumentoModificar' ).addClass(response.style_panel);
  		    $( '.div-mensaje_verificarExisteDocumento' ).addClass(response.style_p);
  		    $( '.div-mensaje_verificarExisteDocumento' ).text(response.message);
  		  }
        $( '#btn-verificarExisteDocumento' ).text('');
        $( '#btn-verificarExisteDocumento' ).append( 'Verificar <span class="fa fa-check"></span>' );
        $( '#btn-verificarExisteDocumento' ).attr('disabled', false);
      }, 'JSON');
    }
  })
  // ./ Button - Verificar si existe documento de enlace
  
  $( '#radio-cliente_varios' ).on('ifChecked', function () {
    $( '.div-cliente_existente' ).hide();
    $( '.div-cliente_nuevo' ).hide();
    
    $( '#txt-AID' ).val( '1' );
  })
  
  $( '#radio-cliente_existente' ).on('ifChecked', function () {
    $( '.div-cliente_existente' ).show();
    $( '.div-cliente_nuevo' ).hide();
    
    $( '#txt-AID' ).val( '' );
  })
  
  $( '#radio-cliente_nuevo' ).on('ifChecked', function () {
    $( '.div-cliente_existente' ).hide();
    $( '.div-cliente_nuevo' ).show();
    
    $( '#txt-AID' ).val( '' );
  })

  //LAE API SUNAT / RENIEC - CLIENTE
  $( '#btn-cloud-api_venta_cliente' ).click(function(){
    if ( $( '#cbo-TiposDocumentoIdentidadCliente' ).val().length === 0){
      $( '#cbo-TiposDocumentoIdentidadCliente' ).closest('.form-group').find('.help-block').html('Seleccionar tipo doc. identidad');
  	  $( '#cbo-TiposDocumentoIdentidadCliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($( '#cbo-TiposDocumentoIdentidadCliente' ).find(':selected').data('nu_cantidad_caracteres') != $( '#txt-Nu_Documento_Identidad_Cliente').val().length ) {
      $( '#txt-Nu_Documento_Identidad_Cliente' ).closest('.form-group').find('.help-block').html('Debe ingresar ' + $( '#cbo-TiposDocumentoIdentidadCliente' ).find(':selected').data('nu_cantidad_caracteres') + ' dígitos' );
  	  $( '#txt-Nu_Documento_Identidad_Cliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if (
      (
        $( '#cbo-TiposDocumentoIdentidadCliente' ).val() == 1 ||
        $( '#cbo-TiposDocumentoIdentidadCliente' ).val() == 3 ||
        $( '#cbo-TiposDocumentoIdentidadCliente' ).val() == 5 ||
        $( '#cbo-TiposDocumentoIdentidadCliente' ).val() == 6
      )
      ) {
      $( '#cbo-TiposDocumentoIdentidadCliente' ).closest('.form-group').find('.help-block').html('Disponible DNI / RUC');
  	  $( '#cbo-TiposDocumentoIdentidadCliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $( '#btn-cloud-api_venta_cliente' ).text('');
      $( '#btn-cloud-api_venta_cliente' ).attr('disabled', true);
      $( '#btn-cloud-api_venta_cliente' ).append( '<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      
      // Obtener datos de SUNAT y RENIEC
      var url_api = 'https://www.laesystems.com/librerias/sunat/partner/format/json/x-api-key/';
			if ( $( '#cbo-TiposDocumentoIdentidadCliente' ).val() == 2 )//2=RENIEC, API SUNAT
				url_api = 'https://www.laesystems.com/librerias/reniec/partner/format/json/x-api-key/';
			url_api = url_api + sTokenGlobal;

      var data = {
        ID_Tipo_Documento_Identidad : $( '#cbo-TiposDocumentoIdentidadCliente' ).val(),
        Nu_Documento_Identidad : $( '#txt-Nu_Documento_Identidad_Cliente' ).val(),
      };

      $.ajax({
        url   : url_api,
        type  :'POST',
        data  : data,
        success: function(response){
          $( '#btn-cloud-api_venta_cliente' ).closest('.form-group').find('.help-block').html('');
      	  $( '#btn-cloud-api_venta_cliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
      	  
          if (response.success==true){
            $( '#txt-No_Entidad_Cliente' ).val( response.data.No_Names );
            if ( $( '#cbo-TiposDocumentoIdentidadCliente' ).val() == 4) {//RUC
              $( '#txt-Txt_Direccion_Entidad_Cliente' ).val( response.data.Txt_Address );
              $( '#txt-Nu_Telefono_Entidad_Cliente' ).val( response.data.Nu_Phone );
              $( '#txt-Nu_Celular_Entidad_Cliente' ).val( response.data.Nu_Cellphone );
              if ( response.data.Nu_Status == 1)
                $("div.estado select").val("1");
              else
                $("div.estado select").val("0");
            }
          } else {
            $( '#txt-No_Entidad_Cliente' ).val( '' );
            if ( $( '#cbo-TiposDocumentoIdentidadCliente' ).val() == 4) {//RUC
              $( '#txt-Txt_Direccion_Entidad_Cliente' ).val( '' );
              $( '#txt-Nu_Telefono_Entidad_Cliente' ).val( '' );
              $( '#txt-Nu_Celular_Entidad_Cliente' ).val( '' );
            }
            $( '#txt-Nu_Documento_Identidad_Cliente' ).closest('.form-group').find('.help-block').html(response.msg);
        	  $( '#txt-Nu_Documento_Identidad_Cliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
        	  
  		  	  $( '#txt-Nu_Documento_Identidad_Cliente' ).focus();
  		  	  $( '#txt-Nu_Documento_Identidad_Cliente' ).select();
          }
  		  	
          $( '#btn-cloud-api_venta_cliente' ).text('');
          $( '#btn-cloud-api_venta_cliente' ).attr('disabled', false);
          $( '#btn-cloud-api_venta_cliente' ).append( '<i class="fa fa-cloud-download fa-lg"></i>' );
        },
        error: function(response){
          $( '#btn-cloud-api_venta_cliente' ).closest('.form-group').find('.help-block').html('Sin acceso');
      	  $( '#btn-cloud-api_venta_cliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
      	  
          $( '#txt-No_Entidad_Cliente' ).val( '' );
          $( '#txt-Txt_Direccion_Entidad_Cliente' ).val( '' );
          $( '#txt-Nu_Telefono_Entidad_Cliente' ).val( '' );
          $( '#txt-Nu_Celular_Entidad_Cliente' ).val( '' );

          $( '#btn-cloud-api_venta_cliente' ).text('');
          $( '#btn-cloud-api_venta_cliente' ).attr('disabled', false);
          $( '#btn-cloud-api_venta_cliente' ).append( '<i class="fa fa-cloud-download fa-lg"></i>' );
        }
      });
    }
  })
  
	/* Tipo Documento Identidad Cliente */
	$( '#cbo-TiposDocumentoIdentidadCliente' ).change(function(){
	  if ( $(this).val() == 2 ) {//DNI
		  $( '#label-Nombre_Documento_Identidad_Cliente' ).text('DNI');
		  $( '#label-No_Entidad_Cliente' ).text('Nombre(s) y Apellidos');
			$( '#txt-Nu_Documento_Identidad_Cliente' ).attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
	  } else if ( $(this).val() == 4 ) {//RUC
		  $( '#label-Nombre_Documento_Identidad_Cliente' ).text('RUC');
		  $( '#label-No_Entidad_Cliente' ).text('Razón Social');
			$( '#txt-Nu_Documento_Identidad_Cliente' ).attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
	  } else {
	    $( '#label-Nombre_Documento_Identidad_Cliente' ).text('# Documento Identidad');
		  $( '#label-No_Entidad_Cliente' ).text('Nombre(s) y Apellidos');
			$( '#txt-Nu_Documento_Identidad_Cliente' ).attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
	  }
	})
  
  url = base_url + 'HelperController/getOrganizaciones';
  $.post( url , function( response ){
    if (response.length == 1) {
      $( '#cbo-filtro-organizacion' ).html( '<option value="' + response[0].ID_Organizacion + '">' + response[0].No_Organizacion + '</option>' );
    } else {
      $( '#cbo-filtro-organizacion' ).html('<option value="" selected="selected">- Seleccionar -</option>');
      for (var i = 0; i < response.length; i++)
        $( '#cbo-filtro-organizacion' ).append( '<option value="' + response[i].ID_Organizacion + '">' + response[i].No_Organizacion + '</option>' );
    }
  }, 'JSON');
  
  url = base_url + 'HelperController/getTiposDocumentos';
  $.post( url, {Nu_Tipo_Filtro : 3}, function( response ){//1 = Venta
    $( '#cbo-Filtro_TiposDocumento' ).html('<option value="0" selected="selected">Todos</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-Filtro_TiposDocumento' ).append( '<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>' );
  }, 'JSON');
  
	$( '#cbo-Filtro_SeriesDocumento' ).html('<option value="0" selected="selected">Todos</option>');
	$( '#cbo-Filtro_TiposDocumento' ).change(function(){
	  $( '#cbo-Filtro_SeriesDocumento' ).html('<option value="0" selected="selected">Todos</option>');
	  if ( $(this).val() > 0) {
		  url = base_url + 'HelperController/getSeriesDocumento';
      $.post( url, { ID_Organizacion: $( '#cbo-filtro-organizacion' ).val(), ID_Tipo_Documento: $(this).val() }, function( response ){
	      $( '#cbo-Filtro_SeriesDocumento' ).html('<option value="0" selected="selected">- Todos -</option>');
        if (response.length > 0) {
          for (var i = 0; i < response.length; i++)
            $( '#cbo-Filtro_SeriesDocumento' ).append( '<option value="' + response[i].ID_Serie_Documento + '">' + response[i].ID_Serie_Documento + '</option>' );
        }
      }, 'JSON');
	  }
	})
	
	// Mostrar tipo de producto para inventarios
	$( '.div-Producto' ).hide();
	$( '#cbo-modal-grupoItem' ).change(function(){
  	$( '.div-Producto' ).show();
	  if ( $(this).val() == 0 ){//Servicio
    	$( '.div-Producto' ).hide();
	  }
	})

  $('#cbo-Filtro_MediosPago').html('<option value="0">Todos</option>');
  url = base_url + 'HelperController/getMediosPago';
  $.post(url, function (response) {
    $('#cbo-Filtro_MediosPago').html('<option value="0">Todos</option>');
    for (var i = 0; i < response.length; i++)
      $('#cbo-Filtro_MediosPago').append('<option value="' + response[i]['ID_Medio_Pago'] + '" data-nu_tipo="' + response[i]['Nu_Tipo'] + '">' + response[i]['No_Medio_Pago'] + '</option>');
  }, 'JSON');

  //Medios de pago - tipos de medios de pago
  $('.div-filtro_modal_datos_tarjeta_credito').hide();
  $('#cbo-Filtro_tarjeta_credito').html('<option value="0">Todos</option>');
  $('#cbo-Filtro_MediosPago').change(function () {
    ID_Medio_Pago = $(this).val();
    Nu_Tipo_Medio_Pago = $(this).find(':selected').data('nu_tipo');
    $('.div-filtro_modal_datos_tarjeta_credito').hide();
    $('#cbo-Filtro_tarjeta_credito').html('<option value="0">Todos</option>');
    if (Nu_Tipo_Medio_Pago == 2) {
      $('.div-filtro_modal_datos_tarjeta_credito').show();

      url = base_url + 'HelperController/getTiposTarjetaCredito';
      $.post(url, { ID_Medio_Pago: ID_Medio_Pago }, function (response) {
        $('#cbo-Filtro_tarjeta_credito').html('<option value="0">Todos</option>');
        for (var i = 0; i < response.length; i++)
          $('#cbo-Filtro_tarjeta_credito').append('<option value="' + response[i].ID_Tipo_Medio_Pago + '">' + response[i].No_Tipo_Medio_Pago + '</option>');
      }, 'JSON');
    }
  })

  url = base_url + 'Ventas/VentaController/ajax_list';
  table_venta = $( '#table-Venta' ).DataTable({
    'dom'       : 'B<"top">frt<"bottom"lp><"clear">',
    buttons     : [{
      extend    : 'excel',
      text      : '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr : 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'pdf',
      text      : '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr : 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'colvis',
      text      : '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr : 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching'   : false,
    'bStateSave'  : true,
    'processing'  : true,
    'serverSide'  : true,
    'info'        : true,
    'autoWidth'   : false,
    'pagingType'  : 'full_numbers',
    'oLanguage' : {
      'sInfo'               : 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu'         : '_MENU_',
      'sSearch'             : 'Buscar por: ',
      'sSearchPlaceholder'  : 'UPC / Nombre',
      'sZeroRecords'        : 'No se encontraron registros',
      'sInfoEmpty'          : 'No hay registros',
      'sLoadingRecords'     : 'Cargando...',
      'sProcessing'         : 'Procesando...',
      'oPaginate'           : {
        'sFirst'    : '<<',
        'sLast'     : '>>',
        'sPrevious' : '<',
        'sNext'     : '>',
      },
    },
    'order':[],
    'ajax': {
      'url'       : url,
      'type'      : 'POST',
      'dataType'  : 'json',
      'data'      : function ( data ) {
        data.Filtro_Fe_Inicio       = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/'),
        data.Filtro_Fe_Fin          = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/'),
          data.Filtro_MediosPago = $('#cbo-Filtro_MediosPago').val(),
          data.Filtro_tarjeta_credito = $('#cbo-Filtro_tarjeta_credito').val(),
        data.Filtro_TiposDocumento  = $( '#cbo-Filtro_TiposDocumento' ).val(),
        data.Filtro_SeriesDocumento = $( '#cbo-Filtro_SeriesDocumento' ).val(),
        data.Filtro_NumeroDocumento = $( '#txt-Filtro_NumeroDocumento' ).val(),
        data.Filtro_Estado          = $( '#cbo-Filtro_Estado' ).val(),
        data.Filtro_Entidad         = $( '#txt-Filtro_Entidad' ).val();
      },
    },
    'columnDefs': [{
      'className' : 'text-center',
      'targets'   : 'no-sort',
      'orderable' : false,
    },
    {
      'className' : 'text-left',
      'targets'   : 'no-sort_left',
      'orderable' : false,
    },
    {
      'className' : 'text-right',
      'targets'   : 'no-sort_right',
      'orderable' : false,
    },
    {
      'className' : 'text-center',
      'targets'   : 'sort_center',
      'orderable' : true,
    },
    {
      'className' : 'text-right',
      'targets'   : 'sort_right',
      'orderable' : true,
    },],
    'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
  });
  
  $( '.dataTables_length' ).addClass('col-md-3');
  $( '.dataTables_paginate' ).addClass('col-md-9');
  
  $( '#btn-filter' ).click(function(){
    table_venta.ajax.reload();
  });
  
  $( '#form-Venta' ).validate({
		rules:{
			Fe_Emision: {
				required: true,
			},
		},
		messages:{
			Fe_Emision:{
				required: "Ingresar F. Emisión",
			},
		},
		errorPlacement : function(error, element) {
			$(element).closest('.form-group').find('.help-block').html(error.html());
    },
		highlight : function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	  },
	  unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			$(element).closest('.form-group').find('.help-block').html('');
	  },
		submitHandler: form_Venta
	});
	
  // COBRAR CREDITO
  $( '#btn-cobrar_cliente' ).click(function(){
		var fPagoClienteCobranza = parseFloat($( '#tel-cobrar_cliente-fPagoCliente' ).val());
    if ( fPagoClienteCobranza == 0.00 || isNaN(fPagoClienteCobranza) ) {
      $( '[name="fPagoCliente"]' ).closest('.form-group').find('.help-block').html( 'Ingresar monto' );
      $( '[name="fPagoCliente"]' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-cobrar_cliente .modal-body'), $( '[name="fPagoCliente"]' ));
    } else if ( fPagoClienteCobranza > parseFloat($( '#hidden-cobrar_cliente-fsaldo' ).val()) ) {
      $( '#tel-cobrar_cliente-fPagoCliente' ).closest('.form-group').find('.help-block').html('Debes de cobrar <b>' + $( '#hidden-cobrar_cliente-fsaldo' ).val() + '</b>' );
      $( '#tel-cobrar_cliente-fPagoCliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-cobrar_cliente .modal-body'), $( '#tel-cobrar_cliente-fPagoCliente' ));
    } else if ( $( '#cbo-modal_quien_recibe' ).val() == 0 && $( '[name="sNombreRecepcion"]' ).val().length === 0 ) {
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').find('.help-block').html('Ingresar datos');
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-cobrar_cliente .modal-body'), $( '[name="sNombreRecepcion"]' ));
    } else {
      $( '.help-block' ).empty();
      $( '[name="fPagoCliente"]' ).closest('.form-group').removeClass('has-error');
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').removeClass('has-error');
      
      $( '#btn-cobrar_cliente' ).text('');
      $( '#btn-cobrar_cliente' ).attr('disabled', true);
      $( '#btn-cobrar_cliente' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      $( '#btn-salir' ).attr('disabled', true);

      url = base_url + 'HelperController/cobranzaClientePuntoVenta';
      $.ajax({
        type : 'POST',
        dataType : 'JSON',
        url : url,
        data : $('#form-cobrar_cliente').serialize(),
        success : function( response ){
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          $( '#modal-message' ).modal('show');

          if ( response.sStatus=='success' ) {
            $( '.modal-cobrar_cliente' ).modal('hide');

            $( '.modal-message' ).addClass( 'modal-' + response.sStatus);
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
            
            table_venta.ajax.reload();
          } else {
            $( '.modal-message' ).addClass( 'modal-' + response.sStatus );
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
          }
          
          $( '#btn-cobrar_cliente' ).text('');
          $( '#btn-cobrar_cliente' ).append( 'Cobrar' );
          $( '#btn-cobrar_cliente' ).attr('disabled', false);
          $( '#btn-salir' ).attr('disabled', false);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
        
        //Message for developer
        console.log(jqXHR.responseText);

        $( '#btn-cobrar_cliente' ).text('');
        $( '#btn-cobrar_cliente' ).attr('disabled', false);
        $( '#btn-cobrar_cliente' ).append( 'Cobrar' );
        $( '#btn-salir' ).attr('disabled', false);
      })
    }
  })

	$( '#cbo-SeriesDocumento' ).change(function(){
	  $( '#txt-ID_Numero_Documento' ).val('');
	  if ( $(this).val() != '') {
		  url = base_url + 'HelperController/getNumeroDocumento';
      $.post( url, { ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Tipo_Documento: $( '#cbo-TiposDocumento' ).val(), ID_Serie_Documento: $(this).val() }, function( response ){
        if (response.length == 0)
          $( '#txt-ID_Numero_Documento' ).val('');
        else
          $( '#txt-ID_Numero_Documento' ).val(response.ID_Numero_Documento);
      }, 'JSON');
    }
	})
	
	$( '#cbo-Monedas' ).change(function(){
	  if ( $(this).val() > 0 )
	    $( '.span-signo' ).text( $(this).find(':selected').data('no_signo') );
	})
	
	$( '.div-MediosPago' ).hide();
	$( '#cbo-MediosPago' ).change(function(){
	  $( '.div-MediosPago' ).hide();
	  if ( $(this).find(':selected').data('nu_tipo') == 1 )// Si es Crédito
	    $( '.div-MediosPago' ).show();
	})
	
	//Validar N/C y N/D
	$( '#cbo-TiposDocumentoModificar' ).change(function(){
	  $( '#cbo-SeriesDocumentoModificar' ).html('');
	  if ( $(this).val() > 0 ) {
		  url = base_url + 'HelperController/getSeriesDocumentoModificarxAlmacen';
      $.post( url, {ID_Organizacion: $( '#header-a-id_organizacion' ).val(), ID_Almacen : $( '#cbo-almacen' ).val(), ID_Tipo_Documento: $(this).val() }, function( response ){
        if (response.length == 0)
          $( '#cbo-SeriesDocumentoModificar' ).html('<option value="0" selected="selected">Sin serie</option>');
        else {
          $( '#cbo-SeriesDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
          for (var i = 0; i < response.length; i++)
            $( '#cbo-SeriesDocumentoModificar' ).append( '<option value="' + response[i]['ID_Serie_Documento'] + '" data-id_serie_documento_pk="' + response[i]['ID_Serie_Documento_PK'] + '">' + response[i]['ID_Serie_Documento'] + '</option>' );
        }
      }, 'JSON');
	  }
	})

  /*
	$( '#cbo-descargar_stock' ).change(function(){
    $( '.div-almacen' ).hide();
	  if ( $(this).val() > 0 ) {
      $( '.div-almacen' ).show();
      var arrParams = {};
      getAlmacenes(arrParams);
    } else {
      var arrParams = {
        ID_Almacen : 0,
      };
      getListaPrecios(arrParams);
    }
	})
  */

	$( '#cbo-almacen' ).change(function(){
	  if ( $(this).val() > 0 ) {
      var arrParams = {
        ID_Almacen : 0,
      };
      getListaPrecios(arrParams);
    }
	})

	$( '#btn-crearItem' ).click(function(){
    bEstadoValidacion = validatePreviousDocumentToSaveSale();
	  if (bEstadoValidacion){
	   crearItem();
	  }
	})

  
   // M02 - INICIO 

 $( '#btn-buscadocumento' ).click(function(){
 
  var tipo_bol = $( '#cbo-TiposDocumentoModificar' ).val();
  var serie = $( '#cbo-SeriesDocumentoModificar' ).val();
  var numero = $( '#txt-ID_Numero_Documento_Modificar' ).val();
  var cod="";
  //var id ='7670';

  //REF : ID_Tipo_Documento: 3, ID_Serie_Documento: 'FPP1' , ID_Numero_Documento: '1772'
      url = base_url + 'HelperController/getIdDocumento_x_SerieBolFact';
      $.post( url, { ID_Tipo_Documento: tipo_bol , ID_Serie_Documento:  serie , ID_Numero_Documento: numero }, 
        function( response ){
        cod = response.ID_Documento_Cabecera;
        //alert('getIdDocumento_x_SerieBolFact 2 : '+ tipo_bol +' - '+ serie +' - '+numero + ' = '+ cod)  ; 
       
        verFacturaVenta_x_NC(cod);
      }, 'JSON');
  

}) 


function verFacturaVenta_x_NC(ID){


  var valor_cbo_TiposDocumento ;
  var valor_cbo_SeriesDocumento ;
  var valor_txt_ID_Numero_Documento;
  var valor_txt_Nu_Presupuesto;
  var valor_txt_Nu_Orden_Compra;

  var valor_Fe_Entrega_Factura;
  var valor_Fe_Entrega_Vencimiento_Factura;
  var valor_Fe_Emision;
  var valor_cbo_formato_pdf;
  var valor_cbo_tipos_servicios;
  var valor_cbo_vendedor;
  var valor_cbo_porcentaje;
  var valor_cbo_descargar_stock;
  var valor_cbo_TiposDocumentoModificar;
  var valor_cbo_SeriesDocumentoModificar;
  var valor_cbo_ID_Numero_Documento_Modificar;
  var valor_cbo_MotivoReferenciaModificar;


  valor_cbo_TiposDocumento = "";
  valor_cbo_SeriesDocumento = "";
  valor_txt_ID_Numero_Documento = "";
  valor_txt_Nu_Presupuesto= "";
  valor_txt_Nu_Orden_Compra= "";

  valor_cbo_formato_pdf ="";
  valor_cbo_tipos_servicios="";
  valor_cbo_vendedor = "";
  valor_cbo_porcentaje = "";
  valor_cbo_descargar_stock= "";
  valor_cbo_TiposDocumentoModificar = "";
  valor_cbo_SeriesDocumentoModificar = "";
  valor_cbo_ID_Numero_Documento_Modificar = "";
  valor_cbo_MotivoReferenciaModificar = "";


  valor_cbo_TiposDocumento = $( '#cbo-TiposDocumento' ).val();
  valor_cbo_SeriesDocumento = $( '#cbo-SeriesDocumento' ).val();
  valor_txt_ID_Numero_Documento = $( '#txt-ID_Numero_Documento' ).val();
  valor_txt_Nu_Presupuesto = $( '#txt-Nu_Presupuesto' ).val();
  valor_txt_Nu_Orden_Compra = $( '#txt-Nu_Orden_Compra' ).val();


  valor_Fe_Entrega_Factura =$( '#txt-Fe_Entrega_Factura' ).val();
  valor_Fe_Entrega_Vencimiento_Factura =$( '#txt-Fe_Entrega_Vencimiento_Factura' ).val();
  valor_Fe_Emision = $( '#txt-Fe_Emision' ).val();
  valor_cbo_formato_pdf = $( '#cbo-formato_pdf' ).val();
  valor_cbo_tipos_servicios = $( '#cbo-tipos_servicios' ).val();
  valor_cbo_vendedor = $( '#cbo-vendedor' ).val();
  valor_cbo_porcentaje = $( '#cbo-porcentaje' ).val();
  valor_cbo_descargar_stock = $( '#cbo-descargar_stock' ).val();
  valor_cbo_TiposDocumentoModificar = $( '#cbo-TiposDocumentoModificar' ).val();
  valor_cbo_SeriesDocumentoModificar = $( '#cbo-SeriesDocumentoModificar' ).val();
  valor_cbo_ID_Numero_Documento_Modificar = $( '#txt-ID_Numero_Documento_Modificar' ).val();
  valor_cbo_MotivoReferenciaModificar = $( '#cbo-MotivoReferenciaModificar' ).val();



  accion = 'upd_factura_venta';
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();

	$( '#txt-EID_Empresa' ).focus();
	
  $( '#form-Venta' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
  $( '#txt-AID' ).val( '' );
  $( '#radio-cliente_varios' ).prop('checked', false).iCheck('update');
  $( '#radio-cliente_existente' ).prop('checked', true).iCheck('update');
  $( '#radio-cliente_nuevo' ).prop('checked', false).iCheck('update');
  
  $( '.div-cliente_existente' ).show();
  $( '.div-cliente_nuevo' ).hide();
  
  // div tipo doc, serie, numero y boton verificar : muestra / oculta
	//$( '.div-DocumentoModificar' ).hide();
	$( '.div-DocumentoModificar' ).show();
 
  $( '#txt-ID_Documento_Guardado' ).val(1);
  $( '.div-DocumentoModificar' ).removeClass('panel-warning panel-danger panel-success');
  $( '.div-mensaje_verificarExisteDocumento' ).removeClass('text-danger text-success');
  $( '.div-mensaje_verificarExisteDocumento' ).text('');
  $( '.div-DocumentoModificar' ).addClass('panel-default');
 
	$( '#panel-DetalleProductosVenta' ).removeClass('panel-danger');
	$( '#panel-DetalleProductosVenta' ).addClass('panel-default');
  
	$( '#txt-subTotal' ).val( value_importes_cero );
	$( '#span-subTotal' ).text( texto_importes_cero );
	
	$( '#txt-exonerada' ).val( value_importes_cero );
	$( '#span-exonerada' ).text( texto_importes_cero );
	
	$( '#txt-inafecto' ).val( value_importes_cero );
	$( '#span-inafecto' ).text( texto_importes_cero );
	
	$( '#txt-gratuita' ).val( value_importes_cero );
	$( '#span-gratuita' ).text( texto_importes_cero );
	
	$( '#txt-impuesto' ).val( value_importes_cero );
	$( '#span-impuesto' ).text( texto_importes_cero );
	
	$( '#txt-descuento' ).val( value_importes_cero );
	$( '#span-descuento' ).text( texto_importes_cero );
	
	$( '#txt-total' ).val( value_importes_cero );
	$( '#span-total' ).text( texto_importes_cero );

  $( '#btn-save' ).attr('disabled', false);

  considerar_igv=0;
  
  url = base_url + 'HelperController/getTiposDocumentoIdentidad';
  $.post( url , function( response ){
    $( '#cbo-TiposDocumentoIdentidadCliente' ).html('');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-TiposDocumentoIdentidadCliente' ).append( '<option value="' + response[i]['ID_Tipo_Documento_Identidad'] + '" data-nu_cantidad_caracteres="' + response[i]['Nu_Cantidad_Caracteres'] + '">' + response[i]['No_Tipo_Documento_Identidad_Breve'] + '</option>' );
  }, 'JSON');
  
  /* Tipo doc, serie, numero y motivo - div-DocumentoModificar */
  url = base_url + 'HelperController/getTiposDocumentosModificar';
  $.post( url, {Nu_Tipo_Filtro : 1}, function( response ){
    $( '#cbo-TiposDocumentoModificar' ).html('');
    $( '#cbo-TiposDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
    for (var i = 0; i < response.length; i++){
          selected = '';
          if(valor_cbo_TiposDocumentoModificar == response[i]['ID_Tipo_Documento'])
          selected = 'selected="selected"';
          $( '#cbo-TiposDocumentoModificar' ).append( '<option value="' + response[i]['ID_Tipo_Documento'] + '" ' + selected + '>' + response[i]['No_Tipo_Documento_Breve'] + '</option>' );
    }
  }, 'JSON'); 

  url = base_url + 'HelperController/getMotivosReferenciaModificar';
          $.post( url, {ID_Tipo_Documento: valor_cbo_TiposDocumento}, function( response ){
            $( '#cbo-MotivoReferenciaModificar' ).html('');
            $( '#cbo-MotivoReferenciaModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++){
              selected = '';
              if(valor_cbo_MotivoReferenciaModificar == response[i].Nu_Valor)
              selected = 'selected="selected"';
              $( '#cbo-MotivoReferenciaModificar' ).append( '<option value="' + response[i].Nu_Valor + '" ' + selected + '>' + response[i].No_Descripcion + '</option>' );
            }
           
            }
, 'JSON');

url = base_url + 'HelperController/getSeriesDocumentoModificar';
      $.post( url, { ID_Organizacion : $( '#header-a-id_organizacion' ).val(), 
          ID_Tipo_Documento: valor_cbo_TiposDocumentoModificar }, function( response ){
      
            $( '#cbo-SeriesDocumentoModificar' ).html('');
            $( '#cbo-SeriesDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++){
              selected = '';
              if(valor_cbo_SeriesDocumentoModificar == response[i].ID_Serie_Documento)
              selected = 'selected="selected"';
              $( '#cbo-SeriesDocumentoModificar' ).append( '<option value="' + response[i].ID_Serie_Documento + '" ' + selected + '>' + response[i].ID_Serie_Documento + '</option>' );
            }
}, 'JSON'); 


$( '#txt-ID_Numero_Documento_Modificar' ).val(valor_cbo_ID_Numero_Documento_Modificar);
$( '#txt-Fe_Emision' ).val(valor_Fe_Emision);
$( '#txt-Fe_Entrega_Factura' ).val(valor_Fe_Entrega_Factura);
$( '#txt-Fe_Entrega_Vencimiento_Factura' ).val(valor_Fe_Entrega_Vencimiento_Factura);

	$( '#cbo-TiposDocumento' ).change(function(){
    if ( $( '#cbo-almacen' ).val() > 0 ) {
      //$( '#cbo-SeriesDocumento' ).html('');
      //$( '#txt-ID_Numero_Documento' ).val('');
      //$( '.div-DocumentoModificar' ).hide();
      if ( $(this).val() > 0 ) {
        considerar_igv = $(this).find(':selected').data('nu_impuesto');
        nu_enlace = $(this).find(':selected').data('nu_enlace');
        
        /*if (nu_enlace == 1) {//Validar N/C y N/D
          $( '.div-DocumentoModificar' ).show();

           url = base_url + 'HelperController/getTiposDocumentosModificar';
          $.post( url, {Nu_Tipo_Filtro : 1}, function( response ){
            $( '#cbo-TiposDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-TiposDocumentoModificar' ).append( '<option value="' + response[i]['ID_Tipo_Documento'] + '">' + response[i]['No_Tipo_Documento_Breve'] + '</option>' );
          }, 'JSON'); 
          
          //Motivos de referencia  
          url = base_url + 'HelperController/getMotivosReferenciaModificar';
          $.post( url, {ID_Tipo_Documento: $(this).val()}, function( response ){
            $( '#cbo-MotivoReferenciaModificar' ).html('');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-MotivoReferenciaModificar' ).append( '<option value="' + response[i].Nu_Valor + '">' + response[i].No_Descripcion + '</option>' );
          }, 'JSON');
        }*/ // /. Validación de N/C y N/D

        /* 
        url = base_url + 'HelperController/getSeriesDocumentoxAlmacen';
        $.post( url, {ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Almacen : $( '#cbo-almacen' ).val(), ID_Tipo_Documento: $(this).val()}, function( response ){
          if (response.length === 1) {
            $( '#cbo-SeriesDocumento' ).html( '<option value="' + response[0].ID_Serie_Documento + '" data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>' );	    
            //Get número cuando solo haya una serie por un tipo de documento
            $( '#txt-ID_Numero_Documento' ).val('');
            url = base_url + 'HelperController/getNumeroDocumentoxAlmacen';
            $.post( url, { ID_Organizacion : $( '#header-a-id_organizacion' ).val(), ID_Almacen : $( '#cbo-almacen' ).val(), ID_Tipo_Documento: $( '#cbo-TiposDocumento' ).val(), ID_Serie_Documento: response[0].ID_Serie_Documento }, function( responseNumeros ){
              if (responseNumeros.length === 0)
                $( '#txt-ID_Numero_Documento' ).val('');
              else
                $( '#txt-ID_Numero_Documento' ).val(responseNumeros.ID_Numero_Documento);
            }, 'JSON');
            //Fin número
            $( '#cbo-SeriesDocumento' ).html( '<option value="' + response[0].ID_Serie_Documento + '"data-id_serie_documento_pk=' + response[0].ID_Serie_Documento_PK + '>' + response[0].ID_Serie_Documento + '</option>' );	    
          } else if (response.length > 1) {
            $( '#cbo-SeriesDocumento' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var i = 0; i < response.length; i++)
              $( '#cbo-SeriesDocumento' ).append( '<option value="' + response[i].ID_Serie_Documento + '"data-id_serie_documento_pk=' + response[i].ID_Serie_Documento_PK + '>' + response[i].ID_Serie_Documento + '</option>' );
          } else
            $( '#cbo-SeriesDocumento' ).html('<option value="0" selected="selected">Sin serie</option>');
        }, 'JSON'); */ 
        
        var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
        var $Ss_SubTotal = 0.00;
        var $Ss_Exonerada = 0.00;
        var $Ss_Inafecto = 0.00;
        var $Ss_Gratuita = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        var iCantDescuento = 0;
        var globalImpuesto = 0;
        var $Ss_Descuento_p = 0;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / fImpuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;
    
          if (iGrupoImpuesto == 1) {
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            globalImpuesto = fImpuesto;
          } else if (iGrupoImpuesto == 2) {
            $Ss_Inafecto += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          } else if (iGrupoImpuesto == 3) {
            $Ss_Exonerada += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          } else {
            $Ss_Gratuita += $Ss_SubTotal_Producto;
            globalImpuesto += 0;
          }
            
          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto) )) / 100);
        });
        
        if ($Ss_Descuento > 0.00) {
          var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
          if ($Ss_SubTotal > 0.00) {
            $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
            $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
            $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
            $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
          }
    
          if ($Ss_Inafecto > 0.00) {
            $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
            $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
            $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
          }
          
          if ($Ss_Exonerada > 0.00) {
            $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
            $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
            $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
          }
          
          if ($Ss_Gratuita > 0.00) {
            $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
            $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
            $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
          }
          
          $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
          $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
        } else
          $Ss_Descuento = $Ss_Descuento_p;
    
        if(isNaN($Ss_Descuento))
          $Ss_Descuento = 0.00;
        
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
        $( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
        
        $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
        $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
        
        $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
        $( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
        
        $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
        $( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
          
        $( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
        $( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
        
        $( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
        $( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    
        $( '#txt-total' ).val( $Ss_Total.toFixed(2) );
        $( '#span-total' ).text( $Ss_Total.toFixed(2) );
      }
    } else {
      $( '#cbo-almacen' ).closest('.form-group').find('.help-block').html('Seleccionar almacén');
      $( '#cbo-almacen' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    }// /. if - else validacion de seleccionar almacen
	})

  $('[name="arrIdPresupuesto"]').val('');

  url = base_url + 'Ventas/VentaController/ajax_edit/' + ID;
  $.ajax({
    url : url,
    type: "GET",
    dataType: "JSON",
    success: function(response){
      $( '.div-AgregarEditar' ).show();
      $( '.title_Venta' ).text('Modifcar Venta');
      $('[name="EID_Empresa"]').val(response.arrEdit[0].ID_Empresa);
      $('[name="EID_Documento_Cabecera"]').val('');
      $('[name="ID_Documento_Cabecera_Orden"]').val('');
      if ( response.arrEdit[0].ID_Tipo_Documento == 3 || response.arrEdit[0].ID_Tipo_Documento == 4 )
        $('[name="ID_Documento_Cabecera_Orden"]').val(response.arrEdit[0].ID_Documento_Cabecera_Enlace);

      //Datos Cliente
      $('[name="AID"]').val(response.arrEdit[0].ID_Entidad);
      $('[name="ANombre"]').val(response.arrEdit[0].No_Entidad);
      $('[name="ACodigo"]').val(response.arrEdit[0].Nu_Documento_Identidad);
      $('[name="Txt_Direccion_Entidad"]').val(response.arrEdit[0].Txt_Direccion_Entidad);

      //Datos Documento
      considerar_igv = response.arrEdit[0].Nu_Impuesto;

      url = base_url + 'HelperController/getOrganizaciones';
      $.post( url, function( responseOrganizaciones ){
        $( '#cbo-OrganizacionesVenta' ).html('');
        for (var i = 0; i < responseOrganizaciones.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Organizacion == responseOrganizaciones[i].ID_Organizacion)
            selected = 'selected="selected"';
          $( '#cbo-OrganizacionesVenta' ).append( '<option value="' + responseOrganizaciones[i].ID_Organizacion + '" ' + selected + '>' + responseOrganizaciones[i].No_Organizacion + '</option>' );
        }
      }, 'JSON');

      
	    nu_enlace = response.arrEdit[0].Nu_Enlace;
      url = base_url + 'HelperController/getTiposDocumentos';
      $.post( url, {Nu_Tipo_Filtro : 3}, function( responseTiposDocumento ){
        $( '#cbo-TiposDocumento' ).html('');
        for (var i = 0; i < responseTiposDocumento.length; i++){
          selected = '';
          /* if(response.arrEdit[0].ID_Tipo_Documento == responseTiposDocumento[i]['ID_Tipo_Documento']) */
          
          if(valor_cbo_TiposDocumento == responseTiposDocumento[i]['ID_Tipo_Documento'])
            selected = 'selected="selected"';
          $( '#cbo-TiposDocumento' ).append( '<option value="' + responseTiposDocumento[i]['ID_Tipo_Documento'] + '" data-nu_impuesto="' + responseTiposDocumento[i]['Nu_Impuesto'] + '" data-nu_enlace="' + responseTiposDocumento[i]['Nu_Enlace'] + '" ' + selected + '>' + responseTiposDocumento[i]['No_Tipo_Documento_Breve'] + '</option>' );
        }
      }, 'JSON');
      
		  url = base_url + 'HelperController/getSeriesDocumento';
      /* $.post( url, { ID_Organizacion : response.arrEdit[0].ID_Organizacion, ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento }, function( responseSeriesDocumento ){ */
        $.post( url, { ID_Organizacion : response.arrEdit[0].ID_Organizacion, ID_Tipo_Documento:valor_cbo_TiposDocumento  }, function( responseSeriesDocumento ){
      
        $( '#cbo-SeriesDocumento' ).html( '' );
        //alert(valor_cbo_SeriesDocumento);
        $('#cbo-SeriesDocumento').html('<option value="0">- Seleccionar -</option>');
        for (var i = 0; i < responseSeriesDocumento.length; i++){
          selected = '';
          if(valor_cbo_SeriesDocumento == responseSeriesDocumento[i]['ID_Serie_Documento'])
          /* if(response.arrEdit[0].ID_Serie_Documento == responseSeriesDocumento[i]['ID_Serie_Documento']) */
          selected = 'selected="selected"';
          $( '#cbo-SeriesDocumento' ).append( '<option value="' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '" ' + selected + ' data-id_serie_documento_pk=' + responseSeriesDocumento[i].ID_Serie_Documento_PK + '>' + responseSeriesDocumento[i]['ID_Serie_Documento'] + '</option>' );
        }
      }, 'JSON');
      
      $('[name="ID_Numero_Documento"]').val(valor_txt_ID_Numero_Documento);  

      url = base_url + 'HelperController/getMonedas';
      $.post( url , function( responseMonedas ){
        $( '#cbo-Monedas' ).html('');
        for (var i = 0; i < responseMonedas.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Moneda == responseMonedas[i]['ID_Moneda']){
            selected = 'selected="selected"';
	          $( '.span-signo' ).text( responseMonedas[i]['No_Signo'] );
          }
          $( '#cbo-Monedas' ).append( '<option value="' + responseMonedas[i]['ID_Moneda'] + '" data-no_signo="' + responseMonedas[i]['No_Signo'] + '" ' + selected + '>' + responseMonedas[i]['No_Moneda'] + '</option>' );
        }
      }, 'JSON');

      url = base_url + 'HelperController/getMediosPago';
      $.post( url , function( responseMediosPago ){
        $( '#cbo-MediosPago' ).html('');
        for (var i = 0; i < responseMediosPago.length; i++){
          selected = '';
          if(response.arrEdit[0].ID_Medio_Pago == responseMediosPago[i]['ID_Medio_Pago'])
            selected = 'selected="selected"';
          $( '#cbo-MediosPago' ).append( '<option value="' + responseMediosPago[i]['ID_Medio_Pago'] + '" data-nu_tipo="' + responseMediosPago[i]['Nu_Tipo'] + '" ' + selected + '>' + responseMediosPago[i]['No_Medio_Pago'] + '</option>' );
        }
      }, 'JSON');

  	  if ( response.arrEdit[0].Nu_Tipo == 1)// Si es Crédito
  	    $( '.div-MediosPago' ).show();

      $('.div-modal_datos_tarjeta_credito').hide();
      if (response.arrEdit[0].ID_Tipo_Medio_Pago > 0) {
        $('.div-modal_datos_tarjeta_credito').show();
        url = base_url + 'HelperController/getTiposTarjetaCredito';
        $.post(url, { ID_Medio_Pago: response.arrEdit[0].ID_Medio_Pago }, function (responseTipoTarjetCredito) {
          $('#cbo-tarjeta_credito').html('');
          for (var i = 0; i < responseTipoTarjetCredito.length; i++) {
            selected = '';
            if (response.arrEdit[0].ID_Tipo_Medio_Pago == responseTipoTarjetCredito[i].ID_Tipo_Medio_Pago)
              selected = 'selected="selected"';
            $('#cbo-tarjeta_credito').append('<option value="' + responseTipoTarjetCredito[i].ID_Tipo_Medio_Pago + '" ' + selected + '>' + responseTipoTarjetCredito[i].No_Tipo_Medio_Pago + '</option>');
          }
        }, 'JSON');
      }

      $( '#txt-Fe_Emision' ).datepicker({}).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $( '#txt-Fe_Vencimiento' ).datepicker('setStartDate', minDate);
      });

      var Fe_Emision = response.arrEdit[0].Fe_Emision.split('-');
      $( '#txt-Fe_Vencimiento' ).datepicker({
        autoclose : true,
        startDate : new Date(parseInt(Fe_Emision[0]), parseInt(Fe_Emision[1]) - 1, parseInt(Fe_Emision[2])),
        todayHighlight: true
      })
      $( '#txt-Fe_Vencimiento' ).datepicker('setStartDate', new Date(Fe_Emision[0] + "/" + Fe_Emision[1] + "/" + Fe_Emision[2]) );
      $( '#txt-Fe_Vencimiento' ).val(ParseDateString(response.arrEdit[0].Fe_Vencimiento, 6, '-'));


  	  //Validar N/C y N/D
	    if (response.arrEdit[0].Nu_Enlace == 1) {
	      $( '.div-DocumentoModificar' ).show();

        url = base_url + 'HelperController/getTiposDocumentosModificar';
        $.post( url, {Nu_Tipo_Filtro : 1}, function( responseTiposDocumentoModificar ){
          $( '#cbo-TiposDocumentoModificar' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
          for (var i = 0; i < responseTiposDocumentoModificar.length; i++){
            selected = '';
            if(response.arrEdit[0].ID_Tipo_Documento_Modificar == responseTiposDocumentoModificar[i]['ID_Tipo_Documento'])
              selected = 'selected="selected"';
            $( '#cbo-TiposDocumentoModificar' ).append( '<option value="' + responseTiposDocumentoModificar[i]['ID_Tipo_Documento'] + '" ' + selected + '>' + responseTiposDocumentoModificar[i]['No_Tipo_Documento_Breve'] + '</option>' );
          }
        }, 'JSON');

        url = base_url + 'HelperController/getMotivosReferenciaModificar';
        $.post( url, { ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento }, function( responseMotivosReferencia ){
          $( '#cbo-MotivoReferenciaModificar' ).html('');
          for (var i = 0; i < responseMotivosReferencia.length; i++){
            selected = '';
            if(response.arrEdit[0].Nu_Codigo_Motivo_Referencia == responseMotivosReferencia[i]['Nu_Valor'])
              selected = 'selected="selected"';
            $( '#cbo-MotivoReferenciaModificar' ).append( '<option value="' + responseMotivosReferencia[i]['Nu_Valor'] + '" ' + selected + '>' + responseMotivosReferencia[i]['No_Descripcion'] + '</option>' );
          }
        }, 'JSON');

  		  url = base_url + 'HelperController/getSeriesDocumentoModificar';
        $.post( url, { ID_Organizacion : response.arrEdit[0].ID_Organizacion, ID_Tipo_Documento: response.arrEdit[0].ID_Tipo_Documento_Modificar }, function( responseSeriesDocumentoModificar ){
          for (var i = 0; i < responseSeriesDocumentoModificar.length; i++){
            selected = '';
            if(response.arrEdit[0].ID_Serie_Documento_Modificar == responseSeriesDocumentoModificar[i]['ID_Serie_Documento'])
              selected = 'selected="selected"';
            $( '#cbo-SeriesDocumentoModificar' ).append( '<option value="' + responseSeriesDocumentoModificar[i]['ID_Serie_Documento'] + '" ' + selected + ' data-id_serie_documento_pk="' + responseSeriesDocumentoModificar[i]['ID_Serie_Documento_PK'] + '">' + responseSeriesDocumentoModificar[i]['ID_Serie_Documento'] + '</option>' );
      }
        }, 'JSON');

        $( '#txt-ID_Numero_Documento_Modificar' ).val(response.arrEdit[0].ID_Numero_Documento_Modificar);
	  }
     
      if ( valor_cbo_descargar_stock == "1" ) {
        $( '#cbo-descargar_stock' ).html( '<option value="1" selected>Si</option>' );
        $( '#cbo-descargar_stock' ).append( '<option value="0">No</option>' );
      } else {
        $( '#cbo-descargar_stock' ).html( '<option value="1">Si</option>' );
        $( '#cbo-descargar_stock' ).append( '<option value="0" selected>No</option>' );
      }
      
      // Presupuesto y oc 
      $('#txt-Nu_Presupuesto').val(response.arrEdit[0].Nu_Presupuesto);
      $('#txt-Nu_Orden_Compra').val(response.arrEdit[0].Nu_Orden_Compra);

      
      $('#cbo-tipos_servicios').html('<option value="">- No hay registros -</option>');
      url = base_url + 'HelperController/getTiposServicios';
      $.post(url, { }, function (responseTiposServicios) {
        if (responseTiposServicios.sStatus == 'success') {
  
          var l = responseTiposServicios.arrData.length;
         
            $('#cbo-tipos_servicios').html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var x = 0; x < l; x++) {
              selected = '';
              /* if (response.arrEdit[0].ID_Servicio_Presupuesto_Negocio == responseTiposServicios.arrData[x].ID_Servicio_Presupuesto_Negocio) */
              if (valor_cbo_tipos_servicios == responseTiposServicios.arrData[x].ID_Servicio_Presupuesto_Negocio)
                selected = 'selected="selected"';
              $('#cbo-tipos_servicios').append('<option value="' + responseTiposServicios.arrData[x].ID_Servicio_Presupuesto_Negocio + '" ' + selected + '>' + responseTiposServicios.arrData[x].No_Servicio_Presupuesto_Negocio + '</option>');
            }

        }
      }, 'JSON'); 


    	/* Personal de ventas */
      
      $( '#cbo-vendedor' ).html('<option value="">- No hay personal -</option>');
      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData: 'entidad', iTipoEntidad: 4}, function( responsePersonal ){
        if ( responsePersonal.sStatus == 'success' ) {
          var l = responsePersonal.arrData.length;
          
            $( '#cbo-vendedor' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var x = 0; x < l; x++) {
              selected = '';
              if(valor_cbo_vendedor == responsePersonal.arrData[x].ID)
                selected = 'selected="selected"';
              $( '#cbo-vendedor' ).append( '<option value="' + responsePersonal.arrData[x].ID + '" ' + selected + '>' + responsePersonal.arrData[x].Nombre + '</option>' );
            }
          
        }
      }, 'JSON');
      
      /* /. Personal de ventas */

    	/* Porcentaje para ventas */
      
      $( '#cbo-porcentaje' ).html('<option value="" selected="selected">- No hay porcentaje -</option>');
      url = base_url + 'HelperController/getDataGeneral';
      $.post( url, {sTipoData: 'Porcentaje_Comision_Vendedores'}, function( responsePorcentaje ){
        if ( responsePorcentaje.sStatus == 'success' ) {
          var l = responsePorcentaje.arrData.length;
        
            $( '#cbo-porcentaje' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
            for (var x = 0; x < l; x++) {
              selected = '';
              if(valor_cbo_porcentaje == responsePorcentaje.arrData[x].ID_Tabla_Dato)
                selected = 'selected="selected"';
              $( '#cbo-porcentaje' ).append( '<option value="' + responsePorcentaje.arrData[x].ID_Tabla_Dato + '" ' + selected + '>' + responsePorcentaje.arrData[x].No_Descripcion + '</option>' );
            }
        
        }
      }, 'JSON'); 
      /* /. Porcentaje para ventas */

	    $( '#cbo-lista_precios' ).html('');
      url = base_url + 'HelperController/getListaPrecio';
      $.post( url, {Nu_Tipo_Lista_Precio : $( '[name="Nu_Tipo_Lista_Precio"]' ).val(), ID_Organizacion: response.arrEdit[0].ID_Organizacion, ID_Almacen : response.arrEdit[0].ID_Almacen}, function( responseLista ){
        for (var i = 0; i < responseLista.length; i++) {
          selected = '';
          if(response.arrEdit[0].ID_Lista_Precio_Cabecera == responseLista[i].ID_Lista_Precio_Cabecera)
            selected = 'selected="selected"';
          $( '#cbo-lista_precios' ).append( '<option value="' + responseLista[i].ID_Lista_Precio_Cabecera + '" ' + selected + '>' + responseLista[i].No_Lista_Precio + '</option>' );
        }
      }, 'JSON');

      $('[name="Txt_Glosa"]').val( '' );
      if (response.arrEdit[0].Txt_Glosa != '' && response.arrEdit[0].Txt_Glosa != null)
        $('[name="Txt_Glosa"]').val( clearHTMLTextArea(response.arrEdit[0].Txt_Glosa) );

      $('[name="Txt_Garantia"]').val( response.arrEdit[0].Txt_Garantia );

      // Detracción
      $('#radio-InactiveDetraccion').prop('checked', true).iCheck('update');
      $('#radio-ActiveDetraccion').prop('checked', false).iCheck('update');
      if (response.arrEdit[0].Nu_Detraccion != '0') {//0 = No y 1 = Si
        $('#radio-InactiveDetraccion').prop('checked', false).iCheck('update');
        $('#radio-ActiveDetraccion').prop('checked', true).iCheck('update');
      }

      // Retencion
      $('#radio-InactiveRetencion').prop('checked', true).iCheck('update');
      $('#radio-ActiveRetencion').prop('checked', false).iCheck('update');
      if (response.arrEdit[0].Nu_Retencion != '0') {//0 = No y 1 = Si
        $('#radio-InactiveRetencion').prop('checked', false).iCheck('update');
        $('#radio-ActiveRetencion').prop('checked', true).iCheck('update');
      }

      //Formato PDF
      
      var arrFormatoPDF = [
        {"No_Formato_PDF": "A4"},
        {"No_Formato_PDF": "A5"},
        {"No_Formato_PDF": "TICKET"},
      ];
      $( '#cbo-formato_pdf' ).html('');
      for (var i = 0; i < arrFormatoPDF.length; i++) {
        selected = '';
        /* if(response.arrEdit[0].No_Formato_PDF == arrFormatoPDF[i]['No_Formato_PDF']) */
        if(valor_cbo_formato_pdf == arrFormatoPDF[i]['No_Formato_PDF'])
          selected = 'selected="selected"';
        $( '#cbo-formato_pdf' ).append( '<option value="' + arrFormatoPDF[i]['No_Formato_PDF'] + '" ' + selected + '>' + arrFormatoPDF[i]['No_Formato_PDF'] + '</option>' );
      }
      
      //Detalle
      $( '#table-DetalleProductosVenta' ).show();
      $( '#table-DetalleProductosVenta tbody' ).empty();

      var table_detalle_producto = '';
      var _ID_Producto = '';
      var $Ss_SubTotal_Producto = 0.00;
      var $Ss_IGV_Producto = 0.00;
      var $Ss_Descuento_Producto = 0.00;
      var $Ss_Total_Producto = 0.00;
      var $Ss_Gravada = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var option_impuesto_producto = '';

      var $fDescuento_Producto = 0;
      var fDescuento_Total_Producto = 0;
      var globalImpuesto = 0;
      var $iDescuentoGravada = 0;
      var $iDescuentoExonerada = 0;
      var $iDescuentoInafecto = 0;
      var $iDescuentoGratuita = 0;
      var $iDescuentoGlobalImpuesto = 0;
      var selected;

      var iTotalRegistros = response.arrEdit.length;
      var iTotalRegistrosImpuestos = response.arrImpuesto.length;

      console.log(response.arrEdit);
      console.log(iTotalRegistros);

      for (var i = 0; i < iTotalRegistros; i++) {
        if (_ID_Producto != response.arrEdit[i].ID_Producto) {
          _ID_Producto = response.arrEdit[i].ID_Producto;
          option_impuesto_producto = '';
        }

        $Ss_SubTotal_Producto = parseFloat(response.arrEdit[i].Ss_SubTotal_Producto)
        if (response.arrEdit[i].Nu_Tipo_Impuesto == 1){
					$Ss_IGV += parseFloat(response.arrEdit[i].Ss_Impuesto_Producto);
					$Ss_Gravada += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 2){
          $Ss_Inafecto += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 3){
          $Ss_Exonerada += $Ss_SubTotal_Producto;
        } else if (response.arrEdit[i].Nu_Tipo_Impuesto == 4){
          $Ss_Gratuita += $Ss_SubTotal_Producto;
        }

        $Ss_Descuento_Producto += parseFloat(response.arrEdit[i].Ss_Descuento_Producto);
        $Ss_Total += parseFloat(response.arrEdit[i].Ss_Total_Producto);

	      for (var x = 0; x < iTotalRegistrosImpuestos; x++){
	        selected = '';
	        if (response.arrImpuesto[x].ID_Impuesto_Cruce_Documento == response.arrEdit[i].ID_Impuesto_Cruce_Documento)
	          selected = 'selected="selected"';
          option_impuesto_producto += "<option value='" + response.arrImpuesto[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + response.arrImpuesto[x].Nu_Tipo_Impuesto + "' data-impuesto_producto='" + response.arrImpuesto[x].Ss_Impuesto + "' " + selected + ">" + response.arrImpuesto[x].No_Impuesto + "</option>";
        }

        console.log(response);
        table_detalle_producto +=
          "<tr id='tr_detalle_producto" + response.arrEdit[i].ID_Producto + "'>"
            + "<td style='display:none;' class='text-left td-iIdItem'>"
              + "<input name='Edit_ID_Documento_Detalle[]' id='Edit_ID_Documento_Detalle' class='txt-Edit_ID_Documento_Detalle' value='" + response.arrEdit[i].ID_Documento_Detalle + "' />"
              + response.arrEdit[i].ID_Producto
            + "</td>"
            + "<td class='text-right'><input type='text' class='txt-Qt_Producto form-control input-decimal' " + (response.arrEdit[i].Nu_Tipo_Producto == 1 ? 'onkeyup=validateStockNow(event);' : '') + " data-id_item='" + response.arrEdit[i].ID_Producto + "' data-id_producto='" + response.arrEdit[i].ID_Producto + "' value='" + response.arrEdit[i].Qt_Producto + "' value='" + response.arrEdit[i].Qt_Producto + "' autocomplete='off'></td>"
            + "<td class='text-left'>" + response.arrEdit[i].No_Producto + "</td>"
            + "<td class='text-right'><input type='text' class='txt-fValorUnitario form-control input-decimal' value='" + parseFloat(response.arrEdit[i].Ss_Precio / response.arrEdit[i].Ss_Impuesto).toFixed(2) + "' autocomplete='off'></td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Precio form-control input-decimal' value='" + response.arrEdit[i].Ss_Precio + "' autocomplete='off'></td>"
            + "<td class='text-right'>"
            + "<select class='cbo-ImpuestosProducto form-control required' style='width: 100%;'>"
            + option_impuesto_producto
            + "</select>"
            + "</td>"
            + "<td style='display:none;' class='text-right'><input type='tel' class='txt-Ss_SubTotal_Producto form-control' value='" + response.arrEdit[i].Ss_SubTotal_Producto + "' autocomplete='off' disabled></td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Descuento form-control input-decimal' value='" + (response.arrEdit[i].Po_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Po_Descuento_Impuesto_Producto) + "' autocomplete='off'></td>"
            + "<td class='text-right'><input type='text' class='txt-Ss_Total_Producto form-control input-decimal' value='" + response.arrEdit[i].Ss_Total_Producto + "' autocomplete='off'></td>"
            + "<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Producto) + "</td>"
            + "<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>" + (response.arrEdit[i].Ss_Descuento_Impuesto_Producto == 0.00 ? '' : response.arrEdit[i].Ss_Descuento_Impuesto_Producto) + "</td>"
            + "<td style='display:none;' class='text-right td-iIdPresupuesto'>" + response.arrEdit[i].ID_OI_Detalle + "</td>"
            +"<td class='text-center'><button type='button' id='btn-deleteProducto' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-2x' aria-hidden='true'> </i></button></td>"
          + "</tr>";
      }

		  $( '#table-DetalleProductosVenta > tbody' ).append(table_detalle_producto);
			$( '#txt-subTotal' ).val( $Ss_Gravada.toFixed(2) );
			$( '#span-subTotal' ).text( $Ss_Gravada.toFixed(2) );
      $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
			$( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
			$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
			$( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
			$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );

      if (parseFloat(response.arrEdit[0].Ss_Descuento) > 0 && $Ss_Descuento_Producto == 0)
        $( '#txt-Ss_Descuento' ).val( response.arrEdit[0].Po_Descuento );
      else
        $( '#txt-Ss_Descuento' ).val( '' );

      $( '#txt-descuento' ).val( response.arrEdit[0].Ss_Descuento );
      $( '#span-descuento' ).text( response.arrEdit[0].Ss_Descuento );
			$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
			$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
			$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
			$( '#span-total' ).text( $Ss_Total.toFixed(2) );

      validateDecimal();
		  validateNumber();
  		validateNumberOperation();

      url = base_url + 'HelperController/getImpuestos';
      $.post( url , function( response ){
        arrImpuestosProducto = '';
        arrImpuestosProductoDetalle = '';
        for (var i = 0; i < response.length; i++)
          arrImpuestosProductoDetalle += '{"ID_Impuesto_Cruce_Documento" : "' + response[i].ID_Impuesto_Cruce_Documento + '", "Ss_Impuesto":"' + response[i].Ss_Impuesto + '", "Nu_Tipo_Impuesto":"' + response[i].Nu_Tipo_Impuesto + '", "No_Impuesto":"' + response[i].No_Impuesto + '"},';
        arrImpuestosProducto = '{ "arrImpuesto" : [' + arrImpuestosProductoDetalle.slice(0, -1) + ']}';

        $( '#modal-loader' ).modal('hide');
      }, 'JSON');

      var _ID_Producto = '';
      var option_impuesto_producto = '';
    }
  })
}


// M02 - FIN 




	$( '#btn-addProducto' ).click(function(){
	  addItemDetail($( '#txt-ID_Producto' ).val(), $( '#txt-No_Producto' ).val(), parseFloat($( '#txt-Ss_Precio' ).val()), $( '#txt-ID_Impuesto_Cruce_Documento' ).val(), $( '#txt-Nu_Tipo_Impuesto' ).val(), $( '#txt-Ss_Impuesto' ).val(), parseFloat($( '#txt-Qt_Producto' ).val()), $( '#txt-nu_tipo_item' ).val(), 0);
	})

  $('#table-DetalleProductosVenta tbody' ).on('input', '.txt-fValorUnitario', function(){
    var fila = $(this).parents("tr");
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var fValorUnitario = fila.find(".txt-fValorUnitario").val();
    var fPrecioVenta = 0.00;
    fPrecioVenta = parseFloat(fValorUnitario * impuesto_producto).toFixed(6);
    var precio = fPrecioVenta;
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;
    
    fila.find(".txt-Ss_Precio").val( fPrecioVenta );

    if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0){
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
	    $( '#table-DetalleProductosVenta tfoot' ).empty();
      if (nu_tipo_impuesto == 1){//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) * impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) * impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". ") );
  		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". ") );
        
        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 1){
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
        });
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
    		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
    		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 2){//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
    		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 3){//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
    		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 4){//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
    		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      }
    }
  })

  $('#table-DetalleProductosVenta tbody' ).on('input', '.txt-Ss_Precio', function(){
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;
    
    fila.find(".txt-fValorUnitario").val( parseFloat(precio / impuesto_producto).toFixed(2) );

    if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0){
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
	    $( '#table-DetalleProductosVenta tfoot' ).empty();
      if (nu_tipo_impuesto == 1){//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". ") );
  		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". ") );
        
        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 1){
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
        });
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
    		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
    		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 2){//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
    		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 3){//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
    		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 4){//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
    		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      }
    }
  })
	
  $('#table-DetalleProductosVenta tbody' ).on('input', '.txt-Qt_Producto', function(){
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;
    
    if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0){
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $( '#table-DetalleProductosVenta tfoot' ).empty();
  		if (nu_tipo_impuesto == 1){//CON IGV
        fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto);
        fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". ") );
  		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". ") );
        
        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 1){
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
        });
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
    		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
    		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 2){//Inafecto
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
    		fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad)  - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );

        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
    		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 3){//Exonerada
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
    		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else if (nu_tipo_impuesto == 4){//Gratuita
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat(((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)) * impuesto_producto).toFixed(2)).toString().split(". ") );
        
        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
    		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
      } else {//Sin ningun tipo de impuesto
        fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
        fila.find(".txt-Ss_Total_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". ") );

        var $Ss_Subtotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
            
          if (considerar_igv == 0)
            $Ss_Subtotal += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-subTotal' ).val( $Ss_Subtotal.toFixed(2) );
    		$( '#span-subTotal' ).text( $Ss_Subtotal.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
  		}
    }
  })

  $('#table-DetalleProductosVenta tbody' ).on('change', '.cbo-ImpuestosProducto', function(){
    var fila = $(this).parents("tr");
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if (considerar_igv == 1) {//SI IGV
      if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(total_producto) > 0){
        if (nu_tipo_impuesto == 1){//CON IGV
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
    		  fila.find(".txt-Ss_Precio").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );
    		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );
    		  
          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
            
            if (Nu_Tipo_Impuesto == 1){
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          });
          
          $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      		
          $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
      		
          $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
      		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
      		
          $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
      		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
      		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
        } else if (nu_tipo_impuesto == 2){//Inafecto
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_Precio").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );
    		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );
    		  
          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
  
            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
            
            if (Nu_Tipo_Impuesto == 1){
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += (($Ss_SubTotal_Producto * Ss_Impuesto) - $Ss_SubTotal_Producto);
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });
          
      		$( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      		
          $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
      		
          $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
      		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
      		
          $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
      		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
      		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
        } else if (nu_tipo_impuesto == 3){//Exonerada
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_Precio").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );
    		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );

          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
    
            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
            
            if (Nu_Tipo_Impuesto == 1){
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += (($Ss_SubTotal_Producto * Ss_Impuesto) - $Ss_SubTotal_Producto);
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });
          
      		$( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      		
          $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
      		
          $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
      		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
      		
          $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
      		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
      		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
        } else if (nu_tipo_impuesto == 4){//Gratuita
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (subtotal_producto * impuesto_producto)) / 100) - ((descuento * (subtotal_producto)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_Precio").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );
    		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(subtotal_producto * impuesto_producto).toFixed(2)).toString().split(". ") );

          var $Ss_SubTotal = 0.00;
          var $Ss_Exonerada = 0.00;
          var $Ss_Inafecto = 0.00;
          var $Ss_Gratuita = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
    
            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
            
            if (Nu_Tipo_Impuesto == 1){
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += (($Ss_SubTotal_Producto * Ss_Impuesto) - $Ss_SubTotal_Producto);
            } else if (Nu_Tipo_Impuesto == 2) {
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 3) {
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            } else if (Nu_Tipo_Impuesto == 4) {
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            }
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });
          
      		$( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      		
          $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
      		
          $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
      		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
      		
          $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
      		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
      		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
    		}
      }
    }
  })
  
  $('#table-DetalleProductosVenta tbody' ).on('input', '.txt-Ss_Descuento', function(){
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    var fDescuento_SubTotal_Producto = 0, fDescuento_Total_Producto = 0;

    if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(descuento) >= 0 && parseFloat(total_producto) > 0 && (parseFloat($( '#txt-Ss_Descuento' ).val()) == 0 || $( '#txt-Ss_Descuento' ).val() == '')){
      if ( parseFloat(subtotal_producto) >= parseFloat(((subtotal_producto * descuento) / 100)) ){
        if (nu_tipo_impuesto == 1){//CON IGV
          fDescuento_SubTotal_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))) / impuesto_producto );
          fDescuento_Total_Producto = parseFloat(((precio * cantidad) - (((descuento * (precio * cantidad)) / 100))));
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10((((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - (((descuento * (precio * cantidad)) / 100) / impuesto_producto), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(Math.round10(fDescuento_SubTotal_Producto, -6)).toFixed(6)).toString().split(". ") );
    		  fila.find(".txt-Ss_Total_Producto").val( (parseFloat(Math.round10(fDescuento_Total_Producto, -2)).toFixed(2)).toString().split(". ") );
          
          var $Ss_SubTotal = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_IGV = 0.00;
          var $Ss_Total = 0.00;
          var $fDescuento_Producto = 0;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Ss_Impuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
            var Nu_Tipo_Impuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / Ss_Impuesto);
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
            var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

            $Ss_Total += $Ss_Total_Producto;

            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
          
            if (Nu_Tipo_Impuesto == 1){
              $Ss_SubTotal += $Ss_SubTotal_Producto;
              $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
            }
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          });
          
          $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
      		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
        } else if (nu_tipo_impuesto == 2){//Inafecto
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
          fila.find(".txt-Ss_Total_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". ") );

          var $Ss_Inafecto = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
  
            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
              
            if (Nu_Tipo_Impuesto == 2)
              $Ss_Inafecto += $Ss_SubTotal_Producto;
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });
          
          $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
      		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
        } else if (nu_tipo_impuesto == 3){//Exonerada
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
          fila.find(".txt-Ss_Total_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". ") );

          var $Ss_Exonerada = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
  
            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
              
            if (Nu_Tipo_Impuesto == 3)
              $Ss_Exonerada += $Ss_SubTotal_Producto;
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });
          
          $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
        } else if (nu_tipo_impuesto == 4){//Gratuita
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
          fila.find(".txt-Ss_Total_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". ") );

          var $Ss_Gratuita = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
  
            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;
              
            if (Nu_Tipo_Impuesto == 4)
              $Ss_Gratuita += $Ss_SubTotal_Producto;
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });
          
          $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
      		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
        } else if (considerar_igv == 0) {//Sin ningun tipo de impuesto
          fila.find(".td-fDescuentoSinImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".td-fDescuentoImpuestosItem").text( (parseFloat(Math.round10(((descuento * (precio * cantidad)) / 100) - ((descuento * (precio * cantidad)) / 100), -2)).toFixed(2)).toString().split(". ") );
          fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(6)).toString().split(". ") );
          fila.find(".txt-Ss_Total_Producto").val( (parseFloat((precio * cantidad) - ((descuento * (precio * cantidad)) / 100)).toFixed(2)).toString().split(". ") );

          var $Ss_SubTotal = 0.00;
          var $Ss_Descuento = 0.00;
          var $Ss_Total = 0.00;          
          $("#table-DetalleProductosVenta > tbody > tr").each(function(){
            var rows = $(this);
            var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
            var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
  
            if(isNaN($Ss_Descuento_Producto))
              $Ss_Descuento_Producto = 0;

            if (considerar_igv == 0)
              $Ss_SubTotal += $Ss_SubTotal_Producto;
            
            $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
            $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
          });
          
          $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      		
      		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
      		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
      		
      		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
      		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
    		}
      }
    }
  })

  $('#table-DetalleProductosVenta tbody' ).on('input', '.txt-Ss_Total_Producto', function(){
    var fila = $(this).parents("tr");
    var $ID_Producto = fila.find(".txt-Ss_Precio").data('id_producto');
    var precio = fila.find(".txt-Ss_Precio").val();
    var cantidad = fila.find(".txt-Qt_Producto").val();
    var subtotal_producto = fila.find(".txt-Ss_SubTotal_Producto").val();
    var impuesto_producto = fila.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto');
    var nu_tipo_impuesto = fila.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var descuento = fila.find(".txt-Ss_Descuento").val();
    var total_producto = fila.find(".txt-Ss_Total_Producto").val();
    
    if ( parseFloat(precio) > 0.00 && parseFloat(cantidad) > 0 && parseFloat(total_producto) > 0){
      $('#tr_detalle_producto' + $ID_Producto).removeClass('danger');
      $( '#table-DetalleProductosVenta tfoot' ).empty();
      if (nu_tipo_impuesto == 1){//CON IGV        
        fila.find(".txt-Ss_Precio").val( (parseFloat(total_producto / cantidad).toFixed(2)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". ") );
        
        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Ss_Impuesto           = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
          var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

          $Ss_Total += $Ss_Total_Producto;

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
          
          if (Nu_Tipo_Impuesto == 1){
            $Ss_SubTotal += $Ss_SubTotal_Producto;
            $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          }
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
        });
        
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
    		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
    		$( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
  		} else if (nu_tipo_impuesto == 2){//Inafecto
        fila.find(".txt-Ss_Precio").val( (parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". ") );
        
        var $Ss_Inafecto = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
          
          if (Nu_Tipo_Impuesto == 2)
            $Ss_Inafecto += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
    		$( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
  		} else if (nu_tipo_impuesto == 3){//Exonerada
        fila.find(".txt-Ss_Precio").val( (parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". ") );
        
        var $Ss_Exonerada = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
          
          if (Nu_Tipo_Impuesto == 3)
            $Ss_Exonerada += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
    		$( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
  		} else if (nu_tipo_impuesto == 4){//Gratuita
        fila.find(".txt-Ss_Precio").val( (parseFloat((total_producto / cantidad) / impuesto_producto).toFixed(3)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(total_producto / impuesto_producto).toFixed(6)).toString().split(". ") );
        
        var $Ss_Gratuita = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_IGV = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var Nu_Tipo_Impuesto      = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;
          
          if (Nu_Tipo_Impuesto == 4)
            $Ss_Gratuita += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
    		$( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
  		} else {//Sin ningun tipo de impuesto
        fila.find(".txt-Ss_Precio").val( (parseFloat((total_producto - ((descuento * total_producto) / 100)) / cantidad).toFixed(3)).toString().split(". ") );
        fila.find(".txt-Ss_SubTotal_Producto").val( (parseFloat(total_producto - ((descuento * total_producto) / 100)).toFixed(6)).toString().split(". ") );

        var $Ss_SubTotal = 0.00;
        var $Ss_Descuento = 0.00;
        var $Ss_Total = 0.00;
        $("#table-DetalleProductosVenta > tbody > tr").each(function(){
          var rows = $(this);
          var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
          var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

          if(isNaN($Ss_Descuento_Producto))
            $Ss_Descuento_Producto = 0;

          if (considerar_igv == 0)
            $Ss_SubTotal += $Ss_SubTotal_Producto;
          
          $Ss_Descuento += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / Ss_Impuesto) )) / 100);
          $Ss_Total += parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());
        });
        
        $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
    		$( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
    		
    		$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    		$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
    		
    		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
    		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
  		}
    }
  })

	$('#table-DetalleProductosVenta tbody' ).on('click', '#btn-deleteProducto', function(){
    if($(this).closest('tr').find('.txt-Edit_ID_Documento_Detalle', this).val()) {
      detalleItemsFacturaDelete.push($(this).closest('tr').find('.txt-Edit_ID_Documento_Detalle', this).val());
    }
    $(this).closest('tr').remove ();

    var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
    var $Ss_SubTotal = 0.00;
    var $Ss_Exonerada = 0.00;
    var $Ss_Inafecto = 0.00;
    var $Ss_Gratuita = 0.00;
    var $Ss_IGV = 0.00;
    var $Ss_Total = 0.00;
    var iCantDescuento = 0;
    var globalImpuesto = 0;
    var $Ss_Descuento_p = 0;
    $("#table-DetalleProductosVenta > tbody > tr").each(function(){
      var rows = $(this);
      var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
      var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
      var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
      var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
      var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

      $Ss_Total += $Ss_Total_Producto;

      if (iGrupoImpuesto == 1) {
        $Ss_SubTotal += $Ss_SubTotal_Producto;
        $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
        globalImpuesto = fImpuesto;
      } else if (iGrupoImpuesto == 2) {
        $Ss_Inafecto += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else if (iGrupoImpuesto == 3) {
        $Ss_Exonerada += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      } else {
        $Ss_Gratuita += $Ss_SubTotal_Producto;
        globalImpuesto += 0;
      }

      if(isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;

      $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto) )) / 100);
    });

    if ($Ss_Descuento > 0.00) {
      var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
      if ($Ss_SubTotal > 0.00) {
        $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
        $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
        $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
        $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
      }

      if ($Ss_Inafecto > 0.00) {
        $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
        $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
        $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
      }

      if ($Ss_Exonerada > 0.00) {
        $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
        $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
        $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
      }

      if ($Ss_Gratuita > 0.00) {
        $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
        $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
        $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
      }

      $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
      $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Gratuita;
    } else
      $Ss_Descuento = $Ss_Descuento_p;

    if(isNaN($Ss_Descuento))
      $Ss_Descuento = 0.00;

    $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
    $( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
    $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
    $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
    $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
    $( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
    $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
    $( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
    $( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
    $( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
  	$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
  	$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
		$( '#span-total' ).text( $Ss_Total.toFixed(2) );

    if ($('#table-DetalleProductosVenta >tbody >tr').length == 0) {
      $('#table-DetalleProductosVenta').hide();
    }
	})

	//Calcular porcentaje - Pendiente con fio, cuando tengo varios items y algunos son inafectos ó igv.
	$('#table-VentaTotal' ).on('input', '#txt-Ss_Descuento', function(){
    var $Ss_Descuento_Producto = 0.00;
    $("#table-DetalleProductosVenta > tbody > tr").each(function(){
      var rows = $(this);
      $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());

      if(isNaN($Ss_Descuento_Producto))
        $Ss_Descuento_Producto = 0;

      $Ss_Descuento_Producto += $Ss_Descuento_Producto;
    })

    if ($Ss_Descuento_Producto == 0) {
  		var $Ss_Descuento = parseFloat($(this).val());
      var $Ss_SubTotal = 0.00;
      var $Ss_Exonerada = 0.00;
      var $Ss_Inafecto = 0.00;
      var $Ss_Gratuita = 0.00;
      var $Ss_IGV = 0.00;
      var $Ss_Total = 0.00;
      var globalImpuesto = 0;
      $("#table-DetalleProductosVenta > tbody > tr").each(function(){
        var rows = $(this);
        var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
        var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
        var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_SubTotal_Producto', this).val());
        var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

        $Ss_Total += $Ss_Total_Producto;

        if (iGrupoImpuesto == 1) {
          $Ss_SubTotal += $Ss_SubTotal_Producto;
          $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
          globalImpuesto = fImpuesto;
        } else if (iGrupoImpuesto == 2) {
          $Ss_Inafecto += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else if (iGrupoImpuesto == 3) {
          $Ss_Exonerada += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        } else {
          $Ss_Gratuita += $Ss_SubTotal_Producto;
          globalImpuesto += 0;
        }
      });

      if ($Ss_Descuento > 0.00) {
        var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
        if ($Ss_SubTotal > 0.00) {
          $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
          $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
          $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
          $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
        }

        if ($Ss_Inafecto > 0.00) {
          $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
          $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
          $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
        }

        if ($Ss_Exonerada > 0.00) {
          $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
          $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
          $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
        }

        if ($Ss_Gratuita > 0.00) {
          $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
          $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
          $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
        }

        $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
        $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
      }

      $( '#txt-subTotal' ).val( $Ss_SubTotal.toFixed(2) );
      $( '#span-subTotal' ).text( $Ss_SubTotal.toFixed(2) );
      $( '#txt-exonerada' ).val( $Ss_Exonerada.toFixed(2) );
      $( '#span-exonerada' ).text( $Ss_Exonerada.toFixed(2) );
      $( '#txt-inafecto' ).val( $Ss_Inafecto.toFixed(2) );
      $( '#span-inafecto' ).text( $Ss_Inafecto.toFixed(2) );
      $( '#txt-gratuita' ).val( $Ss_Gratuita.toFixed(2) );
      $( '#span-gratuita' ).text( $Ss_Gratuita.toFixed(2) );
      $( '#txt-impuesto' ).val( $Ss_IGV.toFixed(2) );
      $( '#span-impuesto' ).text( $Ss_IGV.toFixed(2) );
    	$( '#txt-descuento' ).val( $Ss_Descuento.toFixed(2) );
    	$( '#span-descuento' ).text( $Ss_Descuento.toFixed(2) );
  		$( '#txt-total' ).val( $Ss_Total.toFixed(2) );
  		$( '#span-total' ).text( $Ss_Total.toFixed(2) );
    }
  })
})

function isExistTableTemporalProducto($ID_Producto){
  return Array.from($('tr[id*=tr_detalle_producto]'))
    .some(element => ($('td:nth(0)',$(element)).html()===$ID_Producto));
}

function form_Venta(){
  if (accion == 'add_factura_venta' || accion == 'upd_factura_venta') {
    var arrDetalleVenta = [], arrValidarNumerosEnCero = [], $counterNumerosEnCero = 0, tr_foot = '';

    $("#table-DetalleProductosVenta > tbody > tr").each(function() {
      var rows = $(this);

      var $ID_Documento_Detalle = $('.txt-Edit_ID_Documento_Detalle', this).val();
      var $ID_Producto = rows.find(".td-iIdItem").text();
      var $Qt_Producto = $('.txt-Qt_Producto', this).val();
      var $Ss_Precio = $('.txt-Ss_Precio', this).val();
      var $ID_Impuesto_Cruce_Documento = $('.cbo-ImpuestosProducto option:selected', this).val();
      var $Ss_SubTotal = $('.txt-Ss_SubTotal_Producto', this).val();
      var $Ss_Descuento = $('.txt-Ss_Descuento', this).val();
      var $Ss_Total = $('.txt-Ss_Total_Producto', this).val();
      var $fDescuentoSinImpuestosItem = rows.find(".td-fDescuentoSinImpuestosItem").text();
      var $fDescuentoImpuestosItem = rows.find(".td-fDescuentoImpuestosItem").text();
      var $iIdPresupuesto = rows.find(".td-iIdPresupuesto").text();

      if (parseFloat($Ss_Precio) == 0 || parseFloat($Qt_Producto) == 0 || parseFloat($Ss_Total) == 0){
        arrValidarNumerosEnCero[$counterNumerosEnCero] = $ID_Producto;
        $('#tr_detalle_producto' + $ID_Producto).addClass('danger');
      }

      var obj = {};
      obj.ID_Documento_Detalle	= $ID_Documento_Detalle;
      obj.ID_Producto	= $ID_Producto;
      obj.Ss_Precio = $Ss_Precio;
      obj.Qt_Producto = $Qt_Producto;
      obj.ID_Impuesto_Cruce_Documento	= $ID_Impuesto_Cruce_Documento;
      obj.Ss_SubTotal = $Ss_SubTotal;
      obj.Ss_Descuento = $Ss_Descuento;
      obj.Ss_Impuesto = $Ss_Total - $Ss_SubTotal;
      obj.Ss_Total = $Ss_Total;
      obj.fDescuentoSinImpuestosItem = $fDescuentoSinImpuestosItem;
      obj.fDescuentoImpuestosItem	= $fDescuentoImpuestosItem;
      obj.iIdPresupuesto = $iIdPresupuesto;
      arrDetalleVenta.push(obj);
      $counterNumerosEnCero++;
    });

    bEstadoValidacion = validatePreviousDocumentToSaveSale();

    if ( arrDetalleVenta.length === 0){
  		$( '#panel-DetalleProductosVenta' ).removeClass('panel-default');
  		$( '#panel-DetalleProductosVenta' ).addClass('panel-danger');
      $( '#txt-No_Producto' ).closest('.form-group').find('.help-block').html('Documento <b>sin detalle</b>');
  	  $( '#txt-No_Producto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
		  scrollToError( $("html, body"), $( '#txt-No_Producto' ) );
    } else if (arrValidarNumerosEnCero.length > 0) {
      tr_foot +=
      "<tfoot>"
        +"<tr class='danger'>"
          +"<td colspan='9' class='text-center'>Item(s) con <b>precio / cantidad / total en cero</b></td>"
        +"</tr>"
      +"<tfoot>";
      $( '#table-DetalleProductosVenta > tbody' ).after(tr_foot);
    } else if (bEstadoValidacion) {
      if ($('[name="addCliente"]:checked').attr('value') == 1){//Agregar cliente
        if ( $( '#cbo-Estado' ).val() == 1 ) {//1 = Activo
          saveFacturaVenta(arrDetalleVenta);
        } else {
          $( '#modal-message' ).modal('show');
          $( '.modal-message' ).addClass('modal-danger');
          $( '.modal-title-message' ).text( 'El cliente se encuentra con BAJA DE OFICIO / NO HABIDO' );
          setTimeout(function() {$('#modal-message').modal('hide');}, 2500);
        }
      } else {
        var sNombreEntidad = $( '#txt-AID' ).val() == 1 ? 'clientes varios' : '';
        var arrPOST = {
          sTipoData : 'get_entidad',
          iTipoEntidad : '0',//0=Cliente
          iIDEntidad : $( '#txt-AID' ).val(),
          sNombreEntidad : sNombreEntidad,
        };
        url = base_url + 'HelperController/getDataGeneral';
        $.post(url, arrPOST, function(response){
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          if ( response.sStatus == 'success' ) {// Si el RUC es válido
            if ( response.arrData[0].Nu_Estado == '1' ){
              saveFacturaVenta(arrDetalleVenta);
            } else if ( response.arrData[0].Nu_Estado != '1' ){
              $( '#modal-message' ).modal('show');
              $( '.modal-message' ).addClass('modal-danger');
              $( '.modal-title-message' ).text( 'El cliente se encuentra con BAJA DE OFICIO / NO HABIDO' );
              setTimeout(function() {$('#modal-message').modal('hide');}, 2500);
            }
          } else {
            if( response.sMessageSQL !== undefined ) {
              console.log(response.sMessageSQL);
            }
            $( '#modal-message' ).modal('show');
            $( '.modal-message' ).addClass(response.sClassModal);
            $( '.modal-title-message' ).text(response.sMessage + ' ' + sNombreEntidad);
            setTimeout(function() {$('#modal-message').modal('hide');}, 2500);
          }
        }, 'json');// Obtener informacion de una entidad, para saber si esta HABIDO y SIN BAJA DE OFICIO
      }// /. Verificar si es un cliente existente o nuevo
    }
  }
}

function reload_table_venta(){
  table_venta.ajax.reload(null,false);
}

function _eliminarFacturaVenta($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario){
  $( '#modal-loader' ).modal('show');
  
  url = base_url + 'Ventas/VentaController/eliminarVenta/' + ID + '/' + Nu_Enlace + '/' + Nu_Descargar_Inventario;
  $.ajax({
    url       : url,
    type      : "GET",
    dataType  : "JSON",
    success: function( response ){
      $( '#modal-loader' ).modal('hide');
      $modal_delete.modal('hide');
      
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
  	    reload_table_venta();
		  } else {
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1500);
		  }
		  accion = '';
    },
    error: function (jqXHR, textStatus, errorThrown) {
      accion = '';
      $( '#modal-loader' ).modal('hide');
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function _anularFacturaVenta($modal_delete, ID, Nu_Enlace, Nu_Descargar_Inventario, iEstado, sTipoBajaSunat){
  $( '#modal-loader' ).modal('show');
    
  url = base_url + 'Ventas/VentaController/anularVenta/' + ID + '/' + Nu_Enlace + '/' + Nu_Descargar_Inventario + '/' + iEstado + '/' + sTipoBajaSunat;
  $.ajax({
    url       : url,
    type      : "GET",
    dataType  : "JSON",
    success: function( response ){
      $( '#modal-loader' ).modal('hide');
      
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
		  } else {
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
      }
      reload_table_venta();
		  accion = '';
    },
    error: function (jqXHR, textStatus, errorThrown) {
      accion = '';
      $( '#modal-loader' ).modal('hide');
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function sendFacturaVentaSunat(ID, Nu_Estado, sTipoBajaSunat){
  var $modal_delete = $( '.modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '.modal-message-delete' ).removeClass('modal-danger modal-warning modal-success');
  $( '.modal-message-delete' ).addClass('modal-success');
  
  $( '.modal-title-message-delete' ).text('¿Enviar a sunat?');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });

  $( '#btn-save-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
    
    $( '#btn-sunat-' + ID ).text('');
    $( '#btn-sunat-' + ID ).attr('disabled', true);
    $( '#btn-sunat-' + ID ).append( 'Enviando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      
    _sendFacturaVentaSunat(ID, Nu_Estado, 'json', sTipoBajaSunat);
  });
}

function _sendFacturaVentaSunat(ID, Nu_Estado, sTypeResponse, sTipoBajaSunat){
  url = base_url + 'Ventas/VentaController/sendFacturaVentaSunat/' + ID + '/' + Nu_Estado + '/' + sTypeResponse + '/' + sTipoBajaSunat;
  $.ajax({
    url       : url,
    type      : "GET",
    dataType  : "JSON",
    success: function( response ){
      $( '.modal-title-message' ).text( '' );
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    reload_table_venta();
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
		  } else {
  	    $( '.modal-message' ).addClass(response.style_modal);
        $( '.modal-title-message' ).text( (response.message_nubefact!=null ? response.message_nubefact : response.message) );
        console.log( response.arrMessagePSE );
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 4100);
  	    reload_table_venta();
      }
      
      $( '#btn-sunat-' + ID ).text('');
      $( '#btn-sunat-' + ID ).attr('disabled', false);
      $( '#btn-sunat-' + ID ).append( '<i class="fa fa-cloud-upload"></i> Sunat' );
    }
  });
}

function sendCorreoFacturaVentaSUNAT(ID, iIdCliente){
  var $modal_id = $( '#modal-correo_sunat' );
  $modal_id.modal('show');
  
  $( '#modal-correo_sunat' ).removeClass('modal-danger modal-warning modal-success');
  $( '#modal-correo_sunat' ).addClass('modal-success');
  
  $( '.modal-header_message_correo_sunat' ).text('¿Enviar correo?');
  
  // get cliente
	$( '#txt-email_correo_sunat' ).val( '' );
  url = base_url + 'HelperController/getDataGeneral';
  $.post( url, {sTipoData: 'get_entidad', iTipoEntidad: 0, iIDEntidad: iIdCliente}, function( response ){
    if ( response.sStatus == 'success' ) {
		  $( '#txt-email_correo_sunat' ).val( response.arrData[0].Txt_Email_Entidad );
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      console.log(response.sMessage);
    }
  }, 'JSON');
  // /. get cliente

	$( '#modal-correo_sunat' ).on('shown.bs.modal', function() {
		$( '#txt-email_correo_sunat' ).focus();
	})
  
  $( '#btn-modal_message_correo_sunat-cancel' ).off('click').click(function () {
    $modal_id.modal('hide');
  });

	$( "#txt-email_correo_sunat" ).blur(function() {
		caracteresCorreoValido($(this).val(), '#span-email');
  })
  
  $( '#btn-modal_message_correo_sunat-send' ).off('click').click(function () {
    if (!caracteresCorreoValido($('#txt-email_correo_sunat').val(), '#div-email') ) {
      scrollToError($('#modal-correo_sunat .modal-body'), $( '#txt-email_correo_sunat' ));
    } else {
      $( '#btn-modal_message_correo_sunat-send' ).text('');
      $( '#btn-modal_message_correo_sunat-send' ).attr('disabled', true);
      $( '#btn-modal_message_correo_sunat-send' ).append( 'Enviando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
        
      var sendPost = {
        ID : ID,
        Txt_Email : $( '#txt-email_correo_sunat' ).val(),
      };
      
      url = base_url + 'Ventas/VentaController/sendCorreoFacturaVentaSUNAT/';
      $.ajax({
        url       : url,
        type      : "POST",
        dataType  : "JSON",
        data      : sendPost,
        success: function( response ){
          $modal_id.modal('hide');
            
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
      	  $( '#modal-message' ).modal('show');
    		  
    		  if (response.status == 'success'){
    		    $( '#txt-email_correo_sunat' ).val( '' );
    		    
      	    $( '.modal-message' ).addClass(response.style_modal);
      	    $( '.modal-title-message' ).text(response.message);
      	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
    		  } else {
      	    $( '.modal-message' ).addClass(response.style_modal);
      	    $( '.modal-title-message' ).text(response.message);
      	    setTimeout(function() {$('#modal-message').modal('hide');}, 1800);
          }
          
          $( '#btn-modal_message_correo_sunat-send' ).text('');
          $( '#btn-modal_message_correo_sunat-send' ).attr('disabled', false);
          $( '#btn-modal_message_correo_sunat-send' ).append( 'Enviar' );
        }
      });
    }
  });
}

function validatePreviousDocumentToSaveSale(){
	var bEstadoValidacion = true;
	
	if ( $( '#cbo-TiposDocumento' ).val() == 0){
    $( '#cbo-TiposDocumento' ).closest('.form-group').find('.help-block').html('Seleccionar tipo');
    $( '#cbo-TiposDocumento' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#cbo-TiposDocumento' ) );
  } else if ( $( '#cbo-SeriesDocumento' ).val() == 0){
    $( '#cbo-SeriesDocumento' ).closest('.form-group').find('.help-block').html('Seleccionar serie');
    $( '#cbo-SeriesDocumento' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#cbo-SeriesDocumento' ) );
  } else if ($('#txt-ID_Numero_Documento').val().length === 0) {
    $('#txt-ID_Numero_Documento').closest('.form-group').find('.help-block').html('Ingresar número');
    $('#txt-ID_Numero_Documento').closest('.form-group').removeClass('has-success').addClass('has-error');

    bEstadoValidacion = false;
    scrollToError($("html, body"), $('#txt-ID_Numero_Documento'));
  } else if ( $('[name="addCliente"]:checked').attr('value') == 0 && ($( '#txt-AID' ).val().length === 0 || $( '#txt-ANombre' ).val().length === 0)) {
    $( '#txt-ANombre' ).closest('.form-group').find('.help-block').html('Seleccionar cliente');
		$( '#txt-ANombre' ).closest('.form-group').removeClass('has-success').addClass('has-error');
		
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#txt-ANombre' ) );
  } else if ( $( '#cbo-TiposDocumentoIdentidadCliente' ).val() != 1 && $( '#cbo-TiposDocumentoIdentidadCliente' ).val() != 2 && ($('[name="addCliente"]:checked').attr('value') == 1 && $( '#cbo-TiposDocumentoIdentidadCliente' ).find(':selected').data('nu_cantidad_caracteres') != $( '#txt-Nu_Documento_Identidad_Cliente').val().length) ) {
    $( '#txt-Nu_Documento_Identidad_Cliente' ).closest('.form-group').find('.help-block').html('Debe ingresar ' + $( '#cbo-TiposDocumentoIdentidadCliente' ).find(':selected').data('nu_cantidad_caracteres') + ' dígitos' );
	  $( '#txt-Nu_Documento_Identidad_Cliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	  
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#txt-Nu_Documento_Identidad_Cliente' ) );
  } else if ( $( '#cbo-almacen' ).val() == 0 ){
    $( '#cbo-almacen' ).closest('.form-group').find('.help-block').html('Seleccionar almacén');
    $( '#cbo-almacen' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#cbo-almacen' ) );
  } else if (nu_enlace == 1 && $( '#cbo-TiposDocumentoModificar' ).val() == 0){
    $( '#cbo-TiposDocumentoModificar' ).closest('.form-group').find('.help-block').html('Seleccionar tipo');
    $( '#cbo-TiposDocumentoModificar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#cbo-TiposDocumentoModificar' ) );
  } else if (nu_enlace == 1 && $( '#cbo-SeriesDocumentoModificar' ).val() == 0){
    $( '#cbo-SeriesDocumentoModificar' ).closest('.form-group').find('.help-block').html('Seleccionar serie');
    $( '#cbo-SeriesDocumentoModificar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#cbo-SeriesDocumentoModificar' ) );
  } else if (nu_enlace == 1 && $( '#txt-ID_Numero_Documento_Modificar' ).val().length === 0){
    $( '#txt-ID_Numero_Documento_Modificar' ).closest('.form-group').find('.help-block').html('Ingresar numero');
    $( '#txt-ID_Numero_Documento_Modificar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#txt-ID_Numero_Documento_Modificar' ) );
  } else if (nu_enlace == 1 && $( '#cbo-MotivoReferenciaModificar' ).val() == 0 && ($( '#cbo-SeriesDocumentoModificar' ).val().substr(0,1) == "B" || $( '#cbo-SeriesDocumentoModificar' ).val().substr(0,1) == "F") ){
    $( '#cbo-MotivoReferenciaModificar' ).closest('.form-group').find('.help-block').html('Seleccionar motivo');
    $( '#cbo-MotivoReferenciaModificar' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
    bEstadoValidacion = false;
		scrollToError( $("html, body"), $( '#cbo-MotivoReferenciaModificar' ) );
  } else if ( $('[name="addCliente"]:checked').attr('value') == 1 && $( '#cbo-Estado' ).val() == 0 ) {
    $( '#modal-message' ).modal('show');
    $( '.modal-message' ).addClass('modal-danger');
    $( '.modal-title-message' ).text( 'El cliente se encuentra con BAJA DE OFICIO / NO HABIDO' );
    setTimeout(function() {$('#modal-message').modal('hide');}, 2500);
		
    bEstadoValidacion = false;
  }
	return bEstadoValidacion;
}

function generarTablaTemporalItems($ID_Producto, $No_Producto, $Ss_Precio, $ID_Impuesto_Cruce_Documento, $Nu_Tipo_Impuesto, $Ss_Impuesto, $Qt_Producto, $iTipoItem, $iIdPresupuesto){
  var _ID_Producto = '', option_impuesto_producto = '', $Ss_SubTotal_Producto = 0.00, $Ss_Total_Producto = 0.00;

  var obj = JSON.parse(arrImpuestosProducto);
  for (var x = 0; x < obj.arrImpuesto.length; x++){
    var selected = '';
    if ($ID_Impuesto_Cruce_Documento == obj.arrImpuesto[x].ID_Impuesto_Cruce_Documento)
      selected = 'selected="selected"';
    option_impuesto_producto += "<option value='" + obj.arrImpuesto[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + obj.arrImpuesto[x].Nu_Tipo_Impuesto + "' data-impuesto_producto='" + obj.arrImpuesto[x].Ss_Impuesto + "' " + selected + ">" + obj.arrImpuesto[x].No_Impuesto + "</option>";
  }

  $Ss_Precio = isNaN($Ss_Precio) ? 0 : $Ss_Precio;

  $Ss_Total_Producto = $Ss_Precio;
  $Ss_SubTotal_Producto = parseFloat($Ss_Precio / $Ss_Impuesto);

  var table_detalle_producto =
  "<tr id='tr_detalle_producto" + $ID_Producto + "'>"
    +"<td style='display:none;' class='text-left td-iIdItem'>" + $ID_Producto + "</td>"
    +"<td class='text-right'><input type='text' id=" + $ID_Producto + " class='txt-Qt_Producto form-control input-decimal' " + ($iTipoItem == 1 ? 'onkeyup=validateStockNow(event);' : '') + " data-id_item='" + $ID_Producto + "' data-id_producto='" + $ID_Producto + "' value='1' autocomplete='off'></td>"
    +"<td class='text-left'>" + $No_Producto + "</td>"
    +"<td class='text-right'><input type='text' class='txt-fValorUnitario form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='" + $Ss_SubTotal_Producto.toFixed(2) + "' autocomplete='off'></td>"
    +"<td class='text-right'><input type='text' class='txt-Ss_Precio form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='" + $Ss_Precio + "' autocomplete='off'></td>"
    +"<td class='text-right'>"
      +"<select class='cbo-ImpuestosProducto form-control required' style='width: 100%;'>"
        +option_impuesto_producto
      +"</select>"
    +"</td>"
    +"<td style='display:none;' class='text-right'><input type='tel' class='txt-Ss_SubTotal_Producto form-control' value='" + $Ss_SubTotal_Producto.toFixed(2) + "' autocomplete='off' disabled></td>"
    +"<td class='text-right'><input type='text' class='txt-Ss_Descuento form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='' autocomplete='off'></td>"
    +"<td class='text-right'><input type='text' class='txt-Ss_Total_Producto form-control input-decimal' data-id_producto='" + $ID_Producto + "' value='" + $Ss_Total_Producto.toFixed(2) + "' autocomplete='off'></td>"
    +"<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>0.00</td>"
    +"<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>0.00</td>"
    +"<td style='display:none;' class='text-right td-iIdPresupuesto'>" + $iIdPresupuesto + "</td>"
    +"<td class='text-center'><button type='button' id='btn-deleteProducto' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-2x' aria-hidden='true'> </i></button></td>"
  +"</tr>";

  $( '#txt-ID_Producto' ).val('');
  $( '#txt-No_Producto' ).val('');
  $( '#txt-Ss_Precio' ).val('');
  $( '#txt-Qt_Producto' ).val('');
  $( '#table-DetalleProductosVenta' ).show();
  $( '#table-DetalleProductosVenta >tbody' ).append(table_detalle_producto);
  $( '#' + $ID_Producto ).focus();
  $( '#' + $ID_Producto ).select();

  var $Ss_Descuento = parseFloat($('#txt-Ss_Descuento').val());
  var $Ss_SubTotal = 0.00;
  var $Ss_Exonerada = 0.00;
  var $Ss_Inafecto = 0.00;
  var $Ss_Gratuita = 0.00;
  var $Ss_IGV = 0.00;
  var $Ss_Total = 0.00;
  var iCantDescuento = 0;
  var globalImpuesto = 0;
  var $Ss_Descuento_p = 0;
  $("#table-DetalleProductosVenta > tbody > tr").each(function(){
    var rows = $(this);
    var fImpuesto = parseFloat(rows.find('.cbo-ImpuestosProducto option:selected').data('impuesto_producto'));
    var iGrupoImpuesto = rows.find('.cbo-ImpuestosProducto option:selected').data('nu_tipo_impuesto');
    var $Ss_SubTotal_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val() / fImpuesto);
    var $Ss_Descuento_Producto = parseFloat(rows.find('.txt-Ss_Descuento', this).val());
    var $Ss_Total_Producto = parseFloat(rows.find('.txt-Ss_Total_Producto', this).val());

    $Ss_Total += $Ss_Total_Producto;

    if (iGrupoImpuesto == 1) {
      $Ss_SubTotal += $Ss_SubTotal_Producto;
      $Ss_IGV += $Ss_Total_Producto - $Ss_SubTotal_Producto;
      globalImpuesto = fImpuesto;
    } else if (iGrupoImpuesto == 2) {
      $Ss_Inafecto += $Ss_SubTotal_Producto;
      globalImpuesto += 0;
    } else if (iGrupoImpuesto == 3) {
      $Ss_Exonerada += $Ss_SubTotal_Producto;
      globalImpuesto += 0;
    } else {
      $Ss_Gratuita += $Ss_SubTotal_Producto;
      globalImpuesto += 0;
    }
    if(isNaN($Ss_Descuento_Producto))
      $Ss_Descuento_Producto = 0;
    $Ss_Descuento_p += (($Ss_Descuento_Producto * ((parseFloat(rows.find('.txt-Qt_Producto', this).val()) * parseFloat(rows.find('.txt-Ss_Precio', this).val()) / fImpuesto) )) / 100);
  });

  if ($Ss_Descuento > 0.00) {
    var $Ss_Descuento_Gravadas = 0, $Ss_Descuento_Inafecto = 0, $Ss_Descuento_Exonerada = 0, $Ss_Descuento_Gratuita = 0;
    if ($Ss_SubTotal > 0.00) {
      $Ss_Descuento_Gravadas = (($Ss_Descuento * $Ss_SubTotal) / 100);
      $Ss_SubTotal = $Ss_SubTotal - $Ss_Descuento_Gravadas;
      $Ss_SubTotal = Math.round10($Ss_SubTotal, -2);
      $Ss_IGV = ($Ss_SubTotal * globalImpuesto) - $Ss_SubTotal;
    }
    if ($Ss_Inafecto > 0.00) {
      $Ss_Descuento_Inafecto = (($Ss_Descuento * $Ss_Inafecto) / 100);
      $Ss_Inafecto = $Ss_Inafecto - $Ss_Descuento_Inafecto;
      $Ss_Inafecto = Math.round10($Ss_Inafecto, -2);
    }
    if ($Ss_Exonerada > 0.00) {
      $Ss_Descuento_Exonerada = (($Ss_Descuento * $Ss_Exonerada) / 100);
      $Ss_Exonerada = $Ss_Exonerada - $Ss_Descuento_Exonerada;
      $Ss_Exonerada = Math.round10($Ss_Exonerada, -2);
    }
    if ($Ss_Gratuita > 0.00) {
      $Ss_Descuento_Gratuita = (($Ss_Descuento * $Ss_Gratuita) / 100);
      $Ss_Gratuita = $Ss_Gratuita - $Ss_Descuento_Gratuita;
      $Ss_Gratuita = Math.round10($Ss_Gratuita, -2);
    }
    $Ss_Total = ($Ss_SubTotal * globalImpuesto) + $Ss_Inafecto + $Ss_Exonerada + $Ss_Gratuita;
    $Ss_Descuento = $Ss_Descuento_Gravadas + $Ss_Descuento_Inafecto + $Ss_Descuento_Exonerada + $Ss_Descuento_Gratuita;
  } else {
    $Ss_Descuento = $Ss_Descuento_p;
  }

  if(isNaN($Ss_Descuento)) {
    $Ss_Descuento = 0.00;
  }

  $('#txt-subTotal').val( $Ss_SubTotal.toFixed(2) );
  $('#span-subTotal').text( $Ss_SubTotal.toFixed(2) );
  $('#txt-exonerada').val( $Ss_Exonerada.toFixed(2) );
  $('#span-exonerada').text( $Ss_Exonerada.toFixed(2) );
  $('#txt-inafecto').val( $Ss_Inafecto.toFixed(2) );
  $('#span-inafecto').text( $Ss_Inafecto.toFixed(2) );
  $('#txt-gratuita').val( $Ss_Gratuita.toFixed(2) );
  $('#span-gratuita').text( $Ss_Gratuita.toFixed(2) );
  $('#txt-impuesto').val( $Ss_IGV.toFixed(2) );
  $('#span-impuesto').text( $Ss_IGV.toFixed(2) );
  $('#txt-descuento').val( $Ss_Descuento.toFixed(2) );
  $('#span-descuento').text( $Ss_Descuento.toFixed(2) );
  $('#txt-total').val( $Ss_Total.toFixed(2) );
  $('#span-total').text( $Ss_Total.toFixed(2) );

  validateDecimal();
  validateNumber();
  validateNumberOperation();
}

function verRepresentacionInternaPDF(ID){
  var $modal_delete = $( '#modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '.modal-message-delete' ).removeClass('modal-danger modal-warning modal-success');
  $( '.modal-message-delete' ).addClass('modal-success');
  
  $( '.modal-title-message-delete' ).text('¿Deseas generar PDF?');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });
  
  $( '#btn-save-delete' ).off('click').click(function () {
    sendPDF($modal_delete, ID);
  });
}

function sendPDF($modal_delete, ID){
  $( '#modal-loader' ).modal('show');
  $modal_delete.modal('hide');
  url = base_url + 'Ventas/VentaController/generarRepresentacionInternaPDF/' + ID;
  window.open(url,'_blank');
  $( '#modal-loader' ).modal('hide');
}


function crearItem(){
  $( '#modal-loader' ).modal('show');
  var $modal_crearItem = $( '#modal-crearItem' );
  $modal_crearItem.modal('show');

	$( '.help-block' ).empty();
	
  $( ".div-Producto" ).hide();
  $( "div.div-modal-grupoItem select" ).val("0");

  //Limpiar datos
  $( '#txt-modal-upcItem' ).val( '' );
  $( '#hidden-ID_Tabla_Dato' ).val( '' );
  $( '#txt-No_Descripcion' ).val( '' );
  $( '#txt-modal-precioItem' ).val( '' );
  $('#txt-ID_Presupesto_Existe-modal').val('');
  $('[name="textarea-modal-nombreItem"]').val('');
  $('[name="textarea-modal-nombreItem"]').val('');
  $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val('');
  $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val('');
  
  /* Cargar datos para modal de item */
  url = base_url + 'HelperController/getTiposExistenciaProducto';
  $.post( url , function( responseTiposExistenciaProducto ){
    $( '#cbo-modal-tipoItem' ).html( '' );
    for (var i = 0; i < responseTiposExistenciaProducto.length; i++)
      $( '#cbo-modal-tipoItem' ).append( '<option value="' + responseTiposExistenciaProducto[i].ID_Tipo_Producto + '">' + responseTiposExistenciaProducto[i].No_Tipo_Producto + '</option>' );
  }, 'JSON');
  
  url = base_url + 'HelperController/getImpuestos';
  $.post( url , function( response ){
    $( '#cbo-modal-impuestoItem' ).html( '' );
    for (var i = 0; i < response.length; i++)
      $( '#cbo-modal-impuestoItem' ).append( '<option value="' + response[i].ID_Impuesto + '" data-ss_impuesto="' + response[i].Ss_Impuesto + '" data-nu_tipo_impuesto="' + response[i].Nu_Tipo_Impuesto + '">' + response[i].No_Impuesto + '</option>' );
  }, 'JSON');
  
  url = base_url + 'HelperController/getUnidadesMedida';
  $.post( url , function( responseUnidadMedidas ){
    $( '#cbo-modal-unidad_medidaItem' ).html( '' );
    for (var i = 0; i < responseUnidadMedidas.length; i++)
      $( '#cbo-modal-unidad_medidaItem' ).append( '<option value="' + responseUnidadMedidas[i].ID_Unidad_Medida + '">' + responseUnidadMedidas[i].No_Unidad_Medida + '</option>' );
    $( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
  $( '#modal-header-crearItem' ).text('Crear Item');
  
  $( '#btn-modal-salir' ).off('click').click(function () {
    $modal_crearItem.modal('hide');
  });
  
  $( '#btn-modal-crearItem' ).off('click').click(function () {
    var fPrecio = parseFloat($('#txt-modal-precioItem').val());
    $('.help-block').empty();
    if (fPrecio == 0.00 || isNaN(fPrecio) ){
      $('#txt-modal-precioItem').closest('.form-group').find('.help-block').html('Ingresar precio');
      $('#txt-modal-precioItem').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('#txt-modal-upcItem').val().length === 0) {
      $('#txt-modal-upcItem').closest('.form-group').find('.help-block').html('Ingresar codigo');
      $('#txt-modal-upcItem').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else if ($('[name="textarea-modal-nombreItem"]').val().length === 0){
      $('[name="textarea-modal-nombreItem"]').closest('.form-group').find('.help-block').html('Ingresar datos');
      $('[name="textarea-modal-nombreItem"]').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      if ($('[name="arrIdPresupuesto"]').val() !== undefined) {
        if ($('[name="arrIdPresupuesto"]').val().length == 0 )
          $('[name="arrIdPresupuesto"]').val($('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val());
        else {
          var arrIdPresupuestoAutocompleteProducto = $('[name="arrIdPresupuesto"]').val();
          arrIdPresupuestoAutocompleteProducto += $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val();
          $('[name="arrIdPresupuesto"]').val(arrIdPresupuestoAutocompleteProducto);
        }
      }
      if ($('[name="Txt_Glosa"]').val() !== undefined) {
        if ($('[name="Txt_Glosa"]').val().length == 0)
          $('[name="Txt_Glosa"]').val($('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val());
        else {
          var sGlosa = $('[name="Txt_Glosa"]').val();
          sGlosa += $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val();
          $('[name="Txt_Glosa"]').val(sGlosa);
        }
      }

      $('[name="arrIdPresupuesto_Prespuesto_Autocomplete"]').val('');
      $('[name="Txt_Glosa_Prespuesto_Autocomplete"]').val('');

      $( '.help-block' ).empty();

  		var iIDTipoExistenciaProducto = $( '#cbo-modal-tipoItem' ).val();
      if ( $( '#cbo-modal-grupoItem' ).val() == '0' || $( '#cbo-modal-grupoItem' ).val() == '2' )//Servicio o interno
        iIDTipoExistenciaProducto = 4;//Otros

      var arrProducto = Array();
  		arrProducto = {
  		  'EID_Empresa'               : $('#autocompletar_presupuesto').data('id_empresa'),
        // FIX - reg. prod fv - Inicio 
  		  //'EID_Producto'              : $('#autocompletar_presupuesto').data('id_producto'),
  		  'EID_Producto'              : '',
        // FIX - ref. prod fv - fin
  		  'ENu_Codigo_Barra'          : $( '#txt-modal-upcItem' ).val(),
  		  'Nu_Tipo_Producto'          : $( '#cbo-modal-grupoItem' ).val(),
  		  'ID_Tipo_Producto'          : iIDTipoExistenciaProducto,
  		  'ID_Ubicacion_Inventario'   : 1,
  			'Nu_Codigo_Barra'           : $( '#txt-modal-upcItem' ).val(),
  			'No_Producto'               : $( '[name="textarea-modal-nombreItem"]' ).val(),
  			'No_Codigo_Interno'         : '',
  			'ID_Impuesto'               : $( '#cbo-modal-impuestoItem' ).val(),
  			'ID_Unidad_Medida'          : $( '#cbo-modal-unidad_medidaItem' ).val(),
  			'ID_Marca'                  : '',
  			'ID_Familia'                : '',
  			'ID_Sub_Familia'            : '',
  			'Nu_Compuesto'              : 0,
  			'Nu_Estado'                 : 0,//1 estaba así hasta el día 31/03/2022
  			'Txt_Producto'              : '',
        'ID_Producto_Sunat'         : $( '#hidden-ID_Tabla_Dato' ).val(),
  			'Nu_Stock_Minimo'           : '',
  			'Nu_Receta_Medica'          : '',
  			'ID_Laboratorio'            : '',
  			'Nu_Lote_Vencimiento'       : '',
  			'Txt_Ubicacion_Producto_Tienda' : '',
        'Ss_Precio' : 0,
        'Ss_Costo' : 0,
        'ID_Impuesto_Icbper' : 0,
        'Qt_CO2_Producto' : 0,
        'ID_Tipo_Pedido_Lavado' : 0,
        'Ss_Precio' : $( '#txt-modal-precioItem' ).val(),
        'Ss_Costo': 0,
        'No_Imagen_Item': '',
        'Ss_Precio_Ecommerce_Online_Regular': 0,
        'Ss_Precio_Ecommerce_Online': 0,
        'ID_Familia_Marketplace': 0,
        'ID_Sub_Familia_Marketplace': 0,
        'ID_Marca_Marketplace': 0,
        'No_Modelo_Vehiculo': '',
        'No_Color_Vehiculo': '',
        'No_Motor': '',
  		};

      $('#btn-modal-salir').attr('disabled', true);
      $('#btn-modal-crearItem').text('');
      $('#btn-modal-crearItem').attr('disabled', true);
      $('#btn-modal-crearItem').append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');
      $('#modal-loader').modal('show');
      $('#modal-loader').css("z-index", "5000");

      url = base_url + 'Logistica/ReglasLogistica/ProductoController/crudProducto'; 
    	$.ajax({
        type		  : 'POST',
        dataType	: 'JSON',
    		url		    : url,
    		data		  : {
    		  arrProducto : arrProducto,
    		  arrProductoEnlace : null,
    		},
    		success : function(response){
          console.log(response);
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
      	  $( '#modal-message' ).modal('show');
      	  $( '.modal-message' ).css("z-index", "5000");
    		  if (response.status == 'success'){
		        if ( (!isNaN(fPrecio) && fPrecio > 0.00) && $( '#cbo-lista_precios' ).val() != 0 ){
              $('.modal-message').addClass(response.style_modal);
              $('.modal-title-message').text(response.message);
              setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
              addItemDetail(response.iIDItem, $('[name="textarea-modal-nombreItem"]').val(), fPrecio, $('#cbo-modal-impuestoItem').val(), $('#cbo-modal-impuestoItem').find(':selected').data('nu_tipo_impuesto'), $('#cbo-modal-impuestoItem').find(':selected').data('ss_impuesto'), 0, $('#cbo-modal-grupoItem').val(), $('#txt-ID_Presupesto_Existe-modal').val());
        	    $modal_crearItem.modal('hide');
        	    // Creando precio
              // url = base_url + 'Ventas/ReglasVenta/Lista_precio_controller/crudLista_Precio_Producto';
            	// $.ajax({
              //   type		  : 'POST',
              //   dataType	: 'JSON',
            	// 	url		    : url,
            	// 	data		  : {
            	// 		'ID_Lista_Precio_Cabecera' : $( '#cbo-lista_precios' ).val(),
            	// 		'ID_Producto' : response.iIDItem,
            	// 		'Ss_Precio_Interno' : 0,
            	// 		'Po_Descuento' : 0,
            	// 		'Ss_Precio' : $( '#txt-modal-precioItem' ).val(),
            	// 		'Nu_Estado' : 1,
            	// 	},
            	// 	success : function( responsePrecio ){
              //     $( '#modal-loader' ).modal('hide');
              //     $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
              //     $( '#modal-message' ).modal('show');
              //     $( '.modal-message' ).css("z-index", "5000");

        		  //     if (responsePrecio.status == 'success'){
              //       $( '.modal-message' ).addClass(responsePrecio.style_modal);
              //       $( '.modal-title-message' ).text(responsePrecio.message);
              //       setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
              //       addItemDetail(response.iIDItem, $('[name="textarea-modal-nombreItem"]').val(), fPrecio, $('#cbo-modal-impuestoItem').val(), $('#cbo-modal-impuestoItem').find(':selected').data('nu_tipo_impuesto'), $('#cbo-modal-impuestoItem').find(':selected').data('ss_impuesto'), 0, $('#cbo-modal-grupoItem').val(), $('#txt-ID_Presupesto_Existe-modal').val());
              // 	    $modal_crearItem.modal('hide');
              // 		} else {
              // 	    $( '.modal-message' ).addClass(responsePrecio.style_modal);
              // 	    $( '.modal-title-message' ).text(responsePrecio.message);
              // 	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
            	// 	  }
              //     $( '#btn-modal-salir' ).attr('disabled', false);
              //     $( '#btn-modal-crearItem' ).text('');
              //     $( '#btn-modal-crearItem' ).append( '<span class="fa fa-save"></span> Guardar' );
              //     $( '#btn-modal-crearItem' ).attr('disabled', false);
            	// 	},
              //   error: function (jqXHR, textStatus, errorThrown) {
              //     $( '#modal-loader' ).modal('hide');
            	//     $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
              // 	  $( '#modal-message' ).modal('show');
            	//     $( '.modal-message' ).addClass( 'modal-danger' );
            	//     $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
            	//     setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
            	//     //Message for developer
              //     console.log(jqXHR.responseText);
              //     $( '#btn-modal-salir' ).attr('disabled', false);
              //     $( '#btn-modal-crearItem' ).text('');
              //     $( '#btn-modal-crearItem' ).append( '<span class="fa fa-save"></span> Guardar' );
              //     $( '#btn-modal-crearItem' ).attr('disabled', false);
              //   }
            	// })
            	// Creando precio
		        } else {
              $( '.modal-message' ).addClass(response.style_modal);
              $( '.modal-title-message' ).text(response.message);
              setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
              addItemDetail(response.iIDItem, $('[name="textarea-modal-nombreItem"]').val(), fPrecio, $('#cbo-modal-impuestoItem').val(), $('#cbo-modal-impuestoItem').find(':selected').data('nu_tipo_impuesto'), $('#cbo-modal-impuestoItem').find(':selected').data('ss_impuesto'), 0, $('#cbo-modal-grupoItem').val(), $('#txt-ID_Presupesto_Existe-modal').val());
        	    $modal_crearItem.modal('hide');
		        }
            $( '#modal-loader' ).modal('hide');
    		  } else {
      	    $( '.modal-message' ).addClass(response.style_modal);
      	    $( '.modal-title-message' ).text(response.message);
      	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
    		  }
          $( '#modal-loader' ).modal('hide');
          $( '#btn-modal-salir' ).attr('disabled', false);
          $( '#btn-modal-crearItem' ).text('');
          $( '#btn-modal-crearItem' ).append( '<span class="fa fa-save"></span> Guardar' );
          $( '#btn-modal-crearItem' ).attr('disabled', false);
    		},
        error: function (jqXHR, textStatus, errorThrown) {
          $( '#modal-loader' ).modal('hide');
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
    	    
      	  $( '#modal-message' ).modal('show');
    	    $( '.modal-message' ).addClass( 'modal-danger' );
    	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
    	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
    	    
    	    //Message for developer
          console.log(jqXHR.responseText);
          
          $( '#btn-modal-salir' ).attr('disabled', false);
    		  
          $( '#btn-modal-crearItem' ).text('');
          $( '#btn-modal-crearItem' ).append( '<span class="fa fa-save"></span> Guardar' );
          $( '#btn-modal-crearItem' ).attr('disabled', false);
        }
    	});   
    }
  });
}

function addItemDetail($ID_Producto, $No_Producto, $Ss_Precio, $ID_Impuesto_Cruce_Documento, $Nu_Tipo_Impuesto, $Ss_Impuesto, $Qt_Producto, $iTipoItem, $iIdPresupuesto){
  var bEstadoValidacion=false;
  bEstadoValidacion = validatePreviousDocumentToSaveSale();
  if ( bEstadoValidacion == true && $ID_Producto.length === 0 || $No_Producto.length === 0 ) {
    $( '#txt-No_Producto' ).closest('.form-group').find('.help-block').html('Ingresar producto');
		$( '#txt-No_Producto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
  } else if (bEstadoValidacion) {
    if (iValidarStockGlobal == 1 && $iTipoItem == 1 && (parseFloat($Qt_Producto) <= 0.000000 || isNaN($Qt_Producto)==true) ) {
      $modal_msg_stock = $( '.modal-message' );
      $modal_msg_stock.modal('show');
  
      $modal_msg_stock.removeClass('modal-danger modal-warning modal-success');
      $modal_msg_stock.addClass('modal-warning');
  
      $( '.modal-title-message' ).text('Sin stock disponible');
  
      setTimeout(function() {$modal_msg_stock.modal('hide');}, 1300);
    } else {
      return generarTablaTemporalItems($ID_Producto, $No_Producto, $Ss_Precio, $ID_Impuesto_Cruce_Documento, $Nu_Tipo_Impuesto, $Ss_Impuesto, $Qt_Producto, $iTipoItem, $iIdPresupuesto);
    }
  }// /. Validaciones previas
}

function saveFacturaVenta(arrDetalleVenta){ 

// C-01 - INICIO
for (var i = 0; i < arrDetalleVenta.length; i++)
{ 
  arrDetalleVenta [i].ID_Documento_Cabecera=$( '#txt-EID_Documento_Cabecera' ).val();
 //alert('leer '+arrDetalleVenta [i].ID_Producto +'/'+ arrDetalleVenta [i].ID_Documento_Detalle
 //+'/'+ arrDetalleVenta [i].ID_Documento_Cabecera);
}
// C-01 - FIN 


  $( '.div-mensaje_verificarExisteDocumento' ).text('');
  $( '#panel-DetalleProductosVenta' ).removeClass('panel-danger');
  $( '#panel-DetalleProductosVenta' ).addClass('panel-default');
  nu_enlace = $('#cbo-TiposDocumento').find(':selected').data('nu_enlace');
  var arrVentaCabecera = Array();
  arrVentaCabecera = {
    'delete_ID_Documento_Detalle': detalleItemsFacturaDelete.join(','),
    'esEnlace'                : nu_enlace,
    'EID_Empresa'             : $( '#txt-EID_Empresa' ).val(),
    'EID_Documento_Cabecera'  : $( '#txt-EID_Documento_Cabecera' ).val(),
    'ID_Entidad'              : $( '#txt-AID' ).val(),
    'ID_Tipo_Documento'       : $( '#cbo-TiposDocumento' ).val(),
    'ID_Serie_Documento_PK'   : $( '#cbo-SeriesDocumento' ).find(':selected').data('id_serie_documento_pk'),
    'ID_Serie_Documento'      : $( '#cbo-SeriesDocumento' ).val(),
    'ID_Numero_Documento'     : $( '#txt-ID_Numero_Documento' ).val(),
    'Fe_Emision'              : $( '#txt-Fe_Emision' ).val(),
    'ID_Moneda'               : $( '#cbo-Monedas' ).val(),
    'ID_Medio_Pago'           : $( '#cbo-MediosPago' ).val(),
    'Fe_Vencimiento'          : $( '#txt-Fe_Vencimiento' ).val(),
    'Nu_Descargar_Inventario' : 0,
    'Txt_Glosa'               : $( '[name="Txt_Glosa"]' ).val(),
    'Nu_Detraccion'           : $( '[name="radio-addDetraccion"]:checked' ).attr('value'),
    'Nu_Retencion': $('[name="radio-addRetencion"]:checked').attr('value'),
    'Po_Descuento'            : $( '#txt-Ss_Descuento' ).val(),
    'Ss_Descuento'            : $( '#txt-descuento' ).val(),
    'Ss_Total'                : $( '#txt-total' ).val(),
    'ID_Lista_Precio_Cabecera' : $( '#cbo-lista_precios' ).val(),
    'No_Formato_PDF'           : $( '#cbo-formato_pdf' ).val(),
    'Txt_Garantia' : $( '[name="Txt_Garantia"]' ).val(),
    'ID_Mesero' : $( '#cbo-vendedor' ).val(),
    'ID_Comision' : $( '#cbo-porcentaje' ).val(),
    'Nu_Descargar_Inventario' : $( '#cbo-descargar_stock' ).val(),
    'ID_Almacen' : $( '#cbo-almacen' ).val(),
    'iTipoFormaPago' : $( '#cbo-MediosPago' ).find(':selected').data('nu_tipo'),
    'ID_Documento_Cabecera_Orden' : $( '[name="ID_Documento_Cabecera_Orden"]' ).val(),
    'iTipoCliente': $('[name="addCliente"]:checked').attr('value'),
    'arrIdPresupuesto': $('[name="arrIdPresupuesto"]').val(),
    'ID_Tipo_Medio_Pago': $('#cbo-tarjeta_credito').val(),
    'Nu_Presupuesto': $('[name="Nu_Presupuesto"]').val(),
    'Nu_Orden_Compra': $('[name="Nu_Orden_Compra"]').val(),
    'Fe_Entrega_Factura': $('[name="Fe_Entrega_Factura"]').val(),
    'Fe_Entrega_Vencimiento_Factura': $('[name="Fe_Entrega_Vencimiento_Factura"]').val(),
    'ID_Servicio_Presupuesto_Negocio': $('#cbo-tipos_servicios').val(),
  };

  detalleItemsFacturaDelete = [];

  var arrVentaModificar = Array();
  if (nu_enlace == 1) {
    arrVentaModificar = {
      ID_Documento_Guardado : $( '#txt-ID_Documento_Guardado' ).val(),
      ID_Tipo_Documento_Modificar : $( '#cbo-TiposDocumentoModificar' ).val(),
      ID_Serie_Documento_Modificar : $( '#cbo-SeriesDocumentoModificar' ).val(),
      ID_Numero_Documento_Modificar : $( '#txt-ID_Numero_Documento_Modificar' ).val(),
      Nu_Codigo_Motivo_Referencia : $( '#cbo-MotivoReferenciaModificar' ).val(),
      iIdEntidad : $( '#txt-AID' ).val(),
      iTipoCliente : $('[name="addCliente"]:checked').attr('value'),
    };
  }

  var No_Cliente_Filter=$('#txt-ANombre').val(), arrClienteNuevo = {};
  if ($('[name="addCliente"]:checked').attr('value') == 1){//Agregar cliente
    arrClienteNuevo = {
      'ID_Tipo_Documento_Identidad' : $( '#cbo-TiposDocumentoIdentidadCliente' ).val(),
      'Nu_Documento_Identidad'      : $( '#txt-Nu_Documento_Identidad_Cliente' ).val(),
      'No_Entidad'                  : $( '#txt-No_Entidad_Cliente' ).val(),
      'Txt_Direccion_Entidad'       : $( '#txt-Txt_Direccion_Entidad_Cliente' ).val(),
      'Nu_Telefono_Entidad'         : $( '#txt-Nu_Telefono_Entidad_Cliente' ).val(),
      'Nu_Celular_Entidad'          : $( '#txt-Nu_Celular_Entidad_Cliente' ).val(),
      'Nu_Estado'                   : $( '#cbo-Estado' ).val(),
    };
    No_Cliente_Filter=$('#txt-No_Entidad_Cliente').val();
  }

  $( '#btn-save' ).text('');
  $( '#btn-save' ).attr('disabled', true);
  $( '#btn-save' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  $( '#modal-loader' ).modal('show');

  url = base_url + 'Ventas/VentaController/crudVenta';
  $.ajax({
    type		  : 'POST',
    dataType	: 'JSON',
    url		    : url,
    data		  : {
      arrVentaCabecera : arrVentaCabecera,
      arrDetalleVenta : arrDetalleVenta,
      arrVentaModificar : arrVentaModificar,
      arrClienteNuevo : arrClienteNuevo,
    },
    success: function(response) {
      $('#modal-loader').modal('hide');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');
      $('#modal-message').modal('show');
      $('.div-DocumentoModificar').removeClass('panel-warning panel-danger panel-success');
      $('.div-mensaje_verificarExisteDocumento').removeClass('text-danger text-success');
      $('.div-mensaje_verificarExisteDocumento').text('');

      if (response.status == 'success'){
        accion = '';
        No_Cliente_Filter = ( No_Cliente_Filter !='' ? No_Cliente_Filter : 'clientes varios' );

        $('#txt-Filtro_Entidad').val( No_Cliente_Filter );
        if ( response.sEnviarSunatAutomatic=='No' ) {
          $( '.div-AgregarEditar' ).hide();
          $( '.div-Listar' ).show();
          $( '.modal-message' ).addClass(response.style_modal);
          $( '.modal-title-message' ).text(response.message);
          setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
          
          $( '#form-Venta' )[0].reset();
        } else {
          if (response.status == 'success'){
            $( '.div-AgregarEditar' ).hide();
            $( '.div-Listar' ).show();
            $( '.modal-message' ).addClass(response.style_modal);
            $( '.modal-title-message' ).text(response.message);
            reload_table_venta();
            setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
            
            $( '#form-Venta' )[0].reset();
          } else {
            $( '.modal-message' ).addClass(response.style_modal);
            $( '.modal-title-message' ).text(response.message_nubefact);
            setTimeout(function() {$('#modal-message').modal('hide');}, 4000);
          }
        }
        reload_table_venta();
      } else {
        if ( nu_enlace == 1 ){//Para notas de crédito y débito
          $( '.div-DocumentoModificar' ).addClass(response.style_panel);
          $( '.div-mensaje_verificarExisteDocumento' ).addClass(response.style_p);
          $( '.div-mensaje_verificarExisteDocumento' ).text(response.message);
        }
        
        $( '.modal-message' ).addClass(response.style_modal);
        $( '.modal-title-message' ).text(response.message);
        setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
      }
      
      $( '#btn-save' ).text('');
      $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar (ENTER)' );
      $( '#btn-save' ).attr('disabled', false);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $( '#modal-loader' ).modal('hide');
      $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
      
      $( '#modal-message' ).modal('show');
      $( '.modal-message' ).addClass( 'modal-danger' );
      $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
      setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
      
      //Message for developer
      console.log(jqXHR.responseText);
      
      $( '#btn-save' ).text('');
      $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar (ENTER)' );
      $( '#btn-save' ).attr('disabled', false);
    }
  });
}

// Ayudas - combobox
function getAlmacenes(arrParams){
  url = base_url + 'HelperController/getAlmacenes';
  $.post( url, {}, function( responseAlmacen ){
    var iCantidadRegistros = responseAlmacen.length;
    var selected = '';
    var iIdAlmacen = 0;
    if (iCantidadRegistros == 1) {
      if (arrParams !== undefined) {
        iIdAlmacen = arrParams.ID_Almacen;
      }
      if (iIdAlmacen == responseAlmacen[0]['ID_Almacen']){
        selected = 'selected="selected"';
      }
      $( '#cbo-almacen' ).html( '<option value="' + responseAlmacen[0]['ID_Almacen'] + '" ' + selected + '>' + responseAlmacen[0]['No_Almacen'] + '</option>' );
      var arrParamsListaPrecio = {
        ID_Almacen : responseAlmacen[0]['ID_Almacen'],
      }
      getListaPrecios(arrParamsListaPrecio);
    } else {
      $( '#cbo-almacen' ).html( '<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < iCantidadRegistros; i++) {
        selected = '';
        if (arrParams !== undefined) {
          iIdAlmacen = arrParams.ID_Almacen;
        }
        
        if (iIdAlmacen == responseAlmacen[i]['ID_Almacen']){
          selected = 'selected="selected"';
        }
        $( '#cbo-almacen' ).append( '<option value="' + responseAlmacen[i]['ID_Almacen'] + '" ' + selected + '>' + responseAlmacen[i]['No_Almacen'] + '</option>' );
      }
    }
  }, 'JSON');
}

function getListaPrecios(arrParams){
  url = base_url + 'HelperController/getListaPrecio';
  var arrPost = {
    Nu_Tipo_Lista_Precio : $( '[name="Nu_Tipo_Lista_Precio"]' ).val(),
    ID_Organizacion: $( '#header-a-id_organizacion' ).val(),
    ID_Almacen : arrParams.ID_Almacen,
  }
  $.post( url, arrPost, function( responseListaPrecio ){
    var iCantidadRegistrosListaPrecios = responseListaPrecio.length;
    if ( iCantidadRegistrosListaPrecios == 1 ) {
      $( '#cbo-lista_precios' ).html( '<option value="' + responseListaPrecio[0].ID_Lista_Precio_Cabecera + '">' + responseListaPrecio[0].No_Lista_Precio + '</option>' );
    } else if ( iCantidadRegistrosListaPrecios > 1 ) {
      $( '#cbo-lista_precios' ).html( '<option value="0">- Seleccionar -</option>');
      for (var i = 0; i < iCantidadRegistrosListaPrecios; i++)
        $( '#cbo-lista_precios' ).append( '<option value="' + responseListaPrecio[i].ID_Lista_Precio_Cabecera + '">' + responseListaPrecio[i].No_Lista_Precio + '</option>' );
    } else {
      $( '#cbo-lista_precios' ).html( '<option value="0">- Sin lista precio -</option>');
    }
  }, 'JSON');
}

function cobrarCliente(ID_Documento_Cabecera, Ss_Total_Saldo, No_Entidad, No_Tipo_Documento_Breve, ID_Serie_Documento, ID_Numero_Documento, sSignoMoneda){
  arrParams = {
    'iIdDocumentoCabecera' : ID_Documento_Cabecera,
    'fTotalSaldo' : Ss_Total_Saldo,
    'sCliente' : No_Entidad,
    'sTipoDocumento' : No_Tipo_Documento_Breve,
    'sSerieDocumento' : ID_Serie_Documento,
    'sNumeroDocumento' : ID_Numero_Documento,
    'sSignoMoneda' : sSignoMoneda,
  }

  $( '#form-cobrar_cliente' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();

  $( '.div-forma_pago').hide();
  $( '.div-modal_datos_tarjeta_credito').hide();
  $( '.div-estado_lavado_recepcion_cliente' ).hide();
  $( '.div-recibe_otra_persona' ).hide();

  $( '.modal-cobrar_cliente' ).modal('show');

  $( '[name="iIdDocumentoCabecera"]' ).val( arrParams.iIdDocumentoCabecera );

  $( '#hidden-cobrar_cliente-fsaldo' ).val( arrParams.fTotalSaldo );

  $( '#modal-header-cobrar_cliente-title' ).text(arrParams.sTipoDocumento + ' - ' + arrParams.sSerieDocumento + ' - ' + arrParams.sNumeroDocumento);  
  $( '#cobrar_cliente-modal-body-cliente' ).text('Cliente: ' + arrParams.sCliente);
  $( '#cobrar_cliente-modal-body-saldo_cliente' ).text('Saldo: ' + sSignoMoneda + ' ' + arrParams.fTotalSaldo);

  $( '.div-forma_pago').show();
  $( '.modal-cobrar_cliente' ).on('shown.bs.modal', function() {
    $( '[name="fPagoCliente"]' ).focus();
    $( '[name="fPagoCliente"]' ).val( arrParams.fTotalSaldo );
  });

  url = base_url + 'HelperController/getMediosPago';
  var arrPost = {
    iIdEmpresa : arrParams.iIdEmpresa,
  };
  $.post( url, arrPost, function( response ){
    $( '#cbo-modal_forma_pago' ).html('');
    for (var i = 0; i < response.length; i++) {
      if ( response[i].Nu_Tipo != 1 )
        $( '#cbo-modal_forma_pago' ).append( '<option value="' + response[i].ID_Medio_Pago + '" data-nu_tipo_medio_pago="' + response[i].Nu_Tipo + '">' + response[i].No_Medio_Pago + '</option>' );
    }
  }, 'JSON');
  
  // Modal de cobranza al cliente
  $( '#cbo-modal_forma_pago' ).change(function(){
    ID_Medio_Pago = $(this).val();
    Nu_Tipo_Medio_Pago = $(this).find(':selected').data('nu_tipo_medio_pago');
    $( '.div-modal_datos_tarjeta_credito').hide();
    $( '#cbo-cobrar_cliente-modal_tarjeta_credito' ).html('');
    $( '#tel-nu_referencia' ).val('');
    $( '#tel-nu_ultimo_4_digitos_tarjeta' ).val('');
    if (Nu_Tipo_Medio_Pago==2){
      $( '.div-modal_datos_tarjeta_credito').show();

      url = base_url + 'HelperController/getTiposTarjetaCredito';
      $.post( url, {ID_Medio_Pago : ID_Medio_Pago} , function( response ){
        $( '#cbo-cobrar_cliente-modal_tarjeta_credito' ).html('');
        for (var i = 0; i < response.length; i++)
          $( '#cbo-cobrar_cliente-modal_tarjeta_credito' ).append( '<option value="' + response[i].ID_Tipo_Medio_Pago + '">' + response[i].No_Tipo_Medio_Pago + '</option>' );
      }, 'JSON');
    }

    setTimeout(function(){ $( '[name="fPagoCliente"]' ).focus(); $( '[name="fPagoCliente"]' ).select(); }, 20);		
  })

  $( '#cbo-estado_lavado_recepcion_cliente' ).change(function(){
    $( '.div-estado_lavado_recepcion_cliente' ).hide();
    if($(this).val() == 3) {
      $( '.div-estado_lavado_recepcion_cliente' ).show();
    }
  })

  $( '#cbo-modal_quien_recibe' ).change(function(){
    $( '.div-recibe_otra_persona' ).hide();
    if($(this).val() == 0) {
      $( '.div-recibe_otra_persona' ).show();
      $( '[name="sNombreRecepcion"]' ).focus();
    }
  })
}

function obtenerEstadoPSE(ID) {
  $('#btn-consultar_pse-' + ID).text('');
  $('#btn-consultar_pse-' + ID).attr('disabled', true);
  $('#btn-consultar_pse-' + ID).append('Consultando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

  url = base_url + 'Ventas/VentaController/obtenerEstadoPSE/' + ID;
  $.ajax({
    url: url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      console.log(response);
      $('.modal-title-message').text('');
      $('.modal-message').removeClass('modal-danger modal-warning modal-success');
      $('#modal-message').modal('show');

      if (response.status == 'success') {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text(response.message);
        reload_table_venta();
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1200);
      } else {
        $('.modal-message').addClass(response.style_modal);
        $('.modal-title-message').text((response.message_nubefact != null ? response.message_nubefact : response.message));
        setTimeout(function () { $('#modal-message').modal('hide'); }, 6100);
        reload_table_venta();
      }

      $('#btn-consultar_pse-' + ID).text('');
      $('#btn-consultar_pse-' + ID).attr('disabled', false);
      $('#btn-consultar_pse-' + ID).append('<i class="fa fa-cloud-upload"></i> Sunat');
    }
  });
}