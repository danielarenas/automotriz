var url;
var table_vehiculo;
var accion_producto = '';

function importarExcelProductos(){
  $( ".modal_importar_producto" ).modal( "show" );
}

$(function () {
  // Validate exist file excel product
	$( document ).on('click', '#btn-excel-importar_producto', function(event) {
	  if ( $( "#my-file-selector" ).val().length === 0 ) {
      $( '#my-file-selector' ).closest('.form-group').find('.help-block').html('Seleccionar archivo');
		  $( '#my-file-selector' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	  } else {
      $( '#btn-cancel-product' ).attr('disabled', true);
      $( '#a-download-product' ).attr('disabled', true);
	    
      $( '#btn-excel-importar_producto' ).text('');
      $( '#btn-excel-importar_producto' ).attr('disabled', true);
      $( '#btn-excel-importar_producto' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
  
      $( '#modal-loader' ).modal('show');
	  }
  })

  $(document).on('click', '#btn-cloud-api_sunarp', function (event) {
    if ($('[name="No_Placa_Vehiculo"]').val().length === 0) {
      $('[name="No_Placa_Vehiculo"]').closest('.form-group').find('.help-block').html('Ingresar placa');
      $('[name="No_Placa_Vehiculo"]').closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {
      $('#btn-cloud-api_sunarp').text('');
      $('#btn-cloud-api_sunarp').attr('disabled', true);
      $('#btn-cloud-api_sunarp').append('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

      var url_api = 'https://laesystems.com/librerias_v3/v1/';
      var token = 'f778632f1016e627c3ec25b5a2ce9b34';
      var url = url_api + token + '/sat/sunarp/busca_placa';
      var arrParams = {
        'placa': $('[name="No_Placa_Vehiculo"]').val(),
      }
      $.post(url, arrParams, function ( response ) {
        if (response.success == true) {
          $('[name="No_Year_Vehiculo"]').val(response.result[0].titulo_Anio);
          $('[name="No_Marca_Vehiculo"]').val(response.result[0].marca);
          $('[name="No_Modelo_Vehiculo"]').val(response.result[0].modelo);
          $('[name="No_Tipo_Combustible_Vehiculo"]').val(response.result[0].combustible);
          $('[name="No_Vin_Vehiculo"]').val(response.result[0].serie);
          $('[name="No_Motor"]').val(response.result[0].motor);
          $('[name="No_Color_Vehiculo"]').val(response.result[0].color);
        } else {
          alert( response.message );
        }
        console.log(response);
        $('#btn-cloud-api_sunarp').text('');
        $('#btn-cloud-api_sunarp').attr('disabled', false);
        $('#btn-cloud-api_sunarp').append('<i class="fa fa-cloud-download fa-lg"></i>');
      }, 'json')
      .fail(function (xhr, status, error) {
        $('#btn-cloud-api_sunarp').text('');
        $('#btn-cloud-api_sunarp').attr('disabled', false);
        $('#btn-cloud-api_sunarp').append('<i class="fa fa-cloud-download fa-lg"></i>');
      });
    }
  });

  $( '.div-AgregarEditar' ).hide();
  
  $('.select2').select2();
  
  url = base_url + 'Ventas/ReglasVenta//VehiculoController/ajax_list';
  table_vehiculo = $('#table-Producto').DataTable({
    'dom': 'B<"top">frt<"bottom"lip><"clear">',
    buttons     : [{
      extend    : 'excel',
      text      : '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
      titleAttr : 'Excel',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'pdf',
      text      : '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
      titleAttr : 'PDF',
      exportOptions: {
        columns: ':visible'
      }
    },
    {
      extend    : 'colvis',
      text      : '<i class="fa fa-ellipsis-v"></i> Columnas',
      titleAttr : 'Columnas',
      exportOptions: {
        columns: ':visible'
      }
    }],
    'searching'   : false,
    'bStateSave'  : true,
    'processing'  : true,
    'serverSide'  : true,
    'info'        : true,
    'autoWidth'   : false,
    'pagingType'  : 'full_numbers',
    'oLanguage' : {
      'sInfo'               : 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
      'sLengthMenu'         : '_MENU_',
      'sSearch'             : 'Buscar por: ',
      'sSearchPlaceholder'  : 'UPC / Nombre',
      'sZeroRecords'        : 'No se encontraron registros',
      'sInfoEmpty'          : 'No hay registros',
      'sLoadingRecords'     : 'Cargando...',
      'sProcessing'         : 'Procesando...',
      'oPaginate'           : {
        'sFirst'    : '<<',
        'sLast'     : '>>',
        'sPrevious' : '<',
        'sNext'     : '>',
      },
    },
    'order': [],
    'ajax': {
      'url'       : url,
      'type'      : 'POST',
      'dataType'  : 'json',
      'data'      : function ( data ) {
        data.Filtros_Productos = $( '#cbo-Filtros_Productos' ).val(),
        data.Global_Filter = $( '#txt-Global_Filter' ).val();
      },
    },
    'columnDefs': [{
      'className' : 'text-center',
      'targets'   : 'no-sort',
      'orderable' : false,
    },{
      'className' : 'text-right',
      'targets'   : 'sort_right',
      'orderable' : true,
    },],
    'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
  });
  
  $('.dataTables_length').addClass('col-sm-3 col-md-1');
  $('.dataTables_info').addClass('col-sm-3 col-md-2');
  $('.dataTables_paginate').addClass('col-sm-6 col-md-8');

  $( '#txt-Global_Filter' ).keyup(function() {
    table_vehiculo.search($(this).val()).draw();
  });

  $( '#form-Vehiculo' ).validate({
		rules:{
      ID_Entidad: {
				required: true,
      },
      No_Placa_Vehiculo: {
        required: true,
			},
      No_Year_Vehiculo: {
        required: true,
        minlength: 4,
        maxlength: 4
      },
      No_Marca_Vehiculo: {
        required: true,
      },
      No_Modelo_Vehiculo: {
        required: true,
      },
      No_Tipo_Combustible_Vehiculo: {
        required: true,
      },
      No_Vin_Vehiculo: {
        required: true,
      },
      No_Motor: {
        required: true,
      },
      No_Color_Vehiculo: {
        required: true,
      },
		},
		messages:{
      ID_Entidad:{
				required: "Ingresar cliente",
			},
      No_Placa_Vehiculo:{
        required: "Ingresar placa",
      },
      No_Year_Vehiculo: {
        required: "Ingresar año",
        minlength: "Ingresar 4 dígitos",
        maxlength: "Ingresar 4 dígitos",
      },
      No_Marca_Vehiculo: {
        required: "Ingresar marca",
      },
      No_Modelo_Vehiculo: {
        required: "Ingresar modelo",
      },
      No_Tipo_Combustible_Vehiculo: {
        required: "Ingresar tipo combustible",
      },
      No_Vin_Vehiculo: {
        required: "Ingresar VIN",
      },
      No_Motor: {
        required: "Ingresar Motor",
      },
      No_Color_Vehiculo: {
        required: "Ingresar color",
      },
		},
		errorPlacement : function(error, element) {
			$(element).closest('.form-group').find('.help-block').html(error.html());
	  },
		highlight : function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function(element, errorClass, validClass) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			$(element).closest('.form-group').find('.help-block').html('');
	  },
		submitHandler: form_Producto
	});
	
  $(document).bind('keydown', 'f2', function(){
    agregarProducto();
  });
})

function isExistTableTemporalEnlacesProducto($ID_Producto_Enlace){
  return Array.from($('tr[id*=tr_enlace_producto]'))
    .some(element => ($('td:nth(0)',$(element)).html()===$ID_Producto_Enlace));
}

function agregarProducto(){
  accion_producto = 'add_producto';
  
  $( '#form-Vehiculo' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();
  $( '.div-AgregarEditar' ).show();
  
  $( '.title_Producto' ).text('Nuevo Vehículo');

  $('[name="EID_Producto"]').val('');

  $('[name="ID_Entidad"]').focus();
  $('#modal-loader').modal('hide');
}

function verProducto(ID){
  accion_producto = 'upd_producto';
  
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();
  
  $( '.div-Compuesto' ).hide();
	$( '#table-Producto_Enlace tbody' ).empty();

  $( '#form-Vehiculo' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
 
	$( '.div-Producto' ).show();
  
  $( '#cbo-TiposItem' ).prop('disabled', true);
  
  url = base_url + 'Ventas/ReglasVenta//VehiculoController/ajax_edit/' + ID;
  $.ajax({
    url : url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      if ( response.length > 0 ) {
        response = response[0];

        $( '.div-AgregarEditar' ).show();
        
        $( '.title_Producto' ).text('Modifcar Vehículo');
        
        $('[name="EID_Entidad"]').val(response.ID_Entidad);
        $( '[name="EID_Producto"]' ).val(response.ID_Producto);

        $('[name="ID_Tipo_Producto"]').val(response.ID_Tipo_Producto );
        $('[name="AID"]').val(response.ID_Entidad);
        $('[name="ID_Entidad"]').val(response.No_Entidad);
        $('[name="No_Placa_Vehiculo"]').val(response.No_Placa_Vehiculo );
        $('[name="No_Year_Vehiculo"]').val(response.No_Year_Vehiculo);
        $('[name="No_Marca_Vehiculo"]').val(response.No_Marca_Vehiculo);
        $('[name="No_Modelo_Vehiculo"]').val(response.No_Modelo_Vehiculo);
        $('[name="No_Tipo_Combustible_Vehiculo"]').val(response.No_Tipo_Combustible_Vehiculo);
        $('[name="No_Vin_Vehiculo"]').val(response.No_Vin_Vehiculo);
        $('[name="No_Motor"]').val(response.No_Motor);
        $('[name="Nu_Kilometraje"]').val(response.Nu_Kilometraje);
        $('[name="No_Color_Vehiculo"]').val(response.No_Color_Vehiculo);
        $('[name="No_Placa_Vehiculo"]').focus();
      } else {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text('Problemas al obtener registro');
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);
      }

      $('#modal-loader').modal('hide');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $( '#modal-loader' ).modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    }
  })
}

function form_Producto(){
  if (accion_producto == 'add_producto' || accion_producto == 'upd_producto') {
    $( '.help-block' ).empty();
    if ($( '#txt-AID' ).val().length === 0){
      $( '#txt-AID' ).closest('.form-group').find('.help-block').html('Debe seleccionar cliente');
      $( '#txt-AID' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {      
      $( '#btn-save' ).text('');
      $( '#btn-save' ).attr('disabled', true);
      $( '#btn-save' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
    
      $( '#modal-loader' ).modal('show');
      
      url = base_url + 'Ventas/ReglasVenta//VehiculoController/crudProducto';
    	$.ajax({
        type		  : 'POST',
        dataType	: 'JSON',
        url: url,
        data: $('#form-Vehiculo').serialize(),
    		success : function( response ){
    		  $( '#modal-loader' ).modal('hide');
    		  
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
      	  $( '#modal-message' ).modal('show');
      	  
    		  if (response.status == 'success'){
    		    accion_producto = '';
    		    
    		    $( '#form-Vehiculo' )[0].reset();
            $( '.div-AgregarEditar' ).hide();
            $( '.div-Listar' ).show();
      	    $( '.modal-message' ).addClass(response.style_modal);
      	    $( '.modal-title-message' ).text(response.message);
      	    setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
      	    reload_table_vehiculo();
    		  } else {
      	    $( '.modal-message' ).addClass(response.style_modal);
      	    $( '.modal-title-message' ).text(response.message);
      	    setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
    		  }
    		  
          $( '#btn-save' ).text('');
          $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar (ENTER)' );
          $( '#btn-save' ).attr('disabled', false);
    		},
        error: function (jqXHR, textStatus, errorThrown) {
          $( '#modal-loader' ).modal('hide');
    	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
    	    
      	  $( '#modal-message' ).modal('show');
    	    $( '.modal-message' ).addClass( 'modal-danger' );
    	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
    	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
    	    
    	    //Message for developer
          console.log(jqXHR.responseText);
    	    
          $( '#btn-save' ).text('');
          $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar' );
          $( '#btn-save' ).attr('disabled', false);
        }
    	});
    }
  }// /. if de accion_producto
}

function eliminarProducto(ID, accion_producto){
  var $modal_delete = $( '#modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });
  
  $(document).bind('keydown', 'alt+l', function(){
    if ( accion_producto=='delete' ) {
      _eliminarProducto($modal_delete, ID);
      accion_producto='';
    }
  });

  $( '#btn-save-delete' ).off('click').click(function () {
    _eliminarProducto($modal_delete, ID);
  });
}

function _eliminarProducto($modal_delete, ID){
  $( '#modal-loader' ).modal('show');
  
  url = base_url + 'Ventas/ReglasVenta//VehiculoController/eliminarProducto/' + ID;
  $.ajax({
    url       : url,
    type      : "GET",
    dataType  : "JSON",
    success: function( response ){
      $( '#modal-loader' ).modal('hide');
      
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
    	  accion_producto = '';
    		    
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
  	    reload_table_vehiculo();
		  } else {
    	  accion_producto = '';
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1500);
		  }
    },
    error: function (jqXHR, textStatus, errorThrown) {
    	accion_producto = '';
      $( '#modal-loader' ).modal('hide');
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function reload_table_vehiculo() {
  table_vehiculo.ajax.reload(null, false);
}