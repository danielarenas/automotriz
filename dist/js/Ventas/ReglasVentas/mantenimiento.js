var url;
var table_mantenimiento;
var accion_producto = '';

$(function () {

    url = base_url + 'Ventas/ReglasVenta/MantenimientoController/ajax_list';
    table_mantenimiento = $('#table-Producto').DataTable({
        'dom': 'B<"top">frt<"bottom"lip><"clear">',
        buttons     : [{
          extend    : 'excel',
          text      : '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel',
          titleAttr : 'Excel',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend    : 'pdf',
          text      : '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF',
          titleAttr : 'PDF',
          exportOptions: {
            columns: ':visible'
          }
        },
        {
          extend    : 'colvis',
          text      : '<i class="fa fa-ellipsis-v"></i> Columnas',
          titleAttr : 'Columnas',
          exportOptions: {
            columns: ':visible'
          }
        }],
        'searching'   : false,
        'bStateSave'  : true,
        'processing'  : true,
        'serverSide'  : true,
        'info'        : true,
        'autoWidth'   : false,
        'pagingType'  : 'full_numbers',
        'oLanguage' : {
          'sInfo'               : 'Mostrando (_START_ - _END_) total de registros _TOTAL_',
          'sLengthMenu'         : '_MENU_',
          'sSearch'             : 'Buscar por: ',
          'sSearchPlaceholder'  : 'UPC / Nombre',
          'sZeroRecords'        : 'No se encontraron registros',
          'sInfoEmpty'          : 'No hay registros',
          'sLoadingRecords'     : 'Cargando...',
          'sProcessing'         : 'Procesando...',
          'oPaginate'           : {
            'sFirst'    : '<<',
            'sLast'     : '>>',
            'sPrevious' : '<',
            'sNext'     : '>',
          },
        },
        'order': [],
        'ajax': {
          'url'       : url,
          'type'      : 'POST',
          'dataType'  : 'json',
          'data'      : function ( data ) {
            data.Filtros_Productos = $( '#cbo-Filtros_Productos' ).val(),
            data.Global_Filter = $( '#txt-Global_Filter' ).val();
          },
        },
        'columnDefs': [{
          'className' : 'text-center',
          'targets'   : 'no-sort',
          'orderable' : false,
        },{
          'className' : 'text-right',
          'targets'   : 'sort_right',
          'orderable' : true,
        },],
        'lengthMenu': [[10, 100, 1000, -1], [10, 100, 1000, "Todos"]],
    });
});

$( '#txt-Global_Filter' ).keyup(function() {
  table_mantenimiento.search($(this).val()).draw();
});

function agregarProducto(){
    accion_producto = 'add_producto';
    
    $( '#form-Mantenimiento' )[0].reset();
    $( '.form-group' ).removeClass('has-error');
    $( '.form-group' ).removeClass('has-success');
    $( '.help-block' ).empty();
    
    $( '#modal-loader' ).modal('show');
    
    $( '.div-Listar' ).hide();
    $( '.div-AgregarEditar' ).show();
    
    $( '.title_Producto' ).text('Nuevo Tipo de mantenimiento');
  
    $('[name="ID_Tabla_Dato"]').val('');
  
    $('#modal-loader').modal('hide');
}

function verProducto(ID)
{
  accion_producto = 'upd_producto';
  
  $( '#modal-loader' ).modal('show');
  
  $( '.div-Listar' ).hide();
  
  $( '.div-Compuesto' ).hide();
    $( '#table-Producto_Enlace tbody' ).empty();

  $( '#form-Mantenimiento' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();
  
    $( '.div-Producto' ).show();
  
  $( '#cbo-TiposItem' ).prop('disabled', true);
  
  url = base_url + 'Ventas/ReglasVenta/MantenimientoController/ajax_edit/' + ID;
  $.ajax({
    url : url,
    type: "GET",
    dataType: "JSON",
    success: function (response) {
      if ( response.length > 0 ) {
        response = response[0];

        $( '.div-AgregarEditar' ).show();
        
        $( '.title_Producto' ).text('Modifcar Tipo de Mantenimiento');
        
        $('[name="ID_Tabla_Dato"]').val(response.ID_Tabla_Dato);
        $('[name="No_Descripcion"]').val(response.No_Descripcion );
      } else {
        $('.modal-message').removeClass('modal-danger modal-warning modal-success');
        $('#modal-message').modal('show');
        $('.modal-message').addClass('modal-danger');
        $('.modal-title-message').text('Problemas al obtener registro');
        setTimeout(function () { $('#modal-message').modal('hide'); }, 1700);
      }

      $('#modal-loader').modal('hide');
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $( '#modal-loader' ).modal('hide');
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
        
        //Message for developer
      console.log(jqXHR.responseText);
    }
  })
}

function eliminarProducto(ID, accion_producto)
{
  var $modal_delete = $( '#modal-message-delete' );
  $modal_delete.modal('show');
  
  $( '#btn-cancel-delete' ).off('click').click(function () {
    $modal_delete.modal('hide');
  });
  
  $(document).bind('keydown', 'alt+l', function(){
    if ( accion_producto=='delete' ) {
      _eliminarProducto($modal_delete, ID);
      accion_producto='';
    }
  });

  $( '#btn-save-delete' ).off('click').click(function () {
    _eliminarProducto($modal_delete, ID);
  });
}

function _eliminarProducto($modal_delete, ID){
  $( '#modal-loader' ).modal('show');
  
  url = base_url + 'Ventas/ReglasVenta/MantenimientoController/eliminarProducto/' + ID;
  $.ajax({
    url       : url,
    type      : "GET",
    dataType  : "JSON",
    success: function( response ){
      $( '#modal-loader' ).modal('hide');
      
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
  	  $( '#modal-message' ).modal('show');
		  
		  if (response.status == 'success'){
    	  accion_producto = '';
    		    
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
  	    reload_table_mantenimiento();
		  } else {
    	  accion_producto = '';
  	    $( '.modal-message' ).addClass(response.style_modal);
  	    $( '.modal-title-message' ).text(response.message);
  	    setTimeout(function() {$('#modal-message').modal('hide');}, 1500);
		  }
    },
    error: function (jqXHR, textStatus, errorThrown) {
    	accion_producto = '';
      $( '#modal-loader' ).modal('hide');
      $modal_delete.modal('hide');
	    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
	    
  	  $( '#modal-message' ).modal('show');
	    $( '.modal-message' ).addClass( 'modal-danger' );
	    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
	    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
	    
	    //Message for developer
      console.log(jqXHR.responseText);
    },
  });
}

function form_Producto(){
  if (accion_producto == 'add_producto' || accion_producto == 'upd_producto') {
    $( '.help-block' ).empty();
    if ($( '#No_Descripcion' ).val().length === 0){
      $( '#No_Descripcion' ).closest('.form-group').find('.help-block').html('Debe agregar un nombre');
      $( '#No_Descripcion' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    } else {      
      $( '#btn-save' ).text('');
      $( '#btn-save' ).attr('disabled', true);
      $( '#btn-save' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
    
      $( '#modal-loader' ).modal('show');
      
      url = base_url + 'Ventas/ReglasVenta/MantenimientoController/crudProducto';
      $.ajax({
        type		  : 'POST',
        dataType	: 'JSON',
        url: url,
        data: $('#form-Mantenimiento').serialize(),
        success : function( response ){
          $( '#modal-loader' ).modal('hide');
          
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          $( '#modal-message' ).modal('show');
          
          if (response.status == 'success'){
            accion_producto = '';
            
            $( '#form-Mantenimiento' )[0].reset();
            $( '.div-AgregarEditar' ).hide();
            $( '.div-Listar' ).show();
            $( '.modal-message' ).addClass(response.style_modal);
            $( '.modal-title-message' ).text(response.message);
            setTimeout(function() {$('#modal-message').modal('hide'); }, 1100);
            reload_table_mantenimiento();
          } else {
            $( '.modal-message' ).addClass(response.style_modal);
            $( '.modal-title-message' ).text(response.message);
            setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
          }
          
          $( '#btn-save' ).text('');
          $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar (ENTER)' );
          $( '#btn-save' ).attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          $( '#modal-loader' ).modal('hide');
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          
          $( '#modal-message' ).modal('show');
          $( '.modal-message' ).addClass( 'modal-danger' );
          $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
          setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
          
          //Message for developer
          console.log(jqXHR.responseText);
          
          $( '#btn-save' ).text('');
          $( '#btn-save' ).append( '<span class="fa fa-save"></span> Guardar' );
          $( '#btn-save' ).attr('disabled', false);
        }
      });
    }
  }// /. if de accion_producto
}

$( '#form-Mantenimiento' ).validate({
    rules:{
      No_Descripcion: {
          required: true,
      },
    },
    messages:{
        No_Descripcion:{
            required: "Ingresar nombre",
        },
    },
    errorPlacement : function(error, element) {
        $(element).closest('.form-group').find('.help-block').html(error.html());
    },
    highlight : function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(element).closest('.form-group').find('.help-block').html('');
    },
    submitHandler: form_Producto
});

$(document).bind('keydown', 'f2', function(){
    agregarProducto();
});

function reload_table_mantenimiento() {
  table_mantenimiento.ajax.reload(null, false);
}