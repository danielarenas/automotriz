var url;

$(function () {
  $( '.div-fecha_historica' ).hide();
  $( '#div-venta_punto_venta' ).hide();
  
  $( '#modal-loader' ).modal('show');

  $( '#cbo-tipo_consulta_fecha' ).change(function(){
    $( '.div-fecha_historica' ).hide();
    if (  $(this).val() > 0 )
      $( '.div-fecha_historica' ).show();
  })

  url = base_url + 'HelperController/getTiposDocumentos';
  $.post( url, {Nu_Tipo_Filtro : 1}, function( response ){
    $( '#cbo-filtros_tipos_documento' ).html('<option value="0" selected="selected">Todos</option>');
    for (var i = 0; i < response.length; i++)
      $( '#cbo-filtros_tipos_documento' ).append( '<option value="' + response[i].ID_Tipo_Documento + '">' + response[i].No_Tipo_Documento_Breve + '</option>' );
	  $( '#modal-loader' ).modal('hide');
  }, 'JSON');
  
	$( '#cbo-filtros_series_documento' ).html('<option value="0" selected="selected">Todos</option>');
	$( '#cbo-filtros_tipos_documento' ).change(function(){
	  $( '#cbo-filtros_series_documento' ).html('<option value="0" selected="selected">Todos</option>');
	  if ( $(this).val() > 0) {
		  url = base_url + 'HelperController/getSeriesDocumentoPuntoVenta';
      $.post( url, { ID_Tipo_Documento: $(this).val() }, function( response ){
        for (var i = 0; i < response.length; i++)
          $( '#cbo-filtros_series_documento' ).append( '<option value="' + response[i].ID_Serie_Documento + '">' + response[i].ID_Serie_Documento + '</option>' );
      }, 'JSON');
	  }
  })
  
  $( '.btn-generar_venta_punto_venta' ).click(function(){
    if ( $( '#cbo-tipo_consulta_fecha' ).val() == '1' || ($( '#cbo-tipo_consulta_fecha' ).val() == '0' && $('#header-a-id_matricula_empleado').length) ) {
      $( '.help-block' ).empty();
    
      var Fe_Inicio, Fe_Fin, ID_Tipo_Documento, ID_Serie_Documento, ID_Numero_Documento, Nu_Estado_Documento;
      
      iTipoConsultaFecha  = $( '#cbo-tipo_consulta_fecha' ).val();
      Fe_Inicio           = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
      Fe_Fin              = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
      ID_Tipo_Documento   = $( '#cbo-filtros_tipos_documento' ).val();
      ID_Serie_Documento  = $( '#cbo-filtros_series_documento' ).val();
      ID_Numero_Documento = ($( '#txt-Filtro_NumeroDocumento' ).val().length == 0 ? '-' : $( '#txt-Filtro_NumeroDocumento' ).val());
      Nu_Estado_Documento = $( '#cbo-estado_documento' ).val();
        
      if ($(this).data('type') == 'html') {
        getReporteHTML();
      } else if ($(this).data('type') == 'pdf') {
        $( '#btn-pdf_venta_punto_venta' ).text('');
        $( '#btn-pdf_venta_punto_venta' ).attr('disabled', true);
        $( '#btn-pdf_venta_punto_venta' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
        
        url = base_url + 'PuntoVenta/VentaPuntoVentaController/sendReportePDF/' + iTipoConsultaFecha + '/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Tipo_Documento + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Estado_Documento + '/' + iIdCliente + '/' + sNombreCliente + '/' + iTipoRecepcionCliente;
        window.open(url,'_blank');
        
        $( '#btn-pdf_venta_punto_venta' ).text('');
        $( '#btn-pdf_venta_punto_venta' ).append( '<i class="fa fa-file-pdf-o color_icon_pdf"></i> PDF' );
        $( '#btn-pdf_venta_punto_venta' ).attr('disabled', false);
      } else if ($(this).data('type') == 'excel') {
        $( '#btn-excel_venta_punto_venta' ).text('');
        $( '#btn-excel_venta_punto_venta' ).attr('disabled', true);
        $( '#btn-excel_venta_punto_venta' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
        
        url = base_url + 'PuntoVenta/VentaPuntoVentaController/sendReporteEXCEL/'  + iTipoConsultaFecha + '/' + Fe_Inicio + '/' + Fe_Fin + '/' + ID_Tipo_Documento + '/' + ID_Serie_Documento + '/' + ID_Numero_Documento + '/' + Nu_Estado_Documento + '/' + iIdCliente + '/' + sNombreCliente + '/' + iTipoRecepcionCliente;
        window.open(url,'_blank');
        
        $( '#btn-excel_venta_punto_venta' ).text('');
        $( '#btn-excel_venta_punto_venta' ).append( '<i class="fa fa-file-excel-o color_icon_excel"></i> Excel' );
        $( '#btn-excel_venta_punto_venta' ).attr('disabled', false);
      }
    } else {
      alert('Primero se debe de aperturar caja');
    }// ./ if - else
  })//./ btn
  
  $( '#txt-ID_Tipo_Documento_Identidad' ).val(2);
  
	// Tipo Documento Identidad
	$( '#modal-cbo-tipo_documento' ).change(function(){
		$( '#label_correo' ).show();
		$( '#txt-Txt_Email_Entidad_Cliente' ).show();
	  	if ( $(this).val()==4) {//Boleta
			$( '#label-tipo_documento_identidad' ).text('DNI');
			$( '#txt-ID_Tipo_Documento_Identidad' ).val(2);//DNI
			$( '#txt-ACodigo' ).attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
		} else if ( $(this).val()==3) {//Factura
			$( '#label-tipo_documento_identidad' ).text('RUC');
			$( '#txt-ID_Tipo_Documento_Identidad' ).val(4);//RUC
			$( '#txt-ACodigo' ).attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
		} else {
			$( '#txt-ID_Tipo_Documento_Identidad' ).val(1);//RUC
			$( '#label-tipo_documento_identidad' ).text('OTROS');
			$( '#txt-ACodigo' ).attr('maxlength', 15);
			$( '#label_correo' ).hide();
			$( '#txt-Txt_Email_Entidad_Cliente' ).hide();
		}
  })
  
	$( "#txt-Txt_Email_Entidad_Cliente" ).blur(function() {
		caracteresCorreoValido($(this).val(), '#span-email');
  })

  // COBRAR CREDITO
  $( '#btn-cobrar_cliente' ).click(function(){
		var fPagoClienteCobranza = parseFloat($( '#tel-cobrar_cliente-fPagoCliente' ).val());
    if ( fPagoClienteCobranza == 0.00 || isNaN(fPagoClienteCobranza) ) {
      $( '[name="fPagoCliente"]' ).closest('.form-group').find('.help-block').html( 'Ingresar monto' );
      $( '[name="fPagoCliente"]' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-cobrar_cliente .modal-body'), $( '[name="fPagoCliente"]' ));
    } else if ( fPagoClienteCobranza > parseFloat($( '#hidden-cobrar_cliente-fsaldo' ).val()) ) {
      $( '#tel-cobrar_cliente-fPagoCliente' ).closest('.form-group').find('.help-block').html('Debes de cobrar <b>' + $( '#hidden-cobrar_cliente-fsaldo' ).val() + '</b>' );
      $( '#tel-cobrar_cliente-fPagoCliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-cobrar_cliente .modal-body'), $( '#tel-cobrar_cliente-fPagoCliente' ));
    } else if ( $( '#cbo-modal_quien_recibe' ).val() == 0 && $( '[name="sNombreRecepcion"]' ).val().length === 0 ) {
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').find('.help-block').html('Ingresar datos');
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-cobrar_cliente .modal-body'), $( '[name="sNombreRecepcion"]' ));
    } else {
      $( '.help-block' ).empty();
      $( '[name="fPagoCliente"]' ).closest('.form-group').removeClass('has-error');
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').removeClass('has-error');
      
      $( '#btn-cobrar_cliente' ).text('');
      $( '#btn-cobrar_cliente' ).attr('disabled', true);
      $( '#btn-cobrar_cliente' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      $( '#btn-salir' ).attr('disabled', true);

      url = base_url + 'PuntoVenta/VentaPuntoVentaController/entregarPedidoLavado';
      $.ajax({
        type : 'POST',
        dataType : 'JSON',
        url : url,
        data : $('#form-cobrar_cliente').serialize(),
        success : function( response ){
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          $( '#modal-message' ).modal('show');

          if ( response.sStatus=='success' ) {
            $( '.modal-cobrar_cliente' ).modal('hide');

            $( '.modal-message' ).addClass( 'modal-' + response.sStatus);
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
            
            getReporteHTML();
          } else {
            $( '.modal-message' ).addClass( 'modal-' + response.sStatus );
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
          }
          
          $( '#btn-cobrar_cliente' ).text('');
          $( '#btn-cobrar_cliente' ).append( 'Cobrar' );
          $( '#btn-cobrar_cliente' ).attr('disabled', false);
          $( '#btn-salir' ).attr('disabled', false);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
        
        //Message for developer
        console.log(jqXHR.responseText);

        $( '#btn-cobrar_cliente' ).text('');
        $( '#btn-cobrar_cliente' ).attr('disabled', false);
        $( '#btn-cobrar_cliente' ).append( 'Cobrar' );
        $( '#btn-salir' ).attr('disabled', false);
      })
    }
  })

  // ENTREGAR PEDIDO LAVANDERIA
  $( '#btn-entregar_pedido' ).click(function(){
		var fPagoClienteCobranza = parseFloat($( '#tel-entregar_pedido-fPagoCliente' ).val());
    if ( $( '#hidden-entregar_pedido-fsaldo' ).val() != '0' && (fPagoClienteCobranza == 0.00 || isNaN(fPagoClienteCobranza)) ) {
      $( '[name="fPagoCliente"]' ).closest('.form-group').find('.help-block').html( 'Ingresar monto' );
      $( '[name="fPagoCliente"]' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-entregar_pedido .modal-body'), $( '[name="fPagoCliente"]' ));
    } else if ( $( '#hidden-entregar_pedido-fsaldo' ).val() != '0' && fPagoClienteCobranza > parseFloat($( '#hidden-entregar_pedido-fsaldo' ).val()) ) {
      $( '#tel-entregar_pedido-fPagoCliente' ).closest('.form-group').find('.help-block').html('Debes de cobrar <b>' + $( '#hidden-entregar_pedido-fsaldo' ).val() + '</b>' );
      $( '#tel-entregar_pedido-fPagoCliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-entregar_pedido .modal-body'), $( '#tel-entregar_pedido-fPagoCliente' ));
    } else if ( $( '#cbo-modal_quien_recibe' ).val() == 0 && $( '[name="sNombreRecepcion"]' ).val().length === 0 ) {
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').find('.help-block').html('Ingresar datos');
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-entregar_pedido .modal-body'), $( '[name="sNombreRecepcion"]' ));
    } else {
      $( '.help-block' ).empty();
      $( '[name="sNombreRecepcion"]' ).closest('.form-group').removeClass('has-error');
      
      $( '#btn-entregar_pedido' ).text('');
      $( '#btn-entregar_pedido' ).attr('disabled', true);
      $( '#btn-entregar_pedido' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      $( '#btn-salir' ).attr('disabled', true);

      url = base_url + 'PuntoVenta/VentaPuntoVentaController/entregarPedidoLavado';
      $.ajax({
        type : 'POST',
        dataType : 'JSON',
        url : url,
        data : $('#form-entregar_pedido').serialize(),
        success : function( response ){
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          $( '#modal-message' ).modal('show');

          if ( response.sStatus=='success' ) {
            $( '.modal-entregar_pedido' ).modal('hide');

            $( '.modal-message' ).addClass( 'modal-' + response.sStatus);
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
            
            getReporteHTML();
          } else {
            $( '.modal-message' ).addClass( 'modal-' + response.sStatus );
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
          }
          
          $( '#btn-entregar_pedido' ).text('');
          $( '#btn-entregar_pedido' ).append( 'Entregar' );
          $( '#btn-entregar_pedido' ).attr('disabled', false);
          $( '#btn-salir' ).attr('disabled', false);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
        
        //Message for developer
        console.log(jqXHR.responseText);

        $( '#btn-entregar_pedido' ).text('');
        $( '#btn-entregar_pedido' ).attr('disabled', false);
        $( '#btn-entregar_pedido' ).append( 'Entregar' );
        $( '#btn-salir' ).attr('disabled', false);
      })
    }
  })
  
  // FACTURAR ORDEN LAVANDERIA
  $( '#btn-facturar_orden_lavanderia' ).click(function(){
    if ( $( '#txt-ACodigo' ).val().length === 0 ) {
      $( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html('Ingresar datos');
      $( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-facturar_orden_lavanderia .modal-body'), $( '#txt-ACodigo' ));
    } else if ( $( '#txt-ANombre' ).val().length === 0 ) {
      $( '#txt-ANombre' ).closest('.form-group').find('.help-block').html('Ingresar datos');
      $( '#txt-ANombre' ).closest('.form-group').removeClass('has-success').addClass('has-error');
    
      scrollToError($('.modal-facturar_orden_lavanderia .modal-body'), $( '#txt-ANombre' ));
    } else {
      $( '.help-block' ).empty();
      
      $( '#btn-facturar_orden_lavanderia' ).text('');
      $( '#btn-facturar_orden_lavanderia' ).attr('disabled', true);
      $( '#btn-facturar_orden_lavanderia' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );
      $( '#btn-salir' ).attr('disabled', true);

      url = base_url + 'PuntoVenta/VentaPuntoVentaController/facturarOrdenLavanderia';
      $.ajax({
        type : 'POST',
        dataType : 'JSON',
        url : url,
        data : $('#form-facturar_orden_lavanderia').serialize(),
        success : function( response ){
          $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
          $( '#modal-message' ).modal('show');

          if ( response.sStatus=='success' ) {
            $( '.modal-facturar_orden_lavanderia' ).modal('hide');

            $( '.modal-message' ).addClass( 'modal-' + response.sStatus);
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
            
            getReporteHTML();
          } else {
            $( '.modal-message' ).addClass( 'modal-' + response.sStatus );
            $( '.modal-title-message' ).text( response.sMessage );
            setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
          }
          
          $( '#btn-facturar_orden_lavanderia' ).text('');
          $( '#btn-facturar_orden_lavanderia' ).append( 'Entregar' );
          $( '#btn-facturar_orden_lavanderia' ).attr('disabled', false);
          $( '#btn-salir' ).attr('disabled', false);
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
        
        $( '#modal-message' ).modal('show');
        $( '.modal-message' ).addClass( 'modal-danger' );
        $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
        setTimeout(function() {$('#modal-message').modal('hide');}, 3100);
        
        //Message for developer
        console.log(jqXHR.responseText);

        $( '#btn-facturar_orden_lavanderia' ).text('');
        $( '#btn-facturar_orden_lavanderia' ).attr('disabled', false);
        $( '#btn-facturar_orden_lavanderia' ).append( 'Entregar' );
        $( '#btn-salir' ).attr('disabled', false);
      })
    }
  })
})

function getReporteHTML(){
  var Fe_Inicio, Fe_Fin, ID_Tipo_Documento, ID_Serie_Documento, ID_Numero_Documento, Nu_Estado_Documento, iIdCliente, sNombreCliente, iTipoRecepcionCliente;
  
  iTipoConsultaFecha  = $( '#cbo-tipo_consulta_fecha' ).val();
  Fe_Inicio           = ParseDateString($( '#txt-Filtro_Fe_Inicio' ).val(), 1, '/');
  Fe_Fin              = ParseDateString($( '#txt-Filtro_Fe_Fin' ).val(), 1, '/');
  ID_Tipo_Documento   = $( '#cbo-filtros_tipos_documento' ).val();
  ID_Serie_Documento  = $( '#cbo-filtros_series_documento' ).val();
  ID_Numero_Documento = ($( '#txt-Filtro_NumeroDocumento' ).val().length == 0 ? '-' : $( '#txt-Filtro_NumeroDocumento' ).val());
  Nu_Estado_Documento = $( '#cbo-estado_documento' ).val();
  iIdCliente = ($( '#txt-AID' ).val().length === 0 ? '-' : $( '#txt-AID' ).val());
  sNombreCliente = ($( '#txt-Filtro_Entidad' ).val().length === 0 ? '-' : $( '#txt-Filtro_Entidad' ).val());
  iTipoRecepcionCliente = $( '#cbo-tipo_recepcion_cliente' ).val();
  
  $( '#btn-html_venta_punto_venta' ).text('');
  $( '#btn-html_venta_punto_venta' ).attr('disabled', true);
  $( '#btn-html_venta_punto_venta' ).append( 'Cargando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );

  $( '#table-venta_punto_venta > tbody' ).empty();
  $( '#table-venta_punto_venta > tfoot' ).empty();
  
  var arrPost = {
    iTipoConsultaFecha  : iTipoConsultaFecha,
    Fe_Inicio           : Fe_Inicio,
    Fe_Fin              : Fe_Fin,
    ID_Tipo_Documento   : ID_Tipo_Documento,
    ID_Serie_Documento  : ID_Serie_Documento,
    ID_Numero_Documento : ID_Numero_Documento,
    Nu_Estado_Documento : Nu_Estado_Documento,
    iIdCliente : iIdCliente,
    sNombreCliente : sNombreCliente,
    iTipoRecepcionCliente : iTipoRecepcionCliente,
  };
  url = base_url + 'PuntoVenta/VentaPuntoVentaController/sendReporte';
  $.post( url, arrPost, function( response ){
    if ( response.sStatus == 'success' ) {
      var iTotalRegistros = response.arrData.length, response=response.arrData, tr_body = '', tr_foot = '', arrParams = '', sButtonCobrarPedido = '', sButtonEntregarPedido='', sButtonFacturarLavanderia='';
      var subtotal_s = 0.00, descuento_s = 0.00, igv_s = 0.00, total_s = 0.00, total_d = 0.00, fTotalSaldo=0.00;
      var sum_general_subtotal_s=0.00, sum_general_igv_s=0.00, sum_general_descuento_s=0.00, sum_general_total_s=0.00, sum_general_total_d=0.00;
      for (var i = 0; i < iTotalRegistros; i++) {
        fTotalSaldo = (!isNaN(parseFloat(response[i].Ss_Total_Saldo)) ? parseFloat(response[i].Ss_Total_Saldo) : 0);
        total_s = (!isNaN(parseFloat(response[i].Ss_Total)) ? parseFloat(response[i].Ss_Total) : 0);
        total_d = (!isNaN(parseFloat(response[i].Ss_Total_Extranjero)) ? parseFloat(response[i].Ss_Total_Extranjero) : 0);
        
        arrParams = {
          'iIdEmpresa' : response[i].ID_Empresa,
          'iIdDocumentoCabecera' : response[i].ID_Documento_Cabecera,
          'fTotalSaldo' : fTotalSaldo,
          'sCliente' : response[i].No_Entidad,
          'sTipoDocumento' : response[i].No_Tipo_Documento_Breve,
          'sSerieDocumento' : response[i].ID_Serie_Documento,
          'sNumeroDocumento' : response[i].ID_Numero_Documento,
          'iEstadoLavadoRecepcionCliente' : response[i].Nu_Estado_Lavado_Recepcion_Cliente,
          'sSignoMoneda' : response[i].No_Signo,
          'iIdDocumentoMedioPago' : response[i].ID_Documento_Medio_Pago,
        }
        arrParams = JSON.stringify(arrParams);

        sButtonCobrarPedido = '';
        if ( fTotalSaldo > 0 ) {
          sButtonCobrarPedido = "<button type='button' class='btn btn-xs btn-link' alt='Cobrar' title='Cobrar' href='javascript:void(0)' onclick='cobrarPedido(" + arrParams + ")'>Cobrar</button>";
        }

        sButtonEntregarPedido = '';
        if ( response[i].Nu_Estado_Lavado_Recepcion_Cliente != 3 ) {
          sButtonEntregarPedido = "<button type='button' class='btn btn-xs btn-link' alt='Entregar pedido' title='Entregar pedido' href='javascript:void(0)' onclick='entregarPedido(" + arrParams + ")'>Entregar pedido</button>";
        }

        sButtonFacturarLavanderia = '';
        if ( response[i].ID_Tipo_Documento == 2 ) {
          sButtonFacturarLavanderia = "<button type='button' class='btn btn-xs btn-link' alt='Facturar' title='Facturar' href='javascript:void(0)' onclick='facturarOrdenLavanderia(" + arrParams + ")'>Facturar</button>";
        }

        tr_body +=
        "<tr>"
          +"<td class='text-center'>" + response[i].Fe_Emision_Hora + "</td>"
          +"<td class='text-left'>" + response[i].No_Empleado + "</td>"
          +"<td class='text-center'>" + response[i].No_Tipo_Documento_Breve + "</td>"
          +"<td class='text-center'>" + response[i].ID_Serie_Documento + "</td>"
          +"<td class='text-center'>" + response[i].ID_Numero_Documento + "</td>"
          +"<td class='text-left'>" + response[i].No_Entidad + "</td>"
          +"<td class='text-right'>" + number_format(response[i].Ss_Tipo_Cambio, 3) + "</td>"
          +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_s, 2) + "</td>"
          +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(total_d, 2) + "</td>"
          +"<td class='text-right'>" + (response[i].ID_Tipo_Documento != 5 ? '' : '-') + number_format(fTotalSaldo, 2) + "</td>"
          +"<td class='text-center'><span class='label label-" + response[i].No_Class_Estado + "'>" + response[i].No_Estado + "</span></td>"
          +"<td class='text-center'>" + (response[i].Nu_Estado == 6 || response[i].Nu_Estado == 8 ? response[i].sAccionImprimir : '') + "</td>"
          +"<td class='text-center'>" + (response[i].Nu_Estado == 6 || response[i].Nu_Estado == 8 ? response[i].sAccionVer : '') + "</td>"
          +"<td class='text-center'>" + sButtonCobrarPedido + "</td>";
		      //if ( iIdTipoRubroEmpresaGlobal != 3 )
            //tr_body +="<td class='text-center'>" + sButtonCobrarPedido + "</td>";
          if ( iIdTipoRubroEmpresaGlobal == 3 )
            tr_body +="<td class='text-center'>" + sButtonEntregarPedido + "</td>";
          if ( iIdTipoRubroEmpresaGlobal == 3 )
            tr_body +="<td class='text-center'>" + sButtonFacturarLavanderia + "</td>"
        +"</tr>";
        
        sum_general_total_s += total_s;
        sum_general_total_d += total_d;
      }
      
      tr_foot =
      "<tfoot>"
        +"<tr>"
          +"<th class='text-right' colspan='7'>Total</th>"
          +"<th class='text-right'>" + number_format(sum_general_total_s, 2) + "</th>"
          +"<th class='text-right'>" + number_format(sum_general_total_d, 2) + "</th>"
          +"<th class='text-right'></th>"
        +"</tr>"
      +"</tfoot>";
    } else {
      if( response.sMessageSQL !== undefined ) {
        console.log(response.sMessageSQL);
      }
      tr_body +=
      "<tr>"
        +"<td colspan='13' class='text-center'>" + response.sMessage + "</td>"
      + "</tr>";
    } // ./ if arrData
    
    $( '#div-venta_punto_venta' ).show();
    $( '#table-venta_punto_venta > tbody' ).append(tr_body);
    $( '#table-venta_punto_venta > tbody' ).after(tr_foot);
    
    $( '#btn-html_venta_punto_venta' ).text('');
    $( '#btn-html_venta_punto_venta' ).append( '<i class="fa fa-search"></i> Buscar' );
    $( '#btn-html_venta_punto_venta' ).attr('disabled', false);
  }, 'JSON')
  .fail(function(jqXHR, textStatus, errorThrown) {
    $( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
    
    $( '#modal-message' ).modal('show');
    $( '.modal-message' ).addClass( 'modal-danger' );
    $( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
    setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
    
    //Message for developer
    console.log(jqXHR.responseText);
    
    $( '#btn-html_venta_punto_venta' ).text('');
    $( '#btn-html_venta_punto_venta' ).append( '<i class="fa fa-search"></i> Buscar' );
    $( '#btn-html_venta_punto_venta' ).attr('disabled', false);
  });
}

function cobrarPedido(arrParams){  
  $( '#form-cobrar_cliente' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();

  $( '.div-forma_pago').hide();
  $( '.div-modal_datos_tarjeta_credito').hide();
  $( '.div-estado_lavado_recepcion_cliente' ).hide();
  $( '.div-recibe_otra_persona' ).hide();

  $( '.modal-cobrar_cliente' ).modal('show');

  $( '[name="iIdDocumentoCabecera"]' ).val( arrParams.iIdDocumentoCabecera );
  $( '[name="iIdDocumentoMedioPago"]' ).val( arrParams.iIdDocumentoMedioPago );
  $( '#hidden-cobrar_cliente-fsaldo' ).val( arrParams.fTotalSaldo );

  $( '#modal-header-cobrar_cliente-title' ).text(arrParams.sTipoDocumento + ' - ' + arrParams.sSerieDocumento + ' - ' + arrParams.sNumeroDocumento);  
  $( '#cobrar_cliente-modal-body-cliente' ).text('Cliente: ' + arrParams.sCliente);
  $( '#cobrar_cliente-modal-body-saldo_cliente' ).text('Saldo: ' + arrParams.sSignoMoneda + ' ' + arrParams.fTotalSaldo);

  $( '.div-forma_pago').show();
  $( '.modal-cobrar_cliente' ).on('shown.bs.modal', function() {
    $( '[name="fPagoCliente"]' ).focus();
    $( '[name="fPagoCliente"]' ).val( arrParams.fTotalSaldo );
  });

  url = base_url + 'HelperController/getMediosPago';
  var arrPost = {
    iIdEmpresa : arrParams.iIdEmpresa,
  };
  $.post( url, arrPost, function( response ){
    $( '#cbo-modal_forma_pago' ).html('');
    for (var i = 0; i < response.length; i++) {
      if ( response[i].Nu_Tipo != 1 )
        $( '#cbo-modal_forma_pago' ).append( '<option value="' + response[i].ID_Medio_Pago + '" data-nu_tipo_medio_pago="' + response[i].Nu_Tipo + '">' + response[i].No_Medio_Pago + '</option>' );
    }
  }, 'JSON');
  
  // Modal de cobranza al cliente
  $( '#cbo-modal_forma_pago' ).change(function(){
    ID_Medio_Pago = $(this).val();
    Nu_Tipo_Medio_Pago = $(this).find(':selected').data('nu_tipo_medio_pago');
    $( '.div-modal_datos_tarjeta_credito').hide();
    $( '#cbo-cobrar_cliente-modal_tarjeta_credito' ).html('');
    $( '#tel-nu_referencia' ).val('');
    $( '#tel-nu_ultimo_4_digitos_tarjeta' ).val('');
    if (Nu_Tipo_Medio_Pago==2){
      $( '.div-modal_datos_tarjeta_credito').show();

      url = base_url + 'HelperController/getTiposTarjetaCredito';
      $.post( url, {ID_Medio_Pago : ID_Medio_Pago} , function( response ){
        $( '#cbo-cobrar_cliente-modal_tarjeta_credito' ).html('');
        for (var i = 0; i < response.length; i++)
          $( '#cbo-cobrar_cliente-modal_tarjeta_credito' ).append( '<option value="' + response[i].ID_Tipo_Medio_Pago + '">' + response[i].No_Tipo_Medio_Pago + '</option>' );
      }, 'JSON');
    }

    setTimeout(function(){ $( '[name="fPagoCliente"]' ).focus(); $( '[name="fPagoCliente"]' ).select(); }, 20);		
  })

  $( '#cbo-estado_lavado_recepcion_cliente' ).change(function(){
    $( '.div-estado_lavado_recepcion_cliente' ).hide();
    if($(this).val() == 3) {
      $( '.div-estado_lavado_recepcion_cliente' ).show();
    }
  })

  $( '#cbo-modal_quien_recibe' ).change(function(){
    $( '.div-recibe_otra_persona' ).hide();
    if($(this).val() == 0) {
      $( '.div-recibe_otra_persona' ).show();
      $( '[name="sNombreRecepcion"]' ).focus();
    }
  })
}

function entregarPedido(arrParams){
  $( '#form-entregar_pedido' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();

  $( '.div-forma_pago').hide();
  $( '.div-modal_datos_tarjeta_credito').hide();
  $( '.div-estado_lavado_recepcion_cliente' ).hide();
  $( '.div-recibe_otra_persona' ).hide();

  $( '.modal-entregar_pedido' ).modal('show');

  $( '[name="iIdDocumentoCabecera"]' ).val( arrParams.iIdDocumentoCabecera );
  $( '[name="iIdDocumentoMedioPago"]' ).val( arrParams.iIdDocumentoMedioPago );
  $( '#hidden-entregar_pedido-fsaldo' ).val( arrParams.fTotalSaldo );

  $( '#modal-header-entregar_pedido-title' ).text(arrParams.sTipoDocumento + ' - ' + arrParams.sSerieDocumento + ' - ' + arrParams.sNumeroDocumento);  
  $( '#entregar_pedido-modal-body-cliente' ).text('Cliente: ' + arrParams.sCliente);
  $( '#entregar_pedido-modal-body-saldo_cliente' ).text('Saldo: ' + arrParams.sSignoMoneda + ' ' + arrParams.fTotalSaldo);

  if ( arrParams.fTotalSaldo > 0 ) {
    $( '.div-forma_pago').show();
    $( '.modal-entregar_pedido' ).on('shown.bs.modal', function() {
      $( '[name="fPagoCliente"]' ).focus();
      $( '[name="fPagoCliente"]' ).val( arrParams.fTotalSaldo );
    });

    url = base_url + 'HelperController/getMediosPago';
    var arrPost = {
      iIdEmpresa : arrParams.iIdEmpresa,
    };
    $.post( url, arrPost, function( response ){
      $( '#cbo-modal_forma_pago_entrega_pedido' ).html('');
      for (var i = 0; i < response.length; i++) {
        if (  response[i].Nu_Tipo != 1 ) {
          $( '#cbo-modal_forma_pago_entrega_pedido' ).append( '<option value="' + response[i].ID_Medio_Pago + '" data-nu_tipo_medio_pago="' + response[i].Nu_Tipo + '">' + response[i].No_Medio_Pago + '</option>' );
        }
      }
    }, 'JSON');
    
    // Modal de cobranza al cliente
    $( '#cbo-modal_forma_pago_entrega_pedido' ).change(function(){
      ID_Medio_Pago = $(this).val();
      Nu_Tipo_Medio_Pago = $(this).find(':selected').data('nu_tipo_medio_pago');
      $( '.div-modal_datos_tarjeta_credito').hide();
      $( '#cbo-entregar_pedido-modal_tarjeta_credito' ).html('');
      $( '#tel-nu_referencia' ).val('');
      $( '#tel-nu_ultimo_4_digitos_tarjeta' ).val('');
      if (Nu_Tipo_Medio_Pago==2){
        $( '.div-modal_datos_tarjeta_credito').show();

        url = base_url + 'HelperController/getTiposTarjetaCredito';
        $.post( url, {ID_Medio_Pago : ID_Medio_Pago} , function( response ){
          $( '#cbo-entregar_pedido-modal_tarjeta_credito' ).html('');
          for (var i = 0; i < response.length; i++)
            $( '#cbo-entregar_pedido-modal_tarjeta_credito' ).append( '<option value="' + response[i].ID_Tipo_Medio_Pago + '">' + response[i].No_Tipo_Medio_Pago + '</option>' );
        }, 'JSON');
      }

      setTimeout(function(){ $( '[name="fPagoCliente"]' ).focus(); $( '[name="fPagoCliente"]' ).select(); }, 20);		
    })
  }// /. if saldo > 0

  $( '#cbo-estado_lavado_recepcion_cliente' ).change(function(){
    $( '.div-estado_lavado_recepcion_cliente' ).hide();
    if($(this).val() == 3) {
      $( '.div-estado_lavado_recepcion_cliente' ).show();
    }
  })

  $( '#cbo-modal_quien_recibe' ).change(function(){
    $( '.div-recibe_otra_persona' ).hide();
    if($(this).val() == 0) {
      $( '.div-recibe_otra_persona' ).show();
      $( '[name="sNombreRecepcion"]' ).focus();
    }
  })
}

function facturarOrdenLavanderia(arrParams){
  $( '#form-facturar_orden_lavanderia' )[0].reset();
  $( '.form-group' ).removeClass('has-error');
  $( '.form-group' ).removeClass('has-success');
  $( '.help-block' ).empty();

  $( '.modal-facturar_orden_lavanderia' ).modal('show');

  $( '[name="iIdDocumentoCabecera"]' ).val( arrParams.iIdDocumentoCabecera );
  $( '[name="iIdDocumentoMedioPago"]' ).val( arrParams.iIdDocumentoMedioPago );
  $( '#hidden-facturar_oden_lavanderia-iEstadoLavadoRecepcionCliente' ).val( arrParams.iEstadoLavadoRecepcionCliente );

  $( '#modal-header-facturar_orden_lavanderia-title' ).text(arrParams.sTipoDocumento + ' - ' + arrParams.sSerieDocumento + ' - ' + arrParams.sNumeroDocumento);  
  $( '#facturar_orden_lavanderia-modal-body-cliente' ).text('Cliente: ' + arrParams.sCliente);
}

// Funciones para LAE API
function api_sunat_reniec(n){
	var iIdTipoDocumento = $( '#modal-cbo-tipo_documento' ).val();
	var iCantidadCaracteresIngresados = n.length;
	var iNumeroDocumentoIdentidad = parseFloat( $( '#txt-ACodigo' ).val() );
	if ( iIdTipoDocumento == 2 || iIdTipoDocumento == 4 ) {
		getDatosxDNI(iIdTipoDocumento, iCantidadCaracteresIngresados, 8, iNumeroDocumentoIdentidad);
	} else if ( iIdTipoDocumento == 2 || iIdTipoDocumento == 3 ) {
		getDatosxRUC(iIdTipoDocumento, iCantidadCaracteresIngresados, 11, iNumeroDocumentoIdentidad);
	}
}

function getDatosxDNI(iIdTipoDocumento, iCantidadCaracteresIngresados, iCantidadCaracteres, iNumeroDocumentoIdentidad){
	if( iCantidadCaracteresIngresados == iCantidadCaracteres && isNaN(iNumeroDocumentoIdentidad) == false && $( '#txt-ACodigo' ).val() != $( '#hidden-nu_numero_documento_identidad' ).val() ) {//Si cumple con los caracteres para DNI / RUC
		var arrPost = {
			sNumeroDocumentoIdentidad : $( '#txt-ACodigo' ).val(),
		};
		$('#span-no_nombres_cargando').html('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');
		
		$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
		$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
		// Consulta Cliente en BD local
		$.post( base_url + 'AutocompleteController/getClienteEspecifico', arrPost, function( response ){
			$('#txt-ANombre').val('');
			$('#label-txt_estado_cliente').text('');

			$('#txt-Nu_Celular_Entidad_Cliente').val( '' );
			$('#txt-Txt_Email_Entidad_Cliente').val( '' );
			$('#span-celular').hide();
			$('#span-email').hide();

			if ( response.sStatus=='success' ) {
				$('#span-no_nombres_cargando').html('');
				$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

				var arrData = response.arrData;
				
				$('[name="AID"]').val( arrData[0].ID );
				$('[name="No_Entidad"]').val( arrData[0].Nombre );
				$('[name="Txt_Direccion_Entidad"]').val( arrData[0].Txt_Direccion_Entidad );

				$('#txt-ANombre').val(arrData[0].Nombre);
				$('#label-txt_direccion').text(arrData[0].Txt_Direccion_Entidad);
				$('#label-txt_estado_cliente').text('Existe en B.D. local');
				
				$('#hidden-estado_entidad').val(arrData[0].Nu_Estado);

				$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
				$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
			} else if ( response.sStatus=='warning' ) {// Si no existe en nuestra BD local, consultamos en el LAE API V1
				// Consulta LAE API V1 - RENIEC / SUNAT
				$( '#txt-AID' ).val('');
				$( '#txt-ANombre' ).val('');
				$( '#txt-Txt_Direccion_Entidad' ).val('');
				
				var url_api = 'https://www.laesystems.com/librerias/reniec/partner/format/json/x-api-key/';
				url_api = url_api + sTokenGlobal;
				
				var data = {
					ID_Tipo_Documento_Identidad : 2,
					Nu_Documento_Identidad : $( '#txt-ACodigo' ).val(),
				};
		
				$.ajax({
					url   : url_api,
					type  : 'POST',
					data  : data,
					success: function(response){
						$('#span-no_nombres_cargando').html('');
						$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

						if (response.success == true){
							$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
							$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');

							$('[name="No_Entidad"]').val( response.data.No_Names );
							$('#txt-ANombre').val(response.data.No_Names);

							$('#label-txt_direccion').text('');
							$('#label-txt_estado_cliente').text('Nube');
							
							$('#hidden-estado_entidad').val(response.data.Nu_Status);
						} else {
							$('[name="No_Entidad"]').val( '' );
							$('[name="Txt_Direccion_Entidad"]').val( '' );
			
							$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( response.msg );
							$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
							
							$( '#txt-ACodigo' ).focus();
							$( '#txt-ACodigo' ).select();
		
							$('#txt-ANombre').val('');
							$('#label-txt_direccion').text('');
							$('#label-txt_estado_cliente').text('');

							$('#hidden-estado_entidad').val(0);
						}
					},
					error: function(response){
						$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

						$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( 'Sin acceso' );
						$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
						
						$( '[name="No_Entidad"]' ).val( '' );
						$( '[name="Txt_Direccion_Entidad"]' ).val( '' );
		
						$('#span-no_nombres_cargando').html('');
						$('#txt-ANombre').val('');
						$('#label-txt_direccion').text('');
						$('#label-txt_estado_cliente').text('');

						$('#hidden-estado_entidad').val(0);
					}
				});
				// ./ Consulta LAE API V1 - RENIEC / SUNAT
			} else {
				$('#label-txt_direccion').text(response.sMessage);
			}
		}, 'JSON')
		// ./ Consulta de Cliente en BD local
	}
}

function getDatosxRUC(iIdTipoDocumento, iCantidadCaracteresIngresados, iCantidadCaracteres, iNumeroDocumentoIdentidad){
	if( iCantidadCaracteresIngresados == iCantidadCaracteres && isNaN(iNumeroDocumentoIdentidad) == false && $( '#txt-ACodigo' ).val() != $( '#hidden-nu_numero_documento_identidad' ).val() ) {//Si cumple con los caracteres para DNI / RUC
		var arrPost = {
			sNumeroDocumentoIdentidad : $( '#txt-ACodigo' ).val(),
		};
		$('#span-no_nombres_cargando').html('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

		$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
		$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
		// Consulta Cliente en BD local
		$.post( base_url + 'AutocompleteController/getClienteEspecifico', arrPost, function( response ){
			$('#txt-ANombre').val('');
			$('#label-txt_estado_cliente').text('');

			$('#txt-Nu_Celular_Entidad_Cliente').val( '' );
			$('#txt-Txt_Email_Entidad_Cliente').val( '' );
			$('#span-celular').hide();
			$('#span-email').hide();

			if ( response.sStatus=='success' ) {
				$('#span-no_nombres_cargando').html('');
				$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

				var arrData = response.arrData;
				
				$('[name="AID"]').val( arrData[0].ID );
				$('[name="No_Entidad"]').val( arrData[0].Nombre );
				$('[name="Txt_Direccion_Entidad"]').val( arrData[0].Txt_Direccion_Entidad );

				$('#txt-ANombre').val(arrData[0].Nombre);
				$('#label-txt_direccion').text(arrData[0].Txt_Direccion_Entidad);
				$('#label-txt_estado_cliente').text('Existe en B.D. local');
				
				$('#hidden-estado_entidad').val(arrData[0].Nu_Estado);

				$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
				$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
			} else if ( response.sStatus=='warning' ) {// Si no existe en nuestra BD local, consultamos en el LAE API V1
				// Consulta LAE API V1 - RENIEC / SUNAT
				$( '#txt-AID' ).val('');
				$( '#txt-ANombre' ).val('');
				$( '#txt-Txt_Direccion_Entidad' ).val('');
				
				var url_api = 'https://www.laesystems.com/librerias/sunat/partner/format/json/x-api-key/';
				url_api = url_api + sTokenGlobal;
				
				var data = {
					ID_Tipo_Documento_Identidad : 4,
					Nu_Documento_Identidad : $( '#txt-ACodigo' ).val(),
				};
		
				$.ajax({
					url   : url_api,
					type  : 'POST',
					data  : data,
					success: function(response){
						$('#span-no_nombres_cargando').html('');
						$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

						if (response.success == true){
							$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
							$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');

							$('[name="No_Entidad"]').val( response.data.No_Names );
							$('#txt-ANombre').val(response.data.No_Names);

							$('[name="Txt_Direccion_Entidad"]').val( response.data.Txt_Address );
							$('#label-txt_direccion').text(response.data.Txt_Address);
							
							$('#label-txt_estado_cliente').text('Nube');
							
							$('#hidden-estado_entidad').val(response.data.Nu_Status);
						} else {
							$('[name="No_Entidad"]').val( '' );
							$('[name="Txt_Direccion_Entidad"]').val( '' );
			
							$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( response.msg );
							$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
							
							$( '#txt-ACodigo' ).focus();
							$( '#txt-ACodigo' ).select();
		
							$('#txt-ANombre').val('');
							$('#label-txt_direccion').text('');
							$('#label-txt_estado_cliente').text('');

							$('#hidden-estado_entidad').val(0);
						}
					},
					error: function(response){
						$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

						$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( 'Sin acceso' );
						$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
						
						$( '[name="No_Entidad"]' ).val( '' );
						$( '[name="Txt_Direccion_Entidad"]' ).val( '' );
		
						$('#span-no_nombres_cargando').html('');
						$('#txt-ANombre').val('');
						$('#label-txt_direccion').text('');
						$('#label-txt_estado_cliente').text('');

						$('#hidden-estado_entidad').val(0);
					}
				});
    			return;
				// ./ Consulta LAE API V1 - RENIEC / SUNAT
			} else {
				$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( response.sMessage );
				$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
			}
		}, 'JSON')
		// ./ Consulta de Cliente en BD local
	}
}