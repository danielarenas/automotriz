var url, arrItemVentaTemporal, global_class_method, global_table, filter_lista, $modal_msg_stock, ID_Item, fila, precio, cantidad, tr_body_table_detalle_productos_pos='', $fPrecioItem=0.00, $Sum_Ss_Total_POS=0.00, $fDescuentoTotalItem=0.00;

var fToday = new Date(), // Date today
fYear = fToday.getFullYear(),
fMonth = fToday.getMonth() + 1,//hoy es 0!
fDay = fToday.getDate();
// /. Variables Globales

$(function () {
	$( '.select2' ).select2();
	$( '[data-mask]' ).inputmask();

	$( '#txt-No_Producto' ).focus();

	// crear item modal
	url = base_url + 'HelperController/getTiposExistenciaProducto';
	$.post( url , function( responseTiposExistenciaProducto ){
		$( '#cbo-modal-tipoItem' ).html( '' );
		for (var i = 0; i < responseTiposExistenciaProducto.length; i++)
		$( '#cbo-modal-tipoItem' ).append( '<option value="' + responseTiposExistenciaProducto[i].ID_Tipo_Producto + '">' + responseTiposExistenciaProducto[i].No_Tipo_Producto + '</option>' );
	}, 'JSON');

	url = base_url + 'HelperController/getImpuestos';
	$.post( url , function( response ){
	  $( '#cbo-modal-impuestoItem' ).html( '' );
	  for (var i = 0; i < response.length; i++)
		$( '#cbo-modal-impuestoItem' ).append( '<option value="' + response[i].ID_Impuesto + '" data-ss_impuesto="' + response[i].Ss_Impuesto + '" data-nu_tipo_impuesto="' + response[i].Nu_Tipo_Impuesto + '">' + response[i].No_Impuesto + '</option>' );
	}, 'JSON');
	
	url = base_url + 'HelperController/getUnidadesMedida';
	$.post( url , function( responseUnidadMedidas ){
	  $( '#cbo-modal-unidad_medidaItem' ).html( '' );
	  for (var i = 0; i < responseUnidadMedidas.length; i++)
		$( '#cbo-modal-unidad_medidaItem' ).append( '<option value="' + responseUnidadMedidas[i].ID_Unidad_Medida + '">' + responseUnidadMedidas[i].No_Unidad_Medida + '</option>' );
	  $( '#modal-loader' ).modal('hide');
	}, 'JSON');
	// ./ crear item modal

	$( '#btn-crear_item' ).click(function(){
		$( '.modal-crear_item' ).modal('show');
		$( '#form-crear_item' )[0].reset();
	})

	// Mostrar tipo de producto para inventarios
	$( '.div-Producto' ).show();
	$( '#cbo-modal-grupoItem' ).change(function(){
  	$( '.div-Producto' ).show();
	  if ( $(this).val() == 0 ){//Servicio
    	$( '.div-Producto' ).hide();
	  }
	})
	
	$( '#btn-modal-crear_item' ).off('click').click(function () {
		$( '.help-block' ).empty();
		var fPrecio = parseFloat($( '#txt-modal-precioItem' ).val());
		if ($( '#txt-modal-upcItem' ).val().length === 0){
			$( '#txt-modal-upcItem' ).closest('.form-group').find('.help-block').html('Ingresar datos');
			$( '#txt-modal-upcItem' ).closest('.form-group').removeClass('has-success').addClass('has-error');
		} else if ($( '[name="textarea-modal-nombreItem"]' ).val().length === 0){
			$( '[name="textarea-modal-nombreItem"]' ).closest('.form-group').find('.help-block').html('Ingresar datos');
			$( '[name="textarea-modal-nombreItem"]' ).closest('.form-group').removeClass('has-success').addClass('has-error');
		} else if ( fPrecio == 0.00 || isNaN(fPrecio) ){
			$( '#txt-modal-precioItem' ).closest('.form-group').find('.help-block').html('Ingresar precio');
			$( '#txt-modal-precioItem' ).closest('.form-group').removeClass('has-success').addClass('has-error');
		} else {
			crearItemModal();
		}
	});

	// Valores inicio
	$( '.div-nuevo_cliente' ).hide();
	$( '#txt-ID_Tipo_Documento_Identidad' ).val(2);
	$( '#btn-pagar' ).prop('disabled', true);
	$( '#cbo-tipo_envio_lavado' ).prop('disabled', true);
	$( '#txt-fe_entrega' ).prop('disabled', true);
	$( '#cbo-recepcion' ).prop('disabled', true);

	$( "#txt-Nu_Celular_Entidad_Cliente" ).blur(function() {
		validarNumeroCelular($(this).val(), '#span-celular');
	})

	$( "#txt-Txt_Email_Entidad_Cliente" ).blur(function() {
		caracteresCorreoValido($(this).val(), '#span-email');
	})

	// Combinacion de teclas
	// Cancelar Venta
	$(document).bind('keydown', 'esc', function(){
		limpiarValoresVenta();
	});

	$('input.hotkey-cancelar_venta').bind('keydown', 'esc', function(){
		limpiarValoresVenta();
	});
	// ./ Cancelar Venta

	// Limpiar item
	$(document).bind('keydown', 'F2', function(){
		$( '#txt-ID_Producto' ).val( '' );
		$( '#txt-No_Producto' ).val( '' );
	});

	$('input.hotkey-limpiar_item').bind('keydown', 'F2', function(){
		$( '#txt-ID_Producto' ).val( '' );
		$( '#txt-No_Producto' ).val( '' );
	});
	// ./ Limpiar item

	// Focus item
	$(document).bind('keydown', 'F4', function(){
		$( '#txt-No_Producto' ).focus();
	});
	// ./ Focus item

	// Button cobrar
	$(document).bind('keydown', 'return', function(){
		if ( $('#btn-pagar').prop('disabled') == false ){
			cobrarCliente();
		}
	});

	$('input.autocompletar_lector_codigo_barra').bind('keydown', 'return', function(){
		if ( $('#btn-pagar').prop('disabled') == false ){
			cobrarCliente();
		}
	});

	$('input.input-codigo_barra').bind('keydown', 'return', function(){
		if ( $('#btn-pagar').prop('disabled') == false ){
			cobrarCliente();
		}
	});

	$('input.hotkey-cobrar_cliente').bind('keydown', 'return', function(){
		if ( $('#btn-pagar').prop('disabled') == false ){
			cobrarCliente();
		}
	});

	$('select.hotkey-cobrar_cliente').bind('keydown', 'return', function(){
		if ( $('#btn-pagar').prop('disabled') == false ){
			cobrarCliente();
		}
	});
	// ./ Button cobrar
	
	// Button add forma de pago y generar ticket
	$(document).bind('keydown', 'return', function(){
		$Sum_Ss_Monto_Total = 0.00;
		$("#table-modal_forma_pago > tbody > tr").each(function(){
			fila = $(this);
			$Sum_Ss_Monto_Total += parseFloat(fila.find(".fTotal").text());
		});
	});
	// ./ Button add forma de pago y generar ticket
	// ./ Combinacion de teclas

	// MODAL Precargar datos de cobranza de cliente
	$( '.div-billete_soles' ).show();
	$( '.div-modal_datos_tarjeta_credito' ).hide();
	$( '#div-modal_forma_pago' ).hide();

	url = base_url + 'HelperController/getDataGeneral';
	var arrPost = {
		sTipoData : 'entidad',
		iTipoEntidad : '6',
	};
	$.post( url, arrPost, function( response ){
		if ( response.sStatus == 'success' ) {
			var l = response.arrData.length;
			if (l==1) {
				$( '#cbo-transporte' ).html( '<option value="' + response.arrData[0].ID + '">' + response.arrData[0].Nombre + '</option>' );
			} else {
				$( '#cbo-transporte' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
				for (var x = 0; x < l; x++) {
					$( '#cbo-transporte' ).append( '<option value="' + response.arrData[x].ID + '">' + response.arrData[x].Nombre + '</option>' );
				}
			}
		} else {
			if( response.sMessageSQL !== undefined ) {
				console.log(response.sMessageSQL);
			}
			console.log(response.sMessage);
		}
	}, 'JSON');
	
	//Cargar token dni y ruc, fecha de inicio de sistema
	$.post( base_url + 'HelperController/getToken', function( response ){
		iIdTipoRubroEmpresaGlobal = response.Nu_Tipo_Rubro_Empresa;
		//Verificar tipo de cliente
		if ( iIdTipoRubroEmpresaGlobal == 3 ) {
			$( '.div-nuevo_cliente' ).show();
		}
	}, 'JSON');
	// /. Cargar token dni y ruc, fecha de inicio de sistema

	url = base_url + 'HelperController/getMediosPago';
	var arrPost = {
	  iIdEmpresa : $( '#cbo-Empresas' ).val(),
	};
	$.post( url, arrPost, function( response ){
	  	$( '#cbo-modal_forma_pago' ).html('');
	  	for (var i = 0; i < response.length; i++)
			$( '#cbo-modal_forma_pago' ).append( '<option value="' + response[i].ID_Medio_Pago + '" data-nu_tipo_medio_pago="' + response[i].Nu_Tipo + '">' + response[i].No_Medio_Pago + '</option>' );
	}, 'JSON');
	// ./ MODAL Precargar datos de cobranza de cliente

	url = base_url + 'HelperController/getAlmacenes';
	var arrParams = {
	  iIdOrganizacion : $('#header-a-id_organizacion').val(),
	};
	$.post( url, arrParams, function( response ){
	    if ( response.length == 1 ) {
			$( '#cbo-almacen' ).html( '<option value="' + response[0].ID_Almacen + '">' + response[0].No_Almacen + '</option>' );
		} else {
			$( '#cbo-almacen' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
			for (var i = 0; i < response.length; i++)
				$( '#cbo-almacen' ).append( '<option value="' + response[i].ID_Almacen + '">' + response[i].No_Almacen + '</option>' );
		}
	}, 'JSON');

	url = base_url + 'HelperController/getListaPrecio';
	$.post( url, {Nu_Tipo_Lista_Precio : 1, ID_Organizacion: $('#header-a-id_organizacion').val(), ID_Almacen : $( '#hidden-id_almacen' ).val()}, function( responseLista ){
		if (responseLista.length==1) {
			$( '#cbo-lista_precios' ).html( '<option value="' + responseLista[0].ID_Lista_Precio_Cabecera + '">' + responseLista[0].No_Lista_Precio + '</option>' );
			
			var arrParams={
				iIdListaPrecio : responseLista[0].ID_Lista_Precio_Cabecera,
			};
			getItems(arrParams);
		} else {
			$( '#cbo-lista_precios' ).html('<option value="0" selected="selected">- Seleccionar -</option>');
			for (var i = 0; i < responseLista.length; i++)
				$( '#cbo-lista_precios' ).append( '<option value="' + responseLista[i].ID_Lista_Precio_Cabecera + '">' + responseLista[i].No_Lista_Precio + '</option>' );
		}
	}, 'JSON');

  	//Global autocomplete Producto
  	$( '.autocompletar_lector_codigo_barra' ).autoComplete({
		minChars: 0,
		tabDisabled:false,
		source: function(term, response){
			global_class_method = $( '.autocompletar_lector_codigo_barra' ).data('global-class_method');
			global_table = $( '.autocompletar_lector_codigo_barra' ).data('global-table');

			var send_post = {
				global_table : global_table,
				global_search : term,
				filter_id_almacen : '',
				filter_nu_compuesto : '',
				filter_nu_tipo_producto : 2,//2=Producto
				filter_lista : $( '#cbo-lista_precios' ).val(),
			}

			$.post( base_url + global_class_method, send_post, function( arrData ){
				response(arrData);
			}, 'JSON');
		},
		renderItem: function (item, search){
			search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
			var Ss_Precio = 0;
			if ( (item.Ss_Precio === null || item.Ss_Precio == 0.000000) && (item.Ss_Precio_Item !== null || item.Ss_Precio_Item != 0.000000) )
			  Ss_Precio = item.Ss_Precio_Item;
			if ( item.Ss_Precio !== null )
			  Ss_Precio = item.Ss_Precio;

			//console.log(search.length);
			/*
			if ( search.length==13 ) { //Si cumple con los caracteres para CODIGO BARRA 13 dígitos
				arrItemVentaTemporal={
					iIdItem:item.ID,
					iCodigoItem:item.Codigo,
					sNombreItem:item.Nombre,
					qItem:item.Qt_Producto,
					fPrecio:Ss_Precio,
					iIdImpuestoCruceDocumento:item.ID_Impuesto_Cruce_Documento,
					iTipoImpuesto:item.Nu_Tipo_Impuesto,
					fImpuesto:item.Ss_Impuesto,
					sComposicion:item.Txt_Composicion,
				}
				agregarItemVentaTemporal(arrItemVentaTemporal);
				
				if ( iIdTipoRubroEmpresaGlobal == 1 ) {// 1 = Farmacia
					$( '#table-items_alternativos tbody' ).empty();

					if ( Txt_Composicion != '' && Txt_Composicion != null ){
						arrDataAlternativos={
							iIdListaPrecio : $( '#cbo-lista_precios' ).val(),
							iIdItem:ID,
							sComposicion:Txt_Composicion,
						}
						buscarItemAlternativos(arrDataAlternativos);
					}
				}

				return '';
			} else {
			*/
				var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");	
				return '<div class="autocomplete-suggestion" data-id="'+item.ID+'" data-nu_tipo_item="'+item.Nu_Tipo_Producto+'" data-codigo="'+item.Codigo+'" data-nombre="'+item.Nombre+'" data-precio="'+Ss_Precio+'" data-precio_interno="'+item.Ss_Precio_Interno+'" data-nu_tipo_impuesto="'+item.Nu_Tipo_Impuesto+'" data-id_impuesto_cruce_documento="'+item.ID_Impuesto_Cruce_Documento+'" data-ss_impuesto="'+item.Ss_Impuesto+'" data-qt_producto="'+item.Qt_Producto+'" data-txt_composicion="'+item.Txt_Composicion+'" data-val="'+search+'">' + '[' + (item.No_Marca !== null ? item.No_Marca : 'Sin marca') + '] ' + item.Nombre.replace(re, "<b>$1</b>") + ' / Precio: ' + Ss_Precio + ' / stock: ' + (isNaN(parseFloat(item.Qt_Producto)) ? 0 : parseFloat(item.Qt_Producto)) + '</div>';
			//}
		},
		onSelect: function(e, term, item){
			$( '#txt-ID_Producto' ).val(item.data('id'));
			$( '#txt-Nu_Codigo_Barra' ).val(item.data('codigo'));
			$( '#txt-No_Producto' ).val(item.data('nombre'));
			$( '#txt-Qt_Producto' ).val(item.data('qt_producto'));
			$( '#txt-Ss_Precio_Interno' ).val(item.data('precio_interno'));
			$( '#txt-Ss_Precio' ).val(item.data('precio'));
			$( '#txt-ID_Impuesto_Cruce_Documento' ).val(item.data('id_impuesto_cruce_documento'));
			$( '#txt-Nu_Tipo_Impuesto' ).val(item.data('nu_tipo_impuesto'));
			$( '#txt-Ss_Impuesto' ).val(item.data('ss_impuesto'));
			$( '#txt-Ss_Impuesto' ).val(item.data('ss_impuesto'));
			$( '#txt-nu_tipo_item' ).val(item.data('nu_tipo_item'));

			arrItemVentaTemporal={
				iIdItem:item.data('id'),
				iCodigoItem:item.data('codigo'),
				sNombreItem:item.data('nombre'),
				qItem:item.data('qt_producto'),
				fPrecio:item.data('precio'),
				iIdImpuestoCruceDocumento:item.data('id_impuesto_cruce_documento'),
				iTipoImpuesto:item.data('nu_tipo_impuesto'),
				fImpuesto:item.data('ss_impuesto'),
			}
			agregarItemVentaTemporal(arrItemVentaTemporal);
			
			$( '#table-items_alternativos tbody' ).empty();

			if ( iIdTipoRubroEmpresaGlobal == 1 ) {// 1 = Farmacia
				if ( item.data('txt_composicion') != '' && item.data('txt_composicion') != null ){
					arrDataAlternativos={
						iIdListaPrecio : $( '#cbo-lista_precios' ).val(),
						iIdItem:item.data('id'),
						sComposicion:item.data('txt_composicion'),
					}
					buscarItemAlternativos(arrDataAlternativos);
				}
			}
    	}// ./ OnSelect Item
	});// ./ Autocomplet de item

	$(".div-lista_cuadro_items").on("click", ".li-item_pos", function(event){
		buscarItem( $(this).val() );
	});
	// ./ Lateral Izquierdo

	// Lateral Derecho
  	// Tipo de cliente
	$( '#cbo-tipo_cliente' ).change(function(){
		$( '.div-nuevo_cliente' ).hide();
	
		$('#txt-ACodigo').val( '' );
		$('#txt-Nu_Celular_Entidad_Cliente').val( '' );
		$('#txt-Txt_Email_Entidad_Cliente').val( '' );
	
		$('#span-celular').hide();
		$('#span-email').hide();
	
		$('#txt-ANombre').val( '' );
		$('#label-txt_direccion').text( '' );
		$('#label-txt_estado_cliente').text( '' );
		
		$( '#cbo-tipo_documento' ).val('4');
	
		if ( $(this).val()==3 ) {//3=Cliente Nuevo
			$( '.div-nuevo_cliente' ).show();
			setTimeout(function(){ $('#txt-ACodigo').focus(); }, 20);
		}
		
		$( '.help-block' ).empty();
  	});

	// Tipo Documento Identidad
	$( '#cbo-tipo_documento' ).change(function(){
		$( '#label_correo' ).show();
		$( '#txt-Txt_Email_Entidad_Cliente' ).show();

		$('#txt-AID').val('');
		$('#txt-ACodigo').val( '' );
		$('#txt-Nu_Celular_Entidad_Cliente').val( '' );
		$('#txt-Txt_Email_Entidad_Cliente').val('');
		$('#txt-ANombre').val('');
		$('#span-no_nombres_cargando').html('');

	
	  	if ( $(this).val()==4) {//Boleta
			$( '#label-tipo_documento_identidad' ).text('DNI');
			$( '#txt-ID_Tipo_Documento_Identidad' ).val(2);//DNI
			$( '#txt-ACodigo' ).attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
			$( '#txt-ACodigo' ).attr('maxlength', 15);
		} else if ( $(this).val()==3) {//Factura
			$( '#label-tipo_documento_identidad' ).text('RUC');
			$( '#txt-ID_Tipo_Documento_Identidad' ).val(4);//RUC
			$( '#txt-ACodigo' ).attr('maxlength', $(this).find(':selected').data('nu_cantidad_caracteres'));
		} else {
			$( '#txt-ID_Tipo_Documento_Identidad' ).val(1);//RUC
			$( '#label-tipo_documento_identidad' ).text('OTROS');
			$( '#txt-ACodigo' ).attr('maxlength', 15);
			$( '#label_correo' ).hide();
			$( '#txt-Txt_Email_Entidad_Cliente' ).hide();
		}
	})
  
 	// Lista de productos a comprar
  	$( '#table-detalle_productos_pos tbody' ).on('input', '.txt-Qt_Producto', function(){
		calcularImportexItem($(this).parents("tr"));
  	})

  	$( '#table-detalle_productos_pos tbody' ).on('input', '.input-fDescuentoItem', function(){
		calcularImportexItem($(this).parents("tr"));
	})
	  
	$( '#table-detalle_productos_pos tbody' ).on('click', '#btn-delete_producto_pos', function(){
    	$(this).closest('tr').remove();
    
		calcularTotales();

		$( '#btn-pagar' ).prop('disabled', false);
		$( '#cbo-tipo_envio_lavado' ).prop('disabled', false);
		$( '#txt-fe_entrega' ).prop('disabled', false);
		$( '#cbo-recepcion' ).prop('disabled', false);
		if ($( '#table-detalle_productos_pos > tbody > tr' ).length == 0){
			$( '#table-detalle_productos_pos' ).hide();
			$( '#btn-pagar' ).prop('disabled', true);
			$( '#cbo-tipo_envio_lavado' ).prop('disabled', true);
			$( '#txt-fe_entrega' ).prop('disabled', true);
			$( '#cbo-recepcion' ).prop('disabled', true);
			$( '#cbo-descuento' ).attr('disabled', false);
		}
	})
	
	$( '#table-detalle_productos_pos tbody' ).on('click', '#btn-ver_producto_pos', function(){
		var arrParams = {
			'sTipoData' : 'item',
			'iIdItem' : $(this).data('id_item'),
			'iIdListaPrecioCabecera' : $('#cbo-lista_precios').val(),
		};
		url = base_url + 'HelperController/getDataGeneral';
		$.post( url, arrParams, function( arrResponse ){
			$( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');

			if (arrResponse.sStatus == 'success') {
				$( '#modal-table-info_item' ).remove();

				$( '.modal-info_item' ).modal('show');

				var arrData = arrResponse.arrData[0], Ss_Precio = 0;
				$( '#modal-header-info_item-title' ).html( arrData.No_Producto );

				//obtener composicion por item
				var sHtmlTableInfoItem = '';
				var arrParams = {
					'sTipoData' : 'composicion',
					'sWhereIdComposicion' : arrData.Txt_Composicion,
				};
				url = base_url + 'HelperController/getDataGeneral';
				$.post( url, arrParams, function( arrResponseComposicion ){
					if ( arrResponseComposicion.sStatus == 'success' || arrResponseComposicion.sStatus == 'warning' ) {
						var Txt_Composicion = '';
						if ( arrData.Txt_Composicion.length > 0 && arrResponseComposicion.sStatus == 'success' ) {
							var iCantidadRegistros = arrResponseComposicion.arrData.length, Txt_Composicion = '';
							if ( iCantidadRegistros > 1 ) {
								for (var i=0; i < iCantidadRegistros; i++) {
									Txt_Composicion += arrResponseComposicion.arrData[i].Nombre + ', ';
								}
								Txt_Composicion = Txt_Composicion.slice(0);
							} else {
								Txt_Composicion += arrResponseComposicion.arrData[0].Nombre;
							}
						}
						Ss_Precio = 0;
						if ( arrData.Ss_Precio_Item !== null || arrData.Ss_Precio_Item != 0.000000 )
						  Ss_Precio = arrData.Ss_Precio_Item;
						if ( (arrData.Ss_Precio_Item === null || arrData.Ss_Precio_Item == 0.000000) && arrData.Ss_Precio !== null )
						  Ss_Precio = arrData.Ss_Precio;
						sHtmlTableInfoItem +=
						'<table id="modal-table-info_item" class="table table-hover">'+
							'<tbody>'+
								'<tr>'+
									'<td class="text-left" style="width: 25%"><b>Precio</b></td>'+
									'<td class="text-left" style="width: 75%">' + parseFloat(Ss_Precio).toFixed(2) + '</td>'+
								'</tr>';
								if (iIdTipoRubroEmpresaGlobal != 3) {//3=Lavandería
								sHtmlTableInfoItem +=
								'<tr>'+
									'<td class="text-left" style="width: 25%"><b>Stock Mínimo</b></td>'+
									'<td class="text-left" style="width: 75%">' + (arrData.Nu_Stock_Minimo != null ? arrData.Nu_Stock_Minimo : '') + '</td>'+
								'</tr>'+
								'<tr>'+
									'<td class="text-left" style="width: 25%"><b>Stock Actual</b></td>'+
									'<td class="text-left" style="width: 75%">' + (arrData.Qt_Producto != null ? parseFloat(arrData.Qt_Producto).toFixed(2) : '') + '</td>'+
								'</tr>';
								}
								if (iIdTipoRubroEmpresaGlobal == 1) {//1=Farmacia
									sHtmlTableInfoItem +=
									'<tr>'+
										'<td class="text-left" style="width: 25%"><b>Composición</b></td>'+
										'<td class="text-left" style="width: 75%">' + (arrData.Txt_Composicion != null ? Txt_Composicion : '-') + '</td>'+
									'</tr>'+
									'<tr>'+
										'<td class="text-left" style="width: 25%"><b>Receta Médica</b></td>'+
										'<td class="text-left" style="width: 75%">' + (arrData.Nu_Receta_Medica == 0 ? 'No' : 'Si') + '</td>'+
									'</tr>';
								}
								if (iIdTipoRubroEmpresaGlobal != 3) {//3=Lavandería
								sHtmlTableInfoItem +=
								'<tr>'+
									'<td class="text-left" style="width: 25%"><b>Ubicación</b></td>'+
									'<td class="text-left" style="width: 75%">' + (arrData.Txt_Ubicacion_Producto_Tienda != null ? arrData.Txt_Ubicacion_Producto_Tienda : '-') + '</td>'+
								'</tr>'+
								'<tr>'+
									'<td class="text-left" style="width: 25%"><b>Descripción</b></td>'+
									'<td class="text-left" style="width: 75%">' + (arrData.Txt_Producto != null ? arrData.Txt_Producto : '-') + '</td>'+
								'</tr>';
								}
								sHtmlTableInfoItem +=
							'</tbody>'+
						'</table>';
						$( '#modal-body-info_item' ).append( sHtmlTableInfoItem );
					} else {
						console.log(arrResponseComposicion);
					}
				}, 'json'); // /. obtener composicion por item
			} else {
				$( '#modal-message' ).modal('show');
				$( '.modal-message' ).css("z-index", "2000");
				$( '.modal-message' ).addClass( 'modal-' + arrResponse.sStatus );
				$( '.modal-title-message' ).text( arrResponse.sMessage );
				setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
			}
		}, 'JSON');
	})
  	// ./ Lista de productos a compra

	// Button agregar items alternativos a venta temporal
	$( '#table-items_alternativos tbody' ).on('click', '#btn-agregar_item_venta_temporal', function(){
		arrItemVentaTemporal={
			iIdItem:$(this).data('id'),
			sNombreItem:$(this).data('nombre'),
			qItem:$(this).data('qt_producto'),
			fPrecio:$(this).data('precio'),
			iIdImpuestoCruceDocumento:$(this).data('id_impuesto_cruce_documento'),
			iTipoImpuesto:$(this).data('nu_tipo_impuesto'),
			fImpuesto:$(this).data('ss_impuesto'),
		}
		agregarItemVentaTemporal(arrItemVentaTemporal);
	})
	// ./ Button agregar items alternativos a venta temporal

	// Aperturar Modal de Cobranza del cliente
	$( "#btn-pagar" ).click(function(){
		cobrarCliente();
	})

	// Modal de cobranza al cliente
	$( '#cbo-modal_forma_pago' ).change(function(){
		$( '.div-billete_soles' ).show();

		ID_Medio_Pago = $(this).val();
		Nu_Tipo_Medio_Pago = $(this).find(':selected').data('nu_tipo_medio_pago');
		$( '.div-modal_datos_tarjeta_credito').hide();
		$( '#cbo-modal_tarjeta_credito' ).html('');
		$( '#tel-nu_referencia' ).val('');
		$( '#tel-nu_ultimo_4_digitos_tarjeta' ).val('');
		if (Nu_Tipo_Medio_Pago==2){
			$( '.div-billete_soles' ).hide();
			$( '.div-modal_datos_tarjeta_credito').show();

			url = base_url + 'HelperController/getTiposTarjetaCredito';
			$.post( url, {ID_Medio_Pago : ID_Medio_Pago} , function( response ){
				$( '#cbo-modal_tarjeta_credito' ).html('');
				for (var i = 0; i < response.length; i++)
					$( '#cbo-modal_tarjeta_credito' ).append( '<option value="' + response[i].ID_Tipo_Medio_Pago + '">' + response[i].No_Tipo_Medio_Pago + '</option>' );
			}, 'JSON');
		} else if ( Nu_Tipo_Medio_Pago==1 ) {
			$( '.div-billete_soles' ).hide();
		}

		setTimeout(function(){ $( '.input-modal_monto' ).focus(); $( '.input-modal_monto' ).select(); }, 20);		
	})
  
	// Agregar pagos de cliente
	$( '#btn-add_forma_pago' ).click(function(){
		agregarFormasPagoCliente();
	})

	$( '#table-modal_forma_pago thead' ).on('click', '.icon-clear_all_forma_pago_pos', function(){
		$( '#div-modal_forma_pago' ).hide();
		
		$( '#table-modal_forma_pago tbody' ).empty();
	  
		$( '.label-modal_forma_pago_monto_total' ).text('0.00');
		$( '.input-modal_forma_pago_monto_total' ).val(0.00);
		
		$( '.label-vuelto_pos' ).text('0.00');
		$( '.input-vuelto_pos' ).val(0.00);

		$( '.label-saldo_pos_cliente' ).text('0.00');
		$( '.input-saldo_pos_cliente' ).val(0.00);
		
		$( '.input-modal_monto' ).val(parseFloat($( '.input-total_detalle_productos_pos' ).val()));//Monto restante a cobrar
		$( '.input-modal_monto' ).focus();
		$( '.input-modal_monto' ).select();
		
		$( '#btn-add_forma_pago' ).prop('disabled', false);
		$( '.btn-generar_pedido' ).prop('disabled', true);
	})

	$Sum_Ss_Monto_Total = 0.00;
	$( '#table-modal_forma_pago tbody' ).on('click', '#btn-delete_forma_pago_pos', function(){
    	$(this).closest('tr').remove();
    
		$Sum_Ss_Monto_Total = 0.00;
		$( '#table-modal_forma_pago > tbody > tr' ).each(function(){
			var fila = $(this);
			$Sum_Ss_Monto_Total += parseFloat(fila.find(".fTotal").text());
		});

		$( '.label-modal_forma_pago_monto_total' ).text($Sum_Ss_Monto_Total);
		$( '.input-modal_forma_pago_monto_total' ).val($Sum_Ss_Monto_Total);
	
		$( '.input-modal_monto' ).val(parseFloat($( '.input-total_detalle_productos_pos' ).val()) - $Sum_Ss_Monto_Total);//Monto restante a cobrar
		$( '.input-modal_monto' ).focus();
	    
		$( '#btn-add_forma_pago' ).prop('disabled', false);
			
	    $( '.btn-generar_pedido' ).prop('disabled', false);
	    if ($( '#table-modal_forma_pago > tbody > tr' ).length == 0){
		    $( '#div-modal_forma_pago' ).hide();
		    
			$( '.label-modal_forma_pago_monto_total' ).text('0.00');
			$( '.input-modal_forma_pago_monto_total' ).val(0.00);
			
			$( '.label-vuelto_pos' ).text('0.00');
			$( '.input-vuelto_pos' ).val(0.00);
			
			$( '.label-saldo_pos_cliente' ).text('0.00');
			$( '.input-saldo_pos_cliente' ).val(0.00);
			
			$( '.input-modal_monto' ).val(parseFloat($( '.input-total_detalle_productos_pos' ).val()));//Monto restante a cobrar
			$( '.input-modal_monto' ).focus();
			$( '.input-modal_monto' ).select();
			
			$( '.btn-generar_pedido' ).prop('disabled', true);
	    }
	})// ./ btn Agregar formas de pago del cliente
	
	$( '.billete-soles' ).click(function(){
		$( '.input-modal_monto' ).val( $(this).val() );
		$( '.input-modal_monto' ).focus();
		$( '.input-modal_monto' ).select();
	})

	// Modal - Generar comprobante
	$( '#btn-ticket' ).click(function(){
		generarComprobante();
	})
	
	$( '#cbo-recepcion' ).change(function(){
		$( '.modal-delivery' ).modal('hide');
		if ( $(this).val() == 2 ) {
			$( '.modal-delivery' ).modal('show');
		}
	})
	
	$( '#btn-atajos_teclado' ).click(function(){
		$( '.modal-atajos_teclado' ).modal('show');
	})
	
	$( '#btn-add_nota_global' ).click(function(){
		$( '.modal-add_nota_global' ).modal('show');
	})
	
	$( '#table-detalle_productos_pos > tbody' ).on('click', '#btn-add_nota_producto_pos', function(){
		var fila = $(this).parents("tr");
		var id_item = fila.find( ".td-sNotaItem" ).data( "id_item" );
		var estado = fila.find( ".td-sNotaItem" ).data( "estado" );

		if (estado == 'mostrar') {
			fila.find( "#td-sNotaItem" + id_item ).show();
			fila.find( ".td-sNotaItem" ).data( "estado", "ocultar" );
			fila.find( ".td-sNombreItem" ).css( "width", "5%");
			fila.find( ".input-sNotaItem" ).focus();
		} else {
			fila.find( "#td-sNotaItem" + id_item ).hide();
			fila.find( ".td-sNotaItem" ).data( "estado", "mostrar" );
			fila.find( ".td-sNombreItem" ).css( "width", "44%");
		}
	})
}); // ./ document-ready

// Cargar items para ventas del mas vendido a menor
function getItems(arrParams){
	url = base_url + 'HelperController/getItems';
	var sendData = {
		ID_Almacen : 0,
		ID_Lista_Precio_Cabecera : arrParams.iIdListaPrecio,
		ID_Linea : 0,
	}
	
	$.post( url, sendData, function( response ){
		$( '.div-lista_cuadro_items' ).empty();
		var pos_items = '';
		var i = response.length;
		if (i > 0) {
			for (var x = 0; x < i; x++)
				pos_items +='<li class="li-item_pos list-group-item col-sm-4" value='+response[x].ID_Producto+'>'+response[x].No_Producto+'</li>';
		} else
			pos_items = '';		
		$( '.div-lista_cuadro_items' ).append(pos_items);
	}, 'JSON');
} // ./ cargar items por li cuadro para pantalla touch

// buscar item por evento clic
function buscarItem(n){
	var sendData = {
        global_table : 'producto',
        global_search : n,
        filter_id_almacen : $( '#cbo-sucursal' ).val(),
        filter_nu_compuesto : 0,
        filter_nu_tipo_producto : 2,//2 = Producto
        filter_lista : $( '#cbo-lista_precios' ).val(),
	};
	
  	var tr_body_table_detalle_productos_pos = "", $Ss_Total_Producto_POS = 0.00, $Sum_Ss_Total_POS = 0.00, Ss_Precio = 0;
	$.post( base_url + 'AutocompleteController/getAllProductClic', sendData, function( response ){
		response = response[0];//Me devuelve un arreglo
		Ss_Precio = 0;
		if ( response.Ss_Precio_Item !== null || response.Ss_Precio_Item != 0.000000 )
		  Ss_Precio = response.Ss_Precio_Item;
		if ( (response.Ss_Precio_Item === null || response.Ss_Precio_Item == 0.000000) && response.Ss_Precio !== null )
		  Ss_Precio = response.Ss_Precio;
		arrItemVentaTemporal={
			iIdItem:response.ID,
			iCodigoItem:response.Codigo,
			sNombreItem:response.Nombre,
			qItem:response.Qt_Producto,
			fPrecio:Ss_Precio,
			iIdImpuestoCruceDocumento:response.ID_Impuesto_Cruce_Documento,
			iTipoImpuesto:response.Nu_Tipo_Impuesto,
			fImpuesto:response.Ss_Impuesto,
		}
		agregarItemVentaTemporal(arrItemVentaTemporal);
	}, 'JSON')
}// ./ buscar item por evento clic

function agregarItemVentaTemporal(arrParams){
	$fPrecioItem=(arrParams.fPrecio !== null ? arrParams.fPrecio : '0')
	$fPrecioItem=(parseFloat($fPrecioItem).toFixed(2)).toString().split(". ");

	validacionAlertas(arrParams);
	
	if (iValidarStockGlobal == 1 && $( '#txt-nu_tipo_item' ).val() == 1 && (parseFloat(arrParams.qItem) <= 0.000000 || arrParams.qItem == null) ) {
		$modal_msg_stock = $( '.modal-message' );
		$modal_msg_stock.modal('show');

		$modal_msg_stock.removeClass('modal-danger modal-warning modal-success');
		$modal_msg_stock.addClass('modal-warning');

		$( '.modal-title-message' ).text('Sin stock disponible');

		setTimeout(function() {$modal_msg_stock.modal('hide');}, 1300);
	} else {
		$( '#txt-ID_Producto' ).val('');
		$( '#txt-Nu_Codigo_Barra' ).val('');
		$( '#txt-No_Producto' ).val('');
		$( '#txt-Ss_Precio' ).val('');
		$( '#txt-Ss_Precio_Interno' ).val('');
		$( '#txt-ID_Impuesto_Cruce_Documento' ).val('');
		$( '#txt-Nu_Tipo_Impuesto' ).val('');
		$( '#txt-Ss_Impuesto' ).val('');
		$( '#txt-Qt_Producto' ).val('');

		if (isExistElementProductoPOS(arrParams.iIdItem)) {//Si existe el item, sumamos
			ID_Item = arrParams.iIdItem;
			$( '#' + ID_Item ).val( parseFloat($( '#' + ID_Item ).val()) + 1 );//Sumar cantidad por ID

			$( '#table-detalle_productos_pos > tbody > tr' ).each(function(){
				fila = $(this);
				precio = parseFloat(fila.find(".input-fPrecioItem").val());
				cantidad = fila.find(".txt-Qt_Producto").val();
				fila.find(".td-Ss_Total_Producto").text( ((precio * cantidad).toFixed(2)).toString().split(". ") );
				calcularTotales();
			});

			$( '#' + arrParams.iIdItem ).focus();
			$( '#' + arrParams.iIdItem ).select();
		} else {
			var sCssDisplayPrecio = 'display:none;';
			if ( iIdTipoRubroEmpresaGlobal == 3 ) {//3=Lavandería
				sCssDisplayPrecio='';
			}

			var sCssDisplayDsctoPV = 'display:none;';
			if ( iActivarDescuentoPuntoVenta == 1 ) {//1 activo
				sCssDisplayDsctoPV='';
			}

			tr_body_table_detalle_productos_pos += 
			"<tr id='tr_detalle_producto_pos" + arrParams.iIdItem + "'>"
				+"<td style='display:none;' class='text-left td-iIdItem'>" + arrParams.iIdItem + "</td>"
				+"<td style='width: 15%' class='text-right'><input type='text' id=" + arrParams.iIdItem + " class='hotkey-limpiar_item hotkey-cancelar_venta hotkey-focus_item hotkey-cobrar_cliente txt-Qt_Producto form-control input-decimal input-qItem' onkeyup=validateStockNow(event); value='1' data-id_item=" + arrParams.iIdItem + " data-id_impuesto_cruce_documento='" + arrParams.iIdImpuestoCruceDocumento + "' data-nu_tipo_impuesto='" + arrParams.iTipoImpuesto + "' data-ss_impuesto='" + arrParams.fImpuesto + "' autocomplete='off' title='cantidad'></td>"
				+"<td style='width:35%' class='text-left td-sNombreItem' title='Nombre item'>" + arrParams.sNombreItem + "</td>"
				+ "<td style='width: 15%; " + sCssDisplayPrecio + "' class='text-right'><input type='text' class='hotkey-limpiar_item hotkey-cancelar_venta hotkey-focus_item hotkey-cobrar_cliente txt-Qt_Producto form-control input-decimal input-fPrecioItem' value='" + $fPrecioItem + "' data-id_item=" + arrParams.iIdItem + " data-id_impuesto_cruce_documento='" + arrParams.iIdImpuestoCruceDocumento + "' data-nu_tipo_impuesto='" + arrParams.iTipoImpuesto + "' data-ss_impuesto='" + arrParams.fImpuesto + "' autocomplete='off' title='precio'></td>"
				+"<td style='width: 15%; " + sCssDisplayDsctoPV + "' class='text-right'><input type='text' class='hotkey-limpiar_item hotkey-cancelar_venta hotkey-focus_item hotkey-cobrar_cliente form-control input-decimal input-fDescuentoItem' autocomplete='off' placeholder='Dscto.' title='Descuento (opcional)'></td>"
				+"<td style='width: 15%' class='text-right td-Ss_Total_Producto' title='total'>" + $fPrecioItem + "</td>"
				+"<td style='display:none; width: 39%' class='text-right td-sNotaItem' data-estado='mostrar' data-id_item=" + arrParams.iIdItem + " id='td-sNotaItem" + arrParams.iIdItem + "'>"
					+"<textarea class='form-control input-sNotaItem hotkey-cobrar_cliente hotkey-cancelar_venta hotkey-limpiar_item hotkey-focus_item' placeholder='' maxlength='250' autocomplete='off'></textarea></td>"
				+"</td>"
				+"<td class='text-center'>"
					+"<button type='button' id='btn-add_nota_producto_pos' class='btn btn-sm btn-link' alt='Nota' title='Nota'><i class='fa fa-edit fa-2x' aria-hidden='true'></i></button>"
				+"</td>"
				+"<td style='display:none;' class='text-right td-fDescuentoPorcentajeItem'>0</td>"
				+"<td style='display:none;' class='text-right td-fDescuentoSinImpuestosItem'>0</td>"
				+"<td style='display:none;' class='text-right td-fDescuentoImpuestosItem'>0</td>"
				+"<td style='display:none;' class='text-right td-fSubtotalItem'>" + ($fPrecioItem / parseFloat(arrParams.fImpuesto)).toFixed(6) + "</td>"
				+"<td style='display:none;' class='text-right td-fImpuestoItem'>" + ($fPrecioItem - ($fPrecioItem / parseFloat(arrParams.fImpuesto))).toFixed(6) + "</td>"
				+"<td style='width: 6%' class='text-center'><button type='button' id='btn-delete_producto_pos' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i></button></td>"
				+"<td style='width: 6%' class='text-center'><button type='button' id='btn-ver_producto_pos' data-id_item="+arrParams.iIdItem+" class='btn btn-sm btn-link' alt='Ver' title='Ver'>ver</button></td>"
			+"</tr>";

			$( '#table-detalle_productos_pos' ).show();
			$( '#table-detalle_productos_pos > tbody ' ).append(tr_body_table_detalle_productos_pos);
			tr_body_table_detalle_productos_pos='';

			$( '#' + arrParams.iIdItem ).focus();
			$( '#' + arrParams.iIdItem ).select();

			setTimeout(function(){ $( '#' + arrParams.iIdItem ).focus(); $( '#' + arrParams.iIdItem ).select(); }, 30);	

			calcularTotales();
	
			validateDecimal();
			
			// Combinacion de teclas
			$('input.hotkey-limpiar_item').bind('keydown', 'F2', function(){
				$( '#txt-ID_Producto' ).val( '' );
				$( '#txt-No_Producto' ).val( '' );
			});

			// Cancelar venta
			$('input.hotkey-cancelar_venta').bind('keydown', 'esc', function(){
				limpiarValoresVenta();
			});

			// Focus item
			$('input.hotkey-focus_item').bind('keydown', 'F4', function(){
				$( '#txt-No_Producto' ).focus();
			});
			// ./ Focus item

			// Button cobrar
			$('input.hotkey-cobrar_cliente').bind('keydown', 'return', function(){
				if ( $('#btn-pagar').prop('disabled') == false ){
					cobrarCliente();
				}
			});
			// ./ Button cobrar
			// ./ Combinacion de tecla
		}// ./ Verificacion si el item fue seleccionado para comprar
	}// ./ Validacion de Stock
}// Agregar venta temporal del cliente

// Buscar items alternativos
function buscarItemAlternativos(arrDataAlternativos){
	$.post( base_url + 'AutocompleteController/getItemAlternativos', arrDataAlternativos, function( response ){
		agregarItemAlternativos(response);
	}, 'JSON')
}// ./ Buscar items alternativos

// Buscar items alternativos de autocomplete
function autocompleteItemsAlternativos(sValue){
	if ( sValue.length > 0 ) {
		arrDataAlternativos={
			iIdListaPrecio : $( '#cbo-lista_precios' ).val(),
			sNombreUpcSkuItem : sValue,
			iValidarStockGlobal : iValidarStockGlobal,
		}
		$.post( base_url + 'AutocompleteController/autocompleteItemAlternativos', arrDataAlternativos, function( response ){
			agregarItemAlternativos(response);
		}, 'JSON')
	}
}

// Generar tabla de alternativos por composicion
function agregarItemAlternativos(arrResponse){
	$( '#table-items_alternativos tbody' ).empty();

	var tr_body_table_alternativo = '';
	if ( arrResponse.sStatus=='success' ) {
		var l = arrResponse.arrData.length, $fPrecioItem = 0;
		for (var x=0; x < l; x++){
			arrData = arrResponse.arrData;
			$fPrecioItem = 0;
			if ( arrData[x].Ss_Precio_Item !== null || arrData[x].Ss_Precio_Item != 0.000000 )
				$fPrecioItem = arrData[x].Ss_Precio_Item;
			if ( (arrData[x].Ss_Precio_Item === null || arrData[x].Ss_Precio_Item == 0.000000) && arrData[x].Ss_Precio !== null )
				$fPrecioItem = arrData[x].Ss_Precio;

			tr_body_table_alternativo += 
			"<tr id='tr_item_alternativo" + arrData[x].ID + "'>"
				+"<td style='display:none;' class='text-left'>" + arrData[x].iIdItem + "</td>"
				+"<td class='text-center' style='width: 10%'>" + arrData[x].Qt_Producto + "</td>"
				+"<td class='text-left' style='width: 60%'>" + arrData[x].Nombre + "</td>"
				+"<td class='text-right td-fPrecioItem' style='width: 20%'>" + $fPrecioItem + "</td>"
				+"<td class='text-center' style='width: 10%'><button type='button' id='btn-agregar_item_venta_temporal' class='btn btn-sm btn-link' alt='Agregar' title='Agregar' data-id='" + arrData[x].ID + "' data-nombre='" + arrData[x].Nombre + "' data-qt_producto='" + arrData[x].Qt_Producto + "' data-precio='" + $fPrecioItem + "' data-id_impuesto_cruce_documento='" + arrData[x].ID_Impuesto_Cruce_Documento + "' data-nu_tipo_impuesto='" + arrData[x].Nu_Tipo_Impuesto + "' data-ss_impuesto='" + arrData[x].Ss_Impuesto + "'>Agregar</button></td>"
			+"</tr>";
		}
    } else {
		tr_body_table_alternativo += 
		"<tr>"
			+"<td colspan='4' class='text-center'>" + arrResponse.sMessage + "</td>"
		+"</tr>";
	}

	$( '#table-items_alternativos' ).show();
	$( '#table-items_alternativos > tbody ' ).append(tr_body_table_alternativo);
	tr_body_table_alternativo='';
}// ./ Generar tabla de alternativos por composicion

function isExistElementProductoPOS($ID_Producto){
	return Array.from($('tr[id*=tr_detalle_producto_pos]'))
	.some(element => ($('td:nth(0)',$(element)).html()==$ID_Producto));
}

function validarVentaPrevia(){
	var arrValidarNumerosEnCero = [], $counterNumerosEnCero = 0, arrValidarDescuentoMayorTotal = [], $counterDescuentoMayorTotal = 0;
	$("#table-detalle_productos_pos > tbody > tr").each(function(){
		fila = $(this);
		
		$ID_Producto = fila.find(".td-iIdItem").text();
		$Qt_Producto = fila.find(".input-qItem").val();
		$Ss_Precio = parseFloat(fila.find(".input-fPrecioItem").val());
		$Ss_Total_Producto = fila.find(".td-Ss_Total_Producto").text();

		$fDescuentoItem = parseFloat(fila.find(".td-fDescuentoSinImpuestosItem").text()) + parseFloat(fila.find(".td-fDescuentoImpuestosItem").text());
		
		$('#tr_detalle_producto_pos' + $ID_Producto).removeClass('danger');
		if (parseFloat($Qt_Producto) == 0 || parseFloat($Ss_Precio) == 0 || parseFloat($Ss_Total_Producto) == 0){
			arrValidarNumerosEnCero[$counterNumerosEnCero] = $ID_Producto;
			$('#tr_detalle_producto_pos' + $ID_Producto).addClass('danger');
		}
		if ( parseFloat($fDescuentoItem) > parseFloat($Ss_Total_Producto) ){
			arrValidarDescuentoMayorTotal[$counterDescuentoMayorTotal] = $ID_Producto;
			$('#tr_detalle_producto_pos' + $ID_Producto).addClass('danger');
		}
		$counterNumerosEnCero++;
	});
	$( '#table-detalle_productos_pos tfoot' ).empty();
	
	// Se puede agregar logica de recorrer la tabla de items agregados y separarlos por tipo de impuesto y verificar 700 IGV y 350 EXO o INA
	if ( $( '#cbo-tipo_cliente' ).val()==1 && $( '.input-total_detalle_productos_pos' ).val() >= 700.00 ) {
		$( '#cbo-tipo_cliente' ).closest('.form-group').find('.help-block').html('La venta es mayor igual a <b>S/ 700.00</b> ingresar DNI y Nombres');
		$( '#cbo-tipo_cliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');

		scrollToError( $("html, body"), $( '#cbo-tipo_cliente' ) );
		return false;
	} else if ( $( '#cbo-tipo_cliente' ).val()==3 && $( '.input-total_detalle_productos_pos' ).val() >= 700.00 && ($( '#txt-ACodigo' ).val().length < 8 || $( '#txt-ANombre' ).val().length === 0) ) {
		$( '#cbo-tipo_cliente' ).closest('.form-group').find('.help-block').html('La venta es mayor igual a <b>S/ 700.00</b> ingresar DNI y Nombres');
		$( '#cbo-tipo_cliente' ).closest('.form-group').removeClass('has-success').addClass('has-error');

		scrollToError( $("html, body"), $( '#cbo-tipo_cliente' ) );
		return false;
	} else if ( $( '#cbo-tipo_cliente' ).val()==3 && ($( '#cbo-tipo_documento' ).val()== 3 || $( '#cbo-tipo_documento' ).val()== 4) && $( '#hidden-estado_entidad' ).val() == 0 ) {
		$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( ($( '#cbo-tipo_documento' ).val()== 4 ? 'DNI' : 'RUC') + ' inválido' );
		$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');

		scrollToError( $("html, body"), $( '#txt-ACodigo' ) );
		return false;
	} else if ($( '#cbo-tipo_cliente' ).val()==3 && $( '#cbo-tipo_documento' ).val()==4 && $( '#txt-ACodigo' ).val().length < 8 ) {
		$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html('Ingresar 8 dígitos' );
	  	$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');

		scrollToError( $("html, body"), $( '#txt-ACodigo' ) );
		return false;
	} else if ($( '#cbo-tipo_cliente' ).val()==3 && $( '#cbo-tipo_documento' ).val()==3 && $( '#txt-ACodigo' ).val().length < 11 ) {
		$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html('Ingresar ' + $( '#cbo-tipo_documento' ).find(':selected').data('nu_cantidad_caracteres') + ' dígitos' );
	  	$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');

		scrollToError( $("html, body"), $( '#txt-ACodigo' ) );
		return false;
	} else if (
		$( '#cbo-tipo_cliente' ).val()=='3'
		&& $( '#txt-Txt_Email_Entidad_Cliente' ).val().length > 0
		&& !caracteresCorreoValido($('[name="Txt_Email_Entidad"]').val(), '#div-email')
	) {//3=Cliente Nuevo
		scrollToError( $("html, body"), $( '#txt-Txt_Email_Entidad_Cliente' ) );
		alert( 'ingresa un correo válido' );
		return false;
	} else if (arrValidarNumerosEnCero.length > 0) {
		var tr_foot = '';
		tr_foot +=
		"<tfoot>"
			+"<tr class='danger'>"
				+"<td colspan='9' class='text-center'>Item(s) con <b>precio / cantidad / total en cero</b></td>"
			+"</tr>"
		+"<tfoot>";
		$( '#table-detalle_productos_pos > tbody' ).after(tr_foot);
		return false;
	} else if (arrValidarDescuentoMayorTotal.length > 0) {
		var tr_foot = '';
		tr_foot +=
		"<tfoot>"
			+"<tr class='danger'>"
				+"<td colspan='9' class='text-center'>Item(s) con <b>descuento mayor que el total</b></td>"
			+"</tr>"
		+"<tfoot>";
		$( '#table-detalle_productos_pos > tbody' ).after(tr_foot);
		return false;
	}
    $( '#table-detalle_productos_pos tfoot' ).empty();
	$( '.help-block' ).empty();
	return true;
}

function cobrarCliente(){
	// Validacion venta previa
	bEstadoValidacionPOS = validarVentaPrevia();
	if ( bEstadoValidacionPOS ){
		$( '.modal_forma_pago' ).modal('show');

		var $ID_Medio_Pago, $Nu_Tipo_Medio_Pago, $ID_Tarjeta_Credito, $No_Medio_Pago, $No_Tarjeta_Credito, $Nu_Tarjeta_Credito, $Nu_Transaccion, $Ss_Monto=0.00, $ID_Medio_Pago_Tarjeta_Credito, $Ss_Monto_Restante_Cobrar=0.00, tr_body_modal_forma_pago, $iVerificarIdMedioPagoGuardado = 0, $Sum_Ss_Monto_Total = 0.00;
		$("#table-modal_forma_pago > tbody > tr").each(function(){
			fila = $(this);
			$iVerificarIdMedioPagoGuardado = fila.find(".iIdMedioPago").text();
			$Sum_Ss_Monto_Total += parseFloat(fila.find(".fTotal").text());
		});
		
		$Ss_Monto_Restante_Cobrar = parseFloat($( '.input-total_detalle_productos_pos' ).val()) - $Sum_Ss_Monto_Total;
		
		if ( 
			(parseFloat($( '.input-modal_forma_pago_monto_total' ).val()) >= parseFloat($( '.input-total_detalle_productos_pos' ).val()))
			|| ($Ss_Monto_Restante_Cobrar > 0.00 && $iVerificarIdMedioPagoGuardado == 4)
		) {
			$( '.input-modal_monto' ).val( '' );
			$( '#btn-ticket' ).prop('disabled', false);
			
			$Sum_Ss_Monto_Total = 0.00;
			$("#table-modal_forma_pago > tbody > tr").each(function(){
				fila = $(this);
				$Sum_Ss_Monto_Total += parseFloat(fila.find(".fTotal").text());
			});
	
			$Ss_Vuelto_Pos = 0.00;
			$Ss_Vuelto_Pos = $Sum_Ss_Monto_Total - parseFloat($( '.input-total_detalle_productos_pos' ).val());
			
			if ($Ss_Vuelto_Pos >= 0){
				$( '.label-vuelto_pos' ).text( $Ss_Vuelto_Pos.toFixed(2) );
				$( '.input-vuelto_pos' ).val( $Ss_Vuelto_Pos.toFixed(2) );
				
				$( '#btn-add_forma_pago' ).prop('disabled', true);
				$( '.btn-generar_pedido' ).prop('disabled', false);
			}
		} else {
			$( '.input-modal_monto' ).val($( '.input-total_detalle_productos_pos' ).val());
			$( '#btn-add_forma_pago' ).prop('disabled', false);
			
			$( '.label-vuelto_pos' ).text( '0.00' );
			$( '.input-vuelto_pos' ).val( '0.00' );

			$('#btn-ticket').prop('disabled', true);
		}

		$( '.modal_forma_pago' ).on('shown.bs.modal', function() {
			$( '.input-modal_monto' ).focus();
			$( '.input-modal_monto' ).select();
			
			// Combinacion de tecla
			// Button add forma de pago y generar ticket			
			$('input.input-modal_monto').bind('keydown', 'return', function(){
				$Sum_Ss_Monto_Total = 0.00;
				$("#table-modal_forma_pago > tbody > tr").each(function(){
					fila = $(this);
					$iVerificarIdMedioPagoGuardado = fila.find(".iIdMedioPago").text();
					$Sum_Ss_Monto_Total += parseFloat(fila.find(".fTotal").text());
				});

				if ( $('#btn-add_forma_pago').prop('disabled') == false ){
					agregarFormasPagoCliente();
				}

				if ( 
					($Sum_Ss_Monto_Total >= parseFloat($('.input-total_detalle_productos_pos').val()) || ($Ss_Monto_Restante_Cobrar > 0.00 && $iVerificarIdMedioPagoGuardado == 4))
					&& $('#btn-ticket').prop('disabled') == false
					&& $( '#table-detalle_productos_pos > tbody > tr' ).length > 0
					&& $( '#table-modal_forma_pago > tbody > tr' ).length > 0
				){
					generarComprobante();
				}
			});
			// ./ Button generar ticket
			// ./ Combinacion de tecla
		})
	}
}

// Modal verificar si el cajero ingreso 2 veces el mismo medio de pago
function isExistElementFormaPago($ID_Medio_Pago_Tarjeta_Credito){
	return Array.from($('tr[id*=tr_forma_pago]'))
	.some(element => ($('td:nth(0)',$(element)).html()===$ID_Medio_Pago_Tarjeta_Credito));
}

function agregarFormasPagoCliente(){
	$ID_Medio_Pago = $( '#cbo-modal_forma_pago option:selected' ).val();
	$ID_Tarjeta_Credito = $( '#cbo-modal_tarjeta_credito option:selected' ).val();
	$No_Medio_Pago = $( '#cbo-modal_forma_pago option:selected' ).text();
	$No_Tarjeta_Credito = $( '#cbo-modal_tarjeta_credito option:selected' ).text();
	$Ss_Monto = parseFloat($( '.input-modal_monto' ).val());
	$sNumeroOperacion = $( '#tel-nu_referencia' ).val();
	$sUltimosDigitosTarjeta = $( '#tel-nu_ultimo_4_digitos_tarjeta' ).val();
	
	$Nu_Tipo_Medio_Pago = $( '#cbo-modal_forma_pago' ).find(':selected').data('nu_tipo_medio_pago');
	if ( $Nu_Tipo_Medio_Pago != 1 ) {
		$( '.th-label-vuelto' ).show();
		$( '.th-label-saldo' ).hide();
	} else {
		$( '.th-label-vuelto' ).hide();
		$( '.th-label-saldo' ).show();
	}

	$ID_Medio_Pago_Tarjeta_Credito = $ID_Medio_Pago + $ID_Tarjeta_Credito;

	$iVerificarIdMedioPagoGuardado = 0;
	$Sum_Ss_Monto_Total = 0.00;
	$("#table-modal_forma_pago > tbody > tr").each(function(){
		fila = $(this);
		$iVerificarIdMedioPagoGuardado = fila.find(".iIdMedioPago").text();
		$Sum_Ss_Monto_Total += parseFloat(fila.find(".fTotal").text());
	});
	
	if ($Nu_Tipo_Medio_Pago == 0)//Efectivo
		$ID_Tarjeta_Credito = 0;
	
	$( '.help-block' ).empty();
	if (isNaN($Ss_Monto)) {
		$( '.input-modal_monto' ).closest('.form-group').find('.help-block').html('Ingresar monto');
		$( '.input-modal_monto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	} else if ( ($iVerificarIdMedioPagoGuardado == 1 || $iVerificarIdMedioPagoGuardado == 2 || $iVerificarIdMedioPagoGuardado == 3) && ($ID_Medio_Pago == 4) ) {
		$( '.input-modal_monto' ).closest('.form-group').find('.help-block').html('No se puede mezclar crédito con otras formas de pago');
		$( '.input-modal_monto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	} else if ($Nu_Tipo_Medio_Pago != 1 && $Ss_Monto <= 0) {
		$( '.input-modal_monto' ).closest('.form-group').find('.help-block').html('Monto debe ser mayor 0');
		$( '.input-modal_monto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	} else if( isExistElementFormaPago($ID_Medio_Pago_Tarjeta_Credito) ){
		$( '#cbo-modal_forma_pago' ).closest('.form-group').find('.help-block').html('Ya existe medio pago');
		$( '#cbo-modal_forma_pago' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	} else if (
		(
			($Nu_Tipo_Medio_Pago == 2) ||
			($Nu_Tipo_Medio_Pago == 1)
		) && ($Ss_Monto > (parseFloat($( '.input-total_detalle_productos_pos' ).val()) - parseFloat($( '.input-modal_forma_pago_monto_total' ).val())) )
		) {
		$( '.input-modal_monto' ).closest('.form-group').find('.help-block').html('El monto a pagar es menor');
		$( '.input-modal_monto' ).closest('.form-group').removeClass('has-success').addClass('has-error');
	}  else {
		$( '.input-modal_monto' ).closest('.form-group').removeClass('has-error');

		tr_body_modal_forma_pago = "";
		tr_body_modal_forma_pago += 
		"<tr id='tr_forma_pago" + $ID_Medio_Pago_Tarjeta_Credito + "'>"
			+"<td style='display:none;' class='text-left'>" + $ID_Medio_Pago_Tarjeta_Credito + "</td>"
			+"<td style='display:none;' class='text-left iIdMedioPago'>" + $ID_Medio_Pago + "</td>"
			+"<td style='display:none;' class='text-left'>" + $ID_Tarjeta_Credito + "</td>"
			+"<td class='text-left'>" + $No_Medio_Pago + "</td>"
			+"<td class='text-left'>" + $No_Tarjeta_Credito + "</td>"
			+"<td class='text-right fTotal'>" + $Ss_Monto + "</td>"
			+"<td class='text-center'><button type='button' id='btn-delete_forma_pago_pos' class='btn btn-sm btn-link' alt='Eliminar' title='Eliminar'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i></button></td>"
			+"<td style='display:none;' class='text-left'>" + $sNumeroOperacion + "</td>"
			+"<td style='display:none;' class='text-left'>" + $sUltimosDigitosTarjeta + "</td>"
			+"<td style='display:none;' class='text-left td-iTipoVista'>" + $Nu_Tipo_Medio_Pago + "</td>"
		+"</tr>";

		$( '#div-modal_forma_pago' ).show();
		$( '#table-modal_forma_pago > tbody ' ).append(tr_body_modal_forma_pago);
		
		$Sum_Ss_Monto_Total = 0.00;
		$("#table-modal_forma_pago > tbody > tr").each(function(){
			fila = $(this);
			$Sum_Ss_Monto_Total += parseFloat(fila.find(".fTotal").text());
		});

		$( '.label-modal_forma_pago_monto_total' ).text($Sum_Ss_Monto_Total);
		$( '.input-modal_forma_pago_monto_total' ).val($Sum_Ss_Monto_Total);
		
		$Ss_Monto_Restante_Cobrar = parseFloat($( '.input-total_detalle_productos_pos' ).val()) - $Sum_Ss_Monto_Total;
		
		$( '.input-modal_monto' ).val('');
		if ($Ss_Monto_Restante_Cobrar > 0.00)
			$( '.input-modal_monto' ).val($Ss_Monto_Restante_Cobrar);
		
		$( '#btn-add_forma_pago' ).prop('disabled', false);
		$( '.btn-generar_pedido' ).prop('disabled', true);
		$Ss_Vuelto_Pos = 0.00;
		$Ss_Vuelto_Pos = $Sum_Ss_Monto_Total - parseFloat($( '.input-total_detalle_productos_pos' ).val());
		
		if ($Ss_Vuelto_Pos >= 0 && $Nu_Tipo_Medio_Pago != 1){//1 = Credito
			$( '.label-vuelto_pos' ).text( $Ss_Vuelto_Pos.toFixed(2) );
			$( '.input-vuelto_pos' ).val( $Ss_Vuelto_Pos.toFixed(2) );
			
			$( '#btn-add_forma_pago' ).prop('disabled', true);
			$( '.btn-generar_pedido' ).prop('disabled', false);
		}
		
		if ($Ss_Monto_Restante_Cobrar > 0.00 && $Nu_Tipo_Medio_Pago == 1) {//1 = Credito
			$( '.label-saldo_pos_cliente' ).text( $Ss_Monto_Restante_Cobrar.toFixed(2) );
			$( '.input-saldo_pos_cliente' ).val( $Ss_Monto_Restante_Cobrar.toFixed(2) );
			
			$( '#btn-add_forma_pago' ).prop('disabled', true);
			$( '.btn-generar_pedido' ).prop('disabled', false);
		}
		
		$( '.input-modal_monto' ).focus();
		$( '.input-modal_monto' ).select();
	}
}

function generarComprobante(){
	var arrCliente=Array(), arrCabecera=Array(), arrDetalle=Array(), arrFormaPago=Array(), obj = {}, $ID_Producto, $Qt_Producto, $Ss_Precio, $fDescuentoPorcentajeItem, $fDescuentoSinImpuestosItem, $fDescuentoImpuestosItem, $fSubtotalItem, $fImpuestoItem, $Ss_Total_Producto, $ID_Impuesto_Cruce_Documento, $sNotaItem;

	$("#table-detalle_productos_pos > tbody > tr").each(function(){
		fila = $(this);
		
		$ID_Producto = fila.find(".td-iIdItem").text();
		$Qt_Producto = fila.find(".input-qItem").val();
		$Ss_Precio = parseFloat(fila.find(".input-fPrecioItem").val());
		$fDescuentoPorcentajeItem = fila.find(".td-fDescuentoPorcentajeItem").text();
		$fDescuentoSinImpuestosItem = fila.find(".td-fDescuentoSinImpuestosItem").text();
		$fDescuentoImpuestosItem = fila.find(".td-fDescuentoImpuestosItem").text();
		$fSubtotalItem = fila.find(".td-fSubtotalItem").text();
		$fImpuestoItem = fila.find(".td-fImpuestoItem").text();
		$Ss_Total_Producto = parseFloat(fila.find(".td-Ss_Total_Producto").text());
		$ID_Impuesto_Cruce_Documento = fila.find(".txt-Qt_Producto").data('id_impuesto_cruce_documento');
		$sNotaItem = fila.find(".input-sNotaItem").val();

		obj = {};
		
		obj.ID_Producto = $ID_Producto;
		obj.Qt_Producto = $Qt_Producto;
		obj.Ss_Precio = $Ss_Precio;
		obj.fDescuentoPorcentajeItem = $fDescuentoPorcentajeItem;
		obj.fDescuentoSinImpuestosItem = $fDescuentoSinImpuestosItem;
		obj.fDescuentoImpuestosItem = $fDescuentoImpuestosItem;
		obj.fSubtotalItem = $fSubtotalItem;
		obj.fImpuestoItem = $fImpuestoItem;
		obj.Ss_Total_Producto = $Ss_Total_Producto;
		obj.ID_Impuesto_Cruce_Documento	= $ID_Impuesto_Cruce_Documento;
		obj.Txt_Nota = $sNotaItem;
		
		arrDetalle.push(obj);
	});

	if ( $( '#cbo-tipo_cliente' ).val()=='3' ) {//3=Cliente Nuevo
		arrCliente = {
			'ID_Tipo_Documento_Identidad' : $( '#txt-ID_Tipo_Documento_Identidad' ).val(),
			'Nu_Documento_Identidad' : $( '#txt-ACodigo' ).val(),
			'No_Entidad' : $( '#txt-ANombre' ).val(),
			'Nu_Estado' : 1,//1=Activo o 0=Inactivo
			'Nu_Celular_Entidad' : $('[name="Nu_Celular_Entidad"]').val(),
			'Txt_Email_Entidad' : $('[name="Txt_Email_Entidad"]').val(),
		};
	}
	
	var iIdTipoDocumento = $( '#cbo-tipo_documento' ).val();

	if ( iIdTipoDocumento == 5 )//Boleta OTROS
		iIdTipoDocumento = 4;

	arrCabecera = {
		'ID_Matricula_Empleado' : $( '#hidden-id_matricula_personal' ).val(),
		'ID_Moneda' : $( '#hidden-id_moneda_caja_pos' ).val(),
		'ID_Tipo_Documento' : iIdTipoDocumento,
		'iTipoCliente' : $( '#cbo-tipo_cliente' ).val(),
		'ID_Entidad' : $( '#txt-AID' ).val(),
		'Ss_Total' : parseFloat($( '.input-total_detalle_productos_pos' ).val()),
		'Ss_Total_Saldo' : parseFloat($( '.input-saldo_pos_cliente' ).val()),
		'ID_Lista_Precio_Cabecera' : $( '#cbo-lista_precios' ).val(),
		'Nu_Tipo_Recepcion' : $( '#cbo-recepcion' ).val(),
		'ID_Transporte_Delivery' : ($( '#cbo-recepcion' ).val() == 1 ? 0 : $( '#cbo-transporte' ).val()),
		'Nu_Transporte_Lavanderia_Hoy' : $( '#cbo-tipo_envio_lavado' ).val(),
		'sDireccionDelivery' : ($( '#cbo-recepcion' ).val() == 1 ? '' : $('[name="Txt_Direccion_Delivery"]').val()),
		'Fe_Entrega' : $( '#txt-fe_entrega' ).val(),
		'sGlosa' : $('[name="Txt_Glosa"]').val(),
		'iIdAlmacen' : $( '#cbo-almacen' ).val(),
	}

	var arrFormaPago = [], $ID_Medio_Pago, $ID_Tarjeta_Credito, $Ss_Monto, $sNumeroOperacion, $sUltimosDigitosTarjeta;
	$("#table-modal_forma_pago > tbody > tr").each(function(){
		fila = $(this);
		
		$ID_Medio_Pago = fila.find("td:eq(1)").text();
		$ID_Tarjeta_Credito = fila.find("td:eq(2)").text();
		$Ss_Monto = fila.find("td:eq(5)").text();
		$sNumeroOperacion = fila.find("td:eq(7)").text();
		$sUltimosDigitosTarjeta = fila.find("td:eq(8)").text();
		$iTipoVista = fila.find(".td-iTipoVista").text();
	
		obj = {};
		
		obj.ID_Medio_Pago = $ID_Medio_Pago;
		obj.ID_Tarjeta_Credito = $ID_Tarjeta_Credito;
		obj.Ss_Total = $Ss_Monto;
		obj.Nu_Transaccion = $sNumeroOperacion;
		obj.Nu_Tarjeta = $sUltimosDigitosTarjeta;
		obj.iTipoVista = $iTipoVista;
		arrFormaPago.push(obj);
	});

	var $url = base_url + 'PuntoVenta/POSController/agregarVentaPos';
	var $arrParamsPost = {
		arrCliente : arrCliente,
		arrCabecera : arrCabecera,
		arrDetalle : arrDetalle,
		arrFormaPago : arrFormaPago,
	};

	$( '#btn-ticket' ).text('');
	$( '#btn-salir' ).attr('disabled', true);
	$( '#btn-ticket' ).attr('disabled', true);
	$( '#btn-ticket' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );

	$.post( $url, $arrParamsPost, function( response ) {
		$( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
		$( '#modal-message' ).modal('show');

		if ( response.sStatus=='success' ) {
			$( '.modal_forma_pago' ).modal('hide');

			$( '.modal-message' ).addClass( 'modal-' + response.sStatus );
			$( '.modal-title-message' ).text( response.sMessage );
			setTimeout(function() {$('#modal-message').modal('hide');}, 1100);
			
			limpiarValoresVenta();
			
			// Mandar a imprimir impresora
			var Accion = 'imprimir', iIdDocumentoCabecera = response.iIdDocumentoCabecera, url_print = 'ocultar-img-logo_punto_venta_click';
			formatoImpresionTicket(Accion, iIdDocumentoCabecera, url_print);
		} else {
			$( '.modal_forma_pago' ).modal('hide');
			
			limpiarValoresVenta();
			
			$( '.modal-message' ).addClass( 'modal-' + response.sStatus );
			$( '.modal-title-message' ).text( response.sMessage );
			setTimeout(function() {$('#modal-message').modal('hide');}, 6500);
		}
		
		$( '#btn-ticket' ).text('');
		$( '#btn-ticket' ).append( 'Generar comprobante' );
		$( '#btn-ticket' ).attr('disabled', false);
		$( '#btn-salir' ).attr('disabled', false);
	}, 'json')
	.fail(function(jqXHR, textStatus, errorThrown) {
		$( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
		
		$( '#modal-message' ).modal('show');
		$( '.modal-message' ).addClass( 'modal-danger' );
		$( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
		setTimeout(function() {$('#modal-message').modal('hide');}, 6000);
		
		//Message for developer
		console.log(jqXHR.responseText);

		$( '#btn-ticket' ).text('');
		$( '#btn-ticket' ).attr('disabled', false);
		$( '#btn-ticket' ).append( 'Generar comprobante' );
		$( '#btn-salir' ).attr('disabled', false);
	})
}

// Funciones para LAE API
function api_sunat_reniec(n){
	var iIdTipoDocumento = $( '#cbo-tipo_documento' ).val();
	var iCantidadCaracteresIngresados = n.length;
	var iNumeroDocumentoIdentidad = parseFloat($('#txt-ACodigo').val());
	
	if ( iIdTipoDocumento == 4 ) {
		getDatosxDNI(iIdTipoDocumento, iCantidadCaracteresIngresados, 8, iNumeroDocumentoIdentidad);
	} else if ( iIdTipoDocumento == 3 ) {
		getDatosxRUC(iIdTipoDocumento, iCantidadCaracteresIngresados, 11, iNumeroDocumentoIdentidad);
	}

	/*
	if ( iIdTipoDocumento == 2 || iIdTipoDocumento == 4 ) {
		getDatosxDNI(iIdTipoDocumento, iCantidadCaracteresIngresados, 8, iNumeroDocumentoIdentidad);
	} else if ( iIdTipoDocumento == 2 || iIdTipoDocumento == 3 ) {
		getDatosxRUC(iIdTipoDocumento, iCantidadCaracteresIngresados, 11, iNumeroDocumentoIdentidad);
	}
	*/
}

function getDatosxDNI(iIdTipoDocumento, iCantidadCaracteresIngresados, iCantidadCaracteres, iNumeroDocumentoIdentidad){
	if( iCantidadCaracteresIngresados == iCantidadCaracteres && isNaN(iNumeroDocumentoIdentidad) == false && $( '#txt-ACodigo' ).val() != $( '#hidden-nu_numero_documento_identidad' ).val() ) {//Si cumple con los caracteres para DNI / RUC
		var arrPost = {
			sNumeroDocumentoIdentidad : $( '#txt-ACodigo' ).val(),
		};
		$('#span-no_nombres_cargando').html('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');
		
		$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
		$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
		// Consulta Cliente en BD local
		$.post( base_url + 'AutocompleteController/getClienteEspecifico', arrPost, function( response ){
			$('#txt-ANombre').val('');
			$('#label-txt_estado_cliente').text('');

			$('#txt-Nu_Celular_Entidad_Cliente').val( '' );
			$('#txt-Txt_Email_Entidad_Cliente').val( '' );
			$('#span-celular').hide();
			$('#span-email').hide();

			if ( response.sStatus=='success' ) {
				$('#span-no_nombres_cargando').html('');
				$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

				var arrData = response.arrData;
				
				$('[name="AID"]').val( arrData[0].ID );
				$('[name="No_Entidad"]').val( arrData[0].Nombre );
				$('[name="Txt_Direccion_Entidad"]').val( arrData[0].Txt_Direccion_Entidad );

				$('#txt-ANombre').val(arrData[0].Nombre);
				$('#label-txt_direccion').text(arrData[0].Txt_Direccion_Entidad);
				$('#label-txt_estado_cliente').text('Existe en B.D. local');
				
				$('#hidden-estado_entidad').val(arrData[0].Nu_Estado);

				$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
				$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
			} else if ( response.sStatus=='warning' ) {// Si no existe en nuestra BD local, consultamos en el LAE API V1
				// Consulta LAE API V1 - RENIEC / SUNAT
				$( '#txt-AID' ).val('');
				$( '#txt-ANombre' ).val('');
				$( '#txt-Txt_Direccion_Entidad' ).val('');

				var url_api = 'https://laesystems.com/librerias_v3/v1/';
				var token='b683e1af998079cf9583167a89a6f919';										
				var url= url_api + token + '/reniec';
				var arrParams={
					'dni':$( '#txt-ACodigo' ).val(),
				}
				$.post( url, arrParams, function( data ) {
					$('#span-no_nombres_cargando').html('');
					$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

					if ( data.success == true ) {
						var arrResponseReniecLibreririav3 = data.result;
						var sNombreCompleto = arrResponseReniecLibreririav3.preNombres + ' ' + arrResponseReniecLibreririav3.apePaterno + ' ' + arrResponseReniecLibreririav3.apeMaterno;
						$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
						$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
						
						$('[name="No_Entidad"]').val( sNombreCompleto );
						$('#txt-ANombre').val( sNombreCompleto );

						$('#label-txt_direccion').text('');
						$('#label-txt_estado_cliente').text('librerias_v3');
						
						$('#hidden-estado_entidad').val( 1 );
					} else {
						$('[name="No_Entidad"]').val( '' );
						$('[name="Txt_Direccion_Entidad"]').val( '' );
		
						$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( 'DNI inválido' );
						$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
						
						$( '#txt-ACodigo' ).focus();
						$( '#txt-ACodigo' ).select();
	
						$('#txt-ANombre').val('');
						$('#label-txt_direccion').text('');
						$('#label-txt_estado_cliente').text('');

						$('#hidden-estado_entidad').val(0);
					}
				}, "json");
				// ./ Consulta LAE API V1 - RENIEC / SUNAT
			} else {
				$('#label-txt_direccion').text(response.sMessage);
			}
		}, 'JSON')
		// ./ Consulta de Cliente en BD local
	} else {
		$('#hidden-estado_entidad').val(1);

		$('#txt-ACodigo').closest('.form-group').find('.help-block').html('');
		$('#txt-ACodigo').closest('.form-group').removeClass('has-error');
	}
}

function getDatosxRUC(iIdTipoDocumento, iCantidadCaracteresIngresados, iCantidadCaracteres, iNumeroDocumentoIdentidad){
	if( iCantidadCaracteresIngresados == iCantidadCaracteres && isNaN(iNumeroDocumentoIdentidad) == false && $( '#txt-ACodigo' ).val() != $( '#hidden-nu_numero_documento_identidad' ).val() ) {//Si cumple con los caracteres para DNI / RUC
		var arrPost = {
			sNumeroDocumentoIdentidad : $( '#txt-ACodigo' ).val(),
		};
		$('#span-no_nombres_cargando').html('<i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>');

		$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
		$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
		// Consulta Cliente en BD local
		$.post( base_url + 'AutocompleteController/getClienteEspecifico', arrPost, function( response ){
			$('#txt-ANombre').val('');
			$('#label-txt_estado_cliente').text('');

			$('#txt-Nu_Celular_Entidad_Cliente').val( '' );
			$('#txt-Txt_Email_Entidad_Cliente').val( '' );
			$('#span-celular').hide();
			$('#span-email').hide();

			if ( response.sStatus=='success' ) {
				$('#span-no_nombres_cargando').html('');
				$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

				var arrData = response.arrData;
				
				$('[name="AID"]').val( arrData[0].ID );
				$('[name="No_Entidad"]').val( arrData[0].Nombre );
				$('[name="Txt_Direccion_Entidad"]').val( arrData[0].Txt_Direccion_Entidad );

				$('#txt-ANombre').val(arrData[0].Nombre);
				$('#label-txt_direccion').text(arrData[0].Txt_Direccion_Entidad);
				$('#label-txt_estado_cliente').text('Existe en B.D. local');
				
				$('#hidden-estado_entidad').val(arrData[0].Nu_Estado);

				$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
				$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');
			} else if ( response.sStatus=='warning' ) {// Si no existe en nuestra BD local, consultamos en el LAE API V1
				// Consulta LAE API V1 - RENIEC / SUNAT
				$( '#txt-AID' ).val('');
				$( '#txt-ANombre' ).val('');
				$( '#txt-Txt_Direccion_Entidad' ).val('');
				
				var url_api = 'https://www.laesystems.com/librerias/sunat/partner/format/json/x-api-key/';
				url_api = url_api + sTokenGlobal;
				
				var data = {
					ID_Tipo_Documento_Identidad : 4,
					Nu_Documento_Identidad : $( '#txt-ACodigo' ).val(),
				};
		
				$.ajax({
					url   : url_api,
					type  : 'POST',
					data  : data,
					success: function(response){
						$('#span-no_nombres_cargando').html('');
						$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

						if (response.success == true){
							$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( '' );
							$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-error');

							$('[name="No_Entidad"]').val( response.data.No_Names );
							$('#txt-ANombre').val(response.data.No_Names);

							$('[name="Txt_Direccion_Entidad"]').val( response.data.Txt_Address );
							$('#label-txt_direccion').text(response.data.Txt_Address);
							
							$('#label-txt_estado_cliente').text('Nube');
							
							$('#hidden-estado_entidad').val(response.data.Nu_Status);
						} else {
							$('[name="No_Entidad"]').val( '' );
							$('[name="Txt_Direccion_Entidad"]').val( '' );
			
							$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( response.msg );
							$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
							
							$( '#txt-ACodigo' ).focus();
							$( '#txt-ACodigo' ).select();
		
							$('#txt-ANombre').val('');
							$('#label-txt_direccion').text('');
							$('#label-txt_estado_cliente').text('');

							$('#hidden-estado_entidad').val(0);
						}
					},
					error: function(response){
						$('#hidden-nu_numero_documento_identidad').val( $( '#txt-ACodigo' ).val() );

						$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( 'Sin acceso' );
						$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
						
						$( '[name="No_Entidad"]' ).val( '' );
						$( '[name="Txt_Direccion_Entidad"]' ).val( '' );
		
						$('#span-no_nombres_cargando').html('');
						$('#txt-ANombre').val('');
						$('#label-txt_direccion').text('');
						$('#label-txt_estado_cliente').text('');

						$('#hidden-estado_entidad').val(0);
					}
				});
    			return;
				// ./ Consulta LAE API V1 - RENIEC / SUNAT
			} else {
				$( '#txt-ACodigo' ).closest('.form-group').find('.help-block').html( response.sMessage );
				$( '#txt-ACodigo' ).closest('.form-group').removeClass('has-success').addClass('has-error');
			}
		}, 'JSON')
		// ./ Consulta de Cliente en BD local
	}
}

function limpiarValoresVenta(){
	$( '#label_correo' ).show();
	$( '#txt-Txt_Email_Entidad_Cliente' ).show();
	$( '#form-modal_venta_pos_forma_pago' )[0].reset();
	
	$( '#table-items_alternativos tbody' ).empty();
	$( '#table-detalle_productos_pos tbody' ).empty();
	$( '#table-modal_forma_pago tbody' ).empty();
		
	// Limpiar alertas
	$( '#div-col-alerta-stock_minimo' ).remove();
	$( '#div-col-alerta-receta_medica' ).remove();
	$( '#div-col-alerta-lote_vencimiento' ).remove();

	// Limpiar cliente
	// Set selected
	if ( iIdTipoRubroEmpresaGlobal != 3 ) {
		$( '#cbo-tipo_cliente' ).val('1');
		$( '.div-nuevo_cliente' ).hide();//tipo de documento y numero de documento
		$( '#cbo-tipo_documento').val('4');// 4 = Boleta
	} else {
		$( '#cbo-tipo_cliente' ).val('3');
		$( '.div-nuevo_cliente' ).show();//tipo de documento y numero de documento
		$( '#cbo-tipo_documento').val('2');// Tipo comprobante
	}
	
	$( '#hidden-nu_numero_documento_identidad' ).val( '' );
	$( '#hidden-estado_entidad' ).val( '' );
	$( '#txt-AID' ).val( '' );
	$( '#txt-ANombre' ).val( '' );
	$( '#txt-ACodigo' ).val( '' );
	$( '#span-no_nombres_cargando' ).html( '' );
	$( '#txt-Nu_Celular_Entidad_Cliente' ).val( '' );
	$( '#txt-Txt_Email_Entidad_Cliente' ).val( '' );
	$( '#span-celular' ).hide();
	$( '#span-email' ).hide();

	// Tipo envio y recepcion
	$( '#cbo-transporte').val('0');
	$( '[name="Txt_Direccion_Delivery"]').val( '' );
	$( '#cbo-tipo_envio_lavado').val('1');

	$( '#cbo-recepcion' ).val('1');

	// Limpiar modal forma de pago
	$( '#div-modal_forma_pago' ).hide();
	$( '#cbo-modal_forma_pago' ).select().trigger('change');

	$( '.input-modal_monto' ).closest('.form-group').removeClass('has-error');

	$( '.label-modal_forma_pago_monto_total' ).text('0.00');
	$( '.input-modal_forma_pago_monto_total' ).val(0.00);
	
	$( '.label-total_detalle_productos_pos' ).text('0.00');
	$( '.input-total_detalle_productos_pos' ).val(0.00);
	
	$( '.label-vuelto_pos' ).text('0.00');
	$( '.input-vuelto_pos' ).val(0.00);
	
	$( '.label-saldo_pos_cliente' ).text('0.00');
	$( '.input-saldo_pos_cliente' ).val(0.00);

	$( '#cbo-descuento' ).val('1');
	$( '#cbo-descuento' ).attr('disabled', false);
	$( '#btn-pagar' ).attr('disabled', true);
	$( '#cbo-tipo_envio_lavado' ).prop('disabled', true);
	$( '#txt-fe_entrega' ).prop('disabled', true);
	$( '#cbo-recepcion' ).prop('disabled', true);
	$( '#btn-add_forma_pago' ).attr('disabled', false);

	$('[name="Txt_Glosa"]').val( '' );

	// Limpiar item
	$( '#txt-ID_Producto' ).val('');
	$( '#txt-No_Producto' ).val('');
	setTimeout(function(){ $('#txt-No_Producto').focus(); }, 20);
}

function validacionAlertas(arrParams){
	// Validaciones de alertas
	$( '#div-col-alerta-stock_minimo' ).remove();
	$( '#div-col-alerta-receta_medica' ).remove();
	$( '#div-col-alerta-lote_vencimiento' ).remove();

	// alerta stock mínimo
	var arrParamsAlertas = {
		'sTipoAlerta' : 'Stock_Minimo',
		'iIdItem' : arrParams.iIdItem,
		'fCantidadItem' : arrParams.qItem,
	};
	$.post( base_url + 'HelperController/validacionAlertaItem', arrParamsAlertas, function( arrResponse ){
		if (arrResponse.sStatus == 'success') {
			var sHtmlAlertaStockMinimo = '';
			sHtmlAlertaStockMinimo += '<div id="div-col-alerta-stock_minimo" class="alert alert-danger" style="padding: 5px; margin-bottom: 5px;">' + arrResponse.sMessage + ' ' + arrResponse.arrData[0].Nu_Stock_Minimo + ' vs Stock Actual ' + arrParams.qItem + '</div>';
			$( '#div-row-alertas' ).append( sHtmlAlertaStockMinimo );
		} else {
			console.log(arrResponse.sMessage);
		}
	}, 'JSON')// ./ alerta stock mínimo

	// alerta receta médica
	if ( iIdTipoRubroEmpresaGlobal == 1 ) {// 1 = Farmacia
		var arrParamsAlertas = {
			'sTipoAlerta' : 'Venta_Receta_Medica',
			'iIdItem' : arrParams.iIdItem,
		};
		$.post( base_url + 'HelperController/validacionAlertaItem', arrParamsAlertas, function( arrResponse ){
			if (arrResponse.sStatus == 'success') {
				var sHtmlAlertaRecetaMedica = '';
				sHtmlAlertaRecetaMedica += '<div id="div-col-alerta-receta_medica" class="alert alert-danger" style="padding: 5px; margin-bottom: 5px;">' + arrResponse.sMessage + '</div>';
				$( '#div-row-alertas' ).append( sHtmlAlertaRecetaMedica );
			} else {
				console.log(arrResponse.sMessage);
			}
		}, 'JSON')// ./ alerta receta médica
	}

	// alerta lote vencimiento
	var arrParamsAlertas = {
		'sTipoAlerta' : 'Lote_Vencimiento',
		'iIdItem' : arrParams.iIdItem,
	};
	$.post( base_url + 'HelperController/validacionAlertaItem', arrParamsAlertas, function( arrResponse ){
		if (arrResponse.sStatus == 'success') {
			if ( arrResponse.dToday >= arrResponse.dLoteVencimientoOperacion ) {
				var sHtmlAlertaStockMinimo = '';
				sHtmlAlertaStockMinimo += '<div id="div-col-alerta-lote_vencimiento" class="alert alert-danger" style="padding: 5px; margin-bottom: 5px;">' + arrResponse.sMessage + ' Nro. ' + arrResponse.Nu_Lote_Vencimiento + ' - F. Vencimiento: ' + ParseDate(arrResponse.dLoteVencimiento) + '</div>';
				$( '#div-row-alertas' ).append( sHtmlAlertaStockMinimo );
			}
		} else {
			console.log(arrResponse.sMessage);
		}
	}, 'JSON')// ./ alerta lote vencimiento
}

function calcularImportexItem(fila){
	fila = fila;
	cantidad = parseFloat(fila.find(".txt-Qt_Producto").val());
	fImpuestoTributario = parseFloat(fila.find(".txt-Qt_Producto").data('ss_impuesto'));
	precio = parseFloat(fila.find(".input-fPrecioItem").val());

	fTotalItem = (precio * cantidad);
	fDescuentoItem = parseFloat(fila.find(".input-fDescuentoItem").val());
	fila.find(".td-fDescuentoPorcentajeItem").text( ($( '#cbo-descuento' ).val() == 1 ? 0 : fDescuentoItem) );

	fDescuentoItem = ($( '#cbo-descuento' ).val() == 1 ? fDescuentoItem : (fDescuentoItem * (fTotalItem / 100)));
	if ( fTotalItem > fDescuentoItem )
		fila.find(".td-Ss_Total_Producto").text( (fTotalItem - fDescuentoItem) );
	else if ( isNaN(fDescuentoItem))
		fila.find(".td-Ss_Total_Producto").text( fTotalItem );
	else if ( fDescuentoItem > fTotalItem ) {
		alert( 'El descuento es mayor que el total' );
		fila.find(".input-fDescuentoItem").focus();
	}

	fTotalItem = parseFloat(fila.find(".td-Ss_Total_Producto").text());
	fSubtotalItem = (fTotalItem / fImpuestoTributario).toFixed(6);
	fila.find(".td-fDescuentoSinImpuestosItem").text( parseFloat(fDescuentoItem / fImpuestoTributario).toFixed(2) );
	fila.find(".td-fDescuentoImpuestosItem").text( parseFloat(fDescuentoItem - (parseFloat(fDescuentoItem / fImpuestoTributario))).toFixed(6) );
	fila.find(".td-fSubtotalItem").text( fSubtotalItem );
	fila.find(".td-fImpuestoItem").text( (fila.find(".td-Ss_Total_Producto").text() - fila.find(".td-fSubtotalItem").text()).toFixed(6) );

	calcularTotales();
}

function calcularTotales(){
	$fGravadaTotal = 0.00;
	$fExoneradaTotal = 0.00;
	$fInafectaTotal = 0.00;
	$fGratuitaTotal = 0.00;
	$fTotal = 0.00;
	$fDescuentoTotalItem = 0.00;
	$( '#table-detalle_productos_pos > tbody > tr' ).each(function(){
		fila = $(this);
		fImpuestoTributario = parseFloat(fila.find(".txt-Qt_Producto").data('ss_impuesto'));
		iGrupoImpuestoTributario = fila.find(".txt-Qt_Producto").data('nu_tipo_impuesto');
		fSubtotalItem = parseFloat(fila.find(".td-Ss_Total_Producto").text() / fImpuestoTributario).toFixed(6);
		fImpuestoItem = (fila.find(".td-Ss_Total_Producto").text() - fSubtotalItem);

		if ( iGrupoImpuestoTributario == 1 )
			$fGravadaTotal += parseFloat(fSubtotalItem);
		else if ( iGrupoImpuestoTributario == 2 )
			$fExoneradaTotal += parseFloat(fila.find(".td-Ss_Total_Producto").text());
		else if ( iGrupoImpuestoTributario == 3 )
			$fInafectaTotal += parseFloat(fila.find(".td-Ss_Total_Producto").text());
		else if ( iGrupoImpuestoTributario == 4 )
			$fGratuitaTotal += parseFloat(fila.find(".td-Ss_Total_Producto").text());
			
		$fTotal += (parseFloat(fSubtotalItem) + parseFloat(fImpuestoItem));
		$fDescuentoTotalItem += (isNaN(parseFloat(fila.find(".input-fDescuentoItem").val())) ? 0 : parseFloat(fila.find(".input-fDescuentoItem").val()));
	});
	
	$( '.hidden-gravada' ).val($fGravadaTotal.toFixed(2));
	$( '.hidden-exonerada' ).val($fExoneradaTotal.toFixed(2));
	$( '.hidden-inafecta' ).val($fInafectaTotal.toFixed(2));
	$( '.hidden-gratuita' ).val($fGratuitaTotal.toFixed(2));
	$( '.input-total_detalle_productos_pos' ).val($fTotal.toFixed(2));
	$( '.label-total_detalle_productos_pos' ).text($fTotal.toFixed(2));
	
	$( '#cbo-descuento' ).attr('disabled', false);
	if ( $fDescuentoTotalItem > 0 )
		$( '#cbo-descuento' ).attr('disabled', true);
	
	$( '#btn-pagar' ).prop('disabled', true);
	$( '#cbo-tipo_envio_lavado' ).prop('disabled', true);
	$( '#txt-fe_entrega' ).prop('disabled', true);
	$( '#cbo-recepcion' ).prop('disabled', true);
	if ($fTotal > 0.00) {
		$( '#btn-pagar' ).prop('disabled', false);
		$( '#cbo-tipo_envio_lavado' ).prop('disabled', false);
		$( '#txt-fe_entrega' ).prop('disabled', false);
		$( '#cbo-recepcion' ).prop('disabled', false);				
	}
}

function crearItemModal(){
	$( '.help-block' ).empty();
	
	var iIDTipoExistenciaProducto = $( '#cbo-modal-tipoItem' ).val();
	if ( $( '#cbo-modal-grupoItem' ).val() == '0' || $( '#cbo-modal-grupoItem' ).val() == '2' )//Servicio o interno
		iIDTipoExistenciaProducto = 4;//Otros
	
	var arrProducto = Array();
	arrProducto = {
		'EID_Empresa'               : '',
		'EID_Producto'              : '',
		'ENu_Codigo_Barra'          : '',
		'Nu_Tipo_Producto'          : $( '#cbo-modal-grupoItem' ).val(),
		'ID_Tipo_Producto'          : iIDTipoExistenciaProducto,
		'ID_Ubicacion_Inventario'   : 1,
		'Nu_Codigo_Barra'           : $( '#txt-modal-upcItem' ).val(),
		'No_Producto'               : $( '[name="textarea-modal-nombreItem"]' ).val(),
		'No_Codigo_Interno'         : '',
		'ID_Impuesto'               : $( '#cbo-modal-impuestoItem' ).val(),
		'ID_Unidad_Medida'          : $( '#cbo-modal-unidad_medidaItem' ).val(),
		'ID_Marca'                  : '',
		'ID_Familia'                : '',
		'ID_Sub_Familia'            : '',
		'Nu_Compuesto'              : 0,
		'Nu_Estado'                 : 1,
		'Txt_Producto'              : '',
		'ID_Producto_Sunat'         : $( '#hidden-ID_Tabla_Dato' ).val(),
		'Nu_Stock_Minimo'           : '',
		'Nu_Receta_Medica'          : '',
		'ID_Laboratorio'            : '',
		'Nu_Lote_Vencimiento'       : '',
		'Txt_Ubicacion_Producto_Tienda' : '',
		'Ss_Precio' : 0,
		'Ss_Costo' : 0,
		'ID_Impuesto_Icbper' : 0,
		'Qt_CO2_Producto' : 0,
		'ID_Tipo_Pedido_Lavado' : 0,
        'Ss_Precio' : $( '#txt-modal-precioItem' ).val(),
        'Ss_Costo' : $( '#txt-modal-costoItem' ).val(),
        'No_Imagen_Item' : '',
	};
		
	$( '#btn-modal-salir' ).attr('disabled', true);
	
	$( '#btn-modal-crear_item' ).text('');
	$( '#btn-modal-crear_item' ).attr('disabled', true);
	$( '#btn-modal-crear_item' ).append( 'Guardando <i class="fa fa-refresh fa-spin fa-lg fa-fw"></i>' );

	$( '#modal-loader' ).modal('show');
	$( '#modal-loader' ).css("z-index", "3000");  

	url = base_url + 'Logistica/ReglasLogistica/ProductoController/crudProducto';
	$.ajax({
		type : 'POST',
		dataType : 'JSON',
		url	: url,
		data : {
			arrProducto : arrProducto,
			arrProductoEnlace : null,
		},
		success : function( response ){    		  
			$( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
			$( '#modal-message' ).modal('show');
			$( '.modal-message' ).css("z-index", "3000");
			if (response.status == 'success'){
				$( '.modal-message' ).addClass(response.style_modal);
				$( '.modal-title-message' ).text(response.message);
				setTimeout(function() {$('#modal-message').modal('hide');}, 1200);

				$( '.modal-crear_item' ).modal('hide');
			} else {
				$( '.modal-message' ).addClass(response.style_modal);
				$( '.modal-title-message' ).text(response.message);
				setTimeout(function() {$('#modal-message').modal('hide');}, 1200);
			}// /. if - else crear item modal
			$( '#modal-loader' ).modal('hide');

			$( '#btn-modal-salir' ).attr('disabled', false);
				
			$( '#btn-modal-crear_item' ).text('');
			$( '#btn-modal-crear_item' ).append( '<span class="fa fa-save"></span> Guardar' );
			$( '#btn-modal-crear_item' ).attr('disabled', false);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			$( '#modal-loader' ).modal('hide');
			$( '.modal-message' ).removeClass('modal-danger modal-warning modal-success');
			
			$( '#modal-message' ).modal('show');
			$( '.modal-message' ).addClass( 'modal-danger' );
			$( '.modal-title-message' ).text( textStatus + ' [' + jqXHR.status + ']: ' + errorThrown );
			setTimeout(function() {$('#modal-message').modal('hide');}, 1700);
			
			//Message for developer
			console.log(jqXHR.responseText);
			
			$( '#btn-modal-salir' ).attr('disabled', false);
				
			$( '#btn-modal-crear_item' ).text('');
			$( '#btn-modal-crear_item' ).append( '<span class="fa fa-save"></span> Guardar' );
			$( '#btn-modal-crear_item' ).attr('disabled', false);
		}
	});
}